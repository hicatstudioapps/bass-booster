// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VerticalSeekBar;
import java.util.ArrayList;
import java.util.Arrays;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            k, BassBoosterService, m, BassBoosterApplication, 
//            n, g, h, i, 
//            j, AboutActivity, y, SettingsActivity, 
//            o

public abstract class f extends Activity
    implements android.widget.AdapterView.OnItemSelectedListener, android.widget.CompoundButton.OnCheckedChangeListener, android.widget.SeekBar.OnSeekBarChangeListener
{

    protected ArrayList a;
    protected ArrayList b;
    protected Spinner c;
    protected short d;
    protected SharedPreferences e;
    protected android.content.SharedPreferences.Editor f;
    protected Resources g;
    protected Intent h;
    protected BassBoosterService i;
    protected o j;
    protected IntentFilter k;
    protected android.app.AlertDialog.Builder l;
    protected RelativeLayout m;
    private CheckBox n;
    private SeekBar o;
    private short p;
    private boolean q;
    private boolean r;
    private AudioManager s;
    private ArrayList t;
    private ArrayList u;
    private CheckBox v;
    private boolean w;
    private boolean x;
    private boolean y;
    private ServiceConnection z;

    public f()
    {
        n = null;
        o = null;
        p = 800;
        q = false;
        r = true;
        s = null;
        a = new ArrayList();
        b = new ArrayList();
        t = new ArrayList();
        u = new ArrayList();
        v = null;
        c = null;
        d = 6;
        w = false;
        x = true;
        e = null;
        f = null;
        h = null;
        i = null;
        j = null;
        k = new IntentFilter();
        l = null;
        y = false;
        z = new k(this);
    }

    private void a(int i1, double d1, TextView textview)
    {
        String s1;
        textview.setEnabled(w);
        int j1 = (int)d1;
        if (d1 > 333D)
        {
            j1 = e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(i1)).toString(), 0) / 100;
        }
        s1 = (new StringBuilder()).append(String.valueOf(j1)).append("dB\n").toString();
        if (d >= 6) goto _L2; else goto _L1
_L1:
        i1;
        JVM INSTR tableswitch 0 4: default 128
    //                   0 159
    //                   1 184
    //                   2 209
    //                   3 234
    //                   4 259;
           goto _L3 _L4 _L5 _L6 _L7 _L8
_L3:
        textview.setText(s1);
        if (textview.getLineCount() > 2)
        {
            textview.setText(s1.replace("dB", ""));
        }
        return;
_L4:
        s1 = (new StringBuilder()).append(s1).append("100").toString();
        continue; /* Loop/switch isn't completed */
_L5:
        s1 = (new StringBuilder()).append(s1).append("300").toString();
        continue; /* Loop/switch isn't completed */
_L6:
        s1 = (new StringBuilder()).append(s1).append("1k").toString();
        continue; /* Loop/switch isn't completed */
_L7:
        s1 = (new StringBuilder()).append(s1).append("3k").toString();
        continue; /* Loop/switch isn't completed */
_L8:
        s1 = (new StringBuilder()).append(s1).append("10k").toString();
        if (true) goto _L3; else goto _L2
_L2:
        switch (i1)
        {
        case 0: // '\0'
            s1 = (new StringBuilder()).append(s1).append("15").toString();
            break;

        case 1: // '\001'
            s1 = (new StringBuilder()).append(s1).append("62").toString();
            break;

        case 2: // '\002'
            s1 = (new StringBuilder()).append(s1).append("250").toString();
            break;

        case 3: // '\003'
            s1 = (new StringBuilder()).append(s1).append("1k").toString();
            break;

        case 4: // '\004'
            s1 = (new StringBuilder()).append(s1).append("5k").toString();
            break;

        case 5: // '\005'
            s1 = (new StringBuilder()).append(s1).append("16k").toString();
            break;
        }
        if (true) goto _L3; else goto _L9
_L9:
    }

    static void a(f f1, int i1, double d1, TextView textview)
    {
        f1.a(i1, d1, textview);
    }

    static boolean a(f f1)
    {
        return f1.q;
    }

    static short b(f f1)
    {
        return f1.p;
    }

    static CheckBox c(f f1)
    {
        return f1.n;
    }

    static SeekBar d(f f1)
    {
        return f1.o;
    }

    private void d(int i1)
    {
        switch (i1)
        {
        default:
            o.setProgressDrawable(g.getDrawable(0x7f020042));
            for (int j1 = 0; j1 < a.size(); j1++)
            {
                ((VerticalSeekBar)a.get(j1)).setProgressDrawable(g.getDrawable(0x7f020045));
            }

            break;

        case 1: // '\001'
            o.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-1, -2));
            android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(-2, -1);
            layoutparams.gravity = 1;
            for (int k1 = 0; k1 < a.size(); k1++)
            {
                ((VerticalSeekBar)a.get(k1)).setLayoutParams(layoutparams);
            }

            break;

        case 2: // '\002'
            o.setProgressDrawable(g.getDrawable(0x7f020058));
            for (int l1 = 0; l1 < a.size(); l1++)
            {
                ((VerticalSeekBar)a.get(l1)).setProgressDrawable(g.getDrawable(0x7f02005a));
            }

            break;

        case 3: // '\003'
            o.setProgressDrawable(g.getDrawable(0x7f02002a));
            for (int i2 = 0; i2 < a.size(); i2++)
            {
                ((VerticalSeekBar)a.get(i2)).setProgressDrawable(g.getDrawable(0x7f02002c));
            }

            break;

        case 4: // '\004'
            o.setProgressDrawable(g.getDrawable(0x7f020033));
            for (int j2 = 0; j2 < a.size(); j2++)
            {
                ((VerticalSeekBar)a.get(j2)).setProgressDrawable(g.getDrawable(0x7f020035));
            }

            break;

        case 5: // '\005'
            o.setProgressDrawable(g.getDrawable(0x7f020016));
            for (int k2 = 0; k2 < a.size(); k2++)
            {
                ((VerticalSeekBar)a.get(k2)).setProgressDrawable(g.getDrawable((new int[] {
                    0x7f020018, 0x7f020019, 0x7f02001a, 0x7f02001b, 0x7f02001c, 0x7f02001d
                })[k2]));
            }

            break;
        }
        if (i1 != 1)
        {
            k();
        }
    }

    static boolean e(f f1)
    {
        return f1.w;
    }

    static CheckBox f(f f1)
    {
        return f1.v;
    }

    static ArrayList g(f f1)
    {
        return f1.u;
    }

    static ArrayList h(f f1)
    {
        return f1.t;
    }

    private void k()
    {
        android.graphics.drawable.Drawable drawable = g.getDrawable(0x7f02003e);
        o.setThumb(drawable);
        for (int i1 = 0; i1 < a.size(); i1++)
        {
            ((VerticalSeekBar)a.get(i1)).setThumb(drawable);
        }

    }

    int a(int i1)
    {
        return i1 / 10 + 100;
    }

    void a()
    {
        bindService(new Intent(this, com/desaxedstudios/bassbooster/BassBoosterService), z, 1);
    }

    int b(int i1)
    {
        return (i1 - 100) * 10;
    }

    protected abstract void b();

    double c(int i1)
    {
        return ((double)i1 - 100D) / 10D;
    }

    protected abstract void c();

    public void d()
    {
        ArrayAdapter arrayadapter = new ArrayAdapter(this, 0x1090008, new ArrayList(Arrays.asList(getResources().getStringArray(0x7f060000))));
        for (int i1 = 0; i1 < e.getInt("number_of_saved_custom_presets", 0); i1++)
        {
            arrayadapter.add(e.getString((new StringBuilder()).append("equalizer_custom_values_key").append(String.valueOf(i1 + 1)).append("_name").toString(), (new StringBuilder()).append("Custom Preset #").append(i1 + 1).toString()));
        }

        arrayadapter.setDropDownViewResource(0x1090009);
        c.setAdapter(arrayadapter);
        c.setSelection(e.getInt("equalizer_selected_preset_key", 0));
    }

    public void e()
    {
        short word0 = 0;
        while (word0 < d) 
        {
            if (android.os.Build.VERSION.SDK_INT >= 11 && e.getInt("key_app_theme", 0) != 1)
            {
                ObjectAnimator objectanimator = ObjectAnimator.ofInt(a.get(word0), "progress", new int[] {
                    a(e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(word0)).toString(), 0))
                });
                objectanimator.setDuration(400L);
                objectanimator.setInterpolator(new DecelerateInterpolator());
                objectanimator.start();
            } else
            {
                ((VerticalSeekBar)a.get(word0)).setProgressAndThumb(a(e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(word0)).toString(), 0)));
            }
            a(word0, 1000D, (TextView)t.get(word0));
            word0++;
        }
    }

    public boolean f()
    {
        r = true;
        try
        {
            BassBoost bassboost = new BassBoost(0, 0);
            bassboost.setStrength((short)800);
            bassboost.setEnabled(true);
            bassboost.setEnabled(false);
            bassboost.release();
        }
        catch (Exception exception)
        {
            r = false;
        }
        if (!r)
        {
            try
            {
                BassBoost bassboost1 = new BassBoost(0, 0);
                bassboost1.setStrength((short)800);
                bassboost1.setEnabled(true);
                bassboost1.setEnabled(false);
                bassboost1.release();
                r = true;
            }
            catch (Exception exception1)
            {
                r = false;
            }
        }
        return r;
    }

    public boolean g()
    {
        x = true;
        Object obj = new Equalizer(0, 0);
        short word0 = 0;
_L2:
        if (word0 >= ((Equalizer) (obj)).getNumberOfBands())
        {
            break; /* Loop/switch isn't completed */
        }
        ((Equalizer) (obj)).setBandLevel(word0, (short)10);
        word0++;
        if (true) goto _L2; else goto _L1
_L1:
        try
        {
            ((Equalizer) (obj)).setEnabled(true);
            ((Equalizer) (obj)).setEnabled(false);
            ((Equalizer) (obj)).release();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            x = false;
        }
        obj = new Equalizer(0, 0);
        d = ((Equalizer) (obj)).getNumberOfBands();
        f.putInt("number_of_eq_bands_key", d).apply();
        word0 = 0;
_L4:
        if (word0 >= ((Equalizer) (obj)).getNumberOfBands())
        {
            break; /* Loop/switch isn't completed */
        }
        ((Equalizer) (obj)).setBandLevel(word0, (short)10);
        word0++;
        if (true) goto _L4; else goto _L3
_L3:
        try
        {
            ((Equalizer) (obj)).setEnabled(true);
            ((Equalizer) (obj)).setEnabled(false);
            ((Equalizer) (obj)).release();
            x = true;
        }
        catch (Exception exception)
        {
            x = false;
        }
        return x;
    }

    public void h()
    {
        if (i != null)
        {
            i.a();
            i.c();
            i.b();
        }
    }

    public void i()
    {
        if (i != null)
        {
            i.a(3);
            i.c();
            i.b();
        }
    }

    public void j()
    {
        f.putBoolean("customization_update_needed", false).apply();
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(0x10000);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
    {
        boolean flag2;
        flag2 = false;
        if (flag && !e.getBoolean("KEY_SHOWED_TUTORIAL", false))
        {
            f.putBoolean("KEY_SHOWED_TUTORIAL", true);
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle(0x7f05003c);
            builder.setMessage(0x7f05003b);
            builder.setIcon(0x108009b);
            builder.setPositiveButton(getResources().getString(0x104000a), new m(this));
            builder.show();
        }
        compoundbutton.getId();
        JVM INSTR tableswitch 2131492882 2131492885: default 132
    //                   2131492882 161
    //                   2131492883 132
    //                   2131492884 132
    //                   2131492885 515;
           goto _L1 _L2 _L1 _L1 _L3
_L1:
        startService(h);
        if (i != null)
        {
            i();
            h();
            c();
        }
        return;
_L2:
        if (e.getBoolean("bassboost_enabled_key", true) != flag)
        {
            boolean flag1;
            if (flag)
            {
                compoundbutton = "bb_enabled";
            } else
            {
                compoundbutton = "bb_disabled";
            }
            BassBoosterApplication.a("UI - SFX", compoundbutton);
        }
        q = flag;
        f.putBoolean("bassboost_enabled_key", q).apply();
        o.setEnabled(q);
        if (o.getProgress() == 100)
        {
            o.setProgress(99);
            o.setProgress(100);
        } else
        {
            o.setProgress(o.getProgress() + 1);
            o.setProgress(o.getProgress() - 1);
        }
        if (q)
        {
            n.setText((new StringBuilder()).append(getString(0x7f050027)).append(": ").append(String.valueOf(p / 10)).append("%").toString());
        } else
        {
            n.setText(getString(0x7f050027));
        }
        if (q && !e.getBoolean("force_bass_boost_everywhere", false))
        {
            flag1 = flag2;
            if (!s.isWiredHeadsetOn())
            {
                flag1 = flag2;
                if (!s.isBluetoothA2dpOn())
                {
                    flag1 = flag2;
                    if (!s.isBluetoothScoOn())
                    {
                        flag1 = true;
                    }
                }
            }
            if (flag1)
            {
                compoundbutton = new android.app.AlertDialog.Builder(this);
                compoundbutton.setTitle(0x7f050060);
                compoundbutton.setMessage(0x7f05005f);
                compoundbutton.setIcon(0x108009b);
                compoundbutton.setPositiveButton(getResources().getString(0x104000a), new n(this));
                compoundbutton.show();
            }
        }
        continue; /* Loop/switch isn't completed */
_L3:
        int i1;
        if (e.getBoolean("equalizer_enabled_key", true) != flag)
        {
            if (flag)
            {
                compoundbutton = "eq_enabled";
            } else
            {
                compoundbutton = "eq_disabled";
            }
            BassBoosterApplication.a("UI - SFX", compoundbutton);
        }
        w = flag;
        f.putBoolean("equalizer_enabled_key", w).apply();
        i1 = 0;
        while (i1 < d) 
        {
            ((VerticalSeekBar)a.get(i1)).setEnabled(w);
            if (((VerticalSeekBar)a.get(i1)).getProgress() == 200)
            {
                ((VerticalSeekBar)a.get(i1)).setProgressAndThumb(199);
                ((VerticalSeekBar)a.get(i1)).setProgressAndThumb(200);
            } else
            {
                ((VerticalSeekBar)a.get(i1)).setProgressAndThumb(((VerticalSeekBar)a.get(i1)).getProgress() + 1);
                ((VerticalSeekBar)a.get(i1)).setProgressAndThumb(((VerticalSeekBar)a.get(i1)).getProgress() - 1);
            }
            ((TextView)t.get(i1)).setEnabled(w);
            i1++;
        }
        c.setEnabled(w);
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void onCreate(Bundle bundle)
    {
        int i1;
        super.onCreate(bundle);
        m = (RelativeLayout)RelativeLayout.inflate(this, 0x7f030001, null);
        setContentView(m);
        e = PreferenceManager.getDefaultSharedPreferences(this);
        f = e.edit();
        g = getResources();
        x = e.getBoolean("equalizer_compatible_key", false);
        r = e.getBoolean("bassboost_compatible_key", false);
        d = (short)e.getInt("number_of_eq_bands_key", 5);
        if (e.getInt("CURRENT_VERSION_1ST", 0) == 0)
        {
            i1 = 1;
        } else
        {
            i1 = 0;
        }
        Log.d("ABassBoosterActivity", (new StringBuilder()).append("KEY_FRESH_INSTALL = ").append(e.getInt("CURRENT_VERSION_1ST", 0)).toString());
        if (i1 == 0 && x && r) goto _L2; else goto _L1
_L1:
        int j1;
        try
        {
            j1 = getPackageManager().getPackageInfo(getPackageName(), 1).versionCode;
        }
        // Misplaced declaration of an exception variable
        catch (Bundle bundle)
        {
            j1 = 1;
        }
        Log.d("ABassBoosterActivity", (new StringBuilder()).append("v = ").append(j1).toString());
        f.putInt("CURRENT_VERSION_1ST", j1);
        f.putInt("equalizer_selected_preset_key", 0);
        f.putBoolean("equalizer_enabled_key", false);
        f.putBoolean("bassboost_enabled_key", false);
        f.apply();
        w = false;
        q = false;
        x = g();
        f.putBoolean("equalizer_compatible_key", x).apply();
        r = f();
        f.putBoolean("bassboost_compatible_key", r).apply();
        if (x || r) goto _L4; else goto _L3
_L3:
        l = new android.app.AlertDialog.Builder(this);
        l.setTitle(getString(0x7f050046));
        l.setMessage(getString(0x7f050049));
        l.setIcon(0x108009b);
        l.setPositiveButton(getString(0x104000a), new g(this));
        l.setNegativeButton("www.desaxed.com", new h(this));
        l.show();
_L2:
        if (!r)
        {
            f.putBoolean("bassboost_enabled_key", false).apply();
        }
        if (!x)
        {
            f.putBoolean("equalizer_enabled_key", false).apply();
        }
        r = e.getBoolean("bassboost_compatible_key", false);
        q = e.getBoolean("bassboost_enabled_key", false);
        p = (short)e.getInt("bassboost_strength_key", 800);
        x = e.getBoolean("equalizer_compatible_key", false);
        w = e.getBoolean("equalizer_enabled_key", false);
        d = (short)e.getInt("number_of_eq_bands_key", 5);
        t.add((TextView)findViewById(0x7f0c0018));
        t.add((TextView)findViewById(0x7f0c001b));
        t.add((TextView)findViewById(0x7f0c001e));
        t.add((TextView)findViewById(0x7f0c0021));
        t.add((TextView)findViewById(0x7f0c0024));
        t.add((TextView)findViewById(0x7f0c0027));
        u.add((LinearLayout)findViewById(0x7f0c0017));
        u.add((LinearLayout)findViewById(0x7f0c001a));
        u.add((LinearLayout)findViewById(0x7f0c001d));
        u.add((LinearLayout)findViewById(0x7f0c0020));
        u.add((LinearLayout)findViewById(0x7f0c0023));
        u.add((LinearLayout)findViewById(0x7f0c0026));
        o = (SeekBar)findViewById(0x7f0c0013);
        a.add((VerticalSeekBar)findViewById(0x7f0c0019));
        a.add((VerticalSeekBar)findViewById(0x7f0c001c));
        a.add((VerticalSeekBar)findViewById(0x7f0c001f));
        a.add((VerticalSeekBar)findViewById(0x7f0c0022));
        a.add((VerticalSeekBar)findViewById(0x7f0c0025));
        a.add((VerticalSeekBar)findViewById(0x7f0c0028));
        n = (CheckBox)findViewById(0x7f0c0012);
        v = (CheckBox)findViewById(0x7f0c0015);
        c = (Spinner)findViewById(0x7f0c0016);
        n.setOnCheckedChangeListener(this);
        o.setOnSeekBarChangeListener(this);
        for (i1 = 0; i1 < d; i1++)
        {
            ((VerticalSeekBar)a.get(i1)).setOnSeekBarChangeListener(this);
        }

        break; /* Loop/switch isn't completed */
_L4:
        if (!r && !e.getBoolean("bassboost_not_compatible_warning_was already_shown", false))
        {
            l = new android.app.AlertDialog.Builder(this);
            l.setTitle(getString(0x7f050046));
            l.setMessage(getString(0x7f050028));
            l.setIcon(0x108009b);
            l.setPositiveButton(getString(0x104000a), new i(this));
            l.show();
        } else
        if (!x && !e.getBoolean("equalizer_not_compatible_warning_was already_shown", false))
        {
            l = new android.app.AlertDialog.Builder(this);
            l.setTitle(getString(0x7f050046));
            l.setMessage(getString(0x7f050044));
            l.setIcon(0x108009b);
            l.setPositiveButton(getString(0x104000a), new j(this));
            l.show();
        } else
        if (i1 != 0)
        {
            startActivity(new Intent(this, com/desaxedstudios/bassbooster/AboutActivity));
            Toast.makeText(this, 0x7f050082, 1).show();
        }
        if (true) goto _L2; else goto _L5
_L5:
        c.setOnItemSelectedListener(this);
        v.setOnCheckedChangeListener(this);
        d(e.getInt("key_app_theme", 0));
        h = new Intent(this, com/desaxedstudios/bassbooster/BassBoosterService);
        startService(h);
        a();
        k = new IntentFilter("com.desaxedstudios.bassbooster.REFRESH_ACTIVITY");
        s = (AudioManager)getSystemService("audio");
        return;
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(0x7f0b0000, menu);
        return true;
    }

    public void onDestroy()
    {
        unbindService(z);
        super.onDestroy();
    }

    public void onItemSelected(AdapterView adapterview, View view, int i1, long l1)
    {
        adapterview = (new y(this)).a(i1, d);
        if (adapterview != null)
        {
            for (int j1 = 0; j1 < d; j1++)
            {
                f.putInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(j1)).toString(), ((Integer)adapterview.get(j1)).intValue());
            }

            f.apply();
            e();
        }
        f.putInt("equalizer_selected_preset_key", i1).apply();
        h();
        int k1 = e.getInt((new StringBuilder()).append("equalizer_custom_values_key").append(String.valueOf(i1 - 21)).append("_vol").toString(), -1);
        if (k1 >= 0)
        {
            ((AudioManager)getSystemService("audio")).setStreamVolume(3, k1, 0);
        }
        adapterview = g.getStringArray(0x7f060000);
        if (y)
        {
            y = false;
        } else
        if (i1 != 0 && i1 < adapterview.length && w)
        {
            BassBoosterApplication.a("UI - SFX", "eq_preset", adapterview[i1]);
            return;
        }
    }

    public void onNothingSelected(AdapterView adapterview)
    {
        e();
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 2131492926: 
            startActivity(new Intent(this, com/desaxedstudios/bassbooster/AboutActivity));
            return true;

        case 2131492925: 
            BassBoosterApplication.a("UI - Main", "menu_reset_eq");
            for (int i1 = 0; i1 < d && e.getBoolean("equalizer_compatible_key", true); i1++)
            {
                f.putInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(i1)).toString(), 0);
            }

            f.apply();
            e();
            c.setSelection(0);
            h();
            return true;

        case 2131492927: 
            BassBoosterApplication.a("UI - Main", "menu_quit");
            Toast.makeText(this, 0x7f050083, 1).show();
            f.putBoolean("equalizer_enabled_key", false);
            f.putBoolean("bassboost_enabled_key", false);
            f.apply();
            h();
            i();
            finish();
            return true;

        case 2131492924: 
            BassBoosterApplication.a("UI - Main", "menu_save");
            if (c.isEnabled() && c.getSelectedItemPosition() != 3)
            {
                y = true;
            }
            for (int j1 = 0; j1 < d && e.getBoolean("equalizer_compatible_key", true); j1++)
            {
                b.set(j1, Integer.valueOf(e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(j1)).toString(), 0)));
                f.putInt((new StringBuilder()).append("equalizer_custom_values_key").append(j1).toString(), ((Integer)b.get(j1)).intValue());
            }

            f.apply();
            c.setSelection(3);
            Toast.makeText(this, getString(0x7f05002c), 1).show();
            return true;

        case 2131492923: 
            startActivity(new Intent(this, com/desaxedstudios/bassbooster/SettingsActivity));
            return true;
        }
    }

    public void onPause()
    {
        if (j != null)
        {
            try
            {
                unregisterReceiver(j);
            }
            catch (IllegalArgumentException illegalargumentexception)
            {
                Log.d("ABassBoosterActivity", "unregisterReceiver called on unregistered receiver in AbstractBassBoosterActivity. Please ignore.");
            }
        }
        super.onPause();
    }

    public void onProgressChanged(SeekBar seekbar, int i1, boolean flag)
    {
        switch (seekbar.getId())
        {
        default:
            return;

        case 2131492883: 
            p = (short)(i1 * 10);
            if (q)
            {
                n.setText((new StringBuilder()).append(getString(0x7f050027)).append(": ").append(String.valueOf(p / 10)).append("%").toString());
                return;
            } else
            {
                n.setText(getString(0x7f050027));
                return;
            }

        case 2131492889: 
            a(0, c(i1), (TextView)t.get(0));
            return;

        case 2131492892: 
            a(1, c(i1), (TextView)t.get(1));
            return;

        case 2131492895: 
            a(2, c(i1), (TextView)t.get(2));
            return;

        case 2131492898: 
            a(3, c(i1), (TextView)t.get(3));
            return;

        case 2131492901: 
            a(4, c(i1), (TextView)t.get(4));
            return;

        case 2131492904: 
            a(5, c(i1), (TextView)t.get(5));
            return;
        }
    }

    public void onResume()
    {
        if (e.getBoolean("customization_update_needed", false))
        {
            j();
        }
        if (j == null)
        {
            j = new o(this, null);
        }
        registerReceiver(j, k);
        i();
        h();
        for (int i1 = 0; i1 < d; i1++)
        {
            ((VerticalSeekBar)a.get(i1)).setProgressAndThumb(a(e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(i1)).toString(), 0)));
            a(i1, 1000D, (TextView)t.get(i1));
        }

        c.setSelection(e.getInt("equalizer_selected_preset_key", 0));
        v.setChecked(e.getBoolean("equalizer_enabled_key", false));
        n.setChecked(e.getBoolean("bassboost_enabled_key", false));
        o.setProgress(e.getInt("bassboost_strength_key", 800) / 10);
        b();
        super.onResume();
    }

    public void onStartTrackingTouch(SeekBar seekbar)
    {
    }

    public void onStopTrackingTouch(SeekBar seekbar)
    {
        seekbar.getId();
        JVM INSTR lookupswitch 7: default 72
    //                   2131492883: 166
    //                   2131492889: 215
    //                   2131492892: 220
    //                   2131492895: 225
    //                   2131492898: 230
    //                   2131492901: 235
    //                   2131492904: 240;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8
_L1:
        byte byte0 = -1;
_L10:
        if (byte0 > -1)
        {
            BassBoosterApplication.a("UI - SFX", (new StringBuilder()).append("eq_value").append(byte0).toString(), seekbar.getProgress());
            c.setSelection(0);
            f.putInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(byte0)).toString(), b(seekbar.getProgress())).apply();
            h();
        }
        return;
_L2:
        BassBoosterApplication.a("UI - SFX", "bb_value", seekbar.getProgress());
        f.putInt("bassboost_strength_key", o.getProgress() * 10).apply();
        i();
        byte0 = -1;
        continue; /* Loop/switch isn't completed */
_L3:
        byte0 = 0;
        continue; /* Loop/switch isn't completed */
_L4:
        byte0 = 1;
        continue; /* Loop/switch isn't completed */
_L5:
        byte0 = 2;
        continue; /* Loop/switch isn't completed */
_L6:
        byte0 = 3;
        continue; /* Loop/switch isn't completed */
_L7:
        byte0 = 4;
        continue; /* Loop/switch isn't completed */
_L8:
        byte0 = 5;
        if (true) goto _L10; else goto _L9
_L9:
    }
}
