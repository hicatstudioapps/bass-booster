// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.av;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            z, aa, BassBoosterService, BassBoosterApplication, 
//            v

public class SettingsActivity extends Activity
    implements android.view.View.OnClickListener, android.widget.AdapterView.OnItemSelectedListener, android.widget.CompoundButton.OnCheckedChangeListener
{

    private SharedPreferences a;
    private android.content.SharedPreferences.Editor b;
    private CheckBox c;
    private CheckBox d;
    private CheckBox e;
    private CheckBox f;
    private CheckBox g;
    private CheckBox h;
    private CheckBox i;
    private CheckBox j;
    private CheckBox k;
    private CheckBox l;
    private Spinner m;
    private Spinner n;
    private Spinner o;
    private Spinner p;
    private Button q;
    private Resources r;
    private int s;

    public SettingsActivity()
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = null;
        h = null;
        i = null;
        j = null;
        k = null;
        l = null;
        m = null;
        n = null;
        o = null;
        p = null;
        q = null;
        r = null;
        s = 0;
    }

    static android.content.SharedPreferences.Editor a(SettingsActivity settingsactivity)
    {
        return settingsactivity.b;
    }

    private void a()
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void b()
    {
        e.setEnabled(false);
        f.setEnabled(false);
        i.setEnabled(false);
        n.setEnabled(false);
        j.setEnabled(false);
        k.setEnabled(false);
        o.setEnabled(false);
        p.setEnabled(false);
        h.setEnabled(false);
        l.setEnabled(false);
    }

    public void a(int i1)
    {
        View view = View.inflate(this, 0x7f030003, null);
        ((TextView)view.findViewById(0x7f0c0039)).setText(i1);
        ((CheckBox)view.findViewById(0x7f0c003a)).setOnCheckedChangeListener(new z(this, i1));
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle(0x7f05004e);
        builder.setIcon(0x1080027);
        builder.setView(view);
        builder.setPositiveButton(r.getString(0x104000a), new aa(this));
        builder.show();
    }

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
    {
        compoundbutton.getId();
        JVM INSTR tableswitch 2131492905 2131492919: default 80
    //                   2131492905 103
    //                   2131492906 203
    //                   2131492907 278
    //                   2131492908 80
    //                   2131492909 80
    //                   2131492910 378
    //                   2131492911 399
    //                   2131492912 420
    //                   2131492913 80
    //                   2131492914 80
    //                   2131492915 558
    //                   2131492916 537
    //                   2131492917 441
    //                   2131492918 80
    //                   2131492919 489;
           goto _L1 _L2 _L3 _L4 _L1 _L1 _L5 _L6 _L7 _L1 _L1 _L8 _L9 _L10 _L1 _L11
_L1:
        startService(new Intent(getApplicationContext(), com/desaxedstudios/bassbooster/BassBoosterService));
        b();
        return;
_L2:
        if (a.getBoolean("run_in_background", true) != flag)
        {
            StringBuilder stringbuilder = (new StringBuilder()).append("settings_update_");
            if (flag)
            {
                compoundbutton = "enabled";
            } else
            {
                compoundbutton = "disabled";
            }
            BassBoosterApplication.a("UI - Settings", stringbuilder.append(compoundbutton).toString());
        }
        b.putBoolean("run_in_background", flag).apply();
        if (!flag && !a.getBoolean("do_not_show_warning_for_background_process", false))
        {
            a(0x7f05006c);
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (a.getBoolean("show_status_icon", true) != flag)
        {
            StringBuilder stringbuilder1 = (new StringBuilder()).append("settings_statusicon_");
            if (flag)
            {
                compoundbutton = "enabled";
            } else
            {
                compoundbutton = "disabled";
            }
            BassBoosterApplication.a("UI - Settings", stringbuilder1.append(compoundbutton).toString());
        }
        b.putBoolean("show_status_icon", flag).apply();
        continue; /* Loop/switch isn't completed */
_L4:
        if (a.getBoolean("force_bass_boost_everywhere", true) != flag)
        {
            StringBuilder stringbuilder2 = (new StringBuilder()).append("settings_force_");
            if (flag)
            {
                compoundbutton = "enabled";
            } else
            {
                compoundbutton = "disabled";
            }
            BassBoosterApplication.a("UI - Settings", stringbuilder2.append(compoundbutton).toString());
        }
        b.putBoolean("force_bass_boost_everywhere", flag).apply();
        if (flag && !a.getBoolean("do_not_show_warning_for_force_bassboost", false))
        {
            a(0x7f05004b);
        }
        continue; /* Loop/switch isn't completed */
_L5:
        b.putBoolean("auto_start_on_phone_boot", flag).apply();
        continue; /* Loop/switch isn't completed */
_L6:
        b.putBoolean("auto_detect_current_preset", flag).apply();
        continue; /* Loop/switch isn't completed */
_L7:
        b.putBoolean("if_no_preset_could_be_detected_then_switch_back", flag).apply();
        continue; /* Loop/switch isn't completed */
_L10:
        b.putBoolean("change_eq_preset_on_call", flag).apply();
        if (flag && !a.getBoolean("do_not_show_warning_for_on_call", false))
        {
            a(0x7f050066);
        }
        continue; /* Loop/switch isn't completed */
_L11:
        b.putBoolean("change_eq_preset_during_call", flag).apply();
        if (flag && !a.getBoolean("do_not_show_warning_for_on_call", false))
        {
            a(0x7f050066);
        }
        continue; /* Loop/switch isn't completed */
_L9:
        b.putBoolean("use_decimal_precision_in_eq", flag).apply();
        continue; /* Loop/switch isn't completed */
_L8:
        b.putBoolean("show_visualizer", flag).putBoolean("customization_update_needed", true).apply();
        if (true) goto _L1; else goto _L12
_L12:
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131492871: 
            BassBoosterApplication.a("UI - Settings", "settings_buy");
            break;
        }
        switch (0)
        {
        default:
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.desaxedstudios.bassboosterpro")));
            return;

        case 1: // '\001'
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=com.desaxedstudios.bassboosterpro")));
            return;

        case 2: // '\002'
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("samsungapps://ProductDetail/com.desaxedstudios.bassboosterpro")));
            return;

        case 3: // '\003'
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mma://app/769ffb2d-199a-46fd-a592-f9a17c25b86c")));
            break;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030002);
        a();
        a = PreferenceManager.getDefaultSharedPreferences(this);
        b = a.edit();
        r = getResources();
        BassBoosterApplication.a.a("SettingsActivity");
        c = (CheckBox)findViewById(0x7f0c0029);
        d = (CheckBox)findViewById(0x7f0c002a);
        g = (CheckBox)findViewById(0x7f0c002b);
        e = (CheckBox)findViewById(0x7f0c002e);
        f = (CheckBox)findViewById(0x7f0c002f);
        i = (CheckBox)findViewById(0x7f0c0030);
        j = (CheckBox)findViewById(0x7f0c0035);
        k = (CheckBox)findViewById(0x7f0c0037);
        h = (CheckBox)findViewById(0x7f0c0034);
        l = (CheckBox)findViewById(0x7f0c0033);
        n = (Spinner)findViewById(0x7f0c0031);
        o = (Spinner)findViewById(0x7f0c0036);
        p = (Spinner)findViewById(0x7f0c0038);
        m = (Spinner)findViewById(0x7f0c0032);
        q = (Button)findViewById(0x7f0c0007);
        c.setOnCheckedChangeListener(this);
        d.setOnCheckedChangeListener(this);
        g.setOnCheckedChangeListener(this);
        e.setOnCheckedChangeListener(this);
        f.setOnCheckedChangeListener(this);
        h.setOnCheckedChangeListener(this);
        i.setOnCheckedChangeListener(this);
        j.setOnCheckedChangeListener(this);
        k.setOnCheckedChangeListener(this);
        l.setOnCheckedChangeListener(this);
        n.setOnItemSelectedListener(this);
        o.setOnItemSelectedListener(this);
        p.setOnItemSelectedListener(this);
        m.setOnItemSelectedListener(this);
        q.setOnClickListener(this);
        c.setChecked(a.getBoolean("run_in_background", true));
        d.setChecked(a.getBoolean("show_status_icon", true));
        g.setChecked(a.getBoolean("force_bass_boost_everywhere", false));
        e.setChecked(a.getBoolean("auto_start_on_phone_boot", false));
        f.setChecked(a.getBoolean("auto_detect_current_preset", false));
        i.setChecked(a.getBoolean("if_no_preset_could_be_detected_then_switch_back", false));
        j.setChecked(a.getBoolean("change_eq_preset_on_call", false));
        k.setChecked(a.getBoolean("change_eq_preset_during_call", false));
        bundle = h;
        if (!a.getBoolean("use_decimal_precision_in_eq", true));
        bundle.setChecked(false);
        l.setChecked(a.getBoolean("show_visualizer", false));
        bundle = new ArrayAdapter(this, 0x1090008, new ArrayList(Arrays.asList(r.getStringArray(0x7f060000))));
        for (int i1 = 0; i1 < a.getInt("number_of_saved_custom_presets", 0); i1++)
        {
            bundle.add(a.getString((new StringBuilder()).append("equalizer_custom_values_key").append(String.valueOf(i1 + 1)).append("_name").toString(), (new StringBuilder()).append("Custom Preset #").append(i1 + 1).toString()));
        }

        bundle.setDropDownViewResource(0x1090009);
        n.setAdapter(bundle);
        n.setSelection(a.getInt("default_preset_to_switch_back_to", 0));
        o.setAdapter(bundle);
        o.setSelection(a.getInt("preset_to_set_on_call", 21));
        p.setAdapter(bundle);
        p.setSelection(a.getInt("preset_to_set_during_call", 21));
        bundle = new ArrayAdapter(this, 0x1090008, r.getStringArray(0x7f060002));
        bundle.setDropDownViewResource(0x1090009);
        m.setAdapter(bundle);
        m.setSelection(a.getInt("key_app_theme", 0));
        b();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(0x7f0b0001, menu);
        return true;
    }

    public void onItemSelected(AdapterView adapterview, View view, int i1, long l1)
    {
        adapterview.getId();
        JVM INSTR tableswitch 2131492913 2131492920: default 52
    //                   2131492913 53
    //                   2131492914 110
    //                   2131492915 52
    //                   2131492916 52
    //                   2131492917 52
    //                   2131492918 72
    //                   2131492919 52
    //                   2131492920 91;
           goto _L1 _L2 _L3 _L1 _L1 _L1 _L4 _L1 _L5
_L1:
        return;
_L2:
        b.putInt("default_preset_to_switch_back_to", i1).apply();
        return;
_L4:
        b.putInt("preset_to_set_on_call", i1).apply();
        return;
_L5:
        b.putInt("preset_to_set_during_call", i1).apply();
        return;
_L3:
        b.putBoolean("customization_update_needed", true).putInt("key_app_theme", i1).apply();
        adapterview = r.getStringArray(0x7f060002);
        s = s + 1;
        if (s >= 2 && i1 < adapterview.length)
        {
            BassBoosterApplication.a("UI - Settings", "settings_theme", adapterview[i1]);
            return;
        }
        if (true) goto _L1; else goto _L6
_L6:
    }

    public void onNothingSelected(AdapterView adapterview)
    {
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 2131492928: 
            BassBoosterApplication.a("UI - Settings", "settings_reset");
            c.setChecked(true);
            d.setChecked(true);
            g.setChecked(false);
            e.setChecked(false);
            f.setChecked(false);
            i.setChecked(false);
            n.setSelection(0);
            j.setChecked(false);
            o.setSelection(21);
            k.setChecked(false);
            p.setSelection(21);
            h.setChecked(false);
            l.setChecked(false);
            m.setSelection(0);
            b.putBoolean("customization_update_needed", true).apply();
            b.apply();
            return true;

        case 16908332: 
            av.a(this);
            return true;
        }
    }
}
