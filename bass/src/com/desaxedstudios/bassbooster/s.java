// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.gms.ads.InterstitialAd;

public class s
{

    static InterstitialAd a;
    private Context b;
    private SharedPreferences c;
    private android.content.SharedPreferences.Editor d;
    private boolean e;

    s(Context context)
    {
        c = null;
        d = null;
        e = true;
        b = context;
        c = PreferenceManager.getDefaultSharedPreferences(b);
        d = c.edit();
    }

    public void a()
    {
label0:
        {
            if (a == null)
            {
                a = new InterstitialAd(b);
                a.setAdUnitId(b.getString(0x7f05009d));
            }
            if (!a.isLoaded())
            {
                long l = System.currentTimeMillis();
                long l1 = c.getLong("KEY_AD_LAST_SHOWN", 0L);
                long l2 = c.getLong("KEY_AD_SECOND_LAST_SHOWN", 0L);
                if (e && (l - l1 <= 0x3519c0L || l - l2 <= 0x5248740L))
                {
                    break label0;
                }
                com.google.android.gms.ads.AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).build();
                a.loadAd(adrequest);
            }
            return;
        }
        Log.d("AdManager", "Interstitial shown too recently, not loading.");
    }

    public void b()
    {
        if (a.isLoaded())
        {
            long l = System.currentTimeMillis();
            long l1 = c.getLong("KEY_AD_LAST_SHOWN", 0L);
            long l2 = c.getLong("KEY_AD_SECOND_LAST_SHOWN", 0L);
            if (!e || l - l1 > 0x36ee80L && l - l2 > 0x5265c00L)
            {
                a.show();
                d.putLong("KEY_AD_LAST_SHOWN", l);
                d.putLong("KEY_AD_SECOND_LAST_SHOWN", l1);
                d.apply();
                return;
            } else
            {
                Log.d("AdManager", "Interstitial shown too recently, not showing.");
                return;
            }
        } else
        {
            Log.d("AdManager", "Interstitial not loaded yet.");
            return;
        }
    }
}
