// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            BassBoosterApplication, a, b, c, 
//            d, e, v

public class AboutActivity extends Activity
    implements android.view.View.OnClickListener
{

    private LinearLayout a;
    private Button b;
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;
    private ImageButton h;
    private Button i;
    private Button j;

    public AboutActivity()
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = null;
        h = null;
        i = null;
        j = null;
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131492868: 
        case 2131492872: 
        case 2131492873: 
        default:
            return;

        case 2131492876: 
            BassBoosterApplication.a("UI - About", "about_support");
            view = new Intent("android.intent.action.SEND");
            view.setType("text/plain");
            view.putExtra("android.intent.extra.EMAIL", new String[] {
                "team@desaxed.com"
            });
            view.putExtra("android.intent.extra.SUBJECT", "Bass Booster");
            startActivity(view);
            return;

        case 2131492877: 
            BassBoosterApplication.a("UI - About", "about_www");
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.desaxed.com")));
            return;

        case 2131492875: 
            BassBoosterApplication.a("UI - About", "about_fb");
            view = new android.app.AlertDialog.Builder(this);
            view.setTitle(" ");
            view.setIcon(0x7f02003b);
            view.setPositiveButton(0x1040000, new a(this));
            view.setItems(0x7f060003, new b(this));
            view.show();
            return;

        case 2131492867: 
            finish();
            return;

        case 2131492869: 
            BassBoosterApplication.a("UI - About", "about_rate");
            switch (0)
            {
            default:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.desaxedstudios.bassbooster")));
                return;

            case 1: // '\001'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=com.desaxedstudios.bassbooster")));
                return;

            case 2: // '\002'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("samsungapps://ProductDetail/com.desaxedstudios.bassbooster")));
                return;

            case 3: // '\003'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mma://app/769ffb2d-199a-46fd-a592-f9a17c25b86c")));
                break;
            }
            return;

        case 2131492871: 
            BassBoosterApplication.a("UI - About", "about_buy");
            switch (0)
            {
            default:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.desaxedstudios.bassboosterpro")));
                return;

            case 1: // '\001'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=com.desaxedstudios.bassboosterpro")));
                return;

            case 2: // '\002'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("samsungapps://ProductDetail/com.desaxedstudios.bassboosterpro")));
                return;

            case 3: // '\003'
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mma://app/769ffb2d-199a-46fd-a592-f9a17c25b86c")));
                break;
            }
            return;

        case 2131492870: 
            BassBoosterApplication.a("UI - About", "about_log");
            view = new android.app.AlertDialog.Builder(this);
            view.setTitle(0x7f050088);
            view.setMessage(0x7f050030);
            view.setIcon(0x108009b);
            view.setPositiveButton(getResources().getString(0x104000a), new c(this));
            view.show();
            return;

        case 2131492878: 
            BassBoosterApplication.a("UI - About", "about_credits");
            view = View.inflate(this, 0x7f030004, null);
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle(0x7f050037);
            builder.setView(view);
            builder.setIcon(0x108009b);
            builder.setPositiveButton(getResources().getString(0x104000a), new d(this));
            builder.show();
            return;

        case 2131492874: 
            BassBoosterApplication.a("UI - About", "about_faq");
            view = View.inflate(this, 0x7f030005, null);
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(this);
            builder1.setTitle(0x7f050047);
            builder1.setView(view);
            builder1.setIcon(0x108009b);
            builder1.setPositiveButton(getResources().getString(0x104000a), new e(this));
            builder1.show();
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030000);
        BassBoosterApplication.a.a("AboutActivity");
        b = (Button)findViewById(0x7f0c000c);
        c = (Button)findViewById(0x7f0c000d);
        d = (Button)findViewById(0x7f0c0003);
        e = (Button)findViewById(0x7f0c0005);
        f = (Button)findViewById(0x7f0c0006);
        h = (ImageButton)findViewById(0x7f0c000b);
        i = (Button)findViewById(0x7f0c000a);
        j = (Button)findViewById(0x7f0c000e);
        g = (Button)findViewById(0x7f0c0007);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        e.setOnClickListener(this);
        f.setOnClickListener(this);
        h.setOnClickListener(this);
        d.setOnClickListener(this);
        i.setOnClickListener(this);
        j.setOnClickListener(this);
        g.setOnClickListener(this);
    }
}
