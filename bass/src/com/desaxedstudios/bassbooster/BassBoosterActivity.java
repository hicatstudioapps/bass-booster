// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.VerticalSeekBar;
import com.google.android.gms.ads.AdView;
import java.util.ArrayList;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            f, t, s, BassBoosterApplication, 
//            v, InterstitialSettingsActivity, u

public class BassBoosterActivity extends f
{

    Handler n;
    private s o;
    private long p;

    public BassBoosterActivity()
    {
        n = new Handler();
        p = 0L;
    }

    static s a(BassBoosterActivity bassboosteractivity)
    {
        return bassboosteractivity.o;
    }

    static void a(BassBoosterActivity bassboosteractivity, int i)
    {
        bassboosteractivity.d(i);
    }

    private void d(int i)
    {
        boolean flag = false;
        android.widget.LinearLayout.LayoutParams layoutparams;
        int j;
        if (e.getInt("key_app_theme", 0) == 1)
        {
            j = 1;
        } else
        {
            j = 0;
        }
        if (j != 0)
        {
            j = -2;
        } else
        {
            j = (int)g.getDimension(0x7f070004);
        }
        layoutparams = new android.widget.LinearLayout.LayoutParams(j, -1);
        layoutparams.bottomMargin = i;
        layoutparams.gravity = 1;
        for (i = ((flag) ? 1 : 0); i < 6; i++)
        {
            ((VerticalSeekBar)a.get(i)).setLayoutParams(layoutparams);
        }

    }

    protected void b()
    {
    }

    protected void c()
    {
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = (AdView)findViewById(0x7f0c0010);
        bundle.loadAd((new com.google.android.gms.ads.AdRequest.Builder()).build());
        bundle.setAdListener(new t(this, bundle));
        o = new s(this);
        BassBoosterApplication.a.a("BassBoosterActivity");
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 2131492923: 
            startActivity(new Intent(this, com/desaxedstudios/bassbooster/InterstitialSettingsActivity));
            return true;

        case 2131492925: 
            if (System.currentTimeMillis() - p > 20000L)
            {
                o.b();
                BassBoosterApplication.a("Ads", "show_ad_on_reset");
            }
            return super.onOptionsItemSelected(menuitem);

        case 2131492924: 
            break;
        }
        if (System.currentTimeMillis() - p > 20000L)
        {
            o.b();
            BassBoosterApplication.a("Ads", "show_ad_on_save");
        }
        return super.onOptionsItemSelected(menuitem);
    }

    public void onPause()
    {
        super.onPause();
        n.removeCallbacksAndMessages(null);
    }

    public void onResume()
    {
        super.onResume();
        p = System.currentTimeMillis();
        n.postDelayed(new u(this), 15000L);
    }
}
