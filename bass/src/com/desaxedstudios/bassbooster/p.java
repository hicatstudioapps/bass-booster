// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.media.audiofx.Virtualizer;
import android.preference.PreferenceManager;
import android.support.v4.app.bi;
import android.util.Log;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            q, BassBoosterActivity

public abstract class p extends Service
{

    protected SharedPreferences a;
    private PendingIntent b;
    private PendingIntent c;
    private NotificationManager d;
    private bi e;
    private AlarmManager f;
    private AudioManager g;
    private Calendar h;
    private Equalizer i;
    private BassBoost j;
    private Virtualizer k;
    private PresetReverb l;
    private BroadcastReceiver m;

    public p()
    {
        b = null;
        c = null;
        d = null;
        f = null;
        g = null;
        h = null;
        a = null;
        i = null;
        j = null;
        k = null;
        l = null;
        m = new q(this);
    }

    private void d()
    {
        h.setTimeInMillis(System.currentTimeMillis());
        h.add(13, 10);
        f.set(0, h.getTimeInMillis(), b);
    }

    private CharSequence e()
    {
        boolean flag = a.getBoolean("equalizer_enabled_key", false);
        boolean flag1 = a.getBoolean("bassboost_enabled_key", false);
        Object obj;
        if (flag)
        {
            int k1 = a.getInt("equalizer_selected_preset_key", 0);
            Object obj1;
            if (k1 <= 0)
            {
                obj = (new StringBuilder()).append("").append(getString(0x7f050041)).toString();
            } else
            {
                obj = getResources().getStringArray(0x7f060000);
                ArrayList arraylist = new ArrayList();
                for (int i1 = 0; i1 < obj.length; i1++)
                {
                    arraylist.add(obj[i1]);
                }

                for (int j1 = 0; j1 < a.getInt("number_of_saved_custom_presets", 0); j1++)
                {
                    arraylist.add(a.getString((new StringBuilder()).append("equalizer_custom_values_key").append(String.valueOf(j1 + 1)).append("_name").toString(), (new StringBuilder()).append("Custom Preset #").append(j1 + 1).toString()));
                }

                obj = (new StringBuilder()).append("").append(getString(0x7f050045)).append((String)arraylist.get(k1)).toString();
            }
            if (flag1)
            {
                obj = (new StringBuilder()).append(((String) (obj))).append("  -  ").toString();
            }
        } else
        {
            obj = "";
        }
        obj1 = obj;
        if (flag1)
        {
            obj1 = (new StringBuilder()).append(((String) (obj))).append(getString(0x7f050026)).append(a.getInt("bassboost_strength_key", 800) / 10).append("%").toString();
        }
        return ((CharSequence) (obj1));
    }

    public void a()
    {
        short word0;
        boolean flag;
        flag = false;
        word0 = 0;
        if (!a.getBoolean("equalizer_enabled_key", false)) goto _L2; else goto _L1
_L1:
        i = new Equalizer(0, 0);
_L3:
        if (word0 >= i.getNumberOfBands())
        {
            break MISSING_BLOCK_LABEL_92;
        }
        i.setBandLevel(word0, (short)a.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(word0)).toString(), 0));
        word0++;
          goto _L3
        i.setEnabled(true);
_L5:
        return;
        Object obj;
        obj;
        i = null;
        Log.e("BassBoosterService", "Equalizer not supported in service : IllegalArgumentException.");
        return;
        obj;
        i = null;
        Log.e("BassBoosterService", "Equalizer not supported in service : UnsupportedOperationException.");
        return;
        obj;
        i = null;
        Log.e("BassBoosterService", "Equalizer not supported in service : IllegalStateException (RuntimeException).");
        return;
        obj;
        i = null;
        Log.e("BassBoosterService", "Equalizer not supported in service : Exception.");
        return;
_L2:
        if (i == null) goto _L5; else goto _L4
_L4:
        word0 = flag;
_L7:
        if (word0 >= i.getNumberOfBands())
        {
            break; /* Loop/switch isn't completed */
        }
        i.setBandLevel(word0, (short)0);
        word0++;
        if (true) goto _L7; else goto _L6
_L6:
        try
        {
            i.setEnabled(false);
            return;
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            i = null;
            Log.e("BassBoosterService", "Equalizer couldn't be disabled in service : IllegalArgumentException.");
            return;
        }
        catch (UnsupportedOperationException unsupportedoperationexception)
        {
            i = null;
            Log.e("BassBoosterService", "Equalizer couldn't be disabled in service : UnsupportedOperationException.");
            return;
        }
        catch (RuntimeException runtimeexception)
        {
            i = null;
            Log.e("BassBoosterService", "Equalizer couldn't be disabled in service : IllegalStateException (RuntimeException).");
            return;
        }
        catch (Exception exception)
        {
            i = null;
        }
        Log.e("BassBoosterService", "Equalizer couldn't be disabled in service : Exception.");
        return;
    }

    public void a(int i1)
    {
        boolean flag1 = true;
        if (a.getBoolean("force_bass_boost_everywhere", true)) goto _L2; else goto _L1
_L1:
        boolean flag = flag1;
        i1;
        JVM INSTR tableswitch 0 3: default 52
    //                   0 118
    //                   1 54
    //                   2 52
    //                   3 123;
           goto _L2 _L3 _L4 _L2 _L5
_L2:
        flag = false;
_L4:
        if (!a.getBoolean("bassboost_enabled_key", false) || flag) goto _L7; else goto _L6
_L6:
        j = new BassBoost(0, 0);
        j.setStrength((short)a.getInt("bassboost_strength_key", 800));
        j.setEnabled(true);
_L10:
        return;
_L3:
        flag = false;
          goto _L4
_L5:
        if (g.isWiredHeadsetOn() || g.isBluetoothA2dpOn() || g.isBluetoothScoOn()) goto _L2; else goto _L8
_L8:
        flag = flag1;
          goto _L4
        Object obj;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
        return;
_L7:
        if (j == null) goto _L10; else goto _L9
_L9:
        try
        {
            j.setStrength((short)0);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            j = null;
            Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            j = null;
            Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            j = null;
            Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            j = null;
            Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
        }
        if (j == null) goto _L10; else goto _L11
_L11:
        j.setEnabled(false);
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
        return;
        obj;
        j = null;
        Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
        return;
          goto _L4
    }

    public abstract void b();

    public void c()
    {
        boolean flag = a.getBoolean("equalizer_enabled_key", false);
        boolean flag1 = a.getBoolean("bassboost_enabled_key", false);
        if (a.getBoolean("show_status_icon", true))
        {
            if (!flag1 && !flag)
            {
                d.cancelAll();
                return;
            } else
            {
                d.notify(100, e.b(e()).a());
                return;
            }
        } else
        {
            d.cancelAll();
            return;
        }
    }

    public void onCreate()
    {
        c = PendingIntent.getActivity(this, 0, new Intent(this, com/desaxedstudios/bassbooster/BassBoosterActivity), 0);
        a = PreferenceManager.getDefaultSharedPreferences(this);
        d = (NotificationManager)getSystemService("notification");
        f = (AlarmManager)getSystemService("alarm");
        h = Calendar.getInstance();
        g = (AudioManager)getSystemService("audio");
        Object obj = new Intent(getApplicationContext(), com/desaxedstudios/bassbooster/p);
        ((Intent) (obj)).setAction("repeat");
        ((Intent) (obj)).putExtra("no_notif", true);
        b = PendingIntent.getService(getApplicationContext(), 0, ((Intent) (obj)), 0);
        e = new bi(this);
        e.a(c).a(BitmapFactory.decodeResource(getResources(), 0x7f02003c)).a(0x7f02003d).a(getString(0x7f05008a)).c(null).a(0L).a(true).c(false).b(true).a(c);
        obj = new IntentFilter();
        ((IntentFilter) (obj)).addAction("android.media.AUDIO_BECOMING_NOISY");
        ((IntentFilter) (obj)).addAction("android.intent.action.HEADSET_PLUG");
        registerReceiver(m, ((IntentFilter) (obj)));
    }

    public void onDestroy()
    {
        a(3);
        a();
        try
        {
            unregisterReceiver(m);
            return;
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            Log.d("BassBoosterService", "unregisterReceiver called more than once or on uninitialized receiver in BassBoosterService. Please ignore.");
        }
    }

    public int onStartCommand(Intent intent, int i1, int j1)
    {
        boolean flag = a.getBoolean("equalizer_enabled_key", false);
        boolean flag1 = a.getBoolean("bassboost_enabled_key", false);
        a(3);
        a();
        if (intent != null)
        {
            if (intent.hasExtra("appwidget"))
            {
                b();
            }
            if (!intent.hasExtra("no_notif"))
            {
                c();
            }
        }
        if (!flag1 && !flag)
        {
            f.cancel(b);
            return 2;
        }
        if (a.getBoolean("run_in_background", true))
        {
            d();
            return 2;
        } else
        {
            f.cancel(b);
            return 2;
        }
    }
}
