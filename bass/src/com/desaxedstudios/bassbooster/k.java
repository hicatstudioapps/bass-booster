// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.desaxedstudios.bassbooster;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VerticalSeekBar;
import java.util.ArrayList;

// Referenced classes of package com.desaxedstudios.bassbooster:
//            w, f, l

class k
    implements ServiceConnection
{

    final f a;

    k(f f1)
    {
        a = f1;
        super();
    }

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        a.i = ((w)ibinder).a();
        if (f.a(a))
        {
            f.c(a).setText((new StringBuilder()).append(a.getString(0x7f050027)).append(": ").append(String.valueOf(f.b(a) / 10)).append("%").toString());
        } else
        {
            f.c(a).setText(a.getString(0x7f050027));
        }
        f.d(a).setProgress(f.b(a) / 10);
        f.c(a).setChecked(f.a(a));
        a.i();
        a.b();
        f.f(a).setChecked(f.e(a));
        for (int i = 0; i < a.d; i++)
        {
            a.b.add(Integer.valueOf(a.e.getInt((new StringBuilder()).append("equalizer_custom_values_key").append(String.valueOf(i)).toString(), 0)));
        }

        if (a.d < 6)
        {
            for (int j = 6; j > a.d; j--)
            {
                ((LinearLayout)f.g(a).get(j - 1)).setVisibility(8);
            }

            if (!a.e.getBoolean("equalizer_few_bands_warning_was already_shown", false))
            {
                Toast.makeText(a, (new StringBuilder()).append(a.getString(0x7f050061)).append(a.d).append(a.getString(0x7f050062)).toString(), 1).show();
                a.f.putBoolean("equalizer_few_bands_warning_was already_shown", true).apply();
            }
        }
        if (a.d > 6)
        {
            componentname = new android.app.AlertDialog.Builder(a);
            componentname.setTitle(a.getString(0x7f050052));
            componentname.setMessage((new StringBuilder()).append(a.getString(0x7f050053)).append(a.d).append(a.getString(0x7f050054)).toString());
            componentname.setIcon(0x108009b);
            componentname.setPositiveButton(a.getString(0x104000a), new l(this));
            componentname.show();
        }
        for (int i1 = 0; i1 < a.d; i1++)
        {
            ((VerticalSeekBar)a.a.get(i1)).setProgressAndThumb(a.a(a.e.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(i1)).toString(), 0)));
            f.a(a, i1, 1000D, (TextView)f.h(a).get(i1));
        }

        a.d();
        a.h();
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        a.i = null;
    }
}
