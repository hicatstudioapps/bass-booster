// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.ads.afma.nano;

import com.google.android.gms.b.yz;
import com.google.android.gms.b.za;
import com.google.android.gms.b.zg;
import com.google.android.gms.b.zi;
import com.google.android.gms.b.zl;

public final class clear extends zi
{

    private static volatile encryptedDidSignal a[];
    public byte encryptedDidSignal[];
    public byte encryptedSpamSignals[];

    public static clear[] emptyArray()
    {
        if (a == null)
        {
            synchronized (zg.a)
            {
                if (a == null)
                {
                    a = new a[0];
                }
            }
        }
        return a;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static a parseFrom(yz yz1)
    {
        return (new <init>()).mergeFrom(yz1);
    }

    public static mergeFrom parseFrom(byte abyte0[])
    {
        return (mergeFrom)zi.mergeFrom(new <init>(), abyte0);
    }

    protected int a()
    {
        int j = super.a();
        int i = j;
        if (encryptedSpamSignals != null)
        {
            i = j + za.b(1, encryptedSpamSignals);
        }
        j = i;
        if (encryptedDidSignal != null)
        {
            j = i + za.b(2, encryptedDidSignal);
        }
        return j;
    }

    public encryptedDidSignal clear()
    {
        encryptedSpamSignals = null;
        encryptedDidSignal = null;
        b = -1;
        return this;
    }

    public b mergeFrom(yz yz1)
    {
        do
        {
            int i = yz1.a();
            switch (i)
            {
            default:
                if (zl.a(yz1, i))
                {
                    continue;
                }
                // fall through

            case 0: // '\0'
                return this;

            case 10: // '\n'
                encryptedSpamSignals = yz1.g();
                break;

            case 18: // '\022'
                encryptedDidSignal = yz1.g();
                break;
            }
        } while (true);
    }

    public volatile zi mergeFrom(yz yz1)
    {
        return mergeFrom(yz1);
    }

    public void writeTo(za za1)
    {
        if (encryptedSpamSignals != null)
        {
            za1.a(1, encryptedSpamSignals);
        }
        if (encryptedDidSignal != null)
        {
            za1.a(2, encryptedDidSignal);
        }
        super.writeTo(za1);
    }

    public A()
    {
        clear();
    }
}
