// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.ads.afma.nano;

import com.google.android.gms.b.yz;
import com.google.android.gms.b.za;
import com.google.android.gms.b.zg;
import com.google.android.gms.b.zi;
import com.google.android.gms.b.zl;

public final class clear extends zi
{

    public static final int DEVICE_IDENTIFIER_ADVERTISER_ID = 3;
    public static final int DEVICE_IDENTIFIER_ADVERTISER_ID_UNHASHED = 4;
    public static final int DEVICE_IDENTIFIER_ANDROID_AD_ID = 5;
    public static final int DEVICE_IDENTIFIER_APP_SPECIFIC_ID = 1;
    public static final int DEVICE_IDENTIFIER_GFIBER_ADVERTISING_ID = 6;
    public static final int DEVICE_IDENTIFIER_GLOBAL_ID = 2;
    public static final int DEVICE_IDENTIFIER_NO_ID = 0;
    private static volatile vcdSignal a[];
    public Long actSignal;
    public Long acxSignal;
    public Long acySignal;
    public Long aczSignal;
    public String afmaVersion;
    public Long attSignal;
    public Long atvSignal;
    public Long btlSignal;
    public Long btsSignal;
    public String cerSignal;
    public Boolean didOptOut;
    public String didSignal;
    public String didSignalAndroidAdId;
    public Integer didSignalType;
    public Long evtTime;
    public String intSignal;
    public Long jbkSignal;
    public Long netSignal;
    public Long ornSignal;
    public String osVersion;
    public Long psnSignal;
    public Long reqType;
    public String stkSignal;
    public Long swzSignal;
    public Long tctSignal;
    public Long tcxSignal;
    public Long tcySignal;
    public Long uhSignal;
    public Long uptSignal;
    public Long usgSignal;
    public Long utzSignal;
    public Long uwSignal;
    public Long vcdSignal;
    public Long visSignal;
    public String vnmSignal;

    public static clear[] emptyArray()
    {
        if (a == null)
        {
            synchronized (zg.a)
            {
                if (a == null)
                {
                    a = new a[0];
                }
            }
        }
        return a;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static a parseFrom(yz yz1)
    {
        return (new <init>()).mergeFrom(yz1);
    }

    public static mergeFrom parseFrom(byte abyte0[])
    {
        return (mergeFrom)zi.mergeFrom(new <init>(), abyte0);
    }

    protected int a()
    {
        int j = super.a();
        int i = j;
        if (osVersion != null)
        {
            i = j + za.b(1, osVersion);
        }
        j = i;
        if (afmaVersion != null)
        {
            j = i + za.b(2, afmaVersion);
        }
        i = j;
        if (atvSignal != null)
        {
            i = j + za.c(3, atvSignal.longValue());
        }
        j = i;
        if (attSignal != null)
        {
            j = i + za.c(4, attSignal.longValue());
        }
        i = j;
        if (btsSignal != null)
        {
            i = j + za.c(5, btsSignal.longValue());
        }
        j = i;
        if (btlSignal != null)
        {
            j = i + za.c(6, btlSignal.longValue());
        }
        i = j;
        if (acxSignal != null)
        {
            i = j + za.c(7, acxSignal.longValue());
        }
        j = i;
        if (acySignal != null)
        {
            j = i + za.c(8, acySignal.longValue());
        }
        i = j;
        if (aczSignal != null)
        {
            i = j + za.c(9, aczSignal.longValue());
        }
        j = i;
        if (actSignal != null)
        {
            j = i + za.c(10, actSignal.longValue());
        }
        i = j;
        if (netSignal != null)
        {
            i = j + za.c(11, netSignal.longValue());
        }
        j = i;
        if (ornSignal != null)
        {
            j = i + za.c(12, ornSignal.longValue());
        }
        i = j;
        if (stkSignal != null)
        {
            i = j + za.b(13, stkSignal);
        }
        j = i;
        if (tcxSignal != null)
        {
            j = i + za.c(14, tcxSignal.longValue());
        }
        i = j;
        if (tcySignal != null)
        {
            i = j + za.c(15, tcySignal.longValue());
        }
        j = i;
        if (tctSignal != null)
        {
            j = i + za.c(16, tctSignal.longValue());
        }
        i = j;
        if (uptSignal != null)
        {
            i = j + za.c(17, uptSignal.longValue());
        }
        j = i;
        if (visSignal != null)
        {
            j = i + za.c(18, visSignal.longValue());
        }
        i = j;
        if (swzSignal != null)
        {
            i = j + za.c(19, swzSignal.longValue());
        }
        j = i;
        if (psnSignal != null)
        {
            j = i + za.c(20, psnSignal.longValue());
        }
        i = j;
        if (reqType != null)
        {
            i = j + za.c(21, reqType.longValue());
        }
        j = i;
        if (jbkSignal != null)
        {
            j = i + za.c(22, jbkSignal.longValue());
        }
        i = j;
        if (usgSignal != null)
        {
            i = j + za.c(23, usgSignal.longValue());
        }
        j = i;
        if (didSignal != null)
        {
            j = i + za.b(24, didSignal);
        }
        i = j;
        if (evtTime != null)
        {
            i = j + za.c(25, evtTime.longValue());
        }
        j = i;
        if (didSignalType != null)
        {
            j = i + za.b(26, didSignalType.intValue());
        }
        i = j;
        if (intSignal != null)
        {
            i = j + za.b(27, intSignal);
        }
        j = i;
        if (didOptOut != null)
        {
            j = i + za.b(28, didOptOut.booleanValue());
        }
        i = j;
        if (cerSignal != null)
        {
            i = j + za.b(29, cerSignal);
        }
        j = i;
        if (didSignalAndroidAdId != null)
        {
            j = i + za.b(30, didSignalAndroidAdId);
        }
        i = j;
        if (uwSignal != null)
        {
            i = j + za.c(31, uwSignal.longValue());
        }
        j = i;
        if (uhSignal != null)
        {
            j = i + za.c(32, uhSignal.longValue());
        }
        i = j;
        if (utzSignal != null)
        {
            i = j + za.c(33, utzSignal.longValue());
        }
        j = i;
        if (vnmSignal != null)
        {
            j = i + za.b(34, vnmSignal);
        }
        i = j;
        if (vcdSignal != null)
        {
            i = j + za.c(35, vcdSignal.longValue());
        }
        return i;
    }

    public vcdSignal clear()
    {
        osVersion = null;
        afmaVersion = null;
        atvSignal = null;
        attSignal = null;
        btsSignal = null;
        btlSignal = null;
        acxSignal = null;
        acySignal = null;
        aczSignal = null;
        actSignal = null;
        netSignal = null;
        ornSignal = null;
        stkSignal = null;
        tcxSignal = null;
        tcySignal = null;
        tctSignal = null;
        uptSignal = null;
        visSignal = null;
        swzSignal = null;
        psnSignal = null;
        jbkSignal = null;
        usgSignal = null;
        intSignal = null;
        cerSignal = null;
        uwSignal = null;
        uhSignal = null;
        utzSignal = null;
        vnmSignal = null;
        vcdSignal = null;
        reqType = null;
        didSignal = null;
        didSignalType = null;
        didOptOut = null;
        didSignalAndroidAdId = null;
        evtTime = null;
        b = -1;
        return this;
    }

    public b mergeFrom(yz yz1)
    {
_L38:
        int i = yz1.a();
        i;
        JVM INSTR lookupswitch 36: default 304
    //                   0: 312
    //                   10: 314
    //                   18: 325
    //                   24: 336
    //                   32: 350
    //                   40: 364
    //                   48: 378
    //                   56: 392
    //                   64: 406
    //                   72: 420
    //                   80: 434
    //                   88: 448
    //                   96: 462
    //                   106: 476
    //                   112: 487
    //                   120: 501
    //                   128: 515
    //                   136: 529
    //                   144: 543
    //                   152: 557
    //                   160: 571
    //                   168: 585
    //                   176: 599
    //                   184: 613
    //                   194: 627
    //                   200: 638
    //                   208: 652
    //                   218: 714
    //                   224: 725
    //                   234: 739
    //                   242: 750
    //                   248: 761
    //                   256: 775
    //                   264: 789
    //                   274: 803
    //                   280: 814;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16 _L17 _L18 _L19 _L20 _L21 _L22 _L23 _L24 _L25 _L26 _L27 _L28 _L29 _L30 _L31 _L32 _L33 _L34 _L35 _L36 _L37
_L1:
        if (zl.a(yz1, i)) goto _L38; else goto _L2
_L2:
        return this;
_L3:
        osVersion = yz1.f();
          goto _L38
_L4:
        afmaVersion = yz1.f();
          goto _L38
_L5:
        atvSignal = Long.valueOf(yz1.c());
          goto _L38
_L6:
        attSignal = Long.valueOf(yz1.c());
          goto _L38
_L7:
        btsSignal = Long.valueOf(yz1.c());
          goto _L38
_L8:
        btlSignal = Long.valueOf(yz1.c());
          goto _L38
_L9:
        acxSignal = Long.valueOf(yz1.c());
          goto _L38
_L10:
        acySignal = Long.valueOf(yz1.c());
          goto _L38
_L11:
        aczSignal = Long.valueOf(yz1.c());
          goto _L38
_L12:
        actSignal = Long.valueOf(yz1.c());
          goto _L38
_L13:
        netSignal = Long.valueOf(yz1.c());
          goto _L38
_L14:
        ornSignal = Long.valueOf(yz1.c());
          goto _L38
_L15:
        stkSignal = yz1.f();
          goto _L38
_L16:
        tcxSignal = Long.valueOf(yz1.c());
          goto _L38
_L17:
        tcySignal = Long.valueOf(yz1.c());
          goto _L38
_L18:
        tctSignal = Long.valueOf(yz1.c());
          goto _L38
_L19:
        uptSignal = Long.valueOf(yz1.c());
          goto _L38
_L20:
        visSignal = Long.valueOf(yz1.c());
          goto _L38
_L21:
        swzSignal = Long.valueOf(yz1.c());
          goto _L38
_L22:
        psnSignal = Long.valueOf(yz1.c());
          goto _L38
_L23:
        reqType = Long.valueOf(yz1.c());
          goto _L38
_L24:
        jbkSignal = Long.valueOf(yz1.c());
          goto _L38
_L25:
        usgSignal = Long.valueOf(yz1.c());
          goto _L38
_L26:
        didSignal = yz1.f();
          goto _L38
_L27:
        evtTime = Long.valueOf(yz1.c());
          goto _L38
_L28:
        int j = yz1.d();
        switch (j)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
            didSignalType = Integer.valueOf(j);
            break;
        }
        continue; /* Loop/switch isn't completed */
_L29:
        intSignal = yz1.f();
        continue; /* Loop/switch isn't completed */
_L30:
        didOptOut = Boolean.valueOf(yz1.e());
        continue; /* Loop/switch isn't completed */
_L31:
        cerSignal = yz1.f();
        continue; /* Loop/switch isn't completed */
_L32:
        didSignalAndroidAdId = yz1.f();
        continue; /* Loop/switch isn't completed */
_L33:
        uwSignal = Long.valueOf(yz1.c());
        continue; /* Loop/switch isn't completed */
_L34:
        uhSignal = Long.valueOf(yz1.c());
        continue; /* Loop/switch isn't completed */
_L35:
        utzSignal = Long.valueOf(yz1.c());
        continue; /* Loop/switch isn't completed */
_L36:
        vnmSignal = yz1.f();
        continue; /* Loop/switch isn't completed */
_L37:
        vcdSignal = Long.valueOf(yz1.c());
        if (true) goto _L38; else goto _L39
_L39:
    }

    public volatile zi mergeFrom(yz yz1)
    {
        return mergeFrom(yz1);
    }

    public void writeTo(za za1)
    {
        if (osVersion != null)
        {
            za1.a(1, osVersion);
        }
        if (afmaVersion != null)
        {
            za1.a(2, afmaVersion);
        }
        if (atvSignal != null)
        {
            za1.a(3, atvSignal.longValue());
        }
        if (attSignal != null)
        {
            za1.a(4, attSignal.longValue());
        }
        if (btsSignal != null)
        {
            za1.a(5, btsSignal.longValue());
        }
        if (btlSignal != null)
        {
            za1.a(6, btlSignal.longValue());
        }
        if (acxSignal != null)
        {
            za1.a(7, acxSignal.longValue());
        }
        if (acySignal != null)
        {
            za1.a(8, acySignal.longValue());
        }
        if (aczSignal != null)
        {
            za1.a(9, aczSignal.longValue());
        }
        if (actSignal != null)
        {
            za1.a(10, actSignal.longValue());
        }
        if (netSignal != null)
        {
            za1.a(11, netSignal.longValue());
        }
        if (ornSignal != null)
        {
            za1.a(12, ornSignal.longValue());
        }
        if (stkSignal != null)
        {
            za1.a(13, stkSignal);
        }
        if (tcxSignal != null)
        {
            za1.a(14, tcxSignal.longValue());
        }
        if (tcySignal != null)
        {
            za1.a(15, tcySignal.longValue());
        }
        if (tctSignal != null)
        {
            za1.a(16, tctSignal.longValue());
        }
        if (uptSignal != null)
        {
            za1.a(17, uptSignal.longValue());
        }
        if (visSignal != null)
        {
            za1.a(18, visSignal.longValue());
        }
        if (swzSignal != null)
        {
            za1.a(19, swzSignal.longValue());
        }
        if (psnSignal != null)
        {
            za1.a(20, psnSignal.longValue());
        }
        if (reqType != null)
        {
            za1.a(21, reqType.longValue());
        }
        if (jbkSignal != null)
        {
            za1.a(22, jbkSignal.longValue());
        }
        if (usgSignal != null)
        {
            za1.a(23, usgSignal.longValue());
        }
        if (didSignal != null)
        {
            za1.a(24, didSignal);
        }
        if (evtTime != null)
        {
            za1.a(25, evtTime.longValue());
        }
        if (didSignalType != null)
        {
            za1.a(26, didSignalType.intValue());
        }
        if (intSignal != null)
        {
            za1.a(27, intSignal);
        }
        if (didOptOut != null)
        {
            za1.a(28, didOptOut.booleanValue());
        }
        if (cerSignal != null)
        {
            za1.a(29, cerSignal);
        }
        if (didSignalAndroidAdId != null)
        {
            za1.a(30, didSignalAndroidAdId);
        }
        if (uwSignal != null)
        {
            za1.a(31, uwSignal.longValue());
        }
        if (uhSignal != null)
        {
            za1.a(32, uhSignal.longValue());
        }
        if (utzSignal != null)
        {
            za1.a(33, utzSignal.longValue());
        }
        if (vnmSignal != null)
        {
            za1.a(34, vnmSignal);
        }
        if (vcdSignal != null)
        {
            za1.a(35, vcdSignal.longValue());
        }
        super.writeTo(za1);
    }

    public ()
    {
        clear();
    }
}
