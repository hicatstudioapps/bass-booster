// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.ads.mediation;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.mediation.MediationNativeListener;

// Referenced classes of package com.google.ads.mediation:
//            a, b, AbstractAdViewAdapter

final class e extends AdListener
    implements com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener, com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener, zza
{

    final AbstractAdViewAdapter a;
    final MediationNativeListener b;

    public e(AbstractAdViewAdapter abstractadviewadapter, MediationNativeListener mediationnativelistener)
    {
        a = abstractadviewadapter;
        b = mediationnativelistener;
    }

    public void onAdClicked()
    {
        b.onAdClicked(a);
    }

    public void onAdClosed()
    {
        b.onAdClosed(a);
    }

    public void onAdFailedToLoad(int i)
    {
        b.onAdFailedToLoad(a, i);
    }

    public void onAdLeftApplication()
    {
        b.onAdLeftApplication(a);
    }

    public void onAdLoaded()
    {
    }

    public void onAdOpened()
    {
        b.onAdOpened(a);
    }

    public void onAppInstallAdLoaded(NativeAppInstallAd nativeappinstallad)
    {
        b.onAdLoaded(a, new a(nativeappinstallad));
    }

    public void onContentAdLoaded(NativeContentAd nativecontentad)
    {
        b.onAdLoaded(a, new b(nativecontentad));
    }
}
