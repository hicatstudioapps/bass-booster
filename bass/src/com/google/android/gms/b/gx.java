// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.api.x;

// Referenced classes of package com.google.android.gms.b:
//            gz, gy, ha, tu

public abstract class gx
    implements x
{

    protected tu a;

    public gx(tu tu)
    {
        a = tu;
    }

    static String a(gx gx1, String s)
    {
        return gx1.c(s);
    }

    private String c(String s)
    {
        byte byte0 = -1;
        s.hashCode();
        JVM INSTR lookupswitch 10: default 96
    //                   -1396664534: 239
    //                   -1347010958: 183
    //                   -918817863: 254
    //                   -659376217: 197
    //                   -642208130: 169
    //                   -354048396: 269
    //                   -32082395: 284
    //                   96784904: 155
    //                   580119100: 225
    //                   725497484: 211;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L1:
        break; /* Loop/switch isn't completed */
_L8:
        break MISSING_BLOCK_LABEL_284;
_L12:
        switch (byte0)
        {
        default:
            return "internal";

        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            return "internal";

        case 4: // '\004'
        case 5: // '\005'
            return "io";

        case 6: // '\006'
        case 7: // '\007'
            return "network";

        case 8: // '\b'
        case 9: // '\t'
            return "policy";
        }
_L9:
        if (s.equals("error"))
        {
            byte0 = 0;
        }
          goto _L12
_L6:
        if (s.equals("playerFailed"))
        {
            byte0 = 1;
        }
          goto _L12
_L3:
        if (s.equals("inProgress"))
        {
            byte0 = 2;
        }
          goto _L12
_L5:
        if (s.equals("contentLengthMissing"))
        {
            byte0 = 3;
        }
          goto _L12
_L11:
        if (s.equals("noCacheDir"))
        {
            byte0 = 4;
        }
          goto _L12
_L10:
        if (s.equals("expireFailed"))
        {
            byte0 = 5;
        }
          goto _L12
_L2:
        if (s.equals("badUrl"))
        {
            byte0 = 6;
        }
          goto _L12
_L4:
        if (s.equals("downloadTimeout"))
        {
            byte0 = 7;
        }
          goto _L12
_L7:
        if (s.equals("sizeExceeded"))
        {
            byte0 = 8;
        }
          goto _L12
        if (s.equals("externalAbort"))
        {
            byte0 = 9;
        }
          goto _L12
    }

    public void a()
    {
    }

    protected void a(String s, String s1, int i)
    {
        zza.zzLE.post(new gz(this, s, s1, i));
    }

    protected void a(String s, String s1, int i, int j, boolean flag)
    {
        zza.zzLE.post(new gy(this, s, s1, i, j, flag));
    }

    protected void a(String s, String s1, String s2, String s3)
    {
        zza.zzLE.post(new ha(this, s, s1, s2, s3));
    }

    public abstract boolean a(String s);

    protected String b(String s)
    {
        return zzl.zzcN().zzaE(s);
    }

    public abstract void b();
}
