// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.aj;
import android.support.v4.app.n;
import android.support.v4.app.q;
import android.support.v4.app.x;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.av;
import java.io.FileDescriptor;
import java.io.PrintWriter;

// Referenced classes of package com.google.android.gms.b:
//            xe, xk

public class xj extends n
    implements android.content.DialogInterface.OnCancelListener
{

    private static final b a = com.google.android.gms.common.b.a();
    private boolean b;
    private boolean c;
    private int d;
    private ConnectionResult e;
    private final Handler f = new Handler(Looper.getMainLooper());
    private xe g;
    private final SparseArray h = new SparseArray();

    public xj()
    {
        d = -1;
    }

    private void K()
    {
        c = false;
        d = -1;
        e = null;
        if (g != null)
        {
            g.b();
            g = null;
        }
        for (int i = 0; i < h.size(); i++)
        {
            ((xk)h.valueAt(i)).b.b();
        }

    }

    static int a(xj xj1, int i)
    {
        xj1.d = i;
        return i;
    }

    static xe a(xj xj1, xe xe1)
    {
        xj1.g = xe1;
        return xe1;
    }

    public static xj a(q q1)
    {
label0:
        {
            av.b("Must be called from main thread of process");
            q1 = q1.f();
            xj xj1;
            try
            {
                xj1 = (xj)q1.a("GmsSupportLifecycleFrag");
            }
            // Misplaced declaration of an exception variable
            catch (q q1)
            {
                throw new IllegalStateException("Fragment with tag GmsSupportLifecycleFrag is not a SupportLifecycleFragment", q1);
            }
            if (xj1 != null)
            {
                q1 = xj1;
                if (!xj1.l())
                {
                    break label0;
                }
            }
            q1 = null;
        }
        return q1;
    }

    static ConnectionResult a(xj xj1, ConnectionResult connectionresult)
    {
        xj1.e = connectionresult;
        return connectionresult;
    }

    static b a()
    {
        return a;
    }

    private void a(int i, ConnectionResult connectionresult)
    {
        Log.w("GmsSupportLifecycleFrag", "Unresolved error while connecting client. Stopping auto-manage.");
        Object obj = (xk)h.get(i);
        if (obj != null)
        {
            a(i);
            obj = ((xk) (obj)).c;
            if (obj != null)
            {
                ((r) (obj)).onConnectionFailed(connectionresult);
            }
        }
        K();
    }

    static void a(xj xj1, int i, ConnectionResult connectionresult)
    {
        xj1.a(i, connectionresult);
    }

    static boolean a(xj xj1)
    {
        return xj1.b;
    }

    static boolean a(xj xj1, boolean flag)
    {
        xj1.c = flag;
        return flag;
    }

    public static xj b(q q1)
    {
        xj xj1 = a(q1);
        x x1 = q1.f();
        q1 = xj1;
        if (xj1 == null)
        {
            q1 = new xj();
            x1.a().a(q1, "GmsSupportLifecycleFrag").b();
            x1.b();
        }
        return q1;
    }

    static boolean b(xj xj1)
    {
        return xj1.c;
    }

    static void c(xj xj1)
    {
        xj1.K();
    }

    static Handler d(xj xj1)
    {
        return xj1.f;
    }

    public void a(int i)
    {
        xk xk1 = (xk)h.get(i);
        h.remove(i);
        if (xk1 != null)
        {
            xk1.a();
        }
    }

    public void a(int i, int j, Intent intent)
    {
        boolean flag = true;
        i;
        JVM INSTR tableswitch 1 2: default 28
    //                   1 58
    //                   2 39;
           goto _L1 _L2 _L3
_L1:
        i = 0;
_L4:
        if (i != 0)
        {
            K();
            return;
        } else
        {
            a(d, e);
            return;
        }
_L3:
        if (a.a(g()) != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        i = ((flag) ? 1 : 0);
          goto _L4
_L2:
        i = ((flag) ? 1 : 0);
        if (j == -1) goto _L4; else goto _L5
_L5:
        if (j == 0)
        {
            e = new ConnectionResult(13, null);
        }
        if (true) goto _L1; else goto _L6
_L6:
    }

    public void a(int i, com.google.android.gms.common.api.n n1, r r1)
    {
        av.a(n1, "GoogleApiClient instance cannot be null");
        boolean flag;
        if (h.indexOfKey(i) < 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag, (new StringBuilder()).append("Already managing a GoogleApiClient with id ").append(i).toString());
        r1 = new xk(this, i, n1, r1);
        h.put(i, r1);
        if (b && !c)
        {
            n1.b();
        }
    }

    public void a(Bundle bundle)
    {
        super.a(bundle);
        if (bundle != null)
        {
            c = bundle.getBoolean("resolving_error", false);
            d = bundle.getInt("failed_client_id", -1);
            if (d >= 0)
            {
                e = new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent)bundle.getParcelable("failed_resolution"));
            }
        }
    }

    public void a(String s, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        super.a(s, filedescriptor, printwriter, as);
        for (int i = 0; i < h.size(); i++)
        {
            ((xk)h.valueAt(i)).a(s, filedescriptor, printwriter, as);
        }

    }

    public void c()
    {
        super.c();
        b = true;
        if (!c)
        {
            for (int i = 0; i < h.size(); i++)
            {
                ((xk)h.valueAt(i)).b.b();
            }

        }
    }

    public void d()
    {
        super.d();
        b = false;
        for (int i = 0; i < h.size(); i++)
        {
            ((xk)h.valueAt(i)).b.c();
        }

    }

    public void e(Bundle bundle)
    {
        super.e(bundle);
        bundle.putBoolean("resolving_error", c);
        if (d >= 0)
        {
            bundle.putInt("failed_client_id", d);
            bundle.putInt("failed_status", e.c());
            bundle.putParcelable("failed_resolution", e.d());
        }
    }

    public void onCancel(DialogInterface dialoginterface)
    {
        a(d, new ConnectionResult(13, null));
    }

}
