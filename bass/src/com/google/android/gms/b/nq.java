// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.zzp;

// Referenced classes of package com.google.android.gms.b:
//            ur, tu, rq, tx, 
//            nr

public class nq
    implements Runnable
{

    protected final tu a;
    protected boolean b;
    protected boolean c;
    private final Handler d;
    private final long e;
    private long f;
    private tx g;
    private final int h;
    private final int i;

    public nq(tx tx1, tu tu1, int j, int k)
    {
        this(tx1, tu1, j, k, 200L, 50L);
    }

    public nq(tx tx1, tu tu1, int j, int k, long l, long l1)
    {
        e = l;
        f = l1;
        d = new Handler(Looper.getMainLooper());
        a = tu1;
        g = tx1;
        b = false;
        c = false;
        h = k;
        i = j;
    }

    static int a(nq nq1)
    {
        return nq1.i;
    }

    static int b(nq nq1)
    {
        return nq1.h;
    }

    static long c(nq nq1)
    {
        long l = nq1.f - 1L;
        nq1.f = l;
        return l;
    }

    static long d(nq nq1)
    {
        return nq1.f;
    }

    static tx e(nq nq1)
    {
        return nq1.g;
    }

    static long f(nq nq1)
    {
        return nq1.e;
    }

    static Handler g(nq nq1)
    {
        return nq1.d;
    }

    public void a()
    {
        d.postDelayed(this, e);
    }

    public void a(AdResponseParcel adresponseparcel)
    {
        a(adresponseparcel, new ur(this, a, adresponseparcel.zzGU));
    }

    public void a(AdResponseParcel adresponseparcel, ur ur1)
    {
        a.setWebViewClient(ur1);
        tu tu1 = a;
        if (TextUtils.isEmpty(adresponseparcel.zzDE))
        {
            ur1 = null;
        } else
        {
            ur1 = zzp.zzbx().a(adresponseparcel.zzDE);
        }
        tu1.loadDataWithBaseURL(ur1, adresponseparcel.body, "text/html", "UTF-8", null);
    }

    public void b()
    {
        this;
        JVM INSTR monitorenter ;
        b = true;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public boolean c()
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = b;
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    public boolean d()
    {
        return c;
    }

    public void run()
    {
        if (a == null || c())
        {
            g.a(a, true);
            return;
        } else
        {
            (new nr(this, a.a())).execute(new Void[0]);
            return;
        }
    }
}
