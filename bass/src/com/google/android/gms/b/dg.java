// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.google.android.gms.b:
//            db, cs, dh, dk, 
//            dq, rq

public class dg
{

    BlockingQueue a;
    ExecutorService b;
    LinkedHashMap c;
    Map d;
    String e;
    final Context f;
    final String g;
    private AtomicBoolean h;
    private File i;

    public dg(Context context, String s, String s1, Map map)
    {
        c = new LinkedHashMap();
        d = new HashMap();
        f = context;
        g = s;
        e = s1;
        h = new AtomicBoolean(false);
        h.set(((Boolean)db.I.c()).booleanValue());
        if (h.get())
        {
            context = Environment.getExternalStorageDirectory();
            if (context != null)
            {
                i = new File(context, "sdk_csi_data.txt");
            }
        }
        for (context = map.entrySet().iterator(); context.hasNext(); c.put(s.getKey(), s.getValue()))
        {
            s = (java.util.Map.Entry)context.next();
        }

        a = new ArrayBlockingQueue(30);
        b = Executors.newSingleThreadExecutor();
        b.execute(new dh(this));
        d.put("action", dk.b);
        d.put("ad_format", dk.b);
        d.put("e", dk.c);
    }

    private void a()
    {
        do
        {
            dq dq1;
            String s;
            do
            {
                dq1 = (dq)a.take();
                s = dq1.c();
            } while (TextUtils.isEmpty(s));
            a(a(((Map) (c)), dq1.d()), s);
        } while (true);
        InterruptedException interruptedexception;
        interruptedexception;
        zzb.zzd("CsiReporter:reporter interrupted", interruptedexception);
        return;
    }

    static void a(dg dg1)
    {
        dg1.a();
    }

    private void a(File file, String s)
    {
        if (file == null) goto _L2; else goto _L1
_L1:
        FileOutputStream fileoutputstream = new FileOutputStream(file, true);
        file = fileoutputstream;
        fileoutputstream.write(s.getBytes());
        file = fileoutputstream;
        fileoutputstream.write(10);
        if (fileoutputstream == null)
        {
            break MISSING_BLOCK_LABEL_40;
        }
        fileoutputstream.close();
_L4:
        return;
        file;
        zzb.zzd("CsiReporter: Cannot close file: sdk_csi_data.txt.", file);
        return;
        IOException ioexception;
        ioexception;
        s = null;
_L7:
        file = s;
        zzb.zzd("CsiReporter: Cannot write to file: sdk_csi_data.txt.", ioexception);
        if (s == null) goto _L4; else goto _L3
_L3:
        try
        {
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            zzb.zzd("CsiReporter: Cannot close file: sdk_csi_data.txt.", file);
        }
        return;
        s;
        file = null;
_L6:
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                zzb.zzd("CsiReporter: Cannot close file: sdk_csi_data.txt.", file);
            }
        }
        throw s;
_L2:
        zzb.zzaH("CsiReporter: File doesn't exists. Cannot write CSI data to file.");
        return;
        s;
        if (true) goto _L6; else goto _L5
_L5:
        ioexception;
        s = fileoutputstream;
          goto _L7
    }

    private void a(Map map, String s)
    {
        map = a(e, map, s);
        if (h.get())
        {
            a(i, ((String) (map)));
            return;
        } else
        {
            zzp.zzbx().a(f, g, map);
            return;
        }
    }

    public dk a(String s)
    {
        s = (dk)d.get(s);
        if (s != null)
        {
            return s;
        } else
        {
            return dk.a;
        }
    }

    String a(String s, Map map, String s1)
    {
        s = Uri.parse(s).buildUpon();
        java.util.Map.Entry entry;
        for (map = map.entrySet().iterator(); map.hasNext(); s.appendQueryParameter((String)entry.getKey(), (String)entry.getValue()))
        {
            entry = (java.util.Map.Entry)map.next();
        }

        s = new StringBuilder(s.build().toString());
        s.append("&").append("it").append("=").append(s1);
        return s.toString();
    }

    Map a(Map map, Map map1)
    {
        map = new LinkedHashMap(map);
        if (map1 == null)
        {
            return map;
        }
        String s;
        Object obj;
        String s1;
        for (map1 = map1.entrySet().iterator(); map1.hasNext(); map.put(s, a(s).a(s1, ((String) (obj)))))
        {
            obj = (java.util.Map.Entry)map1.next();
            s = (String)((java.util.Map.Entry) (obj)).getKey();
            obj = (String)((java.util.Map.Entry) (obj)).getValue();
            s1 = (String)map.get(s);
        }

        return map;
    }

    public void a(List list)
    {
        if (list != null && !list.isEmpty())
        {
            c.put("e", TextUtils.join(",", list));
        }
    }

    public boolean a(dq dq1)
    {
        return a.offer(dq1);
    }
}
