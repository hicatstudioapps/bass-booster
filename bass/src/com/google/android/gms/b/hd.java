// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            gx, tu, db, cs, 
//            qt, xx, sw

public class hd extends gx
{

    private static final Set b = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat c = new DecimalFormat("#,###");
    private File d;
    private boolean e;

    public hd(tu tu1)
    {
        super(tu1);
        tu1 = tu1.getContext().getCacheDir();
        if (tu1 == null)
        {
            zzb.zzaH("Context.getCacheDir() returned null");
        } else
        {
            d = new File(tu1, "admobVideoStreams");
            if (!d.isDirectory() && !d.mkdirs())
            {
                zzb.zzaH((new StringBuilder()).append("Could not create preload cache directory at ").append(d.getAbsolutePath()).toString());
                d = null;
                return;
            }
            if (!d.setReadable(true, false) || !d.setExecutable(true, false))
            {
                zzb.zzaH((new StringBuilder()).append("Could not set cache file permissions at ").append(d.getAbsolutePath()).toString());
                d = null;
                return;
            }
        }
    }

    private File a(File file)
    {
        return new File(d, (new StringBuilder()).append(file.getName()).append(".done").toString());
    }

    private static void b(File file)
    {
        if (file.isFile())
        {
            file.setLastModified(System.currentTimeMillis());
            return;
        }
        try
        {
            file.createNewFile();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            return;
        }
    }

    public boolean a(String s)
    {
        Object obj1;
        File file;
        String s2;
        if (d == null)
        {
            a(s, null, "noCacheDir", null);
            return false;
        }
        while (c() > ((Integer)db.o.c()).intValue()) 
        {
            if (!d())
            {
                zzb.zzaH("Unable to expire stream cache");
                a(s, null, "expireFailed", null);
                return false;
            }
        }
        String s1 = b(s);
        file = new File(d, s1);
        obj1 = a(file);
        if (file.isFile() && ((File) (obj1)).isFile())
        {
            int i = (int)file.length();
            zzb.zzaF((new StringBuilder()).append("Stream cache hit at ").append(s).toString());
            a(s, file.getAbsolutePath(), i);
            return true;
        }
        s2 = (new StringBuilder()).append(d.getAbsolutePath()).append(s).toString();
        synchronized (b)
        {
            if (!b.contains(s2))
            {
                break MISSING_BLOCK_LABEL_235;
            }
            zzb.zzaH((new StringBuilder()).append("Stream cache already in progress at ").append(s).toString());
            a(s, file.getAbsolutePath(), "inProgress", null);
        }
        return false;
        s;
        set;
        JVM INSTR monitorexit ;
        throw s;
        b.add(s2);
        set;
        JVM INSTR monitorexit ;
        Object obj3 = null;
        Object obj;
        obj = (new URL(s)).openConnection();
        int j = ((Integer)db.t.c()).intValue();
        ((URLConnection) (obj)).setConnectTimeout(j);
        ((URLConnection) (obj)).setReadTimeout(j);
        if (!(obj instanceof HttpURLConnection)) goto _L2; else goto _L1
_L1:
        int k = ((HttpURLConnection)obj).getResponseCode();
        if (k < 400) goto _L2; else goto _L3
_L3:
        Object obj2;
        obj2 = "badUrl";
        Object obj4;
        Object obj5;
        Object obj6;
        sw sw1;
        int l;
        int i1;
        int j1;
        long l1;
        long l2;
        try
        {
            obj1 = (new StringBuilder()).append("HTTP request failed. Code: ").append(Integer.toString(k)).toString();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj1 = null;
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj1 = null;
            continue; /* Loop/switch isn't completed */
        }
        try
        {
            throw new IOException((new StringBuilder()).append("HTTP status code ").append(k).append(" at ").append(s).toString());
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
_L7:
        if (obj instanceof RuntimeException)
        {
            zzp.zzbA().a(((Throwable) (obj)), true);
        }
        try
        {
            ((FileOutputStream) (obj3)).close();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj3) { }
        // Misplaced declaration of an exception variable
        catch (Object obj3) { }
        if (e)
        {
            zzb.zzaG((new StringBuilder()).append("Preload aborted for URL \"").append(s).append("\"").toString());
        } else
        {
            zzb.zzd((new StringBuilder()).append("Preload failed for URL \"").append(s).append("\"").toString(), ((Throwable) (obj)));
        }
        if (file.exists() && !file.delete())
        {
            zzb.zzaH((new StringBuilder()).append("Could not delete partial cache file at ").append(file.getAbsolutePath()).toString());
        }
        a(s, file.getAbsolutePath(), ((String) (obj2)), ((String) (obj1)));
        b.remove(s2);
        return false;
_L2:
        try
        {
            i1 = ((URLConnection) (obj)).getContentLength();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj1 = null;
            obj2 = "error";
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj1 = null;
            obj2 = "error";
            continue; /* Loop/switch isn't completed */
        }
        if (i1 >= 0)
        {
            break MISSING_BLOCK_LABEL_563;
        }
        zzb.zzaH((new StringBuilder()).append("Stream cache aborted, missing content-length header at ").append(s).toString());
        a(s, file.getAbsolutePath(), "contentLengthMissing", null);
        b.remove(s2);
        return false;
        obj2 = c.format(i1);
        j1 = ((Integer)db.p.c()).intValue();
        if (i1 <= j1)
        {
            break MISSING_BLOCK_LABEL_678;
        }
        zzb.zzaH((new StringBuilder()).append("Content length ").append(((String) (obj2))).append(" exceeds limit at ").append(s).toString());
        obj = (new StringBuilder()).append("File too big for full file cache. Size: ").append(((String) (obj2))).toString();
        a(s, file.getAbsolutePath(), "sizeExceeded", ((String) (obj)));
        b.remove(s2);
        return false;
        zzb.zzaF((new StringBuilder()).append("Caching ").append(((String) (obj2))).append(" bytes from ").append(s).toString());
        obj2 = Channels.newChannel(((URLConnection) (obj)).getInputStream());
        obj = new FileOutputStream(file);
        obj3 = ((FileOutputStream) (obj)).getChannel();
        obj4 = ByteBuffer.allocate(0x100000);
        obj6 = zzp.zzbB();
        k = 0;
        l1 = ((xx) (obj6)).a();
        sw1 = new sw(((Long)db.s.c()).longValue());
        l2 = ((Long)db.r.c()).longValue();
_L5:
        l = ((ReadableByteChannel) (obj2)).read(((ByteBuffer) (obj4)));
        if (l < 0)
        {
            break MISSING_BLOCK_LABEL_1095;
        }
        l = k + l;
        if (l <= j1)
        {
            break MISSING_BLOCK_LABEL_900;
        }
        obj1 = "sizeExceeded";
        obj2 = obj1;
        obj3 = obj1;
        obj4 = (new StringBuilder()).append("File too big for full file cache. Size: ").append(Integer.toString(l)).toString();
        obj3 = obj4;
        obj2 = obj1;
        obj6 = obj1;
        try
        {
            throw new IOException("stream cache file size limit exceeded");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            obj4 = obj;
            obj = obj1;
            obj1 = obj3;
            obj3 = obj4;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            obj3 = obj;
            obj = obj1;
            obj1 = obj4;
            obj2 = obj6;
        }
        continue; /* Loop/switch isn't completed */
        ((ByteBuffer) (obj4)).flip();
        while (((FileChannel) (obj3)).write(((ByteBuffer) (obj4))) > 0) ;
        ((ByteBuffer) (obj4)).clear();
        if (((xx) (obj6)).a() - l1 <= 1000L * l2)
        {
            break MISSING_BLOCK_LABEL_1021;
        }
        obj1 = "downloadTimeout";
        obj2 = obj1;
        obj3 = obj1;
        obj4 = (new StringBuilder()).append("Timeout exceeded. Limit: ").append(Long.toString(l2)).append(" sec").toString();
        obj3 = obj4;
        obj2 = obj1;
        obj6 = obj1;
        throw new IOException("stream cache time limit exceeded");
        if (e)
        {
            obj3 = "externalAbort";
            obj2 = obj3;
            try
            {
                throw new IOException("abort requested");
            }
            // Misplaced declaration of an exception variable
            catch (Object obj5)
            {
                obj1 = null;
                obj3 = obj;
                obj = obj5;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj2)
            {
                obj1 = null;
                obj5 = obj;
                obj = obj2;
                obj2 = obj3;
                obj3 = obj5;
            }
            continue; /* Loop/switch isn't completed */
        }
        k = l;
        if (!sw1.a()) goto _L5; else goto _L4
_L4:
        a(s, file.getAbsolutePath(), l, i1, false);
        k = l;
          goto _L5
        ((FileOutputStream) (obj)).close();
        if (zzb.zzQ(3))
        {
            obj2 = c.format(k);
            zzb.zzaF((new StringBuilder()).append("Preloaded ").append(((String) (obj2))).append(" bytes from ").append(s).toString());
        }
        file.setReadable(true, false);
        b(((File) (obj1)));
        a(s, file.getAbsolutePath(), k);
        b.remove(s2);
        return true;
        obj5;
        obj1 = null;
        obj2 = "error";
        obj3 = obj;
        obj = obj5;
        continue; /* Loop/switch isn't completed */
        IOException ioexception;
        ioexception;
        obj1 = null;
        obj2 = "error";
        obj3 = obj;
        obj = ioexception;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void b()
    {
        e = true;
    }

    public int c()
    {
        int i;
        int k;
        i = 0;
        k = 0;
        if (d != null) goto _L2; else goto _L1
_L1:
        return k;
_L2:
        File afile[] = d.listFiles();
        int l = afile.length;
        int j = 0;
        do
        {
            k = i;
            if (j >= l)
            {
                continue;
            }
            k = i;
            if (!afile[j].getName().endsWith(".done"))
            {
                k = i + 1;
            }
            j++;
            i = k;
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
    }

    public boolean d()
    {
        if (d == null)
        {
            return false;
        }
        File file = null;
        long l = 0x7fffffffffffffffL;
        File afile[] = d.listFiles();
        int j = afile.length;
        for (int i = 0; i < j; i++)
        {
            File file1 = afile[i];
            if (file1.getName().endsWith(".done"))
            {
                continue;
            }
            long l1 = file1.lastModified();
            if (l1 < l)
            {
                file = file1;
                l = l1;
            }
        }

        boolean flag;
        if (file != null)
        {
            boolean flag1 = file.delete();
            file = a(file);
            flag = flag1;
            if (file.isFile())
            {
                flag = flag1 & file.delete();
            }
        } else
        {
            flag = false;
        }
        return flag;
    }

}
