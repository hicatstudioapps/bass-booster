// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            zh, zi, zl

public final class yz
{

    private final byte a[];
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;

    private yz(byte abyte0[], int i1, int j1)
    {
        g = 0x7fffffff;
        i = 64;
        j = 0x4000000;
        a = abyte0;
        b = i1;
        c = i1 + j1;
        e = i1;
    }

    public static long a(long l1)
    {
        return l1 >>> 1 ^ -(1L & l1);
    }

    public static yz a(byte abyte0[], int i1, int j1)
    {
        return new yz(abyte0, i1, j1);
    }

    private void q()
    {
        c = c + d;
        int i1 = c;
        if (i1 > g)
        {
            d = i1 - g;
            c = c - d;
            return;
        } else
        {
            d = 0;
            return;
        }
    }

    public int a()
    {
        if (n())
        {
            f = 0;
            return 0;
        }
        f = i();
        if (f == 0)
        {
            throw zh.d();
        } else
        {
            return f;
        }
    }

    public void a(int i1)
    {
        if (f != i1)
        {
            throw zh.e();
        } else
        {
            return;
        }
    }

    public void a(zi zi1)
    {
        int i1 = i();
        if (h >= i)
        {
            throw zh.g();
        } else
        {
            i1 = c(i1);
            h = h + 1;
            zi1.mergeFrom(this);
            a(0);
            h = h - 1;
            d(i1);
            return;
        }
    }

    public byte[] a(int i1, int j1)
    {
        if (j1 == 0)
        {
            return zl.h;
        } else
        {
            byte abyte0[] = new byte[j1];
            int k1 = b;
            System.arraycopy(a, k1 + i1, abyte0, 0, j1);
            return abyte0;
        }
    }

    public void b()
    {
        int i1;
        do
        {
            i1 = a();
        } while (i1 != 0 && b(i1));
    }

    public boolean b(int i1)
    {
        switch (zl.a(i1))
        {
        default:
            throw zh.f();

        case 0: // '\0'
            d();
            return true;

        case 1: // '\001'
            l();
            return true;

        case 2: // '\002'
            g(i());
            return true;

        case 3: // '\003'
            b();
            a(zl.a(zl.b(i1), 4));
            return true;

        case 4: // '\004'
            return false;

        case 5: // '\005'
            k();
            break;
        }
        return true;
    }

    public int c(int i1)
    {
        if (i1 < 0)
        {
            throw zh.b();
        }
        i1 = e + i1;
        int j1 = g;
        if (i1 > j1)
        {
            throw zh.a();
        } else
        {
            g = i1;
            q();
            return j1;
        }
    }

    public long c()
    {
        return j();
    }

    public int d()
    {
        return i();
    }

    public void d(int i1)
    {
        g = i1;
        q();
    }

    public void e(int i1)
    {
        if (i1 > e - b)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Position ").append(i1).append(" is beyond current ").append(e - b).toString());
        }
        if (i1 < 0)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Bad position ").append(i1).toString());
        } else
        {
            e = b + i1;
            return;
        }
    }

    public boolean e()
    {
        return i() != 0;
    }

    public String f()
    {
        int i1 = i();
        if (i1 <= c - e && i1 > 0)
        {
            String s = new String(a, e, i1, "UTF-8");
            e = i1 + e;
            return s;
        } else
        {
            return new String(f(i1), "UTF-8");
        }
    }

    public byte[] f(int i1)
    {
        if (i1 < 0)
        {
            throw zh.b();
        }
        if (e + i1 > g)
        {
            g(g - e);
            throw zh.a();
        }
        if (i1 <= c - e)
        {
            byte abyte0[] = new byte[i1];
            System.arraycopy(a, e, abyte0, 0, i1);
            e = e + i1;
            return abyte0;
        } else
        {
            throw zh.a();
        }
    }

    public void g(int i1)
    {
        if (i1 < 0)
        {
            throw zh.b();
        }
        if (e + i1 > g)
        {
            g(g - e);
            throw zh.a();
        }
        if (i1 <= c - e)
        {
            e = e + i1;
            return;
        } else
        {
            throw zh.a();
        }
    }

    public byte[] g()
    {
        int i1 = i();
        if (i1 <= c - e && i1 > 0)
        {
            byte abyte0[] = new byte[i1];
            System.arraycopy(a, e, abyte0, 0, i1);
            e = i1 + e;
            return abyte0;
        }
        if (i1 == 0)
        {
            return zl.h;
        } else
        {
            return f(i1);
        }
    }

    public long h()
    {
        return a(j());
    }

    public int i()
    {
        int i1 = p();
        if (i1 < 0) goto _L2; else goto _L1
_L1:
        return i1;
_L2:
        i1 &= 0x7f;
        byte byte0 = p();
        if (byte0 >= 0)
        {
            return i1 | byte0 << 7;
        }
        i1 |= (byte0 & 0x7f) << 7;
        byte0 = p();
        if (byte0 >= 0)
        {
            return i1 | byte0 << 14;
        }
        i1 |= (byte0 & 0x7f) << 14;
        int k1 = p();
        if (k1 >= 0)
        {
            return i1 | k1 << 21;
        }
        byte0 = p();
        k1 = i1 | (k1 & 0x7f) << 21 | byte0 << 28;
        i1 = k1;
        if (byte0 < 0)
        {
            int j1 = 0;
label0:
            do
            {
label1:
                {
                    if (j1 >= 5)
                    {
                        break label1;
                    }
                    i1 = k1;
                    if (p() >= 0)
                    {
                        break label0;
                    }
                    j1++;
                }
            } while (true);
        }
        if (true) goto _L1; else goto _L3
_L3:
        throw zh.c();
    }

    public long j()
    {
        int i1 = 0;
        long l1 = 0L;
        for (; i1 < 64; i1 += 7)
        {
            byte byte0 = p();
            l1 |= (long)(byte0 & 0x7f) << i1;
            if ((byte0 & 0x80) == 0)
            {
                return l1;
            }
        }

        throw zh.c();
    }

    public int k()
    {
        return p() & 0xff | (p() & 0xff) << 8 | (p() & 0xff) << 16 | (p() & 0xff) << 24;
    }

    public long l()
    {
        int i1 = p();
        int j1 = p();
        int k1 = p();
        int l1 = p();
        int i2 = p();
        int j2 = p();
        int k2 = p();
        int l2 = p();
        long l3 = i1;
        return ((long)j1 & 255L) << 8 | l3 & 255L | ((long)k1 & 255L) << 16 | ((long)l1 & 255L) << 24 | ((long)i2 & 255L) << 32 | ((long)j2 & 255L) << 40 | ((long)k2 & 255L) << 48 | ((long)l2 & 255L) << 56;
    }

    public int m()
    {
        if (g == 0x7fffffff)
        {
            return -1;
        } else
        {
            int i1 = e;
            return g - i1;
        }
    }

    public boolean n()
    {
        return e == c;
    }

    public int o()
    {
        return e - b;
    }

    public byte p()
    {
        if (e == c)
        {
            throw zh.a();
        } else
        {
            byte abyte0[] = a;
            int i1 = e;
            e = i1 + 1;
            return abyte0[i1];
        }
    }
}
