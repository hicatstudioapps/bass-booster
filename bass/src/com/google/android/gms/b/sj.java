// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.common.internal.as;
import com.google.android.gms.common.internal.au;

public class sj
{

    public final String a;
    public final double b;
    public final double c;
    public final double d;
    public final int e;

    public sj(String s, double d1, double d2, double d3, 
            int i)
    {
        a = s;
        c = d1;
        b = d2;
        d = d3;
        e = i;
    }

    public boolean equals(Object obj)
    {
        if (obj instanceof sj)
        {
            if (as.a(a, ((sj) (obj = (sj)obj)).a) && b == ((sj) (obj)).b && c == ((sj) (obj)).c && e == ((sj) (obj)).e && Double.compare(d, ((sj) (obj)).d) == 0)
            {
                return true;
            }
        }
        return false;
    }

    public int hashCode()
    {
        return as.a(new Object[] {
            a, Double.valueOf(b), Double.valueOf(c), Double.valueOf(d), Integer.valueOf(e)
        });
    }

    public String toString()
    {
        return as.a(this).a("name", a).a("minBound", Double.valueOf(c)).a("maxBound", Double.valueOf(b)).a("percent", Double.valueOf(d)).a("count", Integer.valueOf(e)).toString();
    }
}
