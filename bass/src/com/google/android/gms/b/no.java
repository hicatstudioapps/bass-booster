// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.internal.av;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.google.android.gms.b:
//            sd, tx, qq, qp, 
//            np, rq, db, cs, 
//            ny, tu, rt

public abstract class no
    implements sd, tx
{

    protected final ny a;
    protected final Context b;
    protected final tu c;
    protected final qq d;
    protected AdResponseParcel e;
    protected final Object f = new Object();
    private Runnable g;
    private AtomicBoolean h;

    protected no(Context context, qq qq1, tu tu1, ny ny1)
    {
        h = new AtomicBoolean(true);
        b = context;
        d = qq1;
        e = d.b;
        c = tu1;
        a = ny1;
    }

    static AtomicBoolean a(no no1)
    {
        return no1.h;
    }

    private qp b(int i)
    {
        AdRequestInfoParcel adrequestinfoparcel = d.a;
        return new qp(adrequestinfoparcel.zzGq, c, e.zzAQ, i, e.zzAR, e.zzGP, e.orientation, e.zzAU, adrequestinfoparcel.zzGt, e.zzGN, null, null, null, null, null, e.zzGO, d.d, e.zzGM, d.f, e.zzGR, e.zzGS, d.h, null);
    }

    public final Void a()
    {
        av.b("Webview render task needs to be called on UI thread.");
        g = new np(this);
        rq.a.postDelayed(g, ((Long)db.ax.c()).longValue());
        b();
        return null;
    }

    protected void a(int i)
    {
        if (i != -2)
        {
            e = new AdResponseParcel(i, e.zzAU);
        }
        a.zzb(b(i));
    }

    public void a(tu tu1, boolean flag)
    {
        zzb.zzaF("WebView finished loading.");
        if (!h.getAndSet(false))
        {
            return;
        }
        int i;
        if (flag)
        {
            i = c();
        } else
        {
            i = -1;
        }
        a(i);
        rq.a.removeCallbacks(g);
    }

    protected abstract void b();

    protected int c()
    {
        return -2;
    }

    public void cancel()
    {
        if (!h.getAndSet(false))
        {
            return;
        } else
        {
            c.stopLoading();
            zzp.zzbz().a(c);
            a(-1);
            rq.a.removeCallbacks(g);
            return;
        }
    }

    public Object zzfR()
    {
        return a();
    }
}
