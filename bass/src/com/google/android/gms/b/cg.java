// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.PriorityQueue;

// Referenced classes of package com.google.android.gms.b:
//            cl, ch, cf, cj, 
//            ci, ck, cm, cn

public class cg
{

    private final int a = 6;
    private final int b;
    private final int c = 0;
    private final cf d = new cl();

    public cg(int i)
    {
        b = i;
    }

    private String b(String s)
    {
        String as[];
        int i;
        as = s.split("\n");
        if (as.length == 0)
        {
            return "";
        }
        s = a();
        Arrays.sort(as, new ch(this));
        i = 0;
_L2:
        if (i >= as.length || i >= b)
        {
            break MISSING_BLOCK_LABEL_91;
        }
        if (as[i].trim().length() != 0)
        {
            break; /* Loop/switch isn't completed */
        }
_L3:
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        s.a(d.a(as[i]));
          goto _L3
        IOException ioexception;
        ioexception;
        zzb.zzb("Error while writing hash to byteStream", ioexception);
        return s.toString();
    }

    cj a()
    {
        return new cj();
    }

    String a(String s)
    {
        Iterator iterator;
        String as[] = s.split("\n");
        if (as.length == 0)
        {
            return "";
        }
        s = a();
        PriorityQueue priorityqueue = new PriorityQueue(b, new ci(this));
        int i = 0;
        while (i < as.length) 
        {
            String as1[] = ck.b(as[i]);
            if (as1.length >= a)
            {
                cm.a(as1, b, a, priorityqueue);
            }
            i++;
        }
        iterator = priorityqueue.iterator();
_L2:
        cn cn1;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        cn1 = (cn)iterator.next();
        s.a(d.a(cn1.b));
        if (true) goto _L2; else goto _L1
        IOException ioexception;
        ioexception;
        zzb.zzb("Error while writing hash to byteStream", ioexception);
_L1:
        return s.toString();
    }

    public String a(ArrayList arraylist)
    {
        StringBuffer stringbuffer = new StringBuffer();
        for (arraylist = arraylist.iterator(); arraylist.hasNext(); stringbuffer.append('\n'))
        {
            stringbuffer.append(((String)arraylist.next()).toLowerCase(Locale.US));
        }

        switch (c)
        {
        default:
            return "";

        case 0: // '\0'
            return a(stringbuffer.toString());

        case 1: // '\001'
            return b(stringbuffer.toString());
        }
    }
}
