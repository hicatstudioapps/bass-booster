// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import java.util.concurrent.Executor;

// Referenced classes of package com.google.android.gms.b:
//            xt, hf, uu, hg, 
//            vu, yk

public class he
    implements xt
{

    private final Executor a;

    public he(Handler handler)
    {
        a = new hf(this, handler);
    }

    public void a(uu uu1, vu vu1)
    {
        a(uu1, vu1, null);
    }

    public void a(uu uu1, vu vu1, Runnable runnable)
    {
        uu1.t();
        uu1.b("post-response");
        a.execute(new hg(this, uu1, vu1, runnable));
    }

    public void a(uu uu1, yk yk)
    {
        uu1.b("post-error");
        yk = vu.a(yk);
        a.execute(new hg(this, uu1, yk, null));
    }
}
