// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzp;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            ic, id, db, cs, 
//            ie, xx, hh, ig

public class ib
{

    private final Map a = new HashMap();
    private final LinkedList b = new LinkedList();
    private hh c;

    public ib()
    {
    }

    private static void a(String s, ic ic1)
    {
        if (zzb.zzQ(2))
        {
            zzb.v(String.format(s, new Object[] {
                ic1
            }));
        }
    }

    private String[] a(String s)
    {
        String as[] = s.split("\0");
        int i = 0;
_L2:
        s = as;
        if (i >= as.length)
        {
            break; /* Loop/switch isn't completed */
        }
        as[i] = new String(Base64.decode(as[i], 0), "UTF-8");
        i++;
        if (true) goto _L2; else goto _L1
        s;
        s = new String[0];
_L1:
        return s;
    }

    private String e()
    {
        Object obj;
        try
        {
            obj = new StringBuilder();
            Iterator iterator = b.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                ((StringBuilder) (obj)).append(Base64.encodeToString(((ic)iterator.next()).toString().getBytes("UTF-8"), 0));
                if (iterator.hasNext())
                {
                    ((StringBuilder) (obj)).append("\0");
                }
            } while (true);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            return "";
        }
        obj = ((StringBuilder) (obj)).toString();
        return ((String) (obj));
    }

    ie a(AdRequestParcel adrequestparcel, String s)
    {
        ic ic1 = new ic(adrequestparcel, s);
        id id1 = (id)a.get(ic1);
        if (id1 == null)
        {
            a("Interstitial pool created at %s.", ic1);
            adrequestparcel = new id(adrequestparcel, s);
            a.put(ic1, adrequestparcel);
        } else
        {
            adrequestparcel = id1;
        }
        b.remove(ic1);
        b.add(ic1);
        ic1.a();
        for (; b.size() > ((Integer)db.ag.c()).intValue(); a.remove(s))
        {
            s = (ic)b.remove();
            id1 = (id)a.get(s);
            a("Evicting interstitial queue for %s.", ((ic) (s)));
            for (; id1.d() > 0; id1.c().a.zzbo()) { }
        }

        while (adrequestparcel.d() > 0) 
        {
            s = adrequestparcel.c();
            if (((ie) (s)).e && zzp.zzbB().a() - ((ie) (s)).d > 1000L * (long)((Integer)db.ai.c()).intValue())
            {
                a("Expired interstitial at %s.", ic1);
            } else
            {
                a("Pooled interstitial returned at %s.", ic1);
                return s;
            }
        }
        return null;
    }

    void a()
    {
        if (c == null)
        {
            return;
        }
        for (Iterator iterator = a.entrySet().iterator(); iterator.hasNext();)
        {
            Object obj = (java.util.Map.Entry)iterator.next();
            ic ic1 = (ic)((java.util.Map.Entry) (obj)).getKey();
            obj = (id)((java.util.Map.Entry) (obj)).getValue();
            while (((id) (obj)).d() < ((Integer)db.ah.c()).intValue()) 
            {
                a("Pooling one interstitial for %s.", ic1);
                ((id) (obj)).a(c);
            }
        }

        b();
    }

    void a(hh hh1)
    {
        if (c == null)
        {
            c = hh1;
            c();
        }
    }

    void b()
    {
        if (c == null)
        {
            return;
        }
        android.content.SharedPreferences.Editor editor = c.b().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0).edit();
        editor.clear();
        Iterator iterator = a.entrySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (java.util.Map.Entry)iterator.next();
            ic ic1 = (ic)((java.util.Map.Entry) (obj)).getKey();
            if (ic1.b())
            {
                obj = (id)((java.util.Map.Entry) (obj)).getValue();
                obj = (new ig(((id) (obj)).a(), ((id) (obj)).b())).a();
                editor.putString(ic1.toString(), ((String) (obj)));
                a("Saved interstitial queue for %s.", ic1);
            }
        } while (true);
        editor.putString("PoolKeys", e());
        editor.commit();
    }

    void c()
    {
        if (c != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        HashMap hashmap;
        SharedPreferences sharedpreferences;
        Iterator iterator;
        sharedpreferences = c.b().getSharedPreferences("com.google.android.gms.ads.internal.interstitial.InterstitialAdPool", 0);
        d();
        hashmap = new HashMap();
        iterator = sharedpreferences.getAll().entrySet().iterator();
_L4:
        Object obj;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (java.util.Map.Entry)iterator.next();
        if (!((String)((java.util.Map.Entry) (obj)).getKey()).equals("PoolKeys"))
        {
            Object obj2 = new ig((String)((java.util.Map.Entry) (obj)).getValue());
            obj = new ic(((ig) (obj2)).a, ((ig) (obj2)).b);
            if (!a.containsKey(obj))
            {
                obj2 = new id(((ig) (obj2)).a, ((ig) (obj2)).b);
                a.put(obj, obj2);
                hashmap.put(((ic) (obj)).toString(), obj);
                a("Restored interstitial queue for %s.", ((ic) (obj)));
            }
        }
        continue; /* Loop/switch isn't completed */
        Object obj1;
        obj1;
_L5:
        zzb.zzd("Malformed preferences value for InterstitialAdPool.", ((Throwable) (obj1)));
        if (true) goto _L4; else goto _L3
_L3:
        String as[] = a(sharedpreferences.getString("PoolKeys", ""));
        int j = as.length;
        int i = 0;
        while (i < j) 
        {
            ic ic1 = (ic)hashmap.get(as[i]);
            if (a.containsKey(ic1))
            {
                b.add(ic1);
            }
            i++;
        }
          goto _L1
        as;
          goto _L5
    }

    void d()
    {
        ic ic1;
        for (; b.size() > 0; a.remove(ic1))
        {
            ic1 = (ic)b.remove();
            id id1 = (id)a.get(ic1);
            a("Flushing interstitial queue for %s.", ic1);
            for (; id1.d() > 0; id1.c().a.zzbo()) { }
        }

    }
}
