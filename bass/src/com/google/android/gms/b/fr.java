// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, xx, tu, dp, 
//            dq

public final class fr
    implements gd
{

    public fr()
    {
    }

    private long a(long l)
    {
        return (l - zzp.zzbB().a()) + zzp.zzbB().b();
    }

    private void a(tu tu1, Map map)
    {
        String s1 = (String)map.get("label");
        String s = (String)map.get("start_label");
        map = (String)map.get("timestamp");
        if (TextUtils.isEmpty(s1))
        {
            zzb.zzaH("No label given for CSI tick.");
            return;
        }
        if (TextUtils.isEmpty(map))
        {
            zzb.zzaH("No timestamp given for CSI tick.");
            return;
        }
        long l;
        try
        {
            l = a(Long.parseLong(map));
        }
        // Misplaced declaration of an exception variable
        catch (tu tu1)
        {
            zzb.zzd("Malformed timestamp for CSI tick.", tu1);
            return;
        }
        map = s;
        if (TextUtils.isEmpty(s))
        {
            map = "native:view_load";
        }
        tu1.w().a(s1, map, l);
    }

    private void b(tu tu1, Map map)
    {
        map = (String)map.get("value");
        if (TextUtils.isEmpty(map))
        {
            zzb.zzaH("No value given for CSI experiment.");
            return;
        }
        tu1 = tu1.w().a();
        if (tu1 == null)
        {
            zzb.zzaH("No ticker for WebView, dropping experiment ID.");
            return;
        } else
        {
            tu1.a("e", map);
            return;
        }
    }

    private void c(tu tu1, Map map)
    {
        String s = (String)map.get("name");
        map = (String)map.get("value");
        if (TextUtils.isEmpty(map))
        {
            zzb.zzaH("No value given for CSI extra.");
            return;
        }
        if (TextUtils.isEmpty(s))
        {
            zzb.zzaH("No name given for CSI extra.");
            return;
        }
        tu1 = tu1.w().a();
        if (tu1 == null)
        {
            zzb.zzaH("No ticker for WebView, dropping extra parameter.");
            return;
        } else
        {
            tu1.a(s, map);
            return;
        }
    }

    public void zza(tu tu1, Map map)
    {
        String s = (String)map.get("action");
        if ("tick".equals(s))
        {
            a(tu1, map);
        } else
        {
            if ("experiment".equals(s))
            {
                b(tu1, map);
                return;
            }
            if ("extra".equals(s))
            {
                c(tu1, map);
                return;
            }
        }
    }
}
