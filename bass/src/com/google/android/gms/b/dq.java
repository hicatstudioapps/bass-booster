// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzp;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            xx, do, qt, dg, 
//            dk

public class dq
{

    boolean a;
    private final List b = new LinkedList();
    private final Map c = new LinkedHashMap();
    private final Object d = new Object();
    private String e;
    private do f;
    private dq g;

    public dq(boolean flag, String s, String s1)
    {
        a = flag;
        c.put("action", s);
        c.put("ad_format", s1);
    }

    public do a()
    {
        return a(zzp.zzbB().b());
    }

    public do a(long l)
    {
        if (!a)
        {
            return null;
        } else
        {
            return new do(l, null, null);
        }
    }

    public void a(dq dq1)
    {
        synchronized (d)
        {
            g = dq1;
        }
        return;
        dq1;
        obj;
        JVM INSTR monitorexit ;
        throw dq1;
    }

    public void a(String s)
    {
        if (!a)
        {
            return;
        }
        synchronized (d)
        {
            e = s;
        }
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void a(String s, String s1)
    {
        dg dg1;
        while (!a || TextUtils.isEmpty(s1) || (dg1 = zzp.zzbA().e()) == null) 
        {
            return;
        }
        synchronized (d)
        {
            dg1.a(s).a(c, s, s1);
        }
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public transient boolean a(do do1, long l, String as[])
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        int j = as.length;
        int i = 0;
_L2:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        do do2 = new do(l, as[i], do1);
        b.add(do2);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        obj;
        JVM INSTR monitorexit ;
        return true;
        do1;
        obj;
        JVM INSTR monitorexit ;
        throw do1;
    }

    public transient boolean a(do do1, String as[])
    {
        if (!a || do1 == null)
        {
            return false;
        } else
        {
            return a(do1, zzp.zzbB().b(), as);
        }
    }

    public void b()
    {
        synchronized (d)
        {
            f = a();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public String c()
    {
        Object obj1 = new StringBuilder();
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        Iterator iterator = b.iterator();
_L2:
        String s;
        do do1;
        long l;
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_118;
            }
            do1 = (do)iterator.next();
            l = do1.a();
            s = do1.b();
            do1 = do1.c();
        } while (do1 == null || l <= 0L);
        long l1 = do1.a();
        ((StringBuilder) (obj1)).append(s).append('.').append(l - l1).append(',');
        if (true) goto _L2; else goto _L1
_L1:
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        b.clear();
        if (TextUtils.isEmpty(e)) goto _L4; else goto _L3
_L3:
        ((StringBuilder) (obj1)).append(e);
_L6:
        obj1 = ((StringBuilder) (obj1)).toString();
        obj;
        JVM INSTR monitorexit ;
        return ((String) (obj1));
_L4:
        if (((StringBuilder) (obj1)).length() <= 0) goto _L6; else goto _L5
_L5:
        ((StringBuilder) (obj1)).setLength(((StringBuilder) (obj1)).length() - 1);
          goto _L6
    }

    Map d()
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        Object obj1 = zzp.zzbA().e();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_25;
        }
        if (g != null)
        {
            break MISSING_BLOCK_LABEL_34;
        }
        obj1 = c;
        obj;
        JVM INSTR monitorexit ;
        return ((Map) (obj1));
        obj1 = ((dg) (obj1)).a(c, g.d());
        obj;
        JVM INSTR monitorexit ;
        return ((Map) (obj1));
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public do e()
    {
        do do1;
        synchronized (d)
        {
            do1 = f;
        }
        return do1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
