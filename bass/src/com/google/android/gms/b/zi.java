// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.io.IOException;
import java.util.Arrays;

// Referenced classes of package com.google.android.gms.b:
//            zh, yz, za, zj

public abstract class zi
{

    protected volatile int b;

    public zi()
    {
        b = -1;
    }

    public static final zi mergeFrom(zi zi1, byte abyte0[])
    {
        return mergeFrom(zi1, abyte0, 0, abyte0.length);
    }

    public static final zi mergeFrom(zi zi1, byte abyte0[], int i, int j)
    {
        try
        {
            abyte0 = yz.a(abyte0, i, j);
            zi1.mergeFrom(((yz) (abyte0)));
            abyte0.a(0);
        }
        // Misplaced declaration of an exception variable
        catch (zi zi1)
        {
            throw zi1;
        }
        // Misplaced declaration of an exception variable
        catch (zi zi1)
        {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
        }
        return zi1;
    }

    public static final boolean messageNanoEquals(zi zi1, zi zi2)
    {
        boolean flag1 = false;
        boolean flag;
        if (zi1 == zi2)
        {
            flag = true;
        } else
        {
            flag = flag1;
            if (zi1 != null)
            {
                flag = flag1;
                if (zi2 != null)
                {
                    flag = flag1;
                    if (zi1.getClass() == zi2.getClass())
                    {
                        int i = zi1.getSerializedSize();
                        flag = flag1;
                        if (zi2.getSerializedSize() == i)
                        {
                            byte abyte0[] = new byte[i];
                            byte abyte1[] = new byte[i];
                            toByteArray(zi1, abyte0, 0, i);
                            toByteArray(zi2, abyte1, 0, i);
                            return Arrays.equals(abyte0, abyte1);
                        }
                    }
                }
            }
        }
        return flag;
    }

    public static final void toByteArray(zi zi1, byte abyte0[], int i, int j)
    {
        try
        {
            abyte0 = za.a(abyte0, i, j);
            zi1.writeTo(abyte0);
            abyte0.b();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (zi zi1)
        {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", zi1);
        }
    }

    public static final byte[] toByteArray(zi zi1)
    {
        byte abyte0[] = new byte[zi1.getSerializedSize()];
        toByteArray(zi1, abyte0, 0, abyte0.length);
        return abyte0;
    }

    protected int a()
    {
        return 0;
    }

    public zi clone()
    {
        return (zi)super.clone();
    }

    public volatile Object clone()
    {
        return clone();
    }

    public int getCachedSize()
    {
        if (b < 0)
        {
            getSerializedSize();
        }
        return b;
    }

    public int getSerializedSize()
    {
        int i = a();
        b = i;
        return i;
    }

    public abstract zi mergeFrom(yz yz1);

    public String toString()
    {
        return zj.a(this);
    }

    public void writeTo(za za1)
    {
    }
}
