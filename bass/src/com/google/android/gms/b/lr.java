// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            ly, tu, rt, rq, 
//            co, qt, ls, lt

public class lr extends ly
{

    private final Map a;
    private final Context b;

    public lr(tu tu1, Map map)
    {
        super(tu1, "storePicture");
        a = map;
        b = tu1.e();
    }

    static Context a(lr lr1)
    {
        return lr1.b;
    }

    android.app.DownloadManager.Request a(String s, String s1)
    {
        s = new android.app.DownloadManager.Request(Uri.parse(s));
        s.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, s1);
        zzp.zzbz().a(s);
        return s;
    }

    String a(String s)
    {
        return Uri.parse(s).getLastPathSegment();
    }

    public void a()
    {
        if (b == null)
        {
            b("Activity context is not available");
            return;
        }
        if (!zzp.zzbx().e(b).c())
        {
            b("Feature is not supported by the device.");
            return;
        }
        String s = (String)a.get("iurl");
        if (TextUtils.isEmpty(s))
        {
            b("Image url cannot be empty.");
            return;
        }
        if (!URLUtil.isValidUrl(s))
        {
            b((new StringBuilder()).append("Invalid image url: ").append(s).toString());
            return;
        }
        String s1 = a(s);
        if (!zzp.zzbx().c(s1))
        {
            b((new StringBuilder()).append("Image type not recognized: ").append(s1).toString());
            return;
        } else
        {
            android.app.AlertDialog.Builder builder = zzp.zzbx().d(b);
            builder.setTitle(zzp.zzbA().a(b.store_picture_title, "Save image"));
            builder.setMessage(zzp.zzbA().a(b.store_picture_message, "Allow Ad to store image in Picture gallery?"));
            builder.setPositiveButton(zzp.zzbA().a(b.accept, "Accept"), new ls(this, s, s1));
            builder.setNegativeButton(zzp.zzbA().a(b.decline, "Decline"), new lt(this));
            builder.create().show();
            return;
        }
    }
}
