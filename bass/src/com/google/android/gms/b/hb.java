// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.common.internal.e;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, gw, tu, gu, 
//            hc

public class hb
    implements gd
{

    public hb()
    {
    }

    public void zza(tu tu1, Map map)
    {
        gw gw1 = zzp.zzbL();
        if (map.containsKey("abort"))
        {
            if (!gw1.a(tu1))
            {
                zzb.zzaH("Precache abort but no preload task running.");
            }
            return;
        }
        String s = (String)map.get("src");
        if (s == null)
        {
            zzb.zzaH("Precache video action is missing the src parameter.");
            return;
        }
        int i;
        try
        {
            i = Integer.parseInt((String)map.get("player"));
        }
        catch (NumberFormatException numberformatexception)
        {
            i = 0;
        }
        if (map.containsKey("mimetype"))
        {
            map = (String)map.get("mimetype");
        } else
        {
            map = "";
        }
        if (gw1.b(tu1))
        {
            zzb.zzaH("Precache task already running.");
            return;
        } else
        {
            e.a(tu1.g());
            (new gu(tu1, tu1.g().zzpm.a(tu1, i, map), s)).zzgX();
            return;
        }
    }
}
