// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.util.Arrays;

// Referenced classes of package com.google.android.gms.b:
//            zc, za, zl, yz, 
//            zr, zn, zo, zp, 
//            zg, ze, zi

public final class zq extends zc
{

    public long c;
    public long d;
    public long e;
    public String f;
    public int g;
    public int h;
    public boolean i;
    public zr j[];
    public zo k;
    public byte l[];
    public byte m[];
    public byte n[];
    public zn o;
    public String p;
    public long q;
    public zp r;
    public byte s[];
    public int t;
    public int u[];

    public zq()
    {
        c();
    }

    protected int a()
    {
        boolean flag = false;
        int i1 = super.a();
        int j1 = i1;
        if (c != 0L)
        {
            j1 = i1 + za.c(1, c);
        }
        i1 = j1;
        if (!f.equals(""))
        {
            i1 = j1 + za.b(2, f);
        }
        j1 = i1;
        if (j != null)
        {
            j1 = i1;
            if (j.length > 0)
            {
                for (j1 = 0; j1 < j.length;)
                {
                    zr zr1 = j[j1];
                    int k1 = i1;
                    if (zr1 != null)
                    {
                        k1 = i1 + za.c(3, zr1);
                    }
                    j1++;
                    i1 = k1;
                }

                j1 = i1;
            }
        }
        i1 = j1;
        if (!Arrays.equals(l, zl.h))
        {
            i1 = j1 + za.b(6, l);
        }
        j1 = i1;
        if (o != null)
        {
            j1 = i1 + za.c(7, o);
        }
        i1 = j1;
        if (!Arrays.equals(m, zl.h))
        {
            i1 = j1 + za.b(8, m);
        }
        j1 = i1;
        if (k != null)
        {
            j1 = i1 + za.c(9, k);
        }
        i1 = j1;
        if (i)
        {
            i1 = j1 + za.b(10, i);
        }
        j1 = i1;
        if (g != 0)
        {
            j1 = i1 + za.b(11, g);
        }
        i1 = j1;
        if (h != 0)
        {
            i1 = j1 + za.b(12, h);
        }
        j1 = i1;
        if (!Arrays.equals(n, zl.h))
        {
            j1 = i1 + za.b(13, n);
        }
        i1 = j1;
        if (!p.equals(""))
        {
            i1 = j1 + za.b(14, p);
        }
        j1 = i1;
        if (q != 0x2bf20L)
        {
            j1 = i1 + za.d(15, q);
        }
        i1 = j1;
        if (r != null)
        {
            i1 = j1 + za.c(16, r);
        }
        j1 = i1;
        if (d != 0L)
        {
            j1 = i1 + za.c(17, d);
        }
        int l1 = j1;
        if (!Arrays.equals(s, zl.h))
        {
            l1 = j1 + za.b(18, s);
        }
        i1 = l1;
        if (t != 0)
        {
            i1 = l1 + za.b(19, t);
        }
        j1 = i1;
        if (u != null)
        {
            j1 = i1;
            if (u.length > 0)
            {
                int i2 = 0;
                for (j1 = ((flag) ? 1 : 0); j1 < u.length; j1++)
                {
                    i2 += za.b(u[j1]);
                }

                j1 = i1 + i2 + u.length * 2;
            }
        }
        i1 = j1;
        if (e != 0L)
        {
            i1 = j1 + za.c(21, e);
        }
        return i1;
    }

    public zq a(yz yz1)
    {
_L23:
        int i1 = yz1.a();
        i1;
        JVM INSTR lookupswitch 21: default 184
    //                   0: 193
    //                   8: 195
    //                   18: 206
    //                   26: 217
    //                   50: 341
    //                   58: 352
    //                   66: 381
    //                   74: 392
    //                   80: 421
    //                   88: 432
    //                   96: 443
    //                   106: 454
    //                   114: 465
    //                   120: 476
    //                   130: 487
    //                   136: 516
    //                   146: 527
    //                   152: 538
    //                   160: 583
    //                   162: 685
    //                   168: 813;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16 _L17 _L18 _L19 _L20 _L21 _L22
_L1:
        if (a(yz1, i1)) goto _L23; else goto _L2
_L2:
        return this;
_L3:
        c = yz1.c();
          goto _L23
_L4:
        f = yz1.f();
          goto _L23
_L5:
        int j2 = zl.b(yz1, 26);
        zr azr[];
        int j1;
        if (j == null)
        {
            j1 = 0;
        } else
        {
            j1 = j.length;
        }
        azr = new zr[j2 + j1];
        j2 = j1;
        if (j1 != 0)
        {
            System.arraycopy(j, 0, azr, 0, j1);
            j2 = j1;
        }
        for (; j2 < azr.length - 1; j2++)
        {
            azr[j2] = new zr();
            yz1.a(azr[j2]);
            yz1.a();
        }

        azr[j2] = new zr();
        yz1.a(azr[j2]);
        j = azr;
          goto _L23
_L6:
        l = yz1.g();
          goto _L23
_L7:
        if (o == null)
        {
            o = new zn();
        }
        yz1.a(o);
          goto _L23
_L8:
        m = yz1.g();
          goto _L23
_L9:
        if (k == null)
        {
            k = new zo();
        }
        yz1.a(k);
          goto _L23
_L10:
        i = yz1.e();
          goto _L23
_L11:
        g = yz1.d();
          goto _L23
_L12:
        h = yz1.d();
          goto _L23
_L13:
        n = yz1.g();
          goto _L23
_L14:
        p = yz1.f();
          goto _L23
_L15:
        q = yz1.h();
          goto _L23
_L16:
        if (r == null)
        {
            r = new zp();
        }
        yz1.a(r);
          goto _L23
_L17:
        d = yz1.c();
          goto _L23
_L18:
        s = yz1.g();
          goto _L23
_L19:
        int k1 = yz1.d();
        switch (k1)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
            t = k1;
            break;
        }
        continue; /* Loop/switch isn't completed */
_L20:
        int k2 = zl.b(yz1, 160);
        int ai[];
        int l1;
        if (u == null)
        {
            l1 = 0;
        } else
        {
            l1 = u.length;
        }
        ai = new int[k2 + l1];
        k2 = l1;
        if (l1 != 0)
        {
            System.arraycopy(u, 0, ai, 0, l1);
            k2 = l1;
        }
        for (; k2 < ai.length - 1; k2++)
        {
            ai[k2] = yz1.d();
            yz1.a();
        }

        ai[k2] = yz1.d();
        u = ai;
        continue; /* Loop/switch isn't completed */
_L21:
        int i3 = yz1.c(yz1.i());
        int i2 = yz1.o();
        int l2;
        for (l2 = 0; yz1.m() > 0; l2++)
        {
            yz1.d();
        }

        yz1.e(i2);
        int ai1[];
        if (u == null)
        {
            i2 = 0;
        } else
        {
            i2 = u.length;
        }
        ai1 = new int[l2 + i2];
        l2 = i2;
        if (i2 != 0)
        {
            System.arraycopy(u, 0, ai1, 0, i2);
            l2 = i2;
        }
        for (; l2 < ai1.length; l2++)
        {
            ai1[l2] = yz1.d();
        }

        u = ai1;
        yz1.d(i3);
        continue; /* Loop/switch isn't completed */
_L22:
        e = yz1.c();
        if (true) goto _L23; else goto _L24
_L24:
    }

    public zq c()
    {
        c = 0L;
        d = 0L;
        e = 0L;
        f = "";
        g = 0;
        h = 0;
        i = false;
        j = zr.c();
        k = null;
        l = zl.h;
        m = zl.h;
        n = zl.h;
        o = null;
        p = "";
        q = 0x2bf20L;
        r = null;
        s = zl.h;
        t = 0;
        u = zl.a;
        a = null;
        b = -1;
        return this;
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        if (obj != this) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(obj instanceof zq)) goto _L4; else goto _L3
_L3:
        obj = (zq)obj;
        flag = flag1;
        if (c != ((zq) (obj)).c) goto _L4; else goto _L5
_L5:
        flag = flag1;
        if (d != ((zq) (obj)).d) goto _L4; else goto _L6
_L6:
        flag = flag1;
        if (e != ((zq) (obj)).e) goto _L4; else goto _L7
_L7:
        if (f != null) goto _L9; else goto _L8
_L8:
        flag = flag1;
        if (((zq) (obj)).f != null) goto _L4; else goto _L10
_L10:
        flag = flag1;
        if (g != ((zq) (obj)).g) goto _L4; else goto _L11
_L11:
        flag = flag1;
        if (h != ((zq) (obj)).h) goto _L4; else goto _L12
_L12:
        flag = flag1;
        if (i != ((zq) (obj)).i) goto _L4; else goto _L13
_L13:
        flag = flag1;
        if (!zg.a(j, ((zq) (obj)).j)) goto _L4; else goto _L14
_L14:
        if (k != null) goto _L16; else goto _L15
_L15:
        flag = flag1;
        if (((zq) (obj)).k != null) goto _L4; else goto _L17
_L17:
        flag = flag1;
        if (!Arrays.equals(l, ((zq) (obj)).l)) goto _L4; else goto _L18
_L18:
        flag = flag1;
        if (!Arrays.equals(m, ((zq) (obj)).m)) goto _L4; else goto _L19
_L19:
        flag = flag1;
        if (!Arrays.equals(n, ((zq) (obj)).n)) goto _L4; else goto _L20
_L20:
        if (o != null) goto _L22; else goto _L21
_L21:
        flag = flag1;
        if (((zq) (obj)).o != null) goto _L4; else goto _L23
_L23:
        if (p != null) goto _L25; else goto _L24
_L24:
        flag = flag1;
        if (((zq) (obj)).p != null) goto _L4; else goto _L26
_L26:
        flag = flag1;
        if (q != ((zq) (obj)).q) goto _L4; else goto _L27
_L27:
        if (r != null) goto _L29; else goto _L28
_L28:
        flag = flag1;
        if (((zq) (obj)).r != null) goto _L4; else goto _L30
_L30:
        flag = flag1;
        if (!Arrays.equals(s, ((zq) (obj)).s)) goto _L4; else goto _L31
_L31:
        flag = flag1;
        if (t != ((zq) (obj)).t) goto _L4; else goto _L32
_L32:
        flag = flag1;
        if (!zg.a(u, ((zq) (obj)).u)) goto _L4; else goto _L33
_L33:
        if (a != null && !a.b())
        {
            break MISSING_BLOCK_LABEL_427;
        }
        if (((zq) (obj)).a == null)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (!((zq) (obj)).a.b()) goto _L4; else goto _L34
_L34:
        return true;
_L9:
        if (!f.equals(((zq) (obj)).f))
        {
            return false;
        }
          goto _L10
_L16:
        if (!k.equals(((zq) (obj)).k))
        {
            return false;
        }
          goto _L17
_L22:
        if (!o.equals(((zq) (obj)).o))
        {
            return false;
        }
          goto _L23
_L25:
        if (!p.equals(((zq) (obj)).p))
        {
            return false;
        }
          goto _L26
_L29:
        if (!r.equals(((zq) (obj)).r))
        {
            return false;
        }
          goto _L30
        return a.equals(((zq) (obj)).a);
          goto _L10
    }

    public int hashCode()
    {
        boolean flag = false;
        int k2 = getClass().getName().hashCode();
        int l2 = (int)(c ^ c >>> 32);
        int i3 = (int)(d ^ d >>> 32);
        int j3 = (int)(e ^ e >>> 32);
        int i1;
        char c1;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        int k3;
        int l3;
        int i4;
        int j4;
        int k4;
        int l4;
        int i5;
        int j5;
        int k5;
        int l5;
        if (f == null)
        {
            i1 = 0;
        } else
        {
            i1 = f.hashCode();
        }
        k3 = g;
        l3 = h;
        if (i)
        {
            c1 = '\u04CF';
        } else
        {
            c1 = '\u04D5';
        }
        i4 = zg.a(j);
        if (k == null)
        {
            j1 = 0;
        } else
        {
            j1 = k.hashCode();
        }
        j4 = Arrays.hashCode(l);
        k4 = Arrays.hashCode(m);
        l4 = Arrays.hashCode(n);
        if (o == null)
        {
            k1 = 0;
        } else
        {
            k1 = o.hashCode();
        }
        if (p == null)
        {
            l1 = 0;
        } else
        {
            l1 = p.hashCode();
        }
        i5 = (int)(q ^ q >>> 32);
        if (r == null)
        {
            i2 = 0;
        } else
        {
            i2 = r.hashCode();
        }
        j5 = Arrays.hashCode(s);
        k5 = t;
        l5 = zg.a(u);
        j2 = ((flag) ? 1 : 0);
        if (a != null)
        {
            if (a.b())
            {
                j2 = ((flag) ? 1 : 0);
            } else
            {
                j2 = a.hashCode();
            }
        }
        return ((((i2 + ((l1 + (k1 + ((((j1 + ((c1 + (((i1 + ((((k2 + 527) * 31 + l2) * 31 + i3) * 31 + j3) * 31) * 31 + k3) * 31 + l3) * 31) * 31 + i4) * 31) * 31 + j4) * 31 + k4) * 31 + l4) * 31) * 31) * 31 + i5) * 31) * 31 + j5) * 31 + k5) * 31 + l5) * 31 + j2;
    }

    public zi mergeFrom(yz yz1)
    {
        return a(yz1);
    }

    public void writeTo(za za1)
    {
        boolean flag = false;
        if (c != 0L)
        {
            za1.a(1, c);
        }
        if (!f.equals(""))
        {
            za1.a(2, f);
        }
        if (j != null && j.length > 0)
        {
            for (int i1 = 0; i1 < j.length; i1++)
            {
                zr zr1 = j[i1];
                if (zr1 != null)
                {
                    za1.a(3, zr1);
                }
            }

        }
        if (!Arrays.equals(l, zl.h))
        {
            za1.a(6, l);
        }
        if (o != null)
        {
            za1.a(7, o);
        }
        if (!Arrays.equals(m, zl.h))
        {
            za1.a(8, m);
        }
        if (k != null)
        {
            za1.a(9, k);
        }
        if (i)
        {
            za1.a(10, i);
        }
        if (g != 0)
        {
            za1.a(11, g);
        }
        if (h != 0)
        {
            za1.a(12, h);
        }
        if (!Arrays.equals(n, zl.h))
        {
            za1.a(13, n);
        }
        if (!p.equals(""))
        {
            za1.a(14, p);
        }
        if (q != 0x2bf20L)
        {
            za1.b(15, q);
        }
        if (r != null)
        {
            za1.a(16, r);
        }
        if (d != 0L)
        {
            za1.a(17, d);
        }
        if (!Arrays.equals(s, zl.h))
        {
            za1.a(18, s);
        }
        if (t != 0)
        {
            za1.a(19, t);
        }
        if (u != null && u.length > 0)
        {
            for (int j1 = ((flag) ? 1 : 0); j1 < u.length; j1++)
            {
                za1.a(20, u[j1]);
            }

        }
        if (e != 0L)
        {
            za1.a(21, e);
        }
        super.writeTo(za1);
    }
}
