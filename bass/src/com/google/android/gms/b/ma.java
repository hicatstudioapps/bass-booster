// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.a.d;
import com.google.android.gms.a.e;
import com.google.android.gms.a.f;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            mb, mf, md, mg, 
//            mc

public final class ma extends e
{

    private static final ma a = new ma();

    private ma()
    {
        super("com.google.android.gms.ads.AdOverlayCreatorImpl");
    }

    public static mc a(Activity activity)
    {
        try
        {
            if (b(activity))
            {
                zzb.zzaF("Using AdOverlay from the client jar.");
                return new zzd(activity);
            }
            activity = a.c(activity);
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            zzb.zzaH(activity.getMessage());
            return null;
        }
        return activity;
    }

    private static boolean b(Activity activity)
    {
        activity = activity.getIntent();
        if (!activity.hasExtra("com.google.android.gms.ads.internal.overlay.useClientJar"))
        {
            throw new mb("Ad overlay requires the useClientJar flag in intent extras.");
        } else
        {
            return activity.getBooleanExtra("com.google.android.gms.ads.internal.overlay.useClientJar", false);
        }
    }

    private mc c(Activity activity)
    {
        try
        {
            com.google.android.gms.a.a a1 = d.a(activity);
            activity = md.zzL(((mf)a(activity)).a(a1));
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            zzb.zzd("Could not create remote AdOverlay.", activity);
            return null;
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            zzb.zzd("Could not create remote AdOverlay.", activity);
            return null;
        }
        return activity;
    }

    protected mf a(IBinder ibinder)
    {
        return mg.a(ibinder);
    }

    protected Object b(IBinder ibinder)
    {
        return a(ibinder);
    }

}
