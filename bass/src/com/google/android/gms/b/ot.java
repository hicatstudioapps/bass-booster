// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zzk;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            ih, cp, pb, iu, 
//            db, dq, cs, os, 
//            ou, rk, je, pm, 
//            pk, pe, pd, mj, 
//            cr, qo, pq, rq, 
//            ov, pj, oz, oy, 
//            ql, ph, xx, pp, 
//            yb, pa, qt, pc, 
//            do, tx

public final class ot extends com.google.android.gms.ads.internal.request.zzj.zza
{

    private static final Object a = new Object();
    private static ot b;
    private final Context c;
    private final os d;
    private final cp e;
    private final ih f;

    ot(Context context, cp cp1, os os1)
    {
        c = context;
        d = os1;
        e = cp1;
        if (context.getApplicationContext() != null)
        {
            context = context.getApplicationContext();
        }
        f = new ih(context, new VersionInfoParcel(0x7e9e10, 0x7e9e10, true), cp1.a(), new pb(this), new iu());
    }

    private static AdResponseParcel a(Context context, ih ih1, cp cp1, os os1, AdRequestInfoParcel adrequestinfoparcel)
    {
        zzb.zzaF("Starting ad request from service.");
        db.a(context);
        dq dq1 = new dq(((Boolean)db.G.c()).booleanValue(), "load_ad", adrequestinfoparcel.zzqV.zztV);
        if (adrequestinfoparcel.versionCode > 10 && adrequestinfoparcel.zzGI != -1L)
        {
            dq1.a(dq1.a(adrequestinfoparcel.zzGI), new String[] {
                "cts"
            });
        }
        do do1 = dq1.a();
        Object obj;
        Object obj1;
        if (adrequestinfoparcel.versionCode >= 4 && adrequestinfoparcel.zzGx != null)
        {
            obj1 = adrequestinfoparcel.zzGx;
        } else
        {
            obj1 = null;
        }
        if (((Boolean)db.P.c()).booleanValue() && os1.i != null)
        {
            obj = obj1;
            if (obj1 == null)
            {
                obj = obj1;
                if (((Boolean)db.Q.c()).booleanValue())
                {
                    zzb.v("contentInfo is not present, but we'll still launch the app index task");
                    obj = new Bundle();
                }
            }
            String s;
            String s1;
            pe pe1;
            pk pk1;
            android.location.Location location;
            List list;
            String s2;
            pr pr;
            if (obj != null)
            {
                obj1 = rk.a(new ou(os1, context, adrequestinfoparcel, ((Bundle) (obj))));
            } else
            {
                obj1 = null;
            }
        } else
        {
            obj = obj1;
            obj1 = null;
        }
        os1.d.a();
        pk1 = zzp.zzbD().a(context);
        if (pk1.m == -1)
        {
            zzb.zzaF("Device is offline.");
            return new AdResponseParcel(2);
        }
        if (adrequestinfoparcel.versionCode >= 7)
        {
            s = adrequestinfoparcel.zzGF;
        } else
        {
            s = UUID.randomUUID().toString();
        }
        pe1 = new pe(s, adrequestinfoparcel.applicationInfo.packageName);
        if (adrequestinfoparcel.zzGq.extras != null)
        {
            s1 = adrequestinfoparcel.zzGq.extras.getString("_ad");
            if (s1 != null)
            {
                return pd.a(context, adrequestinfoparcel, s1);
            }
        }
        location = os1.d.a(250L);
        s1 = os1.e.a(context, adrequestinfoparcel.zzqP, adrequestinfoparcel.zzGr.packageName);
        list = os1.b.a(adrequestinfoparcel);
        s2 = os1.f.a(adrequestinfoparcel);
        pr = os1.g.a(context);
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_450;
        }
        zzb.v("Waiting for app index fetching task.");
        ((Future) (obj1)).get(((Long)db.R.c()).longValue(), TimeUnit.MILLISECONDS);
        zzb.v("App index fetching task completed.");
        break MISSING_BLOCK_LABEL_450;
        obj1;
_L2:
        zzb.zzd("Failed to fetch app index signal", ((Throwable) (obj1)));
          goto _L1
        obj1;
        zzb.zzaF("Timed out waiting for app index fetching task");
_L1:
        obj = pd.a(context, adrequestinfoparcel, pk1, pr, location, cp1, s1, s2, list, ((Bundle) (obj)));
        if (adrequestinfoparcel.versionCode < 7)
        {
            try
            {
                ((JSONObject) (obj)).put("request_id", s);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1) { }
        }
        if (obj == null)
        {
            return new AdResponseParcel(0);
        }
        obj = ((JSONObject) (obj)).toString();
        dq1.a(do1, new String[] {
            "arc"
        });
        obj1 = dq1.a();
        if (((Boolean)db.c.c()).booleanValue())
        {
            rq.a.post(new ov(ih1, pe1, dq1, ((do) (obj1)), ((String) (obj))));
        } else
        {
            rq.a.post(new oy(context, adrequestinfoparcel, pe1, dq1, ((do) (obj1)), ((String) (obj)), cp1));
        }
        obj = (pj)pe1.b().get(10L, TimeUnit.SECONDS);
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_720;
        }
        ih1 = new AdResponseParcel(0);
        rq.a.post(new oz(os1, context, pe1, adrequestinfoparcel));
        return ih1;
        ih1;
        ih1 = new AdResponseParcel(0);
        rq.a.post(new oz(os1, context, pe1, adrequestinfoparcel));
        return ih1;
        if (((pj) (obj)).a() == -2)
        {
            break MISSING_BLOCK_LABEL_765;
        }
        ih1 = new AdResponseParcel(((pj) (obj)).a());
        rq.a.post(new oz(os1, context, pe1, adrequestinfoparcel));
        return ih1;
        if (dq1.e() != null)
        {
            dq1.a(dq1.e(), new String[] {
                "rur"
            });
        }
        ih1 = null;
        if (((pj) (obj)).f())
        {
            ih1 = os1.a.a(adrequestinfoparcel.zzGr.packageName);
        }
        obj1 = adrequestinfoparcel.zzqR.afmaVersion;
        s = ((pj) (obj)).d();
        if (((pj) (obj)).h())
        {
            cp1 = s1;
        } else
        {
            cp1 = null;
        }
        ih1 = a(adrequestinfoparcel, context, ((String) (obj1)), s, ((String) (ih1)), ((String) (cp1)), ((pj) (obj)), dq1, os1);
        if (((AdResponseParcel) (ih1)).zzGZ == 1)
        {
            os1.e.a(context, adrequestinfoparcel.zzGr.packageName);
        }
        dq1.a(do1, new String[] {
            "tts"
        });
        ih1.zzHb = dq1.c();
        rq.a.post(new oz(os1, context, pe1, adrequestinfoparcel));
        return ih1;
        ih1;
        rq.a.post(new oz(os1, context, pe1, adrequestinfoparcel));
        throw ih1;
        obj1;
          goto _L2
    }

    public static AdResponseParcel a(AdRequestInfoParcel adrequestinfoparcel, Context context, String s, String s1, String s2, String s3, pj pj1, dq dq1, 
            os os1)
    {
        HttpURLConnection httpurlconnection;
        ph ph1;
        int i;
        int j;
        do do1;
        byte abyte0[];
        long l;
        if (dq1 != null)
        {
            do1 = dq1.a();
        } else
        {
            do1 = null;
        }
        try
        {
            ph1 = new ph(adrequestinfoparcel);
            zzb.zzaF((new StringBuilder()).append("AdRequestServiceImpl: Sending request: ").append(s1).toString());
            adrequestinfoparcel = new URL(s1);
            l = zzp.zzbB().b();
        }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel)
        {
            zzb.zzaH((new StringBuilder()).append("Error while connecting to ad server: ").append(adrequestinfoparcel.getMessage()).toString());
            return new AdResponseParcel(2);
        }
        i = 0;
_L4:
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_82;
        }
        os1.h.a();
        httpurlconnection = (HttpURLConnection)adrequestinfoparcel.openConnection();
        zzp.zzbx().a(context, s, false, httpurlconnection);
        if (!TextUtils.isEmpty(s2))
        {
            httpurlconnection.addRequestProperty("x-afma-drt-cookie", s2);
        }
        if (!TextUtils.isEmpty(s3))
        {
            httpurlconnection.addRequestProperty("Authorization", (new StringBuilder()).append("Bearer ").append(s3).toString());
        }
        if (pj1 == null)
        {
            break MISSING_BLOCK_LABEL_220;
        }
        if (TextUtils.isEmpty(pj1.c()))
        {
            break MISSING_BLOCK_LABEL_220;
        }
        httpurlconnection.setDoOutput(true);
        abyte0 = pj1.c().getBytes();
        httpurlconnection.setFixedLengthStreamingMode(abyte0.length);
        s1 = new BufferedOutputStream(httpurlconnection.getOutputStream());
        s1.write(abyte0);
        yb.a(s1);
        j = httpurlconnection.getResponseCode();
        s1 = httpurlconnection.getHeaderFields();
        if (j < 200 || j >= 300) goto _L2; else goto _L1
_L1:
        adrequestinfoparcel = adrequestinfoparcel.toString();
        context = new InputStreamReader(httpurlconnection.getInputStream());
        s = zzp.zzbx().a(context);
        yb.a(context);
        a(((String) (adrequestinfoparcel)), ((Map) (s1)), s, j);
        ph1.a(adrequestinfoparcel, s1, s);
        if (dq1 == null)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        dq1.a(do1, new String[] {
            "ufe"
        });
        adrequestinfoparcel = ph1.a(l);
        httpurlconnection.disconnect();
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_346;
        }
        os1.h.b();
        return adrequestinfoparcel;
        adrequestinfoparcel;
        context = null;
_L6:
        yb.a(context);
        throw adrequestinfoparcel;
        adrequestinfoparcel;
        httpurlconnection.disconnect();
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_384;
        }
        os1.h.b();
        throw adrequestinfoparcel;
        adrequestinfoparcel;
        context = null;
_L5:
        yb.a(context);
        throw adrequestinfoparcel;
_L2:
        a(adrequestinfoparcel.toString(), ((Map) (s1)), null, j);
        if (j < 300 || j >= 400)
        {
            break MISSING_BLOCK_LABEL_569;
        }
        adrequestinfoparcel = httpurlconnection.getHeaderField("Location");
        if (!TextUtils.isEmpty(adrequestinfoparcel))
        {
            break MISSING_BLOCK_LABEL_511;
        }
        zzb.zzaH("No location header to follow redirect.");
        adrequestinfoparcel = new AdResponseParcel(0);
        httpurlconnection.disconnect();
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_509;
        }
        os1.h.b();
        return adrequestinfoparcel;
        adrequestinfoparcel = new URL(adrequestinfoparcel);
        i++;
        if (i <= 5)
        {
            break MISSING_BLOCK_LABEL_624;
        }
        zzb.zzaH("Too many redirects.");
        adrequestinfoparcel = new AdResponseParcel(0);
        httpurlconnection.disconnect();
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_567;
        }
        os1.h.b();
        return adrequestinfoparcel;
        zzb.zzaH((new StringBuilder()).append("Received error HTTP response code: ").append(j).toString());
        adrequestinfoparcel = new AdResponseParcel(0);
        httpurlconnection.disconnect();
        if (os1 == null)
        {
            break MISSING_BLOCK_LABEL_622;
        }
        os1.h.b();
        return adrequestinfoparcel;
        ph1.a(s1);
        httpurlconnection.disconnect();
        if (os1 == null) goto _L4; else goto _L3
_L3:
        os1.h.b();
          goto _L4
        adrequestinfoparcel;
          goto _L5
        adrequestinfoparcel;
        context = s1;
          goto _L6
    }

    public static ot a(Context context, cp cp1, os os1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        if (b != null)
        {
            break MISSING_BLOCK_LABEL_41;
        }
        Context context1 = context;
        if (context.getApplicationContext() != null)
        {
            context1 = context.getApplicationContext();
        }
        b = new ot(context1, cp1, os1);
        context = b;
        obj;
        JVM INSTR monitorexit ;
        return context;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    static tx a(String s, dq dq1, do do1)
    {
        return b(s, dq1, do1);
    }

    private static void a(String s, Map map, String s1, int i)
    {
        if (zzb.zzQ(2))
        {
            zzb.v((new StringBuilder()).append("Http Response: {\n  URL:\n    ").append(s).append("\n  Headers:").toString());
            if (map != null)
            {
                for (s = map.keySet().iterator(); s.hasNext();)
                {
                    Object obj = (String)s.next();
                    zzb.v((new StringBuilder()).append("    ").append(((String) (obj))).append(":").toString());
                    obj = ((List)map.get(obj)).iterator();
                    while (((Iterator) (obj)).hasNext()) 
                    {
                        String s2 = (String)((Iterator) (obj)).next();
                        zzb.v((new StringBuilder()).append("      ").append(s2).toString());
                    }
                }

            }
            zzb.v("  Body:");
            if (s1 != null)
            {
                for (int j = 0; j < Math.min(s1.length(), 0x186a0); j += 1000)
                {
                    zzb.v(s1.substring(j, Math.min(s1.length(), j + 1000)));
                }

            } else
            {
                zzb.v("    null");
            }
            zzb.v((new StringBuilder()).append("  Response Code:\n    ").append(i).append("\n}").toString());
        }
    }

    private static tx b(String s, dq dq1, do do1)
    {
        return new pa(dq1, do1, s);
    }

    public void zza(AdRequestInfoParcel adrequestinfoparcel, zzk zzk)
    {
        zzp.zzbA().a(c, adrequestinfoparcel.zzqR);
        rk.a(new pc(this, adrequestinfoparcel, zzk));
    }

    public AdResponseParcel zzd(AdRequestInfoParcel adrequestinfoparcel)
    {
        return a(c, f, e, d, adrequestinfoparcel);
    }

}
