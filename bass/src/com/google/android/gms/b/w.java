// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import com.google.android.gms.clearcut.a;
import com.google.android.gms.clearcut.c;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.api.o;
import com.google.android.gms.common.b;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.b:
//            v, x, ad, zi, 
//            af, db, cs, ag, 
//            ai, ae

public abstract class w extends v
{

    private static String A;
    private static String B;
    private static long C = 0L;
    private static af D;
    private static Random E = new Random();
    private static b F = com.google.android.gms.common.b.a();
    private static boolean G;
    static boolean e = false;
    protected static a f = null;
    protected static com.google.ads.afma.nano.NanoAdshieldEvent.AdShieldEvent g;
    protected static int h;
    protected static boolean i = false;
    protected static boolean j = false;
    protected static boolean k = false;
    protected static boolean l = false;
    private static Method m;
    private static Method n;
    private static Method o;
    private static Method p;
    private static Method q;
    private static Method r;
    private static Method s;
    private static Method t;
    private static Method u;
    private static Method v;
    private static Method w;
    private static Method x;
    private static Method y;
    private static String z;

    protected w(Context context, ad ad1, ae ae)
    {
        super(context, ad1, ae);
        g = new com.google.ads.afma.nano.NanoAdshieldEvent.AdShieldEvent();
        g.appId = context.getPackageName();
    }

    static String a()
    {
        if (z == null)
        {
            throw new x();
        } else
        {
            return z;
        }
    }

    static String a(Context context, ad ad1)
    {
        if (A != null)
        {
            return A;
        }
        if (p == null)
        {
            throw new x();
        }
        try
        {
            context = (ByteBuffer)p.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        if (context != null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        throw new x();
        A = ad1.a(context.array(), true);
        context = A;
        return context;
    }

    static ArrayList a(MotionEvent motionevent, DisplayMetrics displaymetrics)
    {
        if (q == null || motionevent == null)
        {
            throw new x();
        }
        try
        {
            motionevent = (ArrayList)q.invoke(null, new Object[] {
                motionevent, displaymetrics
            });
        }
        // Misplaced declaration of an exception variable
        catch (MotionEvent motionevent)
        {
            throw new x(motionevent);
        }
        // Misplaced declaration of an exception variable
        catch (MotionEvent motionevent)
        {
            throw new x(motionevent);
        }
        return motionevent;
    }

    protected static void a(int i1, int j1)
    {
        if (l && i && f != null)
        {
            c c1 = f.a(zi.toByteArray(g));
            c1.b(j1);
            c1.a(i1);
            c1.a(d);
        }
    }

    protected static void a(String s1, Context context, ad ad1)
    {
        boolean flag = true;
        com/google/android/gms/b/w;
        JVM INSTR monitorenter ;
        boolean flag1 = e;
        if (flag1) goto _L2; else goto _L1
_L1:
        D = new af(ad1, null);
        z = s1;
        com.google.android.gms.b.db.a(context);
        l(context);
        C = b().longValue();
        E = new Random();
        d = (new o(context)).a(a.c).b();
        F = com.google.android.gms.common.b.a();
        if (F.a(context) != 0)
        {
            flag = false;
        }
        G = flag;
        com.google.android.gms.b.db.a(context);
        i = ((Boolean)db.ay.c()).booleanValue();
        f = new a(context, "ADSHIELD", null, null);
_L5:
        e = true;
_L2:
        com/google/android/gms/b/w;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
        s1;
        continue; /* Loop/switch isn't completed */
        s1;
        if (true) goto _L2; else goto _L3
_L3:
        s1;
        if (true) goto _L5; else goto _L4
_L4:
    }

    static Long b()
    {
        if (m == null)
        {
            throw new x();
        }
        Long long1;
        try
        {
            long1 = (Long)m.invoke(null, new Object[0]);
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            throw new x(illegalaccessexception);
        }
        catch (InvocationTargetException invocationtargetexception)
        {
            throw new x(invocationtargetexception);
        }
        return long1;
    }

    static String b(Context context, ad ad1)
    {
        if (B != null)
        {
            return B;
        }
        if (s == null)
        {
            throw new x();
        }
        try
        {
            context = (ByteBuffer)s.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        if (context != null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        throw new x();
        B = ad1.a(context.array(), true);
        context = B;
        return context;
    }

    private static String b(byte abyte0[], String s1)
    {
        try
        {
            abyte0 = new String(D.a(abyte0, s1), "UTF-8");
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            throw new x(abyte0);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            throw new x(abyte0);
        }
        return abyte0;
    }

    static String c()
    {
        if (o == null)
        {
            throw new x();
        }
        String s1;
        try
        {
            s1 = (String)o.invoke(null, new Object[0]);
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            throw new x(illegalaccessexception);
        }
        catch (InvocationTargetException invocationtargetexception)
        {
            throw new x(invocationtargetexception);
        }
        return s1;
    }

    static Long d()
    {
        if (n == null)
        {
            throw new x();
        }
        Long long1;
        try
        {
            long1 = (Long)n.invoke(null, new Object[0]);
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            throw new x(illegalaccessexception);
        }
        catch (InvocationTargetException invocationtargetexception)
        {
            throw new x(invocationtargetexception);
        }
        return long1;
    }

    static String d(Context context)
    {
        if (r == null)
        {
            throw new x();
        }
        try
        {
            context = (String)r.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        if (context != null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        throw new x();
        return context;
    }

    static String e(Context context)
    {
        if (v == null)
        {
            throw new x();
        }
        try
        {
            context = (String)v.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        return context;
    }

    private void e()
    {
        if (l && f != null)
        {
            f.a(d, 100L, TimeUnit.MILLISECONDS);
            d.c();
        }
    }

    static Long f(Context context)
    {
        if (w == null)
        {
            throw new x();
        }
        try
        {
            context = (Long)w.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        return context;
    }

    static ArrayList g(Context context)
    {
        if (t == null)
        {
            throw new x();
        }
        try
        {
            context = (ArrayList)t.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_45;
        }
        if (context.size() == 2)
        {
            break MISSING_BLOCK_LABEL_73;
        }
        throw new x();
        return context;
    }

    static int[] h(Context context)
    {
        if (u == null)
        {
            throw new x();
        }
        try
        {
            context = (int[])u.invoke(null, new Object[] {
                context
            });
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        return context;
    }

    static int i(Context context)
    {
        if (x == null)
        {
            throw new x();
        }
        int i1;
        try
        {
            i1 = ((Integer)x.invoke(null, new Object[] {
                context
            })).intValue();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        return i1;
    }

    static int j(Context context)
    {
        if (y == null)
        {
            throw new x();
        }
        int i1;
        try
        {
            i1 = ((Integer)y.invoke(null, new Object[] {
                context
            })).intValue();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        return i1;
    }

    private void k(Context context)
    {
        if (G)
        {
            d.b();
            l = true;
            return;
        } else
        {
            l = false;
            return;
        }
    }

    private static void l(Context context)
    {
        File file;
        File file1;
        byte abyte0[];
        String s1;
        byte abyte1[];
        Class class1;
        Object obj;
        Class class2;
        Class class3;
        Class class4;
        Class class5;
        Class class6;
        Class class7;
        Class class8;
        Class class9;
        Class class10;
        Object obj1;
        try
        {
            abyte0 = D.a(com.google.android.gms.b.ai.a());
            abyte1 = D.a(abyte0, com.google.android.gms.b.ai.b());
            file1 = context.getCacheDir();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new x(context);
        }
        file = file1;
        if (file1 != null)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        file1 = context.getDir("dex", 0);
        file = file1;
        if (file1 != null)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        throw new x();
        file1 = File.createTempFile("ads", ".jar", file);
        obj = new FileOutputStream(file1);
        ((FileOutputStream) (obj)).write(abyte1, 0, abyte1.length);
        ((FileOutputStream) (obj)).close();
        obj1 = new DexClassLoader(file1.getAbsolutePath(), file.getAbsolutePath(), null, context.getClassLoader());
        context = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.k()));
        class1 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.y()));
        obj = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.s()));
        class2 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, com.google.android.gms.b.ai.o()));
        class3 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.A()));
        class4 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.m()));
        class5 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.w()));
        class6 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.u()));
        class7 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.i()));
        class8 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.g()));
        class9 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.e()));
        class10 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, ai.q()));
        obj1 = ((DexClassLoader) (obj1)).loadClass(b(abyte0, com.google.android.gms.b.ai.c()));
        m = context.getMethod(b(abyte0, ai.l()), new Class[0]);
        n = class1.getMethod(b(abyte0, ai.z()), new Class[0]);
        o = ((Class) (obj)).getMethod(b(abyte0, ai.t()), new Class[0]);
        p = class2.getMethod(b(abyte0, ai.p()), new Class[] {
            android/content/Context
        });
        q = class3.getMethod(b(abyte0, ai.B()), new Class[] {
            android/view/MotionEvent, android/util/DisplayMetrics
        });
        r = class4.getMethod(b(abyte0, com.google.android.gms.b.ai.n()), new Class[] {
            android/content/Context
        });
        s = class5.getMethod(b(abyte0, ai.x()), new Class[] {
            android/content/Context
        });
        t = class6.getMethod(b(abyte0, ai.v()), new Class[] {
            android/content/Context
        });
        u = class7.getMethod(b(abyte0, ai.j()), new Class[] {
            android/content/Context
        });
        v = class8.getMethod(b(abyte0, ai.h()), new Class[] {
            android/content/Context
        });
        w = class9.getMethod(b(abyte0, ai.f()), new Class[] {
            android/content/Context
        });
        x = class10.getMethod(b(abyte0, ai.r()), new Class[] {
            android/content/Context
        });
        y = ((Class) (obj1)).getMethod(b(abyte0, ai.d()), new Class[] {
            android/content/Context
        });
        context = file1.getName();
        file1.delete();
        (new File(file, context.replace(".jar", ".dex"))).delete();
        return;
        context;
        s1 = file1.getName();
        file1.delete();
        (new File(file, s1.replace(".jar", ".dex"))).delete();
        throw context;
    }

    protected void b(Context context)
    {
        k(context);
        h = E.nextInt();
        a(0, h);
        int ai1[];
        x x1;
        try
        {
            a(1, c());
            a(1, h);
        }
        catch (x x6) { }
        try
        {
            a(2, a());
            a(2, h);
        }
        catch (x x5) { }
        try
        {
            long l1 = b().longValue();
            a(25, l1);
            if (C != 0L)
            {
                a(17, l1 - C);
                a(23, C);
            }
            a(25, h);
        }
        catch (x x4) { }
        try
        {
            ArrayList arraylist = g(context);
            a(31, ((Long)arraylist.get(0)).longValue());
            a(32, ((Long)arraylist.get(1)).longValue());
            a(31, h);
        }
        catch (x x3) { }
        try
        {
            a(33, d().longValue());
            a(33, h);
        }
        catch (x x2) { }
        if (!j || !k) goto _L2; else goto _L1
_L1:
        a(27, h);
_L3:
        try
        {
            a(29, b(context, c));
            a(29, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            ai1 = h(context);
            a(5, ai1[0]);
            a(6, ai1[1]);
            a(5, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(12, i(context));
            a(12, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(3, j(context));
            a(3, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(34, e(context));
            a(34, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(35, f(context).longValue());
            a(35, h);
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        e();
        return;
_L2:
        try
        {
            a(27, a(context, c));
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
          goto _L3
        context;
        return;
        if (true) goto _L1; else goto _L4
_L4:
    }

    protected void c(Context context)
    {
        k(context);
        h = E.nextInt();
        ArrayList arraylist;
        x x1;
        try
        {
            a(2, a());
        }
        catch (x x4) { }
        try
        {
            a(1, c());
        }
        catch (x x3) { }
        try
        {
            a(25, b().longValue());
        }
        catch (x x2) { }
        a(0, h);
        try
        {
            arraylist = a(a, b);
            a(14, ((Long)arraylist.get(0)).longValue());
            a(15, ((Long)arraylist.get(1)).longValue());
            if (arraylist.size() >= 3)
            {
                a(16, ((Long)arraylist.get(2)).longValue());
            }
            a(14, h);
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(34, e(context));
        }
        // Misplaced declaration of an exception variable
        catch (x x1) { }
        try
        {
            a(35, f(context).longValue());
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        e();
        return;
        context;
    }

}
