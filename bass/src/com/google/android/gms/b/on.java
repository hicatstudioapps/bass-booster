// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import com.google.android.gms.ads.internal.formats.zza;
import com.google.android.gms.ads.internal.formats.zze;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            ok, oe, th, ea

public class on
    implements ok
{

    private final boolean a;
    private final boolean b;

    public on(boolean flag, boolean flag1)
    {
        a = flag;
        b = flag1;
    }

    public com.google.android.gms.ads.internal.formats.zzh.zza a(oe oe1, JSONObject jsonobject)
    {
        return b(oe1, jsonobject);
    }

    public zze b(oe oe1, JSONObject jsonobject)
    {
        Object obj = oe1.a(jsonobject, "images", true, a, b);
        th th1 = oe1.a(jsonobject, "secondary_image", false, a);
        oe1 = oe1.b(jsonobject);
        ArrayList arraylist = new ArrayList();
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); arraylist.add(((th)((Iterator) (obj)).next()).get())) { }
        return new zze(jsonobject.getString("headline"), arraylist, jsonobject.getString("body"), (ea)th1.get(), jsonobject.getString("call_to_action"), jsonobject.getString("advertiser"), (zza)oe1.get(), new Bundle());
    }
}
