// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            tu, rq, rt, yd, 
//            ug, ta, uf, qt, 
//            dp, dj, gx, dg, 
//            dq, tv, gw, ab, 
//            ue, ly, do, ap, 
//            tt

class ud extends WebView
    implements android.view.ViewTreeObserver.OnGlobalLayoutListener, DownloadListener, tu
{

    private Map A;
    private final WindowManager B = (WindowManager)getContext().getSystemService("window");
    private final uf a;
    private final Object b = new Object();
    private final ab c;
    private final VersionInfoParcel d;
    private final com.google.android.gms.ads.internal.zzd e;
    private tv f;
    private zzd g;
    private AdSizeParcel h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    private Boolean m;
    private int n;
    private boolean o;
    private String p;
    private do q;
    private do r;
    private do s;
    private dp t;
    private zzd u;
    private ta v;
    private int w;
    private int x;
    private int y;
    private int z;

    protected ud(uf uf1, AdSizeParcel adsizeparcel, boolean flag, boolean flag1, ab ab1, VersionInfoParcel versioninfoparcel, dq dq1, 
            com.google.android.gms.ads.internal.zzd zzd1)
    {
        super(uf1);
        o = true;
        p = "";
        w = -1;
        x = -1;
        y = -1;
        z = -1;
        a = uf1;
        h = adsizeparcel;
        k = flag;
        n = -1;
        c = ab1;
        d = versioninfoparcel;
        e = zzd1;
        setBackgroundColor(0);
        adsizeparcel = getSettings();
        adsizeparcel.setAllowFileAccess(false);
        adsizeparcel.setJavaScriptEnabled(true);
        adsizeparcel.setSavePassword(false);
        adsizeparcel.setSupportMultipleWindows(true);
        adsizeparcel.setJavaScriptCanOpenWindowsAutomatically(true);
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            adsizeparcel.setMixedContentMode(0);
        }
        zzp.zzbx().a(uf1, versioninfoparcel.afmaVersion, adsizeparcel);
        zzp.zzbz().a(getContext(), adsizeparcel);
        setDownloadListener(this);
        D();
        if (yd.d())
        {
            addJavascriptInterface(new ug(this), "googleAdsJsInterface");
        }
        v = new ta(a.a(), this, null);
        a(dq1);
    }

    private void B()
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        Boolean boolean1;
        m = zzp.zzbA().i();
        boolean1 = m;
        if (boolean1 != null)
        {
            break MISSING_BLOCK_LABEL_41;
        }
        evaluateJavascript("(function(){})()", null);
        a(Boolean.valueOf(true));
_L1:
        obj;
        JVM INSTR monitorexit ;
        return;
        Object obj1;
        obj1;
        a(Boolean.valueOf(false));
          goto _L1
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
    }

    private void C()
    {
        dj.a(t.a(), q, new String[] {
            "aeh"
        });
    }

    private void D()
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (!k && !h.zztW) goto _L2; else goto _L1
_L1:
        if (android.os.Build.VERSION.SDK_INT >= 14) goto _L4; else goto _L3
_L3:
        zzb.zzaF("Disabling hardware acceleration on an overlay.");
        E();
_L5:
        return;
_L4:
        zzb.zzaF("Enabling hardware acceleration on an overlay.");
        F();
          goto _L5
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
_L2:
label0:
        {
            if (android.os.Build.VERSION.SDK_INT >= 18)
            {
                break label0;
            }
            zzb.zzaF("Disabling hardware acceleration on an AdView.");
            E();
        }
          goto _L5
        zzb.zzaF("Enabling hardware acceleration on an AdView.");
        F();
          goto _L5
    }

    private void E()
    {
        synchronized (b)
        {
            if (!l)
            {
                zzp.zzbz().c(this);
            }
            l = true;
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void F()
    {
        synchronized (b)
        {
            if (l)
            {
                zzp.zzbz().b(this);
            }
            l = false;
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void G()
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (A != null)
        {
            for (Iterator iterator = A.values().iterator(); iterator.hasNext(); ((gx)iterator.next()).a()) { }
        }
        break MISSING_BLOCK_LABEL_58;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        obj;
        JVM INSTR monitorexit ;
    }

    private void H()
    {
        dq dq1;
        if (t != null)
        {
            if ((dq1 = t.a()) != null && zzp.zzbA().e() != null)
            {
                zzp.zzbA().e().a(dq1);
                return;
            }
        }
    }

    static ud a(Context context, AdSizeParcel adsizeparcel, boolean flag, boolean flag1, ab ab1, VersionInfoParcel versioninfoparcel, dq dq1, com.google.android.gms.ads.internal.zzd zzd1)
    {
        return new ud(new uf(context), adsizeparcel, flag, flag1, ab1, versioninfoparcel, dq1, zzd1);
    }

    private void a(dq dq1)
    {
        H();
        t = new dp(new dq(true, "make_wv", h.zztV));
        t.a().a(dq1);
        r = dj.a(t.a());
        t.a("native:view_create", r);
        s = null;
        q = null;
    }

    static void a(ud ud1)
    {
        ud1.WebView.destroy();
    }

    Boolean A()
    {
        Boolean boolean1;
        synchronized (b)
        {
            boolean1 = m;
        }
        return boolean1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public WebView a()
    {
        return this;
    }

    public void a(int i1)
    {
        C();
        HashMap hashmap = new HashMap(2);
        hashmap.put("closetype", String.valueOf(i1));
        hashmap.put("version", d.afmaVersion);
        a("onhide", ((Map) (hashmap)));
    }

    public void a(Context context)
    {
        a.setBaseContext(context);
        v.a(a.a());
    }

    public void a(Context context, AdSizeParcel adsizeparcel, dq dq1)
    {
        synchronized (b)
        {
            v.b();
            a(context);
            g = null;
            h = adsizeparcel;
            k = false;
            i = false;
            p = "";
            n = -1;
            zzp.zzbz().b(this);
            loadUrl("about:blank");
            f.f();
            setOnTouchListener(null);
            setOnClickListener(null);
            o = true;
            a(dq1);
        }
        return;
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
    }

    public void a(AdSizeParcel adsizeparcel)
    {
        synchronized (b)
        {
            h = adsizeparcel;
            requestLayout();
        }
        return;
        adsizeparcel;
        obj;
        JVM INSTR monitorexit ;
        throw adsizeparcel;
    }

    public void a(zzd zzd1)
    {
        synchronized (b)
        {
            g = zzd1;
        }
        return;
        zzd1;
        obj;
        JVM INSTR monitorexit ;
        throw zzd1;
    }

    public void a(ap ap, boolean flag)
    {
        HashMap hashmap = new HashMap();
        if (flag)
        {
            ap = "1";
        } else
        {
            ap = "0";
        }
        hashmap.put("isVisible", ap);
        a("onAdVisibilityChanged", ((Map) (hashmap)));
    }

    void a(Boolean boolean1)
    {
        m = boolean1;
        zzp.zzbA().a(boolean1);
    }

    public void a(String s1)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        super.loadUrl(s1);
_L1:
        return;
        s1;
        zzb.zzaH((new StringBuilder()).append("Could not call loadUrl. ").append(s1).toString());
          goto _L1
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    protected void a(String s1, ValueCallback valuecallback)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (q()) goto _L2; else goto _L1
_L1:
        evaluateJavascript(s1, valuecallback);
_L4:
        return;
_L2:
        zzb.zzaH("The webview is destroyed. Ignoring action.");
        if (valuecallback == null) goto _L4; else goto _L3
_L3:
        valuecallback.onReceiveValue(null);
          goto _L4
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public void a(String s1, String s2)
    {
        d((new StringBuilder()).append(s1).append("(").append(s2).append(");").toString());
    }

    public void a(String s1, Map map)
    {
        try
        {
            map = zzp.zzbx().a(map);
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            zzb.zzaH("Could not convert parameters to JSON.");
            return;
        }
        a(s1, ((JSONObject) (map)));
    }

    public void a(String s1, JSONObject jsonobject)
    {
        Object obj = jsonobject;
        if (jsonobject == null)
        {
            obj = new JSONObject();
        }
        jsonobject = ((JSONObject) (obj)).toString();
        obj = new StringBuilder();
        ((StringBuilder) (obj)).append("AFMA_ReceiveMessage('");
        ((StringBuilder) (obj)).append(s1);
        ((StringBuilder) (obj)).append("'");
        ((StringBuilder) (obj)).append(",");
        ((StringBuilder) (obj)).append(jsonobject);
        ((StringBuilder) (obj)).append(");");
        zzb.v((new StringBuilder()).append("Dispatching AFMA event: ").append(((StringBuilder) (obj)).toString()).toString());
        d(((StringBuilder) (obj)).toString());
    }

    public void a(boolean flag)
    {
        synchronized (b)
        {
            k = flag;
            D();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public View b()
    {
        return this;
    }

    public void b(int i1)
    {
        synchronized (b)
        {
            n = i1;
            if (g != null)
            {
                g.setRequestedOrientation(n);
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void b(zzd zzd1)
    {
        synchronized (b)
        {
            u = zzd1;
        }
        return;
        zzd1;
        obj;
        JVM INSTR monitorexit ;
        throw zzd1;
    }

    public void b(String s1)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        String s2;
        s2 = s1;
        if (s1 == null)
        {
            s2 = "";
        }
        p = s2;
        obj;
        JVM INSTR monitorexit ;
        return;
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public void b(String s1, JSONObject jsonobject)
    {
        JSONObject jsonobject1 = jsonobject;
        if (jsonobject == null)
        {
            jsonobject1 = new JSONObject();
        }
        a(s1, jsonobject1.toString());
    }

    public void b(boolean flag)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (g == null)
        {
            break MISSING_BLOCK_LABEL_32;
        }
        g.zza(f.b(), flag);
_L2:
        return;
        i = flag;
        if (true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void c()
    {
        C();
        HashMap hashmap = new HashMap(1);
        hashmap.put("version", d.afmaVersion);
        a("onhide", hashmap);
    }

    protected void c(String s1)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (q())
        {
            break MISSING_BLOCK_LABEL_22;
        }
        loadUrl(s1);
_L2:
        return;
        zzb.zzaH("The webview is destroyed. Ignoring action.");
        if (true) goto _L2; else goto _L1
_L1:
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public void c(boolean flag)
    {
        synchronized (b)
        {
            o = flag;
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void d()
    {
        if (q == null)
        {
            dj.a(t.a(), s, new String[] {
                "aes"
            });
            q = dj.a(t.a());
            t.a("native:view_show", q);
        }
        HashMap hashmap = new HashMap(1);
        hashmap.put("version", d.afmaVersion);
        a("onshow", hashmap);
    }

    protected void d(String s1)
    {
        if (yd.f())
        {
            if (A() == null)
            {
                B();
            }
            if (A().booleanValue())
            {
                a(s1, ((ValueCallback) (null)));
                return;
            } else
            {
                c((new StringBuilder()).append("javascript:").append(s1).toString());
                return;
            }
        } else
        {
            c((new StringBuilder()).append("javascript:").append(s1).toString());
            return;
        }
    }

    public void destroy()
    {
label0:
        {
            synchronized (b)
            {
                H();
                v.b();
                if (g != null)
                {
                    g.close();
                    g.onDestroy();
                    g = null;
                }
                f.f();
                if (!j)
                {
                    break label0;
                }
            }
            return;
        }
        zzp.zzbL().a(this);
        G();
        j = true;
        zzb.v("Initiating WebView self destruct sequence in 3...");
        f.d();
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public Activity e()
    {
        return a.a();
    }

    public void evaluateJavascript(String s1, ValueCallback valuecallback)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (!q())
        {
            break MISSING_BLOCK_LABEL_34;
        }
        zzb.zzaH("The webview is destroyed. Ignoring action.");
        if (valuecallback == null)
        {
            break MISSING_BLOCK_LABEL_31;
        }
        valuecallback.onReceiveValue(null);
        obj;
        JVM INSTR monitorexit ;
        return;
        super.evaluateJavascript(s1, valuecallback);
        obj;
        JVM INSTR monitorexit ;
        return;
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public Context f()
    {
        return a.b();
    }

    public com.google.android.gms.ads.internal.zzd g()
    {
        return e;
    }

    public zzd h()
    {
        zzd zzd1;
        synchronized (b)
        {
            zzd1 = g;
        }
        return zzd1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public zzd i()
    {
        zzd zzd1;
        synchronized (b)
        {
            zzd1 = u;
        }
        return zzd1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public AdSizeParcel j()
    {
        AdSizeParcel adsizeparcel;
        synchronized (b)
        {
            adsizeparcel = h;
        }
        return adsizeparcel;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public tv k()
    {
        return f;
    }

    public boolean l()
    {
        return i;
    }

    public void loadData(String s1, String s2, String s3)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (q())
        {
            break MISSING_BLOCK_LABEL_27;
        }
        super.loadData(s1, s2, s3);
_L2:
        return;
        zzb.zzaH("The webview is destroyed. Ignoring action.");
        if (true) goto _L2; else goto _L1
_L1:
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public void loadDataWithBaseURL(String s1, String s2, String s3, String s4, String s5)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (q())
        {
            break MISSING_BLOCK_LABEL_31;
        }
        super.loadDataWithBaseURL(s1, s2, s3, s4, s5);
_L2:
        return;
        zzb.zzaH("The webview is destroyed. Ignoring action.");
        if (true) goto _L2; else goto _L1
_L1:
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
    }

    public void loadUrl(String s1)
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        boolean flag = q();
        if (flag)
        {
            break MISSING_BLOCK_LABEL_56;
        }
        super.loadUrl(s1);
_L1:
        obj;
        JVM INSTR monitorexit ;
        return;
        s1;
        zzb.zzaH((new StringBuilder()).append("Could not call loadUrl. ").append(s1).toString());
          goto _L1
        s1;
        obj;
        JVM INSTR monitorexit ;
        throw s1;
        zzb.zzaH("The webview is destroyed. Ignoring action.");
          goto _L1
    }

    public ab m()
    {
        return c;
    }

    public VersionInfoParcel n()
    {
        return d;
    }

    public boolean o()
    {
        boolean flag;
        synchronized (b)
        {
            flag = k;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void onAttachedToWindow()
    {
        synchronized (b)
        {
            super.onAttachedToWindow();
            if (!q())
            {
                v.c();
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void onDetachedFromWindow()
    {
        synchronized (b)
        {
            if (!q())
            {
                v.d();
            }
            super.onDetachedFromWindow();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void onDownloadStart(String s1, String s2, String s3, String s4, long l1)
    {
        try
        {
            s2 = new Intent("android.intent.action.VIEW");
            s2.setDataAndType(Uri.parse(s1), s4);
            zzp.zzbx().a(getContext(), s2);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s2)
        {
            zzb.zzaF((new StringBuilder()).append("Couldn't find an Activity to view url/mimetype: ").append(s1).append(" / ").append(s4).toString());
        }
    }

    protected void onDraw(Canvas canvas)
    {
        while (q() || android.os.Build.VERSION.SDK_INT == 21 && canvas.isHardwareAccelerated() && !isAttachedToWindow()) 
        {
            return;
        }
        super.onDraw(canvas);
    }

    public void onGlobalLayout()
    {
        boolean flag = z();
        zzd zzd1 = h();
        if (zzd1 != null && flag)
        {
            zzd1.zzff();
        }
    }

    protected void onMeasure(int i1, int j1)
    {
        int l1;
label0:
        {
            l1 = 0x7fffffff;
            synchronized (b)
            {
                if (!q())
                {
                    break label0;
                }
                setMeasuredDimension(0, 0);
            }
            return;
        }
        if (!isInEditMode() && !k && !h.zztY && !h.zztZ)
        {
            break MISSING_BLOCK_LABEL_83;
        }
        super.onMeasure(i1, j1);
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        if (!h.zztW)
        {
            break MISSING_BLOCK_LABEL_134;
        }
        DisplayMetrics displaymetrics = new DisplayMetrics();
        B.getDefaultDisplay().getMetrics(displaymetrics);
        setMeasuredDimension(displaymetrics.widthPixels, displaymetrics.heightPixels);
        obj;
        JVM INSTR monitorexit ;
        return;
        int k1;
        int i2;
        int j2;
        int k2;
        k2 = android.view.View.MeasureSpec.getMode(i1);
        k1 = android.view.View.MeasureSpec.getSize(i1);
        j2 = android.view.View.MeasureSpec.getMode(j1);
        i2 = android.view.View.MeasureSpec.getSize(j1);
        break MISSING_BLOCK_LABEL_158;
_L4:
        if (h.widthPixels <= i1 && h.heightPixels <= j1)
        {
            break MISSING_BLOCK_LABEL_326;
        }
        float f1 = a.getResources().getDisplayMetrics().density;
        zzb.zzaH((new StringBuilder()).append("Not enough space to show ad. Needs ").append((int)((float)h.widthPixels / f1)).append("x").append((int)((float)h.heightPixels / f1)).append(" dp, but only has ").append((int)((float)k1 / f1)).append("x").append((int)((float)i2 / f1)).append(" dp.").toString());
        if (getVisibility() != 8)
        {
            setVisibility(4);
        }
        setMeasuredDimension(0, 0);
_L2:
        obj;
        JVM INSTR monitorexit ;
        return;
        if (getVisibility() != 8)
        {
            setVisibility(0);
        }
        setMeasuredDimension(h.widthPixels, h.heightPixels);
        if (true) goto _L2; else goto _L1
_L1:
        if (k2 != 0x80000000 && k2 != 0x40000000)
        {
            i1 = 0x7fffffff;
        } else
        {
            i1 = k1;
        }
        if (j2 != 0x80000000)
        {
            j1 = l1;
            if (j2 != 0x40000000)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        j1 = i2;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onPause()
    {
        if (!q()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!yd.a()) goto _L1; else goto _L3
_L3:
        super.onPause();
        return;
        Exception exception;
        exception;
        zzb.zzb("Could not pause webview.", exception);
        return;
    }

    public void onResume()
    {
        if (!q()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!yd.a()) goto _L1; else goto _L3
_L3:
        super.onResume();
        return;
        Exception exception;
        exception;
        zzb.zzb("Could not resume webview.", exception);
        return;
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (c != null)
        {
            c.a(motionevent);
        }
        if (q())
        {
            return false;
        } else
        {
            return super.onTouchEvent(motionevent);
        }
    }

    public int p()
    {
        int i1;
        synchronized (b)
        {
            i1 = n;
        }
        return i1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean q()
    {
        boolean flag;
        synchronized (b)
        {
            flag = j;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void r()
    {
        synchronized (b)
        {
            zzb.v("Destroying WebView!");
            rq.a.post(new ue(this));
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean s()
    {
        boolean flag;
        synchronized (b)
        {
            dj.a(t.a(), q, new String[] {
                "aebb"
            });
            flag = o;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void setWebViewClient(WebViewClient webviewclient)
    {
        super.setWebViewClient(webviewclient);
        if (webviewclient instanceof tv)
        {
            f = (tv)webviewclient;
        }
    }

    public void stopLoading()
    {
        if (q())
        {
            return;
        }
        try
        {
            super.stopLoading();
            return;
        }
        catch (Exception exception)
        {
            zzb.zzb("Could not stop loading webview.", exception);
        }
    }

    public String t()
    {
        String s1;
        synchronized (b)
        {
            s1 = p;
        }
        return s1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public tt u()
    {
        return null;
    }

    public do v()
    {
        return s;
    }

    public dp w()
    {
        return t;
    }

    public void x()
    {
        v.a();
    }

    public void y()
    {
        if (s == null)
        {
            s = dj.a(t.a());
            t.a("native:view_load", s);
        }
    }

    public boolean z()
    {
        if (k().b())
        {
            DisplayMetrics displaymetrics = zzp.zzbx().a(B);
            int k1 = zzl.zzcN().zzb(displaymetrics, displaymetrics.widthPixels);
            int l1 = zzl.zzcN().zzb(displaymetrics, displaymetrics.heightPixels);
            Activity activity = e();
            int i1;
            int j1;
            if (activity == null || activity.getWindow() == null)
            {
                j1 = l1;
                i1 = k1;
            } else
            {
                int ai[] = zzp.zzbx().a(activity);
                i1 = zzl.zzcN().zzb(displaymetrics, ai[0]);
                j1 = zzl.zzcN().zzb(displaymetrics, ai[1]);
            }
            if (x != k1 || w != l1 || y != i1 || z != j1)
            {
                boolean flag;
                if (x != k1 || w != l1)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                x = k1;
                w = l1;
                y = i1;
                z = j1;
                (new ly(this)).a(k1, l1, i1, j1, displaymetrics.density, B.getDefaultDisplay().getRotation());
                return flag;
            }
        }
        return false;
    }
}
