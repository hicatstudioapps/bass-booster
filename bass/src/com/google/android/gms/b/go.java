// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            gd, tb, tu

public class go
    implements gd
{

    final HashMap a = new HashMap();

    public go()
    {
    }

    public Future a(String s)
    {
        tb tb1 = new tb();
        a.put(s, tb1);
        return tb1;
    }

    public void a(String s, String s1)
    {
        tb tb1;
        zzb.zzaF("Received ad from the cache.");
        tb1 = (tb)a.get(s);
        if (tb1 == null)
        {
            zzb.e("Could not find the ad request for the corresponding ad response.");
            return;
        }
        tb1.b(new JSONObject(s1));
        a.remove(s);
        return;
        s1;
        zzb.zzb("Failed constructing JSON object from value passed from javascript", s1);
        tb1.b(null);
        a.remove(s);
        return;
        s1;
        a.remove(s);
        throw s1;
    }

    public void b(String s)
    {
        tb tb1 = (tb)a.get(s);
        if (tb1 == null)
        {
            zzb.e("Could not find the ad request for the corresponding ad response.");
            return;
        }
        if (!tb1.isDone())
        {
            tb1.cancel(true);
        }
        a.remove(s);
    }

    public void zza(tu tu, Map map)
    {
        a((String)map.get("request_id"), (String)map.get("fetched_ad"));
    }
}
