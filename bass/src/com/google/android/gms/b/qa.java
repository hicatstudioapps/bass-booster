// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;

// Referenced classes of package com.google.android.gms.b:
//            qy, qd, qg, pz, 
//            qq, ka, xx, qf, 
//            qb, qc

public class qa extends qy
    implements qd, qg
{

    private final qq a;
    private final Context b;
    private final pz c;
    private final qg d;
    private final Object e = new Object();
    private final String f;
    private final String g;
    private final String h;
    private int i;
    private int j;

    public qa(Context context, String s, String s1, String s2, qq qq1, pz pz1, qg qg1)
    {
        i = 0;
        j = 3;
        b = context;
        f = s;
        h = s1;
        g = s2;
        a = qq1;
        c = pz1;
        d = qg1;
    }

    static String a(qa qa1)
    {
        return qa1.g;
    }

    static String b(qa qa1)
    {
        return qa1.f;
    }

    private void b(long l)
    {
_L1:
label0:
        {
            synchronized (e)
            {
                if (i == 0)
                {
                    break label0;
                }
            }
            return;
        }
        if (a(l))
        {
            break MISSING_BLOCK_LABEL_35;
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        obj;
        JVM INSTR monitorexit ;
          goto _L1
    }

    static Context c(qa qa1)
    {
        return qa1.b;
    }

    static String d(qa qa1)
    {
        return qa1.h;
    }

    public void a()
    {
        c.b();
        com.google.android.gms.ads.internal.client.AdRequestParcel adrequestparcel = a.a.zzGq;
        ka ka1 = c.a();
        try
        {
            ka1.a(adrequestparcel, g);
            return;
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzd("Fail to load ad from adapter.", remoteexception);
        }
        a(f, 0);
    }

    public void a(int k)
    {
        a(f, 0);
    }

    public void a(String s)
    {
        synchronized (e)
        {
            i = 1;
            e.notify();
        }
        return;
        exception;
        s;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(String s, int k)
    {
        synchronized (e)
        {
            i = 2;
            j = k;
            e.notify();
        }
        return;
        exception;
        s;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected boolean a(long l)
    {
        l = 20000L - (zzp.zzbB().b() - l);
        if (l <= 0L)
        {
            return false;
        }
        try
        {
            e.wait(l);
        }
        catch (InterruptedException interruptedexception)
        {
            return false;
        }
        return true;
    }

    public void onStop()
    {
    }

    public void zzbp()
    {
        qf qf1;
        com.google.android.gms.ads.internal.client.AdRequestParcel adrequestparcel;
        ka ka1;
        if (c == null || c.b() == null || c.a() == null)
        {
            return;
        }
        qf1 = c.b();
        qf1.a(this);
        qf1.a(this);
        adrequestparcel = a.a.zzGq;
        ka1 = c.a();
        if (!ka1.g())
        {
            break MISSING_BLOCK_LABEL_135;
        }
        zza.zzLE.post(new qb(this, ka1, adrequestparcel));
_L1:
        b(zzp.zzbB().b());
        qf1.a(null);
        qf1.a(null);
        RemoteException remoteexception;
        if (i == 1)
        {
            d.a(f);
            return;
        } else
        {
            d.a(f, j);
            return;
        }
        try
        {
            zza.zzLE.post(new qc(this, ka1, adrequestparcel, qf1));
        }
        // Misplaced declaration of an exception variable
        catch (RemoteException remoteexception)
        {
            zzb.zzd("Fail to check if adapter is initialized.", remoteexception);
            a(f, 0);
        }
          goto _L1
    }
}
