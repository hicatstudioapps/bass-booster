// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;

// Referenced classes of package com.google.android.gms.b:
//            rl, rm, tb, ro, 
//            rn, rp, th

public final class rk
{

    private static final ExecutorService a = Executors.newFixedThreadPool(10, a("Default"));
    private static final ExecutorService b = Executors.newFixedThreadPool(5, a("Loader"));

    public static th a(int i, Runnable runnable)
    {
        if (i == 1)
        {
            return a(b, ((Callable) (new rl(runnable))));
        } else
        {
            return a(a, ((Callable) (new rm(runnable))));
        }
    }

    public static th a(Runnable runnable)
    {
        return a(0, runnable);
    }

    public static th a(Callable callable)
    {
        return a(a, callable);
    }

    public static th a(ExecutorService executorservice, Callable callable)
    {
        tb tb1 = new tb();
        try
        {
            tb1.b(new ro(tb1, executorservice.submit(new rn(tb1, callable))));
        }
        // Misplaced declaration of an exception variable
        catch (ExecutorService executorservice)
        {
            zzb.zzd("Thread execution is rejected.", executorservice);
            tb1.cancel(true);
            return tb1;
        }
        return tb1;
    }

    private static ThreadFactory a(String s)
    {
        return new rp(s);
    }

}
