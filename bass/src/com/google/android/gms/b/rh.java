// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

// Referenced classes of package com.google.android.gms.b:
//            ri, rb, rj

final class rh extends ri
{

    final Context a;
    final rj b;

    rh(Context context, rj rj1)
    {
        a = context;
        b = rj1;
        super(null);
    }

    public void zzbp()
    {
        SharedPreferences sharedpreferences = rb.a(a);
        Bundle bundle = new Bundle();
        bundle.putBoolean("content_url_opted_out", sharedpreferences.getBoolean("content_url_opted_out", true));
        if (b != null)
        {
            b.a(bundle);
        }
    }
}
