// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Process;
import com.google.android.gms.ads.internal.zzp;
import java.util.concurrent.Callable;

// Referenced classes of package com.google.android.gms.b:
//            tb, qt

final class rn
    implements Runnable
{

    final tb a;
    final Callable b;

    rn(tb tb1, Callable callable)
    {
        a = tb1;
        b = callable;
        super();
    }

    public void run()
    {
        try
        {
            Process.setThreadPriority(10);
            a.b(b.call());
            return;
        }
        catch (Exception exception)
        {
            zzp.zzbA().a(exception, true);
        }
        a.cancel(true);
    }
}
