// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.RemoteException;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            qa, ka, qf

class qc
    implements Runnable
{

    final ka a;
    final AdRequestParcel b;
    final qf c;
    final qa d;

    qc(qa qa1, ka ka1, AdRequestParcel adrequestparcel, qf qf)
    {
        d = qa1;
        a = ka1;
        b = adrequestparcel;
        c = qf;
        super();
    }

    public void run()
    {
        try
        {
            a.a(com.google.android.gms.a.d.a(qa.c(d)), b, com.google.android.gms.b.qa.d(d), c, qa.a(d));
            return;
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzd((new StringBuilder()).append("Fail to initialize adapter ").append(qa.b(d)).toString(), remoteexception);
        }
        d.a(qa.b(d), 0);
    }
}
