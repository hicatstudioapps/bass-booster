// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.overlay.AdLauncherIntentInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, gs, rq, gr, 
//            lo, rt, tu, tv, 
//            gl

public final class gq
    implements gd
{

    private final gl a;
    private final zze b;
    private final lo c;

    public gq(gl gl1, zze zze1, lo lo1)
    {
        a = gl1;
        b = zze1;
        c = lo1;
    }

    private static void a(Context context, Map map)
    {
        if (TextUtils.isEmpty((String)map.get("u")))
        {
            zzb.zzaH("Destination url cannot be empty.");
            return;
        }
        map = (new gs()).a(context, map);
        try
        {
            zzp.zzbx().a(context, map);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzaH(context.getMessage());
        }
    }

    private static void a(tu tu1, Map map)
    {
        map = (String)map.get("u");
        if (TextUtils.isEmpty(map))
        {
            zzb.zzaH("Destination url cannot be empty.");
            return;
        } else
        {
            (new gr(tu1, map)).zzgX();
            return;
        }
    }

    private void a(boolean flag)
    {
        if (c != null)
        {
            c.a(flag);
        }
    }

    private static boolean a(Map map)
    {
        return "1".equals(map.get("custom_close"));
    }

    private static int b(Map map)
    {
        map = (String)map.get("o");
        if (map != null)
        {
            if ("p".equalsIgnoreCase(map))
            {
                return zzp.zzbz().b();
            }
            if ("l".equalsIgnoreCase(map))
            {
                return zzp.zzbz().a();
            }
            if ("c".equalsIgnoreCase(map))
            {
                return zzp.zzbz().c();
            }
        }
        return -1;
    }

    public void zza(tu tu1, Map map)
    {
        String s = (String)map.get("a");
        if (s == null)
        {
            zzb.zzaH("Action missing from an open GMSG.");
        } else
        {
            if (b != null && !b.zzbg())
            {
                b.zzp((String)map.get("u"));
                return;
            }
            tv tv1 = tu1.k();
            if ("expand".equalsIgnoreCase(s))
            {
                if (tu1.o())
                {
                    zzb.zzaH("Cannot expand WebView that is already expanded.");
                    return;
                } else
                {
                    a(false);
                    tv1.a(a(map), b(map));
                    return;
                }
            }
            if ("webapp".equalsIgnoreCase(s))
            {
                tu1 = (String)map.get("u");
                a(false);
                if (tu1 != null)
                {
                    tv1.a(a(map), b(map), tu1);
                    return;
                } else
                {
                    tv1.a(a(map), b(map), (String)map.get("html"), (String)map.get("baseurl"));
                    return;
                }
            }
            if ("in_app_purchase".equalsIgnoreCase(s))
            {
                tu1 = (String)map.get("product_id");
                map = (String)map.get("report_urls");
                if (a != null)
                {
                    if (map != null && !map.isEmpty())
                    {
                        map = map.split(" ");
                        a.zza(tu1, new ArrayList(Arrays.asList(map)));
                        return;
                    } else
                    {
                        a.zza(tu1, new ArrayList());
                        return;
                    }
                }
            } else
            {
                if ("app".equalsIgnoreCase(s) && "true".equalsIgnoreCase((String)map.get("play_store")))
                {
                    a(tu1, map);
                    return;
                }
                if ("app".equalsIgnoreCase(s) && "true".equalsIgnoreCase((String)map.get("system_browser")))
                {
                    a(tu1.getContext(), map);
                    return;
                }
                a(true);
                tu1.m();
                s = (String)map.get("u");
                if (!TextUtils.isEmpty(s))
                {
                    tu1 = zzp.zzbx().a(tu1, s);
                } else
                {
                    tu1 = s;
                }
                tv1.a(new AdLauncherIntentInfoParcel((String)map.get("i"), tu1, (String)map.get("m"), (String)map.get("p"), (String)map.get("c"), (String)map.get("f"), (String)map.get("e")));
                return;
            }
        }
    }
}
