// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.ae;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

// Referenced classes of package com.google.android.gms.b:
//            wz, yt, xa, wr, 
//            xg, xd, wo, wi, 
//            wm, wd, wj, wn, 
//            wk, wy, vq

public class wc
    implements wz
{

    private final xa a;
    private final Lock b;
    private final Context c;
    private final b d;
    private ConnectionResult e;
    private int f;
    private int g;
    private boolean h;
    private int i;
    private final Bundle j = new Bundle();
    private final Set k = new HashSet();
    private yt l;
    private int m;
    private boolean n;
    private boolean o;
    private ae p;
    private boolean q;
    private boolean r;
    private final com.google.android.gms.common.internal.h s;
    private final Map t;
    private final g u;
    private ArrayList v;

    public wc(xa xa1, com.google.android.gms.common.internal.h h1, Map map, b b1, g g1, Lock lock, Context context)
    {
        g = 0;
        h = false;
        v = new ArrayList();
        a = xa1;
        s = h1;
        t = map;
        d = b1;
        u = g1;
        b = lock;
        c = context;
    }

    static Context a(wc wc1)
    {
        return wc1.c;
    }

    static void a(wc wc1, ConnectionResult connectionresult)
    {
        wc1.d(connectionresult);
    }

    static void a(wc wc1, ConnectionResult connectionresult, a a1, int i1)
    {
        wc1.b(connectionresult, a1, i1);
    }

    static void a(wc wc1, ResolveAccountResponse resolveaccountresponse)
    {
        wc1.a(resolveaccountresponse);
    }

    private void a(ConnectionResult connectionresult)
    {
        if (!b(2))
        {
            return;
        }
        if (connectionresult.b())
        {
            h();
            return;
        }
        if (c(connectionresult))
        {
            j();
            h();
            return;
        } else
        {
            d(connectionresult);
            return;
        }
    }

    private void a(ResolveAccountResponse resolveaccountresponse)
    {
        if (!b(0))
        {
            return;
        }
        ConnectionResult connectionresult = resolveaccountresponse.b();
        if (connectionresult.b())
        {
            p = resolveaccountresponse.a();
            o = true;
            q = resolveaccountresponse.c();
            r = resolveaccountresponse.d();
            e();
            return;
        }
        if (c(connectionresult))
        {
            j();
            e();
            return;
        } else
        {
            d(connectionresult);
            return;
        }
    }

    private void a(boolean flag)
    {
        if (l != null)
        {
            if (l.isConnected() && flag)
            {
                l.c();
            }
            l.disconnect();
            p = null;
        }
    }

    private boolean a(int i1, int j1, ConnectionResult connectionresult)
    {
        while (j1 == 1 && !b(connectionresult) || e != null && i1 >= f) 
        {
            return false;
        }
        return true;
    }

    static boolean a(wc wc1, int i1)
    {
        return wc1.b(i1);
    }

    static b b(wc wc1)
    {
        return wc1.d;
    }

    private void b(ConnectionResult connectionresult, a a1, int i1)
    {
        if (i1 != 2)
        {
            int j1 = a1.a().a();
            if (a(j1, i1, connectionresult))
            {
                e = connectionresult;
                f = j1;
            }
        }
        a.b.put(a1.c(), connectionresult);
    }

    private boolean b(int i1)
    {
        if (g != i1)
        {
            Log.i("GoogleApiClientConnecting", a.g.k());
            Log.wtf("GoogleApiClientConnecting", (new StringBuilder()).append("GoogleApiClient connecting is in step ").append(c(g)).append(" but received callback for step ").append(c(i1)).toString(), new Exception());
            d(new ConnectionResult(8, null));
            return false;
        } else
        {
            return true;
        }
    }

    static boolean b(wc wc1, ConnectionResult connectionresult)
    {
        return wc1.c(connectionresult);
    }

    private boolean b(ConnectionResult connectionresult)
    {
        while (connectionresult.a() || d.b(connectionresult.c()) != null) 
        {
            return true;
        }
        return false;
    }

    private String c(int i1)
    {
        switch (i1)
        {
        default:
            return "UNKNOWN";

        case 0: // '\0'
            return "STEP_GETTING_SERVICE_BINDINGS";

        case 1: // '\001'
            return "STEP_VALIDATING_ACCOUNT";

        case 2: // '\002'
            return "STEP_AUTHENTICATING";

        case 3: // '\003'
            return "STEP_GETTING_REMOTE_SERVICE";
        }
    }

    static Lock c(wc wc1)
    {
        return wc1.b;
    }

    static void c(wc wc1, ConnectionResult connectionresult)
    {
        wc1.a(connectionresult);
    }

    private boolean c(ConnectionResult connectionresult)
    {
        return m == 2 || m == 1 && !connectionresult.a();
    }

    static xa d(wc wc1)
    {
        return wc1.a;
    }

    private void d(ConnectionResult connectionresult)
    {
        k();
        boolean flag;
        if (!connectionresult.a())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        a(flag);
        a.a(connectionresult);
        if (!h)
        {
            a.h.a(connectionresult);
        }
        h = false;
    }

    private boolean d()
    {
        i = i - 1;
        if (i > 0)
        {
            return false;
        }
        if (i < 0)
        {
            Log.i("GoogleApiClientConnecting", a.g.k());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            d(new ConnectionResult(8, null));
            return false;
        }
        if (e != null)
        {
            a.f = f;
            d(e);
            return false;
        } else
        {
            return true;
        }
    }

    private void e()
    {
        if (i == 0)
        {
            if (n)
            {
                if (o)
                {
                    f();
                    return;
                }
            } else
            {
                h();
                return;
            }
        }
    }

    static boolean e(wc wc1)
    {
        return wc1.n;
    }

    static yt f(wc wc1)
    {
        return wc1.l;
    }

    private void f()
    {
        ArrayList arraylist = new ArrayList();
        g = 1;
        i = a.a.size();
        Iterator iterator = a.a.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            i i1 = (i)iterator.next();
            if (a.b.containsKey(i1))
            {
                if (d())
                {
                    g();
                }
            } else
            {
                arraylist.add(a.a.get(i1));
            }
        } while (true);
        if (!arraylist.isEmpty())
        {
            v.add(com.google.android.gms.b.xd.a().submit(new wo(this, arraylist)));
        }
    }

    static ae g(wc wc1)
    {
        return wc1.p;
    }

    private void g()
    {
        g = 2;
        a.g.d = l();
        v.add(com.google.android.gms.b.xd.a().submit(new wi(this, null)));
    }

    static Set h(wc wc1)
    {
        return wc1.l();
    }

    private void h()
    {
        ArrayList arraylist = new ArrayList();
        g = 3;
        i = a.a.size();
        Iterator iterator = a.a.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            i i1 = (i)iterator.next();
            if (a.b.containsKey(i1))
            {
                if (d())
                {
                    i();
                }
            } else
            {
                arraylist.add(a.a.get(i1));
            }
        } while (true);
        if (!arraylist.isEmpty())
        {
            v.add(com.google.android.gms.b.xd.a().submit(new wm(this, arraylist)));
        }
    }

    private void i()
    {
        a.e();
        com.google.android.gms.b.xd.a().execute(new wd(this));
        if (l != null)
        {
            if (q)
            {
                l.a(p, r);
            }
            a(false);
        }
        i i1;
        for (Iterator iterator = a.b.keySet().iterator(); iterator.hasNext(); ((h)a.a.get(i1)).disconnect())
        {
            i1 = (i)iterator.next();
        }

        Bundle bundle;
        if (j.isEmpty())
        {
            bundle = null;
        } else
        {
            bundle = j;
        }
        a.h.a(bundle);
        if (h)
        {
            h = false;
            b();
        }
    }

    static void i(wc wc1)
    {
        wc1.j();
    }

    private void j()
    {
        n = false;
        a.g.d = Collections.emptySet();
        Iterator iterator = k.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            i i1 = (i)iterator.next();
            if (!a.b.containsKey(i1))
            {
                a.b.put(i1, new ConnectionResult(17, null));
            }
        } while (true);
    }

    static void j(wc wc1)
    {
        wc1.h();
    }

    private void k()
    {
        for (Iterator iterator = v.iterator(); iterator.hasNext(); ((Future)iterator.next()).cancel(true)) { }
        v.clear();
    }

    static boolean k(wc wc1)
    {
        return wc1.d();
    }

    private Set l()
    {
        if (s == null)
        {
            return Collections.emptySet();
        }
        HashSet hashset = new HashSet(s.c());
        Map map = s.e();
        Iterator iterator = map.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            a a1 = (a)iterator.next();
            if (!a.b.containsKey(a1.c()))
            {
                hashset.addAll(((com.google.android.gms.common.internal.i)map.get(a1)).a);
            }
        } while (true);
        return hashset;
    }

    static void l(wc wc1)
    {
        wc1.e();
    }

    static void m(wc wc1)
    {
        wc1.g();
    }

    public vq a(vq vq)
    {
        a.g.a.add(vq);
        return vq;
    }

    public void a()
    {
        a.b.clear();
        h = false;
        n = false;
        e = null;
        g = 0;
        m = 2;
        o = false;
        q = false;
        HashMap hashmap = new HashMap();
        Iterator iterator = t.keySet().iterator();
        boolean flag = false;
        while (iterator.hasNext()) 
        {
            a a1 = (a)iterator.next();
            h h1 = (h)a.a.get(a1.c());
            int i1 = ((Integer)t.get(a1)).intValue();
            boolean flag1;
            if (a1.a().a() == 1)
            {
                flag1 = true;
            } else
            {
                flag1 = false;
            }
            if (h1.zzmn())
            {
                n = true;
                if (i1 < m)
                {
                    m = i1;
                }
                if (i1 != 0)
                {
                    k.add(a1.c());
                }
            }
            hashmap.put(h1, new wj(this, a1, i1));
            flag = flag1 | flag;
        }
        if (flag)
        {
            n = false;
        }
        if (n)
        {
            s.a(Integer.valueOf(a.g.l()));
            wn wn1 = new wn(this, null);
            l = (yt)u.a(c, a.g.a(), s, s.h(), wn1, wn1);
        }
        i = a.a.size();
        v.add(com.google.android.gms.b.xd.a().submit(new wk(this, hashmap)));
    }

    public void a(int i1)
    {
        d(new ConnectionResult(8, null));
    }

    public void a(Bundle bundle)
    {
        if (b(3))
        {
            if (bundle != null)
            {
                j.putAll(bundle);
            }
            if (d())
            {
                i();
                return;
            }
        }
    }

    public void a(ConnectionResult connectionresult, a a1, int i1)
    {
        if (b(3))
        {
            b(connectionresult, a1, i1);
            if (d())
            {
                i();
                return;
            }
        }
    }

    public vq b(vq vq)
    {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    public void b()
    {
        Iterator iterator = a.g.a.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            wy wy1 = (wy)iterator.next();
            if (wy1.d() != 1)
            {
                wy1.g();
                iterator.remove();
            }
        } while (true);
        if (e == null && !a.g.a.isEmpty())
        {
            h = true;
            return;
        } else
        {
            k();
            a(true);
            a.a(null);
            return;
        }
    }

    public void c()
    {
        h = false;
    }
}
