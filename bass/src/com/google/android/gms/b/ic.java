// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            db, cs

class ic
{

    private final Object a[];
    private boolean b;

    ic(AdRequestParcel adrequestparcel, String s)
    {
        a = a(adrequestparcel, s);
    }

    private static String a(Bundle bundle)
    {
        if (bundle == null)
        {
            return null;
        }
        StringBuilder stringbuilder = new StringBuilder();
        Collections.sort(new ArrayList(bundle.keySet()));
        Iterator iterator = bundle.keySet().iterator();
        while (iterator.hasNext()) 
        {
            Object obj = bundle.get((String)iterator.next());
            if (obj == null)
            {
                obj = "null";
            } else
            if (obj instanceof Bundle)
            {
                obj = a((Bundle)obj);
            } else
            {
                obj = obj.toString();
            }
            stringbuilder.append(((String) (obj)));
        }
        return stringbuilder.toString();
    }

    private static Object[] a(AdRequestParcel adrequestparcel, String s)
    {
        HashSet hashset = new HashSet(Arrays.asList(((String)db.af.c()).split(",")));
        ArrayList arraylist = new ArrayList();
        arraylist.add(s);
        if (hashset.contains("birthday"))
        {
            arraylist.add(Long.valueOf(adrequestparcel.zztq));
        }
        if (hashset.contains("extras"))
        {
            arraylist.add(a(adrequestparcel.extras));
        }
        if (hashset.contains("gender"))
        {
            arraylist.add(Integer.valueOf(adrequestparcel.zztr));
        }
        if (hashset.contains("keywords"))
        {
            if (adrequestparcel.zzts != null)
            {
                arraylist.add(adrequestparcel.zzts.toString());
            } else
            {
                arraylist.add(null);
            }
        }
        if (hashset.contains("isTestDevice"))
        {
            arraylist.add(Boolean.valueOf(adrequestparcel.zztt));
        }
        if (hashset.contains("tagForChildDirectedTreatment"))
        {
            arraylist.add(Integer.valueOf(adrequestparcel.zztu));
        }
        if (hashset.contains("manualImpressionsEnabled"))
        {
            arraylist.add(Boolean.valueOf(adrequestparcel.zztv));
        }
        if (hashset.contains("publisherProvidedId"))
        {
            arraylist.add(adrequestparcel.zztw);
        }
        if (hashset.contains("location"))
        {
            if (adrequestparcel.zzty != null)
            {
                arraylist.add(adrequestparcel.zzty.toString());
            } else
            {
                arraylist.add(null);
            }
        }
        if (hashset.contains("contentUrl"))
        {
            arraylist.add(adrequestparcel.zztz);
        }
        if (hashset.contains("networkExtras"))
        {
            arraylist.add(a(adrequestparcel.zztA));
        }
        if (hashset.contains("customTargeting"))
        {
            arraylist.add(a(adrequestparcel.zztB));
        }
        if (hashset.contains("categoryExclusions"))
        {
            if (adrequestparcel.zztC != null)
            {
                arraylist.add(adrequestparcel.zztC.toString());
            } else
            {
                arraylist.add(null);
            }
        }
        if (hashset.contains("requestAgent"))
        {
            arraylist.add(adrequestparcel.zztD);
        }
        if (hashset.contains("requestPackage"))
        {
            arraylist.add(adrequestparcel.zztE);
        }
        return arraylist.toArray();
    }

    void a()
    {
        b = true;
    }

    boolean b()
    {
        return b;
    }

    public boolean equals(Object obj)
    {
        if (!(obj instanceof ic))
        {
            return false;
        } else
        {
            obj = (ic)obj;
            return Arrays.equals(a, ((ic) (obj)).a);
        }
    }

    public int hashCode()
    {
        return Arrays.hashCode(a);
    }

    public String toString()
    {
        return (new StringBuilder()).append("[InterstitialAdPoolKey ").append(Arrays.toString(a)).append("]").toString();
    }
}
