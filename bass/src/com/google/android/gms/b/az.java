// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.view.View;
import com.google.android.gms.ads.internal.formats.zzh;
import java.lang.ref.WeakReference;

// Referenced classes of package com.google.android.gms.b:
//            bw, ba

public class az
    implements bw
{

    private WeakReference a;

    public az(zzh zzh1)
    {
        a = new WeakReference(zzh1);
    }

    public View a()
    {
        zzh zzh1 = (zzh)a.get();
        if (zzh1 != null)
        {
            return zzh1.zzdL();
        } else
        {
            return null;
        }
    }

    public boolean b()
    {
        return a.get() == null;
    }

    public bw c()
    {
        return new ba((zzh)a.get());
    }
}
