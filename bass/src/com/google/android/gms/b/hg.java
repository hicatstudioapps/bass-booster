// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            uu, vu, he

class hg
    implements Runnable
{

    final he a;
    private final uu b;
    private final vu c;
    private final Runnable d;

    public hg(he he, uu uu1, vu vu1, Runnable runnable)
    {
        a = he;
        super();
        b = uu1;
        c = vu1;
        d = runnable;
    }

    public void run()
    {
        if (b.g())
        {
            b.c("canceled-at-delivery");
        } else
        {
            if (c.a())
            {
                b.a(c.a);
            } else
            {
                b.b(c.c);
            }
            if (c.d)
            {
                b.b("intermediate-response");
            } else
            {
                b.c("done");
            }
            if (d != null)
            {
                d.run();
                return;
            }
        }
    }
}
