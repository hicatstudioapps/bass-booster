// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zza;
import com.google.android.gms.ads.internal.zzn;

// Referenced classes of package com.google.android.gms.b:
//            qq, ob, sd, oc, 
//            bi, ns, db, cs, 
//            yd, tu, oa, nz, 
//            ab, jx, ny, dq

public class nx
{

    public nx()
    {
    }

    public sd a(Context context, zza zza, qq qq1, ab ab, tu tu1, jx jx, ny ny, 
            dq dq)
    {
        AdResponseParcel adresponseparcel = qq1.b;
        if (adresponseparcel.zzGN)
        {
            context = new ob(context, qq1, jx, ny, dq);
        } else
        if (adresponseparcel.zztY)
        {
            if (zza instanceof zzn)
            {
                context = new oc(context, (zzn)zza, new bi(), qq1, ab, ny);
            } else
            {
                qq1 = (new StringBuilder()).append("Invalid NativeAdManager type. Found: ");
                if (zza != null)
                {
                    context = zza.getClass().getName();
                } else
                {
                    context = "null";
                }
                throw new IllegalArgumentException(qq1.append(context).append("; Required: NativeAdManager.").toString());
            }
        } else
        if (adresponseparcel.zzGT)
        {
            context = new ns(context, qq1, tu1, ny);
        } else
        if (((Boolean)db.U.c()).booleanValue() && yd.f() && !yd.g() && tu1.j().zztW)
        {
            context = new oa(context, qq1, tu1, ny);
        } else
        {
            context = new nz(context, qq1, tu1, ny);
        }
        zzb.zzaF((new StringBuilder()).append("AdRenderer: ").append(context.getClass().getName()).toString());
        context.zzfR();
        return context;
    }
}
