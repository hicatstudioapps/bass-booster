// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.support.v4.b.a;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class xu extends AbstractSet
{

    private final a a;

    public xu()
    {
        a = new a();
    }

    public xu(int i)
    {
        a = new a(i);
    }

    public xu(Collection collection)
    {
        this(collection.size());
        addAll(collection);
    }

    public boolean a(xu xu1)
    {
        int i = size();
        a.a(xu1.a);
        return size() > i;
    }

    public boolean add(Object obj)
    {
        if (a.containsKey(obj))
        {
            return false;
        } else
        {
            a.put(obj, obj);
            return true;
        }
    }

    public boolean addAll(Collection collection)
    {
        if (collection instanceof xu)
        {
            return a((xu)collection);
        } else
        {
            return super.addAll(collection);
        }
    }

    public void clear()
    {
        a.clear();
    }

    public boolean contains(Object obj)
    {
        return a.containsKey(obj);
    }

    public Iterator iterator()
    {
        return a.keySet().iterator();
    }

    public boolean remove(Object obj)
    {
        if (!a.containsKey(obj))
        {
            return false;
        } else
        {
            a.remove(obj);
            return true;
        }
    }

    public int size()
    {
        return a.size();
    }
}
