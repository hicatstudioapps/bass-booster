// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;

// Referenced classes of package com.google.android.gms.b:
//            nt, qq, jo, qp, 
//            ji, jg, nw, jr, 
//            db, cs, ju, jx, 
//            dq, ny

public class ob extends nt
{

    protected jo g;
    private jx h;
    private jg i;
    private ji j;
    private final dq k;

    ob(Context context, qq qq1, jx jx, ny ny, dq dq)
    {
        super(context, qq1, ny);
        h = jx;
        j = qq1.c;
        k = dq;
    }

    protected qp a(int l)
    {
        Object obj = e.a;
        com.google.android.gms.ads.internal.client.AdRequestParcel adrequestparcel = ((AdRequestInfoParcel) (obj)).zzGq;
        java.util.List list = f.zzAQ;
        java.util.List list1 = f.zzAR;
        java.util.List list2 = f.zzGP;
        int i1 = f.orientation;
        long l1 = f.zzAU;
        String s1 = ((AdRequestInfoParcel) (obj)).zzGt;
        boolean flag = f.zzGN;
        ka ka;
        String s;
        jk jk;
        ji ji1;
        if (g != null)
        {
            obj = g.b;
        } else
        {
            obj = null;
        }
        if (g != null)
        {
            ka = g.c;
        } else
        {
            ka = null;
        }
        if (g != null)
        {
            s = g.d;
        } else
        {
            s = com/google/ads/mediation/admob/AdMobAdapter.getName();
        }
        ji1 = j;
        if (g != null)
        {
            jk = g.e;
        } else
        {
            jk = null;
        }
        return new qp(adrequestparcel, null, list, l, list1, list2, i1, l1, s1, flag, ((jh) (obj)), ka, s, ji1, jk, f.zzGO, e.d, f.zzGM, e.f, f.zzGR, f.zzGS, e.h, null);
    }

    protected void a(long l)
    {
        synchronized (d)
        {
            i = b(l);
        }
        g = i.a(j.a);
        switch (g.a)
        {
        default:
            throw new nw((new StringBuilder()).append("Unexpected mediation result: ").append(g.a).toString(), 0);

        case 1: // '\001'
            throw new nw("No fill from any mediation ad networks.", 3);

        case 0: // '\0'
            return;
        }
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    jg b(long l)
    {
        if (j.j != -1)
        {
            return new jr(b, e.a, h, j, f.zztY, l, ((Long)db.ax.c()).longValue(), 2);
        } else
        {
            return new ju(b, e.a, h, j, f.zztY, l, ((Long)db.ax.c()).longValue(), k);
        }
    }

    public void onStop()
    {
        synchronized (d)
        {
            super.onStop();
            if (i != null)
            {
                i.a();
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
