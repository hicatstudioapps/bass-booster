// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.util.Arrays;

// Referenced classes of package com.google.android.gms.b:
//            za

final class zk
{

    final int a;
    final byte b[];

    zk(int i, byte abyte0[])
    {
        a = i;
        b = abyte0;
    }

    int a()
    {
        return 0 + za.f(a) + b.length;
    }

    void a(za za1)
    {
        za1.e(a);
        za1.d(b);
    }

    public boolean equals(Object obj)
    {
        if (obj != this)
        {
            if (!(obj instanceof zk))
            {
                return false;
            }
            obj = (zk)obj;
            if (a != ((zk) (obj)).a || !Arrays.equals(b, ((zk) (obj)).b))
            {
                return false;
            }
        }
        return true;
    }

    public int hashCode()
    {
        return (a + 527) * 31 + Arrays.hashCode(b);
    }
}
