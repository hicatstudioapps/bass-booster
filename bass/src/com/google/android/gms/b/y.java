// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.b:
//            w, e, aa, ah, 
//            x, z, ad, ae

public class y extends w
{

    private static AdvertisingIdClient m = null;
    private static CountDownLatch n = new CountDownLatch(1);
    private static volatile boolean o;
    private boolean p;

    protected y(Context context, ad ad, ae ae, boolean flag)
    {
        super(context, ad, ae);
        p = flag;
    }

    static AdvertisingIdClient a(AdvertisingIdClient advertisingidclient)
    {
        m = advertisingidclient;
        return advertisingidclient;
    }

    public static y a(String s, Context context, boolean flag)
    {
        e e1;
        e1 = new e();
        a(s, context, ((ad) (e1)));
        if (!flag) goto _L2; else goto _L1
_L1:
        com/google/android/gms/b/y;
        JVM INSTR monitorenter ;
        if (m == null)
        {
            (new Thread(new aa(context))).start();
        }
        com/google/android/gms/b/y;
        JVM INSTR monitorexit ;
_L2:
        return new y(context, e1, new ah(239), flag);
        s;
        com/google/android/gms/b/y;
        JVM INSTR monitorexit ;
        throw s;
    }

    static boolean a(boolean flag)
    {
        o = flag;
        return flag;
    }

    static AdvertisingIdClient f()
    {
        return m;
    }

    static CountDownLatch g()
    {
        return n;
    }

    protected void b(Context context)
    {
        super.b(context);
        if (o || !p)
        {
            a(24, d(context));
            a(24, h);
            return;
        }
        String s;
        context = e();
        s = context.a();
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_100;
        }
        long l;
        if (context.b())
        {
            l = 1L;
        } else
        {
            l = 0L;
        }
        try
        {
            a(28, l);
            a(26, 5L);
            a(24, s);
            a(28, h);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
    }

    z e()
    {
label0:
        {
            z z1;
            try
            {
                if (n.await(2L, TimeUnit.SECONDS))
                {
                    break label0;
                }
                z1 = new z(this, null, false);
            }
            catch (InterruptedException interruptedexception)
            {
                return new z(this, null, false);
            }
            return z1;
        }
        com/google/android/gms/b/y;
        JVM INSTR monitorenter ;
        Object obj;
        if (m != null)
        {
            break MISSING_BLOCK_LABEL_71;
        }
        obj = new z(this, null, false);
        com/google/android/gms/b/y;
        JVM INSTR monitorexit ;
        return ((z) (obj));
        Exception exception;
        exception;
        com/google/android/gms/b/y;
        JVM INSTR monitorexit ;
        throw exception;
        exception = m.getInfo();
        com/google/android/gms/b/y;
        JVM INSTR monitorexit ;
        return new z(this, a(exception.getId()), exception.isLimitAdTrackingEnabled());
    }

}
