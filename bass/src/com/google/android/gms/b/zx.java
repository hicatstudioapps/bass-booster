// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.io.FilterInputStream;
import java.io.InputStream;

// Referenced classes of package com.google.android.gms.b:
//            zv

class zx extends FilterInputStream
{

    private int a;

    private zx(InputStream inputstream)
    {
        super(inputstream);
        a = 0;
    }

    zx(InputStream inputstream, zv zv)
    {
        this(inputstream);
    }

    static int a(zx zx1)
    {
        return zx1.a;
    }

    public int read()
    {
        int i = super.read();
        if (i != -1)
        {
            a = a + 1;
        }
        return i;
    }

    public int read(byte abyte0[], int i, int j)
    {
        i = super.read(abyte0, i, j);
        if (i != -1)
        {
            a = a + i;
        }
        return i;
    }
}
