// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;

// Referenced classes of package com.google.android.gms.b:
//            px, jx

public class pw extends com.google.android.gms.ads.internal.reward.client.zzb.zza
{

    private final Context a;
    private final VersionInfoParcel b;
    private final px c;
    private final Object d = new Object();

    public pw(Context context, jx jx, VersionInfoParcel versioninfoparcel)
    {
        a = context;
        b = versioninfoparcel;
        c = new px(context, AdSizeParcel.zzcK(), jx, versioninfoparcel);
    }

    public void destroy()
    {
        synchronized (d)
        {
            c.destroy();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean isLoaded()
    {
        boolean flag;
        synchronized (d)
        {
            flag = c.g();
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void pause()
    {
        synchronized (d)
        {
            c.pause();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void resume()
    {
        synchronized (d)
        {
            c.resume();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void setUserId(String s)
    {
        synchronized (d)
        {
            c.b(s);
        }
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void show()
    {
        synchronized (d)
        {
            c.f();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zza(RewardedVideoAdRequestParcel rewardedvideoadrequestparcel)
    {
        synchronized (d)
        {
            c.a(rewardedvideoadrequestparcel);
        }
        return;
        rewardedvideoadrequestparcel;
        obj;
        JVM INSTR monitorexit ;
        throw rewardedvideoadrequestparcel;
    }

    public void zza(zzd zzd)
    {
        synchronized (d)
        {
            c.a(zzd);
        }
        return;
        zzd;
        obj;
        JVM INSTR monitorexit ;
        throw zzd;
    }
}
