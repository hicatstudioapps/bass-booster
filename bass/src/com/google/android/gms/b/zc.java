// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            zi, ze, zf, yz, 
//            zl, zk, zg, za

public abstract class zc extends zi
{

    protected ze a;

    public zc()
    {
    }

    protected int a()
    {
        int j = 0;
        int k;
        if (a != null)
        {
            int i = 0;
            do
            {
                k = i;
                if (j >= a.a())
                {
                    break;
                }
                i += a.b(j).a();
                j++;
            } while (true);
        } else
        {
            k = 0;
        }
        return k;
    }

    protected final boolean a(yz yz1, int i)
    {
        int j = yz1.o();
        if (!yz1.b(i))
        {
            return false;
        }
        int k = zl.b(i);
        zk zk1 = new zk(i, yz1.a(j, yz1.o() - j));
        yz1 = null;
        Object obj;
        if (a == null)
        {
            a = new ze();
        } else
        {
            yz1 = a.a(k);
        }
        obj = yz1;
        if (yz1 == null)
        {
            obj = new zf();
            a.a(k, ((zf) (obj)));
        }
        ((zf) (obj)).a(zk1);
        return true;
    }

    public zc b()
    {
        zc zc1 = (zc)super.clone();
        zg.a(this, zc1);
        return zc1;
    }

    public zi clone()
    {
        return b();
    }

    public Object clone()
    {
        return b();
    }

    public void writeTo(za za)
    {
        if (a != null)
        {
            int i = 0;
            while (i < a.a()) 
            {
                a.b(i).a(za);
                i++;
            }
        }
    }
}
