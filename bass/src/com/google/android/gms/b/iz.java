// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.b:
//            tn, iv, ja, jb, 
//            jc, tl, it

public class iz extends tn
{

    private final Object d = new Object();
    private it e;
    private boolean f;
    private int g;

    public iz(it it)
    {
        e = it;
        f = false;
        g = 0;
    }

    static it a(iz iz1)
    {
        return iz1.e;
    }

    public iv a()
    {
        iv iv1 = new iv(this);
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        a(((tm) (new ja(this, iv1))), ((tk) (new jb(this, iv1))));
        Exception exception;
        boolean flag;
        if (g >= 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag);
        g = g + 1;
        obj;
        JVM INSTR monitorexit ;
        return iv1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void b()
    {
        boolean flag = true;
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        Exception exception;
        if (g < 1)
        {
            flag = false;
        }
        av.a(flag);
        zzb.v("Releasing 1 reference for JS Engine");
        g = g - 1;
        d();
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void c()
    {
        boolean flag = true;
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        Exception exception;
        if (g < 0)
        {
            flag = false;
        }
        av.a(flag);
        zzb.v("Releasing root reference. JS Engine will be destroyed once other references are released.");
        f = true;
        d();
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void d()
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        Exception exception;
        boolean flag;
        if (g >= 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag);
        if (!f || g != 0)
        {
            break MISSING_BLOCK_LABEL_61;
        }
        zzb.v("No reference is left (including root). Cleaning up engine.");
        a(new jc(this), new tl());
_L2:
        obj;
        JVM INSTR monitorexit ;
        return;
        zzb.v("There are still references to the engine. Not destroying.");
        if (true) goto _L2; else goto _L1
_L1:
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
