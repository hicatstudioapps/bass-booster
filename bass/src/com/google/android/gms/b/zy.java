// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;

// Referenced classes of package com.google.android.gms.b:
//            aab, uu, zz

public class zy
    implements aab
{

    protected final HttpClient a;

    public zy(HttpClient httpclient)
    {
        a = httpclient;
    }

    private static void a(HttpEntityEnclosingRequestBase httpentityenclosingrequestbase, uu uu1)
    {
        uu1 = uu1.o();
        if (uu1 != null)
        {
            httpentityenclosingrequestbase.setEntity(new ByteArrayEntity(uu1));
        }
    }

    private static void a(HttpUriRequest httpurirequest, Map map)
    {
        String s;
        for (Iterator iterator = map.keySet().iterator(); iterator.hasNext(); httpurirequest.setHeader(s, (String)map.get(s)))
        {
            s = (String)iterator.next();
        }

    }

    static HttpUriRequest b(uu uu1, Map map)
    {
        switch (uu1.b())
        {
        default:
            throw new IllegalStateException("Unknown request method.");

        case -1: 
            map = uu1.k();
            if (map != null)
            {
                HttpPost httppost = new HttpPost(uu1.d());
                httppost.addHeader("Content-Type", uu1.j());
                httppost.setEntity(new ByteArrayEntity(map));
                return httppost;
            } else
            {
                return new HttpGet(uu1.d());
            }

        case 0: // '\0'
            return new HttpGet(uu1.d());

        case 3: // '\003'
            return new HttpDelete(uu1.d());

        case 1: // '\001'
            map = new HttpPost(uu1.d());
            map.addHeader("Content-Type", uu1.n());
            a(map, uu1);
            return map;

        case 2: // '\002'
            map = new HttpPut(uu1.d());
            map.addHeader("Content-Type", uu1.n());
            a(map, uu1);
            return map;

        case 4: // '\004'
            return new HttpHead(uu1.d());

        case 5: // '\005'
            return new HttpOptions(uu1.d());

        case 6: // '\006'
            return new HttpTrace(uu1.d());

        case 7: // '\007'
            map = new zz(uu1.d());
            map.addHeader("Content-Type", uu1.n());
            a(map, uu1);
            return map;
        }
    }

    public HttpResponse a(uu uu1, Map map)
    {
        HttpUriRequest httpurirequest = b(uu1, map);
        a(httpurirequest, map);
        a(httpurirequest, uu1.a());
        a(httpurirequest);
        map = httpurirequest.getParams();
        int i = uu1.r();
        HttpConnectionParams.setConnectionTimeout(map, 5000);
        HttpConnectionParams.setSoTimeout(map, i);
        return a.execute(httpurirequest);
    }

    protected void a(HttpUriRequest httpurirequest)
    {
    }
}
