// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;

// Referenced classes of package com.google.android.gms.b:
//            iu, iz, ii, rq, 
//            bm, iq, ir, it, 
//            bg, iv

public class ih
{

    private final Object a;
    private final Context b;
    private final String c;
    private final VersionInfoParcel d;
    private it e;
    private it f;
    private iz g;
    private int h;

    public ih(Context context, VersionInfoParcel versioninfoparcel, String s)
    {
        a = new Object();
        h = 1;
        c = s;
        b = context.getApplicationContext();
        d = versioninfoparcel;
        e = new iu();
        f = new iu();
    }

    public ih(Context context, VersionInfoParcel versioninfoparcel, String s, it it, it it1)
    {
        this(context, versioninfoparcel, s);
        e = it;
        f = it1;
    }

    static int a(ih ih1, int i)
    {
        ih1.h = i;
        return i;
    }

    static Context a(ih ih1)
    {
        return ih1.b;
    }

    static iz a(ih ih1, iz iz1)
    {
        ih1.g = iz1;
        return iz1;
    }

    static VersionInfoParcel b(ih ih1)
    {
        return ih1.d;
    }

    private iz c()
    {
        iz iz1 = new iz(f);
        rq.a(new ii(this, iz1));
        return iz1;
    }

    static Object c(ih ih1)
    {
        return ih1.a;
    }

    static it d(ih ih1)
    {
        return ih1.e;
    }

    static int e(ih ih1)
    {
        return ih1.h;
    }

    static String f(ih ih1)
    {
        return ih1.c;
    }

    static iz g(ih ih1)
    {
        return ih1.g;
    }

    protected bg a(Context context, VersionInfoParcel versioninfoparcel)
    {
        return new bm(context, versioninfoparcel, null);
    }

    protected iz a()
    {
        iz iz1 = c();
        iz1.a(new iq(this, iz1), new ir(this, iz1));
        return iz1;
    }

    public iv b()
    {
label0:
        {
            iv iv;
            synchronized (a)
            {
                if (g != null && g.f() != -1)
                {
                    break label0;
                }
                h = 2;
                g = a();
                iv = g.a();
            }
            return iv;
        }
        iv iv1;
        if (h != 0)
        {
            break MISSING_BLOCK_LABEL_74;
        }
        iv1 = g.a();
        obj;
        JVM INSTR monitorexit ;
        return iv1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        if (h != 1)
        {
            break MISSING_BLOCK_LABEL_104;
        }
        h = 2;
        a();
        exception = g.a();
        obj;
        JVM INSTR monitorexit ;
        return exception;
        if (h != 2)
        {
            break MISSING_BLOCK_LABEL_124;
        }
        exception = g.a();
        obj;
        JVM INSTR monitorexit ;
        return exception;
        exception = g.a();
        obj;
        JVM INSTR monitorexit ;
        return exception;
    }
}
