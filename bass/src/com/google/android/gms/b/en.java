// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.a.a;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            ek, ed, eb, ea

public class en extends NativeAppInstallAd
{

    private final ek a;
    private final List b;
    private final ed c;

    public en(ek ek1)
    {
        b = new ArrayList();
        a = ek1;
        ek1 = a.getImages();
        if (ek1 == null)
        {
            break MISSING_BLOCK_LABEL_93;
        }
        ek1 = ek1.iterator();
_L2:
        ea ea;
        do
        {
            if (!ek1.hasNext())
            {
                break MISSING_BLOCK_LABEL_93;
            }
            ea = a(ek1.next());
        } while (ea == null);
        b.add(new ed(ea));
        if (true) goto _L2; else goto _L1
_L1:
        ek1;
        zzb.zzb("Failed to get image.", ek1);
        ek1 = a.zzdD();
        if (ek1 == null) goto _L4; else goto _L3
_L3:
        ek1 = new ed(ek1);
_L6:
        c = ek1;
        return;
        ek1;
        zzb.zzb("Failed to get icon.", ek1);
_L4:
        ek1 = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    ea a(Object obj)
    {
        if (obj instanceof IBinder)
        {
            return eb.zzt((IBinder)obj);
        } else
        {
            return null;
        }
    }

    protected Object a()
    {
        return b();
    }

    protected a b()
    {
        a a1;
        try
        {
            a1 = a.zzdE();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to retrieve native ad engine.", remoteexception);
            return null;
        }
        return a1;
    }

    public void destroy()
    {
        try
        {
            a.destroy();
            return;
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to destroy", remoteexception);
        }
    }

    public CharSequence getBody()
    {
        String s;
        try
        {
            s = a.getBody();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get body.", remoteexception);
            return null;
        }
        return s;
    }

    public CharSequence getCallToAction()
    {
        String s;
        try
        {
            s = a.getCallToAction();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get call to action.", remoteexception);
            return null;
        }
        return s;
    }

    public Bundle getExtras()
    {
        Bundle bundle;
        try
        {
            bundle = a.getExtras();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get extras", remoteexception);
            return null;
        }
        return bundle;
    }

    public CharSequence getHeadline()
    {
        String s;
        try
        {
            s = a.getHeadline();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get headline.", remoteexception);
            return null;
        }
        return s;
    }

    public com.google.android.gms.ads.formats.NativeAd.Image getIcon()
    {
        return c;
    }

    public List getImages()
    {
        return b;
    }

    public CharSequence getPrice()
    {
        String s;
        try
        {
            s = a.getPrice();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get price.", remoteexception);
            return null;
        }
        return s;
    }

    public Double getStarRating()
    {
        double d;
        try
        {
            d = a.getStarRating();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get star rating.", remoteexception);
            return null;
        }
        if (d == -1D)
        {
            return null;
        } else
        {
            return Double.valueOf(d);
        }
    }

    public CharSequence getStore()
    {
        String s;
        try
        {
            s = a.getStore();
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzb("Failed to get store", remoteexception);
            return null;
        }
        return s;
    }
}
