// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

// Referenced classes of package com.google.android.gms.b:
//            yl, bd, uu, be, 
//            qk, xt, vu, de

public class dd extends Thread
{

    private static final boolean a;
    private final BlockingQueue b;
    private final BlockingQueue c;
    private final bd d;
    private final xt e;
    private volatile boolean f;

    public dd(BlockingQueue blockingqueue, BlockingQueue blockingqueue1, bd bd1, xt xt1)
    {
        f = false;
        b = blockingqueue;
        c = blockingqueue1;
        d = bd1;
        e = xt1;
    }

    static BlockingQueue a(dd dd1)
    {
        return dd1.c;
    }

    public void a()
    {
        f = true;
        interrupt();
    }

    public void run()
    {
        if (a)
        {
            yl.a("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        d.a();
_L1:
        Object obj;
label0:
        {
            do
            {
                try
                {
                    do
                    {
                        obj = (uu)b.take();
                        ((uu) (obj)).b("cache-queue-take");
                        if (!((uu) (obj)).g())
                        {
                            break label0;
                        }
                        ((uu) (obj)).c("cache-discard-canceled");
                    } while (true);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj) { }
            } while (!f);
            return;
        }
        be be1 = d.a(((uu) (obj)).e());
        if (be1 != null)
        {
            break MISSING_BLOCK_LABEL_110;
        }
        ((uu) (obj)).b("cache-miss");
        c.put(obj);
          goto _L1
label1:
        {
            if (!be1.a())
            {
                break label1;
            }
            ((uu) (obj)).b("cache-hit-expired");
            ((uu) (obj)).a(be1);
            c.put(obj);
        }
          goto _L1
        vu vu1;
label2:
        {
            ((uu) (obj)).b("cache-hit");
            vu1 = ((uu) (obj)).a(new qk(be1.a, be1.g));
            ((uu) (obj)).b("cache-hit-parsed");
            if (be1.b())
            {
                break label2;
            }
            e.a(((uu) (obj)), vu1);
        }
          goto _L1
        ((uu) (obj)).b("cache-hit-refresh-needed");
        ((uu) (obj)).a(be1);
        vu1.d = true;
        e.a(((uu) (obj)), vu1, new de(this, ((uu) (obj))));
          goto _L1
    }

    static 
    {
        a = yl.b;
    }
}
