// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zzc;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzn;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            ok, qq, qp, tc, 
//            oj, sl, tb, ol, 
//            of, bg, oh, es, 
//            fc, db, cs, bi, 
//            om, on, rq, og, 
//            oo, td, oi, ab, 
//            th

public class oe
    implements Callable
{

    private static final long a;
    private final Context b;
    private final sl c;
    private final zzn d;
    private final ab e;
    private final bi f;
    private final Object g = new Object();
    private final qq h;
    private boolean i;
    private int j;
    private List k;
    private JSONObject l;

    public oe(Context context, zzn zzn1, bi bi1, sl sl1, ab ab, qq qq1)
    {
        b = context;
        d = zzn1;
        c = sl1;
        f = bi1;
        h = qq1;
        e = ab;
        i = false;
        j = -2;
        k = null;
    }

    private com.google.android.gms.ads.internal.formats.zzh.zza a(bg bg1, ok ok1, JSONObject jsonobject)
    {
        if (b())
        {
            return null;
        }
        JSONObject jsonobject1 = jsonobject.getJSONObject("tracking_urls_and_actions");
        List list = b(jsonobject1, "impression_tracking_urls");
        if (list == null)
        {
            list = null;
        } else
        {
            list = Arrays.asList(list);
        }
        k = list;
        l = jsonobject1.optJSONObject("active_view");
        ok1 = ok1.a(this, jsonobject);
        if (ok1 == null)
        {
            zzb.e("Failed to retrieve ad assets.");
            return null;
        } else
        {
            ok1.zzb(new zzh(b, d, bg1, e, jsonobject, ok1, h.a.zzqR));
            return ok1;
        }
    }

    static zzn a(oe oe1)
    {
        return oe1.d;
    }

    private qp a(com.google.android.gms.ads.internal.formats.zzh.zza zza)
    {
        Object obj = g;
        obj;
        JVM INSTR monitorenter ;
        int j1 = j;
        int i1;
        i1 = j1;
        if (zza != null)
        {
            break MISSING_BLOCK_LABEL_34;
        }
        i1 = j1;
        if (j == -2)
        {
            i1 = 0;
        }
        obj;
        JVM INSTR monitorexit ;
        if (i1 != -2)
        {
            zza = null;
        }
        return new qp(h.a.zzGq, null, h.b.zzAQ, i1, h.b.zzAR, k, h.b.orientation, h.b.zzAU, h.a.zzGt, false, null, null, null, null, null, 0L, h.d, h.b.zzGM, h.f, h.g, h.b.zzGS, l, zza);
        zza;
        obj;
        JVM INSTR monitorexit ;
        throw zza;
    }

    private th a(JSONObject jsonobject, boolean flag, boolean flag1)
    {
        double d1;
        String s;
        if (flag)
        {
            s = jsonobject.getString("url");
        } else
        {
            s = jsonobject.optString("url");
        }
        d1 = jsonobject.optDouble("scale", 1.0D);
        if (TextUtils.isEmpty(s))
        {
            a(0, flag);
            return new tc(null);
        }
        if (flag1)
        {
            return new tc(new zzc(null, Uri.parse(s), d1));
        } else
        {
            return c.a(s, new oj(this, flag, d1, s));
        }
    }

    private Integer a(JSONObject jsonobject, String s)
    {
        int i1;
        try
        {
            jsonobject = jsonobject.getJSONObject(s);
            i1 = Color.rgb(jsonobject.getInt("r"), jsonobject.getInt("g"), jsonobject.getInt("b"));
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            return null;
        }
        return Integer.valueOf(i1);
    }

    static List a(List list)
    {
        return b(list);
    }

    private JSONObject a(bg bg1)
    {
        if (b())
        {
            return null;
        } else
        {
            tb tb1 = new tb();
            ol ol1 = new ol(this);
            of of1 = new of(this, bg1, ol1, tb1);
            ol1.a = of1;
            bg1.a("/nativeAdPreProcess", of1);
            bg1.a("google.afma.nativeAds.preProcessJsonGmsg", new JSONObject(h.b.body));
            return (JSONObject)tb1.get(a, TimeUnit.MILLISECONDS);
        }
    }

    private void a(com.google.android.gms.ads.internal.formats.zzh.zza zza, bg bg1)
    {
        if (!(zza instanceof zzf))
        {
            return;
        } else
        {
            Object obj = (zzf)zza;
            zza = new ol(this);
            obj = new oh(this, ((zzf) (obj)));
            zza.a = ((gd) (obj));
            bg1.a("/nativeAdCustomClick", ((gd) (obj)));
            return;
        }
    }

    private void a(es es1, String s)
    {
        fc fc1;
        try
        {
            fc1 = d.zzr(es1.getCustomTemplateId());
        }
        // Misplaced declaration of an exception variable
        catch (es es1)
        {
            zzb.zzd((new StringBuilder()).append("Failed to call onCustomClick for asset ").append(s).append(".").toString(), es1);
            return;
        }
        if (fc1 == null)
        {
            break MISSING_BLOCK_LABEL_26;
        }
        fc1.a(es1, s);
    }

    static void a(oe oe1, es es1, String s)
    {
        oe1.a(es1, s);
    }

    private static List b(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add((Drawable)com.google.android.gms.a.d.a(((zzc)list.next()).zzdC()))) { }
        return arraylist;
    }

    private String[] b(JSONObject jsonobject, String s)
    {
        jsonobject = jsonobject.optJSONArray(s);
        if (jsonobject == null)
        {
            return null;
        }
        s = new String[jsonobject.length()];
        for (int i1 = 0; i1 < jsonobject.length(); i1++)
        {
            s[i1] = jsonobject.getString(i1);
        }

        return s;
    }

    private bg c()
    {
        if (b())
        {
            return null;
        }
        String s = (String)db.ac.c();
        Object obj;
        if (h.b.zzDE.indexOf("https") == 0)
        {
            obj = "https:";
        } else
        {
            obj = "http:";
        }
        obj = (new StringBuilder()).append(((String) (obj))).append(s).toString();
        obj = (bg)f.a(b, h.a.zzqR, ((String) (obj)), e).get(a, TimeUnit.MILLISECONDS);
        ((bg) (obj)).a(d, d, d, d, false, null, null, null, null);
        return ((bg) (obj));
    }

    protected ok a(JSONObject jsonobject)
    {
        if (b())
        {
            return null;
        }
        String s = jsonobject.getString("template_id");
        boolean flag;
        boolean flag1;
        if (h.a.zzrj != null)
        {
            flag = h.a.zzrj.zzyc;
        } else
        {
            flag = false;
        }
        if (h.a.zzrj != null)
        {
            flag1 = h.a.zzrj.zzye;
        } else
        {
            flag1 = false;
        }
        if ("2".equals(s))
        {
            return new om(flag, flag1);
        }
        if ("1".equals(s))
        {
            return new on(flag, flag1);
        }
        if ("3".equals(s))
        {
            String s1 = jsonobject.getString("custom_template_id");
            tb tb1 = new tb();
            rq.a.post(new og(this, tb1, s1));
            if (tb1.get(a, TimeUnit.MILLISECONDS) != null)
            {
                return new oo(flag);
            }
            zzb.e((new StringBuilder()).append("No handler for custom template: ").append(jsonobject.getString("custom_template_id")).toString());
        } else
        {
            a(0);
        }
        return null;
    }

    public qp a()
    {
        Object obj;
        obj = c();
        Object obj2 = a(((bg) (obj)));
        obj2 = a(((bg) (obj)), a(((JSONObject) (obj2))), ((JSONObject) (obj2)));
        a(((com.google.android.gms.ads.internal.formats.zzh.zza) (obj2)), ((bg) (obj)));
        obj = a(((com.google.android.gms.ads.internal.formats.zzh.zza) (obj2)));
        return ((qp) (obj));
        Object obj1;
        obj1;
        zzb.zzd("Malformed native JSON response.", ((Throwable) (obj1)));
_L2:
        if (!i)
        {
            a(0);
        }
        return a(((com.google.android.gms.ads.internal.formats.zzh.zza) (null)));
        obj1;
        zzb.zzd("Timeout when loading native ad.", ((Throwable) (obj1)));
        continue; /* Loop/switch isn't completed */
        obj1;
        continue; /* Loop/switch isn't completed */
        obj1;
        continue; /* Loop/switch isn't completed */
        obj1;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public th a(JSONObject jsonobject, String s, boolean flag, boolean flag1)
    {
        if (flag)
        {
            jsonobject = jsonobject.getJSONObject(s);
        } else
        {
            jsonobject = jsonobject.optJSONObject(s);
        }
        s = jsonobject;
        if (jsonobject == null)
        {
            s = new JSONObject();
        }
        return a(((JSONObject) (s)), flag, flag1);
    }

    public List a(JSONObject jsonobject, String s, boolean flag, boolean flag1, boolean flag2)
    {
        ArrayList arraylist;
        if (flag)
        {
            jsonobject = jsonobject.getJSONArray(s);
        } else
        {
            jsonobject = jsonobject.optJSONArray(s);
        }
        arraylist = new ArrayList();
        if (jsonobject == null || jsonobject.length() == 0)
        {
            a(0, flag);
            return arraylist;
        }
        int i1;
        int j1;
        if (flag2)
        {
            i1 = jsonobject.length();
        } else
        {
            i1 = 1;
        }
        for (j1 = 0; j1 < i1; j1++)
        {
            JSONObject jsonobject1 = jsonobject.getJSONObject(j1);
            s = jsonobject1;
            if (jsonobject1 == null)
            {
                s = new JSONObject();
            }
            arraylist.add(a(((JSONObject) (s)), flag, flag1));
        }

        return arraylist;
    }

    public Future a(JSONObject jsonobject, String s, boolean flag)
    {
        s = jsonobject.getJSONObject(s);
        boolean flag1 = s.optBoolean("require", true);
        jsonobject = s;
        if (s == null)
        {
            jsonobject = new JSONObject();
        }
        return a(jsonobject, flag1, flag);
    }

    public void a(int i1)
    {
        synchronized (g)
        {
            i = true;
            j = i1;
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(int i1, boolean flag)
    {
        if (flag)
        {
            a(i1);
        }
    }

    public th b(JSONObject jsonobject)
    {
        JSONObject jsonobject1 = jsonobject.optJSONObject("attribution");
        if (jsonobject1 == null)
        {
            return new tc(null);
        }
        String s = jsonobject1.optString("text");
        int i1 = jsonobject1.optInt("text_size", -1);
        Integer integer = a(jsonobject1, "text_color");
        Integer integer1 = a(jsonobject1, "bg_color");
        int j1 = jsonobject1.optInt("animation_ms", 1000);
        int k1 = jsonobject1.optInt("presentation_ms", 4000);
        jsonobject = new ArrayList();
        if (jsonobject1.optJSONArray("images") != null)
        {
            jsonobject = a(jsonobject1, "images", false, false, true);
        } else
        {
            jsonobject.add(a(jsonobject1, "image", false, false));
        }
        return td.a(td.a(jsonobject), new oi(this, s, integer1, integer, i1, k1, j1));
    }

    public boolean b()
    {
        boolean flag;
        synchronized (g)
        {
            flag = i;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public Object call()
    {
        return a();
    }

    static 
    {
        a = TimeUnit.SECONDS.toMillis(60L);
    }
}
