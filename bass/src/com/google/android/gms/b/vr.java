// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.v;
import com.google.android.gms.common.api.w;
import com.google.android.gms.common.api.x;
import com.google.android.gms.common.api.y;
import com.google.android.gms.common.api.z;
import com.google.android.gms.common.internal.ah;
import com.google.android.gms.common.internal.av;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

// Referenced classes of package com.google.android.gms.b:
//            vs, xn

public abstract class vr extends v
{

    private final Object a = new Object();
    protected final vs b;
    private final CountDownLatch c = new CountDownLatch(1);
    private final ArrayList d = new ArrayList();
    private z e;
    private volatile y f;
    private volatile boolean g;
    private boolean h;
    private boolean i;
    private ah j;
    private Integer k;
    private volatile xn l;

    protected vr(Looper looper)
    {
        b = new vs(looper);
    }

    public static void b(y y1)
    {
        if (!(y1 instanceof x))
        {
            break MISSING_BLOCK_LABEL_16;
        }
        ((x)y1).a();
        return;
        RuntimeException runtimeexception;
        runtimeexception;
        Log.w("BasePendingResult", (new StringBuilder()).append("Unable to release ").append(y1).toString(), runtimeexception);
        return;
    }

    private void c(y y1)
    {
        f = y1;
        j = null;
        c.countDown();
        y1 = f.a();
        if (e != null)
        {
            b.a();
            if (!h)
            {
                b.a(e, i());
            }
        }
        for (Iterator iterator = d.iterator(); iterator.hasNext(); ((w)iterator.next()).a(y1)) { }
        d.clear();
    }

    private y i()
    {
        boolean flag = true;
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        y y1;
        if (g)
        {
            flag = false;
        }
        av.a(flag, "Result has already been consumed.");
        av.a(f(), "Result is not ready.");
        y1 = f;
        f = null;
        e = null;
        g = true;
        e();
        return y1;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public Integer a()
    {
        return k;
    }

    public final void a(w w1)
    {
        boolean flag1 = true;
        Object obj;
        boolean flag;
        if (!g)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag, "Result has already been consumed.");
        if (w1 != null)
        {
            flag = flag1;
        } else
        {
            flag = false;
        }
        av.b(flag, "Callback cannot be null.");
        obj = a;
        obj;
        JVM INSTR monitorenter ;
        if (!f())
        {
            break MISSING_BLOCK_LABEL_63;
        }
        w1.a(f.a());
_L2:
        return;
        d.add(w1);
        if (true) goto _L2; else goto _L1
_L1:
        w1;
        obj;
        JVM INSTR monitorexit ;
        throw w1;
    }

    public final void a(y y1)
    {
        boolean flag1;
label0:
        {
            flag1 = true;
            synchronized (a)
            {
                if (!i && !h)
                {
                    break label0;
                }
                b(y1);
            }
            return;
        }
        boolean flag;
        if (!f())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag, "Results have already been set");
        if (!g)
        {
            flag = flag1;
        } else
        {
            flag = false;
        }
        av.a(flag, "Result has already been consumed");
        c(y1);
        obj;
        JVM INSTR monitorexit ;
        return;
        y1;
        obj;
        JVM INSTR monitorexit ;
        throw y1;
    }

    public final void a(z z)
    {
        boolean flag;
        boolean flag1 = true;
        Object obj;
        if (!g)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag, "Result has already been consumed.");
        obj = a;
        obj;
        JVM INSTR monitorenter ;
        if (l == null)
        {
            flag = flag1;
        } else
        {
            flag = false;
        }
        av.a(flag, "Cannot set callbacks if then() has been called.");
        if (!h())
        {
            break MISSING_BLOCK_LABEL_51;
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        if (!f())
        {
            break MISSING_BLOCK_LABEL_78;
        }
        b.a(z, i());
_L1:
        obj;
        JVM INSTR monitorexit ;
        return;
        z;
        obj;
        JVM INSTR monitorexit ;
        throw z;
        e = z;
          goto _L1
    }

    protected abstract y b(Status status);

    public final void d(Status status)
    {
        synchronized (a)
        {
            if (!f())
            {
                a(b(status));
                i = true;
            }
        }
        return;
        status;
        obj;
        JVM INSTR monitorexit ;
        throw status;
    }

    protected void e()
    {
    }

    public final boolean f()
    {
        return c.getCount() == 0L;
    }

    public void g()
    {
label0:
        {
            synchronized (a)
            {
                if (!h && !g)
                {
                    break label0;
                }
            }
            return;
        }
        ah ah1 = j;
        if (ah1 == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        try
        {
            j.a();
        }
        catch (RemoteException remoteexception) { }
        b(f);
        e = null;
        h = true;
        c(b(Status.e));
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean h()
    {
        boolean flag;
        synchronized (a)
        {
            flag = h;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
