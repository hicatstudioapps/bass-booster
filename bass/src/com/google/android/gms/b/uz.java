// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.util.Log;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.clearcut.d;
import com.google.android.gms.clearcut.f;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.api.v;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.b:
//            vh, xz, vd, va, 
//            vf, vb, zq, zi, 
//            xx, vc

public class uz
    implements f
{

    private static final Object a = new Object();
    private static final vh b = new vh(null);
    private static final long c;
    private final xx d;
    private final vc e;
    private final Object f;
    private long g;
    private final long h;
    private ScheduledFuture i;
    private n j;
    private final Runnable k;

    public uz()
    {
        this(((xx) (new xz())), c, ((vc) (new vd())));
    }

    public uz(xx xx, long l, vc vc)
    {
        f = new Object();
        g = 0L;
        i = null;
        j = null;
        k = new va(this);
        d = xx;
        h = l;
        e = vc;
    }

    static vh a()
    {
        return b;
    }

    static n a(uz uz1, n n1)
    {
        uz1.j = n1;
        return n1;
    }

    static Object a(uz uz1)
    {
        return uz1.f;
    }

    static void a(LogEventParcelable logeventparcelable)
    {
        b(logeventparcelable);
    }

    static long b(uz uz1)
    {
        return uz1.g;
    }

    private vf b(n n1, LogEventParcelable logeventparcelable)
    {
        b.a();
        n1 = new vf(this, logeventparcelable, n1);
        n1.a(new vb(this));
        return n1;
    }

    private static void b(LogEventParcelable logeventparcelable)
    {
        if (logeventparcelable.f != null && logeventparcelable.e.l.length == 0)
        {
            logeventparcelable.e.l = logeventparcelable.f.a();
        }
        if (logeventparcelable.g != null && logeventparcelable.e.s.length == 0)
        {
            logeventparcelable.e.s = logeventparcelable.g.a();
        }
        logeventparcelable.c = zi.toByteArray(logeventparcelable.e);
    }

    static xx c(uz uz1)
    {
        return uz1.d;
    }

    static n d(uz uz1)
    {
        return uz1.j;
    }

    public v a(n n1, LogEventParcelable logeventparcelable)
    {
        b(logeventparcelable);
        return n1.a(b(n1, logeventparcelable));
    }

    public boolean a(n n1, long l, TimeUnit timeunit)
    {
        boolean flag;
        try
        {
            flag = b.a(l, timeunit);
        }
        // Misplaced declaration of an exception variable
        catch (n n1)
        {
            Log.e("ClearcutLoggerApiImpl", "flush interrupted");
            Thread.currentThread().interrupt();
            return false;
        }
        return flag;
    }

    static 
    {
        c = TimeUnit.MILLISECONDS.convert(2L, TimeUnit.MINUTES);
    }
}
