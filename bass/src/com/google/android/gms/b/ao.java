// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

// Referenced classes of package com.google.android.gms.b:
//            bf, qp, tu, bc, 
//            az, ap, ih, bw

public class ao
    implements bf
{

    private final Object a = new Object();
    private final WeakHashMap b = new WeakHashMap();
    private final ArrayList c = new ArrayList();
    private final Context d;
    private final VersionInfoParcel e;
    private final ih f;

    public ao(Context context, VersionInfoParcel versioninfoparcel, ih ih)
    {
        d = context.getApplicationContext();
        e = versioninfoparcel;
        f = ih;
    }

    public ap a(AdSizeParcel adsizeparcel, qp qp1)
    {
        return a(adsizeparcel, qp1, qp1.b.b());
    }

    public ap a(AdSizeParcel adsizeparcel, qp qp1, View view)
    {
        return a(adsizeparcel, qp1, ((bw) (new bc(view, qp1))));
    }

    public ap a(AdSizeParcel adsizeparcel, qp qp1, zzh zzh)
    {
        return a(adsizeparcel, qp1, ((bw) (new az(zzh))));
    }

    public ap a(AdSizeParcel adsizeparcel, qp qp1, bw bw)
    {
label0:
        {
            synchronized (a)
            {
                if (!a(qp1))
                {
                    break label0;
                }
                adsizeparcel = (ap)b.get(qp1);
            }
            return adsizeparcel;
        }
        adsizeparcel = new ap(d, adsizeparcel, qp1, e, bw, f);
        adsizeparcel.a(this);
        b.put(qp1, adsizeparcel);
        c.add(adsizeparcel);
        obj;
        JVM INSTR monitorexit ;
        return adsizeparcel;
        adsizeparcel;
        obj;
        JVM INSTR monitorexit ;
        throw adsizeparcel;
    }

    public void a(ap ap1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        if (!ap1.f())
        {
            c.remove(ap1);
            Iterator iterator = b.entrySet().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                if (((java.util.Map.Entry)iterator.next()).getValue() == ap1)
                {
                    iterator.remove();
                }
            } while (true);
        }
        break MISSING_BLOCK_LABEL_77;
        ap1;
        obj;
        JVM INSTR monitorexit ;
        throw ap1;
        obj;
        JVM INSTR monitorexit ;
    }

    public boolean a(qp qp1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        qp1 = (ap)b.get(qp1);
        if (qp1 == null) goto _L2; else goto _L1
_L1:
        if (!qp1.f()) goto _L2; else goto _L3
_L3:
        boolean flag = true;
_L5:
        obj;
        JVM INSTR monitorexit ;
        return flag;
        qp1;
        obj;
        JVM INSTR monitorexit ;
        throw qp1;
_L2:
        flag = false;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void b(qp qp1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        qp1 = (ap)b.get(qp1);
        if (qp1 == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        qp1.d();
        obj;
        JVM INSTR monitorexit ;
        return;
        qp1;
        obj;
        JVM INSTR monitorexit ;
        throw qp1;
    }

    public void c(qp qp1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        qp1 = (ap)b.get(qp1);
        if (qp1 == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        qp1.m();
        obj;
        JVM INSTR monitorexit ;
        return;
        qp1;
        obj;
        JVM INSTR monitorexit ;
        throw qp1;
    }

    public void d(qp qp1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        qp1 = (ap)b.get(qp1);
        if (qp1 == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        qp1.n();
        obj;
        JVM INSTR monitorexit ;
        return;
        qp1;
        obj;
        JVM INSTR monitorexit ;
        throw qp1;
    }

    public void e(qp qp1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        qp1 = (ap)b.get(qp1);
        if (qp1 == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        qp1.o();
        obj;
        JVM INSTR monitorexit ;
        return;
        qp1;
        obj;
        JVM INSTR monitorexit ;
        throw qp1;
    }
}
