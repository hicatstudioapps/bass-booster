// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.internal.av;
import java.util.concurrent.atomic.AtomicReference;

// Referenced classes of package com.google.android.gms.b:
//            vr, wy, wx

public abstract class vq extends vr
    implements wy
{

    private final i a;
    private AtomicReference c;

    protected vq(i j, n n1)
    {
        super(((n)av.a(n1, "GoogleApiClient must not be null")).a());
        c = new AtomicReference();
        a = (i)av.a(j);
    }

    private void a(RemoteException remoteexception)
    {
        c(new Status(8, remoteexception.getLocalizedMessage(), null));
    }

    public void a(wx wx1)
    {
        c.set(wx1);
    }

    protected abstract void a(h h);

    public final i b()
    {
        return a;
    }

    public final void b(h h)
    {
        try
        {
            a(h);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (h h)
        {
            a(h);
            throw h;
        }
        // Misplaced declaration of an exception variable
        catch (h h)
        {
            a(h);
        }
    }

    public void c()
    {
        a(((com.google.android.gms.common.api.z) (null)));
    }

    public final void c(Status status)
    {
        boolean flag;
        if (!status.e())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.b(flag, "Failed result must not be success");
        a(b(status));
    }

    public int d()
    {
        return 0;
    }

    protected void e()
    {
        wx wx1 = (wx)c.getAndSet(null);
        if (wx1 != null)
        {
            wx1.a(this);
        }
    }
}
