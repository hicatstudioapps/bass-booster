// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import android.os.IInterface;
import com.google.android.gms.a.a;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            ea

public interface ek
    extends IInterface
{

    public abstract void destroy();

    public abstract String getBody();

    public abstract String getCallToAction();

    public abstract Bundle getExtras();

    public abstract String getHeadline();

    public abstract List getImages();

    public abstract String getPrice();

    public abstract double getStarRating();

    public abstract String getStore();

    public abstract ea zzdD();

    public abstract a zzdE();
}
