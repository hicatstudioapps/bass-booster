// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.ab;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.av;
import com.google.android.gms.common.internal.w;
import com.google.android.gms.common.internal.x;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;

// Referenced classes of package com.google.android.gms.b:
//            xg, ws, wt, wu, 
//            wy, wv, xa, vx, 
//            xf, vq, xh, ww, 
//            xe, wx

public final class wr extends n
    implements xg
{

    final Queue a = new LinkedList();
    ww b;
    final Map c;
    Set d;
    final com.google.android.gms.common.internal.h e;
    final Map f;
    final g g;
    final Set h = Collections.newSetFromMap(new ConcurrentHashMap(16, 0.75F, 2));
    private final Lock i;
    private final w j;
    private xf k;
    private final int l;
    private final Context m;
    private final Looper n;
    private volatile boolean o;
    private long p;
    private long q;
    private final wu r;
    private final b s;
    private final Set t = Collections.newSetFromMap(new WeakHashMap());
    private ab u;
    private final ArrayList v;
    private Integer w;
    private final wx x = new ws(this);
    private final x y = new wt(this);

    public wr(Context context, Lock lock, Looper looper, com.google.android.gms.common.internal.h h1, b b1, g g1, Map map, 
            List list, List list1, Map map1, int i1, int j1, ArrayList arraylist)
    {
        k = null;
        p = 0x1d4c0L;
        q = 5000L;
        d = new HashSet();
        w = null;
        m = context;
        i = lock;
        j = new w(looper, y);
        n = looper;
        r = new wu(this, looper);
        s = b1;
        l = i1;
        if (l >= 0)
        {
            w = Integer.valueOf(j1);
        }
        f = map;
        c = map1;
        v = arraylist;
        for (context = list.iterator(); context.hasNext(); j.a(lock))
        {
            lock = (q)context.next();
        }

        for (context = list1.iterator(); context.hasNext(); j.a(lock))
        {
            lock = (r)context.next();
        }

        e = h1;
        g = g1;
    }

    public static int a(Iterable iterable, boolean flag)
    {
        boolean flag3 = true;
        iterable = iterable.iterator();
        boolean flag1 = false;
        boolean flag2 = false;
        do
        {
            if (!iterable.hasNext())
            {
                break;
            }
            h h1 = (h)iterable.next();
            if (h1.zzmn())
            {
                flag2 = true;
            }
            if (h1.zzmJ())
            {
                flag1 = true;
            }
        } while (true);
        if (flag2)
        {
            byte byte0 = flag3;
            if (flag1)
            {
                byte0 = flag3;
                if (flag)
                {
                    byte0 = 2;
                }
            }
            return byte0;
        } else
        {
            return 3;
        }
    }

    static ab a(wr wr1)
    {
        return wr1.u;
    }

    private static void a(wy wy1, ab ab1, IBinder ibinder)
    {
        if (wy1.f())
        {
            wy1.a(new wv(wy1, ab1, ibinder, null));
            return;
        }
        if (ibinder != null && ibinder.isBinderAlive())
        {
            wv wv1 = new wv(wy1, ab1, ibinder, null);
            wy1.a(wv1);
            try
            {
                ibinder.linkToDeath(wv1, 0);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (IBinder ibinder)
            {
                wy1.g();
            }
            ab1.a(wy1.a().intValue());
            return;
        } else
        {
            wy1.a(null);
            wy1.g();
            ab1.a(wy1.a().intValue());
            return;
        }
    }

    static String b(int i1)
    {
        switch (i1)
        {
        default:
            return "UNKNOWN";

        case 3: // '\003'
            return "SIGN_IN_MODE_NONE";

        case 1: // '\001'
            return "SIGN_IN_MODE_REQUIRED";

        case 2: // '\002'
            return "SIGN_IN_MODE_OPTIONAL";
        }
    }

    static void b(wr wr1)
    {
        wr1.n();
    }

    private void c(int i1)
    {
        boolean flag;
        if (w == null)
        {
            w = Integer.valueOf(i1);
        } else
        if (w.intValue() != i1)
        {
            throw new IllegalStateException((new StringBuilder()).append("Cannot use sign-in mode: ").append(b(i1)).append(". Mode was already set to ").append(b(w.intValue())).toString());
        }
        if (k != null)
        {
            return;
        }
        Iterator iterator = c.values().iterator();
        i1 = 0;
        flag = false;
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            h h1 = (h)iterator.next();
            if (h1.zzmn())
            {
                flag = true;
            }
            if (h1.zzmJ())
            {
                i1 = 1;
            }
        } while (true);
        w.intValue();
        JVM INSTR tableswitch 1 3: default 180
    //                   1 230
    //                   2 261
    //                   3 180;
           goto _L1 _L2 _L3 _L1
_L1:
        k = new xa(m, this, i, n, s, c, e, f, g, v, this);
        return;
_L2:
        if (!flag)
        {
            throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
        }
        if (i1 != 0)
        {
            throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (flag)
        {
            k = new vx(m, this, i, n, s, c, e, f, g, v);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    static void c(wr wr1)
    {
        wr1.o();
    }

    private void m()
    {
        j.b();
        k.a();
    }

    private void n()
    {
        i.lock();
        if (h())
        {
            m();
        }
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    private void o()
    {
        i.lock();
        if (j())
        {
            m();
        }
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    public Looper a()
    {
        return n;
    }

    public vq a(vq vq1)
    {
        boolean flag;
        if (vq1.b() != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.b(flag, "This task can not be enqueued (it's probably a Batch or malformed)");
        av.b(c.containsKey(vq1.b()), "GoogleApiClient is not configured to use the API required for this call.");
        i.lock();
        if (k != null)
        {
            break MISSING_BLOCK_LABEL_78;
        }
        a.add(vq1);
        i.unlock();
        return vq1;
        vq1 = k.a(vq1);
        i.unlock();
        return vq1;
        vq1;
        i.unlock();
        throw vq1;
    }

    public h a(i i1)
    {
        i1 = (h)c.get(i1);
        av.a(i1, "Appropriate Api was not requested.");
        return i1;
    }

    public void a(int i1)
    {
        boolean flag1 = true;
        i.lock();
        boolean flag = flag1;
        if (i1 != 3)
        {
            flag = flag1;
            if (i1 != 1)
            {
                if (i1 == 2)
                {
                    flag = flag1;
                } else
                {
                    flag = false;
                }
            }
        }
        av.b(flag, (new StringBuilder()).append("Illegal sign-in mode: ").append(i1).toString());
        c(i1);
        m();
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    public void a(Bundle bundle)
    {
        for (; !a.isEmpty(); b((vq)a.remove())) { }
        j.a(bundle);
    }

    void a(wy wy1)
    {
        h.add(wy1);
        wy1.a(x);
    }

    public void a(ConnectionResult connectionresult)
    {
        if (!s.a(m, connectionresult.c()))
        {
            j();
        }
        if (!h())
        {
            j.a(connectionresult);
            j.a();
        }
    }

    public void a(r r1)
    {
        j.a(r1);
    }

    public void a(String s1, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        printwriter.append(s1).append("mContext=").println(m);
        printwriter.append(s1).append("mResuming=").print(o);
        printwriter.append(" mWorkQueue.size()=").print(a.size());
        printwriter.append(" mUnconsumedRunners.size()=").println(h.size());
        if (k != null)
        {
            k.a(s1, filedescriptor, printwriter, as);
        }
    }

    public void a_(int i1)
    {
        if (i1 == 1)
        {
            i();
        }
        for (Iterator iterator = h.iterator(); iterator.hasNext(); ((wy)iterator.next()).d(new Status(8, "The connection to Google Play services was lost"))) { }
        j.a(i1);
        j.a();
        if (i1 == 2)
        {
            m();
        }
    }

    public vq b(vq vq1)
    {
        boolean flag;
        if (vq1.b() != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.b(flag, "This task can not be executed (it's probably a Batch or malformed)");
        i.lock();
        if (k == null)
        {
            throw new IllegalStateException("GoogleApiClient is not connected yet.");
        }
        break MISSING_BLOCK_LABEL_60;
        vq1;
        i.unlock();
        throw vq1;
        if (!h())
        {
            break MISSING_BLOCK_LABEL_131;
        }
        a.add(vq1);
        wy wy1;
        for (; !a.isEmpty(); wy1.c(Status.c))
        {
            wy1 = (wy)a.remove();
            a(wy1);
        }

        i.unlock();
        return vq1;
        vq1 = k.b(vq1);
        i.unlock();
        return vq1;
    }

    public void b()
    {
        boolean flag;
        flag = false;
        i.lock();
        if (l < 0) goto _L2; else goto _L1
_L1:
        if (w != null)
        {
            flag = true;
        }
        av.a(flag, "Sign-in mode should have been set explicitly by auto-manage.");
_L3:
        a(w.intValue());
        i.unlock();
        return;
_L2:
        if (w != null)
        {
            continue; /* Loop/switch isn't completed */
        }
        w = Integer.valueOf(a(c.values(), false));
          goto _L3
        Exception exception;
        exception;
        i.unlock();
        throw exception;
        if (w.intValue() != 2) goto _L3; else goto _L4
_L4:
        throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
    }

    public void b(r r1)
    {
        j.b(r1);
    }

    public void c()
    {
        i.lock();
        e();
        if (k != null)
        {
            break MISSING_BLOCK_LABEL_34;
        }
        f();
        i.unlock();
        return;
        j();
        k.b();
        j.a();
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    void e()
    {
        for (Iterator iterator = h.iterator(); iterator.hasNext();)
        {
            wy wy1 = (wy)iterator.next();
            wy1.a(null);
            if (wy1.a() == null)
            {
                wy1.g();
            } else
            {
                wy1.c();
                IBinder ibinder = a(wy1.b()).zzoC();
                a(wy1, u, ibinder);
            }
        }

        h.clear();
        for (Iterator iterator1 = t.iterator(); iterator1.hasNext(); ((xh)iterator1.next()).a()) { }
        t.clear();
    }

    void f()
    {
        wy wy1;
        for (Iterator iterator = a.iterator(); iterator.hasNext(); wy1.g())
        {
            wy1 = (wy)iterator.next();
            wy1.a(null);
        }

        a.clear();
    }

    public boolean g()
    {
        return k != null && k.c();
    }

    boolean h()
    {
        return o;
    }

    void i()
    {
        if (h())
        {
            return;
        }
        o = true;
        if (b == null)
        {
            b = (ww)xe.a(m.getApplicationContext(), new ww(this), s);
        }
        r.sendMessageDelayed(r.obtainMessage(1), p);
        r.sendMessageDelayed(r.obtainMessage(2), q);
    }

    boolean j()
    {
        if (!h())
        {
            return false;
        }
        o = false;
        r.removeMessages(2);
        r.removeMessages(1);
        if (b != null)
        {
            b.b();
            b = null;
        }
        return true;
    }

    String k()
    {
        StringWriter stringwriter = new StringWriter();
        a("", null, new PrintWriter(stringwriter), null);
        return stringwriter.toString();
    }

    public int l()
    {
        return System.identityHashCode(this);
    }
}
