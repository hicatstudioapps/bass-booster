// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            db, cs, ce, bx, 
//            tu, by, op, cb, 
//            yd, cc, bz

public class ca extends Thread
{

    private boolean a;
    private boolean b;
    private boolean c;
    private final Object d = new Object();
    private final bz e;
    private final by f;
    private final op g;
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final int l;

    public ca(bz bz1, by by1, op op1)
    {
        a = false;
        b = false;
        c = false;
        e = bz1;
        f = by1;
        g = op1;
        i = ((Integer)db.K.c()).intValue();
        j = ((Integer)db.L.c()).intValue();
        k = ((Integer)db.M.c()).intValue();
        l = ((Integer)db.N.c()).intValue();
        h = ((Integer)db.O.c()).intValue();
        setName("ContentFetchTask");
    }

    ce a(View view, bx bx1)
    {
        int i1 = 0;
        if (view == null)
        {
            return new ce(this, 0, 0);
        }
        if ((view instanceof TextView) && !(view instanceof EditText))
        {
            view = ((TextView)view).getText();
            if (!TextUtils.isEmpty(view))
            {
                bx1.b(view.toString());
                return new ce(this, 1, 0);
            } else
            {
                return new ce(this, 0, 0);
            }
        }
        if ((view instanceof WebView) && !(view instanceof tu))
        {
            bx1.e();
            if (a((WebView)view, bx1))
            {
                return new ce(this, 0, 1);
            } else
            {
                return new ce(this, 0, 0);
            }
        }
        if (view instanceof ViewGroup)
        {
            view = (ViewGroup)view;
            int j1 = 0;
            int k1 = 0;
            for (; i1 < view.getChildCount(); i1++)
            {
                ce ce1 = a(view.getChildAt(i1), bx1);
                k1 += ce1.a;
                j1 += ce1.b;
            }

            return new ce(this, k1, j1);
        } else
        {
            return new ce(this, 0, 0);
        }
    }

    public void a()
    {
label0:
        {
            synchronized (d)
            {
                if (!a)
                {
                    break label0;
                }
                zzb.zzaF("Content hash thread already started, quiting...");
            }
            return;
        }
        a = true;
        obj;
        JVM INSTR monitorexit ;
        start();
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    void a(Activity activity)
    {
        if (activity != null)
        {
            Object obj = null;
            View view = obj;
            if (activity.getWindow() != null)
            {
                view = obj;
                if (activity.getWindow().getDecorView() != null)
                {
                    view = activity.getWindow().getDecorView().findViewById(0x1020002);
                }
            }
            if (view != null)
            {
                a(view);
                return;
            }
        }
    }

    void a(bx bx1, WebView webview, String s)
    {
        bx1.d();
        if (!TextUtils.isEmpty(s))
        {
            s = (new JSONObject(s)).optString("text");
            if (TextUtils.isEmpty(webview.getTitle()))
            {
                break MISSING_BLOCK_LABEL_82;
            }
            bx1.a((new StringBuilder()).append(webview.getTitle()).append("\n").append(s).toString());
        }
_L2:
        if (bx1.a())
        {
            f.b(bx1);
            return;
        }
        break MISSING_BLOCK_LABEL_113;
        bx1.a(s);
        if (true) goto _L2; else goto _L1
_L1:
        bx1;
        zzb.zzaF("Json string may be malformed.");
        return;
        bx1;
        zzb.zza("Failed to get webview content.", bx1);
        g.a(bx1, true);
    }

    boolean a(android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo)
    {
        return runningappprocessinfo.importance == 100;
    }

    boolean a(Context context)
    {
        context = (PowerManager)context.getSystemService("power");
        if (context == null)
        {
            return false;
        } else
        {
            return context.isScreenOn();
        }
    }

    boolean a(View view)
    {
        if (view == null)
        {
            return false;
        } else
        {
            view.post(new cb(this, view));
            return true;
        }
    }

    boolean a(WebView webview, bx bx1)
    {
        if (!yd.f())
        {
            return false;
        } else
        {
            bx1.e();
            webview.post(new cc(this, bx1, webview));
            return true;
        }
    }

    void b(View view)
    {
        bx bx1;
        bx1 = new bx(i, j, k, l);
        view = a(view, bx1);
        bx1.f();
        if (((ce) (view)).a == 0 && ((ce) (view)).b == 0)
        {
            return;
        }
        try
        {
            if ((((ce) (view)).b != 0 || bx1.h() != 0) && (((ce) (view)).b != 0 || !f.a(bx1)))
            {
                f.c(bx1);
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            zzb.zzb("Exception in fetchContentOnUIThread", view);
            g.a(view, true);
        }
        return;
    }

    boolean b()
    {
        Context context;
        KeyguardManager keyguardmanager;
        Object obj;
        android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo;
        boolean flag;
        try
        {
            context = e.b();
        }
        catch (Throwable throwable)
        {
            return false;
        }
        if (context == null)
        {
            return false;
        }
        obj = (ActivityManager)context.getSystemService("activity");
        keyguardmanager = (KeyguardManager)context.getSystemService("keyguard");
        if (obj == null || keyguardmanager == null)
        {
            break MISSING_BLOCK_LABEL_131;
        }
        obj = ((ActivityManager) (obj)).getRunningAppProcesses();
        if (obj == null)
        {
            return false;
        }
        obj = ((List) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break MISSING_BLOCK_LABEL_126;
            }
            runningappprocessinfo = (android.app.ActivityManager.RunningAppProcessInfo)((Iterator) (obj)).next();
        } while (Process.myPid() != runningappprocessinfo.pid);
        if (!a(runningappprocessinfo) || keyguardmanager.inKeyguardRestrictedInputMode())
        {
            break MISSING_BLOCK_LABEL_126;
        }
        flag = a(context);
        if (flag)
        {
            return true;
        }
        return false;
        return false;
    }

    public bx c()
    {
        return f.a();
    }

    public void d()
    {
        synchronized (d)
        {
            b = false;
            d.notifyAll();
            zzb.zzaF("ContentFetchThread: wakeup");
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void e()
    {
        synchronized (d)
        {
            b = true;
            zzb.zzaF((new StringBuilder()).append("ContentFetchThread: paused, mPause = ").append(b).toString());
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean f()
    {
        return b;
    }

    public void run()
    {
_L11:
        if (c)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!b()) goto _L2; else goto _L1
_L1:
        Object obj = e.a();
        if (obj != null) goto _L4; else goto _L3
_L3:
        try
        {
            zzb.zzaF("ContentFetchThread: no activity");
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            zzb.zzb("Error in ContentFetchTask", ((Throwable) (obj)));
            g.a(((Throwable) (obj)), true);
        }
_L8:
        obj = d;
        obj;
        JVM INSTR monitorenter ;
_L7:
        boolean flag = b;
        if (!flag) goto _L6; else goto _L5
_L5:
        Exception exception;
        try
        {
            zzb.zzaF("ContentFetchTask: waiting");
            d.wait();
        }
        catch (InterruptedException interruptedexception) { }
        finally { }
        if (true) goto _L7; else goto _L6
_L4:
        a(((Activity) (obj)));
_L9:
        Thread.sleep(h * 1000);
          goto _L8
_L2:
        zzb.zzaF("ContentFetchTask: sleeping");
        e();
          goto _L9
_L6:
        obj;
        JVM INSTR monitorexit ;
        if (true) goto _L11; else goto _L10
        obj;
        JVM INSTR monitorexit ;
        throw exception;
_L10:
    }
}
