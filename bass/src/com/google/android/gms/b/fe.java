// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.IBinder;
import android.os.Parcel;

// Referenced classes of package com.google.android.gms.b:
//            fc, es

class fe
    implements fc
{

    private IBinder a;

    fe(IBinder ibinder)
    {
        a = ibinder;
    }

    public void a(es es1, String s)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.formats.client.IOnCustomClickListener");
        if (es1 == null)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        es1 = es1.asBinder();
_L1:
        parcel.writeStrongBinder(es1);
        parcel.writeString(s);
        a.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        es1 = null;
          goto _L1
        es1;
        parcel1.recycle();
        parcel.recycle();
        throw es1;
    }

    public IBinder asBinder()
    {
        return a;
    }
}
