// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            jg, dq, jh, jo, 
//            jl, rq, jv, jx, 
//            ji

public class ju
    implements jg
{

    private final AdRequestInfoParcel a;
    private final jx b;
    private final Context c;
    private final Object d = new Object();
    private final ji e;
    private final boolean f;
    private final long g;
    private final long h;
    private final dq i;
    private boolean j;
    private jl k;

    public ju(Context context, AdRequestInfoParcel adrequestinfoparcel, jx jx, ji ji, boolean flag, long l, 
            long l1, dq dq1)
    {
        j = false;
        c = context;
        a = adrequestinfoparcel;
        b = jx;
        e = ji;
        f = flag;
        g = l;
        h = l1;
        i = dq1;
    }

    public jo a(List list)
    {
        do do1;
        Iterator iterator;
        zzb.zzaF("Starting mediation.");
        obj = new ArrayList();
        do1 = i.a();
        iterator = list.iterator();
_L4:
        jh jh1;
        Iterator iterator1;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_376;
        }
        jh1 = (jh)iterator.next();
        zzb.zzaG((new StringBuilder()).append("Trying mediation network: ").append(jh1.b).toString());
        iterator1 = jh1.c.iterator();
_L2:
        String s;
        do do2;
label0:
        {
            if (!iterator1.hasNext())
            {
                break; /* Loop/switch isn't completed */
            }
            s = (String)iterator1.next();
            do2 = i.a();
            synchronized (d)
            {
                if (!j)
                {
                    break label0;
                }
                obj = new jo(-1);
            }
            return ((jo) (obj));
        }
        k = new jl(c, s, b, e, jh1, a.zzGq, a.zzqV, a.zzqR, f, a.zzrj, a.zzrl);
        list;
        JVM INSTR monitorexit ;
        list = k.a(g, h);
        if (((jo) (list)).a == 0)
        {
            zzb.zzaF("Adapter succeeded.");
            i.a("mediation_network_succeed", s);
            if (!((List) (obj)).isEmpty())
            {
                i.a("mediation_networks_fail", TextUtils.join(",", ((Iterable) (obj))));
            }
            i.a(do2, new String[] {
                "mls"
            });
            i.a(do1, new String[] {
                "ttm"
            });
            return list;
        }
        break MISSING_BLOCK_LABEL_322;
        obj;
        list;
        JVM INSTR monitorexit ;
        throw obj;
        ((List) (obj)).add(s);
        i.a(do2, new String[] {
            "mlf"
        });
        if (((jo) (list)).c != null)
        {
            rq.a.post(new jv(this, list));
        }
        if (true) goto _L2; else goto _L1
_L1:
        if (true) goto _L4; else goto _L3
_L3:
        if (!((List) (obj)).isEmpty())
        {
            i.a("mediation_networks_fail", TextUtils.join(",", ((Iterable) (obj))));
        }
        return new jo(1);
    }

    public void a()
    {
        synchronized (d)
        {
            j = true;
            if (k != null)
            {
                k.a();
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
