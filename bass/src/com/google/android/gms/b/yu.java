// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.common.api.f;
import com.google.android.gms.common.api.s;

// Referenced classes of package com.google.android.gms.b:
//            yw, yv

public final class yu
    implements f
{

    public static final yu a = (new yw()).a();
    private final boolean b;
    private final boolean c;
    private final String d;
    private final s e;
    private final boolean f;
    private final boolean g;
    private final boolean h;

    private yu(boolean flag, boolean flag1, String s, s s1, boolean flag2, boolean flag3, boolean flag4)
    {
        b = flag;
        c = flag1;
        d = s;
        e = s1;
        f = flag2;
        g = flag3;
        h = flag4;
    }

    yu(boolean flag, boolean flag1, String s, s s1, boolean flag2, boolean flag3, boolean flag4, 
            yv yv)
    {
        this(flag, flag1, s, s1, flag2, flag3, flag4);
    }

    public boolean a()
    {
        return b;
    }

    public boolean b()
    {
        return c;
    }

    public String c()
    {
        return d;
    }

    public s d()
    {
        return e;
    }

    public boolean e()
    {
        return f;
    }

    public boolean f()
    {
        return g;
    }

    public boolean g()
    {
        return h;
    }

}
