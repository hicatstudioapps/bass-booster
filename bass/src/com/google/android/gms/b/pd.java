// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.SearchAdRequestParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            rt, ot, pk, rq, 
//            qx, pr, cp

public final class pd
{

    private static final SimpleDateFormat a;

    public static AdResponseParcel a(Context context, AdRequestInfoParcel adrequestinfoparcel, String s)
    {
        String s1;
        String s2;
        Object obj;
        Object obj1;
        JSONObject jsonobject;
        String s3;
        String s4;
        int i;
        int j;
        long l;
        long l1;
        boolean flag;
        try
        {
            jsonobject = new JSONObject(s);
            s1 = jsonobject.optString("ad_base_url", null);
            s2 = jsonobject.optString("ad_url", null);
            s3 = jsonobject.optString("ad_size", null);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzaH((new StringBuilder()).append("Could not parse the mediation config: ").append(context.getMessage()).toString());
            return new AdResponseParcel(0);
        }
        if (adrequestinfoparcel == null) goto _L2; else goto _L1
_L1:
        if (adrequestinfoparcel.zzGw == 0) goto _L2; else goto _L3
_L3:
        flag = true;
_L50:
        if (!flag) goto _L5; else goto _L4
_L4:
        s = jsonobject.optString("ad_json", null);
_L13:
        l1 = -1L;
        s4 = jsonobject.optString("debug_dialog", null);
        if (!jsonobject.has("interstitial_timeout")) goto _L7; else goto _L6
_L6:
        l = (long)(jsonobject.getDouble("interstitial_timeout") * 1000D);
_L51:
        obj = jsonobject.optString("orientation", null);
        i = -1;
        if (!"portrait".equals(obj)) goto _L9; else goto _L8
_L8:
        i = zzp.zzbz().b();
_L15:
        if (TextUtils.isEmpty(s)) goto _L11; else goto _L10
_L10:
        if (TextUtils.isEmpty(s1))
        {
            zzb.zzaH("Could not parse the mediation config: Missing required ad_base_url field");
            return new AdResponseParcel(0);
        }
          goto _L12
_L5:
        s = jsonobject.optString("ad_html", null);
          goto _L13
_L9:
        if (!"landscape".equals(obj)) goto _L15; else goto _L14
_L14:
        i = zzp.zzbz().a();
          goto _L15
_L11:
        if (TextUtils.isEmpty(s2)) goto _L17; else goto _L16
_L16:
        obj = ot.a(adrequestinfoparcel, context, adrequestinfoparcel.zzqR.afmaVersion, s2, null, null, null, null, null);
        s1 = ((AdResponseParcel) (obj)).zzDE;
        s2 = ((AdResponseParcel) (obj)).body;
        l1 = ((AdResponseParcel) (obj)).zzGR;
_L49:
        obj1 = jsonobject.optJSONArray("click_urls");
        if (obj != null) goto _L19; else goto _L18
_L18:
        context = null;
_L27:
        if (obj1 == null) goto _L21; else goto _L20
_L20:
        s = context;
        if (context != null) goto _L23; else goto _L22
_L22:
        s = new LinkedList();
          goto _L23
_L26:
        if (j >= ((JSONArray) (obj1)).length()) goto _L25; else goto _L24
_L24:
        s.add(((JSONArray) (obj1)).getString(j));
        j++;
          goto _L26
_L17:
        adrequestinfoparcel = (new StringBuilder()).append("Could not parse the mediation config: Missing required ");
        if (flag)
        {
            context = "ad_json";
        } else
        {
            context = "ad_html";
        }
        zzb.zzaH(adrequestinfoparcel.append(context).append(" or ").append("ad_url").append(" field.").toString());
        context = new AdResponseParcel(0);
        return context;
_L19:
        context = ((AdResponseParcel) (obj)).zzAQ;
          goto _L27
_L48:
        Object obj2 = jsonobject.optJSONArray("impression_urls");
        if (obj != null) goto _L29; else goto _L28
_L28:
        context = null;
_L37:
        if (obj2 == null) goto _L31; else goto _L30
_L30:
        s = context;
        if (context != null) goto _L33; else goto _L32
_L32:
        s = new LinkedList();
          goto _L33
_L36:
        if (j >= ((JSONArray) (obj2)).length()) goto _L35; else goto _L34
_L34:
        s.add(((JSONArray) (obj2)).getString(j));
        j++;
          goto _L36
_L29:
        context = ((AdResponseParcel) (obj)).zzAR;
          goto _L37
_L47:
        JSONArray jsonarray = jsonobject.optJSONArray("manual_impression_urls");
        if (obj != null) goto _L39; else goto _L38
_L38:
        context = null;
_L45:
        if (jsonarray == null) goto _L41; else goto _L40
_L40:
        s = context;
        if (context != null) goto _L43; else goto _L42
_L42:
        s = new LinkedList();
          goto _L43
_L44:
        if (j >= jsonarray.length())
        {
            break MISSING_BLOCK_LABEL_901;
        }
        s.add(jsonarray.getString(j));
        j++;
          goto _L44
_L39:
        context = ((AdResponseParcel) (obj)).zzGP;
          goto _L45
_L41:
        j = i;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_826;
        }
        if (((AdResponseParcel) (obj)).orientation != -1)
        {
            i = ((AdResponseParcel) (obj)).orientation;
        }
        j = i;
        if (((AdResponseParcel) (obj)).zzGM <= 0L)
        {
            break MISSING_BLOCK_LABEL_826;
        }
        l = ((AdResponseParcel) (obj)).zzGM;
_L46:
        obj = jsonobject.optString("active_view");
        s = null;
        boolean flag1 = jsonobject.optBoolean("ad_is_javascript", false);
        if (!flag1)
        {
            break MISSING_BLOCK_LABEL_667;
        }
        s = jsonobject.optString("ad_passback_url", null);
        boolean flag2 = jsonobject.optBoolean("mediation", false);
        boolean flag3 = jsonobject.optBoolean("custom_render_allowed", false);
        boolean flag4 = jsonobject.optBoolean("content_url_opted_out", true);
        boolean flag5 = jsonobject.optBoolean("prefetch", false);
        j = jsonobject.optInt("oauth2_token_status", 0);
        long l2 = jsonobject.optLong("refresh_interval_milliseconds", -1L);
        long l3 = jsonobject.optLong("mediation_config_cache_time_milliseconds", -1L);
        String s5 = jsonobject.optString("gws_query_id", "");
        boolean flag6 = "height".equals(jsonobject.optString("fluid", ""));
        context = new AdResponseParcel(adrequestinfoparcel, s1, s2, ((List) (obj1)), ((List) (obj2)), l, flag2, l3, context, l2, i, s3, l1, s4, flag1, s, ((String) (obj)), flag3, flag, adrequestinfoparcel.zzGy, flag4, flag5, j, s5, flag6);
        return context;
        i = j;
          goto _L46
_L31:
        obj2 = context;
          goto _L47
_L21:
        obj1 = context;
          goto _L48
_L12:
        obj = null;
        s2 = s;
          goto _L49
_L2:
        flag = false;
          goto _L50
_L7:
        l = -1L;
          goto _L51
_L23:
        j = 0;
          goto _L26
_L25:
        obj1 = s;
          goto _L48
_L33:
        j = 0;
          goto _L36
_L35:
        obj2 = s;
          goto _L47
_L43:
        j = 0;
          goto _L44
        context = s;
          goto _L41
    }

    private static Integer a(boolean flag)
    {
        int i;
        if (flag)
        {
            i = 1;
        } else
        {
            i = 0;
        }
        return Integer.valueOf(i);
    }

    private static String a(int i)
    {
        return String.format(Locale.US, "#%06x", new Object[] {
            Integer.valueOf(0xffffff & i)
        });
    }

    private static String a(NativeAdOptionsParcel nativeadoptionsparcel)
    {
        int i;
        if (nativeadoptionsparcel != null)
        {
            i = nativeadoptionsparcel.zzyd;
        } else
        {
            i = 0;
        }
        switch (i)
        {
        default:
            return "any";

        case 1: // '\001'
            return "portrait";

        case 2: // '\002'
            return "landscape";
        }
    }

    public static JSONObject a(Context context, AdRequestInfoParcel adrequestinfoparcel, pk pk1, pr pr1, Location location, cp cp, String s, String s1, 
            List list, Bundle bundle)
    {
        int k;
        context = new HashMap();
        if (list.size() > 0)
        {
            context.put("eid", TextUtils.join(",", list));
        }
        if (adrequestinfoparcel.zzGp != null)
        {
            context.put("ad_pos", adrequestinfoparcel.zzGp);
        }
        a(((HashMap) (context)), adrequestinfoparcel.zzGq);
        context.put("format", adrequestinfoparcel.zzqV.zztV);
        if (adrequestinfoparcel.zzqV.width == -1)
        {
            context.put("smart_w", "full");
        }
        if (adrequestinfoparcel.zzqV.height == -2)
        {
            context.put("smart_h", "auto");
        }
        if (adrequestinfoparcel.zzqV.zztZ)
        {
            context.put("fluid", "height");
        }
        if (adrequestinfoparcel.zzqV.zztX == null)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        location = new StringBuilder();
        cp = adrequestinfoparcel.zzqV.zztX;
        k = cp.length;
        int i = 0;
_L3:
        if (i >= k)
        {
            break MISSING_BLOCK_LABEL_308;
        }
        list = cp[i];
        if (location.length() != 0)
        {
            location.append("|");
        }
        if (((AdSizeParcel) (list)).width != -1) goto _L2; else goto _L1
_L1:
        int j = (int)((float)((AdSizeParcel) (list)).widthPixels / pk1.r);
_L4:
        location.append(j);
        location.append("x");
        if (((AdSizeParcel) (list)).height != -2)
        {
            break MISSING_BLOCK_LABEL_298;
        }
        j = (int)((float)((AdSizeParcel) (list)).heightPixels / pk1.r);
_L5:
        location.append(j);
        i++;
          goto _L3
_L2:
        boolean flag;
        try
        {
            j = ((AdSizeParcel) (list)).width;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzaH((new StringBuilder()).append("Problem serializing ad request to JSON: ").append(context.getMessage()).toString());
            return null;
        }
          goto _L4
        j = ((AdSizeParcel) (list)).height;
          goto _L5
        context.put("sz", location);
        if (adrequestinfoparcel.zzGw != 0)
        {
            context.put("native_version", Integer.valueOf(adrequestinfoparcel.zzGw));
            if (!adrequestinfoparcel.zzqV.zzua)
            {
                context.put("native_templates", adrequestinfoparcel.zzrl);
                context.put("native_image_orientation", a(adrequestinfoparcel.zzrj));
                if (!adrequestinfoparcel.zzGH.isEmpty())
                {
                    context.put("native_custom_templates", adrequestinfoparcel.zzGH);
                }
            }
        }
        context.put("slotname", adrequestinfoparcel.zzqP);
        context.put("pn", adrequestinfoparcel.applicationInfo.packageName);
        if (adrequestinfoparcel.zzGr != null)
        {
            context.put("vc", Integer.valueOf(adrequestinfoparcel.zzGr.versionCode));
        }
        context.put("ms", s1);
        context.put("seq_num", adrequestinfoparcel.zzGt);
        context.put("session_id", adrequestinfoparcel.zzGu);
        context.put("js", adrequestinfoparcel.zzqR.afmaVersion);
        a(((HashMap) (context)), pk1, pr1);
        context.put("platform", Build.MANUFACTURER);
        context.put("submodel", Build.MODEL);
        if (adrequestinfoparcel.zzGq.versionCode >= 2 && adrequestinfoparcel.zzGq.zzty != null)
        {
            a(((HashMap) (context)), adrequestinfoparcel.zzGq.zzty);
        }
        if (adrequestinfoparcel.versionCode >= 2)
        {
            context.put("quality_signals", adrequestinfoparcel.zzGv);
        }
        if (adrequestinfoparcel.versionCode >= 4 && adrequestinfoparcel.zzGy)
        {
            context.put("forceHttps", Boolean.valueOf(adrequestinfoparcel.zzGy));
        }
        if (bundle == null)
        {
            break MISSING_BLOCK_LABEL_627;
        }
        context.put("content_info", bundle);
        if (adrequestinfoparcel.versionCode < 5) goto _L7; else goto _L6
_L6:
        context.put("u_sd", Float.valueOf(adrequestinfoparcel.zzGC));
        context.put("sh", Integer.valueOf(adrequestinfoparcel.zzGB));
        context.put("sw", Integer.valueOf(adrequestinfoparcel.zzGA));
_L10:
        if (adrequestinfoparcel.versionCode < 6) goto _L9; else goto _L8
_L8:
        flag = TextUtils.isEmpty(adrequestinfoparcel.zzGD);
        if (flag)
        {
            break MISSING_BLOCK_LABEL_722;
        }
        context.put("view_hierarchy", new JSONObject(adrequestinfoparcel.zzGD));
_L11:
        context.put("correlation_id", Long.valueOf(adrequestinfoparcel.zzGE));
_L9:
        if (adrequestinfoparcel.versionCode >= 7)
        {
            context.put("request_id", adrequestinfoparcel.zzGF);
        }
        if (adrequestinfoparcel.versionCode >= 11 && adrequestinfoparcel.zzGJ != null)
        {
            context.put("capability", adrequestinfoparcel.zzGJ.toBundle());
        }
        a(((HashMap) (context)), s);
        if (adrequestinfoparcel.versionCode >= 12 && !TextUtils.isEmpty(adrequestinfoparcel.zzGK))
        {
            context.put("anchor", adrequestinfoparcel.zzGK);
        }
        if (zzb.zzQ(2))
        {
            adrequestinfoparcel = zzp.zzbx().a(context).toString(2);
            zzb.v((new StringBuilder()).append("Ad Request JSON: ").append(adrequestinfoparcel).toString());
        }
        return zzp.zzbx().a(context);
_L7:
        context.put("u_sd", Float.valueOf(pk1.r));
        context.put("sh", Integer.valueOf(pk1.t));
        context.put("sw", Integer.valueOf(pk1.s));
          goto _L10
        pk1;
        zzb.zzd("Problem serializing view hierarchy to JSON", pk1);
          goto _L11
    }

    private static void a(HashMap hashmap, Location location)
    {
        HashMap hashmap1 = new HashMap();
        float f = location.getAccuracy();
        long l = location.getTime();
        long l1 = (long)(location.getLatitude() * 10000000D);
        long l2 = (long)(location.getLongitude() * 10000000D);
        hashmap1.put("radius", Float.valueOf(f * 1000F));
        hashmap1.put("lat", Long.valueOf(l1));
        hashmap1.put("long", Long.valueOf(l2));
        hashmap1.put("time", Long.valueOf(l * 1000L));
        hashmap.put("uule", hashmap1);
    }

    private static void a(HashMap hashmap, AdRequestParcel adrequestparcel)
    {
        String s = qx.a();
        if (s != null)
        {
            hashmap.put("abf", s);
        }
        if (adrequestparcel.zztq != -1L)
        {
            hashmap.put("cust_age", a.format(new Date(adrequestparcel.zztq)));
        }
        if (adrequestparcel.extras != null)
        {
            hashmap.put("extras", adrequestparcel.extras);
        }
        if (adrequestparcel.zztr != -1)
        {
            hashmap.put("cust_gender", Integer.valueOf(adrequestparcel.zztr));
        }
        if (adrequestparcel.zzts != null)
        {
            hashmap.put("kw", adrequestparcel.zzts);
        }
        if (adrequestparcel.zztu != -1)
        {
            hashmap.put("tag_for_child_directed_treatment", Integer.valueOf(adrequestparcel.zztu));
        }
        if (adrequestparcel.zztt)
        {
            hashmap.put("adtest", "on");
        }
        if (adrequestparcel.versionCode >= 2)
        {
            if (adrequestparcel.zztv)
            {
                hashmap.put("d_imp_hdr", Integer.valueOf(1));
            }
            if (!TextUtils.isEmpty(adrequestparcel.zztw))
            {
                hashmap.put("ppid", adrequestparcel.zztw);
            }
            if (adrequestparcel.zztx != null)
            {
                a(hashmap, adrequestparcel.zztx);
            }
        }
        if (adrequestparcel.versionCode >= 3 && adrequestparcel.zztz != null)
        {
            hashmap.put("url", adrequestparcel.zztz);
        }
        if (adrequestparcel.versionCode >= 5)
        {
            if (adrequestparcel.zztB != null)
            {
                hashmap.put("custom_targeting", adrequestparcel.zztB);
            }
            if (adrequestparcel.zztC != null)
            {
                hashmap.put("category_exclusions", adrequestparcel.zztC);
            }
            if (adrequestparcel.zztD != null)
            {
                hashmap.put("request_agent", adrequestparcel.zztD);
            }
        }
        if (adrequestparcel.versionCode >= 6 && adrequestparcel.zztE != null)
        {
            hashmap.put("request_pkg", adrequestparcel.zztE);
        }
        if (adrequestparcel.versionCode >= 7)
        {
            hashmap.put("is_designed_for_families", Boolean.valueOf(adrequestparcel.zztF));
        }
    }

    private static void a(HashMap hashmap, SearchAdRequestParcel searchadrequestparcel)
    {
        Object obj;
        obj = null;
        if (Color.alpha(searchadrequestparcel.zzuI) != 0)
        {
            hashmap.put("acolor", a(searchadrequestparcel.zzuI));
        }
        if (Color.alpha(searchadrequestparcel.backgroundColor) != 0)
        {
            hashmap.put("bgcolor", a(searchadrequestparcel.backgroundColor));
        }
        if (Color.alpha(searchadrequestparcel.zzuJ) != 0 && Color.alpha(searchadrequestparcel.zzuK) != 0)
        {
            hashmap.put("gradientto", a(searchadrequestparcel.zzuJ));
            hashmap.put("gradientfrom", a(searchadrequestparcel.zzuK));
        }
        if (Color.alpha(searchadrequestparcel.zzuL) != 0)
        {
            hashmap.put("bcolor", a(searchadrequestparcel.zzuL));
        }
        hashmap.put("bthick", Integer.toString(searchadrequestparcel.zzuM));
        searchadrequestparcel.zzuN;
        JVM INSTR tableswitch 0 3: default 176
    //                   0 358
    //                   1 365
    //                   2 372
    //                   3 379;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        String s = null;
_L10:
        if (s != null)
        {
            hashmap.put("btype", s);
        }
        searchadrequestparcel.zzuO;
        JVM INSTR tableswitch 0 2: default 220
    //                   0 393
    //                   1 400
    //                   2 386;
           goto _L6 _L7 _L8 _L9
_L8:
        break MISSING_BLOCK_LABEL_400;
_L6:
        s = obj;
_L11:
        if (s != null)
        {
            hashmap.put("callbuttoncolor", s);
        }
        if (searchadrequestparcel.zzuP != null)
        {
            hashmap.put("channel", searchadrequestparcel.zzuP);
        }
        if (Color.alpha(searchadrequestparcel.zzuQ) != 0)
        {
            hashmap.put("dcolor", a(searchadrequestparcel.zzuQ));
        }
        if (searchadrequestparcel.zzuR != null)
        {
            hashmap.put("font", searchadrequestparcel.zzuR);
        }
        if (Color.alpha(searchadrequestparcel.zzuS) != 0)
        {
            hashmap.put("hcolor", a(searchadrequestparcel.zzuS));
        }
        hashmap.put("headersize", Integer.toString(searchadrequestparcel.zzuT));
        if (searchadrequestparcel.zzuU != null)
        {
            hashmap.put("q", searchadrequestparcel.zzuU);
        }
        return;
_L2:
        s = "none";
          goto _L10
_L3:
        s = "dashed";
          goto _L10
_L4:
        s = "dotted";
          goto _L10
_L5:
        s = "solid";
          goto _L10
_L9:
        s = "dark";
          goto _L11
_L7:
        s = "light";
          goto _L11
        s = "medium";
          goto _L11
    }

    private static void a(HashMap hashmap, pk pk1, pr pr1)
    {
        hashmap.put("am", Integer.valueOf(pk1.a));
        hashmap.put("cog", a(pk1.b));
        hashmap.put("coh", a(pk1.c));
        if (!TextUtils.isEmpty(pk1.d))
        {
            hashmap.put("carrier", pk1.d);
        }
        hashmap.put("gl", pk1.e);
        if (pk1.f)
        {
            hashmap.put("simulator", Integer.valueOf(1));
        }
        if (pk1.g)
        {
            hashmap.put("is_sidewinder", Integer.valueOf(1));
        }
        hashmap.put("ma", a(pk1.h));
        hashmap.put("sp", a(pk1.i));
        hashmap.put("hl", pk1.j);
        if (!TextUtils.isEmpty(pk1.k))
        {
            hashmap.put("mv", pk1.k);
        }
        hashmap.put("muv", Integer.valueOf(pk1.l));
        if (pk1.m != -2)
        {
            hashmap.put("cnt", Integer.valueOf(pk1.m));
        }
        hashmap.put("gnt", Integer.valueOf(pk1.n));
        hashmap.put("pt", Integer.valueOf(pk1.o));
        hashmap.put("rm", Integer.valueOf(pk1.p));
        hashmap.put("riv", Integer.valueOf(pk1.q));
        Bundle bundle = new Bundle();
        bundle.putString("build", pk1.y);
        Bundle bundle1 = new Bundle();
        bundle1.putBoolean("is_charging", pk1.v);
        bundle1.putDouble("battery_level", pk1.u);
        bundle.putBundle("battery", bundle1);
        bundle1 = new Bundle();
        bundle1.putInt("active_network_state", pk1.x);
        bundle1.putBoolean("active_network_metered", pk1.w);
        if (pr1 != null)
        {
            pk1 = new Bundle();
            pk1.putInt("predicted_latency_micros", pr1.a);
            pk1.putLong("predicted_down_throughput_bps", pr1.b);
            pk1.putLong("predicted_up_throughput_bps", pr1.c);
            bundle1.putBundle("predictions", pk1);
        }
        bundle.putBundle("network", bundle1);
        hashmap.put("device", bundle);
    }

    private static void a(HashMap hashmap, String s)
    {
        if (s != null)
        {
            HashMap hashmap1 = new HashMap();
            hashmap1.put("token", s);
            hashmap.put("pan", hashmap1);
        }
    }

    static 
    {
        a = new SimpleDateFormat("yyyyMMdd", Locale.US);
    }
}
