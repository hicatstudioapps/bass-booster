// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.h;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            wz, xa, wr, wy, 
//            xg, wb, vq

public class wa
    implements wz
{

    private final xa a;

    public wa(xa xa1)
    {
        a = xa1;
    }

    private void a(wy wy1)
    {
        a.g.a(wy1);
        h h1 = a.g.a(wy1.b());
        if (!h1.isConnected() && a.b.containsKey(wy1.b()))
        {
            wy1.c(new Status(17));
            return;
        } else
        {
            wy1.b(h1);
            return;
        }
    }

    public vq a(vq vq)
    {
        return b(vq);
    }

    public void a()
    {
    }

    public void a(int i)
    {
        a.a(null);
        a.h.a_(i);
    }

    public void a(Bundle bundle)
    {
    }

    public void a(ConnectionResult connectionresult, a a1, int i)
    {
    }

    public vq b(vq vq)
    {
        try
        {
            a(vq);
        }
        catch (DeadObjectException deadobjectexception)
        {
            a.a(new wb(this, this));
            return vq;
        }
        return vq;
    }

    public void b()
    {
        a.a(null);
    }

    public void c()
    {
    }
}
