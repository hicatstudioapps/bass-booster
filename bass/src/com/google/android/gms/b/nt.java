// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            qy, qq, ny, nw, 
//            rq, nv, nu, qp

public abstract class nt extends qy
{

    protected final ny a;
    protected final Context b;
    protected final Object c = new Object();
    protected final Object d = new Object();
    protected final qq e;
    protected AdResponseParcel f;

    protected nt(Context context, qq qq1, ny ny1)
    {
        super(true);
        b = context;
        e = qq1;
        f = qq1.b;
        a = ny1;
    }

    protected abstract qp a(int i);

    protected abstract void a(long l);

    protected void a(qp qp)
    {
        a.zzb(qp);
    }

    public void onStop()
    {
    }

    public void zzbp()
    {
        Object obj = c;
        obj;
        JVM INSTR monitorenter ;
        int i;
        zzb.zzaF("AdRendererBackgroundTask started.");
        i = e.e;
        a(SystemClock.elapsedRealtime());
_L3:
        qp qp = a(i);
        rq.a.post(new nv(this, qp));
        return;
        Object obj1;
        obj1;
        i = ((nw) (obj1)).a();
        if (i != 3 && i != -1) goto _L2; else goto _L1
_L1:
        zzb.zzaG(((nw) (obj1)).getMessage());
_L4:
        if (f != null)
        {
            break MISSING_BLOCK_LABEL_127;
        }
        f = new AdResponseParcel(i);
_L5:
        rq.a.post(new nu(this));
          goto _L3
_L2:
        zzb.zzaH(((nw) (obj1)).getMessage());
          goto _L4
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        f = new AdResponseParcel(i, f.zzAU);
          goto _L5
    }
}
