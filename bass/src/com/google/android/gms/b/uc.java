// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.Map;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            tu, tt, tv, dq, 
//            ap, ab, do, dp

class uc extends FrameLayout
    implements tu
{

    private final tu a;
    private final tt b;

    public uc(tu tu1)
    {
        super(tu1.getContext());
        a = tu1;
        b = new tt(tu1.f(), this, this);
        tu1 = a.k();
        if (tu1 != null)
        {
            tu1.a(this);
        }
        addView(a.b());
    }

    public WebView a()
    {
        return a.a();
    }

    public void a(int i1)
    {
        a.a(i1);
    }

    public void a(Context context)
    {
        a.a(context);
    }

    public void a(Context context, AdSizeParcel adsizeparcel, dq dq)
    {
        a.a(context, adsizeparcel, dq);
    }

    public void a(AdSizeParcel adsizeparcel)
    {
        a.a(adsizeparcel);
    }

    public void a(zzd zzd)
    {
        a.a(zzd);
    }

    public void a(ap ap, boolean flag)
    {
        a.a(ap, flag);
    }

    public void a(String s1)
    {
        a.a(s1);
    }

    public void a(String s1, String s2)
    {
        a.a(s1, s2);
    }

    public void a(String s1, Map map)
    {
        a.a(s1, map);
    }

    public void a(String s1, JSONObject jsonobject)
    {
        a.a(s1, jsonobject);
    }

    public void a(boolean flag)
    {
        a.a(flag);
    }

    public View b()
    {
        return this;
    }

    public void b(int i1)
    {
        a.b(i1);
    }

    public void b(zzd zzd)
    {
        a.b(zzd);
    }

    public void b(String s1)
    {
        a.b(s1);
    }

    public void b(String s1, JSONObject jsonobject)
    {
        a.b(s1, jsonobject);
    }

    public void b(boolean flag)
    {
        a.b(flag);
    }

    public void c()
    {
        a.c();
    }

    public void c(boolean flag)
    {
        a.c(flag);
    }

    public void clearCache(boolean flag)
    {
        a.clearCache(flag);
    }

    public void d()
    {
        a.d();
    }

    public void destroy()
    {
        a.destroy();
    }

    public Activity e()
    {
        return a.e();
    }

    public Context f()
    {
        return a.f();
    }

    public com.google.android.gms.ads.internal.zzd g()
    {
        return a.g();
    }

    public zzd h()
    {
        return a.h();
    }

    public zzd i()
    {
        return a.i();
    }

    public AdSizeParcel j()
    {
        return a.j();
    }

    public tv k()
    {
        return a.k();
    }

    public boolean l()
    {
        return a.l();
    }

    public void loadData(String s1, String s2, String s3)
    {
        a.loadData(s1, s2, s3);
    }

    public void loadDataWithBaseURL(String s1, String s2, String s3, String s4, String s5)
    {
        a.loadDataWithBaseURL(s1, s2, s3, s4, s5);
    }

    public void loadUrl(String s1)
    {
        a.loadUrl(s1);
    }

    public ab m()
    {
        return a.m();
    }

    public VersionInfoParcel n()
    {
        return a.n();
    }

    public boolean o()
    {
        return a.o();
    }

    public void onPause()
    {
        b.b();
        a.onPause();
    }

    public void onResume()
    {
        a.onResume();
    }

    public int p()
    {
        return a.p();
    }

    public boolean q()
    {
        return a.q();
    }

    public void r()
    {
        b.c();
        a.r();
    }

    public boolean s()
    {
        return a.s();
    }

    public void setBackgroundColor(int i1)
    {
        a.setBackgroundColor(i1);
    }

    public void setOnClickListener(android.view.View.OnClickListener onclicklistener)
    {
        a.setOnClickListener(onclicklistener);
    }

    public void setOnTouchListener(android.view.View.OnTouchListener ontouchlistener)
    {
        a.setOnTouchListener(ontouchlistener);
    }

    public void setWebChromeClient(WebChromeClient webchromeclient)
    {
        a.setWebChromeClient(webchromeclient);
    }

    public void setWebViewClient(WebViewClient webviewclient)
    {
        a.setWebViewClient(webviewclient);
    }

    public void stopLoading()
    {
        a.stopLoading();
    }

    public String t()
    {
        return a.t();
    }

    public tt u()
    {
        return b;
    }

    public do v()
    {
        return a.v();
    }

    public dp w()
    {
        return a.w();
    }

    public void x()
    {
        a.x();
    }

    public void y()
    {
        a.y();
    }
}
