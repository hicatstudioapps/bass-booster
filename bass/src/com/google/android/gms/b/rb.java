// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.concurrent.Future;

// Referenced classes of package com.google.android.gms.b:
//            re, rd, rc, rf, 
//            rg, rh, rj

public final class rb
{

    static SharedPreferences a(Context context)
    {
        return b(context);
    }

    public static Future a(Context context, int i)
    {
        return (new re(context, i)).zzgX();
    }

    public static Future a(Context context, rj rj)
    {
        return (new rd(context, rj)).zzgX();
    }

    public static Future a(Context context, boolean flag)
    {
        return (new rc(context, flag)).zzgX();
    }

    private static SharedPreferences b(Context context)
    {
        return context.getSharedPreferences("admob", 0);
    }

    public static Future b(Context context, rj rj)
    {
        return (new rf(context, rj)).zzgX();
    }

    public static Future b(Context context, boolean flag)
    {
        return (new rg(context, flag)).zzgX();
    }

    public static Future c(Context context, rj rj)
    {
        return (new rh(context, rj)).zzgX();
    }
}
