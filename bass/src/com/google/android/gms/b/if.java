// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.a.a;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzu;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.ads.internal.zzp;

// Referenced classes of package com.google.android.gms.b:
//            hh, hz, ib, ie, 
//            hi, nf, jx, dw, 
//            mt

public class if extends com.google.android.gms.ads.internal.client.zzs.zza
{

    private String a;
    private hh b;
    private zzk c;
    private hz d;
    private nf e;
    private String f;

    public if(Context context, String s, jx jx, VersionInfoParcel versioninfoparcel, zzd zzd)
    {
        this(s, new hh(context.getApplicationContext(), jx, versioninfoparcel, zzd));
    }

    public if(String s, hh hh1)
    {
        a = s;
        b = hh1;
        d = new hz();
        zzp.zzbI().a(hh1);
    }

    private void b()
    {
        if (c != null && e != null)
        {
            c.zza(e, f);
        }
    }

    void a()
    {
        if (c != null)
        {
            return;
        } else
        {
            c = b.a(a);
            d.a(c);
            b();
            return;
        }
    }

    public void destroy()
    {
        if (c != null)
        {
            c.destroy();
        }
    }

    public String getMediationAdapterClassName()
    {
        if (c != null)
        {
            return c.getMediationAdapterClassName();
        } else
        {
            return null;
        }
    }

    public boolean isLoading()
    {
        return c != null && c.isLoading();
    }

    public boolean isReady()
    {
        return c != null && c.isReady();
    }

    public void pause()
    {
        if (c != null)
        {
            c.pause();
        }
    }

    public void resume()
    {
        if (c != null)
        {
            c.resume();
        }
    }

    public void setManualImpressionsEnabled(boolean flag)
    {
        a();
        if (c != null)
        {
            c.setManualImpressionsEnabled(flag);
        }
    }

    public void showInterstitial()
    {
        if (c != null)
        {
            c.showInterstitial();
            return;
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Interstitial ad must be loaded before showInterstitial().");
            return;
        }
    }

    public void stopLoading()
    {
        if (c != null)
        {
            c.stopLoading();
        }
    }

    public void zza(AdSizeParcel adsizeparcel)
    {
        if (c != null)
        {
            c.zza(adsizeparcel);
        }
    }

    public void zza(zzn zzn)
    {
        d.e = zzn;
        if (c != null)
        {
            d.a(c);
        }
    }

    public void zza(zzo zzo)
    {
        d.a = zzo;
        if (c != null)
        {
            d.a(c);
        }
    }

    public void zza(zzu zzu)
    {
        d.b = zzu;
        if (c != null)
        {
            d.a(c);
        }
    }

    public void zza(zzv zzv)
    {
        a();
        if (c != null)
        {
            c.zza(zzv);
        }
    }

    public void zza(dw dw)
    {
        d.d = dw;
        if (c != null)
        {
            d.a(c);
        }
    }

    public void zza(mt mt)
    {
        d.c = mt;
        if (c != null)
        {
            d.a(c);
        }
    }

    public void zza(nf nf, String s)
    {
        e = nf;
        f = s;
        b();
    }

    public a zzaO()
    {
        if (c != null)
        {
            return c.zzaO();
        } else
        {
            return null;
        }
    }

    public AdSizeParcel zzaP()
    {
        if (c != null)
        {
            return c.zzaP();
        } else
        {
            return null;
        }
    }

    public void zzaR()
    {
        if (c != null)
        {
            c.zzaR();
            return;
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Interstitial ad must be loaded before pingManualTrackingUrl().");
            return;
        }
    }

    public boolean zzb(AdRequestParcel adrequestparcel)
    {
        if (adrequestparcel.zztx != null)
        {
            a();
        }
        if (c != null)
        {
            return c.zzb(adrequestparcel);
        }
        ie ie1 = zzp.zzbI().a(adrequestparcel, a);
        if (ie1 != null)
        {
            if (!ie1.e)
            {
                ie1.a(adrequestparcel);
            }
            c = ie1.a;
            ie1.a(b);
            ie1.c.a(d);
            d.a(c);
            b();
            return ie1.f;
        } else
        {
            c = b.a(a);
            d.a(c);
            b();
            return c.zzb(adrequestparcel);
        }
    }
}
