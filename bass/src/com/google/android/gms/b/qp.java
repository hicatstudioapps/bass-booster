// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            qq, tu, tv, jh, 
//            ka, ji, jk

public class qp
{

    public final AdRequestParcel a;
    public final tu b;
    public final List c;
    public final int d;
    public final List e;
    public final List f;
    public final int g;
    public final long h;
    public final String i;
    public final JSONObject j;
    public final boolean k;
    public final jh l;
    public final ka m;
    public final String n;
    public final ji o;
    public final jk p;
    public final long q;
    public final AdSizeParcel r;
    public final long s;
    public final long t;
    public final long u;
    public final String v;
    public final com.google.android.gms.ads.internal.formats.zzh.zza w;

    public qp(AdRequestParcel adrequestparcel, tu tu1, List list, int i1, List list1, List list2, int j1, 
            long l1, String s1, boolean flag, jh jh, ka ka, String s2, 
            ji ji, jk jk, long l2, AdSizeParcel adsizeparcel, long l3, 
            long l4, long l5, String s3, JSONObject jsonobject, com.google.android.gms.ads.internal.formats.zzh.zza zza)
    {
        a = adrequestparcel;
        b = tu1;
        if (list != null)
        {
            adrequestparcel = Collections.unmodifiableList(list);
        } else
        {
            adrequestparcel = null;
        }
        c = adrequestparcel;
        d = i1;
        if (list1 != null)
        {
            adrequestparcel = Collections.unmodifiableList(list1);
        } else
        {
            adrequestparcel = null;
        }
        e = adrequestparcel;
        if (list2 != null)
        {
            adrequestparcel = Collections.unmodifiableList(list2);
        } else
        {
            adrequestparcel = null;
        }
        f = adrequestparcel;
        g = j1;
        h = l1;
        i = s1;
        k = flag;
        l = jh;
        m = ka;
        n = s2;
        o = ji;
        p = jk;
        q = l2;
        r = adsizeparcel;
        s = l3;
        t = l4;
        u = l5;
        v = s3;
        j = jsonobject;
        w = zza;
    }

    public qp(qq qq1, tu tu1, jh jh, ka ka, String s1, jk jk, com.google.android.gms.ads.internal.formats.zzh.zza zza)
    {
        this(qq1.a.zzGq, tu1, qq1.b.zzAQ, qq1.e, qq1.b.zzAR, qq1.b.zzGP, qq1.b.orientation, qq1.b.zzAU, qq1.a.zzGt, qq1.b.zzGN, jh, ka, s1, qq1.c, jk, qq1.b.zzGO, qq1.d, qq1.b.zzGM, qq1.f, qq1.g, qq1.b.zzGS, qq1.h, zza);
    }

    public boolean a()
    {
        if (b == null || b.k() == null)
        {
            return false;
        } else
        {
            return b.k().b();
        }
    }
}
