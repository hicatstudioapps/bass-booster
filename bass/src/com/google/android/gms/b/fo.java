// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, fp, tu

public final class fo
    implements gd
{

    private final fp a;

    public fo(fp fp1)
    {
        a = fp1;
    }

    public void zza(tu tu, Map map)
    {
        tu = (String)map.get("name");
        if (tu == null)
        {
            zzb.zzaH("App event with no name parameter.");
            return;
        } else
        {
            a.onAppEvent(tu, (String)map.get("info"));
            return;
        }
    }
}
