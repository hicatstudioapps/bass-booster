// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.zzp;
import java.util.List;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.b:
//            cs, cx, dc, sy

public final class db
{

    public static final cs A = cs.a(0, "gads:looper_for_gms_client:enabled", Boolean.valueOf(true));
    public static final cs B = cs.a(0, "gads:sw_ad_request_service:enabled", Boolean.valueOf(true));
    public static final cs C = cs.a(0, "gads:sw_dynamite:enabled", Boolean.valueOf(true));
    public static final cs D = cs.a(0, "gad:mraid:url_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_banner.js");
    public static final cs E = cs.a(0, "gad:mraid:url_expanded_banner", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_expanded_banner.js");
    public static final cs F = cs.a(0, "gad:mraid:url_interstitial", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/mraid/v2/mraid_app_interstitial.js");
    public static final cs G = cs.a(0, "gads:enabled_sdk_csi", Boolean.valueOf(false));
    public static final cs H = cs.a(0, "gads:sdk_csi_server", "https://csi.gstatic.com/csi");
    public static final cs I = cs.a(0, "gads:sdk_csi_write_to_file", Boolean.valueOf(false));
    public static final cs J = cs.a(0, "gads:enable_content_fetching", Boolean.valueOf(true));
    public static final cs K = cs.a(0, "gads:content_length_weight", 1);
    public static final cs L = cs.a(0, "gads:content_age_weight", 1);
    public static final cs M = cs.a(0, "gads:min_content_len", 11);
    public static final cs N = cs.a(0, "gads:fingerprint_number", 10);
    public static final cs O = cs.a(0, "gads:sleep_sec", 10);
    public static final cs P = cs.a(0, "gad:app_index_enabled", Boolean.valueOf(true));
    public static final cs Q = cs.a(0, "gads:app_index:without_content_info_present:enabled", Boolean.valueOf(true));
    public static final cs R = cs.a(0, "gads:app_index:timeout_ms", 1000L);
    public static final cs S = cs.a(0, "gads:app_index:experiment_id");
    public static final cs T = cs.a(0, "gads:kitkat_interstitial_workaround:experiment_id");
    public static final cs U = cs.a(0, "gads:kitkat_interstitial_workaround:enabled", Boolean.valueOf(true));
    public static final cs V = cs.a(0, "gads:interstitial_follow_url", Boolean.valueOf(true));
    public static final cs W = cs.a(0, "gads:interstitial_follow_url:register_click", Boolean.valueOf(true));
    public static final cs X = cs.a(0, "gads:interstitial_follow_url:experiment_id");
    public static final cs Y = cs.a(0, "gads:analytics_enabled", Boolean.valueOf(true));
    public static final cs Z = cs.a(0, "gads:ad_key_enabled", Boolean.valueOf(false));
    public static final cs a = cs.a(0, "gads:sdk_core_experiment_id");
    public static final cs aA = cs.a(0, "gass:enable_int_signal", Boolean.valueOf(true));
    public static final cs aB = cs.a(0, "gads:adid_notification:first_party_check:enabled", Boolean.valueOf(true));
    public static final cs aC = cs.a(0, "gads:edu_device_helper:enabled", Boolean.valueOf(true));
    public static final cs aD = cs.a(0, "gads:support_screen_shot", Boolean.valueOf(true));
    public static final cs aE;
    public static final cs aa = cs.a(0, "gads:webview_cache_version", 0);
    public static final cs ab = cs.b(0, "gads:pan:experiment_id");
    public static final cs ac = cs.a(0, "gads:native:engine_url", "//googleads.g.doubleclick.net/mads/static/mad/sdk/native/native_ads.html");
    public static final cs ad = cs.a(0, "gads:ad_manager_creator:enabled", Boolean.valueOf(true));
    public static final cs ae = cs.a(1, "gads:interstitial_ad_pool:enabled", Boolean.valueOf(false));
    public static final cs af = cs.a(1, "gads:interstitial_ad_pool:schema", "customTargeting");
    public static final cs ag = cs.a(1, "gads:interstitial_ad_pool:max_pools", 3);
    public static final cs ah = cs.a(1, "gads:interstitial_ad_pool:max_pool_depth", 2);
    public static final cs ai = cs.a(1, "gads:interstitial_ad_pool:time_limit_sec", 1200);
    public static final cs aj = cs.a(1, "gads:interstitial_ad_pool:experiment_id");
    public static final cs ak = cs.a(0, "gads:log:verbose_enabled", Boolean.valueOf(false));
    public static final cs al = cs.a(0, "gads:device_info_caching:enabled", Boolean.valueOf(true));
    public static final cs am = cs.a(0, "gads:device_info_caching_expiry_ms:expiry", 0x493e0L);
    public static final cs an = cs.a(0, "gads:gen204_signals:enabled", Boolean.valueOf(false));
    public static final cs ao = cs.a(0, "gads:webview:error_reporting_enabled", Boolean.valueOf(false));
    public static final cs ap = cs.a(0, "gads:adid_reporting:enabled", Boolean.valueOf(false));
    public static final cs aq = cs.a(0, "gads:ad_settings_page_reporting:enabled", Boolean.valueOf(false));
    public static final cs ar = cs.a(0, "gads:adid_info_gmscore_upgrade_reporting:enabled", Boolean.valueOf(false));
    public static final cs as = cs.a(0, "gads:request_pkg:enabled", Boolean.valueOf(true));
    public static final cs at = cs.a(0, "gads:gmsg:disable_back_button:enabled", Boolean.valueOf(false));
    public static final cs au = cs.a(0, "gads:network:cache_prediction_duration_s", 300L);
    public static final cs av = cs.a(0, "gads:mediation:dynamite_first", Boolean.valueOf(true));
    public static final cs aw = cs.a(0, "gads:ad_loader:timeout_ms", 60000L);
    public static final cs ax = cs.a(0, "gads:rendering:timeout_ms", 60000L);
    public static final cs ay = cs.a(0, "gads:adshield:enable_adshield_instrumentation", Boolean.valueOf(false));
    public static final cs az = cs.a(0, "gass:enabled", Boolean.valueOf(false));
    public static final cs b = cs.a(0, "gads:sdk_core_location", "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html");
    public static final cs c = cs.a(0, "gads:request_builder:singleton_webview", Boolean.valueOf(false));
    public static final cs d = cs.a(0, "gads:request_builder:singleton_webview_experiment_id");
    public static final cs e = cs.a(0, "gads:sdk_use_dynamic_module", Boolean.valueOf(false));
    public static final cs f = cs.a(0, "gads:sdk_use_dynamic_module_experiment_id");
    public static final cs g = cs.a(0, "gads:sdk_crash_report_enabled", Boolean.valueOf(false));
    public static final cs h = cs.a(0, "gads:sdk_crash_report_full_stacktrace", Boolean.valueOf(false));
    public static final cs i = cs.a(0, "gads:block_autoclicks", Boolean.valueOf(false));
    public static final cs j = cs.a(0, "gads:block_autoclicks_experiment_id");
    public static final cs k = cs.b(0, "gads:prefetch:experiment_id");
    public static final cs l = cs.a(0, "gads:spam_app_context:experiment_id");
    public static final cs m = cs.a(0, "gads:spam_app_context:enabled", Boolean.valueOf(false));
    public static final cs n = cs.a(0, "gads:video_stream_cache:experiment_id");
    public static final cs o = cs.a(0, "gads:video_stream_cache:limit_count", 5);
    public static final cs p = cs.a(0, "gads:video_stream_cache:limit_space", 0x800000);
    public static final cs q = cs.a(0, "gads:video_stream_exo_cache:buffer_size", 0x800000);
    public static final cs r = cs.a(0, "gads:video_stream_cache:limit_time_sec", 300L);
    public static final cs s = cs.a(0, "gads:video_stream_cache:notify_interval_millis", 1000L);
    public static final cs t = cs.a(0, "gads:video_stream_cache:connect_timeout_millis", 10000);
    public static final cs u = cs.a(0, "gads:video:metric_reporting_enabled", Boolean.valueOf(false));
    public static final cs v = cs.a(0, "gads:video:metric_frame_hash_times", "");
    public static final cs w = cs.a(0, "gads:video:metric_frame_hash_time_leniency", 500L);
    public static final cs x = cs.b(0, "gads:spam_ad_id_decorator:experiment_id");
    public static final cs y = cs.a(0, "gads:spam_ad_id_decorator:enabled", Boolean.valueOf(false));
    public static final cs z = cs.b(0, "gads:looper_for_gms_client:experiment_id");

    public static List a()
    {
        return zzp.zzbF().a();
    }

    public static void a(Context context)
    {
        sy.a(new dc(context));
    }

    static 
    {
        aE = cs.a(0, "gads:js_flags:update_interval", TimeUnit.HOURS.toMillis(12L));
    }
}
