// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import com.google.android.gms.common.b;

abstract class xe extends BroadcastReceiver
{

    protected Context a;

    xe()
    {
    }

    public static xe a(Context context, xe xe1)
    {
        return a(context, xe1, com.google.android.gms.common.b.a());
    }

    public static xe a(Context context, xe xe1, b b1)
    {
        Object obj = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        ((IntentFilter) (obj)).addDataScheme("package");
        context.registerReceiver(xe1, ((IntentFilter) (obj)));
        xe1.a = context;
        obj = xe1;
        if (!b1.a(context, "com.google.android.gms"))
        {
            xe1.a();
            xe1.b();
            obj = null;
        }
        return ((xe) (obj));
    }

    protected abstract void a();

    public void b()
    {
        this;
        JVM INSTR monitorenter ;
        if (a != null)
        {
            a.unregisterReceiver(this);
        }
        a = null;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void onReceive(Context context, Intent intent)
    {
        intent = intent.getData();
        context = null;
        if (intent != null)
        {
            context = intent.getSchemeSpecificPart();
        }
        if ("com.google.android.gms".equals(context))
        {
            a();
            b();
        }
    }
}
