// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;

// Referenced classes of package com.google.android.gms.b:
//            yd

public class uf extends MutableContextWrapper
{

    private Activity a;
    private Context b;
    private Context c;

    public uf(Context context)
    {
        super(context);
        setBaseContext(context);
    }

    public Activity a()
    {
        return a;
    }

    public Context b()
    {
        return c;
    }

    public Object getSystemService(String s)
    {
        return c.getSystemService(s);
    }

    public void setBaseContext(Context context)
    {
        b = context.getApplicationContext();
        Activity activity;
        if (context instanceof Activity)
        {
            activity = (Activity)context;
        } else
        {
            activity = null;
        }
        a = activity;
        c = context;
        super.setBaseContext(b);
    }

    public void startActivity(Intent intent)
    {
        if (a != null && !yd.g())
        {
            a.startActivity(intent);
            return;
        } else
        {
            intent.setFlags(0x10000000);
            b.startActivity(intent);
            return;
        }
    }
}
