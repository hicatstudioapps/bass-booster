// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.IBinder;
import com.google.android.gms.common.api.ab;
import java.lang.ref.WeakReference;

// Referenced classes of package com.google.android.gms.b:
//            wx, wy, ws

class wv
    implements android.os.IBinder.DeathRecipient, wx
{

    private final WeakReference a;
    private final WeakReference b;
    private final WeakReference c;

    private wv(wy wy1, ab ab1, IBinder ibinder)
    {
        b = new WeakReference(ab1);
        a = new WeakReference(wy1);
        c = new WeakReference(ibinder);
    }

    wv(wy wy1, ab ab1, IBinder ibinder, ws ws)
    {
        this(wy1, ab1, ibinder);
    }

    private void a()
    {
        Object obj = (wy)a.get();
        ab ab1 = (ab)b.get();
        if (ab1 != null && obj != null)
        {
            ab1.a(((wy) (obj)).a().intValue());
        }
        obj = (IBinder)c.get();
        if (c != null)
        {
            ((IBinder) (obj)).unlinkToDeath(this, 0);
        }
    }

    public void a(wy wy1)
    {
        a();
    }

    public void binderDied()
    {
        a();
    }
}
