// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

// Referenced classes of package com.google.android.gms.b:
//            xf, vt, xc, wq, 
//            wz, wa, wc, wr, 
//            xg, vq, xb

public class xa
    implements xf
{

    final Map a;
    final Map b = new HashMap();
    final com.google.android.gms.common.internal.h c;
    final Map d;
    final g e;
    int f;
    final wr g;
    final xg h;
    private final Lock i;
    private final Condition j;
    private final Context k;
    private final b l;
    private final xc m;
    private volatile wz n;
    private ConnectionResult o;

    public xa(Context context, wr wr1, Lock lock, Looper looper, b b1, Map map, com.google.android.gms.common.internal.h h1, 
            Map map1, g g1, ArrayList arraylist, xg xg)
    {
        o = null;
        k = context;
        i = lock;
        l = b1;
        a = map;
        c = h1;
        d = map1;
        e = g1;
        g = wr1;
        h = xg;
        for (context = arraylist.iterator(); context.hasNext(); ((vt)context.next()).a(this)) { }
        m = new xc(this, looper);
        j = lock.newCondition();
        n = new wq(this);
    }

    static Lock a(xa xa1)
    {
        return xa1.i;
    }

    static wz b(xa xa1)
    {
        return xa1.n;
    }

    public vq a(vq vq)
    {
        return n.a(vq);
    }

    public void a()
    {
        n.c();
    }

    public void a(int i1)
    {
        i.lock();
        n.a(i1);
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    public void a(Bundle bundle)
    {
        i.lock();
        n.a(bundle);
        i.unlock();
        return;
        bundle;
        i.unlock();
        throw bundle;
    }

    void a(xb xb)
    {
        xb = m.obtainMessage(1, xb);
        m.sendMessage(xb);
    }

    void a(ConnectionResult connectionresult)
    {
        i.lock();
        o = connectionresult;
        n = new wq(this);
        n.a();
        j.signalAll();
        i.unlock();
        return;
        connectionresult;
        i.unlock();
        throw connectionresult;
    }

    public void a(ConnectionResult connectionresult, a a1, int i1)
    {
        i.lock();
        n.a(connectionresult, a1, i1);
        i.unlock();
        return;
        connectionresult;
        i.unlock();
        throw connectionresult;
    }

    void a(RuntimeException runtimeexception)
    {
        runtimeexception = m.obtainMessage(2, runtimeexception);
        m.sendMessage(runtimeexception);
    }

    public void a(String s, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        String s1 = (new StringBuilder()).append(s).append("  ").toString();
        a a1;
        for (Iterator iterator = d.keySet().iterator(); iterator.hasNext(); ((h)a.get(a1.c())).dump(s1, filedescriptor, printwriter, as))
        {
            a1 = (a)iterator.next();
            printwriter.append(s).append(a1.e()).println(":");
        }

    }

    public vq b(vq vq)
    {
        return n.b(vq);
    }

    public void b()
    {
        b.clear();
        n.b();
    }

    public boolean c()
    {
        return n instanceof wa;
    }

    void d()
    {
        i.lock();
        n = new wc(this, c, d, l, e, i, k);
        n.a();
        j.signalAll();
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    void e()
    {
        i.lock();
        g.j();
        n = new wa(this);
        n.a();
        j.signalAll();
        i.unlock();
        return;
        Exception exception;
        exception;
        i.unlock();
        throw exception;
    }

    void f()
    {
        for (Iterator iterator = a.values().iterator(); iterator.hasNext(); ((h)iterator.next()).disconnect()) { }
    }
}
