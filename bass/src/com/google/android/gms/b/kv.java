// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.location.Location;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import java.util.Date;
import java.util.List;
import java.util.Set;

public final class kv
    implements NativeMediationAdRequest
{

    private final Date a;
    private final int b;
    private final Set c;
    private final boolean d;
    private final Location e;
    private final int f;
    private final NativeAdOptionsParcel g;
    private final List h;
    private final boolean i;

    public kv(Date date, int j, Set set, Location location, boolean flag, int k, NativeAdOptionsParcel nativeadoptionsparcel, 
            List list, boolean flag1)
    {
        a = date;
        b = j;
        c = set;
        e = location;
        d = flag;
        f = k;
        g = nativeadoptionsparcel;
        h = list;
        i = flag1;
    }

    public Date getBirthday()
    {
        return a;
    }

    public int getGender()
    {
        return b;
    }

    public Set getKeywords()
    {
        return c;
    }

    public Location getLocation()
    {
        return e;
    }

    public NativeAdOptions getNativeAdOptions()
    {
        if (g == null)
        {
            return null;
        } else
        {
            return (new com.google.android.gms.ads.formats.NativeAdOptions.Builder()).setReturnUrlsForImageAssets(g.zzyc).setImageOrientation(g.zzyd).setRequestMultipleImages(g.zzye).build();
        }
    }

    public boolean isAppInstallAdRequested()
    {
        return h != null && h.contains("2");
    }

    public boolean isContentAdRequested()
    {
        return h != null && h.contains("1");
    }

    public boolean isDesignedForFamilies()
    {
        return i;
    }

    public boolean isTesting()
    {
        return d;
    }

    public int taggedForChildDirectedTreatment()
    {
        return f;
    }
}
