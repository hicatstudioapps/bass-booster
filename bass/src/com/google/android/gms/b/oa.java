// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.Context;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            nz, tu, qq, ny

public class oa extends nz
{

    private Object g;
    private PopupWindow h;
    private boolean i;

    oa(Context context, qq qq, tu tu1, ny ny)
    {
        super(context, qq, tu1, ny);
        g = new Object();
        i = false;
    }

    private void e()
    {
        synchronized (g)
        {
            i = true;
            if ((b instanceof Activity) && ((Activity)b).isDestroyed())
            {
                h = null;
            }
            if (h != null)
            {
                if (h.isShowing())
                {
                    h.dismiss();
                }
                h = null;
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void a(int j)
    {
        e();
        super.a(j);
    }

    public void cancel()
    {
        e();
        super.cancel();
    }

    protected void d()
    {
        FrameLayout framelayout;
        if (b instanceof Activity)
        {
            obj = ((Activity)b).getWindow();
        } else
        {
            obj = null;
        }
        while (obj == null || ((Window) (obj)).getDecorView() == null || ((Activity)b).isDestroyed()) 
        {
            return;
        }
        framelayout = new FrameLayout(b);
        framelayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(-1, -1));
        framelayout.addView(c.b(), -1, -1);
        synchronized (g)
        {
            if (!i)
            {
                break MISSING_BLOCK_LABEL_108;
            }
        }
        return;
        obj;
        obj1;
        JVM INSTR monitorexit ;
        throw obj;
        h = new PopupWindow(framelayout, 1, 1, false);
        h.setOutsideTouchable(true);
        h.setClippingEnabled(false);
        zzb.zzaF("Displaying the 1x1 popup off the screen.");
        h.showAtLocation(((Window) (obj)).getDecorView(), 0, -1, -1);
_L1:
        obj1;
        JVM INSTR monitorexit ;
        return;
        obj;
        h = null;
          goto _L1
    }
}
