// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            gd, gh, gi, rq, 
//            gj, gk, gf, rk, 
//            tu

public class ge
    implements gd
{

    private final Context a;
    private final VersionInfoParcel b;

    public ge(Context context, VersionInfoParcel versioninfoparcel)
    {
        a = context;
        b = versioninfoparcel;
    }

    protected gi a(JSONObject jsonobject)
    {
        String s = jsonobject.optString("http_request_id");
        Object obj = jsonobject.optString("url");
        String s1 = jsonobject.optString("post_body", null);
        JSONArray jsonarray;
        ArrayList arraylist;
        int i;
        try
        {
            obj = new URL(((String) (obj)));
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            zzb.zzb("Error constructing http request.", ((Throwable) (obj)));
            obj = null;
        }
        arraylist = new ArrayList();
        jsonarray = jsonobject.optJSONArray("headers");
        jsonobject = jsonarray;
        if (jsonarray == null)
        {
            jsonobject = new JSONArray();
        }
        i = 0;
        while (i < jsonobject.length()) 
        {
            JSONObject jsonobject1 = jsonobject.optJSONObject(i);
            if (jsonobject1 != null)
            {
                arraylist.add(new gh(jsonobject1.optString("key"), jsonobject1.optString("value")));
            }
            i++;
        }
        return new gi(s, ((URL) (obj)), arraylist, s1);
    }

    protected gj a(gi gi1)
    {
        HttpURLConnection httpurlconnection;
        try
        {
            httpurlconnection = (HttpURLConnection)gi1.b().openConnection();
            zzp.zzbx().a(a, b.afmaVersion, false, httpurlconnection);
            gh gh1;
            for (Iterator iterator = gi1.c().iterator(); iterator.hasNext(); httpurlconnection.addRequestProperty(gh1.a(), gh1.b()))
            {
                gh1 = (gh)iterator.next();
            }

        }
        // Misplaced declaration of an exception variable
        catch (gi gi1)
        {
            return new gj(this, false, null, gi1.toString());
        }
        ArrayList arraylist;
        if (!TextUtils.isEmpty(gi1.d()))
        {
            httpurlconnection.setDoOutput(true);
            byte abyte0[] = gi1.d().getBytes();
            httpurlconnection.setFixedLengthStreamingMode(abyte0.length);
            BufferedOutputStream bufferedoutputstream = new BufferedOutputStream(httpurlconnection.getOutputStream());
            bufferedoutputstream.write(abyte0);
            bufferedoutputstream.close();
        }
        arraylist = new ArrayList();
        if (httpurlconnection.getHeaderFields() != null)
        {
            for (Iterator iterator1 = httpurlconnection.getHeaderFields().entrySet().iterator(); iterator1.hasNext();)
            {
                java.util.Map.Entry entry = (java.util.Map.Entry)iterator1.next();
                Iterator iterator2 = ((List)entry.getValue()).iterator();
                while (iterator2.hasNext()) 
                {
                    String s = (String)iterator2.next();
                    arraylist.add(new gh((String)entry.getKey(), s));
                }
            }

        }
        gi1 = new gj(this, true, new gk(gi1.a(), httpurlconnection.getResponseCode(), arraylist, zzp.zzbx().a(new InputStreamReader(httpurlconnection.getInputStream()))), null);
        return gi1;
    }

    protected JSONObject a(gk gk1)
    {
        JSONObject jsonobject;
        JSONArray jsonarray;
        jsonobject = new JSONObject();
        try
        {
            jsonobject.put("http_request_id", gk1.a());
            if (gk1.d() != null)
            {
                jsonobject.put("body", gk1.d());
            }
            jsonarray = new JSONArray();
            gh gh1;
            for (Iterator iterator = gk1.c().iterator(); iterator.hasNext(); jsonarray.put((new JSONObject()).put("key", gh1.a()).put("value", gh1.b())))
            {
                gh1 = (gh)iterator.next();
            }

        }
        // Misplaced declaration of an exception variable
        catch (gk gk1)
        {
            zzb.zzb("Error constructing JSON for http response.", gk1);
            return jsonobject;
        }
        jsonobject.put("headers", jsonarray);
        jsonobject.put("response_code", gk1.b());
        return jsonobject;
    }

    public JSONObject a(String s)
    {
        String s1;
        JSONObject jsonobject;
        Object obj;
        try
        {
            obj = new JSONObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            zzb.e("The request is not a valid JSON.");
            try
            {
                s = (new JSONObject()).put("success", false);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return new JSONObject();
            }
            return s;
        }
        jsonobject = new JSONObject();
        s = "";
        try
        {
            s1 = ((JSONObject) (obj)).optString("http_request_id");
        }
        catch (Exception exception)
        {
            try
            {
                jsonobject.put("response", (new JSONObject()).put("http_request_id", s));
                jsonobject.put("success", false);
                jsonobject.put("reason", exception.toString());
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return jsonobject;
            }
            return jsonobject;
        }
        s = s1;
        obj = a(a(((JSONObject) (obj))));
        s = s1;
        if (!((gj) (obj)).c())
        {
            break MISSING_BLOCK_LABEL_119;
        }
        s = s1;
        jsonobject.put("response", a(((gj) (obj)).b()));
        s = s1;
        jsonobject.put("success", true);
        return jsonobject;
        s = s1;
        jsonobject.put("response", (new JSONObject()).put("http_request_id", s1));
        s = s1;
        jsonobject.put("success", false);
        s = s1;
        jsonobject.put("reason", ((gj) (obj)).a());
        return jsonobject;
    }

    public void zza(tu tu, Map map)
    {
        rk.a(new gf(this, map, tu));
    }
}
