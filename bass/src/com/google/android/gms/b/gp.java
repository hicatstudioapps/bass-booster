// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zze;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, xy, lo, ll, 
//            lr, lq, tu

public class gp
    implements gd
{

    static final Map a = xy.a("resize", Integer.valueOf(1), "playVideo", Integer.valueOf(2), "storePicture", Integer.valueOf(3), "createCalendarEvent", Integer.valueOf(4), "setOrientationProperties", Integer.valueOf(5), "closeResizedAd", Integer.valueOf(6));
    private final zze b;
    private final lo c;

    public gp(zze zze1, lo lo1)
    {
        b = zze1;
        c = lo1;
    }

    public void zza(tu tu, Map map)
    {
        String s = (String)map.get("a");
        int i = ((Integer)a.get(s)).intValue();
        if (i != 5 && b != null && !b.zzbg())
        {
            b.zzp(null);
            return;
        }
        switch (i)
        {
        case 2: // '\002'
        default:
            zzb.zzaG("Unknown MRAID command called.");
            return;

        case 1: // '\001'
            c.a(map);
            return;

        case 4: // '\004'
            (new ll(tu, map)).a();
            return;

        case 3: // '\003'
            (new lr(tu, map)).a();
            return;

        case 5: // '\005'
            (new lq(tu, map)).a();
            return;

        case 6: // '\006'
            c.a(true);
            break;
        }
    }

}
