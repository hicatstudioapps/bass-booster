// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// Referenced classes of package com.google.android.gms.b:
//            jg, rq, jt, jo, 
//            th, ji, xx, kg, 
//            jh, jl, js, rk, 
//            jx

public class jr
    implements jg
{

    private final AdRequestInfoParcel a;
    private final jx b;
    private final Context c;
    private final ji d;
    private final boolean e;
    private final long f;
    private final long g;
    private final int h;
    private final Object i = new Object();
    private boolean j;
    private final Map k = new HashMap();

    public jr(Context context, AdRequestInfoParcel adrequestinfoparcel, jx jx, ji ji1, boolean flag, long l, 
            long l1, int i1)
    {
        j = false;
        c = context;
        a = adrequestinfoparcel;
        b = jx;
        d = ji1;
        e = flag;
        f = l;
        g = l1;
        h = i1;
    }

    static Object a(jr jr1)
    {
        return jr1.i;
    }

    private void a(th th1)
    {
        rq.a.post(new jt(this, th1));
    }

    private jo b(List list)
    {
label0:
        {
            synchronized (i)
            {
                if (!j)
                {
                    break label0;
                }
                list = new jo(-1);
            }
            return list;
        }
        obj;
        JVM INSTR monitorexit ;
        obj = list.iterator();
_L2:
        if (!((Iterator) (obj)).hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        list = (th)((Iterator) (obj)).next();
        jo jo1 = (jo)list.get();
        if (jo1 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (jo1.a != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        a(list);
        return jo1;
        list;
_L3:
        zzb.zzd("Exception while processing an adapter; continuing with other adapters", list);
        if (true) goto _L2; else goto _L1
        list;
        obj;
        JVM INSTR monitorexit ;
        throw list;
_L1:
        a(((th) (null)));
        return new jo(1);
        list;
          goto _L3
    }

    static boolean b(jr jr1)
    {
        return jr1.j;
    }

    static long c(jr jr1)
    {
        return jr1.f;
    }

    private jo c(List list)
    {
label0:
        {
            synchronized (i)
            {
                if (!j)
                {
                    break label0;
                }
                list = new jo(-1);
            }
            return list;
        }
        obj;
        JVM INSTR monitorexit ;
        List list1;
        th th1;
        long l1;
        long l2;
        int l = -1;
        obj = null;
        list1 = null;
        Iterator iterator;
        kg kg1;
        int i1;
        if (d.k != -1L)
        {
            l1 = d.k;
        } else
        {
            l1 = 10000L;
        }
        iterator = list.iterator();
_L11:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        th1 = (th)iterator.next();
        l2 = zzp.zzbB().a();
        if (l1 != 0L) goto _L4; else goto _L3
_L3:
        if (!th1.isDone()) goto _L4; else goto _L5
_L5:
        list = (jo)th1.get();
_L6:
        if (list == null)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        if (((jo) (list)).a != 0)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        kg1 = ((jo) (list)).f;
        if (kg1 == null)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        if (kg1.a() <= l)
        {
            break MISSING_BLOCK_LABEL_318;
        }
        i1 = kg1.a();
        l = i1;
        obj = th1;
_L9:
        l1 = Math.max(l1 - (zzp.zzbB().a() - l2), 0L);
_L7:
        list1 = list;
        continue; /* Loop/switch isn't completed */
        list;
        obj;
        JVM INSTR monitorexit ;
        throw list;
_L4:
        list = (jo)th1.get(l1, TimeUnit.MILLISECONDS);
          goto _L6
        list;
_L8:
        zzb.zzd("Exception while processing an adapter; continuing with other adapters", list);
        l1 = Math.max(l1 - (zzp.zzbB().a() - l2), 0L);
        list = list1;
          goto _L7
        list;
        Math.max(l1 - (zzp.zzbB().a() - l2), 0L);
        throw list;
_L2:
        a(((th) (obj)));
        if (list1 == null)
        {
            return new jo(1);
        }
        break MISSING_BLOCK_LABEL_323;
        list;
          goto _L8
        list;
          goto _L8
        list;
          goto _L8
        list = list1;
          goto _L9
        return list1;
        if (true) goto _L11; else goto _L10
_L10:
    }

    static long d(jr jr1)
    {
        return jr1.g;
    }

    static Map e(jr jr1)
    {
        return jr1.k;
    }

    public jo a(List list)
    {
        zzb.zzaF("Starting mediation.");
        java.util.concurrent.ExecutorService executorservice = Executors.newCachedThreadPool();
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext();)
        {
            jh jh1 = (jh)list.next();
            zzb.zzaG((new StringBuilder()).append("Trying mediation network: ").append(jh1.b).toString());
            Iterator iterator = jh1.c.iterator();
            while (iterator.hasNext()) 
            {
                Object obj = (String)iterator.next();
                obj = new jl(c, ((String) (obj)), b, d, jh1, a.zzGq, a.zzqV, a.zzqR, e, a.zzrj, a.zzrl);
                th th1 = rk.a(executorservice, new js(this, ((jl) (obj))));
                k.put(th1, obj);
                arraylist.add(th1);
            }
        }

        switch (h)
        {
        default:
            return b(arraylist);

        case 2: // '\002'
            return c(arraylist);
        }
    }

    public void a()
    {
        Object obj = i;
        obj;
        JVM INSTR monitorenter ;
        j = true;
        for (Iterator iterator = k.values().iterator(); iterator.hasNext(); ((jl)iterator.next()).a()) { }
        break MISSING_BLOCK_LABEL_56;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        obj;
        JVM INSTR monitorexit ;
    }
}
