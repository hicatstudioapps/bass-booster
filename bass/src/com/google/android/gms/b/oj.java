// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import com.google.android.gms.ads.internal.formats.zzc;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package com.google.android.gms.b:
//            sp, oe, yb

class oj
    implements sp
{

    final boolean a;
    final double b;
    final String c;
    final oe d;

    oj(oe oe1, boolean flag, double d1, String s)
    {
        d = oe1;
        a = flag;
        b = d1;
        c = s;
        super();
    }

    public zzc a()
    {
        d.a(2, a);
        return null;
    }

    public zzc a(InputStream inputstream)
    {
        try
        {
            inputstream = yb.a(inputstream);
        }
        // Misplaced declaration of an exception variable
        catch (InputStream inputstream)
        {
            inputstream = null;
        }
        if (inputstream == null)
        {
            d.a(2, a);
            return null;
        }
        inputstream = BitmapFactory.decodeByteArray(inputstream, 0, inputstream.length);
        if (inputstream == null)
        {
            d.a(2, a);
            return null;
        } else
        {
            inputstream.setDensity((int)(160D * b));
            return new zzc(new BitmapDrawable(Resources.getSystem(), inputstream), Uri.parse(c), b);
        }
    }

    public Object b()
    {
        return a();
    }

    public Object b(InputStream inputstream)
    {
        return a(inputstream);
    }
}
