// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.io.IOException;

public class zh extends IOException
{

    public zh(String s)
    {
        super(s);
    }

    static zh a()
    {
        return new zh("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static zh b()
    {
        return new zh("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static zh c()
    {
        return new zh("CodedInputStream encountered a malformed varint.");
    }

    static zh d()
    {
        return new zh("Protocol message contained an invalid tag (zero).");
    }

    static zh e()
    {
        return new zh("Protocol message end-group tag did not match expected tag.");
    }

    static zh f()
    {
        return new zh("Protocol message tag had invalid wire type.");
    }

    static zh g()
    {
        return new zh("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
