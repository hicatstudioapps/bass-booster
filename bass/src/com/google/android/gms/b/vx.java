// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.b.a;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.av;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;

// Referenced classes of package com.google.android.gms.b:
//            xf, vt, vy, xa, 
//            vz, wr, vq, xi

public class vx
    implements xf
{

    private final Context a;
    private final wr b;
    private final Looper c;
    private final xa d;
    private final xa e;
    private final Map f = new a();
    private final Set g = Collections.newSetFromMap(new WeakHashMap());
    private final h h;
    private Bundle i;
    private ConnectionResult j;
    private ConnectionResult k;
    private final AtomicInteger l = new AtomicInteger(0);
    private final Lock m;
    private int n;

    public vx(Context context, wr wr1, Lock lock, Looper looper, b b1, Map map, com.google.android.gms.common.internal.h h1, 
            Map map1, g g1, ArrayList arraylist)
    {
        j = null;
        k = null;
        n = 0;
        a = context;
        b = wr1;
        m = lock;
        c = looper;
        wr1 = null;
        a a1 = new a();
        a a2 = new a();
        for (Iterator iterator1 = map.keySet().iterator(); iterator1.hasNext();)
        {
            i i1 = (i)iterator1.next();
            h h2 = (h)map.get(i1);
            if (h2.zzmJ())
            {
                wr1 = h2;
            }
            if (h2.zzmn())
            {
                a1.put(i1, h2);
            } else
            {
                a2.put(i1, h2);
            }
        }

        h = wr1;
        if (a1.isEmpty())
        {
            throw new IllegalStateException("CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        }
        wr1 = new a();
        map = new a();
        for (Iterator iterator = map1.keySet().iterator(); iterator.hasNext();)
        {
            com.google.android.gms.common.api.a a3 = (com.google.android.gms.common.api.a)iterator.next();
            i j1 = a3.c();
            if (a1.containsKey(j1))
            {
                wr1.put(a3, map1.get(a3));
            } else
            if (a2.containsKey(j1))
            {
                map.put(a3, map1.get(a3));
            } else
            {
                throw new IllegalStateException("Each API in the apiTypeMap must have a corresponding client in the clients map.");
            }
        }

        map1 = new ArrayList();
        ArrayList arraylist1 = new ArrayList();
        for (arraylist = arraylist.iterator(); arraylist.hasNext();)
        {
            vt vt1 = (vt)arraylist.next();
            if (wr1.containsKey(vt1.a))
            {
                map1.add(vt1);
            } else
            if (map.containsKey(vt1.a))
            {
                arraylist1.add(vt1);
            } else
            {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the apiTypeMap");
            }
        }

        arraylist = new vy(this);
        d = new xa(context, b, lock, looper, b1, a2, null, map, null, arraylist1, arraylist);
        map = new vz(this);
        e = new xa(context, b, lock, looper, b1, a1, h1, wr1, g1, map1, map);
        for (context = a2.keySet().iterator(); context.hasNext(); f.put(wr1, d))
        {
            wr1 = (i)context.next();
        }

        for (context = a1.keySet().iterator(); context.hasNext(); f.put(wr1, e))
        {
            wr1 = (i)context.next();
        }

    }

    static ConnectionResult a(vx vx1, ConnectionResult connectionresult)
    {
        vx1.j = connectionresult;
        return connectionresult;
    }

    static Lock a(vx vx1)
    {
        return vx1.m;
    }

    private void a(Bundle bundle)
    {
        if (i == null)
        {
            i = bundle;
        } else
        if (bundle != null)
        {
            i.putAll(bundle);
            return;
        }
    }

    static void a(vx vx1, Bundle bundle)
    {
        vx1.a(bundle);
    }

    static void a(vx vx1, xa xa1, int i1)
    {
        vx1.a(xa1, i1);
    }

    private void a(xa xa1, int i1)
    {
        if (l.getAndIncrement() % 2 == 1)
        {
            b.a_(i1);
        }
        xa1.a(i1);
        k = null;
        j = null;
    }

    private void a(ConnectionResult connectionresult)
    {
        n;
        JVM INSTR tableswitch 1 2: default 28
    //                   1 57
    //                   2 49;
           goto _L1 _L2 _L3
_L1:
        Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CALLBACK_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
_L5:
        n = 0;
        return;
_L3:
        b.a(connectionresult);
_L2:
        h();
        if (true) goto _L5; else goto _L4
_L4:
    }

    static ConnectionResult b(vx vx1, ConnectionResult connectionresult)
    {
        vx1.k = connectionresult;
        return connectionresult;
    }

    static void b(vx vx1)
    {
        vx1.f();
    }

    private static boolean b(ConnectionResult connectionresult)
    {
        return connectionresult != null && connectionresult.b();
    }

    static xa c(vx vx1)
    {
        return vx1.e;
    }

    private boolean c(vq vq1)
    {
        vq1 = vq1.b();
        av.b(f.containsKey(vq1), "GoogleApiClient is not configured to use the API required for this call.");
        return ((xa)f.get(vq1)).equals(e);
    }

    static xa d(vx vx1)
    {
        return vx1.d;
    }

    private void e()
    {
        k = null;
        j = null;
        d.a();
        e.a();
    }

    private void f()
    {
        if (!b(j)) goto _L2; else goto _L1
_L1:
        if (!b(k) && !i()) goto _L4; else goto _L3
_L3:
        g();
_L6:
        return;
_L4:
        if (k != null)
        {
            if (n == 1)
            {
                h();
                return;
            } else
            {
                a(k);
                d.b();
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (j != null && b(k))
        {
            e.b();
            a(j);
            return;
        }
        if (j != null && k != null)
        {
            ConnectionResult connectionresult = j;
            if (e.f < d.f)
            {
                connectionresult = k;
            }
            a(connectionresult);
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void g()
    {
        n;
        JVM INSTR tableswitch 1 2: default 28
    //                   1 60
    //                   2 49;
           goto _L1 _L2 _L3
_L1:
        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CALLBACK_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
_L5:
        n = 0;
        return;
_L3:
        b.a(i);
_L2:
        h();
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void h()
    {
        for (Iterator iterator = g.iterator(); iterator.hasNext(); ((xi)iterator.next()).a()) { }
        g.clear();
    }

    private boolean i()
    {
        return k != null && k.c() == 4;
    }

    private PendingIntent j()
    {
        if (h == null)
        {
            return null;
        } else
        {
            return PendingIntent.getActivity(a, b.l(), h.zzmK(), 0x8000000);
        }
    }

    public vq a(vq vq1)
    {
        if (vq1.d() == 1)
        {
            throw new IllegalStateException("ReportingServices.getReportingState is not supported with SIGN_IN_MODE_OPTIONAL.");
        }
        if (c(vq1))
        {
            if (i())
            {
                vq1.c(new Status(4, null, j()));
                return vq1;
            } else
            {
                return e.a(vq1);
            }
        } else
        {
            return d.a(vq1);
        }
    }

    public void a()
    {
        n = 2;
        e();
    }

    public void a(String s, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        printwriter.append(s).append("authClient").println(":");
        e.a((new StringBuilder()).append(s).append("  ").toString(), filedescriptor, printwriter, as);
        printwriter.append(s).append("unauthClient").println(":");
        d.a((new StringBuilder()).append(s).append("  ").toString(), filedescriptor, printwriter, as);
    }

    public vq b(vq vq1)
    {
        if (c(vq1))
        {
            if (i())
            {
                vq1.c(new Status(4, null, j()));
                return vq1;
            } else
            {
                return e.b(vq1);
            }
        } else
        {
            return d.b(vq1);
        }
    }

    public void b()
    {
        k = null;
        j = null;
        n = 0;
        d.b();
        e.b();
        h();
    }

    public boolean c()
    {
        boolean flag1;
        flag1 = true;
        m.lock();
        if (!d.c()) goto _L2; else goto _L1
_L1:
        boolean flag = flag1;
        if (d()) goto _L4; else goto _L3
_L3:
        flag = flag1;
        if (i()) goto _L4; else goto _L5
_L5:
        int i1 = n;
        if (i1 != 1) goto _L2; else goto _L6
_L6:
        flag = flag1;
_L4:
        m.unlock();
        return flag;
_L2:
        flag = false;
        if (true) goto _L4; else goto _L7
_L7:
        Exception exception;
        exception;
        m.unlock();
        throw exception;
    }

    public boolean d()
    {
        return e.c();
    }
}
