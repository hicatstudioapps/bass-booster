// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.support.v4.app.q;
import android.support.v4.app.x;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.b;
import com.google.android.gms.common.e;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            xj, xm, xe

class xl
    implements Runnable
{

    final xj a;
    private final int b;
    private final ConnectionResult c;

    public xl(xj xj1, int i, ConnectionResult connectionresult)
    {
        a = xj1;
        super();
        b = i;
        c = connectionresult;
    }

    public void run()
    {
        if (!xj.a(a) || com.google.android.gms.b.xj.b(a))
        {
            return;
        }
        xj.a(a, true);
        xj.a(a, b);
        xj.a(a, c);
        if (c.a())
        {
            try
            {
                int i = a.g().f().d().indexOf(a);
                c.a(a.g(), (i + 1 << 16) + 1);
                return;
            }
            catch (android.content.IntentSender.SendIntentException sendintentexception)
            {
                xj.c(a);
            }
            return;
        }
        if (xj.a().a(c.c()))
        {
            e.a(c.c(), a.g(), a, 2, a);
            return;
        }
        if (c.c() == 18)
        {
            android.app.Dialog dialog = xj.a().a(a.g(), a);
            xj.a(a, xe.a(a.g().getApplicationContext(), new xm(this, dialog)));
            return;
        } else
        {
            xj.a(a, b, c);
            return;
        }
    }
}
