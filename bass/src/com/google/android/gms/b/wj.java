// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.u;
import com.google.android.gms.common.internal.av;
import java.lang.ref.WeakReference;
import java.util.concurrent.locks.Lock;

// Referenced classes of package com.google.android.gms.b:
//            wc, xa, wr

class wj
    implements u
{

    private final WeakReference a;
    private final a b;
    private final int c;

    public wj(wc wc1, a a1, int i)
    {
        a = new WeakReference(wc1);
        b = a1;
        c = i;
    }

    public void a(ConnectionResult connectionresult)
    {
        wc wc1;
        boolean flag = false;
        wc1 = (wc)a.get();
        if (wc1 == null)
        {
            return;
        }
        if (Looper.myLooper() == wc.d(wc1).g.a())
        {
            flag = true;
        }
        av.a(flag, "onReportServiceBinding must be called on the GoogleApiClient handler thread");
        wc.c(wc1).lock();
        boolean flag1 = com.google.android.gms.b.wc.a(wc1, 0);
        if (!flag1)
        {
            wc.c(wc1).unlock();
            return;
        }
        if (!connectionresult.b())
        {
            com.google.android.gms.b.wc.a(wc1, connectionresult, b, c);
        }
        if (wc.k(wc1))
        {
            wc.l(wc1);
        }
        wc.c(wc1).unlock();
        return;
        connectionresult;
        wc.c(wc1).unlock();
        throw connectionresult;
    }

    public void b(ConnectionResult connectionresult)
    {
        wc wc1;
        boolean flag = true;
        wc1 = (wc)a.get();
        if (wc1 == null)
        {
            return;
        }
        if (Looper.myLooper() != wc.d(wc1).g.a())
        {
            flag = false;
        }
        av.a(flag, "onReportAccountValidation must be called on the GoogleApiClient handler thread");
        wc.c(wc1).lock();
        flag = com.google.android.gms.b.wc.a(wc1, 1);
        if (!flag)
        {
            wc.c(wc1).unlock();
            return;
        }
        if (!connectionresult.b())
        {
            com.google.android.gms.b.wc.a(wc1, connectionresult, b, c);
        }
        if (wc.k(wc1))
        {
            wc.m(wc1);
        }
        wc.c(wc1).unlock();
        return;
        connectionresult;
        wc.c(wc1).unlock();
        throw connectionresult;
    }
}
