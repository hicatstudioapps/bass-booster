// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.gms.ads.internal.overlay.zzk;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.b:
//            tu, dp, dj, tv

public class tt
{

    private final tu a;
    private final Context b;
    private final ViewGroup c;
    private zzk d;

    public tt(Context context, ViewGroup viewgroup, tu tu1)
    {
        this(context, viewgroup, tu1, null);
    }

    tt(Context context, ViewGroup viewgroup, tu tu1, zzk zzk1)
    {
        b = context;
        c = viewgroup;
        a = tu1;
        d = zzk1;
    }

    public zzk a()
    {
        av.b("getAdVideoUnderlay must be called from the UI thread.");
        return d;
    }

    public void a(int i, int j, int k, int l)
    {
        av.b("The underlay may only be modified from the UI thread.");
        if (d != null)
        {
            d.zzd(i, j, k, l);
        }
    }

    public void a(int i, int j, int k, int l, int i1)
    {
        if (d != null)
        {
            return;
        } else
        {
            dj.a(a.w().a(), a.v(), new String[] {
                "vpr"
            });
            do do1 = dj.a(a.w().a());
            d = new zzk(b, a, i1, a.w().a(), do1);
            c.addView(d, 0, new android.view.ViewGroup.LayoutParams(-1, -1));
            d.zzd(i, j, k, l);
            a.k().a(false);
            return;
        }
    }

    public void b()
    {
        av.b("onPause must be called from the UI thread.");
        if (d != null)
        {
            d.pause();
        }
    }

    public void c()
    {
        av.b("onDestroy must be called from the UI thread.");
        if (d != null)
        {
            d.destroy();
        }
    }
}
