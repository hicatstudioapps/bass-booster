// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            hi, hk, hl, hm, 
//            hn, ho

class hj extends com.google.android.gms.ads.internal.client.zzo.zza
{

    final hi a;

    hj(hi hi1)
    {
        a = hi1;
        super();
    }

    public void onAdClosed()
    {
        hi.a(a).add(new hk(this));
    }

    public void onAdFailedToLoad(int i)
    {
        hi.a(a).add(new hl(this, i));
        zzb.v("Pooled interstitial failed to load.");
    }

    public void onAdLeftApplication()
    {
        hi.a(a).add(new hm(this));
    }

    public void onAdLoaded()
    {
        hi.a(a).add(new hn(this));
        zzb.v("Pooled interstitial loaded.");
    }

    public void onAdOpened()
    {
        hi.a(a).add(new ho(this));
    }
}
