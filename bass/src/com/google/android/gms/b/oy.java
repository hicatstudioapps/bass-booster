// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;

// Referenced classes of package com.google.android.gms.b:
//            ub, qt, tu, pe, 
//            dq, ot, tv, fs, 
//            cp, do

final class oy
    implements Runnable
{

    final Context a;
    final AdRequestInfoParcel b;
    final pe c;
    final dq d;
    final do e;
    final String f;
    final cp g;

    oy(Context context, AdRequestInfoParcel adrequestinfoparcel, pe pe1, dq dq1, do do1, String s, cp cp1)
    {
        a = context;
        b = adrequestinfoparcel;
        c = pe1;
        d = dq1;
        e = do1;
        f = s;
        g = cp1;
        super();
    }

    public void run()
    {
        tu tu1 = zzp.zzby().a(a, new AdSizeParcel(), false, false, null, b.zzqR);
        if (zzp.zzbA().k())
        {
            tu1.clearCache(true);
        }
        tu1.a().setWillNotDraw(true);
        c.a(tu1);
        d.a(e, new String[] {
            "rwc"
        });
        Object obj = d.a();
        obj = ot.a(f, d, ((do) (obj)));
        tv tv1 = tu1.k();
        tv1.a("/invalidRequest", c.c);
        tv1.a("/loadAdURL", c.d);
        tv1.a("/log", fs.h);
        tv1.a(((tx) (obj)));
        zzb.zzaF("Loading the JS library.");
        tu1.loadUrl(g.a());
    }
}
