// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

// Referenced classes of package com.google.android.gms.b:
//            qy, qg, px, pz, 
//            qa, qq, ji, jh, 
//            qp, qi, qj

public class qh extends qy
    implements qg
{

    private final qq a;
    private final Context b;
    private final ArrayList c = new ArrayList();
    private final ArrayList d = new ArrayList();
    private final HashSet e = new HashSet();
    private final Object f = new Object();
    private final px g;
    private final String h;

    public qh(Context context, String s, qq qq1, px px1)
    {
        b = context;
        h = s;
        a = qq1;
        g = px1;
    }

    static px a(qh qh1)
    {
        return qh1.g;
    }

    private void a(String s, String s1)
    {
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        pz pz1 = g.c(s);
        if (pz1 == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        if (pz1.b() != null && pz1.a() != null)
        {
            break MISSING_BLOCK_LABEL_41;
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        s1 = new qa(b, s, h, s1, a, pz1, this);
        c.add(s1.zzgX());
        d.add(s);
        obj;
        JVM INSTR monitorexit ;
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void a(String s)
    {
        synchronized (f)
        {
            e.add(s);
        }
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void a(String s, int i)
    {
    }

    public void onStop()
    {
    }

    public void zzbp()
    {
        int i;
        for (Iterator iterator = a.c.a.iterator(); iterator.hasNext();)
        {
            Object obj3 = (jh)iterator.next();
            String s = ((jh) (obj3)).h;
            obj3 = ((jh) (obj3)).c.iterator();
            while (((Iterator) (obj3)).hasNext()) 
            {
                a((String)((Iterator) (obj3)).next(), s);
            }
        }

        i = 0;
_L2:
        if (i >= c.size())
        {
            break MISSING_BLOCK_LABEL_361;
        }
        ((Future)c.get(i)).get();
label0:
        {
            synchronized (f)
            {
                if (!e.contains(d.get(i)))
                {
                    break label0;
                }
                Object obj2 = (String)d.get(i);
                obj2 = new qp(a.a.zzGq, null, a.b.zzAQ, -2, a.b.zzAR, a.b.zzGP, a.b.orientation, a.b.zzAU, a.a.zzGt, a.b.zzGN, (jh)a.c.a.get(i), null, ((String) (obj2)), a.c, null, a.b.zzGO, a.d, a.b.zzGM, a.f, a.b.zzGR, a.b.zzGS, a.h, null);
                zza.zzLE.post(new qi(this, ((qp) (obj2))));
            }
            return;
        }
        obj;
        JVM INSTR monitorexit ;
        break MISSING_BLOCK_LABEL_545;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        Object obj1;
        obj1;
        qp qp1 = new qp(a.a.zzGq, null, a.b.zzAQ, 3, a.b.zzAR, a.b.zzGP, a.b.orientation, a.b.zzAU, a.a.zzGt, a.b.zzGN, null, null, null, a.c, null, a.b.zzGO, a.d, a.b.zzGM, a.f, a.b.zzGR, a.b.zzGS, a.h, null);
        zza.zzLE.post(new qj(this, qp1));
        return;
        qp1;
        i++;
        if (true) goto _L2; else goto _L1
_L1:
    }
}
