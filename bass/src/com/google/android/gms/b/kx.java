// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import android.os.RemoteException;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            ky, kd, ld, li, 
//            le, lj, kz, lf, 
//            la, lg, lb, lh, 
//            lc

public final class kx
    implements MediationBannerListener, MediationInterstitialListener
{

    private final kd a;

    public kx(kd kd1)
    {
        a = kd1;
    }

    static kd a(kx kx1)
    {
        return kx1.a;
    }

    public void onClick(MediationBannerAdapter mediationbanneradapter)
    {
        zzb.zzaF("Adapter called onClick.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onClick must be called on the main UI thread.");
            zza.zzLE.post(new ky(this));
            return;
        }
        try
        {
            a.a();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdClicked.", mediationbanneradapter);
        }
    }

    public void onDismissScreen(MediationBannerAdapter mediationbanneradapter)
    {
        zzb.zzaF("Adapter called onDismissScreen.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onDismissScreen must be called on the main UI thread.");
            zza.zzLE.post(new ld(this));
            return;
        }
        try
        {
            a.b();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdClosed.", mediationbanneradapter);
        }
    }

    public void onDismissScreen(MediationInterstitialAdapter mediationinterstitialadapter)
    {
        zzb.zzaF("Adapter called onDismissScreen.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onDismissScreen must be called on the main UI thread.");
            zza.zzLE.post(new li(this));
            return;
        }
        try
        {
            a.b();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationInterstitialAdapter mediationinterstitialadapter)
        {
            zzb.zzd("Could not call onAdClosed.", mediationinterstitialadapter);
        }
    }

    public void onFailedToReceiveAd(MediationBannerAdapter mediationbanneradapter, com.google.ads.AdRequest.ErrorCode errorcode)
    {
        zzb.zzaF((new StringBuilder()).append("Adapter called onFailedToReceiveAd with error. ").append(errorcode).toString());
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onFailedToReceiveAd must be called on the main UI thread.");
            zza.zzLE.post(new le(this, errorcode));
            return;
        }
        try
        {
            a.a(lj.a(errorcode));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdFailedToLoad.", mediationbanneradapter);
        }
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter mediationinterstitialadapter, com.google.ads.AdRequest.ErrorCode errorcode)
    {
        zzb.zzaF((new StringBuilder()).append("Adapter called onFailedToReceiveAd with error ").append(errorcode).append(".").toString());
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onFailedToReceiveAd must be called on the main UI thread.");
            zza.zzLE.post(new kz(this, errorcode));
            return;
        }
        try
        {
            a.a(lj.a(errorcode));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationInterstitialAdapter mediationinterstitialadapter)
        {
            zzb.zzd("Could not call onAdFailedToLoad.", mediationinterstitialadapter);
        }
    }

    public void onLeaveApplication(MediationBannerAdapter mediationbanneradapter)
    {
        zzb.zzaF("Adapter called onLeaveApplication.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onLeaveApplication must be called on the main UI thread.");
            zza.zzLE.post(new lf(this));
            return;
        }
        try
        {
            a.c();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdLeftApplication.", mediationbanneradapter);
        }
    }

    public void onLeaveApplication(MediationInterstitialAdapter mediationinterstitialadapter)
    {
        zzb.zzaF("Adapter called onLeaveApplication.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onLeaveApplication must be called on the main UI thread.");
            zza.zzLE.post(new la(this));
            return;
        }
        try
        {
            a.c();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationInterstitialAdapter mediationinterstitialadapter)
        {
            zzb.zzd("Could not call onAdLeftApplication.", mediationinterstitialadapter);
        }
    }

    public void onPresentScreen(MediationBannerAdapter mediationbanneradapter)
    {
        zzb.zzaF("Adapter called onPresentScreen.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onPresentScreen must be called on the main UI thread.");
            zza.zzLE.post(new lg(this));
            return;
        }
        try
        {
            a.d();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdOpened.", mediationbanneradapter);
        }
    }

    public void onPresentScreen(MediationInterstitialAdapter mediationinterstitialadapter)
    {
        zzb.zzaF("Adapter called onPresentScreen.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onPresentScreen must be called on the main UI thread.");
            zza.zzLE.post(new lb(this));
            return;
        }
        try
        {
            a.d();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationInterstitialAdapter mediationinterstitialadapter)
        {
            zzb.zzd("Could not call onAdOpened.", mediationinterstitialadapter);
        }
    }

    public void onReceivedAd(MediationBannerAdapter mediationbanneradapter)
    {
        zzb.zzaF("Adapter called onReceivedAd.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onReceivedAd must be called on the main UI thread.");
            zza.zzLE.post(new lh(this));
            return;
        }
        try
        {
            a.e();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationBannerAdapter mediationbanneradapter)
        {
            zzb.zzd("Could not call onAdLoaded.", mediationbanneradapter);
        }
    }

    public void onReceivedAd(MediationInterstitialAdapter mediationinterstitialadapter)
    {
        zzb.zzaF("Adapter called onReceivedAd.");
        if (!zzl.zzcN().zzhr())
        {
            zzb.zzaH("onReceivedAd must be called on the main UI thread.");
            zza.zzLE.post(new lc(this));
            return;
        }
        try
        {
            a.e();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (MediationInterstitialAdapter mediationinterstitialadapter)
        {
            zzb.zzd("Could not call onAdLoaded.", mediationinterstitialadapter);
        }
    }
}
