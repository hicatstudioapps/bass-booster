// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            av, aw, ax, bw, 
//            sw, an, qp, ih, 
//            aq, ar, iv, as, 
//            at, rt, au, bt, 
//            ay, tl, am, qt, 
//            rq, bf, xx, gd

public class ap
    implements android.view.ViewTreeObserver.OnGlobalLayoutListener, android.view.ViewTreeObserver.OnScrollChangedListener
{

    BroadcastReceiver a;
    private final Object b = new Object();
    private final WeakReference c;
    private WeakReference d;
    private final bw e;
    private final an f;
    private final Context g;
    private final ih h;
    private final iv i;
    private boolean j;
    private final WindowManager k;
    private final PowerManager l;
    private final KeyguardManager m;
    private bf n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private boolean s;
    private final HashSet t = new HashSet();
    private sw u;
    private final gd v = new av(this);
    private final gd w = new aw(this);
    private final gd x = new ax(this);

    public ap(Context context, AdSizeParcel adsizeparcel, qp qp1, VersionInfoParcel versioninfoparcel, bw bw1, ih ih1)
    {
        p = false;
        q = false;
        bw bw2 = bw1.c();
        h = ih1;
        c = new WeakReference(qp1);
        e = bw1;
        d = new WeakReference(null);
        r = true;
        u = new sw(200L);
        f = new an(UUID.randomUUID().toString(), versioninfoparcel, adsizeparcel.zztV, qp1.j, qp1.a(), adsizeparcel.zztY);
        i = h.b();
        k = (WindowManager)context.getSystemService("window");
        l = (PowerManager)context.getApplicationContext().getSystemService("power");
        m = (KeyguardManager)context.getSystemService("keyguard");
        g = context;
        try
        {
            context = a(bw2.a());
            i.a(new aq(this, context), new ar(this));
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzb("Failure while processing active view data.", context);
        }
        i.a(new as(this), new at(this));
        zzb.zzaF((new StringBuilder()).append("Tracking ad unit: ").append(f.d()).toString());
    }

    static an a(ap ap1)
    {
        return ap1.f;
    }

    static boolean a(ap ap1, boolean flag)
    {
        ap1.j = flag;
        return flag;
    }

    protected int a(int i1, DisplayMetrics displaymetrics)
    {
        float f1 = displaymetrics.density;
        return (int)((float)i1 / f1);
    }

    protected JSONObject a(View view)
    {
        if (view == null)
        {
            return j();
        }
        boolean flag = zzp.zzbz().a(view);
        int ai1[] = new int[2];
        int ai[] = new int[2];
        DisplayMetrics displaymetrics;
        Rect rect;
        Rect rect1;
        Rect rect2;
        Rect rect3;
        Rect rect4;
        JSONObject jsonobject;
        boolean flag1;
        boolean flag2;
        try
        {
            view.getLocationOnScreen(ai1);
            view.getLocationInWindow(ai);
        }
        catch (Exception exception)
        {
            zzb.zzb("Failure getting view location.", exception);
        }
        displaymetrics = view.getContext().getResources().getDisplayMetrics();
        rect = new Rect();
        rect.left = ai1[0];
        rect.top = ai1[1];
        rect.right = rect.left + view.getWidth();
        rect.bottom = rect.top + view.getHeight();
        rect1 = new Rect();
        rect1.right = k.getDefaultDisplay().getWidth();
        rect1.bottom = k.getDefaultDisplay().getHeight();
        rect2 = new Rect();
        flag1 = view.getGlobalVisibleRect(rect2, null);
        rect3 = new Rect();
        flag2 = view.getLocalVisibleRect(rect3);
        rect4 = new Rect();
        view.getHitRect(rect4);
        jsonobject = i();
        jsonobject.put("windowVisibility", view.getWindowVisibility()).put("isAttachedToWindow", flag).put("viewBox", (new JSONObject()).put("top", a(rect1.top, displaymetrics)).put("bottom", a(rect1.bottom, displaymetrics)).put("left", a(rect1.left, displaymetrics)).put("right", a(rect1.right, displaymetrics))).put("adBox", (new JSONObject()).put("top", a(rect.top, displaymetrics)).put("bottom", a(rect.bottom, displaymetrics)).put("left", a(rect.left, displaymetrics)).put("right", a(rect.right, displaymetrics))).put("globalVisibleBox", (new JSONObject()).put("top", a(rect2.top, displaymetrics)).put("bottom", a(rect2.bottom, displaymetrics)).put("left", a(rect2.left, displaymetrics)).put("right", a(rect2.right, displaymetrics))).put("globalVisibleBoxVisible", flag1).put("localVisibleBox", (new JSONObject()).put("top", a(rect3.top, displaymetrics)).put("bottom", a(rect3.bottom, displaymetrics)).put("left", a(rect3.left, displaymetrics)).put("right", a(rect3.right, displaymetrics))).put("localVisibleBoxVisible", flag2).put("hitBox", (new JSONObject()).put("top", a(rect4.top, displaymetrics)).put("bottom", a(rect4.bottom, displaymetrics)).put("left", a(rect4.left, displaymetrics)).put("right", a(rect4.right, displaymetrics))).put("screenDensity", displaymetrics.density).put("isVisible", b(view));
        return jsonobject;
    }

    protected void a()
    {
label0:
        {
            synchronized (b)
            {
                if (a == null)
                {
                    break label0;
                }
            }
            return;
        }
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.intent.action.SCREEN_ON");
        intentfilter.addAction("android.intent.action.SCREEN_OFF");
        a = new au(this);
        g.registerReceiver(a, intentfilter);
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void a(View view, Map map)
    {
        b(false);
    }

    public void a(am am1)
    {
        t.add(am1);
    }

    public void a(bf bf1)
    {
        synchronized (b)
        {
            n = bf1;
        }
        return;
        bf1;
        obj;
        JVM INSTR monitorexit ;
        throw bf1;
    }

    protected void a(bt bt1)
    {
        bt1.a("/updateActiveView", v);
        bt1.a("/untrackActiveViewUnit", w);
        bt1.a("/visibilityChanged", x);
    }

    protected void a(JSONObject jsonobject)
    {
        try
        {
            JSONArray jsonarray = new JSONArray();
            JSONObject jsonobject1 = new JSONObject();
            jsonarray.put(jsonobject);
            jsonobject1.put("units", jsonarray);
            i.a(new ay(this, jsonobject1), new tl());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            zzb.zzb("Skipping active view message.", jsonobject);
        }
    }

    protected void a(boolean flag)
    {
        for (Iterator iterator = t.iterator(); iterator.hasNext(); ((am)iterator.next()).a(this, flag)) { }
    }

    protected boolean a(Map map)
    {
        if (map == null)
        {
            return false;
        }
        map = (String)map.get("hashCode");
        boolean flag;
        if (!TextUtils.isEmpty(map) && map.equals(f.d()))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
    }

    protected void b()
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        BroadcastReceiver broadcastreceiver = a;
        if (broadcastreceiver == null) goto _L2; else goto _L1
_L1:
        g.unregisterReceiver(a);
_L3:
        a = null;
_L2:
        obj;
        JVM INSTR monitorexit ;
        return;
        Object obj1;
        obj1;
        zzb.zzb("Failed trying to unregister the receiver", ((Throwable) (obj1)));
          goto _L3
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        obj1;
        zzp.zzbA().a(((Throwable) (obj1)), true);
          goto _L3
    }

    protected void b(boolean flag)
    {
label0:
        {
            synchronized (b)
            {
                if (j && r)
                {
                    break label0;
                }
            }
            return;
        }
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_46;
        }
        if (u.a())
        {
            break MISSING_BLOCK_LABEL_46;
        }
        obj1;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj1;
        JVM INSTR monitorexit ;
        throw exception;
        if (!e.b())
        {
            break MISSING_BLOCK_LABEL_65;
        }
        d();
        obj1;
        JVM INSTR monitorexit ;
        return;
        a(a(e.a()));
_L2:
        g();
        e();
        obj1;
        JVM INSTR monitorexit ;
        return;
_L3:
        Object obj;
        zzb.zza("Active view update failed.", ((Throwable) (obj)));
        if (true) goto _L2; else goto _L1
_L1:
        obj;
          goto _L3
        obj;
          goto _L3
    }

    protected boolean b(View view)
    {
        return view.getVisibility() == 0 && view.isShown() && k() && (!m.inKeyguardRestrictedInputMode() || zzp.zzbx().a());
    }

    protected void c()
    {
        synchronized (b)
        {
            h();
            b();
            r = false;
            e();
            i.a();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void d()
    {
        Object obj = b;
        obj;
        JVM INSTR monitorenter ;
        if (!r) goto _L2; else goto _L1
_L1:
        s = true;
        a(l());
_L3:
        zzb.zzaF((new StringBuilder()).append("Untracking ad unit: ").append(f.d()).toString());
_L2:
        return;
        Object obj1;
        obj1;
        zzb.zzb("JSON failure while processing active view data.", ((Throwable) (obj1)));
          goto _L3
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        obj1;
        zzb.zzb("Failure while processing active view data.", ((Throwable) (obj1)));
          goto _L3
    }

    protected void e()
    {
        if (n != null)
        {
            n.a(this);
        }
    }

    public boolean f()
    {
        boolean flag;
        synchronized (b)
        {
            flag = r;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void g()
    {
        Object obj = e.c().a();
        if (obj != null)
        {
            ViewTreeObserver viewtreeobserver = (ViewTreeObserver)d.get();
            obj = ((View) (obj)).getViewTreeObserver();
            if (obj != viewtreeobserver)
            {
                h();
                if (!o || viewtreeobserver != null && viewtreeobserver.isAlive())
                {
                    o = true;
                    ((ViewTreeObserver) (obj)).addOnScrollChangedListener(this);
                    ((ViewTreeObserver) (obj)).addOnGlobalLayoutListener(this);
                }
                d = new WeakReference(obj);
                return;
            }
        }
    }

    protected void h()
    {
        ViewTreeObserver viewtreeobserver = (ViewTreeObserver)d.get();
        if (viewtreeobserver == null || !viewtreeobserver.isAlive())
        {
            return;
        } else
        {
            viewtreeobserver.removeOnScrollChangedListener(this);
            viewtreeobserver.removeGlobalOnLayoutListener(this);
            return;
        }
    }

    protected JSONObject i()
    {
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("afmaVersion", f.b()).put("activeViewJSON", f.c()).put("timestamp", zzp.zzbB().b()).put("adFormat", f.a()).put("hashCode", f.d()).put("isMraid", f.e()).put("isStopped", q).put("isPaused", p).put("isScreenOn", k()).put("isNative", f.f());
        return jsonobject;
    }

    protected JSONObject j()
    {
        return i().put("isAttachedToWindow", false).put("isScreenOn", k()).put("isVisible", false);
    }

    boolean k()
    {
        return l.isScreenOn();
    }

    protected JSONObject l()
    {
        JSONObject jsonobject = i();
        jsonobject.put("doneReasonCode", "u");
        return jsonobject;
    }

    public void m()
    {
        synchronized (b)
        {
            q = true;
            b(false);
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void n()
    {
        synchronized (b)
        {
            p = true;
            b(false);
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void o()
    {
        synchronized (b)
        {
            p = false;
            b(false);
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void onGlobalLayout()
    {
        b(false);
    }

    public void onScrollChanged()
    {
        b(true);
    }
}
