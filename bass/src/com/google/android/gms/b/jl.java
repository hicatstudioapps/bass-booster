// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.ads.mediation.AdUrlAdapter;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            jp, ji, jh, ka, 
//            jx, kg, jn, db, 
//            cs, kr, jk, rq, 
//            jm, jo

public final class jl
    implements jp
{

    private final String a;
    private final jx b;
    private final long c;
    private final ji d;
    private final jh e;
    private final AdRequestParcel f;
    private final AdSizeParcel g;
    private final Context h;
    private final Object i = new Object();
    private final VersionInfoParcel j;
    private final boolean k;
    private final NativeAdOptionsParcel l;
    private final List m;
    private ka n;
    private int o;
    private kg p;

    public jl(Context context, String s, jx jx1, ji ji1, jh jh1, AdRequestParcel adrequestparcel, AdSizeParcel adsizeparcel, 
            VersionInfoParcel versioninfoparcel, boolean flag, NativeAdOptionsParcel nativeadoptionsparcel, List list)
    {
        o = -2;
        h = context;
        b = jx1;
        e = jh1;
        long l1;
        if ("com.google.ads.mediation.customevent.CustomEventAdapter".equals(s))
        {
            a = b();
        } else
        {
            a = s;
        }
        d = ji1;
        if (ji1.b != -1L)
        {
            l1 = ji1.b;
        } else
        {
            l1 = 10000L;
        }
        c = l1;
        f = adrequestparcel;
        g = adsizeparcel;
        j = versioninfoparcel;
        k = flag;
        l = nativeadoptionsparcel;
        m = list;
    }

    static ka a(jl jl1, ka ka1)
    {
        jl1.n = ka1;
        return ka1;
    }

    static Object a(jl jl1)
    {
        return jl1.i;
    }

    private String a(String s)
    {
        if (s == null || !e() || b(2))
        {
            return s;
        }
        Object obj;
        try
        {
            obj = new JSONObject(s);
            ((JSONObject) (obj)).remove("cpm_floor_cents");
            obj = ((JSONObject) (obj)).toString();
        }
        catch (JSONException jsonexception)
        {
            zzb.zzaH("Could not remove field. Returning the original value");
            return s;
        }
        return ((String) (obj));
    }

    private void a(long l1, long l2, long l3, long l4)
    {
        do
        {
            if (o != -2)
            {
                return;
            }
            b(l1, l2, l3, l4);
        } while (true);
    }

    private void a(jk jk1)
    {
        String s;
        if ("com.google.ads.mediation.AdUrlAdapter".equals(a))
        {
            Bundle bundle1 = f.zztA.getBundle(a);
            Bundle bundle = bundle1;
            if (bundle1 == null)
            {
                bundle = new Bundle();
            }
            bundle.putString("sdk_less_network_id", e.b);
            f.zztA.putBundle(a, bundle);
        }
        s = a(e.h);
        if (j.zzLG >= 0x3e8fa0)
        {
            break MISSING_BLOCK_LABEL_166;
        }
        if (g.zztW)
        {
            n.a(com.google.android.gms.a.d.a(h), f, s, jk1);
            return;
        }
        try
        {
            n.a(com.google.android.gms.a.d.a(h), g, f, s, jk1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (jk jk1)
        {
            zzb.zzd("Could not request ad from mediation adapter.", jk1);
        }
        a(5);
        return;
        if (k)
        {
            n.a(com.google.android.gms.a.d.a(h), f, s, e.a, jk1, l, m);
            return;
        }
        if (g.zztW)
        {
            n.a(com.google.android.gms.a.d.a(h), f, s, e.a, jk1);
            return;
        }
        n.a(com.google.android.gms.a.d.a(h), g, f, s, e.a, jk1);
        return;
    }

    static void a(jl jl1, jk jk1)
    {
        jl1.a(jk1);
    }

    static boolean a(jl jl1, int i1)
    {
        return jl1.b(i1);
    }

    static int b(jl jl1)
    {
        return jl1.o;
    }

    private String b()
    {
        if (!TextUtils.isEmpty(e.e))
        {
            if (b.b(e.e))
            {
                return "com.google.android.gms.ads.mediation.customevent.CustomEventAdapter";
            } else
            {
                return "com.google.ads.mediation.customevent.CustomEventAdapter";
            }
        }
        break MISSING_BLOCK_LABEL_44;
        RemoteException remoteexception;
        remoteexception;
        zzb.zzaH("Fail to determine the custom event's version, assuming the old one.");
        return "com.google.ads.mediation.customevent.CustomEventAdapter";
    }

    private void b(long l1, long l2, long l3, long l4)
    {
        long l5 = SystemClock.elapsedRealtime();
        l1 = l2 - (l5 - l1);
        l2 = l4 - (l5 - l3);
        if (l1 <= 0L || l2 <= 0L)
        {
            zzb.zzaG("Timed out waiting for adapter.");
            o = 3;
            return;
        }
        try
        {
            i.wait(Math.min(l1, l2));
            return;
        }
        catch (InterruptedException interruptedexception)
        {
            o = -1;
        }
    }

    private boolean b(int i1)
    {
        boolean flag = false;
        if (!k) goto _L2; else goto _L1
_L1:
        Bundle bundle = n.l();
_L3:
        RemoteException remoteexception;
        if (bundle != null)
        {
            if ((bundle.getInt("capabilities", 0) & i1) == i1)
            {
                flag = true;
            } else
            {
                flag = false;
            }
        }
        return flag;
_L2:
label0:
        {
            if (!g.zztW)
            {
                break label0;
            }
            bundle = n.k();
        }
          goto _L3
        try
        {
            bundle = n.j();
        }
        // Misplaced declaration of an exception variable
        catch (RemoteException remoteexception)
        {
            zzb.zzaH("Could not get adapter info. Returning false");
            return false;
        }
          goto _L3
    }

    static ka c(jl jl1)
    {
        return jl1.d();
    }

    private kg c()
    {
        if (o != 0 || !e())
        {
            return null;
        }
        kg kg1;
        if (!b(4) || p == null || p.a() == 0)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        kg1 = p;
        return kg1;
        RemoteException remoteexception;
        remoteexception;
        zzb.zzaH("Could not get cpm value from MediationResponseMetadata");
        return c(f());
    }

    private static kg c(int i1)
    {
        return new jn(i1);
    }

    private ka d()
    {
        zzb.zzaG((new StringBuilder()).append("Instantiating mediation adapter: ").append(a).toString());
        if (((Boolean)db.av.c()).booleanValue())
        {
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(a))
            {
                return new kr(new AdMobAdapter());
            }
            if ("com.google.ads.mediation.AdUrlAdapter".equals(a))
            {
                return new kr(new AdUrlAdapter());
            }
        }
        ka ka1;
        try
        {
            ka1 = b.a(a);
        }
        catch (RemoteException remoteexception)
        {
            zzb.zza((new StringBuilder()).append("Could not instantiate mediation adapter: ").append(a).toString(), remoteexception);
            return null;
        }
        return ka1;
    }

    static ka d(jl jl1)
    {
        return jl1.n;
    }

    private boolean e()
    {
        return d.j != -1;
    }

    static boolean e(jl jl1)
    {
        return jl1.e();
    }

    private int f()
    {
        int j1;
        if (e.h == null)
        {
            j1 = 0;
        } else
        {
            Object obj;
            try
            {
                obj = new JSONObject(e.h);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                zzb.zzaH("Could not convert to json. Returning 0");
                return 0;
            }
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(a))
            {
                return ((JSONObject) (obj)).optInt("cpm_cents", 0);
            }
            int i1;
            if (b(2))
            {
                i1 = ((JSONObject) (obj)).optInt("cpm_floor_cents", 0);
            } else
            {
                i1 = 0;
            }
            j1 = i1;
            if (i1 == 0)
            {
                return ((JSONObject) (obj)).optInt("penalized_average_cpm_cents", 0);
            }
        }
        return j1;
    }

    static String f(jl jl1)
    {
        return jl1.a;
    }

    public jo a(long l1, long l2)
    {
        Object obj1;
        synchronized (i)
        {
            long l3 = SystemClock.elapsedRealtime();
            obj1 = new jk();
            rq.a.post(new jm(this, ((jk) (obj1))));
            a(l3, c, l1, l2);
            obj1 = new jo(e, n, a, ((jk) (obj1)), o, c());
        }
        return ((jo) (obj1));
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a()
    {
        Object obj = i;
        obj;
        JVM INSTR monitorenter ;
        if (n != null)
        {
            n.c();
        }
_L1:
        o = -1;
        i.notify();
        return;
        Object obj1;
        obj1;
        zzb.zzd("Could not destroy mediation adapter.", ((Throwable) (obj1)));
          goto _L1
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
    }

    public void a(int i1)
    {
        synchronized (i)
        {
            o = i1;
            i.notify();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(int i1, kg kg1)
    {
        synchronized (i)
        {
            o = i1;
            p = kg1;
            i.notify();
        }
        return;
        kg1;
        obj;
        JVM INSTR monitorexit ;
        throw kg1;
    }
}
