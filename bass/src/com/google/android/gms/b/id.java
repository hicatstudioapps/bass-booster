// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.common.internal.av;
import java.util.LinkedList;

// Referenced classes of package com.google.android.gms.b:
//            ie, hh

class id
{

    private final LinkedList a = new LinkedList();
    private AdRequestParcel b;
    private final String c;

    id(AdRequestParcel adrequestparcel, String s)
    {
        av.a(adrequestparcel);
        av.a(s);
        b = adrequestparcel;
        c = s;
    }

    static AdRequestParcel a(id id1, AdRequestParcel adrequestparcel)
    {
        id1.b = adrequestparcel;
        return adrequestparcel;
    }

    static String a(id id1)
    {
        return id1.c;
    }

    static LinkedList b(id id1)
    {
        return id1.a;
    }

    static AdRequestParcel c(id id1)
    {
        return id1.b;
    }

    AdRequestParcel a()
    {
        return b;
    }

    void a(hh hh)
    {
        hh = new ie(this, hh);
        a.add(hh);
        hh.a(b);
    }

    String b()
    {
        return c;
    }

    ie c()
    {
        return (ie)a.remove();
    }

    int d()
    {
        return a.size();
    }
}
