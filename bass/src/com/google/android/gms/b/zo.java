// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            zc, za, yz, ze, 
//            zi

public final class zo extends zc
{

    public int c;
    public String d;
    public String e;

    public zo()
    {
        c();
    }

    protected int a()
    {
        int j = super.a();
        int i = j;
        if (c != 0)
        {
            i = j + za.b(1, c);
        }
        j = i;
        if (!d.equals(""))
        {
            j = i + za.b(2, d);
        }
        i = j;
        if (!e.equals(""))
        {
            i = j + za.b(3, e);
        }
        return i;
    }

    public zo a(yz yz1)
    {
_L6:
        int i = yz1.a();
        i;
        JVM INSTR lookupswitch 4: default 48
    //                   0: 57
    //                   8: 59
    //                   18: 199
    //                   26: 210;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        if (a(yz1, i)) goto _L6; else goto _L2
_L2:
        return this;
_L3:
        int j = yz1.d();
        switch (j)
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 11: // '\013'
        case 12: // '\f'
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 17: // '\021'
        case 18: // '\022'
        case 19: // '\023'
        case 20: // '\024'
        case 21: // '\025'
        case 22: // '\026'
        case 23: // '\027'
        case 24: // '\030'
        case 25: // '\031'
        case 26: // '\032'
            c = j;
            break;
        }
        continue; /* Loop/switch isn't completed */
_L4:
        d = yz1.f();
        continue; /* Loop/switch isn't completed */
_L5:
        e = yz1.f();
        if (true) goto _L6; else goto _L7
_L7:
    }

    public zo c()
    {
        c = 0;
        d = "";
        e = "";
        a = null;
        b = -1;
        return this;
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        if (obj != this) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(obj instanceof zo)) goto _L4; else goto _L3
_L3:
        obj = (zo)obj;
        flag = flag1;
        if (c != ((zo) (obj)).c) goto _L4; else goto _L5
_L5:
        if (d != null) goto _L7; else goto _L6
_L6:
        flag = flag1;
        if (((zo) (obj)).d != null) goto _L4; else goto _L8
_L8:
        if (e != null) goto _L10; else goto _L9
_L9:
        flag = flag1;
        if (((zo) (obj)).e != null) goto _L4; else goto _L11
_L11:
        if (a != null && !a.b())
        {
            break MISSING_BLOCK_LABEL_140;
        }
        if (((zo) (obj)).a == null)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (!((zo) (obj)).a.b()) goto _L4; else goto _L12
_L12:
        return true;
_L7:
        if (!d.equals(((zo) (obj)).d))
        {
            return false;
        }
          goto _L8
_L10:
        if (!e.equals(((zo) (obj)).e))
        {
            return false;
        }
          goto _L11
        return a.equals(((zo) (obj)).a);
          goto _L8
    }

    public int hashCode()
    {
        boolean flag = false;
        int l = getClass().getName().hashCode();
        int i1 = c;
        int i;
        int j;
        int k;
        if (d == null)
        {
            i = 0;
        } else
        {
            i = d.hashCode();
        }
        if (e == null)
        {
            j = 0;
        } else
        {
            j = e.hashCode();
        }
        k = ((flag) ? 1 : 0);
        if (a != null)
        {
            if (a.b())
            {
                k = ((flag) ? 1 : 0);
            } else
            {
                k = a.hashCode();
            }
        }
        return (j + (i + ((l + 527) * 31 + i1) * 31) * 31) * 31 + k;
    }

    public zi mergeFrom(yz yz1)
    {
        return a(yz1);
    }

    public void writeTo(za za1)
    {
        if (c != 0)
        {
            za1.a(1, c);
        }
        if (!d.equals(""))
        {
            za1.a(2, d);
        }
        if (!e.equals(""))
        {
            za1.a(3, e);
        }
        super.writeTo(za1);
    }
}
