// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.FrameLayout;
import com.google.android.gms.a.d;
import com.google.android.gms.a.e;
import com.google.android.gms.a.f;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.formats.zzj;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            eh, ef, ei, ee

public class fj extends e
{

    public fj()
    {
        super("com.google.android.gms.ads.NativeAdViewDelegateCreatorImpl");
    }

    private ee b(Context context, FrameLayout framelayout, FrameLayout framelayout1)
    {
        com.google.android.gms.a.a a1 = d.a(context);
        framelayout = d.a(framelayout);
        framelayout1 = d.a(framelayout1);
        context = ef.zzu(((eh)a(context)).a(a1, framelayout, framelayout1, 0x7e9e10));
        return context;
        context;
_L2:
        zzb.zzd("Could not create remote NativeAdViewDelegate.", context);
        return null;
        context;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public ee a(Context context, FrameLayout framelayout, FrameLayout framelayout1)
    {
label0:
        {
            if (zzl.zzcN().zzT(context))
            {
                ee ee = b(context, framelayout, framelayout1);
                context = ee;
                if (ee != null)
                {
                    break label0;
                }
            }
            zzb.zzaF("Using NativeAdViewDelegate from the client jar.");
            context = new zzj(framelayout, framelayout1);
        }
        return context;
    }

    protected eh a(IBinder ibinder)
    {
        return ei.a(ibinder);
    }

    protected Object b(IBinder ibinder)
    {
        return a(ibinder);
    }
}
