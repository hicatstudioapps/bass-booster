// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.y;
import com.google.android.gms.common.api.z;

// Referenced classes of package com.google.android.gms.b:
//            vr

public class vs extends Handler
{

    public vs()
    {
        this(Looper.getMainLooper());
    }

    public vs(Looper looper)
    {
        super(looper);
    }

    public void a()
    {
        removeMessages(2);
    }

    public void a(z z1, y y1)
    {
        sendMessage(obtainMessage(1, new Pair(z1, y1)));
    }

    protected void b(z z1, y y1)
    {
        try
        {
            z1.a(y1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (z z1)
        {
            vr.b(y1);
        }
        throw z1;
    }

    public void handleMessage(Message message)
    {
        switch (message.what)
        {
        default:
            Log.wtf("BasePendingResult", (new StringBuilder()).append("Don't know how to handle message: ").append(message.what).toString(), new Exception());
            return;

        case 1: // '\001'
            message = (Pair)message.obj;
            b((z)((Pair) (message)).first, (y)((Pair) (message)).second);
            return;

        case 2: // '\002'
            ((vr)message.obj).d(Status.d);
            break;
        }
    }
}
