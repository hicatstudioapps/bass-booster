// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.util.Log;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.api.y;

// Referenced classes of package com.google.android.gms.b:
//            ve, vg, uz, vi

final class vf extends ve
{

    final uz a;
    private final LogEventParcelable c;

    vf(uz uz1, LogEventParcelable logeventparcelable, n n)
    {
        a = uz1;
        super(n);
        c = logeventparcelable;
    }

    protected Status a(Status status)
    {
        return status;
    }

    protected void a(vi vi1)
    {
        vg vg1 = new vg(this);
        try
        {
            uz.a(c);
        }
        // Misplaced declaration of an exception variable
        catch (vi vi1)
        {
            Log.e("ClearcutLoggerApiImpl", (new StringBuilder()).append("MessageNanoProducer ").append(c.f.toString()).append(" threw: ").append(vi1.toString()).toString());
            return;
        }
        vi1.a(vg1, c);
    }

    protected volatile void a(h h)
    {
        a((vi)h);
    }

    protected y b(Status status)
    {
        return a(status);
    }

    public boolean equals(Object obj)
    {
        if (!(obj instanceof vf))
        {
            return false;
        } else
        {
            obj = (vf)obj;
            return c.equals(((vf) (obj)).c);
        }
    }

    public String toString()
    {
        return (new StringBuilder()).append("MethodImpl(").append(c).append(")").toString();
    }
}
