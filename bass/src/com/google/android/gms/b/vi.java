// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.clearcut.LogEventParcelable;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.m;

// Referenced classes of package com.google.android.gms.b:
//            vn, vm, vj

public class vi extends m
{

    public vi(Context context, Looper looper, h h, q q, r r)
    {
        super(context, looper, 40, h, q, r);
    }

    protected vm a(IBinder ibinder)
    {
        return vn.a(ibinder);
    }

    protected String a()
    {
        return "com.google.android.gms.clearcut.service.START";
    }

    public void a(vj vj, LogEventParcelable logeventparcelable)
    {
        ((vm)zzqs()).a(vj, logeventparcelable);
    }

    protected IInterface b(IBinder ibinder)
    {
        return a(ibinder);
    }

    protected String b()
    {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }
}
