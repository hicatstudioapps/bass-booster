// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import com.google.android.gms.ads.internal.zzp;
import java.util.LinkedHashMap;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            db, cs, rq, pm, 
//            pk

public class df
{

    private boolean a;
    private String b;
    private Map c;
    private Context d;
    private String e;

    public df(Context context, String s)
    {
        d = null;
        e = null;
        d = context;
        e = s;
        a = ((Boolean)db.G.c()).booleanValue();
        b = (String)db.H.c();
        c = new LinkedHashMap();
        c.put("s", "gmob_sdk");
        c.put("v", "3");
        c.put("os", android.os.Build.VERSION.RELEASE);
        c.put("sdk", android.os.Build.VERSION.SDK);
        c.put("device", zzp.zzbx().d());
        s = c;
        if (context.getApplicationContext() != null)
        {
            context = context.getApplicationContext().getPackageName();
        } else
        {
            context = context.getPackageName();
        }
        s.put("app", context);
        context = zzp.zzbD().a(d);
        c.put("network_coarse", Integer.toString(((pk) (context)).m));
        c.put("network_fine", Integer.toString(((pk) (context)).n));
    }

    boolean a()
    {
        return a;
    }

    String b()
    {
        return b;
    }

    Context c()
    {
        return d;
    }

    String d()
    {
        return e;
    }

    Map e()
    {
        return c;
    }
}
