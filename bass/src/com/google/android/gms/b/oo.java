// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.support.v4.b.l;
import com.google.android.gms.ads.internal.formats.zza;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.concurrent.Future;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            ok, oe

public class oo
    implements ok
{

    private final boolean a;

    public oo(boolean flag)
    {
        a = flag;
    }

    private l a(l l1)
    {
        l l2 = new l();
        for (int i = 0; i < l1.size(); i++)
        {
            l2.put(l1.b(i), ((Future)l1.c(i)).get());
        }

        return l2;
    }

    private void a(oe oe1, JSONObject jsonobject, l l1)
    {
        l1.put(jsonobject.getString("name"), oe1.a(jsonobject, "image_value", a));
    }

    private void a(JSONObject jsonobject, l l1)
    {
        l1.put(jsonobject.getString("name"), jsonobject.getString("string_value"));
    }

    public com.google.android.gms.ads.internal.formats.zzh.zza a(oe oe1, JSONObject jsonobject)
    {
        return b(oe1, jsonobject);
    }

    public zzf b(oe oe1, JSONObject jsonobject)
    {
        l l1 = new l();
        l l2 = new l();
        th th = oe1.b(jsonobject);
        JSONArray jsonarray = jsonobject.getJSONArray("custom_assets");
        int i = 0;
        while (i < jsonarray.length()) 
        {
            JSONObject jsonobject1 = jsonarray.getJSONObject(i);
            String s = jsonobject1.getString("type");
            if ("string".equals(s))
            {
                a(jsonobject1, l2);
            } else
            if ("image".equals(s))
            {
                a(oe1, jsonobject1, l1);
            } else
            {
                zzb.zzaH((new StringBuilder()).append("Unknown custom asset type: ").append(s).toString());
            }
            i++;
        }
        return new zzf(jsonobject.getString("custom_template_id"), a(l1), l2, (zza)th.get());
    }
}
