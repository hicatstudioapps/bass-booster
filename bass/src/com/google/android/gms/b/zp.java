// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.util.Arrays;

// Referenced classes of package com.google.android.gms.b:
//            zc, zl, za, yz, 
//            zg, ze, zi

public final class zp extends zc
{

    public byte c[];
    public byte d[][];
    public boolean e;

    public zp()
    {
        c();
    }

    protected int a()
    {
        boolean flag = false;
        int j = super.a();
        int i = j;
        if (!Arrays.equals(c, zl.h))
        {
            i = j + za.b(1, c);
        }
        j = i;
        if (d != null)
        {
            j = i;
            if (d.length > 0)
            {
                int k = 0;
                int l = 0;
                for (j = ((flag) ? 1 : 0); j < d.length;)
                {
                    byte abyte0[] = d[j];
                    int j1 = k;
                    int i1 = l;
                    if (abyte0 != null)
                    {
                        i1 = l + 1;
                        j1 = k + za.c(abyte0);
                    }
                    j++;
                    k = j1;
                    l = i1;
                }

                j = i + k + l * 1;
            }
        }
        i = j;
        if (e)
        {
            i = j + za.b(3, e);
        }
        return i;
    }

    public zp a(yz yz1)
    {
        do
        {
            int i = yz1.a();
            switch (i)
            {
            default:
                if (a(yz1, i))
                {
                    continue;
                }
                // fall through

            case 0: // '\0'
                return this;

            case 10: // '\n'
                c = yz1.g();
                break;

            case 18: // '\022'
                int k = zl.b(yz1, 18);
                byte abyte0[][];
                int j;
                if (d == null)
                {
                    j = 0;
                } else
                {
                    j = d.length;
                }
                abyte0 = new byte[k + j][];
                k = j;
                if (j != 0)
                {
                    System.arraycopy(d, 0, abyte0, 0, j);
                    k = j;
                }
                for (; k < abyte0.length - 1; k++)
                {
                    abyte0[k] = yz1.g();
                    yz1.a();
                }

                abyte0[k] = yz1.g();
                d = abyte0;
                break;

            case 24: // '\030'
                e = yz1.e();
                break;
            }
        } while (true);
    }

    public zp c()
    {
        c = zl.h;
        d = zl.g;
        e = false;
        a = null;
        b = -1;
        return this;
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        if (obj != this) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(obj instanceof zp)) goto _L4; else goto _L3
_L3:
        obj = (zp)obj;
        flag = flag1;
        if (!Arrays.equals(c, ((zp) (obj)).c)) goto _L4; else goto _L5
_L5:
        flag = flag1;
        if (!zg.a(d, ((zp) (obj)).d)) goto _L4; else goto _L6
_L6:
        flag = flag1;
        if (e != ((zp) (obj)).e) goto _L4; else goto _L7
_L7:
        if (a != null && !a.b())
        {
            break MISSING_BLOCK_LABEL_108;
        }
        if (((zp) (obj)).a == null)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (!((zp) (obj)).a.b()) goto _L4; else goto _L8
_L8:
        return true;
        return a.equals(((zp) (obj)).a);
    }

    public int hashCode()
    {
        int j = getClass().getName().hashCode();
        int k = Arrays.hashCode(c);
        int l = zg.a(d);
        char c1;
        int i;
        if (e)
        {
            c1 = '\u04CF';
        } else
        {
            c1 = '\u04D5';
        }
        if (a == null || a.b())
        {
            i = 0;
        } else
        {
            i = a.hashCode();
        }
        return i + (c1 + (((j + 527) * 31 + k) * 31 + l) * 31) * 31;
    }

    public zi mergeFrom(yz yz1)
    {
        return a(yz1);
    }

    public void writeTo(za za1)
    {
        if (!Arrays.equals(c, zl.h))
        {
            za1.a(1, c);
        }
        if (d != null && d.length > 0)
        {
            for (int i = 0; i < d.length; i++)
            {
                byte abyte0[] = d[i];
                if (abyte0 != null)
                {
                    za1.a(2, abyte0);
                }
            }

        }
        if (e)
        {
            za1.a(3, e);
        }
        super.writeTo(za1);
    }
}
