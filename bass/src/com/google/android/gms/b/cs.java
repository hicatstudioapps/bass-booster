// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.SharedPreferences;
import com.google.android.gms.ads.internal.zzp;

// Referenced classes of package com.google.android.gms.b:
//            cx, cu, cv, ct, 
//            cw, cz

public abstract class cs
{

    private final int a;
    private final String b;
    private final Object c;

    private cs(int i, String s, Object obj)
    {
        a = i;
        b = s;
        c = obj;
        zzp.zzbF().a(this);
    }

    cs(int i, String s, Object obj, ct ct1)
    {
        this(i, s, obj);
    }

    public static cs a(int i, String s)
    {
        s = a(i, s, (String)null);
        zzp.zzbF().b(s);
        return s;
    }

    public static cs a(int i, String s, int j)
    {
        return new cu(i, s, Integer.valueOf(j));
    }

    public static cs a(int i, String s, long l)
    {
        return new cv(i, s, Long.valueOf(l));
    }

    public static cs a(int i, String s, Boolean boolean1)
    {
        return new ct(i, s, boolean1);
    }

    public static cs a(int i, String s, String s1)
    {
        return new cw(i, s, s1);
    }

    public static cs b(int i, String s)
    {
        s = a(i, s, (String)null);
        zzp.zzbF().c(s);
        return s;
    }

    protected abstract Object a(SharedPreferences sharedpreferences);

    public String a()
    {
        return b;
    }

    public Object b()
    {
        return c;
    }

    public Object c()
    {
        return zzp.zzbG().a(this);
    }
}
