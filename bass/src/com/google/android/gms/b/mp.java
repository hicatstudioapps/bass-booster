// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            mo, mn

class mp extends WebViewClient
{

    final WebView a;
    final mo b;

    mp(mo mo1, WebView webview)
    {
        b = mo1;
        a = webview;
        super();
    }

    public void onPageFinished(WebView webview, String s)
    {
        zzb.zzaF("Loading assets have finished");
        b.c.a.remove(a);
    }

    public void onReceivedError(WebView webview, int i, String s, String s1)
    {
        zzb.zzaH("Loading assets have failed.");
        b.c.a.remove(a);
    }
}
