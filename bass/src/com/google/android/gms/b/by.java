// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            bx

public class by
{

    private final Object a = new Object();
    private int b;
    private List c;

    public by()
    {
        c = new LinkedList();
    }

    public bx a()
    {
        bx bx1;
label0:
        {
            bx1 = null;
            synchronized (a)
            {
                if (c.size() != 0)
                {
                    break label0;
                }
                zzb.zzaF("Queue empty");
            }
            return null;
        }
        if (c.size() < 2)
        {
            break MISSING_BLOCK_LABEL_121;
        }
        int i = 0x80000000;
        Iterator iterator = c.iterator();
_L1:
        bx bx2;
        int j;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_101;
        }
        bx2 = (bx)iterator.next();
        j = bx2.g();
        if (j > i)
        {
            bx1 = bx2;
            i = j;
        }
          goto _L1
        c.remove(bx1);
        obj;
        JVM INSTR monitorexit ;
        return bx1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        exception = (bx)c.get(0);
        exception.c();
        obj;
        JVM INSTR monitorexit ;
        return exception;
    }

    public boolean a(bx bx1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        if (c.contains(bx1))
        {
            return true;
        }
        obj;
        JVM INSTR monitorexit ;
        return false;
        bx1;
        obj;
        JVM INSTR monitorexit ;
        throw bx1;
    }

    public boolean b(bx bx1)
    {
        Object obj = a;
        obj;
        JVM INSTR monitorenter ;
        Iterator iterator = c.iterator();
_L2:
        bx bx2;
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_68;
            }
            bx2 = (bx)iterator.next();
        } while (bx1 == bx2);
        if (!bx2.b().equals(bx1.b())) goto _L2; else goto _L1
_L1:
        iterator.remove();
        obj;
        JVM INSTR monitorexit ;
        return true;
        obj;
        JVM INSTR monitorexit ;
        return false;
        bx1;
        obj;
        JVM INSTR monitorexit ;
        throw bx1;
    }

    public void c(bx bx1)
    {
        synchronized (a)
        {
            if (c.size() >= 10)
            {
                zzb.zzaF((new StringBuilder()).append("Queue is full, current size = ").append(c.size()).toString());
                c.remove(0);
            }
            int i = b;
            b = i + 1;
            bx1.a(i);
            c.add(bx1);
        }
        return;
        bx1;
        obj;
        JVM INSTR monitorexit ;
        throw bx1;
    }
}
