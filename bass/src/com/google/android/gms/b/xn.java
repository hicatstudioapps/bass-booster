// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.aa;
import com.google.android.gms.common.api.ac;
import com.google.android.gms.common.api.af;
import com.google.android.gms.common.api.v;
import com.google.android.gms.common.api.x;
import com.google.android.gms.common.api.y;
import com.google.android.gms.common.api.z;
import com.google.android.gms.common.internal.av;

public class xn extends af
    implements z
{

    private ac a;
    private xn b;
    private aa c;
    private v d;
    private final Object e;

    private void a()
    {
        if (d == null || a == null && c == null)
        {
            return;
        } else
        {
            d.a(this);
            return;
        }
    }

    private void b(y y1)
    {
        if (!(y1 instanceof x))
        {
            break MISSING_BLOCK_LABEL_16;
        }
        ((x)y1).a();
        return;
        RuntimeException runtimeexception;
        runtimeexception;
        Log.w("TransformedResultImpl", (new StringBuilder()).append("Unable to release ").append(y1).toString(), runtimeexception);
        return;
    }

    public void a(Status status)
    {
        Object obj = e;
        obj;
        JVM INSTR monitorenter ;
        if (a == null) goto _L2; else goto _L1
_L1:
        status = a.a(status);
        av.a(status, "onFailure must not return null");
        b.a(status);
_L4:
        return;
_L2:
        if (c != null)
        {
            c.a(status);
        }
        if (true) goto _L4; else goto _L3
_L3:
        status;
        obj;
        JVM INSTR monitorexit ;
        throw status;
    }

    public void a(v v1)
    {
        synchronized (e)
        {
            d = v1;
            a();
        }
        return;
        v1;
        obj;
        JVM INSTR monitorexit ;
        throw v1;
    }

    public void a(y y1)
    {
        Object obj = e;
        obj;
        JVM INSTR monitorenter ;
        if (!y1.a().e())
        {
            break MISSING_BLOCK_LABEL_96;
        }
        if (a == null) goto _L2; else goto _L1
_L1:
        v v1 = a.a(y1);
        if (v1 != null) goto _L4; else goto _L3
_L3:
        a(new Status(13, "Transform returned null"));
_L5:
        b(y1);
_L7:
        obj;
        JVM INSTR monitorexit ;
        return;
_L4:
        b.a(v1);
          goto _L5
        y1;
        obj;
        JVM INSTR monitorexit ;
        throw y1;
_L2:
        if (c == null) goto _L7; else goto _L6
_L6:
        c.b(y1);
          goto _L7
        a(y1.a());
        b(y1);
          goto _L7
    }
}
