// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            zc, za, yz, zl, 
//            zg, ze, zi

public final class zn extends zc
{

    public String c[];
    public String d[];
    public int e[];
    public long f[];

    public zn()
    {
        c();
    }

    protected int a()
    {
        boolean flag = false;
        int l2 = super.a();
        int i;
        int k;
        if (c != null && c.length > 0)
        {
            i = 0;
            int j = 0;
            int l;
            int l1;
            for (l = 0; i < c.length; l = l1)
            {
                String s = c[i];
                int j2 = j;
                l1 = l;
                if (s != null)
                {
                    l1 = l + 1;
                    j2 = j + za.b(s);
                }
                i++;
                j = j2;
            }

            i = l2 + j + l * 1;
        } else
        {
            i = l2;
        }
        k = i;
        if (d != null)
        {
            k = i;
            if (d.length > 0)
            {
                k = 0;
                int i1 = 0;
                int i2;
                int k2;
                for (i2 = 0; k < d.length; i2 = k2)
                {
                    String s1 = d[k];
                    l2 = i1;
                    k2 = i2;
                    if (s1 != null)
                    {
                        k2 = i2 + 1;
                        l2 = i1 + za.b(s1);
                    }
                    k++;
                    i1 = l2;
                }

                k = i + i1 + i2 * 1;
            }
        }
        i = k;
        if (e != null)
        {
            i = k;
            if (e.length > 0)
            {
                i = 0;
                int j1 = 0;
                for (; i < e.length; i++)
                {
                    j1 += za.b(e[i]);
                }

                i = k + j1 + e.length * 1;
            }
        }
        k = i;
        if (f != null)
        {
            k = i;
            if (f.length > 0)
            {
                int k1 = 0;
                for (k = ((flag) ? 1 : 0); k < f.length; k++)
                {
                    k1 += za.c(f[k]);
                }

                k = i + k1 + f.length * 1;
            }
        }
        return k;
    }

    public zn a(yz yz1)
    {
        do
        {
            int i = yz1.a();
            switch (i)
            {
            default:
                if (a(yz1, i))
                {
                    continue;
                }
                // fall through

            case 0: // '\0'
                return this;

            case 10: // '\n'
                int l1 = zl.b(yz1, 10);
                String as[];
                int j;
                if (c == null)
                {
                    j = 0;
                } else
                {
                    j = c.length;
                }
                as = new String[l1 + j];
                l1 = j;
                if (j != 0)
                {
                    System.arraycopy(c, 0, as, 0, j);
                    l1 = j;
                }
                for (; l1 < as.length - 1; l1++)
                {
                    as[l1] = yz1.f();
                    yz1.a();
                }

                as[l1] = yz1.f();
                c = as;
                break;

            case 18: // '\022'
                int i2 = zl.b(yz1, 18);
                String as1[];
                int k;
                if (d == null)
                {
                    k = 0;
                } else
                {
                    k = d.length;
                }
                as1 = new String[i2 + k];
                i2 = k;
                if (k != 0)
                {
                    System.arraycopy(d, 0, as1, 0, k);
                    i2 = k;
                }
                for (; i2 < as1.length - 1; i2++)
                {
                    as1[i2] = yz1.f();
                    yz1.a();
                }

                as1[i2] = yz1.f();
                d = as1;
                break;

            case 24: // '\030'
                int j2 = zl.b(yz1, 24);
                int ai[];
                int l;
                if (e == null)
                {
                    l = 0;
                } else
                {
                    l = e.length;
                }
                ai = new int[j2 + l];
                j2 = l;
                if (l != 0)
                {
                    System.arraycopy(e, 0, ai, 0, l);
                    j2 = l;
                }
                for (; j2 < ai.length - 1; j2++)
                {
                    ai[j2] = yz1.d();
                    yz1.a();
                }

                ai[j2] = yz1.d();
                e = ai;
                break;

            case 26: // '\032'
                int j3 = yz1.c(yz1.i());
                int i1 = yz1.o();
                int k2;
                for (k2 = 0; yz1.m() > 0; k2++)
                {
                    yz1.d();
                }

                yz1.e(i1);
                int ai1[];
                if (e == null)
                {
                    i1 = 0;
                } else
                {
                    i1 = e.length;
                }
                ai1 = new int[k2 + i1];
                k2 = i1;
                if (i1 != 0)
                {
                    System.arraycopy(e, 0, ai1, 0, i1);
                    k2 = i1;
                }
                for (; k2 < ai1.length; k2++)
                {
                    ai1[k2] = yz1.d();
                }

                e = ai1;
                yz1.d(j3);
                break;

            case 32: // ' '
                int l2 = zl.b(yz1, 32);
                long al[];
                int j1;
                if (f == null)
                {
                    j1 = 0;
                } else
                {
                    j1 = f.length;
                }
                al = new long[l2 + j1];
                l2 = j1;
                if (j1 != 0)
                {
                    System.arraycopy(f, 0, al, 0, j1);
                    l2 = j1;
                }
                for (; l2 < al.length - 1; l2++)
                {
                    al[l2] = yz1.c();
                    yz1.a();
                }

                al[l2] = yz1.c();
                f = al;
                break;

            case 34: // '"'
                int k3 = yz1.c(yz1.i());
                int k1 = yz1.o();
                int i3;
                for (i3 = 0; yz1.m() > 0; i3++)
                {
                    yz1.c();
                }

                yz1.e(k1);
                long al1[];
                if (f == null)
                {
                    k1 = 0;
                } else
                {
                    k1 = f.length;
                }
                al1 = new long[i3 + k1];
                i3 = k1;
                if (k1 != 0)
                {
                    System.arraycopy(f, 0, al1, 0, k1);
                    i3 = k1;
                }
                for (; i3 < al1.length; i3++)
                {
                    al1[i3] = yz1.c();
                }

                f = al1;
                yz1.d(k3);
                break;
            }
        } while (true);
    }

    public zn c()
    {
        c = zl.f;
        d = zl.f;
        e = zl.a;
        f = zl.b;
        a = null;
        b = -1;
        return this;
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        if (obj != this) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(obj instanceof zn)) goto _L4; else goto _L3
_L3:
        obj = (zn)obj;
        flag = flag1;
        if (!zg.a(c, ((zn) (obj)).c)) goto _L4; else goto _L5
_L5:
        flag = flag1;
        if (!zg.a(d, ((zn) (obj)).d)) goto _L4; else goto _L6
_L6:
        flag = flag1;
        if (!zg.a(e, ((zn) (obj)).e)) goto _L4; else goto _L7
_L7:
        flag = flag1;
        if (!zg.a(f, ((zn) (obj)).f)) goto _L4; else goto _L8
_L8:
        if (a != null && !a.b())
        {
            break MISSING_BLOCK_LABEL_127;
        }
        if (((zn) (obj)).a == null)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (!((zn) (obj)).a.b()) goto _L4; else goto _L9
_L9:
        return true;
        return a.equals(((zn) (obj)).a);
    }

    public int hashCode()
    {
        int j = getClass().getName().hashCode();
        int k = zg.a(c);
        int l = zg.a(d);
        int i1 = zg.a(e);
        int j1 = zg.a(f);
        int i;
        if (a == null || a.b())
        {
            i = 0;
        } else
        {
            i = a.hashCode();
        }
        return i + (((((j + 527) * 31 + k) * 31 + l) * 31 + i1) * 31 + j1) * 31;
    }

    public zi mergeFrom(yz yz1)
    {
        return a(yz1);
    }

    public void writeTo(za za1)
    {
        boolean flag = false;
        if (c != null && c.length > 0)
        {
            for (int i = 0; i < c.length; i++)
            {
                String s = c[i];
                if (s != null)
                {
                    za1.a(1, s);
                }
            }

        }
        if (d != null && d.length > 0)
        {
            for (int j = 0; j < d.length; j++)
            {
                String s1 = d[j];
                if (s1 != null)
                {
                    za1.a(2, s1);
                }
            }

        }
        if (e != null && e.length > 0)
        {
            for (int k = 0; k < e.length; k++)
            {
                za1.a(3, e[k]);
            }

        }
        if (f != null && f.length > 0)
        {
            for (int l = ((flag) ? 1 : 0); l < f.length; l++)
            {
                za1.a(4, f[l]);
            }

        }
        super.writeTo(za1);
    }
}
