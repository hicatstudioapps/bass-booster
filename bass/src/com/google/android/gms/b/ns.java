// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            no, tu, nq, tv, 
//            qq, ny

public class ns extends no
{

    private nq g;

    ns(Context context, qq qq, tu tu1, ny ny)
    {
        super(context, qq, tu1, ny);
    }

    protected void b()
    {
        Object obj = c.j();
        int i;
        int j;
        if (((AdSizeParcel) (obj)).zztW)
        {
            obj = b.getResources().getDisplayMetrics();
            j = ((DisplayMetrics) (obj)).widthPixels;
            i = ((DisplayMetrics) (obj)).heightPixels;
        } else
        {
            j = ((AdSizeParcel) (obj)).widthPixels;
            i = ((AdSizeParcel) (obj)).heightPixels;
        }
        g = new nq(this, c, j, i);
        c.k().a(this);
        g.a(e);
    }

    protected int c()
    {
        if (g.c())
        {
            zzb.zzaF("Ad-Network indicated no fill with passback URL.");
            return 3;
        }
        return g.d() ? -2 : 2;
    }
}
