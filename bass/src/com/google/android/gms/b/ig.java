// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Parcel;
import android.util.Base64;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.zzg;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

class ig
{

    final AdRequestParcel a;
    final String b;

    ig(AdRequestParcel adrequestparcel, String s)
    {
        a = adrequestparcel;
        b = s;
    }

    ig(String s)
    {
        Object aobj[];
        aobj = s.split("\0");
        if (aobj.length != 2)
        {
            throw new IOException("Incorrect field count for QueueSeed.");
        }
        s = Parcel.obtain();
        b = new String(Base64.decode(aobj[0], 0), "UTF-8");
        aobj = Base64.decode(aobj[1], 0);
        s.unmarshall(((byte []) (aobj)), 0, aobj.length);
        s.setDataPosition(0);
        a = AdRequestParcel.CREATOR.zzb(s);
        s.recycle();
        return;
        Object obj;
        obj;
        throw new IOException("Malformed QueueSeed encoding.");
        obj;
        s.recycle();
        throw obj;
    }

    String a()
    {
        Parcel parcel = Parcel.obtain();
        String s;
        s = Base64.encodeToString(b.getBytes("UTF-8"), 0);
        a.writeToParcel(parcel, 0);
        String s1 = Base64.encodeToString(parcel.marshall(), 0);
        s = (new StringBuilder()).append(s).append("\0").append(s1).toString();
        parcel.recycle();
        return s;
        Object obj;
        obj;
        zzb.e("QueueSeed encode failed because UTF-8 is not available.");
        parcel.recycle();
        return "";
        obj;
        parcel.recycle();
        throw obj;
    }
}
