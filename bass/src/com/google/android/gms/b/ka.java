// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import android.os.IInterface;
import com.google.android.gms.a.a;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            kd, kk, kn

public interface ka
    extends IInterface
{

    public abstract a a();

    public abstract void a(a a1, AdRequestParcel adrequestparcel, String s, zza zza, String s1);

    public abstract void a(a a1, AdRequestParcel adrequestparcel, String s, kd kd);

    public abstract void a(a a1, AdRequestParcel adrequestparcel, String s, String s1, kd kd);

    public abstract void a(a a1, AdRequestParcel adrequestparcel, String s, String s1, kd kd, NativeAdOptionsParcel nativeadoptionsparcel, List list);

    public abstract void a(a a1, AdSizeParcel adsizeparcel, AdRequestParcel adrequestparcel, String s, kd kd);

    public abstract void a(a a1, AdSizeParcel adsizeparcel, AdRequestParcel adrequestparcel, String s, String s1, kd kd);

    public abstract void a(AdRequestParcel adrequestparcel, String s);

    public abstract void b();

    public abstract void c();

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract boolean g();

    public abstract kk h();

    public abstract kn i();

    public abstract Bundle j();

    public abstract Bundle k();

    public abstract Bundle l();
}
