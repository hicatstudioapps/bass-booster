// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


public final class uw extends Enum
{

    public static final uw a;
    public static final uw b;
    public static final uw c;
    public static final uw d;
    private static final uw e[];

    private uw(String s, int i)
    {
        super(s, i);
    }

    public static uw valueOf(String s)
    {
        return (uw)Enum.valueOf(com/google/android/gms/b/uw, s);
    }

    public static uw[] values()
    {
        return (uw[])e.clone();
    }

    static 
    {
        a = new uw("LOW", 0);
        b = new uw("NORMAL", 1);
        c = new uw("HIGH", 2);
        d = new uw("IMMEDIATE", 3);
        e = (new uw[] {
            a, b, c, d
        });
    }
}
