// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.a;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.b:
//            xa

public class vt
    implements q, r
{

    public final a a;
    private final int b;
    private xa c;

    public vt(a a1, int i)
    {
        a = a1;
        b = i;
    }

    private void a()
    {
        av.a(c, "Callbacks must be attached to a GoogleApiClient instance before connecting the client.");
    }

    public void a(xa xa1)
    {
        c = xa1;
    }

    public void onConnected(Bundle bundle)
    {
        a();
        c.a(bundle);
    }

    public void onConnectionFailed(ConnectionResult connectionresult)
    {
        a();
        c.a(connectionresult, a, b);
    }

    public void onConnectionSuspended(int i)
    {
        a();
        c.a(i);
    }
}
