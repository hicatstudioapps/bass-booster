// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            qa, ka

class qb
    implements Runnable
{

    final ka a;
    final AdRequestParcel b;
    final qa c;

    qb(qa qa1, ka ka1, AdRequestParcel adrequestparcel)
    {
        c = qa1;
        a = ka1;
        b = adrequestparcel;
        super();
    }

    public void run()
    {
        try
        {
            a.a(b, qa.a(c));
            return;
        }
        catch (RemoteException remoteexception)
        {
            zzb.zzd("Fail to load ad from adapter.", remoteexception);
        }
        c.a(qa.b(c), 0);
    }
}
