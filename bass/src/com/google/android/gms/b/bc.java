// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.view.View;
import java.lang.ref.WeakReference;

// Referenced classes of package com.google.android.gms.b:
//            bw, bb, qp

public class bc
    implements bw
{

    private final WeakReference a;
    private final WeakReference b;

    public bc(View view, qp qp1)
    {
        a = new WeakReference(view);
        b = new WeakReference(qp1);
    }

    public View a()
    {
        return (View)a.get();
    }

    public boolean b()
    {
        return a.get() == null || b.get() == null;
    }

    public bw c()
    {
        return new bb((View)a.get(), (qp)b.get());
    }
}
