// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzn;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// Referenced classes of package com.google.android.gms.b:
//            qy, oe, sl, qq, 
//            qp, rk, rq, od, 
//            ny, bi, ab

public class oc extends qy
{

    private final ny a;
    private final AdResponseParcel b;
    private final qq c;
    private final oe d;
    private final Object e;
    private Future f;

    public oc(Context context, zzn zzn, bi bi, qq qq1, ab ab, ny ny)
    {
        this(qq1, ny, new oe(context, zzn, bi, new sl(context), ab, qq1));
    }

    oc(qq qq1, ny ny, oe oe1)
    {
        e = new Object();
        c = qq1;
        b = qq1.b;
        a = ny;
        d = oe1;
    }

    static ny a(oc oc1)
    {
        return oc1.a;
    }

    private qp a(int i)
    {
        return new qp(c.a.zzGq, null, null, i, null, null, b.orientation, b.zzAU, c.a.zzGt, false, null, null, null, null, null, b.zzGO, c.d, b.zzGM, c.f, b.zzGR, b.zzGS, c.h, null);
    }

    public void onStop()
    {
        synchronized (e)
        {
            if (f != null)
            {
                f.cancel(true);
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zzbp()
    {
        synchronized (e)
        {
            f = rk.a(d);
        }
        obj = (qp)f.get(60000L, TimeUnit.MILLISECONDS);
        byte byte0 = -2;
_L1:
        if (obj == null)
        {
            obj = a(byte0);
        }
        rq.a.post(new od(this, ((qp) (obj))));
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        try
        {
            throw exception;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            zzb.zzaH("Timed out waiting for native ad.");
            f.cancel(true);
            byte0 = 2;
            obj = null;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            byte0 = 0;
            obj = null;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
            byte0 = -1;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
            byte0 = -1;
        }
          goto _L1
    }
}
