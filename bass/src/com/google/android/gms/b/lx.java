// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            ly, gd, tu, lw, 
//            co, rq, tv, lu

public class lx extends ly
    implements gd
{

    DisplayMetrics a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    private final tu h;
    private final Context i;
    private final WindowManager j;
    private final co k;
    private float l;
    private int m;

    public lx(tu tu1, Context context, co co1)
    {
        super(tu1);
        b = -1;
        c = -1;
        d = -1;
        e = -1;
        f = -1;
        g = -1;
        h = tu1;
        i = context;
        k = co1;
        j = (WindowManager)context.getSystemService("window");
    }

    private void g()
    {
        a = new DisplayMetrics();
        Display display = j.getDefaultDisplay();
        display.getMetrics(a);
        l = a.density;
        m = display.getRotation();
    }

    private void h()
    {
        int ai[] = new int[2];
        h.getLocationOnScreen(ai);
        a(zzl.zzcN().zzc(i, ai[0]), zzl.zzcN().zzc(i, ai[1]));
    }

    private lu i()
    {
        return (new lw()).b(k.a()).a(k.b()).c(k.f()).d(k.c()).e(k.d()).a();
    }

    void a()
    {
        b = zzl.zzcN().zzb(a, a.widthPixels);
        c = zzl.zzcN().zzb(a, a.heightPixels);
        Activity activity = h.e();
        if (activity == null || activity.getWindow() == null)
        {
            d = b;
            e = c;
            return;
        } else
        {
            int ai[] = zzp.zzbx().a(activity);
            d = zzl.zzcN().zzb(a, ai[0]);
            e = zzl.zzcN().zzb(a, ai[1]);
            return;
        }
    }

    public void a(int i1, int j1)
    {
        int k1;
        if (i instanceof Activity)
        {
            k1 = zzp.zzbx().d((Activity)i)[0];
        } else
        {
            k1 = 0;
        }
        b(i1, j1 - k1, f, g);
        h.k().a(i1, j1);
    }

    void b()
    {
        if (h.j().zztW)
        {
            f = b;
            g = c;
            return;
        } else
        {
            h.measure(0, 0);
            f = zzl.zzcN().zzc(i, h.getMeasuredWidth());
            g = zzl.zzcN().zzc(i, h.getMeasuredHeight());
            return;
        }
    }

    public void c()
    {
        g();
        a();
        b();
        e();
        f();
        h();
        d();
    }

    void d()
    {
        if (zzb.zzQ(2))
        {
            zzb.zzaG("Dispatching Ready Event.");
        }
        c(h.n().afmaVersion);
    }

    void e()
    {
        a(b, c, d, e, l, m);
    }

    void f()
    {
        lu lu1 = i();
        h.a("onDeviceFeaturesReceived", lu1.a());
    }

    public void zza(tu tu1, Map map)
    {
        c();
    }
}
