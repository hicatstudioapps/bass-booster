// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.webkit.WebView;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.zze;
import com.google.android.gms.ads.internal.zzp;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            bg, ub, tu, rq, 
//            tv, bs, bp, bo, 
//            bn, bv, br, bq, 
//            ab, fp, gl, gn, 
//            lz, bh, gd, bu

public class bm
    implements bg
{

    private final tu a;

    public bm(Context context, VersionInfoParcel versioninfoparcel, ab ab)
    {
        a = zzp.zzby().a(context, new AdSizeParcel(), false, false, ab, versioninfoparcel);
        a.a().setWillNotDraw(true);
    }

    static tu a(bm bm1)
    {
        return bm1.a;
    }

    private void a(Runnable runnable)
    {
        if (zzl.zzcN().zzhr())
        {
            runnable.run();
            return;
        } else
        {
            rq.a.post(runnable);
            return;
        }
    }

    public void a()
    {
        a.destroy();
    }

    public void a(com.google.android.gms.ads.internal.client.zza zza1, zzg zzg, fp fp, zzn zzn, boolean flag, gl gl, gn gn, 
            zze zze1, lz lz)
    {
        a.k().a(zza1, zzg, fp, zzn, flag, gl, gn, new zze(false), lz);
    }

    public void a(bh bh)
    {
        a.k().a(new bs(this, bh));
    }

    public void a(String s)
    {
        a(((Runnable) (new bp(this, String.format("<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>", new Object[] {
            s
        })))));
    }

    public void a(String s, gd gd)
    {
        a.k().a(s, gd);
    }

    public void a(String s, String s1)
    {
        a(((Runnable) (new bo(this, s, s1))));
    }

    public void a(String s, JSONObject jsonobject)
    {
        a(((Runnable) (new bn(this, s, jsonobject))));
    }

    public bu b()
    {
        return new bv(this);
    }

    public void b(String s)
    {
        a(new br(this, s));
    }

    public void b(String s, gd gd)
    {
        a.k().b(s, gd);
    }

    public void b(String s, JSONObject jsonobject)
    {
        a.a(s, jsonobject);
    }

    public void c(String s)
    {
        a(new bq(this, s));
    }
}
