// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.b:
//            jh, jq

public final class ji
{

    public final List a;
    public final long b;
    public final List c;
    public final List d;
    public final List e;
    public final String f;
    public final long g;
    public final String h;
    public final int i;
    public final int j;
    public final long k;
    public int l;
    public int m;

    public ji(String s)
    {
        s = new JSONObject(s);
        if (zzb.zzQ(2))
        {
            zzb.v((new StringBuilder()).append("Mediation Response JSON: ").append(s.toString(2)).toString());
        }
        JSONArray jsonarray = s.getJSONArray("ad_networks");
        ArrayList arraylist = new ArrayList(jsonarray.length());
        int i1 = 0;
        int j1;
        int k1;
        for (j1 = -1; i1 < jsonarray.length(); j1 = k1)
        {
            jh jh1 = new jh(jsonarray.getJSONObject(i1));
            arraylist.add(jh1);
            k1 = j1;
            if (j1 < 0)
            {
                k1 = j1;
                if (a(jh1))
                {
                    k1 = i1;
                }
            }
            i1++;
        }

        l = j1;
        m = jsonarray.length();
        a = Collections.unmodifiableList(arraylist);
        f = s.getString("qdata");
        j = s.optInt("fs_model_type", -1);
        k = s.optLong("timeout_ms", -1L);
        s = s.optJSONObject("settings");
        if (s != null)
        {
            b = s.optLong("ad_network_timeout_millis", -1L);
            c = zzp.zzbK().a(s, "click_urls");
            d = zzp.zzbK().a(s, "imp_urls");
            e = zzp.zzbK().a(s, "nofill_urls");
            long l1 = s.optLong("refresh", -1L);
            if (l1 > 0L)
            {
                l1 *= 1000L;
            } else
            {
                l1 = -1L;
            }
            g = l1;
            s = s.optJSONArray("rewards");
            if (s == null || s.length() == 0)
            {
                h = null;
                i = 0;
                return;
            } else
            {
                h = s.getJSONObject(0).optString("rb_type");
                i = s.getJSONObject(0).optInt("rb_amount");
                return;
            }
        } else
        {
            b = -1L;
            c = null;
            d = null;
            e = null;
            g = -1L;
            h = null;
            i = 0;
            return;
        }
    }

    private boolean a(jh jh1)
    {
        for (jh1 = jh1.c.iterator(); jh1.hasNext();)
        {
            if (((String)jh1.next()).equals("com.google.ads.mediation.admob.AdMobAdapter"))
            {
                return true;
            }
        }

        return false;
    }
}
