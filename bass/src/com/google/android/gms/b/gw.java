// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.zzp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

// Referenced classes of package com.google.android.gms.b:
//            gu, gx, tu

public class gw
    implements Iterable
{

    private final List a = new LinkedList();

    public gw()
    {
    }

    private gu c(tu tu)
    {
        for (Iterator iterator1 = zzp.zzbL().iterator(); iterator1.hasNext();)
        {
            gu gu1 = (gu)iterator1.next();
            if (gu1.a == tu)
            {
                return gu1;
            }
        }

        return null;
    }

    public void a(gu gu1)
    {
        a.add(gu1);
    }

    public boolean a(tu tu)
    {
        tu = c(tu);
        if (tu != null)
        {
            ((gu) (tu)).b.b();
            return true;
        } else
        {
            return false;
        }
    }

    public void b(gu gu1)
    {
        a.remove(gu1);
    }

    public boolean b(tu tu)
    {
        return c(tu) != null;
    }

    public Iterator iterator()
    {
        return a.iterator();
    }
}
