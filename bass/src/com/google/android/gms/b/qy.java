// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import java.util.concurrent.Future;

// Referenced classes of package com.google.android.gms.b:
//            sd, qz, rk

public abstract class qy
    implements sd
{

    private final Runnable a;
    private volatile Thread b;
    private boolean c;

    public qy()
    {
        a = new qz(this);
        c = false;
    }

    public qy(boolean flag)
    {
        a = new qz(this);
        c = flag;
    }

    static Thread a(qy qy1, Thread thread)
    {
        qy1.b = thread;
        return thread;
    }

    public final void cancel()
    {
        onStop();
        if (b != null)
        {
            b.interrupt();
        }
    }

    public abstract void onStop();

    public abstract void zzbp();

    public Object zzfR()
    {
        return zzgX();
    }

    public final Future zzgX()
    {
        if (c)
        {
            return rk.a(1, a);
        } else
        {
            return rk.a(a);
        }
    }
}
