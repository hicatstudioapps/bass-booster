// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.clearcut.LogEventParcelable;

// Referenced classes of package com.google.android.gms.b:
//            vm, vj

class vo
    implements vm
{

    private IBinder a;

    vo(IBinder ibinder)
    {
        a = ibinder;
    }

    public void a(vj vj1, LogEventParcelable logeventparcelable)
    {
        IBinder ibinder;
        Parcel parcel;
        ibinder = null;
        parcel = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.clearcut.internal.IClearcutLoggerService");
        if (vj1 == null)
        {
            break MISSING_BLOCK_LABEL_25;
        }
        ibinder = vj1.asBinder();
        parcel.writeStrongBinder(ibinder);
        if (logeventparcelable == null)
        {
            break MISSING_BLOCK_LABEL_69;
        }
        parcel.writeInt(1);
        logeventparcelable.writeToParcel(parcel, 0);
_L1:
        a.transact(1, parcel, null, 1);
        parcel.recycle();
        return;
        parcel.writeInt(0);
          goto _L1
        vj1;
        parcel.recycle();
        throw vj1;
    }

    public IBinder asBinder()
    {
        return a;
    }
}
