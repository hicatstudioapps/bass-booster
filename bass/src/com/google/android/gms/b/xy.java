// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.support.v4.b.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            xu

public final class xy
{

    public static Map a(Object obj, Object obj1, Object obj2, Object obj3, Object obj4, Object obj5, Object obj6, Object obj7, 
            Object obj8, Object obj9, Object obj10, Object obj11)
    {
        a a1 = new a(6);
        a1.put(obj, obj1);
        a1.put(obj2, obj3);
        a1.put(obj4, obj5);
        a1.put(obj6, obj7);
        a1.put(obj8, obj9);
        a1.put(obj10, obj11);
        return Collections.unmodifiableMap(a1);
    }

    public static Set a()
    {
        return Collections.emptySet();
    }

    public static Set a(Object obj)
    {
        return Collections.singleton(obj);
    }

    public static Set a(Object obj, Object obj1)
    {
        xu xu1 = new xu(2);
        xu1.add(obj);
        xu1.add(obj1);
        return Collections.unmodifiableSet(xu1);
    }

    public static Set a(Object obj, Object obj1, Object obj2)
    {
        xu xu1 = new xu(3);
        xu1.add(obj);
        xu1.add(obj1);
        xu1.add(obj2);
        return Collections.unmodifiableSet(xu1);
    }

    public static Set a(Object obj, Object obj1, Object obj2, Object obj3)
    {
        xu xu1 = new xu(4);
        xu1.add(obj);
        xu1.add(obj1);
        xu1.add(obj2);
        xu1.add(obj3);
        return Collections.unmodifiableSet(xu1);
    }

    public static transient Set a(Object aobj[])
    {
        switch (aobj.length)
        {
        default:
            if (aobj.length <= 32)
            {
                aobj = new xu(Arrays.asList(aobj));
            } else
            {
                aobj = new HashSet(Arrays.asList(aobj));
            }
            return Collections.unmodifiableSet(((Set) (aobj)));

        case 0: // '\0'
            return a();

        case 1: // '\001'
            return a(aobj[0]);

        case 2: // '\002'
            return a(aobj[0], aobj[1]);

        case 3: // '\003'
            return a(aobj[0], aobj[1], aobj[2]);

        case 4: // '\004'
            return a(aobj[0], aobj[1], aobj[2], aobj[3]);
        }
    }
}
