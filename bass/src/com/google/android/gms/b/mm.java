// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.net.TrafficStats;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;

// Referenced classes of package com.google.android.gms.b:
//            uu, xt, yk, kj, 
//            qk, yl, vu, bd

public class mm extends Thread
{

    private final BlockingQueue a;
    private final kj b;
    private final bd c;
    private final xt d;
    private volatile boolean e;

    public mm(BlockingQueue blockingqueue, kj kj1, bd bd1, xt xt1)
    {
        e = false;
        a = blockingqueue;
        b = kj1;
        c = bd1;
        d = xt1;
    }

    private void a(uu uu1)
    {
        if (android.os.Build.VERSION.SDK_INT >= 14)
        {
            TrafficStats.setThreadStatsTag(uu1.c());
        }
    }

    private void a(uu uu1, yk yk1)
    {
        yk1 = uu1.a(yk1);
        d.a(uu1, yk1);
    }

    public void a()
    {
        e = true;
        interrupt();
    }

    public void run()
    {
        Process.setThreadPriority(10);
_L2:
        Object obj;
        long l;
        l = SystemClock.elapsedRealtime();
        yk yk1;
        try
        {
            obj = (uu)a.take();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            if (e)
            {
                return;
            }
            continue; /* Loop/switch isn't completed */
        }
        ((uu) (obj)).b("network-queue-take");
        if (((uu) (obj)).g())
        {
            ((uu) (obj)).c("network-discard-cancelled");
            continue; /* Loop/switch isn't completed */
        }
        a(((uu) (obj)));
        Object obj1 = b.a(((uu) (obj)));
        ((uu) (obj)).b("network-http-complete");
        if (((qk) (obj1)).d && ((uu) (obj)).u())
        {
            ((uu) (obj)).c("not-modified");
            continue; /* Loop/switch isn't completed */
        }
        try
        {
            obj1 = ((uu) (obj)).a(((qk) (obj1)));
            ((uu) (obj)).b("network-parse-complete");
            if (((uu) (obj)).p() && ((vu) (obj1)).b != null)
            {
                c.a(((uu) (obj)).e(), ((vu) (obj1)).b);
                ((uu) (obj)).b("network-cache-written");
            }
            ((uu) (obj)).t();
            d.a(((uu) (obj)), ((vu) (obj1)));
        }
        // Misplaced declaration of an exception variable
        catch (yk yk1)
        {
            yk1.a(SystemClock.elapsedRealtime() - l);
            a(((uu) (obj)), yk1);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            yl.a(((Throwable) (obj1)), "Unhandled exception %s", new Object[] {
                ((Exception) (obj1)).toString()
            });
            obj1 = new yk(((Throwable) (obj1)));
            ((yk) (obj1)).a(SystemClock.elapsedRealtime() - l);
            d.a(((uu) (obj)), ((yk) (obj1)));
        }
        if (true) goto _L2; else goto _L1
_L1:
    }
}
