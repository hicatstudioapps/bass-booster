// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.reward.client.RewardedVideoAdRequestParcel;
import com.google.android.gms.ads.internal.reward.client.zzd;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.ads.internal.zzq;
import com.google.android.gms.common.internal.av;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            qe, qp, jh, jq, 
//            ji, pv, pz, jx, 
//            ka, qq, rq, py, 
//            qh, sd, dq

public class px extends zzb
    implements qe
{

    private zzd l;
    private String m;
    private boolean n;
    private HashMap o;

    public px(Context context, AdSizeParcel adsizeparcel, jx jx1, VersionInfoParcel versioninfoparcel)
    {
        super(context, adsizeparcel, null, jx1, versioninfoparcel, null);
        o = new HashMap();
    }

    public void a(RewardedVideoAdRequestParcel rewardedvideoadrequestparcel)
    {
        av.b("loadAd must be called on the main UI thread.");
        if (TextUtils.isEmpty(rewardedvideoadrequestparcel.zzqP))
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Invalid ad unit id. Aborting.");
            return;
        } else
        {
            n = false;
            f.zzqP = rewardedvideoadrequestparcel.zzqP;
            super.zzb(rewardedvideoadrequestparcel.zzGq);
            return;
        }
    }

    public void a(zzd zzd1)
    {
        av.b("setRewardedVideoAdListener must be called on the main UI thread.");
        l = zzd1;
    }

    public void a(RewardItemParcel rewarditemparcel)
    {
        zzp.zzbK().a(f.context, f.zzqR.afmaVersion, f.zzqW, f.zzqP, false, f.zzqW.l.j);
        if (l == null)
        {
            return;
        }
        try
        {
            if (f.zzqW != null && f.zzqW.o != null && !TextUtils.isEmpty(f.zzqW.o.h))
            {
                l.zza(new pv(f.zzqW.o.h, f.zzqW.o.i));
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (RewardItemParcel rewarditemparcel)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onRewarded().", rewarditemparcel);
            return;
        }
        l.zza(new pv(rewarditemparcel.type, rewarditemparcel.zzJD));
        return;
    }

    protected boolean a(int i1)
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Failed to load ad: ").append(i1).toString());
        d = false;
        if (l == null)
        {
            return false;
        }
        try
        {
            l.onRewardedVideoAdFailedToLoad(i1);
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onAdFailedToLoad().", remoteexception);
            return false;
        }
        return true;
    }

    public void b(String s)
    {
        av.b("setUserId must be called on the main UI thread.");
        m = s;
    }

    public pz c(String s)
    {
        Object obj;
        Object obj1;
        obj = (pz)o.get(s);
        obj1 = obj;
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        obj1 = new pz(j.a(s), this);
        o.put(s, obj1);
        return ((pz) (obj1));
        obj1;
_L2:
        com.google.android.gms.ads.internal.util.client.zzb.zzd((new StringBuilder()).append("Fail to instantiate adapter ").append(s).toString(), ((Throwable) (obj1)));
        return ((pz) (obj));
        Exception exception;
        exception;
        obj = obj1;
        obj1 = exception;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void destroy()
    {
        Iterator iterator;
        av.b("destroy must be called on the main UI thread.");
        iterator = o.keySet().iterator();
_L2:
        String s;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s = (String)iterator.next();
        pz pz1 = (pz)o.get(s);
        if (pz1 != null)
        {
            try
            {
                if (pz1.a() != null)
                {
                    pz1.a().c();
                }
            }
            catch (RemoteException remoteexception)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Fail to destroy adapter: ").append(s).toString());
            }
        }
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void f()
    {
        av.b("showAd must be called on the main UI thread.");
        if (!g())
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("The reward video has not loaded.");
        } else
        {
            n = true;
            pz pz1 = c(f.zzqW.n);
            if (pz1 != null && pz1.a() != null)
            {
                try
                {
                    pz1.a().f();
                    return;
                }
                catch (RemoteException remoteexception)
                {
                    com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call showVideo.", remoteexception);
                }
                return;
            }
        }
    }

    public boolean g()
    {
        av.b("isLoaded must be called on the main UI thread.");
        return f.zzqT == null && f.zzqU == null && f.zzqW != null && !n;
    }

    public void h()
    {
        a(f.zzqW, false);
        if (l == null)
        {
            return;
        }
        try
        {
            l.onRewardedVideoAdOpened();
            return;
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onAdOpened().", remoteexception);
        }
    }

    public void i()
    {
        zzp.zzbK().a(f.context, f.zzqR.afmaVersion, f.zzqW, f.zzqP, false, f.zzqW.l.i);
        if (l == null)
        {
            return;
        }
        try
        {
            l.onRewardedVideoStarted();
            return;
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onVideoStarted().", remoteexception);
        }
    }

    public void j()
    {
        if (l == null)
        {
            return;
        }
        try
        {
            l.onRewardedVideoAdClosed();
            return;
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onAdClosed().", remoteexception);
        }
    }

    public void k()
    {
        onAdClicked();
    }

    public void l()
    {
        if (l == null)
        {
            return;
        }
        try
        {
            l.onRewardedVideoAdLeftApplication();
            return;
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onAdLeftApplication().", remoteexception);
        }
    }

    public void pause()
    {
        Iterator iterator;
        av.b("pause must be called on the main UI thread.");
        iterator = o.keySet().iterator();
_L2:
        String s;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s = (String)iterator.next();
        pz pz1 = (pz)o.get(s);
        if (pz1 != null)
        {
            try
            {
                if (pz1.a() != null)
                {
                    pz1.a().d();
                }
            }
            catch (RemoteException remoteexception)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Fail to pause adapter: ").append(s).toString());
            }
        }
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void resume()
    {
        Iterator iterator;
        av.b("resume must be called on the main UI thread.");
        iterator = o.keySet().iterator();
_L2:
        String s;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s = (String)iterator.next();
        pz pz1 = (pz)o.get(s);
        if (pz1 != null)
        {
            try
            {
                if (pz1.a() != null)
                {
                    pz1.a().e();
                }
            }
            catch (RemoteException remoteexception)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Fail to resume adapter: ").append(s).toString());
            }
        }
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void zza(qq qq1, dq dq)
    {
        if (qq1.e != -2)
        {
            rq.a.post(new py(this, qq1));
            return;
        } else
        {
            f.zzrp = 0;
            f.zzqU = new qh(f.context, m, qq1, this);
            com.google.android.gms.ads.internal.util.client.zzb.zzaF((new StringBuilder()).append("AdRenderer: ").append(f.zzqU.getClass().getName()).toString());
            f.zzqU.zzfR();
            return;
        }
    }

    public boolean zza(qp qp1, qp qp2)
    {
        if (l != null)
        {
            try
            {
                l.onRewardedVideoAdLoaded();
            }
            // Misplaced declaration of an exception variable
            catch (qp qp1)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call RewardedVideoAdListener.onAdLoaded().", qp1);
            }
        }
        return true;
    }
}
