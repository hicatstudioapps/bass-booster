// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;


// Referenced classes of package com.google.android.gms.b:
//            zc, zg, za, yz, 
//            ze, zi

public final class zr extends zc
{

    private static volatile zr e[];
    public String c;
    public String d;

    public zr()
    {
        d();
    }

    public static zr[] c()
    {
        if (e == null)
        {
            synchronized (zg.a)
            {
                if (e == null)
                {
                    e = new zr[0];
                }
            }
        }
        return e;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected int a()
    {
        int j = super.a();
        int i = j;
        if (!c.equals(""))
        {
            i = j + za.b(1, c);
        }
        j = i;
        if (!d.equals(""))
        {
            j = i + za.b(2, d);
        }
        return j;
    }

    public zr a(yz yz1)
    {
        do
        {
            int i = yz1.a();
            switch (i)
            {
            default:
                if (a(yz1, i))
                {
                    continue;
                }
                // fall through

            case 0: // '\0'
                return this;

            case 10: // '\n'
                c = yz1.f();
                break;

            case 18: // '\022'
                d = yz1.f();
                break;
            }
        } while (true);
    }

    public zr d()
    {
        c = "";
        d = "";
        a = null;
        b = -1;
        return this;
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        if (obj != this) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(obj instanceof zr)) goto _L4; else goto _L3
_L3:
        obj = (zr)obj;
        if (c != null) goto _L6; else goto _L5
_L5:
        flag = flag1;
        if (((zr) (obj)).c != null) goto _L4; else goto _L7
_L7:
        if (d != null) goto _L9; else goto _L8
_L8:
        flag = flag1;
        if (((zr) (obj)).d != null) goto _L4; else goto _L10
_L10:
        if (a != null && !a.b())
        {
            break MISSING_BLOCK_LABEL_127;
        }
        if (((zr) (obj)).a == null)
        {
            break; /* Loop/switch isn't completed */
        }
        flag = flag1;
        if (!((zr) (obj)).a.b()) goto _L4; else goto _L11
_L11:
        return true;
_L6:
        if (!c.equals(((zr) (obj)).c))
        {
            return false;
        }
          goto _L7
_L9:
        if (!d.equals(((zr) (obj)).d))
        {
            return false;
        }
          goto _L10
        return a.equals(((zr) (obj)).a);
          goto _L7
    }

    public int hashCode()
    {
        boolean flag = false;
        int l = getClass().getName().hashCode();
        int i;
        int j;
        int k;
        if (c == null)
        {
            i = 0;
        } else
        {
            i = c.hashCode();
        }
        if (d == null)
        {
            j = 0;
        } else
        {
            j = d.hashCode();
        }
        k = ((flag) ? 1 : 0);
        if (a != null)
        {
            if (a.b())
            {
                k = ((flag) ? 1 : 0);
            } else
            {
                k = a.hashCode();
            }
        }
        return (j + (i + (l + 527) * 31) * 31) * 31 + k;
    }

    public zi mergeFrom(yz yz1)
    {
        return a(yz1);
    }

    public void writeTo(za za1)
    {
        if (!c.equals(""))
        {
            za1.a(1, c);
        }
        if (!d.equals(""))
        {
            za1.a(2, d);
        }
        super.writeTo(za1);
    }
}
