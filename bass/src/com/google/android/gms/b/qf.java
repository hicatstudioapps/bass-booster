// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.reward.mediation.client.RewardItemParcel;

// Referenced classes of package com.google.android.gms.b:
//            qe, qd, qg

public class qf extends com.google.android.gms.ads.internal.reward.mediation.client.zza.zza
{

    private qd a;
    private qg b;
    private qe c;

    public qf(qe qe1)
    {
        c = qe1;
    }

    public void a(qd qd1)
    {
        a = qd1;
    }

    public void a(qg qg1)
    {
        b = qg1;
    }

    public void zza(a a1, RewardItemParcel rewarditemparcel)
    {
        if (c != null)
        {
            c.a(rewarditemparcel);
        }
    }

    public void zzb(a a1, int i)
    {
        if (a != null)
        {
            a.a(i);
        }
    }

    public void zzc(a a1, int i)
    {
        if (b != null)
        {
            b.a(d.a(a1).getClass().getName(), i);
        }
    }

    public void zzg(a a1)
    {
        if (a != null)
        {
            a.a();
        }
    }

    public void zzh(a a1)
    {
        if (b != null)
        {
            b.a(d.a(a1).getClass().getName());
        }
    }

    public void zzi(a a1)
    {
        if (c != null)
        {
            c.h();
        }
    }

    public void zzj(a a1)
    {
        if (c != null)
        {
            c.i();
        }
    }

    public void zzk(a a1)
    {
        if (c != null)
        {
            c.j();
        }
    }

    public void zzl(a a1)
    {
        if (c != null)
        {
            c.k();
        }
    }

    public void zzm(a a1)
    {
        if (c != null)
        {
            c.l();
        }
    }
}
