// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.view.View;
import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.zzg;

// Referenced classes of package com.google.android.gms.b:
//            du

public final class dr extends du
{

    private final zzg a;
    private final String b;
    private final String c;

    public dr(zzg zzg1, String s, String s1)
    {
        a = zzg1;
        b = s;
        c = s1;
    }

    public String a()
    {
        return b;
    }

    public void a(a a1)
    {
        if (a1 == null)
        {
            return;
        } else
        {
            a.zzc((View)com.google.android.gms.a.d.a(a1));
            return;
        }
    }

    public String b()
    {
        return c;
    }

    public void c()
    {
        a.recordClick();
    }

    public void d()
    {
        a.recordImpression();
    }
}
