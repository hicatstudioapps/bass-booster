// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.content.Context;
import android.content.MutableContextWrapper;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzk;

// Referenced classes of package com.google.android.gms.b:
//            jx

public class hh
{

    private MutableContextWrapper a;
    private final jx b;
    private final VersionInfoParcel c;
    private final zzd d;

    hh(Context context, jx jx, VersionInfoParcel versioninfoparcel, zzd zzd)
    {
        a = new MutableContextWrapper(context.getApplicationContext());
        b = jx;
        c = versioninfoparcel;
        d = zzd;
    }

    public zzk a(String s)
    {
        return new zzk(a, new AdSizeParcel(), s, b, c, d);
    }

    public hh a()
    {
        return new hh(a.getBaseContext(), b, c, d);
    }

    MutableContextWrapper b()
    {
        return a;
    }
}
