// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.google.android.gms.b:
//            he, mm, uu, yl, 
//            dd, uy, bd, kj, 
//            xt

public class ux
{

    private AtomicInteger a;
    private final Map b;
    private final Set c;
    private final PriorityBlockingQueue d;
    private final PriorityBlockingQueue e;
    private final bd f;
    private final kj g;
    private final xt h;
    private mm i[];
    private dd j;
    private List k;

    public ux(bd bd, kj kj)
    {
        this(bd, kj, 4);
    }

    public ux(bd bd, kj kj, int l)
    {
        this(bd, kj, l, ((xt) (new he(new Handler(Looper.getMainLooper())))));
    }

    public ux(bd bd, kj kj, int l, xt xt)
    {
        a = new AtomicInteger();
        b = new HashMap();
        c = new HashSet();
        d = new PriorityBlockingQueue();
        e = new PriorityBlockingQueue();
        k = new ArrayList();
        f = bd;
        g = kj;
        i = new mm[l];
        h = xt;
    }

    public uu a(uu uu1)
    {
        uu1.a(this);
        synchronized (c)
        {
            c.add(uu1);
        }
        uu1.a(c());
        uu1.b("add-to-queue");
        if (!uu1.p())
        {
            e.add(uu1);
            return uu1;
        }
        break MISSING_BLOCK_LABEL_64;
        uu1;
        set;
        JVM INSTR monitorexit ;
        throw uu1;
        Map map = b;
        map;
        JVM INSTR monitorenter ;
        Queue queue;
        String s;
        s = uu1.e();
        if (!b.containsKey(s))
        {
            break MISSING_BLOCK_LABEL_174;
        }
        queue = (Queue)b.get(s);
        Object obj;
        obj = queue;
        if (queue != null)
        {
            break MISSING_BLOCK_LABEL_122;
        }
        obj = new LinkedList();
        ((Queue) (obj)).add(uu1);
        b.put(s, obj);
        if (yl.b)
        {
            yl.a("Request for cacheKey=%s is in flight, putting on hold.", new Object[] {
                s
            });
        }
_L1:
        map;
        JVM INSTR monitorexit ;
        return uu1;
        uu1;
        map;
        JVM INSTR monitorexit ;
        throw uu1;
        b.put(s, null);
        d.add(uu1);
          goto _L1
    }

    public void a()
    {
        b();
        j = new dd(d, e, f, h);
        j.start();
        for (int l = 0; l < i.length; l++)
        {
            mm mm1 = new mm(e, g, f, h);
            i[l] = mm1;
            mm1.start();
        }

    }

    public void b()
    {
        if (j != null)
        {
            j.a();
        }
        for (int l = 0; l < i.length; l++)
        {
            if (i[l] != null)
            {
                i[l].a();
            }
        }

    }

    void b(uu uu1)
    {
        synchronized (c)
        {
            c.remove(uu1);
        }
        obj = k;
        obj;
        JVM INSTR monitorenter ;
        for (Iterator iterator = k.iterator(); iterator.hasNext(); ((uy)iterator.next()).a(uu1)) { }
        break MISSING_BLOCK_LABEL_74;
        uu1;
        obj;
        JVM INSTR monitorexit ;
        throw uu1;
        uu1;
        obj;
        JVM INSTR monitorexit ;
        throw uu1;
        obj;
        JVM INSTR monitorexit ;
        if (!uu1.p())
        {
            break MISSING_BLOCK_LABEL_161;
        }
        Map map = b;
        map;
        JVM INSTR monitorenter ;
        Queue queue;
        uu1 = uu1.e();
        queue = (Queue)b.remove(uu1);
        if (queue == null)
        {
            break MISSING_BLOCK_LABEL_153;
        }
        if (yl.b)
        {
            yl.a("Releasing %d waiting requests for cacheKey=%s.", new Object[] {
                Integer.valueOf(queue.size()), uu1
            });
        }
        d.addAll(queue);
        map;
        JVM INSTR monitorexit ;
        return;
        uu1;
        map;
        JVM INSTR monitorexit ;
        throw uu1;
    }

    public int c()
    {
        return a.incrementAndGet();
    }
}
