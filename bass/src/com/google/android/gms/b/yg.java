// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.common.internal.av;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.google.android.gms.b:
//            yh

public class yg
    implements ThreadFactory
{

    private final String a;
    private final int b;
    private final AtomicInteger c;
    private final ThreadFactory d;

    public yg(String s)
    {
        this(s, 0);
    }

    public yg(String s, int i)
    {
        c = new AtomicInteger();
        d = Executors.defaultThreadFactory();
        a = (String)av.a(s, "Name must not be null");
        b = i;
    }

    public Thread newThread(Runnable runnable)
    {
        runnable = d.newThread(new yh(runnable, b));
        runnable.setName((new StringBuilder()).append(a).append("[").append(c.getAndIncrement()).append("]").toString());
        return runnable;
    }
}
