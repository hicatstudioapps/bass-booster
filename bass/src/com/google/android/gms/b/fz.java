// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.util.client.zzb;
import java.util.Map;

// Referenced classes of package com.google.android.gms.b:
//            gd, tu

final class fz
    implements gd
{

    fz()
    {
    }

    public void zza(tu tu1, Map map)
    {
        map = tu1.h();
        if (map != null)
        {
            map.close();
            return;
        }
        tu1 = tu1.i();
        if (tu1 != null)
        {
            tu1.close();
            return;
        } else
        {
            zzb.zzaH("A GMSG tried to close something that wasn't an overlay.");
            return;
        }
    }
}
