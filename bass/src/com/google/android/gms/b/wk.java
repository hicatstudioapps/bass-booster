// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.u;
import com.google.android.gms.common.b;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.google.android.gms.b:
//            wp, wc, wl, xa, 
//            yt

class wk extends wp
{

    final wc a;
    private final Map c;

    public wk(wc wc1, Map map)
    {
        a = wc1;
        super(wc1, null);
        c = map;
    }

    public void a()
    {
        int i = com.google.android.gms.b.wc.b(a).a(wc.a(a));
        if (i != 0)
        {
            ConnectionResult connectionresult = new ConnectionResult(i, null);
            wc.d(a).a(new wl(this, a, connectionresult));
        } else
        {
            if (wc.e(a))
            {
                wc.f(a).d();
            }
            Iterator iterator = c.keySet().iterator();
            while (iterator.hasNext()) 
            {
                h h1 = (h)iterator.next();
                h1.zza((u)c.get(h1));
            }
        }
    }
}
