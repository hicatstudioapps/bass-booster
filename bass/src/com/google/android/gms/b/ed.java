// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.b;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.RemoteException;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.util.client.zzb;

// Referenced classes of package com.google.android.gms.b:
//            ea

public class ed extends com.google.android.gms.ads.formats.NativeAd.Image
{

    private final ea a;
    private final Drawable b;
    private final Uri c;
    private final double d;

    public ed(ea ea1)
    {
        Object obj;
        obj = null;
        super();
        a = ea1;
        ea1 = a.zzdC();
        if (ea1 == null) goto _L2; else goto _L1
_L1:
        ea1 = (Drawable)com.google.android.gms.a.d.a(ea1);
_L3:
        b = ea1;
        double d1;
        double d2;
        try
        {
            ea1 = a.getUri();
        }
        // Misplaced declaration of an exception variable
        catch (ea ea1)
        {
            zzb.zzb("Failed to get uri.", ea1);
            ea1 = obj;
        }
        c = ea1;
        d1 = 1.0D;
        d2 = a.getScale();
        d1 = d2;
_L4:
        d = d1;
        return;
        ea1;
        zzb.zzb("Failed to get drawable.", ea1);
_L2:
        ea1 = null;
          goto _L3
        ea1;
        zzb.zzb("Failed to get scale.", ea1);
          goto _L4
    }

    public Drawable getDrawable()
    {
        return b;
    }

    public double getScale()
    {
        return d;
    }

    public Uri getUri()
    {
        return c;
    }
}
