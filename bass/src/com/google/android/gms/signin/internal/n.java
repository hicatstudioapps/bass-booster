// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.a.a;
import com.google.android.gms.b.yt;
import com.google.android.gms.b.yu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.internal.AuthAccountRequest;
import com.google.android.gms.common.internal.BinderWrapper;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.common.internal.ae;
import com.google.android.gms.common.internal.ao;
import com.google.android.gms.common.internal.av;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.m;
import com.google.android.gms.common.internal.s;
import java.util.Set;
import java.util.concurrent.ExecutorService;

// Referenced classes of package com.google.android.gms.signin.internal:
//            o, j, i, AuthAccountResult, 
//            f

public class n extends m
    implements yt
{

    private final boolean a;
    private final h d;
    private final Bundle e;
    private Integer f;

    public n(Context context, Looper looper, boolean flag, h h1, Bundle bundle, q q, r r)
    {
        super(context, looper, 44, h1, q, r);
        a = flag;
        d = h1;
        e = bundle;
        f = h1.i();
    }

    public n(Context context, Looper looper, boolean flag, h h1, yu yu1, q q, r r, 
            ExecutorService executorservice)
    {
        this(context, looper, flag, h1, a(yu1, h1.i(), executorservice), q, r);
    }

    public static Bundle a(yu yu1, Integer integer, ExecutorService executorservice)
    {
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", yu1.a());
        bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", yu1.b());
        bundle.putString("com.google.android.gms.signin.internal.serverClientId", yu1.c());
        if (yu1.d() != null)
        {
            bundle.putParcelable("com.google.android.gms.signin.internal.signInCallbacks", new BinderWrapper((new o(yu1, executorservice)).asBinder()));
        }
        if (integer != null)
        {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", integer.intValue());
        }
        bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", yu1.e());
        bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", yu1.f());
        bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", yu1.g());
        return bundle;
    }

    protected i a(IBinder ibinder)
    {
        return com.google.android.gms.signin.internal.j.a(ibinder);
    }

    protected String a()
    {
        return "com.google.android.gms.signin.service.START";
    }

    public void a(ae ae, Set set, f f1)
    {
        av.a(f1, "Expecting a valid ISignInCallbacks");
        try
        {
            ((i)zzqs()).a(new AuthAccountRequest(ae, set), f1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("SignInClientImpl", "Remote service probably died when authAccount is called");
        }
        try
        {
            f1.a(new ConnectionResult(8, null), new AuthAccountResult(8, null));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Set set)
        {
            Log.wtf("SignInClientImpl", "ISignInCallbacks#onAuthAccount should be executed from the same process, unexpected RemoteException.", ae);
        }
    }

    public void a(ae ae, boolean flag)
    {
        try
        {
            ((i)zzqs()).a(ae, f.intValue(), flag);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    public void a(ao ao1)
    {
        av.a(ao1, "Expecting a valid IResolveAccountCallbacks");
        Object obj;
        Account account;
        try
        {
            account = d.b();
        }
        catch (RemoteException remoteexception)
        {
            Log.w("SignInClientImpl", "Remote service probably died when resolveAccount is called");
            try
            {
                ao1.a(new ResolveAccountResponse(8));
                return;
            }
            // Misplaced declaration of an exception variable
            catch (ao ao1)
            {
                Log.wtf("SignInClientImpl", "IResolveAccountCallbacks#onAccountResolutionComplete should be executed from the same process, unexpected RemoteException.", remoteexception);
            }
            return;
        }
        obj = null;
        if ("<<default account>>".equals(account.name))
        {
            obj = com.google.android.gms.auth.api.signin.a.a.a(getContext()).a();
        }
        obj = new ResolveAccountRequest(account, f.intValue(), ((com.google.android.gms.auth.api.signin.GoogleSignInAccount) (obj)));
        ((i)zzqs()).a(((ResolveAccountRequest) (obj)), ao1);
        return;
    }

    protected IInterface b(IBinder ibinder)
    {
        return a(ibinder);
    }

    protected String b()
    {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    public void c()
    {
        try
        {
            ((i)zzqs()).a(f.intValue());
            return;
        }
        catch (RemoteException remoteexception)
        {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    public void d()
    {
        zza(new s(this));
    }

    protected Bundle e()
    {
        String s1 = d.f();
        if (!getContext().getPackageName().equals(s1))
        {
            e.putString("com.google.android.gms.signin.internal.realClientPackageName", d.f());
        }
        return e;
    }

    public boolean zzmn()
    {
        return a;
    }
}
