// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.y;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.signin.internal:
//            a

public class AuthAccountResult
    implements y, SafeParcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new a();
    final int a;
    private int b;
    private Intent c;

    public AuthAccountResult()
    {
        this(0, null);
    }

    AuthAccountResult(int i, int j, Intent intent)
    {
        a = i;
        b = j;
        c = intent;
    }

    public AuthAccountResult(int i, Intent intent)
    {
        this(2, i, intent);
    }

    public Status a()
    {
        if (b == 0)
        {
            return Status.a;
        } else
        {
            return Status.e;
        }
    }

    public int b()
    {
        return b;
    }

    public Intent c()
    {
        return c;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        com.google.android.gms.signin.internal.a.a(this, parcel, i);
    }

}
