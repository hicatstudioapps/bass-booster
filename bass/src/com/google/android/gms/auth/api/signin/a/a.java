// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.auth.api.signin.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.av;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;

public class a
{

    private static final Lock a = new ReentrantLock();
    private static a b;
    private final Lock c = new ReentrantLock();
    private final SharedPreferences d;

    a(Context context)
    {
        d = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    public static a a(Context context)
    {
        av.a(context);
        a.lock();
        if (b == null)
        {
            b = new a(context.getApplicationContext());
        }
        context = b;
        a.unlock();
        return context;
        context;
        a.unlock();
        throw context;
    }

    private String a(String s, String s1)
    {
        return (new StringBuilder()).append(s).append(":").append(s1).toString();
    }

    public GoogleSignInAccount a()
    {
        return a(b("defaultGoogleSignInAccount"));
    }

    GoogleSignInAccount a(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            if ((s = b(a("googleSignInAccount", s))) != null)
            {
                try
                {
                    s = GoogleSignInAccount.a(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    return null;
                }
                return s;
            }
        }
        return null;
    }

    protected String b(String s)
    {
        c.lock();
        s = d.getString(s, null);
        c.unlock();
        return s;
        s;
        c.unlock();
        throw s;
    }

}
