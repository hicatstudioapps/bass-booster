// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.auth.api.signin;

import android.net.Uri;
import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.b.xx;
import com.google.android.gms.b.xz;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.av;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.auth.api.signin:
//            b, a

public class GoogleSignInAccount
    implements SafeParcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new b();
    public static xx a = xz.d();
    private static Comparator l = new a();
    final int b;
    List c;
    private String d;
    private String e;
    private String f;
    private String g;
    private Uri h;
    private String i;
    private long j;
    private String k;

    GoogleSignInAccount(int i1, String s, String s1, String s2, String s3, Uri uri, String s4, 
            long l1, String s5, List list)
    {
        b = i1;
        d = s;
        e = s1;
        f = s2;
        g = s3;
        h = uri;
        i = s4;
        j = l1;
        k = s5;
        c = list;
    }

    public static GoogleSignInAccount a(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        JSONObject jsonobject = new JSONObject(s);
        s = jsonobject.optString("photoUrl", null);
        HashSet hashset;
        JSONArray jsonarray;
        int j1;
        long l1;
        if (!TextUtils.isEmpty(s))
        {
            s = Uri.parse(s);
        } else
        {
            s = null;
        }
        l1 = Long.parseLong(jsonobject.getString("expirationTime"));
        hashset = new HashSet();
        jsonarray = jsonobject.getJSONArray("grantedScopes");
        j1 = jsonarray.length();
        for (int i1 = 0; i1 < j1; i1++)
        {
            hashset.add(new Scope(jsonarray.getString(i1)));
        }

        return a(jsonobject.optString("id"), jsonobject.optString("tokenId", null), jsonobject.optString("email", null), jsonobject.optString("displayName", null), ((Uri) (s)), Long.valueOf(l1), jsonobject.getString("obfuscatedIdentifier"), ((Set) (hashset))).b(jsonobject.optString("serverAuthCode", null));
    }

    public static GoogleSignInAccount a(String s, String s1, String s2, String s3, Uri uri, Long long1, String s4, Set set)
    {
        Long long2 = long1;
        if (long1 == null)
        {
            long2 = Long.valueOf(a.a() / 1000L);
        }
        return new GoogleSignInAccount(2, s, s1, s2, s3, uri, null, long2.longValue(), av.a(s4), new ArrayList((Collection)av.a(set)));
    }

    private JSONObject j()
    {
        Object obj;
        JSONArray jsonarray;
        obj = new JSONObject();
        try
        {
            if (a() != null)
            {
                ((JSONObject) (obj)).put("id", a());
            }
            if (b() != null)
            {
                ((JSONObject) (obj)).put("tokenId", b());
            }
            if (c() != null)
            {
                ((JSONObject) (obj)).put("email", c());
            }
            if (d() != null)
            {
                ((JSONObject) (obj)).put("displayName", d());
            }
            if (e() != null)
            {
                ((JSONObject) (obj)).put("photoUrl", e().toString());
            }
            if (f() != null)
            {
                ((JSONObject) (obj)).put("serverAuthCode", f());
            }
            ((JSONObject) (obj)).put("expirationTime", j);
            ((JSONObject) (obj)).put("obfuscatedIdentifier", h());
            jsonarray = new JSONArray();
            Collections.sort(c, l);
            for (Iterator iterator = c.iterator(); iterator.hasNext(); jsonarray.put(((Scope)iterator.next()).a())) { }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            throw new RuntimeException(((Throwable) (obj)));
        }
        ((JSONObject) (obj)).put("grantedScopes", jsonarray);
        return ((JSONObject) (obj));
    }

    public String a()
    {
        return d;
    }

    public GoogleSignInAccount b(String s)
    {
        i = s;
        return this;
    }

    public String b()
    {
        return e;
    }

    public String c()
    {
        return f;
    }

    public String d()
    {
        return g;
    }

    public int describeContents()
    {
        return 0;
    }

    public Uri e()
    {
        return h;
    }

    public boolean equals(Object obj)
    {
        if (!(obj instanceof GoogleSignInAccount))
        {
            return false;
        } else
        {
            return ((GoogleSignInAccount)obj).i().equals(i());
        }
    }

    public String f()
    {
        return i;
    }

    public long g()
    {
        return j;
    }

    public String h()
    {
        return k;
    }

    public String i()
    {
        return j().toString();
    }

    public void writeToParcel(Parcel parcel, int i1)
    {
        com.google.android.gms.auth.api.signin.b.a(this, parcel, i1);
    }

}
