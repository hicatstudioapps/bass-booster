// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.a;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.a:
//            f

public abstract class e
{

    private final String a;
    private Object b;

    protected e(String s)
    {
        a = s;
    }

    protected final Object a(Context context)
    {
        if (b == null)
        {
            av.a(context);
            context = com.google.android.gms.common.e.e(context);
            if (context == null)
            {
                throw new f("Could not get remote context.");
            }
            context = context.getClassLoader();
            try
            {
                b = b((IBinder)context.loadClass(a).newInstance());
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                throw new f("Could not load creator class.", context);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                throw new f("Could not instantiate creator.", context);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                throw new f("Could not access creator.", context);
            }
        }
        return b;
    }

    protected abstract Object b(IBinder ibinder);
}
