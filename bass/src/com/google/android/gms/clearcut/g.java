// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.clearcut;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;
import com.google.android.gms.playlog.internal.PlayLoggerContext;

// Referenced classes of package com.google.android.gms.clearcut:
//            LogEventParcelable

public class g
    implements android.os.Parcelable.Creator
{

    public g()
    {
    }

    static void a(LogEventParcelable logeventparcelable, Parcel parcel, int i)
    {
        int j = c.a(parcel);
        c.a(parcel, 1, logeventparcelable.a);
        c.a(parcel, 2, logeventparcelable.b, i, false);
        c.a(parcel, 3, logeventparcelable.c, false);
        c.a(parcel, 4, logeventparcelable.d, false);
        c.a(parcel, j);
    }

    public LogEventParcelable a(Parcel parcel)
    {
        int ai[];
        Object obj;
        byte abyte0[];
        int i;
        int j;
        ai = null;
        j = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        i = 0;
        abyte0 = null;
        obj = null;
_L7:
        int k;
        if (parcel.dataPosition() >= j)
        {
            break MISSING_BLOCK_LABEL_179;
        }
        k = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
        com.google.android.gms.common.internal.safeparcel.a.a(k);
        JVM INSTR tableswitch 1 4: default 68
    //                   1 97
    //                   2 118
    //                   3 142
    //                   4 159;
           goto _L1 _L2 _L3 _L4 _L5
_L5:
        break MISSING_BLOCK_LABEL_159;
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, k);
        byte abyte1[] = abyte0;
        abyte0 = ((byte []) (obj));
        obj = abyte1;
_L8:
        byte abyte2[] = abyte0;
        abyte0 = ((byte []) (obj));
        obj = abyte2;
        if (true) goto _L7; else goto _L6
_L6:
        i = com.google.android.gms.common.internal.safeparcel.a.d(parcel, k);
        Object obj1 = obj;
        obj = abyte0;
        abyte0 = ((byte []) (obj1));
          goto _L8
_L3:
        PlayLoggerContext playloggercontext = (PlayLoggerContext)com.google.android.gms.common.internal.safeparcel.a.a(parcel, k, PlayLoggerContext.CREATOR);
        obj = abyte0;
        abyte0 = playloggercontext;
          goto _L8
_L4:
        byte abyte3[] = com.google.android.gms.common.internal.safeparcel.a.j(parcel, k);
        abyte0 = ((byte []) (obj));
        obj = abyte3;
          goto _L8
        ai = com.google.android.gms.common.internal.safeparcel.a.k(parcel, k);
        byte abyte4[] = ((byte []) (obj));
        obj = abyte0;
        abyte0 = abyte4;
          goto _L8
        if (parcel.dataPosition() != j)
        {
            throw new b((new StringBuilder()).append("Overread allowed size end=").append(j).toString(), parcel);
        } else
        {
            return new LogEventParcelable(i, ((PlayLoggerContext) (obj)), abyte0, ai);
        }
    }

    public LogEventParcelable[] a(int i)
    {
        return new LogEventParcelable[i];
    }

    public Object createFromParcel(Parcel parcel)
    {
        return a(parcel);
    }

    public Object[] newArray(int i)
    {
        return a(i);
    }
}
