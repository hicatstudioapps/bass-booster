// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.clearcut;

import android.os.Parcel;
import com.google.android.gms.b.zq;
import com.google.android.gms.common.internal.ar;
import com.google.android.gms.common.internal.as;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.Arrays;

// Referenced classes of package com.google.android.gms.clearcut:
//            g, d

public class LogEventParcelable
    implements SafeParcelable
{

    public static final g CREATOR = new g();
    public final int a;
    public PlayLoggerContext b;
    public byte c[];
    public int d[];
    public final zq e;
    public final d f;
    public final d g;

    LogEventParcelable(int i, PlayLoggerContext playloggercontext, byte abyte0[], int ai[])
    {
        a = i;
        b = playloggercontext;
        c = abyte0;
        d = ai;
        e = null;
        f = null;
        g = null;
    }

    public LogEventParcelable(PlayLoggerContext playloggercontext, zq zq, d d1, d d2, int ai[])
    {
        a = 1;
        b = playloggercontext;
        e = zq;
        f = d1;
        g = d2;
        d = ai;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj instanceof LogEventParcelable)
            {
                if (a != ((LogEventParcelable) (obj = (LogEventParcelable)obj)).a || !as.a(b, ((LogEventParcelable) (obj)).b) || !Arrays.equals(c, ((LogEventParcelable) (obj)).c) || !Arrays.equals(d, ((LogEventParcelable) (obj)).d) || !as.a(e, ((LogEventParcelable) (obj)).e) || !as.a(f, ((LogEventParcelable) (obj)).f) || !as.a(g, ((LogEventParcelable) (obj)).g))
                {
                    return false;
                }
            } else
            {
                return false;
            }
        }
        return true;
    }

    public int hashCode()
    {
        return as.a(new Object[] {
            Integer.valueOf(a), b, c, d, e, f, g
        });
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder("LogEventParcelable[");
        stringbuilder.append(a);
        stringbuilder.append(", ");
        stringbuilder.append(b);
        stringbuilder.append(", ");
        String s;
        if (c == null)
        {
            s = null;
        } else
        {
            s = new String(c);
        }
        stringbuilder.append(s);
        stringbuilder.append(", ");
        if (d == null)
        {
            s = (String)null;
        } else
        {
            s = ar.a(", ").a(Arrays.asList(new int[][] {
                d
            }));
        }
        stringbuilder.append(s);
        stringbuilder.append(", ");
        stringbuilder.append(e);
        stringbuilder.append(", ");
        stringbuilder.append(f);
        stringbuilder.append(", ");
        stringbuilder.append(g);
        stringbuilder.append("]");
        return stringbuilder.toString();
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        com.google.android.gms.clearcut.g.a(this, parcel, i);
    }

}
