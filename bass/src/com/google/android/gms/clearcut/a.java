// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.clearcut;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.b.uz;
import com.google.android.gms.b.xx;
import com.google.android.gms.b.xz;
import com.google.android.gms.common.api.g;
import com.google.android.gms.common.api.i;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.internal.av;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.clearcut:
//            b, e, c, f

public final class a
{

    public static final i a;
    public static final g b;
    public static final com.google.android.gms.common.api.a c;
    public static final f d = new uz();
    private final Context e;
    private final String f;
    private final int g;
    private String h;
    private int i;
    private String j;
    private String k;
    private final boolean l;
    private int m;
    private final f n;
    private final xx o;
    private e p;

    a(Context context, int i1, String s, String s1, String s2, boolean flag, f f1, 
            xx xx)
    {
        boolean flag1 = false;
        super();
        i = -1;
        m = 0;
        e = context.getApplicationContext();
        f = context.getPackageName();
        g = a(context);
        i = i1;
        h = s;
        j = s1;
        k = s2;
        l = flag;
        n = f1;
        o = xx;
        p = new e();
        m = 0;
        if (l)
        {
            flag = flag1;
            if (j == null)
            {
                flag = true;
            }
            av.b(flag, "can't be anonymous with an upload account");
        }
    }

    public a(Context context, String s, String s1, String s2)
    {
        this(context, -1, s, s1, s2, false, d, xz.d());
    }

    private int a(Context context)
    {
        int i1;
        try
        {
            i1 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.wtf("ClearcutLogger", "This can't happen.");
            return 0;
        }
        return i1;
    }

    static int a(a a1)
    {
        return a1.i;
    }

    static int[] a(ArrayList arraylist)
    {
        return b(arraylist);
    }

    static String b(a a1)
    {
        return a1.h;
    }

    private static int[] b(ArrayList arraylist)
    {
        if (arraylist == null)
        {
            return null;
        }
        int ai[] = new int[arraylist.size()];
        arraylist = arraylist.iterator();
        for (int i1 = 0; arraylist.hasNext(); i1++)
        {
            ai[i1] = ((Integer)arraylist.next()).intValue();
        }

        return ai;
    }

    static String c(a a1)
    {
        return a1.j;
    }

    static String d(a a1)
    {
        return a1.k;
    }

    static int e(a a1)
    {
        return a1.m;
    }

    static xx f(a a1)
    {
        return a1.o;
    }

    static e g(a a1)
    {
        return a1.p;
    }

    static boolean h(a a1)
    {
        return a1.l;
    }

    static String i(a a1)
    {
        return a1.f;
    }

    static int j(a a1)
    {
        return a1.g;
    }

    static f k(a a1)
    {
        return a1.n;
    }

    public c a(byte abyte0[])
    {
        return new c(this, abyte0, null);
    }

    public boolean a(n n1, long l1, TimeUnit timeunit)
    {
        return n.a(n1, l1, timeunit);
    }

    static 
    {
        a = new i();
        b = new b();
        c = new com.google.android.gms.common.api.a("ClearcutLogger.API", b, a);
    }
}
