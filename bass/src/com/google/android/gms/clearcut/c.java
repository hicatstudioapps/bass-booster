// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.clearcut;

import com.google.android.gms.b.xx;
import com.google.android.gms.b.zq;
import com.google.android.gms.common.api.n;
import com.google.android.gms.common.api.v;
import com.google.android.gms.playlog.internal.PlayLoggerContext;
import java.util.ArrayList;

// Referenced classes of package com.google.android.gms.clearcut:
//            a, e, LogEventParcelable, f, 
//            d, b

public class c
{

    final a a;
    private int b;
    private String c;
    private String d;
    private String e;
    private int f;
    private final d g;
    private d h;
    private ArrayList i;
    private final zq j;
    private boolean k;

    private c(a a1, byte abyte0[])
    {
        this(a1, abyte0, ((d) (null)));
    }

    c(a a1, byte abyte0[], b b1)
    {
        this(a1, abyte0);
    }

    private c(a a1, byte abyte0[], d d1)
    {
        a = a1;
        super();
        b = com.google.android.gms.clearcut.a.a(a);
        c = com.google.android.gms.clearcut.a.b(a);
        d = com.google.android.gms.clearcut.a.c(a);
        e = com.google.android.gms.clearcut.a.d(a);
        f = com.google.android.gms.clearcut.a.e(a);
        i = null;
        j = new zq();
        k = false;
        d = com.google.android.gms.clearcut.a.c(a1);
        e = com.google.android.gms.clearcut.a.d(a1);
        j.c = com.google.android.gms.clearcut.a.f(a1).a();
        j.d = com.google.android.gms.clearcut.a.f(a1).b();
        j.q = com.google.android.gms.clearcut.a.g(a1).a(j.c);
        if (abyte0 != null)
        {
            j.l = abyte0;
        }
        g = d1;
    }

    public LogEventParcelable a()
    {
        return new LogEventParcelable(new PlayLoggerContext(com.google.android.gms.clearcut.a.i(a), com.google.android.gms.clearcut.a.j(a), b, c, d, e, com.google.android.gms.clearcut.a.h(a), f), j, g, h, com.google.android.gms.clearcut.a.a(i));
    }

    public c a(int l)
    {
        j.g = l;
        return this;
    }

    public v a(n n)
    {
        if (k)
        {
            throw new IllegalStateException("do not reuse LogEventBuilder");
        } else
        {
            k = true;
            return com.google.android.gms.clearcut.a.k(a).a(n, a());
        }
    }

    public c b(int l)
    {
        j.h = l;
        return this;
    }
}
