// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzab;
import com.google.android.gms.ads.reward.RewardedVideoAd;

public class MobileAds
{

    private MobileAds()
    {
    }

    public static RewardedVideoAd getRewardedVideoAdInstance(Context context)
    {
        return zzab.zzdc().getRewardedVideoAdInstance(context);
    }

    public static void initialize(Context context)
    {
        zzab.zzdc().initialize(context);
    }

    public static void initialize(Context context, String s)
    {
        initialize(context, s, null);
    }

    public static void initialize(Context context, String s, Settings settings)
    {
        zzab zzab1 = zzab.zzdc();
        if (settings == null)
        {
            settings = null;
        } else
        {
            settings = settings.a();
        }
        zzab1.zza(context, s, settings);
    }

    private class Settings
    {

        private final zzac a = new zzac();

        zzac a()
        {
            return a;
        }

        public String getTrackingId()
        {
            return a.getTrackingId();
        }

        public boolean isGoogleAnalyticsEnabled()
        {
            return a.isGoogleAnalyticsEnabled();
        }

        public Settings setGoogleAnalyticsEnabled(boolean flag)
        {
            a.zzm(flag);
            return this;
        }

        public Settings setTrackingId(String s)
        {
            a.zzO(s);
            return this;
        }

        public Settings()
        {
        }
    }

}
