// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.mediation;

import java.util.List;

// Referenced classes of package com.google.android.gms.ads.mediation:
//            NativeAdMapper

public abstract class NativeAppInstallAdMapper extends NativeAdMapper
{

    private String d;
    private List e;
    private String f;
    private com.google.android.gms.ads.formats.NativeAd.Image g;
    private String h;
    private double i;
    private String j;
    private String k;

    public NativeAppInstallAdMapper()
    {
    }

    public final String getBody()
    {
        return f;
    }

    public final String getCallToAction()
    {
        return h;
    }

    public final String getHeadline()
    {
        return d;
    }

    public final com.google.android.gms.ads.formats.NativeAd.Image getIcon()
    {
        return g;
    }

    public final List getImages()
    {
        return e;
    }

    public final String getPrice()
    {
        return k;
    }

    public final double getStarRating()
    {
        return i;
    }

    public final String getStore()
    {
        return j;
    }

    public final void setBody(String s)
    {
        f = s;
    }

    public final void setCallToAction(String s)
    {
        h = s;
    }

    public final void setHeadline(String s)
    {
        d = s;
    }

    public final void setIcon(com.google.android.gms.ads.formats.NativeAd.Image image)
    {
        g = image;
    }

    public final void setImages(List list)
    {
        e = list;
    }

    public final void setPrice(String s)
    {
        k = s;
    }

    public final void setStarRating(double d1)
    {
        i = d1;
    }

    public final void setStore(String s)
    {
        j = s;
    }
}
