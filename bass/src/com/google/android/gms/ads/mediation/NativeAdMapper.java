// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import android.view.View;

public abstract class NativeAdMapper
{

    protected boolean a;
    protected boolean b;
    protected Bundle c;

    public NativeAdMapper()
    {
        c = new Bundle();
    }

    public final Bundle getExtras()
    {
        return c;
    }

    public final boolean getOverrideClickHandling()
    {
        return b;
    }

    public final boolean getOverrideImpressionRecording()
    {
        return a;
    }

    public void handleClick(View view)
    {
    }

    public void recordImpression()
    {
    }

    public final void setExtras(Bundle bundle)
    {
        c = bundle;
    }

    public final void setOverrideClickHandling(boolean flag)
    {
        b = flag;
    }

    public final void setOverrideImpressionRecording(boolean flag)
    {
        a = flag;
    }

    public void trackView(View view)
    {
    }
}
