// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;

// Referenced classes of package com.google.android.gms.ads:
//            b, AdListener, AdSize, AdRequest

public final class NativeExpressAdView extends b
{

    public NativeExpressAdView(Context context)
    {
        super(context, 2);
    }

    public NativeExpressAdView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset, 2);
    }

    public NativeExpressAdView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i, 2);
    }

    public volatile void destroy()
    {
        super.destroy();
    }

    public volatile AdListener getAdListener()
    {
        return super.getAdListener();
    }

    public volatile AdSize getAdSize()
    {
        return super.getAdSize();
    }

    public volatile String getAdUnitId()
    {
        return super.getAdUnitId();
    }

    public volatile InAppPurchaseListener getInAppPurchaseListener()
    {
        return super.getInAppPurchaseListener();
    }

    public volatile String getMediationAdapterClassName()
    {
        return super.getMediationAdapterClassName();
    }

    public volatile boolean isLoading()
    {
        return super.isLoading();
    }

    public volatile void loadAd(AdRequest adrequest)
    {
        super.loadAd(adrequest);
    }

    public volatile void pause()
    {
        super.pause();
    }

    public volatile void resume()
    {
        super.resume();
    }

    public volatile void setAdListener(AdListener adlistener)
    {
        super.setAdListener(adlistener);
    }

    public volatile void setAdSize(AdSize adsize)
    {
        super.setAdSize(adsize);
    }

    public volatile void setAdUnitId(String s)
    {
        super.setAdUnitId(s);
    }

    public volatile void setInAppPurchaseListener(InAppPurchaseListener inapppurchaselistener)
    {
        super.setInAppPurchaseListener(inapppurchaselistener);
    }

    public volatile void setPlayStorePurchaseParams(PlayStorePurchaseListener playstorepurchaselistener, String s)
    {
        super.setPlayStorePurchaseParams(playstorepurchaselistener, s);
    }
}
