// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            zzf, CapabilityParcel

public final class AdRequestInfoParcel
    implements SafeParcelable
{

    public static final zzf CREATOR = new zzf();
    public final ApplicationInfo applicationInfo;
    public final int versionCode;
    public final int zzGA;
    public final int zzGB;
    public final float zzGC;
    public final String zzGD;
    public final long zzGE;
    public final String zzGF;
    public final List zzGG;
    public final List zzGH;
    public final long zzGI;
    public final CapabilityParcel zzGJ;
    public final String zzGK;
    public final Bundle zzGp;
    public final AdRequestParcel zzGq;
    public final PackageInfo zzGr;
    public final String zzGs;
    public final String zzGt;
    public final String zzGu;
    public final Bundle zzGv;
    public final int zzGw;
    public final Bundle zzGx;
    public final boolean zzGy;
    public final Messenger zzGz;
    public final String zzqO;
    public final String zzqP;
    public final VersionInfoParcel zzqR;
    public final AdSizeParcel zzqV;
    public final NativeAdOptionsParcel zzrj;
    public final List zzrl;

    AdRequestInfoParcel(int i, Bundle bundle, AdRequestParcel adrequestparcel, AdSizeParcel adsizeparcel, String s, ApplicationInfo applicationinfo, PackageInfo packageinfo, 
            String s1, String s2, String s3, VersionInfoParcel versioninfoparcel, Bundle bundle1, int j, List list, 
            Bundle bundle2, boolean flag, Messenger messenger, int k, int l, float f, String s4, 
            long l1, String s5, List list1, String s6, NativeAdOptionsParcel nativeadoptionsparcel, List list2, 
            long l2, CapabilityParcel capabilityparcel, String s7)
    {
        versionCode = i;
        zzGp = bundle;
        zzGq = adrequestparcel;
        zzqV = adsizeparcel;
        zzqP = s;
        applicationInfo = applicationinfo;
        zzGr = packageinfo;
        zzGs = s1;
        zzGt = s2;
        zzGu = s3;
        zzqR = versioninfoparcel;
        zzGv = bundle1;
        zzGw = j;
        zzrl = list;
        if (list2 == null)
        {
            bundle = Collections.emptyList();
        } else
        {
            bundle = Collections.unmodifiableList(list2);
        }
        zzGH = bundle;
        zzGx = bundle2;
        zzGy = flag;
        zzGz = messenger;
        zzGA = k;
        zzGB = l;
        zzGC = f;
        zzGD = s4;
        zzGE = l1;
        zzGF = s5;
        if (list1 == null)
        {
            bundle = Collections.emptyList();
        } else
        {
            bundle = Collections.unmodifiableList(list1);
        }
        zzGG = bundle;
        zzqO = s6;
        zzrj = nativeadoptionsparcel;
        zzGI = l2;
        zzGJ = capabilityparcel;
        zzGK = s7;
    }

    public AdRequestInfoParcel(Bundle bundle, AdRequestParcel adrequestparcel, AdSizeParcel adsizeparcel, String s, ApplicationInfo applicationinfo, PackageInfo packageinfo, String s1, 
            String s2, String s3, VersionInfoParcel versioninfoparcel, Bundle bundle1, int i, List list, List list1, 
            Bundle bundle2, boolean flag, Messenger messenger, int j, int k, float f, String s4, 
            long l, String s5, List list2, String s6, NativeAdOptionsParcel nativeadoptionsparcel, long l1, CapabilityParcel capabilityparcel, String s7)
    {
        this(12, bundle, adrequestparcel, adsizeparcel, s, applicationinfo, packageinfo, s1, s2, s3, versioninfoparcel, bundle1, i, list, bundle2, flag, messenger, j, k, f, s4, l, s5, list2, s6, nativeadoptionsparcel, list1, l1, capabilityparcel, s7);
    }

    public AdRequestInfoParcel(zza zza1, String s, long l)
    {
        this(zza1.zzGp, zza1.zzGq, zza1.zzqV, zza1.zzqP, zza1.applicationInfo, zza1.zzGr, s, zza1.zzGt, zza1.zzGu, zza1.zzqR, zza1.zzGv, zza1.zzGw, zza1.zzrl, zza1.zzGH, zza1.zzGx, zza1.zzGy, zza1.zzGz, zza1.zzGA, zza1.zzGB, zza1.zzGC, zza1.zzGD, zza1.zzGE, zza1.zzGF, zza1.zzGG, zza1.zzqO, zza1.zzrj, l, zza1.zzGJ, zza1.zzGK);
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        zzf.a(this, parcel, i);
    }


    private class zza
    {

        public final ApplicationInfo applicationInfo;
        public final int zzGA;
        public final int zzGB;
        public final float zzGC;
        public final String zzGD;
        public final long zzGE;
        public final String zzGF;
        public final List zzGG;
        public final List zzGH;
        public final CapabilityParcel zzGJ;
        public final String zzGK;
        public final Bundle zzGp;
        public final AdRequestParcel zzGq;
        public final PackageInfo zzGr;
        public final String zzGt;
        public final String zzGu;
        public final Bundle zzGv;
        public final int zzGw;
        public final Bundle zzGx;
        public final boolean zzGy;
        public final Messenger zzGz;
        public final String zzqO;
        public final String zzqP;
        public final VersionInfoParcel zzqR;
        public final AdSizeParcel zzqV;
        public final NativeAdOptionsParcel zzrj;
        public final List zzrl;

        public zza(Bundle bundle, AdRequestParcel adrequestparcel, AdSizeParcel adsizeparcel, String s, ApplicationInfo applicationinfo, PackageInfo packageinfo, String s1, 
                String s2, VersionInfoParcel versioninfoparcel, Bundle bundle1, List list, List list1, Bundle bundle2, boolean flag, 
                Messenger messenger, int i, int j, float f, String s3, long l, 
                String s4, List list2, String s5, NativeAdOptionsParcel nativeadoptionsparcel, CapabilityParcel capabilityparcel, String s6)
        {
            zzGp = bundle;
            zzGq = adrequestparcel;
            zzqV = adsizeparcel;
            zzqP = s;
            applicationInfo = applicationinfo;
            zzGr = packageinfo;
            zzGt = s1;
            zzGu = s2;
            zzqR = versioninfoparcel;
            zzGv = bundle1;
            zzGy = flag;
            zzGz = messenger;
            zzGA = i;
            zzGB = j;
            zzGC = f;
            if (list != null && list.size() > 0)
            {
                zzGw = 3;
                zzrl = list;
                zzGH = list1;
            } else
            {
                if (adsizeparcel.zzua)
                {
                    zzGw = 4;
                } else
                {
                    zzGw = 0;
                }
                zzrl = null;
                zzGH = null;
            }
            zzGx = bundle2;
            zzGD = s3;
            zzGE = l;
            zzGF = s4;
            zzGG = list2;
            zzqO = s5;
            zzrj = nativeadoptionsparcel;
            zzGJ = capabilityparcel;
            zzGK = s6;
        }
    }

}
