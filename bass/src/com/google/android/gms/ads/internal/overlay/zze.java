// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.yd;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            AdOverlayInfoParcel, zza

public class zze
{

    public zze()
    {
    }

    public void zza(Context context, AdOverlayInfoParcel adoverlayinfoparcel)
    {
        zza(context, adoverlayinfoparcel, true);
    }

    public void zza(Context context, AdOverlayInfoParcel adoverlayinfoparcel, boolean flag)
    {
        if (adoverlayinfoparcel.zzDI == 4 && adoverlayinfoparcel.zzDB == null)
        {
            if (adoverlayinfoparcel.zzDA != null)
            {
                adoverlayinfoparcel.zzDA.onAdClicked();
            }
            zzp.zzbu().zza(context, adoverlayinfoparcel.zzDz, adoverlayinfoparcel.zzDH);
            return;
        }
        Intent intent = new Intent();
        intent.setClassName(context, "com.google.android.gms.ads.AdActivity");
        intent.putExtra("com.google.android.gms.ads.internal.overlay.useClientJar", adoverlayinfoparcel.zzqR.zzLH);
        intent.putExtra("shouldCallOnOverlayOpened", flag);
        com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel.zza(intent, adoverlayinfoparcel);
        if (!yd.g())
        {
            intent.addFlags(0x80000);
        }
        if (!(context instanceof Activity))
        {
            intent.addFlags(0x10000000);
        }
        zzp.zzbx().a(context, intent);
    }
}
