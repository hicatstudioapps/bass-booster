// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.sd;
import com.google.android.gms.b.tj;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            zzg, zzj, AdResponseParcel, g, 
//            h, AdRequestInfoParcel

public abstract class zzd
    implements zzc.zza, sd
{

    private final tj a;
    private final zzc.zza b;
    private final Object c = new Object();

    public zzd(tj tj1, zzc.zza zza)
    {
        a = tj1;
        b = zza;
    }

    boolean a(zzj zzj1, AdRequestInfoParcel adrequestinfoparcel)
    {
        zzj1.zza(adrequestinfoparcel, new zzg(this));
        return true;
        zzj1;
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not fetch ad response from ad request service.", zzj1);
        zzp.zzbA().a(zzj1, true);
_L2:
        b.zzb(new AdResponseParcel(0));
        return false;
        zzj1;
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not fetch ad response from ad request service due to an Exception.", zzj1);
        zzp.zzbA().a(zzj1, true);
        continue; /* Loop/switch isn't completed */
        zzj1;
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not fetch ad response from ad request service due to an Exception.", zzj1);
        zzp.zzbA().a(zzj1, true);
        continue; /* Loop/switch isn't completed */
        zzj1;
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not fetch ad response from ad request service due to an Exception.", zzj1);
        zzp.zzbA().a(zzj1, true);
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void cancel()
    {
        zzge();
    }

    public void zzb(AdResponseParcel adresponseparcel)
    {
        synchronized (c)
        {
            b.zzb(adresponseparcel);
            zzge();
        }
        return;
        adresponseparcel;
        obj;
        JVM INSTR monitorexit ;
        throw adresponseparcel;
    }

    public Void zzfO()
    {
        zzj zzj1 = zzgf();
        if (zzj1 == null)
        {
            b.zzb(new AdResponseParcel(0));
            zzge();
            return null;
        } else
        {
            a.a(new g(this, zzj1), new h(this));
            return null;
        }
    }

    public Object zzfR()
    {
        return zzfO();
    }

    public abstract void zzge();

    public abstract zzj zzgf();
}
