// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.view.MotionEvent;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.rk;
import com.google.android.gms.b.u;
import com.google.android.gms.b.y;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzq

class g
    implements u, Runnable
{

    CountDownLatch a;
    private final List b = new Vector();
    private final AtomicReference c = new AtomicReference();
    private zzq d;

    public g(zzq zzq1)
    {
        a = new CountDownLatch(1);
        d = zzq1;
        if (zzl.zzcN().zzhr())
        {
            rk.a(this);
            return;
        } else
        {
            run();
            return;
        }
    }

    private Context b(Context context)
    {
        Context context1;
        if (((Boolean)db.m.c()).booleanValue())
        {
            if ((context1 = context.getApplicationContext()) != null)
            {
                return context1;
            }
        }
        return context;
    }

    private void b()
    {
        if (b.isEmpty())
        {
            return;
        }
        Iterator iterator = b.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object aobj[] = (Object[])iterator.next();
            if (aobj.length == 1)
            {
                ((u)c.get()).a((MotionEvent)aobj[0]);
            } else
            if (aobj.length == 3)
            {
                ((u)c.get()).a(((Integer)aobj[0]).intValue(), ((Integer)aobj[1]).intValue(), ((Integer)aobj[2]).intValue());
            }
        } while (true);
        b.clear();
    }

    protected u a(String s, Context context, boolean flag)
    {
        return y.a(s, context, flag);
    }

    public String a(Context context)
    {
        if (a())
        {
            u u1 = (u)c.get();
            if (u1 != null)
            {
                b();
                return u1.a(b(context));
            }
        }
        return "";
    }

    public String a(Context context, String s)
    {
        if (a())
        {
            u u1 = (u)c.get();
            if (u1 != null)
            {
                b();
                return u1.a(b(context), s);
            }
        }
        return "";
    }

    public void a(int i, int j, int k)
    {
        u u1 = (u)c.get();
        if (u1 != null)
        {
            b();
            u1.a(i, j, k);
            return;
        } else
        {
            b.add(((Object) (new Object[] {
                Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k)
            })));
            return;
        }
    }

    public void a(MotionEvent motionevent)
    {
        u u1 = (u)c.get();
        if (u1 != null)
        {
            b();
            u1.a(motionevent);
            return;
        } else
        {
            b.add(((Object) (new Object[] {
                motionevent
            })));
            return;
        }
    }

    protected void a(u u1)
    {
        c.set(u1);
    }

    protected boolean a()
    {
        try
        {
            a.await();
        }
        catch (InterruptedException interruptedexception)
        {
            zzb.zzd("Interrupted during GADSignals creation.", interruptedexception);
            return false;
        }
        return true;
    }

    public void run()
    {
        Exception exception;
        boolean flag;
        if (((Boolean)db.y.c()).booleanValue() && !d.zzqR.zzLH)
        {
            flag = false;
        } else
        {
            flag = true;
        }
        a(a(d.zzqR.afmaVersion, b(d.context), flag));
        a.countDown();
        d = null;
        return;
        exception;
        a.countDown();
        d = null;
        throw exception;
    }
}
