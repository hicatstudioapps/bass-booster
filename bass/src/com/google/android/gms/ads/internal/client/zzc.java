// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;

public final class zzc extends zzo.zza
{

    private final AdListener a;

    public zzc(AdListener adlistener)
    {
        a = adlistener;
    }

    public void onAdClosed()
    {
        a.onAdClosed();
    }

    public void onAdFailedToLoad(int i)
    {
        a.onAdFailedToLoad(i);
    }

    public void onAdLeftApplication()
    {
        a.onAdLeftApplication();
    }

    public void onAdLoaded()
    {
        a.onAdLoaded();
    }

    public void onAdOpened()
    {
        a.onAdOpened();
    }
}
