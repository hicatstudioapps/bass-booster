// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ao;
import com.google.android.gms.b.ap;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.ka;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzc, zzq, zzp, f, 
//            zzd, zze

public class zzf extends zzc
{

    private boolean l;

    public zzf(Context context, AdSizeParcel adsizeparcel, String s, jx jx, VersionInfoParcel versioninfoparcel, zzd zzd)
    {
        super(context, adsizeparcel, s, jx, versioninfoparcel, zzd);
    }

    private AdSizeParcel a(qq qq1)
    {
        if (qq1.b.zztZ)
        {
            return f.zzqV;
        }
        qq1 = qq1.b.zzGQ;
        if (qq1 != null)
        {
            qq1 = qq1.split("[xX]");
            qq1[0] = qq1[0].trim();
            qq1[1] = qq1[1].trim();
            qq1 = new AdSize(Integer.parseInt(qq1[0]), Integer.parseInt(qq1[1]));
        } else
        {
            qq1 = f.zzqV.zzcL();
        }
        return new AdSizeParcel(f.context, qq1);
    }

    private boolean a(qp qp1, qp qp2)
    {
        if (!qp2.k)
        {
            break MISSING_BLOCK_LABEL_194;
        }
        View view;
        try
        {
            qp2 = qp2.m.a();
        }
        // Misplaced declaration of an exception variable
        catch (qp qp1)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not get View from mediation adapter.", qp1);
            return false;
        }
        if (qp2 != null)
        {
            break MISSING_BLOCK_LABEL_28;
        }
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("View in mediation adapter is null.");
        return false;
        qp2 = (View)d.a(qp2);
        view = f.c.getNextView();
        if (view != null)
        {
            if (view instanceof tu)
            {
                ((tu)view).destroy();
            }
            f.c.removeView(view);
        }
        try
        {
            a(((View) (qp2)));
        }
        // Misplaced declaration of an exception variable
        catch (qp qp1)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not add mediation view to view hierarchy.", qp1);
            return false;
        }
_L1:
        if (f.c.getChildCount() > 1)
        {
            f.c.showNext();
        }
        if (qp1 != null)
        {
            qp1 = f.c.getNextView();
            if (qp1 instanceof tu)
            {
                ((tu)qp1).a(f.context, f.zzqV, a);
            } else
            if (qp1 != null)
            {
                f.c.removeView(qp1);
            }
            f.zzbP();
        }
        f.c.setVisibility(0);
        return true;
        if (qp2.r != null && qp2.b != null)
        {
            qp2.b.a(qp2.r);
            f.c.removeAllViews();
            f.c.setMinimumWidth(qp2.r.widthPixels);
            f.c.setMinimumHeight(qp2.r.heightPixels);
            a(qp2.b.b());
        }
          goto _L1
    }

    protected tu a(qq qq1, zze zze)
    {
        if (f.zzqV.zztZ)
        {
            f.zzqV = a(qq1);
        }
        return super.a(qq1, zze);
    }

    AdRequestParcel b(AdRequestParcel adrequestparcel)
    {
        if (adrequestparcel.zztv == l)
        {
            return adrequestparcel;
        }
        int i = adrequestparcel.versionCode;
        long l1 = adrequestparcel.zztq;
        android.os.Bundle bundle = adrequestparcel.extras;
        int j = adrequestparcel.zztr;
        java.util.List list = adrequestparcel.zzts;
        boolean flag1 = adrequestparcel.zztt;
        int k = adrequestparcel.zztu;
        boolean flag;
        if (adrequestparcel.zztv || l)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return new AdRequestParcel(i, l1, bundle, j, list, flag1, k, flag, adrequestparcel.zztw, adrequestparcel.zztx, adrequestparcel.zzty, adrequestparcel.zztz, adrequestparcel.zztA, adrequestparcel.zztB, adrequestparcel.zztC, adrequestparcel.zztD, adrequestparcel.zztE, adrequestparcel.zztF);
    }

    protected boolean e()
    {
        boolean flag = true;
        if (!zzp.zzbx().a(f.context.getPackageManager(), f.context.getPackageName(), "android.permission.INTERNET"))
        {
            zzl.zzcN().zza(f.c, f.zzqV, "Missing internet permission in AndroidManifest.xml.", "Missing internet permission in AndroidManifest.xml. You must have the following declaration: <uses-permission android:name=\"android.permission.INTERNET\" />");
            flag = false;
        }
        if (!zzp.zzbx().a(f.context))
        {
            zzl.zzcN().zza(f.c, f.zzqV, "Missing AdActivity with android:configChanges in AndroidManifest.xml.", "Missing AdActivity with android:configChanges in AndroidManifest.xml. You must have the following declaration within the <application> element: <activity android:name=\"com.google.android.gms.ads.AdActivity\" android:configChanges=\"keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize\" />");
            flag = false;
        }
        if (!flag && f.c != null)
        {
            f.c.setVisibility(0);
        }
        return flag;
    }

    public void setManualImpressionsEnabled(boolean flag)
    {
        av.b("setManualImpressionsEnabled must be called from the main thread.");
        l = flag;
    }

    public void showInterstitial()
    {
        throw new IllegalStateException("Interstitial is NOT supported by BannerAdManager.");
    }

    public boolean zza(qp qp1, qp qp2)
    {
        if (!super.zza(qp1, qp2))
        {
            return false;
        }
        if (f.zzbQ() && !a(qp1, qp2))
        {
            a(0);
            return false;
        }
        a(qp2, false);
        if (!f.zzbQ()) goto _L2; else goto _L1
_L1:
        if (qp2.b != null)
        {
            if (qp2.j != null)
            {
                h.a(f.zzqV, qp2);
            }
            if (qp2.a())
            {
                h.a(f.zzqV, qp2).a(qp2.b);
            } else
            {
                qp2.b.k().a(new f(this, qp2));
            }
        }
_L4:
        return true;
_L2:
        if (f.r != null && qp2.j != null)
        {
            h.a(f.zzqV, qp2, f.r);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public boolean zzb(AdRequestParcel adrequestparcel)
    {
        return super.zzb(b(adrequestparcel));
    }
}
