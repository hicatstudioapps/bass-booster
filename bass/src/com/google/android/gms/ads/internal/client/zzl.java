// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.reward.client.zzf;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.b.fj;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zze, zzad

public class zzl
{

    private static final Object a = new Object();
    private static zzl b;
    private final zza c = new zza();
    private final zze d = new zze();
    private final zzad e = new zzad();
    private final fj f = new fj();
    private final zzf g = new zzf();

    protected zzl()
    {
    }

    private static zzl a()
    {
        zzl zzl1;
        synchronized (a)
        {
            zzl1 = b;
        }
        return zzl1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected static void a(zzl zzl1)
    {
        synchronized (a)
        {
            b = zzl1;
        }
        return;
        zzl1;
        obj;
        JVM INSTR monitorexit ;
        throw zzl1;
    }

    public static zza zzcN()
    {
        return a().c;
    }

    public static zze zzcO()
    {
        return a().d;
    }

    public static zzad zzcP()
    {
        return a().e;
    }

    public static fj zzcQ()
    {
        return a().f;
    }

    public static zzf zzcR()
    {
        return a().g;
    }

    static 
    {
        a(new zzl());
    }
}
