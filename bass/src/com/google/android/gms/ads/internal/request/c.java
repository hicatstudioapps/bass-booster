// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.os.Handler;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.tj;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            zzb

class c
    implements Runnable
{

    final tj a;
    final zzb b;

    c(zzb zzb1, tj tj)
    {
        b = zzb1;
        a = tj;
        super();
    }

    public void run()
    {
        synchronized (zzb.a(b))
        {
            b.a = b.a(zzb.b(b).zzqR, a);
            if (b.a == null)
            {
                zzb.a(b, 0, "Could not start the ad request service.");
                rq.a.removeCallbacks(zzb.c(b));
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
