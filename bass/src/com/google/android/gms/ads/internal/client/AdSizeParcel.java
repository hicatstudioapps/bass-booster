// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzi, zzl

public class AdSizeParcel
    implements SafeParcelable
{

    public static final zzi CREATOR = new zzi();
    public final int height;
    public final int heightPixels;
    public final int versionCode;
    public final int width;
    public final int widthPixels;
    public final String zztV;
    public final boolean zztW;
    public final AdSizeParcel zztX[];
    public final boolean zztY;
    public final boolean zztZ;
    public boolean zzua;

    public AdSizeParcel()
    {
        this(5, "interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    AdSizeParcel(int i, String s, int j, int k, boolean flag, int l, int i1, 
            AdSizeParcel aadsizeparcel[], boolean flag1, boolean flag2, boolean flag3)
    {
        versionCode = i;
        zztV = s;
        height = j;
        heightPixels = k;
        zztW = flag;
        width = l;
        widthPixels = i1;
        zztX = aadsizeparcel;
        zztY = flag1;
        zztZ = flag2;
        zzua = flag3;
    }

    public AdSizeParcel(Context context, AdSize adsize)
    {
        this(context, new AdSize[] {
            adsize
        });
    }

    public AdSizeParcel(Context context, AdSize aadsize[])
    {
        AdSize adsize = aadsize[0];
        versionCode = 5;
        zztW = false;
        zztZ = adsize.isFluid();
        DisplayMetrics displaymetrics;
        int i;
        boolean flag;
        boolean flag1;
        int j;
        if (zztZ)
        {
            width = AdSize.BANNER.getWidth();
            height = AdSize.BANNER.getHeight();
        } else
        {
            width = adsize.getWidth();
            height = adsize.getHeight();
        }
        if (width == -1)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (height == -2)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        displaymetrics = context.getResources().getDisplayMetrics();
        if (flag)
        {
            double d;
            if (zzl.zzcN().zzU(context) && zzl.zzcN().zzV(context))
            {
                widthPixels = zza(displaymetrics) - zzl.zzcN().zzW(context);
            } else
            {
                widthPixels = zza(displaymetrics);
            }
            d = (float)widthPixels / displaymetrics.density;
            j = (int)d;
            i = j;
            if (d - (double)(int)d >= 0.01D)
            {
                i = j + 1;
            }
        } else
        {
            i = width;
            widthPixels = zzl.zzcN().zza(displaymetrics, width);
        }
        if (flag1)
        {
            j = a(displaymetrics);
        } else
        {
            j = height;
        }
        heightPixels = zzl.zzcN().zza(displaymetrics, j);
        if (flag || flag1)
        {
            zztV = (new StringBuilder()).append(i).append("x").append(j).append("_as").toString();
        } else
        if (zztZ)
        {
            zztV = "320x50_mb";
        } else
        {
            zztV = adsize.toString();
        }
        if (aadsize.length > 1)
        {
            zztX = new AdSizeParcel[aadsize.length];
            for (i = 0; i < aadsize.length; i++)
            {
                zztX[i] = new AdSizeParcel(context, aadsize[i]);
            }

        } else
        {
            zztX = null;
        }
        zztY = false;
        zzua = false;
    }

    public AdSizeParcel(AdSizeParcel adsizeparcel, AdSizeParcel aadsizeparcel[])
    {
        this(5, adsizeparcel.zztV, adsizeparcel.height, adsizeparcel.heightPixels, adsizeparcel.zztW, adsizeparcel.width, adsizeparcel.widthPixels, aadsizeparcel, adsizeparcel.zztY, adsizeparcel.zztZ, adsizeparcel.zzua);
    }

    private static int a(DisplayMetrics displaymetrics)
    {
        int i = (int)((float)displaymetrics.heightPixels / displaymetrics.density);
        if (i <= 400)
        {
            return 32;
        }
        return i > 720 ? 90 : 50;
    }

    public static int zza(DisplayMetrics displaymetrics)
    {
        return displaymetrics.widthPixels;
    }

    public static int zzb(DisplayMetrics displaymetrics)
    {
        return (int)((float)a(displaymetrics) * displaymetrics.density);
    }

    public static AdSizeParcel zzcK()
    {
        return new AdSizeParcel(5, "reward_mb", 0, 0, false, 0, 0, null, false, false, false);
    }

    public static AdSizeParcel zzt(Context context)
    {
        return new AdSizeParcel(5, "320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        com.google.android.gms.ads.internal.client.zzi.a(this, parcel, i);
    }

    public AdSize zzcL()
    {
        return com.google.android.gms.ads.zza.zza(width, height, zztV);
    }

    public void zzi(boolean flag)
    {
        zzua = flag;
    }

}
