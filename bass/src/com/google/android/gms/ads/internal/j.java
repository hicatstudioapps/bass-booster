// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.graphics.Bitmap;
import android.os.Handler;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.tu;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzk, zzq, zzp, InterstitialAdParameterParcel, 
//            k

class j extends qy
{

    final zzk a;
    private final Bitmap b;
    private final String c;

    public j(zzk zzk1, Bitmap bitmap, String s)
    {
        a = zzk1;
        super();
        b = bitmap;
        c = s;
    }

    public void onStop()
    {
    }

    public void zzbp()
    {
        Object obj;
        int i;
        int l;
        boolean flag;
        boolean flag1;
        boolean flag2;
        if (a.f.t)
        {
            flag = zzp.zzbx().a(a.f.context, b, c);
        } else
        {
            flag = false;
        }
        flag1 = a.f.t;
        flag2 = a.f();
        if (flag)
        {
            obj = c;
        } else
        {
            obj = null;
        }
        obj = new InterstitialAdParameterParcel(flag1, flag2, ((String) (obj)), zzk.a(a), zzk.b(a));
        l = a.f.zzqW.b.p();
        i = l;
        if (l == -1)
        {
            i = a.f.zzqW.g;
        }
        obj = new AdOverlayInfoParcel(a, a, a, a.f.zzqW.b, i, a.f.zzqR, a.f.zzqW.v, ((InterstitialAdParameterParcel) (obj)));
        rq.a.post(new k(this, ((AdOverlayInfoParcel) (obj))));
    }
}
