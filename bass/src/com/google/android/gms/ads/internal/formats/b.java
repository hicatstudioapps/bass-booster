// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.text.TextUtils;
import com.google.android.gms.b.gd;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import java.util.Map;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zzh, c

class b
    implements gd
{

    final zzh a;

    b(zzh zzh1)
    {
        a = zzh1;
        super();
    }

    public void zza(tu tu1, Map map)
    {
        zzh.c(a).k().a(new c(this, map));
        tu1 = (String)map.get("overlayHtml");
        map = (String)map.get("baseUrl");
        if (TextUtils.isEmpty(map))
        {
            zzh.c(a).loadData(tu1, "text/html", "UTF-8");
            return;
        } else
        {
            zzh.c(a).loadDataWithBaseURL(map, tu1, "text/html", "UTF-8", null);
            return;
        }
    }
}
