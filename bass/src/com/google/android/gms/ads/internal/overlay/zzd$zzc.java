// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.view.ViewGroup;
import com.google.android.gms.b.tu;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            i

public class 
{

    public final Context context;
    public final int index;
    public final android.view.p.LayoutParams zzDv;
    public final ViewGroup zzDw;

    public _cls9(tu tu1)
    {
        zzDv = tu1.getLayoutParams();
        android.view.ViewParent viewparent = tu1.getParent();
        context = tu1.f();
        if (viewparent != null && (viewparent instanceof ViewGroup))
        {
            zzDw = (ViewGroup)viewparent;
            index = zzDw.indexOfChild(tu1.b());
            zzDw.removeView(tu1.b());
            tu1.a(true);
            return;
        } else
        {
            throw new i("Could not get the parent of the WebView for an overlay.");
        }
    }
}
