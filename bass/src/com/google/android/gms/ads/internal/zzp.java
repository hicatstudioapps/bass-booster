// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.overlay.zze;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.request.zza;
import com.google.android.gms.b.cx;
import com.google.android.gms.b.cy;
import com.google.android.gms.b.cz;
import com.google.android.gms.b.di;
import com.google.android.gms.b.gw;
import com.google.android.gms.b.ib;
import com.google.android.gms.b.jq;
import com.google.android.gms.b.nx;
import com.google.android.gms.b.pm;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.rt;
import com.google.android.gms.b.st;
import com.google.android.gms.b.ub;
import com.google.android.gms.b.xx;
import com.google.android.gms.b.xz;

public class zzp
{

    private static final Object a = new Object();
    private static zzp b;
    private final zza c = new zza();
    private final com.google.android.gms.ads.internal.overlay.zza d = new com.google.android.gms.ads.internal.overlay.zza();
    private final zze e = new zze();
    private final nx f = new nx();
    private final rq g = new rq();
    private final ub h = new ub();
    private final rt i;
    private final qt j;
    private final xx k = new xz();
    private final di l = new di();
    private final pm m = new pm();
    private final cy n = new cy();
    private final cx o = new cx();
    private final cz p = new cz();
    private final zzi q = new zzi();
    private final ib r = new ib();
    private final st s = new st();
    private final jq t = new jq();
    private final gw u = new gw();

    protected zzp()
    {
        i = rt.a(android.os.Build.VERSION.SDK_INT);
        j = new qt(g);
    }

    private static zzp a()
    {
        zzp zzp1;
        synchronized (a)
        {
            zzp1 = b;
        }
        return zzp1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected static void a(zzp zzp1)
    {
        synchronized (a)
        {
            b = zzp1;
        }
        return;
        zzp1;
        obj;
        JVM INSTR monitorexit ;
        throw zzp1;
    }

    public static qt zzbA()
    {
        return a().j;
    }

    public static xx zzbB()
    {
        return a().k;
    }

    public static di zzbC()
    {
        return a().l;
    }

    public static pm zzbD()
    {
        return a().m;
    }

    public static cy zzbE()
    {
        return a().n;
    }

    public static cx zzbF()
    {
        return a().o;
    }

    public static cz zzbG()
    {
        return a().p;
    }

    public static zzi zzbH()
    {
        return a().q;
    }

    public static ib zzbI()
    {
        return a().r;
    }

    public static st zzbJ()
    {
        return a().s;
    }

    public static jq zzbK()
    {
        return a().t;
    }

    public static gw zzbL()
    {
        return a().u;
    }

    public static zza zzbt()
    {
        return a().c;
    }

    public static com.google.android.gms.ads.internal.overlay.zza zzbu()
    {
        return a().d;
    }

    public static zze zzbv()
    {
        return a().e;
    }

    public static nx zzbw()
    {
        return a().f;
    }

    public static rq zzbx()
    {
        return a().g;
    }

    public static ub zzby()
    {
        return a().h;
    }

    public static rt zzbz()
    {
        return a().i;
    }

    static 
    {
        a(new zzp());
    }
}
