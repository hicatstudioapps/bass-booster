// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.a.d;
import com.google.android.gms.a.e;
import com.google.android.gms.a.f;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzd;
import com.google.android.gms.ads.internal.zzf;
import com.google.android.gms.ads.internal.zzk;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.if;
import com.google.android.gms.b.jw;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzt, zzl, AdSizeParcel, zzs

public class zze extends e
{

    public zze()
    {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }

    private zzs a(Context context, AdSizeParcel adsizeparcel, String s, jw jw, int i)
    {
        com.google.android.gms.a.a a1 = d.a(context);
        context = com.google.android.gms.ads.internal.client.zzs.zza.zzk(((zzt)a(context)).zza(a1, adsizeparcel, s, jw, 0x7e9e10, i));
        return context;
        context;
_L2:
        com.google.android.gms.ads.internal.util.client.zzb.zza("Could not create remote AdManager.", context);
        return null;
        context;
        if (true) goto _L2; else goto _L1
_L1:
    }

    protected zzt a(IBinder ibinder)
    {
        return zzt.zza.zzl(ibinder);
    }

    protected Object b(IBinder ibinder)
    {
        return a(ibinder);
    }

    public zzs zza(Context context, AdSizeParcel adsizeparcel, String s, jw jw)
    {
        Object obj;
label0:
        {
            if (zzl.zzcN().zzT(context))
            {
                zzs zzs = a(context, adsizeparcel, s, jw, 1);
                obj = zzs;
                if (zzs != null)
                {
                    break label0;
                }
            }
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Using BannerAdManager from the client jar.");
            obj = new zzf(context, adsizeparcel, s, jw, new VersionInfoParcel(0x7e9e10, 0x7e9e10, true), zzd.zzbf());
        }
        return ((zzs) (obj));
    }

    public zzs zzb(Context context, AdSizeParcel adsizeparcel, String s, jw jw)
    {
        Object obj;
label0:
        {
label1:
            {
                if (zzl.zzcN().zzT(context))
                {
                    zzs zzs = a(context, adsizeparcel, s, jw, 2);
                    obj = zzs;
                    if (zzs != null)
                    {
                        break label1;
                    }
                }
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Using InterstitialAdManager from the client jar.");
                obj = new VersionInfoParcel(0x7e9e10, 0x7e9e10, true);
                if (!((Boolean)db.ae.c()).booleanValue())
                {
                    break label0;
                }
                obj = new if(context, s, jw, ((VersionInfoParcel) (obj)), zzd.zzbf());
            }
            return ((zzs) (obj));
        }
        return new zzk(context, adsizeparcel, s, jw, ((VersionInfoParcel) (obj)), zzd.zzbf());
    }
}
