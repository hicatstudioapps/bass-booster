// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            AdRequestParcel, SearchAdRequestParcel

public final class zzf
{

    private long a;
    private Bundle b;
    private int c;
    private List d;
    private boolean e;
    private int f;
    private boolean g;
    private String h;
    private SearchAdRequestParcel i;
    private Location j;
    private String k;
    private Bundle l;
    private Bundle m;
    private List n;
    private String o;
    private String p;
    private boolean q;

    public zzf()
    {
        a = -1L;
        b = new Bundle();
        c = -1;
        d = new ArrayList();
        e = false;
        f = -1;
        g = false;
        h = null;
        i = null;
        j = null;
        k = null;
        l = new Bundle();
        m = new Bundle();
        n = new ArrayList();
        o = null;
        p = null;
        q = false;
    }

    public zzf(AdRequestParcel adrequestparcel)
    {
        a = adrequestparcel.zztq;
        b = adrequestparcel.extras;
        c = adrequestparcel.zztr;
        d = adrequestparcel.zzts;
        e = adrequestparcel.zztt;
        f = adrequestparcel.zztu;
        g = adrequestparcel.zztv;
        h = adrequestparcel.zztw;
        i = adrequestparcel.zztx;
        j = adrequestparcel.zzty;
        k = adrequestparcel.zztz;
        l = adrequestparcel.zztA;
        m = adrequestparcel.zztB;
        n = adrequestparcel.zztC;
        o = adrequestparcel.zztD;
        p = adrequestparcel.zztE;
    }

    public zzf zza(Location location)
    {
        j = location;
        return this;
    }

    public AdRequestParcel zzcI()
    {
        return new AdRequestParcel(7, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q);
    }
}
