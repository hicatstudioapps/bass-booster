// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import com.google.android.gms.a.a;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.mt;
import com.google.android.gms.b.nf;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            AdSizeParcel, zzn, zzo, zzu, 
//            zzv, AdRequestParcel

public interface zzs
    extends IInterface
{

    public abstract void destroy();

    public abstract String getMediationAdapterClassName();

    public abstract boolean isLoading();

    public abstract boolean isReady();

    public abstract void pause();

    public abstract void resume();

    public abstract void setManualImpressionsEnabled(boolean flag);

    public abstract void showInterstitial();

    public abstract void stopLoading();

    public abstract void zza(AdSizeParcel adsizeparcel);

    public abstract void zza(zzn zzn);

    public abstract void zza(zzo zzo);

    public abstract void zza(zzu zzu);

    public abstract void zza(zzv zzv);

    public abstract void zza(dw dw);

    public abstract void zza(mt mt);

    public abstract void zza(nf nf, String s);

    public abstract a zzaO();

    public abstract AdSizeParcel zzaP();

    public abstract void zzaR();

    public abstract boolean zzb(AdRequestParcel adrequestparcel);
}
