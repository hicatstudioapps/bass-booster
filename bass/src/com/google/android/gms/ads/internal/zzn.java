// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v4.b.l;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.formats.zzd;
import com.google.android.gms.ads.internal.formats.zze;
import com.google.android.gms.ads.internal.formats.zzf;
import com.google.android.gms.ads.internal.formats.zzg;
import com.google.android.gms.ads.internal.formats.zzh;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ao;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.fc;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.ka;
import com.google.android.gms.b.kk;
import com.google.android.gms.b.kn;
import com.google.android.gms.b.mt;
import com.google.android.gms.b.nx;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.rq;
import com.google.android.gms.common.internal.av;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzb, m, n, o, 
//            zzo, zzq, zzp, l

public class zzn extends com.google.android.gms.ads.internal.zzb
{

    public zzn(Context context, AdSizeParcel adsizeparcel, String s, jx jx, VersionInfoParcel versioninfoparcel)
    {
        super(context, adsizeparcel, s, jx, versioninfoparcel, null);
    }

    private static zzd a(kk kk1)
    {
        String s = kk1.a();
        List list = kk1.b();
        String s1 = kk1.c();
        com.google.android.gms.b.ea ea;
        if (kk1.d() != null)
        {
            ea = kk1.d();
        } else
        {
            ea = null;
        }
        return new zzd(s, list, s1, ea, kk1.e(), kk1.f(), kk1.g(), kk1.h(), null, kk1.l());
    }

    private static zze a(kn kn1)
    {
        String s = kn1.a();
        List list = kn1.b();
        String s1 = kn1.c();
        com.google.android.gms.b.ea ea;
        if (kn1.d() != null)
        {
            ea = kn1.d();
        } else
        {
            ea = null;
        }
        return new zze(s, list, s1, ea, kn1.e(), kn1.f(), null, kn1.j());
    }

    private void a(zzd zzd1)
    {
        rq.a.post(new m(this, zzd1));
    }

    private void a(zze zze1)
    {
        rq.a.post(new n(this, zze1));
    }

    private void a(qp qp1, String s)
    {
        rq.a.post(new o(this, s, qp1));
    }

    protected boolean a(AdRequestParcel adrequestparcel, qp qp1, boolean flag)
    {
        return e.zzbr();
    }

    public void pause()
    {
        throw new IllegalStateException("Native Ad DOES NOT support pause().");
    }

    public void recordImpression()
    {
        a(f.zzqW, false);
    }

    public void resume()
    {
        throw new IllegalStateException("Native Ad DOES NOT support resume().");
    }

    public void showInterstitial()
    {
        throw new IllegalStateException("Interstitial is NOT supported by NativeAdManager.");
    }

    public void zza(l l1)
    {
        av.b("setOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        f.m = l1;
    }

    public void zza(zzh zzh)
    {
        if (f.zzqW.j != null)
        {
            zzp.zzbA().j().a(f.zzqV, f.zzqW, zzh);
        }
    }

    public void zza(dw dw)
    {
        throw new IllegalStateException("CustomRendering is NOT supported by NativeAdManager.");
    }

    public void zza(mt mt)
    {
        throw new IllegalStateException("In App Purchase is NOT supported by NativeAdManager.");
    }

    public void zza(qq qq1, dq dq)
    {
        if (qq1.d != null)
        {
            f.zzqV = qq1.d;
        }
        if (qq1.e != -2)
        {
            rq.a.post(new com.google.android.gms.ads.internal.l(this, qq1));
            return;
        } else
        {
            f.zzrp = 0;
            f.zzqU = zzp.zzbw().a(f.context, this, qq1, f.b, null, j, this, dq);
            com.google.android.gms.ads.internal.util.client.zzb.zzaF((new StringBuilder()).append("AdRenderer: ").append(f.zzqU.getClass().getName()).toString());
            return;
        }
    }

    public void zza(List list)
    {
        av.b("setNativeTemplates must be called on the main UI thread.");
        f.p = list;
    }

    protected boolean zza(qp qp1, qp qp2)
    {
        zza(((List) (null)));
        if (!f.zzbQ())
        {
            throw new IllegalStateException("Native ad DOES NOT have custom rendering mode.");
        }
        if (!qp2.k) goto _L2; else goto _L1
_L1:
        kk kk1;
        Object obj;
        kk1 = qp2.m.h();
        obj = qp2.m.i();
        if (kk1 == null) goto _L4; else goto _L3
_L3:
        try
        {
            obj = a(kk1);
            ((zzd) (obj)).zzb(new zzg(f.context, this, f.b, kk1));
            a(((zzd) (obj)));
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Failed to get native ad mapper", remoteexception);
        }
_L6:
        return super.zza(qp1, qp2);
_L4:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_163;
        }
        zze zze1 = a(((kn) (obj)));
        zze1.zzb(new zzg(f.context, this, f.b, ((kn) (obj))));
        a(zze1);
        continue; /* Loop/switch isn't completed */
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("No matching mapper for retrieved native ad template.");
        a(0);
        return false;
_L2:
        com.google.android.gms.ads.internal.formats.zzh.zza zza1 = qp2.w;
        if ((zza1 instanceof zze) && f.k != null)
        {
            a((zze)qp2.w);
        } else
        if ((zza1 instanceof zzd) && f.j != null)
        {
            a((zzd)qp2.w);
        } else
        if ((zza1 instanceof zzf) && f.m != null && f.m.get(((zzf)zza1).getCustomTemplateId()) != null)
        {
            a(qp2, ((zzf)zza1).getCustomTemplateId());
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("No matching listener for retrieved native ad template.");
            a(0);
            return false;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void zzb(l l1)
    {
        av.b("setOnCustomClickListener must be called on the main UI thread.");
        f.l = l1;
    }

    public void zzb(NativeAdOptionsParcel nativeadoptionsparcel)
    {
        av.b("setNativeAdOptions must be called on the main UI thread.");
        f.n = nativeadoptionsparcel;
    }

    public void zzb(ew ew)
    {
        av.b("setOnAppInstallAdLoadedListener must be called on the main UI thread.");
        f.j = ew;
    }

    public void zzb(ez ez)
    {
        av.b("setOnContentAdLoadedListener must be called on the main UI thread.");
        f.k = ez;
    }

    public l zzbq()
    {
        av.b("getOnCustomTemplateAdLoadedListeners must be called on the main UI thread.");
        return f.m;
    }

    public fc zzr(String s)
    {
        av.b("getOnCustomClickListener must be called on the main UI thread.");
        return (fc)f.l.get(s);
    }
}
