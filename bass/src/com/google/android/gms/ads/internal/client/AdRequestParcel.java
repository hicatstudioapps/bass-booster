// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.as;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzg, SearchAdRequestParcel

public final class AdRequestParcel
    implements SafeParcelable
{

    public static final zzg CREATOR = new zzg();
    public final Bundle extras;
    public final int versionCode;
    public final Bundle zztA;
    public final Bundle zztB;
    public final List zztC;
    public final String zztD;
    public final String zztE;
    public final boolean zztF;
    public final long zztq;
    public final int zztr;
    public final List zzts;
    public final boolean zztt;
    public final int zztu;
    public final boolean zztv;
    public final String zztw;
    public final SearchAdRequestParcel zztx;
    public final Location zzty;
    public final String zztz;

    public AdRequestParcel(int i, long l, Bundle bundle, int j, List list, boolean flag, 
            int k, boolean flag1, String s, SearchAdRequestParcel searchadrequestparcel, Location location, String s1, Bundle bundle1, 
            Bundle bundle2, List list1, String s2, String s3, boolean flag2)
    {
        versionCode = i;
        zztq = l;
        Bundle bundle3 = bundle;
        if (bundle == null)
        {
            bundle3 = new Bundle();
        }
        extras = bundle3;
        zztr = j;
        zzts = list;
        zztt = flag;
        zztu = k;
        zztv = flag1;
        zztw = s;
        zztx = searchadrequestparcel;
        zzty = location;
        zztz = s1;
        zztA = bundle1;
        zztB = bundle2;
        zztC = list1;
        zztD = s2;
        zztE = s3;
        zztF = flag2;
    }

    public int describeContents()
    {
        return 0;
    }

    public boolean equals(Object obj)
    {
        if (obj instanceof AdRequestParcel)
        {
            if (versionCode == ((AdRequestParcel) (obj = (AdRequestParcel)obj)).versionCode && zztq == ((AdRequestParcel) (obj)).zztq && as.a(extras, ((AdRequestParcel) (obj)).extras) && zztr == ((AdRequestParcel) (obj)).zztr && as.a(zzts, ((AdRequestParcel) (obj)).zzts) && zztt == ((AdRequestParcel) (obj)).zztt && zztu == ((AdRequestParcel) (obj)).zztu && zztv == ((AdRequestParcel) (obj)).zztv && as.a(zztw, ((AdRequestParcel) (obj)).zztw) && as.a(zztx, ((AdRequestParcel) (obj)).zztx) && as.a(zzty, ((AdRequestParcel) (obj)).zzty) && as.a(zztz, ((AdRequestParcel) (obj)).zztz) && as.a(zztA, ((AdRequestParcel) (obj)).zztA) && as.a(zztB, ((AdRequestParcel) (obj)).zztB) && as.a(zztC, ((AdRequestParcel) (obj)).zztC) && as.a(zztD, ((AdRequestParcel) (obj)).zztD) && as.a(zztE, ((AdRequestParcel) (obj)).zztE) && zztF == ((AdRequestParcel) (obj)).zztF)
            {
                return true;
            }
        }
        return false;
    }

    public int hashCode()
    {
        return as.a(new Object[] {
            Integer.valueOf(versionCode), Long.valueOf(zztq), extras, Integer.valueOf(zztr), zzts, Boolean.valueOf(zztt), Integer.valueOf(zztu), Boolean.valueOf(zztv), zztw, zztx, 
            zzty, zztz, zztA, zztB, zztC, zztD, zztE, Boolean.valueOf(zztF)
        });
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        zzg.a(this, parcel, i);
    }

}
