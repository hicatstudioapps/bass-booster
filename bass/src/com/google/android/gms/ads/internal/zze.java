// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;

public class zze
{

    private zza a;
    private boolean b;
    private boolean c;

    public zze()
    {
        c = ((Boolean)db.i.c()).booleanValue();
    }

    public zze(boolean flag)
    {
        c = flag;
    }

    public void recordClick()
    {
        b = true;
    }

    public void zza(zza zza1)
    {
        a = zza1;
    }

    public boolean zzbg()
    {
        return !c || b;
    }

    public void zzp(String s)
    {
        zzb.zzaF("Action was blocked because no click was detected.");
        if (a != null)
        {
            a.zzq(s);
        }
    }

    private class zza
    {

        public abstract void zzq(String s);
    }

}
