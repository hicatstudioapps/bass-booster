// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ef;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.tp;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zzh, zzg, a, h

public class zzj extends ef
    implements android.view.View.OnClickListener, android.view.View.OnTouchListener, android.view.ViewTreeObserver.OnGlobalLayoutListener, android.view.ViewTreeObserver.OnScrollChangedListener
{

    boolean a;
    int b;
    int c;
    private final Object d = new Object();
    private final FrameLayout e;
    private FrameLayout f;
    private Map g;
    private com.google.android.gms.ads.internal.formats.a h;
    private zzh i;

    public zzj(FrameLayout framelayout, FrameLayout framelayout1)
    {
        g = new HashMap();
        a = false;
        e = framelayout;
        f = framelayout1;
        tp.a(e, this);
        tp.a(e, this);
        e.setOnTouchListener(this);
    }

    static FrameLayout a(zzj zzj1)
    {
        return zzj1.f;
    }

    int a()
    {
        return e.getMeasuredWidth();
    }

    int a(int j)
    {
        return zzl.zzcN().zzc(i.getContext(), j);
    }

    Point a(MotionEvent motionevent)
    {
        int ai[] = new int[2];
        e.getLocationOnScreen(ai);
        float f1 = motionevent.getRawX();
        float f2 = ai[0];
        float f3 = motionevent.getRawY();
        float f4 = ai[1];
        return new Point((int)(f1 - f2), (int)(f3 - f4));
    }

    com.google.android.gms.ads.internal.formats.a a(zzh zzh1)
    {
        return zzh1.zza(this);
    }

    void a(View view)
    {
        if (i != null)
        {
            zzh zzh1;
            if (i instanceof zzg)
            {
                zzh1 = ((zzg)i).zzdJ();
            } else
            {
                zzh1 = i;
            }
            if (zzh1 != null)
            {
                zzh1.zzj(view);
            }
        }
    }

    int b()
    {
        return e.getMeasuredHeight();
    }

    Point b(View view)
    {
        if (h != null && h.a().equals(view))
        {
            Point point = new Point();
            e.getGlobalVisibleRect(new Rect(), point);
            Point point2 = new Point();
            view.getGlobalVisibleRect(new Rect(), point2);
            return new Point(point2.x - point.x, point2.y - point.y);
        } else
        {
            Point point1 = new Point();
            view.getGlobalVisibleRect(new Rect(), point1);
            return point1;
        }
    }

    public void destroy()
    {
        f.removeAllViews();
        f = null;
        g = null;
        h = null;
        i = null;
    }

    public void onClick(View view)
    {
label0:
        {
            synchronized (d)
            {
                if (i != null)
                {
                    break label0;
                }
            }
            return;
        }
        JSONObject jsonobject;
        Object obj1;
        jsonobject = new JSONObject();
        obj1 = g.entrySet().iterator();
_L1:
        Object obj2;
        View view1;
        Point point;
        JSONObject jsonobject1;
        if (!((Iterator) (obj1)).hasNext())
        {
            break MISSING_BLOCK_LABEL_227;
        }
        obj2 = (java.util.Map.Entry)((Iterator) (obj1)).next();
        view1 = (View)((WeakReference)((java.util.Map.Entry) (obj2)).getValue()).get();
        point = b(view1);
        jsonobject1 = new JSONObject();
        jsonobject1.put("width", a(view1.getWidth()));
        jsonobject1.put("height", a(view1.getHeight()));
        jsonobject1.put("x", a(point.x));
        jsonobject1.put("y", a(point.y));
        jsonobject.put((String)((java.util.Map.Entry) (obj2)).getKey(), jsonobject1);
          goto _L1
        JSONException jsonexception;
        jsonexception;
        com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Unable to get view rectangle for view ").append((String)((java.util.Map.Entry) (obj2)).getKey()).toString());
          goto _L1
        view;
        obj;
        JVM INSTR monitorexit ;
        throw view;
        obj1 = new JSONObject();
        ((JSONObject) (obj1)).put("x", a(b));
        ((JSONObject) (obj1)).put("y", a(c));
_L2:
        obj2 = new JSONObject();
        ((JSONObject) (obj2)).put("width", a(a()));
        ((JSONObject) (obj2)).put("height", a(b()));
_L3:
        if (h == null || !h.a().equals(view))
        {
            break MISSING_BLOCK_LABEL_370;
        }
        i.zza("1007", jsonobject, ((JSONObject) (obj1)), ((JSONObject) (obj2)));
_L4:
        obj;
        JVM INSTR monitorexit ;
        return;
        obj2;
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("Unable to get click location");
          goto _L2
        jsonexception;
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("Unable to get native ad view bounding box");
          goto _L3
        i.zza(view, g, jsonobject, ((JSONObject) (obj1)), ((JSONObject) (obj2)));
          goto _L4
    }

    public void onGlobalLayout()
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        int j;
        int k;
        if (!a)
        {
            break MISSING_BLOCK_LABEL_56;
        }
        j = a();
        k = b();
        if (j == 0 || k == 0)
        {
            break MISSING_BLOCK_LABEL_56;
        }
        f.setLayoutParams(new android.widget.FrameLayout.LayoutParams(j, k));
        a = false;
        if (i != null)
        {
            i.zzi(e);
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void onScrollChanged()
    {
        synchronized (d)
        {
            if (i != null)
            {
                i.zzi(e);
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean onTouch(View view, MotionEvent motionevent)
    {
label0:
        {
            synchronized (d)
            {
                if (i != null)
                {
                    break label0;
                }
            }
            return false;
        }
        Point point = a(motionevent);
        b = point.x;
        c = point.y;
        motionevent = MotionEvent.obtain(motionevent);
        motionevent.setLocation(point.x, point.y);
        i.zzb(motionevent);
        motionevent.recycle();
        view;
        JVM INSTR monitorexit ;
        return false;
        motionevent;
        view;
        JVM INSTR monitorexit ;
        throw motionevent;
    }

    public a zzU(String s)
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        s = (WeakReference)g.get(s);
        if (s != null) goto _L2; else goto _L1
_L1:
        s = null;
_L4:
        s = com.google.android.gms.a.d.a(s);
        obj;
        JVM INSTR monitorexit ;
        return s;
_L2:
        s = (View)s.get();
        if (true) goto _L4; else goto _L3
_L3:
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void zza(String s, a a1)
    {
        View view = (View)com.google.android.gms.a.d.a(a1);
        a1 = ((a) (d));
        a1;
        JVM INSTR monitorenter ;
        if (view != null)
        {
            break MISSING_BLOCK_LABEL_33;
        }
        g.remove(s);
_L2:
        a1;
        JVM INSTR monitorexit ;
        return;
        g.put(s, new WeakReference(view));
        view.setOnTouchListener(this);
        view.setOnClickListener(this);
        if (true) goto _L2; else goto _L1
_L1:
        s;
        a1;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void zzb(a a1)
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        a = true;
        a(((View) (null)));
        a1 = (zzh)com.google.android.gms.a.d.a(a1);
        if (!(i instanceof zzg) || !((zzg)i).zzdI())
        {
            break MISSING_BLOCK_LABEL_155;
        }
        ((zzg)i).zzc(a1);
_L2:
        f.removeAllViews();
        h = a(a1);
        if (h != null)
        {
            g.put("1007", new WeakReference(h.a()));
            f.addView(h);
        }
        rq.a.post(new h(this, a1));
        a1.zzh(e);
        a(e);
        return;
        i = a1;
        if (i instanceof zzg)
        {
            ((zzg)i).zzc(null);
        }
        if (true) goto _L2; else goto _L1
_L1:
        a1;
        obj;
        JVM INSTR monitorexit ;
        throw a1;
    }
}
