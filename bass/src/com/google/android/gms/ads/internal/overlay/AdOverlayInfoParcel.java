// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.a.a;
import com.google.android.gms.a.b;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.b.fp;
import com.google.android.gms.b.gl;
import com.google.android.gms.b.tu;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            zzf, zzg, zzn, AdLauncherIntentInfoParcel

public final class AdOverlayInfoParcel
    implements SafeParcelable
{

    public static final zzf CREATOR = new zzf();
    public final int orientation;
    public final String url;
    public final int versionCode;
    public final zza zzDA;
    public final zzg zzDB;
    public final tu zzDC;
    public final fp zzDD;
    public final String zzDE;
    public final boolean zzDF;
    public final String zzDG;
    public final zzn zzDH;
    public final int zzDI;
    public final gl zzDJ;
    public final String zzDK;
    public final InterstitialAdParameterParcel zzDL;
    public final AdLauncherIntentInfoParcel zzDz;
    public final VersionInfoParcel zzqR;

    AdOverlayInfoParcel(int i, AdLauncherIntentInfoParcel adlauncherintentinfoparcel, IBinder ibinder, IBinder ibinder1, IBinder ibinder2, IBinder ibinder3, String s, 
            boolean flag, String s1, IBinder ibinder4, int j, int k, String s2, VersionInfoParcel versioninfoparcel, 
            IBinder ibinder5, String s3, InterstitialAdParameterParcel interstitialadparameterparcel)
    {
        versionCode = i;
        zzDz = adlauncherintentinfoparcel;
        zzDA = (zza)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder));
        zzDB = (zzg)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder1));
        zzDC = (tu)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder2));
        zzDD = (fp)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder3));
        zzDE = s;
        zzDF = flag;
        zzDG = s1;
        zzDH = (zzn)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder4));
        orientation = j;
        zzDI = k;
        url = s2;
        zzqR = versioninfoparcel;
        zzDJ = (gl)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder5));
        zzDK = s3;
        zzDL = interstitialadparameterparcel;
    }

    public AdOverlayInfoParcel(zza zza1, zzg zzg1, zzn zzn1, tu tu1, int i, VersionInfoParcel versioninfoparcel, String s, 
            InterstitialAdParameterParcel interstitialadparameterparcel)
    {
        versionCode = 4;
        zzDz = null;
        zzDA = zza1;
        zzDB = zzg1;
        zzDC = tu1;
        zzDD = null;
        zzDE = null;
        zzDF = false;
        zzDG = null;
        zzDH = zzn1;
        orientation = i;
        zzDI = 1;
        url = null;
        zzqR = versioninfoparcel;
        zzDJ = null;
        zzDK = s;
        zzDL = interstitialadparameterparcel;
    }

    public AdOverlayInfoParcel(zza zza1, zzg zzg1, zzn zzn1, tu tu1, boolean flag, int i, VersionInfoParcel versioninfoparcel)
    {
        versionCode = 4;
        zzDz = null;
        zzDA = zza1;
        zzDB = zzg1;
        zzDC = tu1;
        zzDD = null;
        zzDE = null;
        zzDF = flag;
        zzDG = null;
        zzDH = zzn1;
        orientation = i;
        zzDI = 2;
        url = null;
        zzqR = versioninfoparcel;
        zzDJ = null;
        zzDK = null;
        zzDL = null;
    }

    public AdOverlayInfoParcel(zza zza1, zzg zzg1, fp fp1, zzn zzn1, tu tu1, boolean flag, int i, 
            String s, VersionInfoParcel versioninfoparcel, gl gl1)
    {
        versionCode = 4;
        zzDz = null;
        zzDA = zza1;
        zzDB = zzg1;
        zzDC = tu1;
        zzDD = fp1;
        zzDE = null;
        zzDF = flag;
        zzDG = null;
        zzDH = zzn1;
        orientation = i;
        zzDI = 3;
        url = s;
        zzqR = versioninfoparcel;
        zzDJ = gl1;
        zzDK = null;
        zzDL = null;
    }

    public AdOverlayInfoParcel(zza zza1, zzg zzg1, fp fp1, zzn zzn1, tu tu1, boolean flag, int i, 
            String s, String s1, VersionInfoParcel versioninfoparcel, gl gl1)
    {
        versionCode = 4;
        zzDz = null;
        zzDA = zza1;
        zzDB = zzg1;
        zzDC = tu1;
        zzDD = fp1;
        zzDE = s1;
        zzDF = flag;
        zzDG = s;
        zzDH = zzn1;
        orientation = i;
        zzDI = 3;
        url = null;
        zzqR = versioninfoparcel;
        zzDJ = gl1;
        zzDK = null;
        zzDL = null;
    }

    public AdOverlayInfoParcel(AdLauncherIntentInfoParcel adlauncherintentinfoparcel, zza zza1, zzg zzg1, zzn zzn1, VersionInfoParcel versioninfoparcel)
    {
        versionCode = 4;
        zzDz = adlauncherintentinfoparcel;
        zzDA = zza1;
        zzDB = zzg1;
        zzDC = null;
        zzDD = null;
        zzDE = null;
        zzDF = false;
        zzDG = null;
        zzDH = zzn1;
        orientation = -1;
        zzDI = 4;
        url = null;
        zzqR = versioninfoparcel;
        zzDJ = null;
        zzDK = null;
        zzDL = null;
    }

    public static void zza(Intent intent, AdOverlayInfoParcel adoverlayinfoparcel)
    {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", adoverlayinfoparcel);
        intent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", bundle);
    }

    public static AdOverlayInfoParcel zzb(Intent intent)
    {
        try
        {
            intent = intent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
            intent.setClassLoader(com/google/android/gms/ads/internal/overlay/AdOverlayInfoParcel.getClassLoader());
            intent = (AdOverlayInfoParcel)intent.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            return null;
        }
        return intent;
    }

    IBinder a()
    {
        return com.google.android.gms.a.d.a(zzDA).asBinder();
    }

    IBinder b()
    {
        return com.google.android.gms.a.d.a(zzDB).asBinder();
    }

    IBinder c()
    {
        return com.google.android.gms.a.d.a(zzDC).asBinder();
    }

    IBinder d()
    {
        return com.google.android.gms.a.d.a(zzDD).asBinder();
    }

    public int describeContents()
    {
        return 0;
    }

    IBinder e()
    {
        return com.google.android.gms.a.d.a(zzDJ).asBinder();
    }

    IBinder f()
    {
        return com.google.android.gms.a.d.a(zzDH).asBinder();
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        com.google.android.gms.ads.internal.overlay.zzf.a(this, parcel, i);
    }

}
