// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.fc;
import com.google.android.gms.b.fg;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzo, zzv, zzp

public interface zzq
    extends IInterface
{

    public abstract void zza(NativeAdOptionsParcel nativeadoptionsparcel);

    public abstract void zza(ew ew);

    public abstract void zza(ez ez);

    public abstract void zza(String s, fg fg, fc fc);

    public abstract void zzb(zzo zzo);

    public abstract void zzb(zzv zzv);

    public abstract zzp zzbm();
}
