// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.support.v4.b.l;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ea;
import com.google.android.gms.b.et;
import java.util.Arrays;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zzh, zza

public class zzf extends et
    implements zzh.zza
{

    private final zza a;
    private final String b;
    private final l c;
    private final l d;
    private final Object e = new Object();
    private zzh f;

    public zzf(String s, l l1, l l2, zza zza)
    {
        b = s;
        c = l1;
        d = l2;
        a = zza;
    }

    public List getAvailableAssetNames()
    {
        boolean flag = false;
        String as[] = new String[c.size() + d.size()];
        int j = 0;
        int i = 0;
        int k;
        int i1;
        do
        {
            k = ((flag) ? 1 : 0);
            i1 = i;
            if (j >= c.size())
            {
                break;
            }
            as[i] = (String)c.b(j);
            i++;
            j++;
        } while (true);
        while (k < d.size()) 
        {
            as[i1] = (String)d.b(k);
            k++;
            i1++;
        }
        return Arrays.asList(as);
    }

    public String getCustomTemplateId()
    {
        return b;
    }

    public void performClick(String s)
    {
label0:
        {
            synchronized (e)
            {
                if (f != null)
                {
                    break label0;
                }
                com.google.android.gms.ads.internal.util.client.zzb.e("Attempt to call performClick before ad initialized.");
            }
            return;
        }
        f.zza(s, null, null, null);
        obj;
        JVM INSTR monitorexit ;
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void recordImpression()
    {
label0:
        {
            synchronized (e)
            {
                if (f != null)
                {
                    break label0;
                }
                com.google.android.gms.ads.internal.util.client.zzb.e("Attempt to perform recordImpression before ad initialized.");
            }
            return;
        }
        f.recordImpression();
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public String zzS(String s)
    {
        return (String)d.get(s);
    }

    public ea zzT(String s)
    {
        return (ea)c.get(s);
    }

    public void zzb(zzh zzh1)
    {
        synchronized (e)
        {
            f = zzh1;
        }
        return;
        zzh1;
        obj;
        JVM INSTR monitorexit ;
        throw zzh1;
    }

    public String zzdF()
    {
        return "3";
    }

    public zza zzdG()
    {
        return a;
    }
}
