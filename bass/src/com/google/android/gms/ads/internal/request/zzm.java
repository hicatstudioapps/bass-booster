// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.bg;
import com.google.android.gms.b.cp;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.gd;
import com.google.android.gms.b.ge;
import com.google.android.gms.b.go;
import com.google.android.gms.b.ih;
import com.google.android.gms.b.iv;
import com.google.android.gms.b.pd;
import com.google.android.gms.b.pm;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.xx;
import com.google.android.gms.common.c;
import com.google.android.gms.common.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            AdResponseParcel, l, AdRequestInfoParcel, o, 
//            k

public class zzm extends qy
{

    static final long a;
    private static final Object b = new Object();
    private static boolean c = false;
    private static ih d = null;
    private static ge e = null;
    private static go f = null;
    private static gd g = null;
    private final zza.zza h;
    private final AdRequestInfoParcel.zza i;
    private final Object j;
    private final Context k;
    private iv l;

    public zzm(Context context, AdRequestInfoParcel.zza zza1, zza.zza zza2)
    {
        super(true);
        j = new Object();
        h = zza2;
        k = context;
        i = zza1;
        synchronized (b)
        {
            if (!c)
            {
                f = new go();
                e = new ge(context.getApplicationContext(), zza1.zzqR);
                g = new zzc();
                d = new ih(k.getApplicationContext(), i.zzqR, (String)db.b.c(), new zzb(), new zza());
                c = true;
            }
        }
        return;
        context;
        zza2;
        JVM INSTR monitorexit ;
        throw context;
    }

    private AdResponseParcel a(AdRequestInfoParcel adrequestinfoparcel)
    {
        Object obj;
        JSONObject jsonobject;
        obj = UUID.randomUUID().toString();
        jsonobject = a(adrequestinfoparcel, ((String) (obj)));
        if (jsonobject != null) goto _L2; else goto _L1
_L1:
        adrequestinfoparcel = new AdResponseParcel(0);
_L4:
        return adrequestinfoparcel;
_L2:
        long l1 = zzp.zzbB().b();
        Future future = f.a(((String) (obj)));
        com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new l(this, jsonobject, ((String) (obj))));
        long l2 = a;
        long l3 = zzp.zzbB().b();
        try
        {
            obj = (JSONObject)future.get(l2 - (l3 - l1), TimeUnit.MILLISECONDS);
            break MISSING_BLOCK_LABEL_102;
        }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel) { }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel) { }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel)
        {
            return new AdResponseParcel(2);
        }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel)
        {
            return new AdResponseParcel(0);
        }
        return new AdResponseParcel(-1);
        if (obj == null)
        {
            return new AdResponseParcel(-1);
        }
        obj = pd.a(k, adrequestinfoparcel, ((JSONObject) (obj)).toString());
        adrequestinfoparcel = ((AdRequestInfoParcel) (obj));
        if (((AdResponseParcel) (obj)).errorCode != -3)
        {
            adrequestinfoparcel = ((AdRequestInfoParcel) (obj));
            if (TextUtils.isEmpty(((AdResponseParcel) (obj)).body))
            {
                return new AdResponseParcel(3);
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    static zza.zza a(zzm zzm1)
    {
        return zzm1.h;
    }

    static go a()
    {
        return f;
    }

    static iv a(zzm zzm1, iv iv)
    {
        zzm1.l = iv;
        return iv;
    }

    private JSONObject a(AdRequestInfoParcel adrequestinfoparcel, String s)
    {
        Bundle bundle;
        String s1;
        bundle = adrequestinfoparcel.zzGq.extras.getBundle("sdk_less_server_data");
        s1 = adrequestinfoparcel.zzGq.extras.getString("sdk_less_network_id");
        if (bundle != null) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject;
        return null;
_L2:
        if ((jsonobject = pd.a(k, adrequestinfoparcel, zzp.zzbD().a(k), null, null, new cp((String)db.b.c()), null, null, new ArrayList(), null)) == null) goto _L1; else goto _L3
_L3:
        adrequestinfoparcel = AdvertisingIdClient.getAdvertisingIdInfo(k);
_L4:
        HashMap hashmap = new HashMap();
        hashmap.put("request_id", s);
        hashmap.put("network_id", s1);
        hashmap.put("request_param", jsonobject);
        hashmap.put("data", bundle);
        if (adrequestinfoparcel != null)
        {
            hashmap.put("adid", adrequestinfoparcel.getId());
            int i1;
            if (adrequestinfoparcel.isLimitAdTrackingEnabled())
            {
                i1 = 1;
            } else
            {
                i1 = 0;
            }
            hashmap.put("lat", Integer.valueOf(i1));
        }
        try
        {
            adrequestinfoparcel = zzp.zzbx().a(hashmap);
        }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel)
        {
            return null;
        }
        return adrequestinfoparcel;
        adrequestinfoparcel;
_L5:
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Cannot get advertising id info", adrequestinfoparcel);
        adrequestinfoparcel = null;
          goto _L4
        adrequestinfoparcel;
          goto _L5
        adrequestinfoparcel;
          goto _L5
        adrequestinfoparcel;
          goto _L5
    }

    protected static void a(bg bg1)
    {
        bg1.a("/loadAd", f);
        bg1.a("/fetchHttpRequest", e);
        bg1.a("/invalidRequest", g);
    }

    static ih b()
    {
        return d;
    }

    static iv b(zzm zzm1)
    {
        return zzm1.l;
    }

    protected static void b(bg bg1)
    {
        bg1.b("/loadAd", f);
        bg1.b("/fetchHttpRequest", e);
        bg1.b("/invalidRequest", g);
    }

    public void onStop()
    {
        synchronized (j)
        {
            com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new o(this));
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zzbp()
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaF("SdkLessAdLoaderBackgroundTask started.");
        Object obj = new AdRequestInfoParcel(i, null, -1L);
        AdResponseParcel adresponseparcel = a(((AdRequestInfoParcel) (obj)));
        long l1 = zzp.zzbB().b();
        obj = new qq(((AdRequestInfoParcel) (obj)), adresponseparcel, null, null, adresponseparcel.errorCode, l1, adresponseparcel.zzGR, null);
        com.google.android.gms.ads.internal.util.client.zza.zzLE.post(new k(this, ((qq) (obj))));
    }

    static 
    {
        a = TimeUnit.SECONDS.toMillis(10L);
    }

    private class zzc
        implements gd
    {

        public void zza(tu tu, Map map)
        {
            tu = (String)map.get("request_id");
            map = (String)map.get("errors");
            com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Invalid request: ").append(map).toString());
            zzm.a().b(tu);
        }

        public zzc()
        {
        }
    }


    private class zzb
        implements it
    {

        public void zza(bg bg1)
        {
            zzm.a(bg1);
        }

        public void zzc(Object obj)
        {
            zza((bg)obj);
        }

        public zzb()
        {
        }
    }


    private class zza
        implements it
    {

        public void zza(bg bg1)
        {
            zzm.b(bg1);
        }

        public void zzc(Object obj)
        {
            zza((bg)obj);
        }

        public zza()
        {
        }
    }

}
