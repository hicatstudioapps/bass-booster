// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.fc;
import com.google.android.gms.b.fg;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzq, zzo, zzv, zzp

class d
    implements zzq
{

    private IBinder a;

    d(IBinder ibinder)
    {
        a = ibinder;
    }

    public IBinder asBinder()
    {
        return a;
    }

    public void zza(NativeAdOptionsParcel nativeadoptionsparcel)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        if (nativeadoptionsparcel == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        parcel.writeInt(1);
        nativeadoptionsparcel.writeToParcel(parcel, 0);
_L1:
        a.transact(6, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        parcel.writeInt(0);
          goto _L1
        nativeadoptionsparcel;
        parcel1.recycle();
        parcel.recycle();
        throw nativeadoptionsparcel;
    }

    public void zza(ew ew1)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        if (ew1 == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        ew1 = ew1.asBinder();
_L1:
        parcel.writeStrongBinder(ew1);
        a.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        ew1 = null;
          goto _L1
        ew1;
        parcel1.recycle();
        parcel.recycle();
        throw ew1;
    }

    public void zza(ez ez1)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        if (ez1 == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        ez1 = ez1.asBinder();
_L1:
        parcel.writeStrongBinder(ez1);
        a.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        ez1 = null;
          goto _L1
        ez1;
        parcel1.recycle();
        parcel.recycle();
        throw ez1;
    }

    public void zza(String s, fg fg1, fc fc1)
    {
        Object obj;
        Parcel parcel;
        Parcel parcel1;
        obj = null;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        parcel.writeString(s);
        if (fg1 == null)
        {
            break MISSING_BLOCK_LABEL_95;
        }
        s = fg1.asBinder();
_L1:
        parcel.writeStrongBinder(s);
        s = obj;
        if (fc1 == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        s = fc1.asBinder();
        parcel.writeStrongBinder(s);
        a.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        s = null;
          goto _L1
        s;
        parcel1.recycle();
        parcel.recycle();
        throw s;
    }

    public void zzb(zzo zzo1)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        if (zzo1 == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        zzo1 = zzo1.asBinder();
_L1:
        parcel.writeStrongBinder(zzo1);
        a.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        zzo1 = null;
          goto _L1
        zzo1;
        parcel1.recycle();
        parcel.recycle();
        throw zzo1;
    }

    public void zzb(zzv zzv1)
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        if (zzv1 == null)
        {
            break MISSING_BLOCK_LABEL_58;
        }
        zzv1 = zzv1.asBinder();
_L1:
        parcel.writeStrongBinder(zzv1);
        a.transact(7, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        zzv1 = null;
          goto _L1
        zzv1;
        parcel1.recycle();
        parcel.recycle();
        throw zzv1;
    }

    public zzp zzbm()
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        zzp zzp;
        parcel.writeInterfaceToken("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
        a.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        zzp = com.google.android.gms.ads.internal.client.zzp.zza.zzh(parcel1.readStrongBinder());
        parcel1.recycle();
        parcel.recycle();
        return zzp;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }
}
