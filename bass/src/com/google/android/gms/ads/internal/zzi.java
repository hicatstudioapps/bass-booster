// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.support.v4.b.l;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.rq;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzn, h

public class zzi extends com.google.android.gms.ads.internal.client.zzp.zza
{

    private final Context a;
    private final zzo b;
    private final jx c;
    private final ew d;
    private final ez e;
    private final l f;
    private final l g;
    private final NativeAdOptionsParcel h;
    private final List i = b();
    private final zzv j;
    private final String k;
    private final VersionInfoParcel l;
    private WeakReference m;
    private final Object n = new Object();

    zzi(Context context, String s, jx jx, VersionInfoParcel versioninfoparcel, zzo zzo, ew ew, ez ez, 
            l l1, l l2, NativeAdOptionsParcel nativeadoptionsparcel, zzv zzv)
    {
        a = context;
        k = s;
        c = jx;
        l = versioninfoparcel;
        b = zzo;
        e = ez;
        d = ew;
        f = l1;
        g = l2;
        h = nativeadoptionsparcel;
        j = zzv;
    }

    static Object a(zzi zzi1)
    {
        return zzi1.n;
    }

    static WeakReference a(zzi zzi1, WeakReference weakreference)
    {
        zzi1.m = weakreference;
        return weakreference;
    }

    static ew b(zzi zzi1)
    {
        return zzi1.d;
    }

    private List b()
    {
        ArrayList arraylist = new ArrayList();
        if (e != null)
        {
            arraylist.add("1");
        }
        if (d != null)
        {
            arraylist.add("2");
        }
        if (f.size() > 0)
        {
            arraylist.add("3");
        }
        return arraylist;
    }

    static ez c(zzi zzi1)
    {
        return zzi1.e;
    }

    static l d(zzi zzi1)
    {
        return zzi1.f;
    }

    static zzo e(zzi zzi1)
    {
        return zzi1.b;
    }

    static l f(zzi zzi1)
    {
        return zzi1.g;
    }

    static List g(zzi zzi1)
    {
        return zzi1.b();
    }

    static NativeAdOptionsParcel h(zzi zzi1)
    {
        return zzi1.h;
    }

    static zzv i(zzi zzi1)
    {
        return zzi1.j;
    }

    protected zzn a()
    {
        return new zzn(a, AdSizeParcel.zzt(a), k, c, l);
    }

    protected void a(Runnable runnable)
    {
        rq.a.post(runnable);
    }

    public String getMediationAdapterClassName()
    {
        Object obj1 = n;
        obj1;
        JVM INSTR monitorenter ;
        if (m == null) goto _L2; else goto _L1
_L1:
        Object obj = (zzn)m.get();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        obj = ((zzn) (obj)).getMediationAdapterClassName();
_L3:
        obj1;
        JVM INSTR monitorexit ;
        return ((String) (obj));
_L2:
        obj1;
        JVM INSTR monitorexit ;
        return null;
        obj;
        obj1;
        JVM INSTR monitorexit ;
        throw obj;
        obj = null;
          goto _L3
    }

    public boolean isLoading()
    {
        Object obj = n;
        obj;
        JVM INSTR monitorenter ;
        if (m == null) goto _L2; else goto _L1
_L1:
        zzn zzn1 = (zzn)m.get();
        if (zzn1 == null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        boolean flag = zzn1.isLoading();
_L3:
        obj;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        obj;
        JVM INSTR monitorexit ;
        return false;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        flag = false;
          goto _L3
    }

    public void zzf(AdRequestParcel adrequestparcel)
    {
        a(new h(this, adrequestparcel));
    }
}
