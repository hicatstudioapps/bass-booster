// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.TextureView;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.dj;
import com.google.android.gms.b.do;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.sh;
import com.google.android.gms.b.sj;
import com.google.android.gms.b.sk;
import com.google.android.gms.b.xx;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            zzi

public class zzp
{

    private final Context a;
    private final String b;
    private final VersionInfoParcel c;
    private final do d;
    private final dq e;
    private final sh f = (new sk()).a("min_1", 4.9406564584124654E-324D, 1.0D).a("1_5", 1.0D, 5D).a("5_10", 5D, 10D).a("10_20", 10D, 20D).a("20_30", 20D, 30D).a("30_max", 30D, 1.7976931348623157E+308D).a();
    private final long g[];
    private final String h[];
    private do i;
    private do j;
    private do k;
    private do l;
    private boolean m;
    private zzi n;
    private boolean o;
    private boolean p;
    private long q;

    public zzp(Context context, VersionInfoParcel versioninfoparcel, String s, dq dq, do do1)
    {
        q = -1L;
        a = context;
        c = versioninfoparcel;
        b = s;
        e = dq;
        d = do1;
        context = (String)db.v.c();
        if (context == null)
        {
            h = new String[0];
            g = new long[0];
        } else
        {
            context = TextUtils.split(context, ",");
            h = new String[context.length];
            g = new long[context.length];
            int i1 = 0;
            while (i1 < context.length) 
            {
                try
                {
                    g[i1] = Long.parseLong(context[i1]);
                }
                // Misplaced declaration of an exception variable
                catch (VersionInfoParcel versioninfoparcel)
                {
                    com.google.android.gms.ads.internal.util.client.zzb.zzd("Unable to parse frame hash target time number.", versioninfoparcel);
                    g[i1] = -1L;
                }
                i1++;
            }
        }
    }

    private void a()
    {
        if (k != null && l == null)
        {
            dj.a(e, k, new String[] {
                "vff"
            });
            dj.a(e, d, new String[] {
                "vtt"
            });
            l = dj.a(e);
        }
        long l1 = com.google.android.gms.ads.internal.zzp.zzbB().c();
        if (m && p && q != -1L)
        {
            double d1 = (double)TimeUnit.SECONDS.toNanos(1L) / (double)(l1 - q);
            f.a(d1);
        }
        p = m;
        q = l1;
    }

    private void a(zzi zzi1)
    {
        int i1;
        long l1;
        long l2;
        l1 = ((Long)db.w.c()).longValue();
        l2 = zzi1.getCurrentPosition();
        i1 = 0;
_L3:
        if (i1 >= h.length)
        {
            break; /* Loop/switch isn't completed */
        }
          goto _L1
_L5:
        i1++;
        if (true) goto _L3; else goto _L2
_L1:
        if (h[i1] != null || l1 <= Math.abs(l2 - g[i1])) goto _L5; else goto _L4
_L4:
        h[i1] = a(((TextureView) (zzi1)));
_L2:
    }

    String a(TextureView textureview)
    {
        textureview = textureview.getBitmap(8, 8);
        long l3 = 0L;
        long l2 = 63L;
        for (int i1 = 0; i1 < 8;)
        {
            long l4 = l2;
            int j1 = 0;
            l2 = l3;
            l3 = l4;
            while (j1 < 8) 
            {
                int k1 = textureview.getPixel(j1, i1);
                int l1 = Color.blue(k1);
                int i2 = Color.red(k1);
                if (Color.green(k1) + (l1 + i2) > 128)
                {
                    l4 = 1L;
                } else
                {
                    l4 = 0L;
                }
                l2 |= l4 << (int)l3;
                l3--;
                j1++;
            }
            i1++;
            l4 = l2;
            l2 = l3;
            l3 = l4;
        }

        return String.format("%016X", new Object[] {
            Long.valueOf(l3)
        });
    }

    public void onStop()
    {
        if (((Boolean)db.u.c()).booleanValue() && !o)
        {
            Bundle bundle = new Bundle();
            bundle.putString("type", "native-player-metrics");
            bundle.putString("request", b);
            bundle.putString("player", n.zzeO());
            sj sj1;
            for (Iterator iterator = f.a().iterator(); iterator.hasNext(); bundle.putString((new StringBuilder()).append("fps_p_").append(sj1.a).toString(), Double.toString(sj1.d)))
            {
                sj1 = (sj)iterator.next();
                bundle.putString((new StringBuilder()).append("fps_c_").append(sj1.a).toString(), Integer.toString(sj1.e));
            }

            int i1 = 0;
            while (i1 < g.length) 
            {
                String s = h[i1];
                if (s != null)
                {
                    bundle.putString((new StringBuilder()).append("fh_").append(Long.valueOf(g[i1])).toString(), s);
                }
                i1++;
            }
            com.google.android.gms.ads.internal.zzp.zzbx().a(a, c.afmaVersion, "gmob-apps", bundle, true);
            o = true;
        }
    }

    public void zza(zzi zzi1)
    {
        dj.a(e, d, new String[] {
            "vpc"
        });
        i = dj.a(e);
        n = zzi1;
    }

    public void zzb(zzi zzi1)
    {
        a();
        a(zzi1);
    }

    public void zzfB()
    {
        m = true;
        if (j != null && k == null)
        {
            dj.a(e, j, new String[] {
                "vfp"
            });
            k = dj.a(e);
        }
    }

    public void zzfC()
    {
        m = false;
    }

    public void zzfo()
    {
        if (i == null || j != null)
        {
            return;
        } else
        {
            dj.a(e, i, new String[] {
                "vfr"
            });
            j = dj.a(e);
            return;
        }
    }
}
