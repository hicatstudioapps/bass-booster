// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.b.ea;
import com.google.android.gms.b.ep;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zza, zzh

public class zze extends ep
    implements zzh.zza
{

    private String a;
    private List b;
    private String c;
    private ea d;
    private String e;
    private String f;
    private zza g;
    private Bundle h;
    private Object i;
    private zzh j;

    public zze(String s, List list, String s1, ea ea, String s2, String s3, zza zza, 
            Bundle bundle)
    {
        i = new Object();
        a = s;
        b = list;
        c = s1;
        d = ea;
        e = s2;
        f = s3;
        g = zza;
        h = bundle;
    }

    public void destroy()
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = null;
        h = null;
        i = null;
        j = null;
    }

    public String getAdvertiser()
    {
        return f;
    }

    public String getBody()
    {
        return c;
    }

    public String getCallToAction()
    {
        return e;
    }

    public String getCustomTemplateId()
    {
        return "";
    }

    public Bundle getExtras()
    {
        return h;
    }

    public String getHeadline()
    {
        return a;
    }

    public List getImages()
    {
        return b;
    }

    public void zzb(zzh zzh)
    {
        synchronized (i)
        {
            j = zzh;
        }
        return;
        zzh;
        obj;
        JVM INSTR monitorexit ;
        throw zzh;
    }

    public a zzdE()
    {
        return com.google.android.gms.a.d.a(j);
    }

    public String zzdF()
    {
        return "1";
    }

    public zza zzdG()
    {
        return g;
    }

    public ea zzdH()
    {
        return d;
    }
}
