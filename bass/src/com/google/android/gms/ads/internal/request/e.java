// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            f

final class e
    implements f
{

    final Context a;

    e(Context context)
    {
        a = context;
        super();
    }

    public boolean a(VersionInfoParcel versioninfoparcel)
    {
        return versioninfoparcel.zzLH || com.google.android.gms.common.e.h(a) && !((Boolean)db.B.c()).booleanValue();
    }
}
