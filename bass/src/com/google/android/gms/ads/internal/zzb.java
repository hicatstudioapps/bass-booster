// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.b.l;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.overlay.zzg;
import com.google.android.gms.ads.internal.purchase.GInAppPurchaseManagerInfoParcel;
import com.google.android.gms.ads.internal.purchase.zzc;
import com.google.android.gms.ads.internal.purchase.zzd;
import com.google.android.gms.ads.internal.purchase.zzf;
import com.google.android.gms.ads.internal.purchase.zzi;
import com.google.android.gms.ads.internal.purchase.zzj;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.request.CapabilityParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.b.ao;
import com.google.android.gms.b.db;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.gl;
import com.google.android.gms.b.jh;
import com.google.android.gms.b.ji;
import com.google.android.gms.b.jj;
import com.google.android.gms.b.jk;
import com.google.android.gms.b.jq;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.ka;
import com.google.android.gms.b.ml;
import com.google.android.gms.b.mt;
import com.google.android.gms.b.nf;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qr;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.qw;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.rt;
import com.google.android.gms.common.internal.av;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zza, zzq, zzp, zzo, 
//            a, zzd

public abstract class zzb extends com.google.android.gms.ads.internal.zza
    implements zzg, zzj, gl, jj
{

    protected final jx j;
    protected transient boolean k;
    private final Messenger l;

    public zzb(Context context, AdSizeParcel adsizeparcel, String s, jx jx, VersionInfoParcel versioninfoparcel, com.google.android.gms.ads.internal.zzd zzd1)
    {
        this(new zzq(context, adsizeparcel, s, versioninfoparcel), jx, null, zzd1);
    }

    zzb(zzq zzq1, jx jx, zzo zzo1, com.google.android.gms.ads.internal.zzd zzd1)
    {
        super(zzq1, zzo1, zzd1);
        j = jx;
        l = new Messenger(new ml(f.context));
        k = false;
    }

    private com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza a(AdRequestParcel adrequestparcel, Bundle bundle)
    {
        ApplicationInfo applicationinfo = f.context.getApplicationInfo();
        Object obj;
        Object obj1;
        String s;
        DisplayMetrics displaymetrics;
        String s1;
        Object obj2;
        Bundle bundle1;
        ArrayList arraylist;
        long l2;
        long l3;
        try
        {
            obj = f.context.getPackageManager().getPackageInfo(applicationinfo.packageName, 0);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
        }
        displaymetrics = f.context.getResources().getDisplayMetrics();
        s = null;
        obj1 = s;
        if (f.c != null)
        {
            obj1 = s;
            if (f.c.getParent() != null)
            {
                obj1 = new int[2];
                f.c.getLocationOnScreen(((int []) (obj1)));
                int j1 = obj1[0];
                int k1 = obj1[1];
                int l1 = f.c.getWidth();
                int i2 = f.c.getHeight();
                boolean flag = false;
                int i = ((flag) ? 1 : 0);
                if (f.c.isShown())
                {
                    i = ((flag) ? 1 : 0);
                    if (j1 + l1 > 0)
                    {
                        i = ((flag) ? 1 : 0);
                        if (k1 + i2 > 0)
                        {
                            i = ((flag) ? 1 : 0);
                            if (j1 <= displaymetrics.widthPixels)
                            {
                                i = ((flag) ? 1 : 0);
                                if (k1 <= displaymetrics.heightPixels)
                                {
                                    i = 1;
                                }
                            }
                        }
                    }
                }
                obj1 = new Bundle(5);
                ((Bundle) (obj1)).putInt("x", j1);
                ((Bundle) (obj1)).putInt("y", k1);
                ((Bundle) (obj1)).putInt("width", l1);
                ((Bundle) (obj1)).putInt("height", i2);
                ((Bundle) (obj1)).putInt("visible", i);
            }
        }
        s = zzp.zzbA().c();
        f.zzqY = new qr(s, f.zzqP);
        f.zzqY.a(adrequestparcel);
        s1 = zzp.zzbx().a(f.context, f.c, f.zzqV);
        l3 = 0L;
        l2 = l3;
        if (f.g != null)
        {
            try
            {
                l2 = f.g.getValue();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj2)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Cannot get correlation id, default to 0.");
                l2 = l3;
            }
        }
        obj2 = UUID.randomUUID().toString();
        bundle1 = zzp.zzbA().a(f.context, this, s);
        arraylist = new ArrayList();
        for (int i1 = 0; i1 < f.m.size(); i1++)
        {
            arraylist.add(f.m.b(i1));
        }

        boolean flag1;
        boolean flag2;
        if (f.h != null)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        if (f.i != null && zzp.zzbA().l())
        {
            flag2 = true;
        } else
        {
            flag2 = false;
        }
        return new com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza(((Bundle) (obj1)), adrequestparcel, f.zzqV, f.zzqP, applicationinfo, ((PackageInfo) (obj)), s, zzp.zzbA().a(), f.zzqR, bundle1, f.p, arraylist, bundle, zzp.zzbA().g(), l, displaymetrics.widthPixels, displaymetrics.heightPixels, displaymetrics.density, s1, l2, ((String) (obj2)), db.a(), f.a, f.n, new CapabilityParcel(flag1, flag2), f.zzbU());
    }

    protected void a(qp qp1, boolean flag)
    {
        if (qp1 == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Ad state was null when trying to ping impression URLs.");
        } else
        {
            super.b(qp1);
            if (qp1.o != null && qp1.o.d != null)
            {
                zzp.zzbK().a(f.context, f.zzqR.afmaVersion, qp1, f.zzqP, flag, qp1.o.d);
            }
            if (qp1.l != null && qp1.l.g != null)
            {
                zzp.zzbK().a(f.context, f.zzqR.afmaVersion, qp1, f.zzqP, flag, qp1.l.g);
                return;
            }
        }
    }

    protected boolean a(AdRequestParcel adrequestparcel)
    {
        return super.a(adrequestparcel) && !k;
    }

    protected boolean a(AdRequestParcel adrequestparcel, qp qp1, boolean flag)
    {
        if (flag || !f.zzbQ()) goto _L2; else goto _L1
_L1:
        if (qp1.h <= 0L) goto _L4; else goto _L3
_L3:
        e.zza(adrequestparcel, qp1.h);
_L2:
        return e.zzbr();
_L4:
        if (qp1.o != null && qp1.o.g > 0L)
        {
            e.zza(adrequestparcel, qp1.o.g);
        } else
        if (!qp1.k && qp1.d == 2)
        {
            e.zzg(adrequestparcel);
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    boolean a(qp qp1)
    {
        boolean flag = false;
        if (g == null) goto _L2; else goto _L1
_L1:
        AdRequestParcel adrequestparcel;
        adrequestparcel = g;
        g = null;
_L4:
        return a(adrequestparcel, qp1, flag);
_L2:
        AdRequestParcel adrequestparcel1 = qp1.a;
        adrequestparcel = adrequestparcel1;
        if (adrequestparcel1.extras != null)
        {
            flag = adrequestparcel1.extras.getBoolean("_noRefresh", false);
            adrequestparcel = adrequestparcel1;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected boolean e()
    {
        boolean flag = true;
        if (!zzp.zzbx().a(f.context.getPackageManager(), f.context.getPackageName(), "android.permission.INTERNET") || !zzp.zzbx().a(f.context))
        {
            flag = false;
        }
        return flag;
    }

    public String getMediationAdapterClassName()
    {
        if (f.zzqW == null)
        {
            return null;
        } else
        {
            return f.zzqW.n;
        }
    }

    public void onAdClicked()
    {
        if (f.zzqW == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Ad state was null when trying to ping click URLs.");
            return;
        }
        if (f.zzqW.o != null && f.zzqW.o.c != null)
        {
            zzp.zzbK().a(f.context, f.zzqR.afmaVersion, f.zzqW, f.zzqP, false, f.zzqW.o.c);
        }
        if (f.zzqW.l != null && f.zzqW.l.f != null)
        {
            zzp.zzbK().a(f.context, f.zzqR.afmaVersion, f.zzqW, f.zzqP, false, f.zzqW.l.f);
        }
        super.onAdClicked();
    }

    public void pause()
    {
        av.b("pause must be called on the main UI thread.");
        if (f.zzqW != null && f.zzqW.b != null && f.zzbQ())
        {
            zzp.zzbz().a(f.zzqW.b);
        }
        if (f.zzqW != null && f.zzqW.m != null)
        {
            try
            {
                f.zzqW.m.d();
            }
            catch (RemoteException remoteexception)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Could not pause mediation adapter.");
            }
        }
        h.d(f.zzqW);
        e.pause();
    }

    public void resume()
    {
        av.b("resume must be called on the main UI thread.");
        if (f.zzqW != null && f.zzqW.b != null && f.zzbQ())
        {
            zzp.zzbz().b(f.zzqW.b);
        }
        if (f.zzqW != null && f.zzqW.m != null)
        {
            try
            {
                f.zzqW.m.e();
            }
            catch (RemoteException remoteexception)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Could not resume mediation adapter.");
            }
        }
        e.resume();
        h.e(f.zzqW);
    }

    public void showInterstitial()
    {
        throw new IllegalStateException("showInterstitial is not supported for current ad type");
    }

    public void zza(mt mt1)
    {
        av.b("setInAppPurchaseListener must be called on the main UI thread.");
        f.h = mt1;
    }

    public void zza(nf nf1, String s)
    {
        av.b("setPlayStorePurchaseParams must be called on the main UI thread.");
        f.q = new zzk(s);
        f.i = nf1;
        if (!zzp.zzbA().f() && nf1 != null)
        {
            (new zzc(f.context, f.i, f.q)).zzgX();
        }
    }

    public void zza(String s, ArrayList arraylist)
    {
        arraylist = new zzd(s, arraylist, f.context, f.zzqR.afmaVersion);
        if (f.h == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("InAppPurchaseListener is not set. Try to launch default purchase flow.");
            if (!zzl.zzcN().zzT(f.context))
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Google Play Service unavailable, cannot launch default purchase flow.");
                return;
            }
            if (f.i == null)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("PlayStorePurchaseListener is not set.");
                return;
            }
            if (f.q == null)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("PlayStorePurchaseVerifier is not initialized.");
                return;
            }
            if (f.s)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("An in-app purchase request is already in progress, abort");
                return;
            }
            f.s = true;
            try
            {
                if (!f.i.a(s))
                {
                    f.s = false;
                    return;
                }
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Could not start In-App purchase.");
                f.s = false;
                return;
            }
            zzp.zzbH().zza(f.context, f.zzqR.zzLH, new GInAppPurchaseManagerInfoParcel(f.context, f.q, arraylist, this));
            return;
        }
        try
        {
            f.h.a(arraylist);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Could not start In-App purchase.");
        }
    }

    public void zza(String s, boolean flag, int i, Intent intent, zzf zzf)
    {
        try
        {
            if (f.i != null)
            {
                f.i.a(new com.google.android.gms.ads.internal.purchase.zzg(f.context, s, flag, i, intent, zzf));
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Fail to invoke PlayStorePurchaseListener.");
        }
        rq.a.postDelayed(new a(this, intent), 500L);
    }

    public boolean zza(AdRequestParcel adrequestparcel, dq dq1)
    {
        if (!e())
        {
            return false;
        }
        Bundle bundle = a(zzp.zzbA().a(f.context));
        e.cancel();
        f.zzrp = 0;
        adrequestparcel = a(adrequestparcel, bundle);
        dq1.a("seq_num", ((com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza) (adrequestparcel)).zzGt);
        dq1.a("request_id", ((com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza) (adrequestparcel)).zzGF);
        dq1.a("session_id", ((com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza) (adrequestparcel)).zzGu);
        if (((com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza) (adrequestparcel)).zzGr != null)
        {
            dq1.a("app_version", String.valueOf(((com.google.android.gms.ads.internal.request.AdRequestInfoParcel.zza) (adrequestparcel)).zzGr.versionCode));
        }
        f.zzqT = zzp.zzbt().zza(f.context, adrequestparcel, f.b, this);
        return true;
    }

    protected boolean zza(qp qp1, qp qp2)
    {
        int i = 0;
        if (qp1 != null && qp1.p != null)
        {
            qp1.p.a(null);
        }
        if (qp2.p != null)
        {
            qp2.p.a(this);
        }
        int i1;
        if (qp2.o != null)
        {
            i1 = qp2.o.l;
            i = qp2.o.m;
        } else
        {
            i1 = 0;
        }
        f.zzrn.a(i1, i);
        return true;
    }

    public void zzaX()
    {
        h.b(f.zzqW);
        k = false;
        b();
        f.zzqY.c();
    }

    public void zzaY()
    {
        k = true;
        c();
    }

    public void zzaZ()
    {
        onAdClicked();
    }

    public void zzb(qp qp1)
    {
        super.zzb(qp1);
        if (qp1.d == 3 && qp1.o != null && qp1.o.e != null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Pinging no fill URLs.");
            zzp.zzbK().a(f.context, f.zzqR.afmaVersion, qp1, f.zzqP, false, qp1.o.e);
        }
    }

    public void zzba()
    {
        zzaX();
    }

    public void zzbb()
    {
        zzaQ();
    }

    public void zzbc()
    {
        zzaY();
    }

    public void zzbd()
    {
        if (f.zzqW != null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Mediation adapter ").append(f.zzqW.n).append(" refreshed, but mediation adapters should never refresh.").toString());
        }
        a(f.zzqW, true);
        d();
    }
}
