// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.ab;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.ji;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.rk;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.sd;
import com.google.android.gms.b.tj;
import com.google.android.gms.b.tn;
import com.google.android.gms.b.u;
import com.google.android.gms.b.xx;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            AdResponseParcel, AdRequestInfoParcel, d, zzc, 
//            b, c

public class zzb extends qy
    implements zzc.zza
{

    sd a;
    AdResponseParcel b;
    ji c;
    private final zza.zza d;
    private final AdRequestInfoParcel.zza e;
    private final Object f = new Object();
    private final Context g;
    private final ab h;
    private AdRequestInfoParcel i;
    private Runnable j;

    public zzb(Context context, AdRequestInfoParcel.zza zza, ab ab1, zza.zza zza1)
    {
        d = zza1;
        g = context;
        e = zza;
        h = ab1;
    }

    static Object a(zzb zzb1)
    {
        return zzb1.f;
    }

    private void a(int k, String s)
    {
        if (k == 3 || k == -1)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaG(s);
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH(s);
        }
        if (b == null)
        {
            b = new AdResponseParcel(k);
        } else
        {
            b = new AdResponseParcel(k, b.zzAU);
        }
        if (i != null)
        {
            s = i;
        } else
        {
            s = new AdRequestInfoParcel(e, null, -1L);
        }
        s = new qq(s, b, c, null, k, -1L, b.zzGR, null);
        d.zza(s);
    }

    static void a(zzb zzb1, int k, String s)
    {
        zzb1.a(k, s);
    }

    static AdRequestInfoParcel.zza b(zzb zzb1)
    {
        return zzb1.e;
    }

    static Runnable c(zzb zzb1)
    {
        return zzb1.j;
    }

    protected AdSizeParcel a(AdRequestInfoParcel adrequestinfoparcel)
    {
        if (b.zzGQ == null)
        {
            throw new d("The ad response must specify one of the supported ad sizes.", 0);
        }
        Object aobj[] = b.zzGQ.split("x");
        if (aobj.length != 2)
        {
            throw new d((new StringBuilder()).append("Invalid ad size format from the ad response: ").append(b.zzGQ).toString(), 0);
        }
        int j1;
        int k1;
        int l1;
        try
        {
            j1 = Integer.parseInt(aobj[0]);
            k1 = Integer.parseInt(aobj[1]);
        }
        // Misplaced declaration of an exception variable
        catch (AdRequestInfoParcel adrequestinfoparcel)
        {
            throw new d((new StringBuilder()).append("Invalid ad size number from the ad response: ").append(b.zzGQ).toString(), 0);
        }
        aobj = adrequestinfoparcel.zzqV.zztX;
        l1 = aobj.length;
        AdSizeParcel adsizeparcel;
        int l;
        int i1;
        for (int k = 0; k < l1; k++)
        {
            adsizeparcel = aobj[k];
            float f1 = g.getResources().getDisplayMetrics().density;
            if (adsizeparcel.width == -1)
            {
                l = (int)((float)adsizeparcel.widthPixels / f1);
            } else
            {
                l = adsizeparcel.width;
            }
            if (adsizeparcel.height == -2)
            {
                i1 = (int)((float)adsizeparcel.heightPixels / f1);
            } else
            {
                i1 = adsizeparcel.height;
            }
            if (j1 == l && k1 == i1)
            {
                return new AdSizeParcel(adsizeparcel, adrequestinfoparcel.zzqV.zztX);
            }
        }

        throw new d((new StringBuilder()).append("The ad size from the ad response was not one of the requested sizes: ").append(b.zzGQ).toString(), 0);
    }

    sd a(VersionInfoParcel versioninfoparcel, tj tj1)
    {
        return zzc.zza(g, versioninfoparcel, tj1, this);
    }

    protected void a()
    {
        if (b.errorCode != -3)
        {
            if (TextUtils.isEmpty(b.body))
            {
                throw new d("No fill from ad server.", 3);
            }
            zzp.zzbA().a(g, b.zzGy);
            if (b.zzGN)
            {
                try
                {
                    c = new ji(b.body);
                    return;
                }
                catch (JSONException jsonexception)
                {
                    throw new d((new StringBuilder()).append("Could not parse mediation config: ").append(b.body).toString(), 0);
                }
            }
        }
    }

    public void onStop()
    {
        synchronized (f)
        {
            if (a != null)
            {
                a.cancel();
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zzb(AdResponseParcel adresponseparcel)
    {
        long l;
        com.google.android.gms.ads.internal.util.client.zzb.zzaF("Received ad response.");
        b = adresponseparcel;
        l = zzp.zzbB().b();
        synchronized (f)
        {
            a = null;
        }
        try
        {
            if (b.errorCode != -2 && b.errorCode != -3)
            {
                throw new d((new StringBuilder()).append("There was a problem getting an ad response. ErrorCode: ").append(b.errorCode).toString(), b.errorCode);
            }
        }
        // Misplaced declaration of an exception variable
        catch (AdResponseParcel adresponseparcel)
        {
            a(adresponseparcel.a(), adresponseparcel.getMessage());
            rq.a.removeCallbacks(j);
            return;
        }
        break MISSING_BLOCK_LABEL_127;
        exception;
        adresponseparcel;
        JVM INSTR monitorexit ;
        throw exception;
        a();
        if (i.zzqV.zztX == null) goto _L2; else goto _L1
_L1:
        adresponseparcel = a(i);
_L7:
        zzp.zzbA().a(b.zzGX);
        if (TextUtils.isEmpty(b.zzGV)) goto _L4; else goto _L3
_L3:
        Object obj = new JSONObject(b.zzGV);
_L5:
        adresponseparcel = new qq(i, b, c, adresponseparcel, -2, l, b.zzGR, ((JSONObject) (obj)));
        d.zza(adresponseparcel);
        rq.a.removeCallbacks(j);
        return;
        obj;
        com.google.android.gms.ads.internal.util.client.zzb.zzb("Error parsing the JSON for Active View.", ((Throwable) (obj)));
_L4:
        obj = null;
        if (true) goto _L5; else goto _L2
_L2:
        adresponseparcel = null;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void zzbp()
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaF("AdLoaderBackgroundTask started.");
        j = new b(this);
        rq.a.postDelayed(j, ((Long)db.aw.c()).longValue());
        tn tn1 = new tn();
        long l = zzp.zzbB().b();
        rk.a(new c(this, tn1));
        String s = h.a().a(g);
        i = new AdRequestInfoParcel(e, s, l);
        tn1.a(i);
    }
}
