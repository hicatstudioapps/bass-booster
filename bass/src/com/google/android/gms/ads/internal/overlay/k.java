// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.os.Handler;
import com.google.android.gms.ads.internal.InterstitialAdParameterParcel;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.rt;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            zzd, AdOverlayInfoParcel, l, h

class k extends qy
{

    final zzd a;

    private k(zzd zzd1)
    {
        a = zzd1;
        super();
    }

    k(zzd zzd1, h h)
    {
        this(zzd1);
    }

    public void onStop()
    {
    }

    public void zzbp()
    {
        Object obj = zzp.zzbx().b(zzd.a(a), a.b.zzDL.zzqc);
        if (obj != null)
        {
            obj = zzp.zzbz().a(zzd.a(a), ((android.graphics.Bitmap) (obj)), a.b.zzDL.zzqd, a.b.zzDL.zzqe);
            rq.a.post(new l(this, ((android.graphics.drawable.Drawable) (obj))));
        }
    }
}
