// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.ThinAdSizeParcel;
import com.google.android.gms.ads.internal.client.zzf;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzu;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ao;
import com.google.android.gms.b.bx;
import com.google.android.gms.b.ca;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.dg;
import com.google.android.gms.b.do;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.fp;
import com.google.android.gms.b.mt;
import com.google.android.gms.b.nf;
import com.google.android.gms.b.ny;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.qr;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.qv;
import com.google.android.gms.b.qw;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.rt;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import com.google.android.gms.common.e;
import com.google.android.gms.common.internal.av;
import java.util.HashSet;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzp, zzq, zzo, zzd

public abstract class zza extends com.google.android.gms.ads.internal.client.zzs.zza
    implements com.google.android.gms.ads.internal.client.zza, zzn, com.google.android.gms.ads.internal.request.zza, fp, ny, qv
{

    protected dq a;
    protected do b;
    protected do c;
    protected boolean d;
    protected final com.google.android.gms.ads.internal.zzo e;
    protected final zzq f;
    protected transient AdRequestParcel g;
    protected final ao h = zzp.zzbA().j();
    protected final zzd i;

    zza(zzq zzq1, com.google.android.gms.ads.internal.zzo zzo1, zzd zzd1)
    {
        d = false;
        f = zzq1;
        if (zzo1 == null)
        {
            zzo1 = new com.google.android.gms.ads.internal.zzo(this);
        }
        e = zzo1;
        i = zzd1;
        zzp.zzbx().b(f.context);
        zzp.zzbA().a(f.context, f.zzqR);
    }

    private AdRequestParcel b(AdRequestParcel adrequestparcel)
    {
        AdRequestParcel adrequestparcel1 = adrequestparcel;
        if (com.google.android.gms.common.e.h(f.context))
        {
            adrequestparcel1 = adrequestparcel;
            if (adrequestparcel.zzty != null)
            {
                adrequestparcel1 = (new zzf(adrequestparcel)).zza(null).zzcI();
            }
        }
        return adrequestparcel1;
    }

    private boolean e()
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaG("Ad leaving application.");
        if (f.e == null)
        {
            return false;
        }
        try
        {
            f.e.onAdLeftApplication();
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call AdListener.onAdLeftApplication().", remoteexception);
            return false;
        }
        return true;
    }

    long a(String s)
    {
        int j;
        int l;
        l = s.indexOf("ufe");
        int k = s.indexOf(',', l);
        j = k;
        if (k == -1)
        {
            j = s.length();
        }
        long l1 = Long.parseLong(s.substring(l + 4, j));
        return l1;
        s;
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("Invalid index for Url fetch time in CSI latency info.");
_L2:
        return -1L;
        s;
        com.google.android.gms.ads.internal.util.client.zzb.zzaH("Cannot find valid format of Url fetch time in CSI latency info.");
        if (true) goto _L2; else goto _L1
_L1:
    }

    Bundle a(ca ca1)
    {
        if (ca1 != null)
        {
            if (ca1.f())
            {
                ca1.d();
            }
            bx bx1 = ca1.c();
            if (bx1 != null)
            {
                ca1 = bx1.b();
                com.google.android.gms.ads.internal.util.client.zzb.zzaF((new StringBuilder()).append("In AdManger: loadAd, ").append(bx1.toString()).toString());
            } else
            {
                ca1 = null;
            }
            if (ca1 != null)
            {
                Bundle bundle = new Bundle(1);
                bundle.putString("fingerprint", ca1);
                bundle.putInt("v", 1);
                return bundle;
            }
        }
        return null;
    }

    void a()
    {
        a = new dq(((Boolean)db.G.c()).booleanValue(), "load_ad", f.zzqV.zztV);
        b = new do(-1L, null, null);
        c = new do(-1L, null, null);
    }

    protected void a(View view)
    {
        f.c.addView(view, zzp.zzbz().d());
    }

    protected boolean a(int j)
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaH((new StringBuilder()).append("Failed to load ad: ").append(j).toString());
        d = false;
        if (f.e == null)
        {
            return false;
        }
        try
        {
            f.e.onAdFailedToLoad(j);
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call AdListener.onAdFailedToLoad().", remoteexception);
            return false;
        }
        return true;
    }

    protected boolean a(AdRequestParcel adrequestparcel)
    {
        adrequestparcel = f.c.getParent();
        return (adrequestparcel instanceof View) && ((View)adrequestparcel).isShown() && zzp.zzbx().a();
    }

    boolean a(qp qp1)
    {
        return false;
    }

    protected void b(qp qp1)
    {
        if (qp1 == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Ad state was null when trying to ping impression URLs.");
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Pinging Impression URLs.");
            f.zzqY.a();
            if (qp1.e != null)
            {
                zzp.zzbx().a(f.context, f.zzqR.afmaVersion, qp1.e);
                return;
            }
        }
    }

    protected boolean b()
    {
        com.google.android.gms.ads.internal.util.client.zzb.v("Ad closing.");
        if (f.e == null)
        {
            return false;
        }
        try
        {
            f.e.onAdClosed();
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call AdListener.onAdClosed().", remoteexception);
            return false;
        }
        return true;
    }

    protected boolean c()
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaG("Ad opening.");
        if (f.e == null)
        {
            return false;
        }
        try
        {
            f.e.onAdOpened();
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call AdListener.onAdOpened().", remoteexception);
            return false;
        }
        return true;
    }

    protected boolean d()
    {
        com.google.android.gms.ads.internal.util.client.zzb.zzaG("Ad finished loading.");
        d = false;
        if (f.e == null)
        {
            return false;
        }
        try
        {
            f.e.onAdLoaded();
        }
        catch (RemoteException remoteexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call AdListener.onAdLoaded().", remoteexception);
            return false;
        }
        return true;
    }

    public void destroy()
    {
        av.b("destroy must be called on the main UI thread.");
        e.cancel();
        h.c(f.zzqW);
        f.destroy();
    }

    public boolean isLoading()
    {
        return d;
    }

    public boolean isReady()
    {
        av.b("isLoaded must be called on the main UI thread.");
        return f.zzqT == null && f.zzqU == null && f.zzqW != null;
    }

    public void onAdClicked()
    {
        if (f.zzqW == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Ad state was null when trying to ping click URLs.");
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Pinging click URLs.");
            f.zzqY.b();
            if (f.zzqW.c != null)
            {
                zzp.zzbx().a(f.context, f.zzqR.afmaVersion, f.zzqW.c);
            }
            if (f.d != null)
            {
                try
                {
                    f.d.onAdClicked();
                    return;
                }
                catch (RemoteException remoteexception)
                {
                    com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not notify onAdClicked event.", remoteexception);
                }
                return;
            }
        }
    }

    public void onAppEvent(String s, String s1)
    {
        if (f.f == null)
        {
            break MISSING_BLOCK_LABEL_24;
        }
        f.f.onAppEvent(s, s1);
        return;
        s;
        com.google.android.gms.ads.internal.util.client.zzb.zzd("Could not call the AppEventListener.", s);
        return;
    }

    public void pause()
    {
        av.b("pause must be called on the main UI thread.");
    }

    protected void recordImpression()
    {
        b(f.zzqW);
    }

    public void resume()
    {
        av.b("resume must be called on the main UI thread.");
    }

    public void setManualImpressionsEnabled(boolean flag)
    {
        throw new UnsupportedOperationException("Attempt to call setManualImpressionsEnabled for an unsupported ad type.");
    }

    public void stopLoading()
    {
        av.b("stopLoading must be called on the main UI thread.");
        d = false;
        f.zzf(true);
    }

    public void zza(AdSizeParcel adsizeparcel)
    {
        av.b("setAdSize must be called on the main UI thread.");
        f.zzqV = adsizeparcel;
        if (f.zzqW != null && f.zzqW.b != null && f.zzrp == 0)
        {
            f.zzqW.b.a(adsizeparcel);
        }
        if (f.c == null)
        {
            return;
        }
        if (f.c.getChildCount() > 1)
        {
            f.c.removeView(f.c.getNextView());
        }
        f.c.setMinimumWidth(adsizeparcel.widthPixels);
        f.c.setMinimumHeight(adsizeparcel.heightPixels);
        f.c.requestLayout();
    }

    public void zza(com.google.android.gms.ads.internal.client.zzn zzn1)
    {
        av.b("setAdListener must be called on the main UI thread.");
        f.d = zzn1;
    }

    public void zza(zzo zzo1)
    {
        av.b("setAdListener must be called on the main UI thread.");
        f.e = zzo1;
    }

    public void zza(zzu zzu1)
    {
        av.b("setAppEventListener must be called on the main UI thread.");
        f.f = zzu1;
    }

    public void zza(zzv zzv)
    {
        av.b("setCorrelationIdProvider must be called on the main UI thread");
        f.g = zzv;
    }

    public void zza(dw dw)
    {
        throw new IllegalStateException("setOnCustomRenderedAdLoadedListener is not supported for current ad type");
    }

    public void zza(mt mt)
    {
        throw new IllegalStateException("setInAppPurchaseListener is not supported for current ad type");
    }

    public void zza(nf nf, String s)
    {
        throw new IllegalStateException("setPlayStorePurchaseParams is not supported for current ad type");
    }

    public void zza(qq qq1)
    {
        if (qq1.b.zzGR != -1L && !TextUtils.isEmpty(qq1.b.zzHb))
        {
            long l = a(qq1.b.zzHb);
            if (l != -1L)
            {
                do do1 = a.a(l + qq1.b.zzGR);
                a.a(do1, new String[] {
                    "stc"
                });
            }
        }
        a.a(qq1.b.zzHb);
        a.a(b, new String[] {
            "arf"
        });
        c = a.a();
        a.a("gqi", qq1.b.zzHc);
        f.zzqT = null;
        f.zzqX = qq1;
        zza(qq1, a);
    }

    protected abstract void zza(qq qq1, dq dq1);

    public void zza(HashSet hashset)
    {
        f.zza(hashset);
    }

    protected abstract boolean zza(AdRequestParcel adrequestparcel, dq dq1);

    protected abstract boolean zza(qp qp1, qp qp2);

    public a zzaO()
    {
        av.b("getAdFrame must be called on the main UI thread.");
        return com.google.android.gms.a.d.a(f.c);
    }

    public AdSizeParcel zzaP()
    {
        av.b("getAdSize must be called on the main UI thread.");
        if (f.zzqV == null)
        {
            return null;
        } else
        {
            return new ThinAdSizeParcel(f.zzqV);
        }
    }

    public void zzaQ()
    {
        e();
    }

    public void zzaR()
    {
        av.b("recordManualImpression must be called on the main UI thread.");
        if (f.zzqW == null)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaH("Ad state was null when trying to ping manual tracking URLs.");
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaF("Pinging manual tracking URLs.");
            if (f.zzqW.f != null)
            {
                zzp.zzbx().a(f.context, f.zzqR.afmaVersion, f.zzqW.f);
                return;
            }
        }
    }

    public void zzb(qp qp1)
    {
        a.a(c, new String[] {
            "awr"
        });
        f.zzqU = null;
        if (qp1.d != -2 && qp1.d != 3)
        {
            zzp.zzbA().a(f.zzbM());
        }
        if (qp1.d == -1)
        {
            d = false;
        } else
        {
            if (a(qp1))
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaF("Ad refresh scheduled.");
            }
            if (qp1.d != -2)
            {
                a(qp1.d);
                return;
            }
            if (f.zzrn == null)
            {
                f.zzrn = new qw(f.zzqP);
            }
            h.b(f.zzqW);
            if (zza(f.zzqW, qp1))
            {
                f.zzqW = qp1;
                f.zzbV();
                dq dq1 = a;
                if (f.zzqW.a())
                {
                    qp1 = "1";
                } else
                {
                    qp1 = "0";
                }
                dq1.a("is_mraid", qp1);
                dq1 = a;
                if (f.zzqW.k)
                {
                    qp1 = "1";
                } else
                {
                    qp1 = "0";
                }
                dq1.a("is_mediation", qp1);
                if (f.zzqW.b != null && f.zzqW.b.k() != null)
                {
                    dq dq2 = a;
                    if (f.zzqW.b.k().c())
                    {
                        qp1 = "1";
                    } else
                    {
                        qp1 = "0";
                    }
                    dq2.a("is_video", qp1);
                }
                a.a(b, new String[] {
                    "ttc"
                });
                if (zzp.zzbA().e() != null)
                {
                    zzp.zzbA().e().a(a);
                }
                if (f.zzbQ())
                {
                    d();
                    return;
                }
            }
        }
    }

    public boolean zzb(AdRequestParcel adrequestparcel)
    {
        av.b("loadAd must be called on the main UI thread.");
        adrequestparcel = b(adrequestparcel);
        if (f.zzqT != null || f.zzqU != null)
        {
            if (g != null)
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Aborting last ad request since another ad request is already in progress. The current request object will still be cached for future refreshes.");
            } else
            {
                com.google.android.gms.ads.internal.util.client.zzb.zzaH("Loading already in progress, saving this object for future refreshes.");
            }
            g = adrequestparcel;
            return false;
        }
        com.google.android.gms.ads.internal.util.client.zzb.zzaG("Starting ad request.");
        a();
        b = a.a();
        if (!adrequestparcel.zztt)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaG((new StringBuilder()).append("Use AdRequest.Builder.addTestDevice(\"").append(zzl.zzcN().zzS(f.context)).append("\") to get test ads on this device.").toString());
        }
        d = zza(adrequestparcel, a);
        return d;
    }

    public void zzd(AdRequestParcel adrequestparcel)
    {
        if (a(adrequestparcel))
        {
            zzb(adrequestparcel);
            return;
        } else
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzaG("Ad is not visible. Not refreshing ad.");
            e.zzg(adrequestparcel);
            return;
        }
    }
}
