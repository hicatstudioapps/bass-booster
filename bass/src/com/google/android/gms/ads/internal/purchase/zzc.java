// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.purchase;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.nf;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.rq;
import com.google.android.gms.common.stats.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.google.android.gms.ads.internal.purchase:
//            zzb, zzh, zzf, zzi, 
//            a, zzk

public class zzc extends qy
    implements ServiceConnection
{

    private final Object a;
    private boolean b;
    private Context c;
    private nf d;
    private com.google.android.gms.ads.internal.purchase.zzb e;
    private zzh f;
    private List g;
    private zzk h;

    public zzc(Context context, nf nf, zzk zzk)
    {
        this(context, nf, zzk, new com.google.android.gms.ads.internal.purchase.zzb(context), zzh.zzy(context.getApplicationContext()));
    }

    zzc(Context context, nf nf, zzk zzk, com.google.android.gms.ads.internal.purchase.zzb zzb1, zzh zzh1)
    {
        a = new Object();
        b = false;
        g = null;
        c = context;
        d = nf;
        h = zzk;
        e = zzb1;
        f = zzh1;
        g = f.zzg(10L);
    }

    static zzk a(zzc zzc1)
    {
        return zzc1.h;
    }

    private void a(long l)
    {
        do
        {
            if (!b(l))
            {
                zzb.v("Timeout waiting for pending transaction to be processed.");
            }
        } while (!b);
    }

    static Context b(zzc zzc1)
    {
        return zzc1.c;
    }

    private boolean b(long l)
    {
        l = 60000L - (SystemClock.elapsedRealtime() - l);
        if (l <= 0L)
        {
            return false;
        }
        try
        {
            a.wait(l);
        }
        catch (InterruptedException interruptedexception)
        {
            zzb.zzaH("waitWithTimeout_lock interrupted");
        }
        return true;
    }

    static nf c(zzc zzc1)
    {
        return zzc1.d;
    }

    protected void a()
    {
        if (!g.isEmpty()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        HashMap hashmap;
        hashmap = new HashMap();
        zzf zzf1;
        for (Iterator iterator = g.iterator(); iterator.hasNext(); hashmap.put(zzf1.zzEQ, zzf1))
        {
            zzf1 = (zzf)iterator.next();
        }

        obj = null;
_L6:
        obj = e.zzi(c.getPackageName(), ((String) (obj)));
          goto _L3
_L5:
        obj = hashmap.keySet().iterator();
        while (((Iterator) (obj)).hasNext()) 
        {
            String s = (String)((Iterator) (obj)).next();
            f.zza((zzf)hashmap.get(s));
        }
          goto _L1
_L3:
        if (obj == null || zzp.zzbH().zzd(((Bundle) (obj))) != 0) goto _L5; else goto _L4
_L4:
        ArrayList arraylist = ((Bundle) (obj)).getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        ArrayList arraylist1 = ((Bundle) (obj)).getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        ArrayList arraylist2 = ((Bundle) (obj)).getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        obj = ((Bundle) (obj)).getString("INAPP_CONTINUATION_TOKEN");
        for (int i = 0; i < arraylist.size(); i++)
        {
            if (!hashmap.containsKey(arraylist.get(i)))
            {
                continue;
            }
            String s1 = (String)arraylist.get(i);
            String s2 = (String)arraylist1.get(i);
            String s3 = (String)arraylist2.get(i);
            zzf zzf2 = (zzf)hashmap.get(s1);
            String s4 = zzp.zzbH().zzap(s2);
            if (zzf2.zzEP.equals(s4))
            {
                a(zzf2, s2, s3);
                hashmap.remove(s1);
            }
        }

        if (obj == null || hashmap.isEmpty()) goto _L5; else goto _L6
    }

    protected void a(zzf zzf1, String s, String s1)
    {
        Intent intent = new Intent();
        zzp.zzbH();
        intent.putExtra("RESPONSE_CODE", 0);
        zzp.zzbH();
        intent.putExtra("INAPP_PURCHASE_DATA", s);
        zzp.zzbH();
        intent.putExtra("INAPP_DATA_SIGNATURE", s1);
        rq.a.post(new a(this, zzf1, intent));
    }

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        synchronized (a)
        {
            e.zzN(ibinder);
            a();
            b = true;
            a.notify();
        }
        return;
        ibinder;
        componentname;
        JVM INSTR monitorexit ;
        throw ibinder;
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        zzb.zzaG("In-app billing service disconnected.");
        e.destroy();
    }

    public void onStop()
    {
        synchronized (a)
        {
            com.google.android.gms.common.stats.b.a().a(c, this);
            e.destroy();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zzbp()
    {
        synchronized (a)
        {
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            com.google.android.gms.common.stats.b.a().a(c, intent, this, 1);
            a(SystemClock.elapsedRealtime());
            com.google.android.gms.common.stats.b.a().a(c, this);
            e.destroy();
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
