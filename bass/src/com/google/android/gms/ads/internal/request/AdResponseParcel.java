// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            zzh, StringParcel, LargeParcelTeleporter, AdRequestInfoParcel

public final class AdResponseParcel
    implements SafeParcelable
{

    public static final zzh CREATOR = new zzh();
    private AdRequestInfoParcel a;
    public String body;
    public final int errorCode;
    public final int orientation;
    public final int versionCode;
    public final List zzAQ;
    public final List zzAR;
    public final long zzAU;
    public final String zzDE;
    public final long zzGM;
    public final boolean zzGN;
    public final long zzGO;
    public final List zzGP;
    public final String zzGQ;
    public final long zzGR;
    public final String zzGS;
    public final boolean zzGT;
    public final String zzGU;
    public final String zzGV;
    public final boolean zzGW;
    public final boolean zzGX;
    public final boolean zzGY;
    public final int zzGZ;
    public final boolean zzGy;
    public LargeParcelTeleporter zzHa;
    public String zzHb;
    public String zzHc;
    public final boolean zztY;
    public boolean zztZ;

    public AdResponseParcel(int i)
    {
        this(14, null, null, null, i, null, -1L, false, -1L, null, -1L, -1, null, -1L, null, false, null, null, false, false, false, true, false, 0, null, null, null, false);
    }

    public AdResponseParcel(int i, long l)
    {
        this(14, null, null, null, i, null, -1L, false, -1L, null, l, -1, null, -1L, null, false, null, null, false, false, false, true, false, 0, null, null, null, false);
    }

    AdResponseParcel(int i, String s, String s1, List list, int j, List list1, long l, boolean flag, long l1, List list2, long l2, 
            int k, String s2, long l3, String s3, boolean flag1, String s4, 
            String s5, boolean flag2, boolean flag3, boolean flag4, boolean flag5, boolean flag6, int i1, 
            LargeParcelTeleporter largeparcelteleporter, String s6, String s7, boolean flag7)
    {
        versionCode = i;
        zzDE = s;
        body = s1;
        if (list != null)
        {
            s = Collections.unmodifiableList(list);
        } else
        {
            s = null;
        }
        zzAQ = s;
        errorCode = j;
        if (list1 != null)
        {
            s = Collections.unmodifiableList(list1);
        } else
        {
            s = null;
        }
        zzAR = s;
        zzGM = l;
        zzGN = flag;
        zzGO = l1;
        if (list2 != null)
        {
            s = Collections.unmodifiableList(list2);
        } else
        {
            s = null;
        }
        zzGP = s;
        zzAU = l2;
        orientation = k;
        zzGQ = s2;
        zzGR = l3;
        zzGS = s3;
        zzGT = flag1;
        zzGU = s4;
        zzGV = s5;
        zzGW = flag2;
        zztY = flag3;
        zzGy = flag4;
        zzGX = flag5;
        zzGY = flag6;
        zzGZ = i1;
        zzHa = largeparcelteleporter;
        zzHb = s6;
        zzHc = s7;
        if (body == null && zzHa != null)
        {
            s = (StringParcel)zzHa.zza(StringParcel.CREATOR);
            if (s != null && !TextUtils.isEmpty(s.zzgm()))
            {
                body = s.zzgm();
            }
        }
        zztZ = flag7;
    }

    public AdResponseParcel(AdRequestInfoParcel adrequestinfoparcel, String s, String s1, List list, List list1, long l, 
            boolean flag, long l1, List list2, long l2, int i, 
            String s2, long l3, String s3, String s4, boolean flag1, boolean flag2, 
            boolean flag3, boolean flag4, boolean flag5, int j, String s5, boolean flag6)
    {
        this(14, s, s1, list, -2, list1, l, flag, l1, list2, l2, i, s2, l3, s3, false, null, s4, flag1, flag2, flag3, flag4, flag5, j, null, null, s5, flag6);
        a = adrequestinfoparcel;
    }

    public AdResponseParcel(AdRequestInfoParcel adrequestinfoparcel, String s, String s1, List list, List list1, long l, 
            boolean flag, long l1, List list2, long l2, int i, 
            String s2, long l3, String s3, boolean flag1, String s4, String s5, 
            boolean flag2, boolean flag3, boolean flag4, boolean flag5, boolean flag6, int j, String s6, 
            boolean flag7)
    {
        this(14, s, s1, list, -2, list1, l, flag, l1, list2, l2, i, s2, l3, s3, flag1, s4, s5, flag2, flag3, flag4, flag5, flag6, j, null, null, s6, flag7);
        a = adrequestinfoparcel;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        if (a != null && a.versionCode >= 9 && !TextUtils.isEmpty(body))
        {
            zzHa = new LargeParcelTeleporter(new StringParcel(body));
            body = null;
        }
        zzh.a(this, parcel, i);
    }

}
