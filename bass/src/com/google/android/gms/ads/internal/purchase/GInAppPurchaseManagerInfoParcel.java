// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.purchase;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.a.a;
import com.google.android.gms.a.b;
import com.google.android.gms.a.d;
import com.google.android.gms.b.mq;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.ads.internal.purchase:
//            zza, zzk, zzj

public final class GInAppPurchaseManagerInfoParcel
    implements SafeParcelable
{

    public static final zza CREATOR = new zza();
    public final int versionCode;
    public final mq zzEv;
    public final Context zzEw;
    public final zzj zzEx;
    public final zzk zzrm;

    GInAppPurchaseManagerInfoParcel(int i, IBinder ibinder, IBinder ibinder1, IBinder ibinder2, IBinder ibinder3)
    {
        versionCode = i;
        zzrm = (zzk)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder));
        zzEv = (mq)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder1));
        zzEw = (Context)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder2));
        zzEx = (zzj)com.google.android.gms.a.d.a(com.google.android.gms.a.b.a(ibinder3));
    }

    public GInAppPurchaseManagerInfoParcel(Context context, zzk zzk1, mq mq1, zzj zzj1)
    {
        versionCode = 2;
        zzEw = context;
        zzrm = zzk1;
        zzEv = mq1;
        zzEx = zzj1;
    }

    public static void zza(Intent intent, GInAppPurchaseManagerInfoParcel ginapppurchasemanagerinfoparcel)
    {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo", ginapppurchasemanagerinfoparcel);
        intent.putExtra("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo", bundle);
    }

    public static GInAppPurchaseManagerInfoParcel zzc(Intent intent)
    {
        try
        {
            intent = intent.getBundleExtra("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo");
            intent.setClassLoader(com/google/android/gms/ads/internal/purchase/GInAppPurchaseManagerInfoParcel.getClassLoader());
            intent = (GInAppPurchaseManagerInfoParcel)intent.getParcelable("com.google.android.gms.ads.internal.purchase.InAppPurchaseManagerInfo");
        }
        // Misplaced declaration of an exception variable
        catch (Intent intent)
        {
            return null;
        }
        return intent;
    }

    IBinder a()
    {
        return com.google.android.gms.a.d.a(zzEx).asBinder();
    }

    IBinder b()
    {
        return com.google.android.gms.a.d.a(zzrm).asBinder();
    }

    IBinder c()
    {
        return com.google.android.gms.a.d.a(zzEv).asBinder();
    }

    IBinder d()
    {
        return com.google.android.gms.a.d.a(zzEw).asBinder();
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        com.google.android.gms.ads.internal.purchase.zza.a(this, parcel, i);
    }

}
