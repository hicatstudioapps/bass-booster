// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.a.d;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzn;
import com.google.android.gms.b.ab;
import com.google.android.gms.b.kk;
import com.google.android.gms.b.kn;
import com.google.android.gms.b.tu;
import com.google.android.gms.common.internal.av;
import java.util.Map;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zzh, a

public class zzg extends zzh
{

    private kk a;
    private kn b;
    private final zzn c;
    private zzh d;
    private boolean e;
    private Object f;

    private zzg(Context context, zzn zzn1, ab ab)
    {
        super(context, zzn1, null, ab, null, null, null);
        e = false;
        f = new Object();
        c = zzn1;
    }

    public zzg(Context context, zzn zzn1, ab ab, kk kk1)
    {
        this(context, zzn1, ab);
        a = kk1;
    }

    public zzg(Context context, zzn zzn1, ab ab, kn kn1)
    {
        this(context, zzn1, ab);
        b = kn1;
    }

    public void recordImpression()
    {
        av.b("recordImpression must be called on the main UI thread.");
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        a(true);
        if (d == null) goto _L2; else goto _L1
_L1:
        d.recordImpression();
_L5:
        c.recordImpression();
        return;
_L2:
        if (a == null || a.k()) goto _L4; else goto _L3
_L3:
        a.i();
          goto _L5
        Object obj1;
        obj1;
        zzb.zzd("Failed to call recordImpression", ((Throwable) (obj1)));
          goto _L5
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
_L4:
        if (b == null || b.i()) goto _L5; else goto _L6
_L6:
        b.g();
          goto _L5
    }

    public a zza(android.view.View.OnClickListener onclicklistener)
    {
        return null;
    }

    public void zza(View view, Map map, JSONObject jsonobject, JSONObject jsonobject1, JSONObject jsonobject2)
    {
        av.b("performClick must be called on the main UI thread.");
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        if (d == null) goto _L2; else goto _L1
_L1:
        d.zza(view, map, jsonobject, jsonobject1, jsonobject2);
_L3:
        c.onAdClicked();
        return;
_L2:
        if (a != null && !a.k())
        {
            a.a(com.google.android.gms.a.d.a(view));
        }
        if (b != null && !b.i())
        {
            a.a(com.google.android.gms.a.d.a(view));
        }
          goto _L3
        view;
        zzb.zzd("Failed to call performClick", view);
          goto _L3
        view;
        obj;
        JVM INSTR monitorexit ;
        throw view;
    }

    public void zzc(zzh zzh1)
    {
        synchronized (f)
        {
            d = zzh1;
        }
        return;
        zzh1;
        obj;
        JVM INSTR monitorexit ;
        throw zzh1;
    }

    public boolean zzdI()
    {
        boolean flag;
        synchronized (f)
        {
            flag = e;
        }
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public zzh zzdJ()
    {
        zzh zzh1;
        synchronized (f)
        {
            zzh1 = d;
        }
        return zzh1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public tu zzdK()
    {
        return null;
    }

    public void zzh(View view)
    {
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        e = true;
        if (a == null) goto _L2; else goto _L1
_L1:
        a.b(com.google.android.gms.a.d.a(view));
_L4:
        e = false;
        return;
_L2:
        if (b == null) goto _L4; else goto _L3
_L3:
        b.b(com.google.android.gms.a.d.a(view));
          goto _L4
        view;
        zzb.zzd("Failed to call prepareAd", view);
          goto _L4
        view;
        obj;
        JVM INSTR monitorexit ;
        throw view;
    }
}
