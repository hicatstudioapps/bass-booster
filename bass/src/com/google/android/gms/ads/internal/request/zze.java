// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.m;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            zzj

public class zze extends m
{

    final int a;

    public zze(Context context, Looper looper, q q, r r, int i)
    {
        super(context, looper, 8, h.a(context), q, r);
        a = i;
    }

    protected zzj a(IBinder ibinder)
    {
        return zzj.zza.zzX(ibinder);
    }

    protected String a()
    {
        return "com.google.android.gms.ads.service.START";
    }

    protected IInterface b(IBinder ibinder)
    {
        return a(ibinder);
    }

    protected String b()
    {
        return "com.google.android.gms.ads.internal.request.IAdRequestService";
    }

    public zzj zzgj()
    {
        return (zzj)super.zzqs();
    }
}
