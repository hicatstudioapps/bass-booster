// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.a.d;
import com.google.android.gms.a.e;
import com.google.android.gms.a.f;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzm;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzx, zzl, zzw

public class zzad extends e
{

    public zzad()
    {
        super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
    }

    private zzw b(Context context)
    {
        try
        {
            com.google.android.gms.a.a a1 = d.a(context);
            context = zzw.zza.zzo(((zzx)a(context)).zza(a1, 0x7e9e10));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzd("Could not get remote MobileAdsSettingManager.", context);
            return null;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            zzb.zzd("Could not get remote MobileAdsSettingManager.", context);
            return null;
        }
        return context;
    }

    protected zzx a(IBinder ibinder)
    {
        return zzx.zza.zzp(ibinder);
    }

    protected Object b(IBinder ibinder)
    {
        return a(ibinder);
    }

    public zzw zzu(Context context)
    {
        Object obj;
label0:
        {
            if (zzl.zzcN().zzT(context))
            {
                zzw zzw = b(context);
                obj = zzw;
                if (zzw != null)
                {
                    break label0;
                }
            }
            zzb.zzaF("Using MobileAdsSettingManager from the client jar.");
            new VersionInfoParcel(0x7e9e10, 0x7e9e10, true);
            obj = zzm.zzr(context);
        }
        return ((zzw) (obj));
    }
}
