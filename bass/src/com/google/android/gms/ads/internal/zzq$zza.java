// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;
import android.widget.ViewSwitcher;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.se;
import com.google.android.gms.b.ta;
import com.google.android.gms.b.tu;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class b extends ViewSwitcher
{

    private final se a;
    private final ta b;

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if (b != null)
        {
            b.c();
        }
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (b != null)
        {
            b.d();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        a.a(motionevent);
        return false;
    }

    public void removeAllViews()
    {
        Object obj = new ArrayList();
        for (int i = 0; i < getChildCount(); i++)
        {
            android.view.View view = getChildAt(i);
            if (view != null && (view instanceof tu))
            {
                ((List) (obj)).add((tu)view);
            }
        }

        super.removeAllViews();
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); ((tu)((Iterator) (obj)).next()).destroy()) { }
    }

    public void zzbS()
    {
        zzb.v("Disable position monitoring on adFrame.");
        if (b != null)
        {
            b.b();
        }
    }

    public se zzbW()
    {
        return a;
    }

    public r(Context context, android.view.Observer.OnGlobalLayoutListener ongloballayoutlistener, android.view.Observer.OnScrollChangedListener onscrollchangedlistener)
    {
        super(context);
        a = new se(context);
        if (context instanceof Activity)
        {
            b = new ta((Activity)context, ongloballayoutlistener, onscrollchangedlistener);
            b.a();
            return;
        } else
        {
            b = null;
            return;
        }
    }
}
