// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.dr;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.nx;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.tu;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzc, zzq, zzp, zze, 
//            d, e

class c
    implements Runnable
{

    final qq a;
    final dq b;
    final zzc c;

    c(zzc zzc1, qq qq1, dq dq)
    {
        c = zzc1;
        a = qq1;
        b = dq;
        super();
    }

    public void run()
    {
        if (a.b.zzGW && c.f.o != null)
        {
            Object obj = null;
            if (a.b.zzDE != null)
            {
                obj = zzp.zzbx().a(a.b.zzDE);
            }
            obj = new dr(c, ((String) (obj)), a.b.body);
            c.f.zzrp = 1;
            try
            {
                c.f.o.a(((com.google.android.gms.b.dt) (obj)));
                return;
            }
            catch (RemoteException remoteexception)
            {
                zzb.zzd("Could not call the onCustomRenderedAdLoadedListener.", remoteexception);
            }
        }
        zze zze1 = new zze();
        tu tu1 = c.a(a, zze1);
        zze1.zza(new zze.zzb(a, tu1));
        tu1.setOnTouchListener(new d(this, zze1));
        tu1.setOnClickListener(new e(this, zze1));
        c.f.zzrp = 0;
        c.f.zzqU = zzp.zzbw().a(c.f.context, c, a, c.f.b, tu1, c.j, c, b);
    }
}
