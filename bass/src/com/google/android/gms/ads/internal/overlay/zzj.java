// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import com.google.android.gms.b.do;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.yd;

// Referenced classes of package com.google.android.gms.ads.internal.overlay:
//            zzi

public abstract class zzj
{

    public zzj()
    {
    }

    protected boolean a(Context context)
    {
        context = context.getApplicationInfo();
        return yd.c() && (context == null || ((ApplicationInfo) (context)).targetSdkVersion >= 11);
    }

    public abstract zzi zza(Context context, tu tu, int i, dq dq, do do1);
}
