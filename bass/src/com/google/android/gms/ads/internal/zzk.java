// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.View;
import android.view.Window;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zze;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ao;
import com.google.android.gms.b.cs;
import com.google.android.gms.b.db;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.gn;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.ka;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.rt;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import com.google.android.gms.b.ub;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzc, zzp, zzq, zzo, 
//            j, InterstitialAdParameterParcel, i, zzd, 
//            zze

public class zzk extends zzc
    implements gn
{

    protected transient boolean l;
    private boolean m;
    private float n;
    private String o;

    public zzk(Context context, AdSizeParcel adsizeparcel, String s, jx jx, VersionInfoParcel versioninfoparcel, zzd zzd1)
    {
        super(context, adsizeparcel, s, jx, versioninfoparcel, zzd1);
        l = false;
        o = (new StringBuilder()).append("background").append(hashCode()).append(".").append("png").toString();
    }

    private void a(Bundle bundle)
    {
        zzp.zzbx().b(f.context, f.zzqR.afmaVersion, "gmob-apps", bundle, false);
    }

    static boolean a(zzk zzk1)
    {
        return zzk1.m;
    }

    static float b(zzk zzk1)
    {
        return zzk1.n;
    }

    protected tu a(qq qq1, com.google.android.gms.ads.internal.zze zze1)
    {
        tu tu1 = zzp.zzby().a(f.context, f.zzqV, false, false, f.b, f.zzqR, a, i);
        tu1.k().a(this, null, this, this, ((Boolean)db.V.c()).booleanValue(), this, this, zze1, null);
        tu1.b(qq1.a.zzGF);
        return tu1;
    }

    protected boolean a(AdRequestParcel adrequestparcel, qp qp1, boolean flag)
    {
        if (f.zzbQ() && qp1.b != null)
        {
            zzp.zzbz().a(qp1.b);
        }
        return e.zzbr();
    }

    protected boolean b()
    {
        zzbo();
        return super.b();
    }

    protected boolean d()
    {
        if (super.d())
        {
            l = true;
            return true;
        } else
        {
            return false;
        }
    }

    protected boolean f()
    {
        Window window;
        if (f.context instanceof Activity)
        {
            if ((window = ((Activity)f.context).getWindow()) != null && window.getDecorView() != null)
            {
                Rect rect = new Rect();
                Rect rect1 = new Rect();
                window.getDecorView().getGlobalVisibleRect(rect, null);
                window.getDecorView().getWindowVisibleDisplayFrame(rect1);
                boolean flag;
                if (rect.bottom != 0 && rect1.bottom != 0 && rect.top == rect1.top)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                return flag;
            }
        }
        return false;
    }

    public void showInterstitial()
    {
        av.b("showInterstitial must be called on the main UI thread.");
        if (f.zzqW == null)
        {
            zzb.zzaH("The interstitial has not loaded.");
        } else
        {
            if (((Boolean)db.an.c()).booleanValue())
            {
                Object obj;
                if (f.context.getApplicationContext() != null)
                {
                    obj = f.context.getApplicationContext().getPackageName();
                } else
                {
                    obj = f.context.getPackageName();
                }
                if (!l)
                {
                    zzb.zzaH("It is not recommended to show an interstitial before onAdLoaded completes.");
                    Bundle bundle = new Bundle();
                    bundle.putString("appid", ((String) (obj)));
                    bundle.putString("action", "show_interstitial_before_load_finish");
                    a(bundle);
                }
                if (!zzp.zzbx().g(f.context))
                {
                    zzb.zzaH("It is not recommended to show an interstitial when app is not in foreground.");
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("appid", ((String) (obj)));
                    bundle1.putString("action", "show_interstitial_app_not_in_foreground");
                    a(bundle1);
                }
            }
            if (!f.zzbR())
            {
                if (f.zzqW.k)
                {
                    try
                    {
                        f.zzqW.m.b();
                        return;
                    }
                    // Misplaced declaration of an exception variable
                    catch (Object obj)
                    {
                        zzb.zzd("Could not show interstitial.", ((Throwable) (obj)));
                    }
                    zzbo();
                    return;
                }
                if (f.zzqW.b == null)
                {
                    zzb.zzaH("The interstitial failed to load.");
                    return;
                }
                if (f.zzqW.b.o())
                {
                    zzb.zzaH("The interstitial is already showing.");
                    return;
                }
                f.zzqW.b.a(true);
                if (f.zzqW.j != null)
                {
                    h.a(f.zzqV, f.zzqW);
                }
                Object obj1;
                if (f.t)
                {
                    obj1 = zzp.zzbx().h(f.context);
                } else
                {
                    obj1 = null;
                }
                if (((Boolean)db.aD.c()).booleanValue() && obj1 != null)
                {
                    (new j(this, ((android.graphics.Bitmap) (obj1)), o)).zzgX();
                    return;
                }
                obj1 = new InterstitialAdParameterParcel(f.t, f(), null, false, 0.0F);
                int i1 = f.zzqW.b.p();
                int k = i1;
                if (i1 == -1)
                {
                    k = f.zzqW.g;
                }
                obj1 = new AdOverlayInfoParcel(this, this, this, f.zzqW.b, k, f.zzqR, f.zzqW.v, ((InterstitialAdParameterParcel) (obj1)));
                zzp.zzbv().zza(f.context, ((AdOverlayInfoParcel) (obj1)));
                return;
            }
        }
    }

    public void zza(boolean flag, float f1)
    {
        m = flag;
        n = f1;
    }

    public boolean zza(AdRequestParcel adrequestparcel, dq dq)
    {
        if (f.zzqW != null)
        {
            zzb.zzaH("An interstitial is already loading. Aborting.");
            return false;
        } else
        {
            return super.zza(adrequestparcel, dq);
        }
    }

    public boolean zza(qp qp1, qp qp2)
    {
        if (!super.zza(qp1, qp2))
        {
            return false;
        }
        if (!f.zzbQ() && f.r != null && qp2.j != null)
        {
            h.a(f.zzqV, qp2, f.r);
        }
        return true;
    }

    public void zzaY()
    {
        recordImpression();
        super.zzaY();
    }

    public void zzbo()
    {
        (new i(this, o)).zzgX();
        if (f.zzbQ())
        {
            f.zzbN();
            f.zzqW = null;
            f.t = false;
            l = false;
        }
    }

    public void zzd(boolean flag)
    {
        f.t = flag;
    }
}
