// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.dq;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.jx;
import com.google.android.gms.b.lz;
import com.google.android.gms.b.nx;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.rq;
import com.google.android.gms.b.se;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import com.google.android.gms.b.ub;
import com.google.android.gms.common.internal.av;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzb, zzg, zzq, zzp, 
//            b, c, zzd, zze

public abstract class zzc extends com.google.android.gms.ads.internal.zzb
    implements zzg, lz
{

    public zzc(Context context, AdSizeParcel adsizeparcel, String s, jx jx, VersionInfoParcel versioninfoparcel, zzd zzd)
    {
        super(context, adsizeparcel, s, jx, versioninfoparcel, zzd);
    }

    protected tu a(qq qq1, zze zze)
    {
        Object obj = f.c.getNextView();
        if (!(obj instanceof tu)) goto _L2; else goto _L1
_L1:
        zzb.zzaF("Reusing webview...");
        obj = (tu)obj;
        ((tu) (obj)).a(f.context, f.zzqV, a);
_L4:
        ((tu) (obj)).k().a(this, this, this, this, false, this, null, zze, this);
        ((tu) (obj)).b(qq1.a.zzGF);
        return ((tu) (obj));
_L2:
        if (obj != null)
        {
            f.c.removeView(((View) (obj)));
        }
        obj = zzp.zzby().a(f.context, f.zzqV, false, false, f.b, f.zzqR, a, i);
        if (f.zzqV.zztX == null)
        {
            a(((tu) (obj)).b());
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void recordClick()
    {
        onAdClicked();
    }

    public void recordImpression()
    {
        a(f.zzqW, false);
    }

    public void zza(int i, int j, int k, int l)
    {
        c();
    }

    public void zza(dw dw)
    {
        av.b("setOnCustomRenderedAdLoadedListener must be called on the main UI thread.");
        f.o = dw;
    }

    protected void zza(qq qq1, dq dq)
    {
        if (qq1.e != -2)
        {
            rq.a.post(new b(this, qq1));
            return;
        }
        if (qq1.d != null)
        {
            f.zzqV = qq1.d;
        }
        if (qq1.b.zzGN)
        {
            f.zzrp = 0;
            f.zzqU = zzp.zzbw().a(f.context, this, qq1, f.b, null, j, this, dq);
            return;
        } else
        {
            rq.a.post(new c(this, qq1, dq));
            return;
        }
    }

    protected boolean zza(qp qp1, qp qp2)
    {
        if (f.zzbQ() && f.c != null)
        {
            f.c.zzbW().a(qp2.v);
        }
        return super.zza(qp1, qp2);
    }

    public void zzbe()
    {
        b();
    }

    public void zzc(View view)
    {
        f.r = view;
        zzb(new qp(f.zzqX, null, null, null, null, null, null));
    }
}
