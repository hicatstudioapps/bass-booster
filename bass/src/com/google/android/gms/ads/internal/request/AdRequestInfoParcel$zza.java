// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Messenger;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            CapabilityParcel

public final class zzGK
{

    public final ApplicationInfo applicationInfo;
    public final int zzGA;
    public final int zzGB;
    public final float zzGC;
    public final String zzGD;
    public final long zzGE;
    public final String zzGF;
    public final List zzGG;
    public final List zzGH;
    public final CapabilityParcel zzGJ;
    public final String zzGK;
    public final Bundle zzGp;
    public final AdRequestParcel zzGq;
    public final PackageInfo zzGr;
    public final String zzGt;
    public final String zzGu;
    public final Bundle zzGv;
    public final int zzGw;
    public final Bundle zzGx;
    public final boolean zzGy;
    public final Messenger zzGz;
    public final String zzqO;
    public final String zzqP;
    public final VersionInfoParcel zzqR;
    public final AdSizeParcel zzqV;
    public final NativeAdOptionsParcel zzrj;
    public final List zzrl;

    public I(Bundle bundle, AdRequestParcel adrequestparcel, AdSizeParcel adsizeparcel, String s, ApplicationInfo applicationinfo, PackageInfo packageinfo, String s1, 
            String s2, VersionInfoParcel versioninfoparcel, Bundle bundle1, List list, List list1, Bundle bundle2, boolean flag, 
            Messenger messenger, int i, int j, float f, String s3, long l, 
            String s4, List list2, String s5, NativeAdOptionsParcel nativeadoptionsparcel, CapabilityParcel capabilityparcel, String s6)
    {
        zzGp = bundle;
        zzGq = adrequestparcel;
        zzqV = adsizeparcel;
        zzqP = s;
        applicationInfo = applicationinfo;
        zzGr = packageinfo;
        zzGt = s1;
        zzGu = s2;
        zzqR = versioninfoparcel;
        zzGv = bundle1;
        zzGy = flag;
        zzGz = messenger;
        zzGA = i;
        zzGB = j;
        zzGC = f;
        if (list != null && list.size() > 0)
        {
            zzGw = 3;
            zzrl = list;
            zzGH = list1;
        } else
        {
            if (adsizeparcel.zzua)
            {
                zzGw = 4;
            } else
            {
                zzGw = 0;
            }
            zzrl = null;
            zzGH = null;
        }
        zzGx = bundle2;
        zzGD = s3;
        zzGE = l;
        zzGF = s4;
        zzGG = list2;
        zzqO = s5;
        zzrj = nativeadoptionsparcel;
        zzGJ = capabilityparcel;
        zzGK = s6;
    }
}
