// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.support.v4.b.l;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.fc;
import com.google.android.gms.b.fg;
import com.google.android.gms.b.jx;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzi

public class zzj extends com.google.android.gms.ads.internal.client.zzq.zza
{

    private zzo a;
    private ew b;
    private ez c;
    private l d;
    private l e;
    private NativeAdOptionsParcel f;
    private zzv g;
    private final Context h;
    private final jx i;
    private final String j;
    private final VersionInfoParcel k;

    public zzj(Context context, String s, jx jx, VersionInfoParcel versioninfoparcel)
    {
        h = context;
        j = s;
        i = jx;
        k = versioninfoparcel;
        e = new l();
        d = new l();
    }

    public void zza(NativeAdOptionsParcel nativeadoptionsparcel)
    {
        f = nativeadoptionsparcel;
    }

    public void zza(ew ew)
    {
        b = ew;
    }

    public void zza(ez ez)
    {
        c = ez;
    }

    public void zza(String s, fg fg, fc fc)
    {
        if (TextUtils.isEmpty(s))
        {
            throw new IllegalArgumentException("Custom template ID for native custom template ad is empty. Please provide a valid template id.");
        } else
        {
            e.put(s, fg);
            d.put(s, fc);
            return;
        }
    }

    public void zzb(zzo zzo)
    {
        a = zzo;
    }

    public void zzb(zzv zzv)
    {
        g = zzv;
    }

    public zzp zzbm()
    {
        return new zzi(h, j, i, k, a, b, c, e, d, f, g);
    }
}
