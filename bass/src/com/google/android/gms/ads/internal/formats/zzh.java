// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.ads.internal.zzn;
import com.google.android.gms.ads.internal.zzp;
import com.google.android.gms.b.ab;
import com.google.android.gms.b.bg;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import com.google.android.gms.b.ub;
import com.google.android.gms.common.internal.av;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            a, b, d, e, 
//            f, g

public class zzh
{

    private final Object a = new Object();
    private final zzn b;
    private final Context c;
    private final JSONObject d;
    private final bg e;
    private final zza f;
    private final ab g;
    private final VersionInfoParcel h;
    private boolean i;
    private tu j;
    private String k;
    private WeakReference l;

    public zzh(Context context, zzn zzn1, bg bg1, ab ab1, JSONObject jsonobject, zza zza1, VersionInfoParcel versioninfoparcel)
    {
        l = null;
        c = context;
        b = zzn1;
        e = bg1;
        g = ab1;
        d = jsonobject;
        f = zza1;
        h = versioninfoparcel;
    }

    static String a(zzh zzh1)
    {
        return zzh1.k;
    }

    static String a(zzh zzh1, String s)
    {
        zzh1.k = s;
        return s;
    }

    static bg b(zzh zzh1)
    {
        return zzh1.e;
    }

    static tu c(zzh zzh1)
    {
        return zzh1.j;
    }

    tu a()
    {
        return zzp.zzby().a(c, AdSizeParcel.zzt(c), false, false, g, h);
    }

    protected void a(boolean flag)
    {
        i = flag;
    }

    public Context getContext()
    {
        return c;
    }

    public void recordImpression()
    {
        av.b("recordImpression must be called on the main UI thread.");
        a(true);
        try
        {
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("ad", d);
            e.a("google.afma.nativeAds.handleImpressionPing", jsonobject);
        }
        catch (JSONException jsonexception)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzb("Unable to create impression JSON.", jsonexception);
        }
        b.zza(this);
    }

    public a zza(android.view.View.OnClickListener onclicklistener)
    {
        Object obj = f.zzdG();
        if (obj == null)
        {
            return null;
        } else
        {
            obj = new a(c, ((com.google.android.gms.ads.internal.formats.zza) (obj)));
            ((a) (obj)).setLayoutParams(new android.widget.FrameLayout.LayoutParams(-1, -1));
            ((a) (obj)).a().setOnClickListener(onclicklistener);
            ((a) (obj)).a().setContentDescription("Ad attribution icon");
            return ((a) (obj));
        }
    }

    public void zza(View view, Map map, JSONObject jsonobject, JSONObject jsonobject1, JSONObject jsonobject2)
    {
        av.b("performClick must be called on the main UI thread.");
        map = map.entrySet().iterator();
        do
        {
            if (!map.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)map.next();
            if (!view.equals((View)((WeakReference)entry.getValue()).get()))
            {
                continue;
            }
            zza((String)entry.getKey(), jsonobject, jsonobject1, jsonobject2);
            break;
        } while (true);
    }

    public void zza(String s, JSONObject jsonobject, JSONObject jsonobject1, JSONObject jsonobject2)
    {
        av.b("performClick must be called on the main UI thread.");
        JSONObject jsonobject3 = new JSONObject();
        jsonobject3.put("asset", s);
        jsonobject3.put("template", f.zzdF());
        s = new JSONObject();
        s.put("ad", d);
        s.put("click", jsonobject3);
        boolean flag;
        if (b.zzr(f.getCustomTemplateId()) != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        try
        {
            s.put("has_custom_click_handler", flag);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.google.android.gms.ads.internal.util.client.zzb.zzb("Unable to create click JSON.", s);
            return;
        }
        if (jsonobject == null)
        {
            break MISSING_BLOCK_LABEL_111;
        }
        s.put("view_rectangles", jsonobject);
        if (jsonobject1 == null)
        {
            break MISSING_BLOCK_LABEL_123;
        }
        s.put("click_point", jsonobject1);
        if (jsonobject2 == null)
        {
            break MISSING_BLOCK_LABEL_137;
        }
        s.put("native_view_rectangle", jsonobject2);
        e.a("google.afma.nativeAds.handleClickGmsg", s);
        return;
    }

    public void zzb(MotionEvent motionevent)
    {
        g.a(motionevent);
    }

    public tu zzdK()
    {
        j = a();
        j.b().setVisibility(8);
        e.a("/loadHtml", new b(this));
        e.a("/showOverlay", new d(this));
        e.a("/hideOverlay", new e(this));
        j.k().a("/hideOverlay", new f(this));
        j.k().a("/sendMessageToSdk", new g(this));
        return j;
    }

    public View zzdL()
    {
        if (l != null)
        {
            return (View)l.get();
        } else
        {
            return null;
        }
    }

    public void zzh(View view)
    {
    }

    public void zzi(View view)
    {
label0:
        {
            synchronized (a)
            {
                if (!i)
                {
                    break label0;
                }
            }
            return;
        }
        if (view.isShown())
        {
            break MISSING_BLOCK_LABEL_32;
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        view;
        obj;
        JVM INSTR monitorexit ;
        throw view;
        if (view.getGlobalVisibleRect(new Rect(), null))
        {
            break MISSING_BLOCK_LABEL_50;
        }
        obj;
        JVM INSTR monitorexit ;
        return;
        recordImpression();
        obj;
        JVM INSTR monitorexit ;
    }

    public void zzj(View view)
    {
        l = new WeakReference(view);
    }

    private class zza
    {

        public abstract String getCustomTemplateId();

        public abstract void zzb(zzh zzh1);

        public abstract String zzdF();

        public abstract com.google.android.gms.ads.internal.formats.zza zzdG();
    }

}
