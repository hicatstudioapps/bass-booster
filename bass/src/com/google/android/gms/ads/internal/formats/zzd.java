// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.formats;

import android.os.Bundle;
import com.google.android.gms.a.a;
import com.google.android.gms.a.d;
import com.google.android.gms.b.ea;
import com.google.android.gms.b.el;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.internal.formats:
//            zza, zzh

public class zzd extends el
    implements zzh.zza
{

    private String a;
    private List b;
    private String c;
    private ea d;
    private String e;
    private double f;
    private String g;
    private String h;
    private zza i;
    private Bundle j;
    private Object k;
    private zzh l;

    public zzd(String s, List list, String s1, ea ea, String s2, double d1, 
            String s3, String s4, zza zza, Bundle bundle)
    {
        k = new Object();
        a = s;
        b = list;
        c = s1;
        d = ea;
        e = s2;
        f = d1;
        g = s3;
        h = s4;
        i = zza;
        j = bundle;
    }

    public void destroy()
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = 0.0D;
        g = null;
        h = null;
        i = null;
        j = null;
        k = null;
        l = null;
    }

    public String getBody()
    {
        return c;
    }

    public String getCallToAction()
    {
        return e;
    }

    public String getCustomTemplateId()
    {
        return "";
    }

    public Bundle getExtras()
    {
        return j;
    }

    public String getHeadline()
    {
        return a;
    }

    public List getImages()
    {
        return b;
    }

    public String getPrice()
    {
        return h;
    }

    public double getStarRating()
    {
        return f;
    }

    public String getStore()
    {
        return g;
    }

    public void zzb(zzh zzh)
    {
        synchronized (k)
        {
            l = zzh;
        }
        return;
        zzh;
        obj;
        JVM INSTR monitorexit ;
        throw zzh;
    }

    public ea zzdD()
    {
        return d;
    }

    public a zzdE()
    {
        return com.google.android.gms.a.d.a(l);
    }

    public String zzdF()
    {
        return "2";
    }

    public zza zzdG()
    {
        return i;
    }
}
