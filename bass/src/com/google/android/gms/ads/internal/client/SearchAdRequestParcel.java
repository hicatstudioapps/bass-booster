// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.ads.internal.client:
//            zzae

public final class SearchAdRequestParcel
    implements SafeParcelable
{

    public static final zzae CREATOR = new zzae();
    public final int backgroundColor;
    public final int versionCode;
    public final int zzuI;
    public final int zzuJ;
    public final int zzuK;
    public final int zzuL;
    public final int zzuM;
    public final int zzuN;
    public final int zzuO;
    public final String zzuP;
    public final int zzuQ;
    public final String zzuR;
    public final int zzuS;
    public final int zzuT;
    public final String zzuU;

    SearchAdRequestParcel(int i, int j, int k, int l, int i1, int j1, int k1, 
            int l1, int i2, String s, int j2, String s1, int k2, int l2, 
            String s2)
    {
        versionCode = i;
        zzuI = j;
        backgroundColor = k;
        zzuJ = l;
        zzuK = i1;
        zzuL = j1;
        zzuM = k1;
        zzuN = l1;
        zzuO = i2;
        zzuP = s;
        zzuQ = j2;
        zzuR = s1;
        zzuS = k2;
        zzuT = l2;
        zzuU = s2;
    }

    public SearchAdRequestParcel(SearchAdRequest searchadrequest)
    {
        versionCode = 1;
        zzuI = searchadrequest.getAnchorTextColor();
        backgroundColor = searchadrequest.getBackgroundColor();
        zzuJ = searchadrequest.getBackgroundGradientBottom();
        zzuK = searchadrequest.getBackgroundGradientTop();
        zzuL = searchadrequest.getBorderColor();
        zzuM = searchadrequest.getBorderThickness();
        zzuN = searchadrequest.getBorderType();
        zzuO = searchadrequest.getCallButtonColor();
        zzuP = searchadrequest.getCustomChannels();
        zzuQ = searchadrequest.getDescriptionTextColor();
        zzuR = searchadrequest.getFontFace();
        zzuS = searchadrequest.getHeaderTextColor();
        zzuT = searchadrequest.getHeaderTextSize();
        zzuU = searchadrequest.getQuery();
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        zzae.a(this, parcel, i);
    }

}
