// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal.request;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.sd;
import com.google.android.gms.b.tj;

// Referenced classes of package com.google.android.gms.ads.internal.request:
//            f, e

public final class zzc
{

    private static sd a(Context context, VersionInfoParcel versioninfoparcel, tj tj, zza zza1)
    {
        zzb.zzaF("Fetching ad response from remote ad request service.");
        if (!zzl.zzcN().zzT(context))
        {
            zzb.zzaH("Failed to connect to remote ad request service.");
            return null;
        } else
        {
            return new zzd.zzb(context, versioninfoparcel, tj, zza1);
        }
    }

    static sd a(Context context, VersionInfoParcel versioninfoparcel, tj tj, zza zza1, f f1)
    {
        if (f1.a(versioninfoparcel))
        {
            return a(context, tj, zza1);
        } else
        {
            return a(context, versioninfoparcel, tj, zza1);
        }
    }

    private static sd a(Context context, tj tj, zza zza1)
    {
        zzb.zzaF("Fetching ad response from local ad request service.");
        context = new zzd.zza(context, tj, zza1);
        context.zzfO();
        return context;
    }

    public static sd zza(Context context, VersionInfoParcel versioninfoparcel, tj tj, zza zza1)
    {
        return a(context, versioninfoparcel, tj, zza1, new e(context));
    }
}
