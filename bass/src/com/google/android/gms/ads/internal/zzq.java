// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.internal;

import android.content.Context;
import android.graphics.Rect;
import android.os.RemoteException;
import android.support.v4.b.l;
import android.view.View;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzn;
import com.google.android.gms.ads.internal.client.zzo;
import com.google.android.gms.ads.internal.client.zzu;
import com.google.android.gms.ads.internal.client.zzv;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.purchase.zzk;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.util.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.b.ab;
import com.google.android.gms.b.db;
import com.google.android.gms.b.dg;
import com.google.android.gms.b.dw;
import com.google.android.gms.b.ew;
import com.google.android.gms.b.ez;
import com.google.android.gms.b.ka;
import com.google.android.gms.b.mt;
import com.google.android.gms.b.nf;
import com.google.android.gms.b.qp;
import com.google.android.gms.b.qq;
import com.google.android.gms.b.qr;
import com.google.android.gms.b.qt;
import com.google.android.gms.b.qw;
import com.google.android.gms.b.qy;
import com.google.android.gms.b.sd;
import com.google.android.gms.b.sw;
import com.google.android.gms.b.tu;
import com.google.android.gms.b.tv;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

// Referenced classes of package com.google.android.gms.ads.internal:
//            zzp, g

public final class zzq
    implements android.view.ViewTreeObserver.OnGlobalLayoutListener, android.view.ViewTreeObserver.OnScrollChangedListener
{

    private boolean A;
    final String a;
    final ab b;
    zza c;
    public final Context context;
    zzn d;
    zzo e;
    zzu f;
    zzv g;
    mt h;
    nf i;
    ew j;
    ez k;
    l l;
    l m;
    NativeAdOptionsParcel n;
    dw o;
    List p;
    zzk q;
    View r;
    boolean s;
    boolean t;
    private HashSet u;
    private int v;
    private int w;
    private sw x;
    private boolean y;
    private boolean z;
    public String zzqP;
    public final VersionInfoParcel zzqR;
    public qy zzqT;
    public sd zzqU;
    public AdSizeParcel zzqV;
    public qp zzqW;
    public qq zzqX;
    public qr zzqY;
    public qw zzrn;
    public int zzrp;

    public zzq(Context context1, AdSizeParcel adsizeparcel, String s1, VersionInfoParcel versioninfoparcel)
    {
        this(context1, adsizeparcel, s1, versioninfoparcel, null);
    }

    zzq(Context context1, AdSizeParcel adsizeparcel, String s1, VersionInfoParcel versioninfoparcel, ab ab1)
    {
        zzrn = null;
        r = null;
        zzrp = 0;
        s = false;
        t = false;
        u = null;
        v = -1;
        w = -1;
        y = true;
        z = true;
        A = false;
        db.a(context1);
        if (zzp.zzbA().e() != null)
        {
            List list = db.a();
            if (versioninfoparcel.zzLF != 0)
            {
                list.add(Integer.toString(versioninfoparcel.zzLF));
            }
            zzp.zzbA().e().a(list);
        }
        a = UUID.randomUUID().toString();
        if (adsizeparcel.zztW || adsizeparcel.zztY)
        {
            c = null;
        } else
        {
            c = new zza(context1, this, this);
            c.setMinimumWidth(adsizeparcel.widthPixels);
            c.setMinimumHeight(adsizeparcel.heightPixels);
            c.setVisibility(4);
        }
        zzqV = adsizeparcel;
        zzqP = s1;
        context = context1;
        zzqR = versioninfoparcel;
        if (ab1 == null)
        {
            ab1 = new ab(new g(this));
        }
        b = ab1;
        x = new sw(200L);
        m = new l();
    }

    private void a()
    {
        View view = c.getRootView().findViewById(0x1020002);
        if (view != null)
        {
            Rect rect = new Rect();
            Rect rect1 = new Rect();
            c.getGlobalVisibleRect(rect);
            view.getGlobalVisibleRect(rect1);
            if (rect.top != rect1.top)
            {
                y = false;
            }
            if (rect.bottom != rect1.bottom)
            {
                z = false;
                return;
            }
        }
    }

    private void a(boolean flag)
    {
        boolean flag1;
        for (flag1 = true; c == null || zzqW == null || zzqW.b == null || flag && !x.a();)
        {
            return;
        }

        if (zzqW.b.k().b())
        {
            int ai[] = new int[2];
            c.getLocationOnScreen(ai);
            int i1 = zzl.zzcN().zzc(context, ai[0]);
            int j1 = zzl.zzcN().zzc(context, ai[1]);
            if (i1 != v || j1 != w)
            {
                v = i1;
                w = j1;
                tv tv1 = zzqW.b.k();
                i1 = v;
                j1 = w;
                if (!flag)
                {
                    flag = flag1;
                } else
                {
                    flag = false;
                }
                tv1.a(i1, j1, flag);
            }
        }
        a();
    }

    public void destroy()
    {
        zzbS();
        e = null;
        f = null;
        i = null;
        h = null;
        o = null;
        g = null;
        zzf(false);
        if (c != null)
        {
            c.removeAllViews();
        }
        zzbN();
        zzbP();
        zzqW = null;
    }

    public void onGlobalLayout()
    {
        a(false);
    }

    public void onScrollChanged()
    {
        a(true);
        A = true;
    }

    public void zza(HashSet hashset)
    {
        u = hashset;
    }

    public HashSet zzbM()
    {
        return u;
    }

    public void zzbN()
    {
        if (zzqW != null && zzqW.b != null)
        {
            zzqW.b.destroy();
        }
    }

    public void zzbO()
    {
        if (zzqW != null && zzqW.b != null)
        {
            zzqW.b.stopLoading();
        }
    }

    public void zzbP()
    {
        if (zzqW == null || zzqW.m == null)
        {
            break MISSING_BLOCK_LABEL_29;
        }
        zzqW.m.c();
        return;
        RemoteException remoteexception;
        remoteexception;
        zzb.zzaH("Could not destroy mediation adapter.");
        return;
    }

    public boolean zzbQ()
    {
        return zzrp == 0;
    }

    public boolean zzbR()
    {
        return zzrp == 1;
    }

    public void zzbS()
    {
        if (c != null)
        {
            c.zzbS();
        }
    }

    public String zzbU()
    {
        if (y && z)
        {
            return "";
        }
        if (y)
        {
            if (A)
            {
                return "top-scrollable";
            } else
            {
                return "top-locked";
            }
        }
        if (z)
        {
            if (A)
            {
                return "bottom-scrollable";
            } else
            {
                return "bottom-locked";
            }
        } else
        {
            return "";
        }
    }

    public void zzbV()
    {
        zzqY.a(zzqW.t);
        zzqY.b(zzqW.u);
        zzqY.a(zzqV.zztW);
        zzqY.b(zzqW.k);
    }

    public void zzf(boolean flag)
    {
        if (zzrp == 0)
        {
            zzbO();
        }
        if (zzqT != null)
        {
            zzqT.cancel();
        }
        if (zzqU != null)
        {
            zzqU.cancel();
        }
        if (flag)
        {
            zzqW = null;
        }
    }

    private class zza extends ViewSwitcher
    {

        private final se a;
        private final ta b;

        protected void onAttachedToWindow()
        {
            super.onAttachedToWindow();
            if (b != null)
            {
                b.c();
            }
        }

        protected void onDetachedFromWindow()
        {
            super.onDetachedFromWindow();
            if (b != null)
            {
                b.d();
            }
        }

        public boolean onInterceptTouchEvent(MotionEvent motionevent)
        {
            a.a(motionevent);
            return false;
        }

        public void removeAllViews()
        {
            Object obj = new ArrayList();
            for (int i1 = 0; i1 < getChildCount(); i1++)
            {
                View view = getChildAt(i1);
                if (view != null && (view instanceof tu))
                {
                    ((List) (obj)).add((tu)view);
                }
            }

            super.removeAllViews();
            for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); ((tu)((Iterator) (obj)).next()).destroy()) { }
        }

        public void zzbS()
        {
            zzb.v("Disable position monitoring on adFrame.");
            if (b != null)
            {
                b.b();
            }
        }

        public se zzbW()
        {
            return a;
        }

        public zza(Context context1, android.view.ViewTreeObserver.OnGlobalLayoutListener ongloballayoutlistener, android.view.ViewTreeObserver.OnScrollChangedListener onscrollchangedlistener)
        {
            super(context1);
            a = new se(context1);
            if (context1 instanceof Activity)
            {
                b = new ta((Activity)context1, ongloballayoutlistener, onscrollchangedlistener);
                b.a();
                return;
            } else
            {
                b = null;
                return;
            }
        }
    }

}
