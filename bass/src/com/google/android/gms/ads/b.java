// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.ads.internal.client.zzz;
import com.google.android.gms.ads.purchase.InAppPurchaseListener;
import com.google.android.gms.ads.purchase.PlayStorePurchaseListener;

// Referenced classes of package com.google.android.gms.ads:
//            AdRequest, AdSize, AdListener

class b extends ViewGroup
{

    private final zzz a;

    public b(Context context, int i)
    {
        super(context);
        a = new zzz(this, a(i));
    }

    public b(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset);
        a = new zzz(this, attributeset, false, a(i));
    }

    public b(Context context, AttributeSet attributeset, int i, int j)
    {
        super(context, attributeset, i);
        a = new zzz(this, attributeset, false, a(j));
    }

    private static boolean a(int i)
    {
        return i == 2;
    }

    public void destroy()
    {
        a.destroy();
    }

    public AdListener getAdListener()
    {
        return a.getAdListener();
    }

    public AdSize getAdSize()
    {
        return a.getAdSize();
    }

    public String getAdUnitId()
    {
        return a.getAdUnitId();
    }

    public InAppPurchaseListener getInAppPurchaseListener()
    {
        return a.getInAppPurchaseListener();
    }

    public String getMediationAdapterClassName()
    {
        return a.getMediationAdapterClassName();
    }

    public boolean isLoading()
    {
        return a.isLoading();
    }

    public void loadAd(AdRequest adrequest)
    {
        a.zza(adrequest.zzaG());
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        View view = getChildAt(0);
        if (view != null && view.getVisibility() != 8)
        {
            int i1 = view.getMeasuredWidth();
            int j1 = view.getMeasuredHeight();
            i = (k - i - i1) / 2;
            j = (l - j - j1) / 2;
            view.layout(i, j, i1 + i, j1 + j);
        }
    }

    protected void onMeasure(int i, int j)
    {
        int k = 0;
        View view = getChildAt(0);
        int l;
        if (view != null && view.getVisibility() != 8)
        {
            measureChild(view, i, j);
            l = view.getMeasuredWidth();
            k = view.getMeasuredHeight();
        } else
        {
            AdSize adsize = getAdSize();
            if (adsize != null)
            {
                Context context = getContext();
                l = adsize.getWidthInPixels(context);
                k = adsize.getHeightInPixels(context);
            } else
            {
                l = 0;
            }
        }
        l = Math.max(l, getSuggestedMinimumWidth());
        k = Math.max(k, getSuggestedMinimumHeight());
        setMeasuredDimension(View.resolveSize(l, i), View.resolveSize(k, j));
    }

    public void pause()
    {
        a.pause();
    }

    public void resume()
    {
        a.resume();
    }

    public void setAdListener(AdListener adlistener)
    {
        a.setAdListener(adlistener);
        if (adlistener != null && (adlistener instanceof zza))
        {
            a.zza((zza)adlistener);
        } else
        if (adlistener == null)
        {
            a.zza(null);
            return;
        }
    }

    public void setAdSize(AdSize adsize)
    {
        a.setAdSizes(new AdSize[] {
            adsize
        });
    }

    public void setAdUnitId(String s)
    {
        a.setAdUnitId(s);
    }

    public void setInAppPurchaseListener(InAppPurchaseListener inapppurchaselistener)
    {
        a.setInAppPurchaseListener(inapppurchaselistener);
    }

    public void setPlayStorePurchaseParams(PlayStorePurchaseListener playstorepurchaselistener, String s)
    {
        a.setPlayStorePurchaseParams(playstorepurchaselistener, s);
    }
}
