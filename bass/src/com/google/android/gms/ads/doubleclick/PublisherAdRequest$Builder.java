// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.doubleclick;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.common.internal.ar;
import com.google.android.gms.common.internal.av;
import java.util.Date;
import java.util.List;

// Referenced classes of package com.google.android.gms.ads.doubleclick:
//            PublisherAdRequest

public final class 
{

    private final com.google.android.gms.ads.internal.client.sEnabled a = new com.google.android.gms.ads.internal.client.sEnabled();

    static com.google.android.gms.ads.internal.client. a( )
    {
        return .a;
    }

    public a addCategoryExclusion(String s)
    {
        a.(s);
        return this;
    }

    public a addCustomEventExtrasBundle(Class class1, Bundle bundle)
    {
        a.(class1, bundle);
        return this;
    }

    public a addCustomTargeting(String s, String s1)
    {
        a.(s, s1);
        return this;
    }

    public a addCustomTargeting(String s, List list)
    {
        if (list != null)
        {
            a.(s, ar.a(",").a(list));
        }
        return this;
    }

    public a addKeyword(String s)
    {
        a.(s);
        return this;
    }

    public a addNetworkExtras(NetworkExtras networkextras)
    {
        a.(networkextras);
        return this;
    }

    public a addNetworkExtrasBundle(Class class1, Bundle bundle)
    {
        a.(class1, bundle);
        return this;
    }

    public a addTestDevice(String s)
    {
        a.(s);
        return this;
    }

    public PublisherAdRequest build()
    {
        return new PublisherAdRequest(this, null);
    }

    public a setBirthday(Date date)
    {
        a.(date);
        return this;
    }

    public a setContentUrl(String s)
    {
        av.a(s, "Content URL must be non-null.");
        av.a(s, "Content URL must be non-empty.");
        boolean flag;
        if (s.length() <= 512)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.b(flag, "Content URL must not exceed %d in length.  Provided length was %d.", new Object[] {
            Integer.valueOf(512), Integer.valueOf(s.length())
        });
        a.(s);
        return this;
    }

    public a setGender(int i)
    {
        a.(i);
        return this;
    }

    public a setIsDesignedForFamilies(boolean flag)
    {
        a.(flag);
        return this;
    }

    public a setLocation(Location location)
    {
        a.(location);
        return this;
    }

    public a setManualImpressionsEnabled(boolean flag)
    {
        a.sEnabled(flag);
        return this;
    }

    public sionsEnabled setPublisherProvidedId(String s)
    {
        a.sEnabled(s);
        return this;
    }

    public a setRequestAgent(String s)
    {
        a.sEnabled(s);
        return this;
    }

    public a tagForChildDirectedTreatment(boolean flag)
    {
        a.sEnabled(flag);
        return this;
    }

    public ()
    {
    }
}
