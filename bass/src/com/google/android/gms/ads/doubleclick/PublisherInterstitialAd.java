// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.internal.client.zzaa;

// Referenced classes of package com.google.android.gms.ads.doubleclick:
//            PublisherAdRequest, AppEventListener, OnCustomRenderedAdLoadedListener

public final class PublisherInterstitialAd
{

    private final zzaa a;

    public PublisherInterstitialAd(Context context)
    {
        a = new zzaa(context, this);
    }

    public AdListener getAdListener()
    {
        return a.getAdListener();
    }

    public String getAdUnitId()
    {
        return a.getAdUnitId();
    }

    public AppEventListener getAppEventListener()
    {
        return a.getAppEventListener();
    }

    public String getMediationAdapterClassName()
    {
        return a.getMediationAdapterClassName();
    }

    public OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener()
    {
        return a.getOnCustomRenderedAdLoadedListener();
    }

    public boolean isLoaded()
    {
        return a.isLoaded();
    }

    public boolean isLoading()
    {
        return a.isLoading();
    }

    public void loadAd(PublisherAdRequest publisheradrequest)
    {
        a.zza(publisheradrequest.zzaG());
    }

    public void setAdListener(AdListener adlistener)
    {
        a.setAdListener(adlistener);
    }

    public void setAdUnitId(String s)
    {
        a.setAdUnitId(s);
    }

    public void setAppEventListener(AppEventListener appeventlistener)
    {
        a.setAppEventListener(appeventlistener);
    }

    public void setCorrelator(Correlator correlator)
    {
        a.setCorrelator(correlator);
    }

    public void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener oncustomrenderedadloadedlistener)
    {
        a.setOnCustomRenderedAdLoadedListener(oncustomrenderedadloadedlistener);
    }

    public void show()
    {
        a.show();
    }
}
