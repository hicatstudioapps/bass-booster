// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.Correlator;
import com.google.android.gms.ads.internal.client.zzz;

// Referenced classes of package com.google.android.gms.ads.doubleclick:
//            PublisherAdRequest, AppEventListener, OnCustomRenderedAdLoadedListener

public final class PublisherAdView extends ViewGroup
{

    private final zzz a;

    public PublisherAdView(Context context)
    {
        super(context);
        a = new zzz(this);
    }

    public PublisherAdView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        a = new zzz(this, attributeset, true);
    }

    public PublisherAdView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        a = new zzz(this, attributeset, true);
    }

    public void destroy()
    {
        a.destroy();
    }

    public AdListener getAdListener()
    {
        return a.getAdListener();
    }

    public AdSize getAdSize()
    {
        return a.getAdSize();
    }

    public AdSize[] getAdSizes()
    {
        return a.getAdSizes();
    }

    public String getAdUnitId()
    {
        return a.getAdUnitId();
    }

    public AppEventListener getAppEventListener()
    {
        return a.getAppEventListener();
    }

    public String getMediationAdapterClassName()
    {
        return a.getMediationAdapterClassName();
    }

    public OnCustomRenderedAdLoadedListener getOnCustomRenderedAdLoadedListener()
    {
        return a.getOnCustomRenderedAdLoadedListener();
    }

    public boolean isLoading()
    {
        return a.isLoading();
    }

    public void loadAd(PublisherAdRequest publisheradrequest)
    {
        a.zza(publisheradrequest.zzaG());
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        View view = getChildAt(0);
        if (view != null && view.getVisibility() != 8)
        {
            int i1 = view.getMeasuredWidth();
            int j1 = view.getMeasuredHeight();
            i = (k - i - i1) / 2;
            j = (l - j - j1) / 2;
            view.layout(i, j, i1 + i, j1 + j);
        }
    }

    protected void onMeasure(int i, int j)
    {
        int k = 0;
        View view = getChildAt(0);
        int l;
        if (view != null && view.getVisibility() != 8)
        {
            measureChild(view, i, j);
            l = view.getMeasuredWidth();
            k = view.getMeasuredHeight();
        } else
        {
            AdSize adsize = getAdSize();
            if (adsize != null)
            {
                Context context = getContext();
                l = adsize.getWidthInPixels(context);
                k = adsize.getHeightInPixels(context);
            } else
            {
                l = 0;
            }
        }
        l = Math.max(l, getSuggestedMinimumWidth());
        k = Math.max(k, getSuggestedMinimumHeight());
        setMeasuredDimension(View.resolveSize(l, i), View.resolveSize(k, j));
    }

    public void pause()
    {
        a.pause();
    }

    public void recordManualImpression()
    {
        a.recordManualImpression();
    }

    public void resume()
    {
        a.resume();
    }

    public void setAdListener(AdListener adlistener)
    {
        a.setAdListener(adlistener);
    }

    public transient void setAdSizes(AdSize aadsize[])
    {
        if (aadsize == null || aadsize.length < 1)
        {
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        } else
        {
            a.zza(aadsize);
            return;
        }
    }

    public void setAdUnitId(String s)
    {
        a.setAdUnitId(s);
    }

    public void setAppEventListener(AppEventListener appeventlistener)
    {
        a.setAppEventListener(appeventlistener);
    }

    public void setCorrelator(Correlator correlator)
    {
        a.setCorrelator(correlator);
    }

    public void setManualImpressionsEnabled(boolean flag)
    {
        a.setManualImpressionsEnabled(flag);
    }

    public void setOnCustomRenderedAdLoadedListener(OnCustomRenderedAdLoadedListener oncustomrenderedadloadedlistener)
    {
        a.setOnCustomRenderedAdLoadedListener(oncustomrenderedadloadedlistener);
    }
}
