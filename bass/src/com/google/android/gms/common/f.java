// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.m;
import android.support.v4.app.x;
import com.google.android.gms.common.internal.av;

public class f extends m
{

    private Dialog ak;
    private android.content.DialogInterface.OnCancelListener al;

    public f()
    {
        ak = null;
        al = null;
    }

    public static f a(Dialog dialog, android.content.DialogInterface.OnCancelListener oncancellistener)
    {
        f f1 = new f();
        dialog = (Dialog)av.a(dialog, "Cannot display null dialog");
        dialog.setOnCancelListener(null);
        dialog.setOnDismissListener(null);
        f1.ak = dialog;
        if (oncancellistener != null)
        {
            f1.al = oncancellistener;
        }
        return f1;
    }

    public void a(x x, String s)
    {
        super.a(x, s);
    }

    public Dialog c(Bundle bundle)
    {
        if (ak == null)
        {
            b(false);
        }
        return ak;
    }

    public void onCancel(DialogInterface dialoginterface)
    {
        if (al != null)
        {
            al.onCancel(dialoginterface);
        }
    }
}
