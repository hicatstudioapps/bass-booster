// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.q;
import android.support.v4.b.a;
import android.view.View;
import com.google.android.gms.b.vt;
import com.google.android.gms.b.wr;
import com.google.android.gms.b.xj;
import com.google.android.gms.b.yo;
import com.google.android.gms.b.yu;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.av;
import com.google.android.gms.common.internal.c;
import com.google.android.gms.common.internal.h;
import com.google.android.gms.common.internal.i;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

// Referenced classes of package com.google.android.gms.common.api:
//            g, k, p, a, 
//            h, n, r, q

public final class o
{

    private Account a;
    private final Set b = new HashSet();
    private final Set c = new HashSet();
    private int d;
    private View e;
    private String f;
    private String g;
    private final Map h = new a();
    private final Context i;
    private final Map j = new a();
    private q k;
    private int l;
    private r m;
    private Looper n;
    private b o;
    private g p;
    private final ArrayList q = new ArrayList();
    private final ArrayList r = new ArrayList();
    private yu s;

    public o(Context context)
    {
        l = -1;
        o = com.google.android.gms.common.b.a();
        p = yo.c;
        i = context;
        n = context.getMainLooper();
        f = context.getPackageName();
        g = context.getClass().getName();
    }

    static q a(o o1)
    {
        return o1.k;
    }

    private static com.google.android.gms.common.api.h a(g g1, Object obj, Context context, Looper looper, h h1, com.google.android.gms.common.api.q q1, r r1)
    {
        return g1.a(context, looper, h1, obj, q1, r1);
    }

    private static c a(k k1, Object obj, Context context, Looper looper, h h1, com.google.android.gms.common.api.q q1, r r1)
    {
        return new c(context, looper, k1.b(), q1, r1, h1, k1.a(obj));
    }

    private void a(xj xj1, n n1)
    {
        xj1.a(l, n1, m);
    }

    private void a(n n1)
    {
        xj xj1 = xj.a(k);
        if (xj1 == null)
        {
            (new Handler(i.getMainLooper())).post(new p(this, n1));
            return;
        } else
        {
            a(xj1, n1);
            return;
        }
    }

    static void a(o o1, xj xj1, n n1)
    {
        o1.a(xj1, n1);
    }

    private n c()
    {
        h h1 = a();
        Object obj = null;
        Map map = h1.e();
        a a3 = new a();
        a a4 = new a();
        ArrayList arraylist = new ArrayList();
        Iterator iterator = j.keySet().iterator();
        com.google.android.gms.common.api.a a1 = null;
        Object obj1;
        for (; iterator.hasNext(); obj = obj1)
        {
            com.google.android.gms.common.api.a a2 = (com.google.android.gms.common.api.a)iterator.next();
            obj1 = j.get(a2);
            int i1 = 0;
            vt vt1;
            if (map.get(a2) != null)
            {
                if (((i)map.get(a2)).b)
                {
                    i1 = 1;
                } else
                {
                    i1 = 2;
                }
            }
            a3.put(a2, Integer.valueOf(i1));
            vt1 = new vt(a2, i1);
            arraylist.add(vt1);
            if (a2.d())
            {
                k k1 = a2.b();
                if (k1.a() == 1)
                {
                    a1 = a2;
                }
                obj1 = a(k1, obj1, i, n, h1, vt1, vt1);
            } else
            {
                g g1 = a2.a();
                if (g1.a() == 1)
                {
                    a1 = a2;
                }
                obj1 = a(g1, obj1, i, n, h1, vt1, vt1);
            }
            a4.put(a2.c(), obj1);
            if (((com.google.android.gms.common.api.h) (obj1)).zzmJ())
            {
                obj1 = a2;
                if (obj != null)
                {
                    throw new IllegalStateException((new StringBuilder()).append(a2.e()).append(" cannot be used with ").append(((com.google.android.gms.common.api.a) (obj)).e()).toString());
                }
            } else
            {
                obj1 = obj;
            }
        }

        if (obj != null)
        {
            if (a1 != null)
            {
                throw new IllegalStateException((new StringBuilder()).append(((com.google.android.gms.common.api.a) (obj)).e()).append(" cannot be used with ").append(a1.e()).toString());
            }
            int j1;
            boolean flag;
            if (a == null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            av.a(flag, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", new Object[] {
                ((com.google.android.gms.common.api.a) (obj)).e()
            });
            av.a(b.equals(c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", new Object[] {
                ((com.google.android.gms.common.api.a) (obj)).e()
            });
            if (s == null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            av.a(flag, "Must not call requestServerAuthCode in GoogleApiClient.Builder when using %s. Call requestServerAuthCode in GoogleSignInOptions.Builder instead.", new Object[] {
                ((com.google.android.gms.common.api.a) (obj)).e()
            });
        }
        j1 = wr.a(a4.values(), true);
        return new wr(i, new ReentrantLock(), n, h1, o, p, a3, q, r, a4, l, j1, arraylist);
    }

    public o a(com.google.android.gms.common.api.a a1)
    {
        av.a(a1, "Api must not be null");
        j.put(a1, null);
        a1 = a1.a().a(null);
        c.addAll(a1);
        b.addAll(a1);
        return this;
    }

    public h a()
    {
        yu yu1;
        if (j.containsKey(yo.g))
        {
            Account account;
            Set set;
            Map map;
            View view;
            String s1;
            String s2;
            int i1;
            boolean flag;
            if (s == null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            av.a(flag, "SignIn.API can't be used in conjunction with requestServerAuthCode.");
            s = (yu)j.get(yo.g);
        }
        account = a;
        set = b;
        map = h;
        i1 = d;
        view = e;
        s1 = f;
        s2 = g;
        if (s != null)
        {
            yu1 = s;
        } else
        {
            yu1 = yu.a;
        }
        return new h(account, set, map, i1, view, s1, s2, yu1);
    }

    public n b()
    {
        n n1;
        boolean flag;
        if (!j.isEmpty())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.b(flag, "must call addApi() to add at least one API");
        n1 = c();
        synchronized (com.google.android.gms.common.api.n.d())
        {
            com.google.android.gms.common.api.n.d().add(n1);
        }
        if (l >= 0)
        {
            a(n1);
        }
        return n1;
        exception;
        set;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
