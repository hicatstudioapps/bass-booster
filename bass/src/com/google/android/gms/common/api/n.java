// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.b.vq;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

// Referenced classes of package com.google.android.gms.common.api:
//            i, h, r

public abstract class n
{

    private static final Set a = Collections.newSetFromMap(new WeakHashMap());

    public n()
    {
    }

    static Set d()
    {
        return a;
    }

    public Looper a()
    {
        throw new UnsupportedOperationException();
    }

    public vq a(vq vq)
    {
        throw new UnsupportedOperationException();
    }

    public h a(i i)
    {
        throw new UnsupportedOperationException();
    }

    public void a(int i)
    {
        throw new UnsupportedOperationException();
    }

    public abstract void a(r r);

    public abstract void a(String s, FileDescriptor filedescriptor, PrintWriter printwriter, String as[]);

    public vq b(vq vq)
    {
        throw new UnsupportedOperationException();
    }

    public abstract void b();

    public abstract void b(r r);

    public abstract void c();

}
