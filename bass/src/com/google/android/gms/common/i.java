// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

// Referenced classes of package com.google.android.gms.common:
//            ConnectionResult

public class i
    implements android.os.Parcelable.Creator
{

    public i()
    {
    }

    static void a(ConnectionResult connectionresult, Parcel parcel, int j)
    {
        int k = c.a(parcel);
        c.a(parcel, 1, connectionresult.b);
        c.a(parcel, 2, connectionresult.c());
        c.a(parcel, 3, connectionresult.d(), j, false);
        c.a(parcel, 4, connectionresult.e(), false);
        c.a(parcel, k);
    }

    public ConnectionResult a(Parcel parcel)
    {
        String s;
        PendingIntent pendingintent;
        int j;
        int k;
        int i1;
        s = null;
        k = 0;
        i1 = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        pendingintent = null;
        j = 0;
_L7:
        int l;
        if (parcel.dataPosition() >= i1)
        {
            break MISSING_BLOCK_LABEL_189;
        }
        l = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
        com.google.android.gms.common.internal.safeparcel.a.a(l);
        JVM INSTR tableswitch 1 4: default 68
    //                   1 101
    //                   2 120
    //                   3 139
    //                   4 167;
           goto _L1 _L2 _L3 _L4 _L5
_L5:
        break MISSING_BLOCK_LABEL_167;
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, l);
        l = k;
        k = j;
        j = l;
_L8:
        l = k;
        k = j;
        j = l;
        if (true) goto _L7; else goto _L6
_L6:
        l = com.google.android.gms.common.internal.safeparcel.a.d(parcel, l);
        j = k;
        k = l;
          goto _L8
_L3:
        l = com.google.android.gms.common.internal.safeparcel.a.d(parcel, l);
        k = j;
        j = l;
          goto _L8
_L4:
        pendingintent = (PendingIntent)com.google.android.gms.common.internal.safeparcel.a.a(parcel, l, PendingIntent.CREATOR);
        l = j;
        j = k;
        k = l;
          goto _L8
        s = com.google.android.gms.common.internal.safeparcel.a.g(parcel, l);
        l = j;
        j = k;
        k = l;
          goto _L8
        if (parcel.dataPosition() != i1)
        {
            throw new b((new StringBuilder()).append("Overread allowed size end=").append(i1).toString(), parcel);
        } else
        {
            return new ConnectionResult(j, k, pendingintent, s);
        }
    }

    public ConnectionResult[] a(int j)
    {
        return new ConnectionResult[j];
    }

    public Object createFromParcel(Parcel parcel)
    {
        return a(parcel);
    }

    public Object[] newArray(int j)
    {
        return a(j);
    }
}
