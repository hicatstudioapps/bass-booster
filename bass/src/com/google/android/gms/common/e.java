// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common;

import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.UserManager;
import android.support.v4.app.n;
import android.support.v4.app.q;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import com.google.android.gms.b;
import com.google.android.gms.b.xv;
import com.google.android.gms.b.yd;
import com.google.android.gms.common.internal.g;
import com.google.android.gms.common.internal.j;
import com.google.android.gms.common.internal.k;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.google.android.gms.common:
//            lz, gd, k, f, 
//            a, j, b, c, 
//            d

public final class e
{

    public static final int a = b();
    public static boolean b = false;
    public static boolean c = false;
    static final AtomicBoolean d = new AtomicBoolean();
    private static int e = -1;
    private static final Object f = new Object();
    private static String g = null;
    private static Integer h = null;
    private static final AtomicBoolean i = new AtomicBoolean();

    public static int a(Context context)
    {
        Object obj;
        PackageManager packagemanager;
        lz lz1;
        if (g.a)
        {
            return 0;
        }
        packagemanager = context.getPackageManager();
        try
        {
            context.getResources().getString(b.common_google_play_services_unknown_issue);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName()))
        {
            j(context);
        }
        try
        {
            obj = packagemanager.getPackageInfo("com.google.android.gms", 64);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 1;
        }
        lz1 = lz.a();
        if (xv.a(context))
        {
            if (lz1.a(((PackageInfo) (obj)), gd.a) == null)
            {
                Log.w("GooglePlayServicesUtil", "Google Play services signature invalid.");
                return 9;
            }
            break MISSING_BLOCK_LABEL_176;
        }
        try
        {
            context = lz1.a(packagemanager.getPackageInfo("com.android.vending", 8256), gd.a);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.w("GooglePlayServicesUtil", "Google Play Store is neither installed nor updating.");
            return 9;
        }
        if (context != null)
        {
            break MISSING_BLOCK_LABEL_149;
        }
        Log.w("GooglePlayServicesUtil", "Google Play Store signature invalid.");
        return 9;
        if (lz1.a(((PackageInfo) (obj)), new com.google.android.gms.common.k[] {
    context
}) != null)
        {
            break MISSING_BLOCK_LABEL_176;
        }
        Log.w("GooglePlayServicesUtil", "Google Play services signature invalid.");
        return 9;
        int l = xv.a(a);
        if (xv.a(((PackageInfo) (obj)).versionCode) < l)
        {
            Log.w("GooglePlayServicesUtil", (new StringBuilder()).append("Google Play services out of date.  Requires ").append(a).append(" but found ").append(((PackageInfo) (obj)).versionCode).toString());
            return 2;
        }
        obj = ((PackageInfo) (obj)).applicationInfo;
        context = ((Context) (obj));
        if (obj == null)
        {
            try
            {
                context = packagemanager.getApplicationInfo("com.google.android.gms", 0);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Log.wtf("GooglePlayServicesUtil", "Google Play services missing when getting application info.", context);
                return 1;
            }
        }
        return ((ApplicationInfo) (context)).enabled ? 0 : 3;
    }

    public static void a(Activity activity, android.content.DialogInterface.OnCancelListener oncancellistener, String s, Dialog dialog)
    {
        boolean flag;
        try
        {
            flag = activity instanceof q;
        }
        catch (NoClassDefFoundError noclassdeffounderror)
        {
            flag = false;
        }
        if (flag)
        {
            activity = ((q)activity).f();
            com.google.android.gms.common.f.a(dialog, oncancellistener).a(activity, s);
            return;
        }
        if (yd.a())
        {
            activity = activity.getFragmentManager();
            com.google.android.gms.common.a.a(dialog, oncancellistener).show(activity, s);
            return;
        } else
        {
            throw new RuntimeException("This Activity does not support Fragments.");
        }
    }

    public static boolean a()
    {
        if (b)
        {
            return c;
        } else
        {
            return "user".equals(Build.TYPE);
        }
    }

    public static boolean a(int l)
    {
        switch (l)
        {
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        default:
            return false;

        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
        case 9: // '\t'
            return true;
        }
    }

    public static boolean a(int l, Activity activity, n n, int i1, android.content.DialogInterface.OnCancelListener oncancellistener)
    {
        n = b(l, activity, n, i1, oncancellistener);
        if (n == null)
        {
            return false;
        } else
        {
            a(activity, oncancellistener, "GooglePlayServicesErrorDialog", ((Dialog) (n)));
            return true;
        }
    }

    public static boolean a(Context context, int l)
    {
        return a(context, l, "com.google.android.gms") && a(context.getPackageManager(), "com.google.android.gms");
    }

    public static boolean a(Context context, int l, String s)
    {
label0:
        {
            {
                boolean flag1 = false;
                if (!yd.f())
                {
                    break label0;
                }
                context = (AppOpsManager)context.getSystemService("appops");
                boolean flag;
                try
                {
                    context.checkPackage(l, s);
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    return false;
                }
                flag = true;
            }
            return flag;
        }
        context = context.getPackageManager().getPackagesForUid(l);
        flag = flag1;
        if (s == null)
        {
            continue;
        }
        flag = flag1;
        if (context == null)
        {
            continue;
        }
        l = 0;
        do
        {
            flag = flag1;
            if (l >= context.length)
            {
                continue;
            }
            if (s.equals(context[l]))
            {
                return true;
            }
            l++;
        } while (true);
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_28;
_L1:
    }

    static boolean a(Context context, String s)
    {
label0:
        {
            if (!yd.h())
            {
                break label0;
            }
            Iterator iterator = context.getPackageManager().getPackageInstaller().getAllSessions().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
            } while (!s.equals(((android.content.pm.PackageInstaller.SessionInfo)iterator.next()).getAppPackageName()));
            return true;
        }
        if (i(context))
        {
            return false;
        }
        context = context.getPackageManager();
        boolean flag;
        try
        {
            flag = context.getApplicationInfo(s, 8192).enabled;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return flag;
    }

    public static boolean a(PackageManager packagemanager)
    {
        boolean flag = true;
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        int l = e;
        if (l != -1) goto _L2; else goto _L1
_L1:
        packagemanager = packagemanager.getPackageInfo("com.google.android.gms", 64);
        if (lz.a().a(packagemanager, new com.google.android.gms.common.k[] {
            com.google.android.gms.common.j.b[1]
        }) == null) goto _L4; else goto _L3
_L3:
        e = 1;
_L2:
        if (e == 0)
        {
            flag = false;
        }
        obj;
        JVM INSTR monitorexit ;
        return flag;
_L4:
        e = 0;
          goto _L2
        packagemanager;
        e = 0;
          goto _L2
        packagemanager;
        obj;
        JVM INSTR monitorexit ;
        throw packagemanager;
    }

    public static boolean a(PackageManager packagemanager, String s)
    {
        return lz.a().a(packagemanager, s);
    }

    private static int b()
    {
        return 0x7e9e10;
    }

    private static Dialog b(int l, Activity activity, n n, int i1, android.content.DialogInterface.OnCancelListener oncancellistener)
    {
        android.app.AlertDialog.Builder builder1 = null;
        if (l == 0)
        {
            return null;
        }
        int j1 = l;
        if (xv.a(activity))
        {
            j1 = l;
            if (l == 2)
            {
                j1 = 42;
            }
        }
        android.app.AlertDialog.Builder builder = builder1;
        if (yd.c())
        {
            TypedValue typedvalue = new TypedValue();
            activity.getTheme().resolveAttribute(0x1010309, typedvalue, true);
            builder = builder1;
            if ("Theme.Dialog.Alert".equals(activity.getResources().getResourceEntryName(typedvalue.resourceId)))
            {
                builder = new android.app.AlertDialog.Builder(activity, 5);
            }
        }
        builder1 = builder;
        if (builder == null)
        {
            builder1 = new android.app.AlertDialog.Builder(activity);
        }
        builder1.setMessage(com.google.android.gms.common.internal.j.a(activity, j1, f(activity)));
        if (oncancellistener != null)
        {
            builder1.setOnCancelListener(oncancellistener);
        }
        oncancellistener = com.google.android.gms.common.b.a().a(activity, j1, "d");
        if (n == null)
        {
            n = new k(activity, oncancellistener, i1);
        } else
        {
            n = new k(n, oncancellistener, i1);
        }
        oncancellistener = com.google.android.gms.common.internal.j.b(activity, j1);
        if (oncancellistener != null)
        {
            builder1.setPositiveButton(oncancellistener, n);
        }
        activity = com.google.android.gms.common.internal.j.a(activity, j1);
        if (activity != null)
        {
            builder1.setTitle(activity);
        }
        return builder1.create();
    }

    public static void b(Context context)
    {
        int l = com.google.android.gms.common.b.a().a(context);
        if (l != 0)
        {
            context = com.google.android.gms.common.b.a().a(context, l, "e");
            Log.e("GooglePlayServicesUtil", (new StringBuilder()).append("GooglePlayServices not available due to error ").append(l).toString());
            if (context == null)
            {
                throw new c(l);
            } else
            {
                throw new d(l, "Google Play Services not available", context);
            }
        } else
        {
            return;
        }
    }

    public static boolean b(Context context, int l)
    {
        if (l == 18)
        {
            return true;
        }
        if (l == 1)
        {
            return a(context, "com.google.android.gms");
        } else
        {
            return false;
        }
    }

    public static boolean b(PackageManager packagemanager)
    {
        return a(packagemanager) || !a();
    }

    public static void c(Context context)
    {
        if (d.getAndSet(true))
        {
            return;
        }
        try
        {
            ((NotificationManager)context.getSystemService("notification")).cancel(10436);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
    }

    public static Resources d(Context context)
    {
        try
        {
            context = context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static Context e(Context context)
    {
        try
        {
            context = context.createPackageContext("com.google.android.gms", 3);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String f(Context context)
    {
        String s1 = context.getApplicationInfo().name;
        String s = s1;
        if (TextUtils.isEmpty(s1))
        {
            s = context.getPackageName();
            PackageManager packagemanager = context.getApplicationContext().getPackageManager();
            try
            {
                context = packagemanager.getApplicationInfo(context.getPackageName(), 0);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context = null;
            }
            if (context != null)
            {
                s = packagemanager.getApplicationLabel(context).toString();
            }
        }
        return s;
    }

    public static int g(Context context)
    {
        try
        {
            context = context.getPackageManager().getPackageInfo("com.google.android.gms", 0);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
        return ((PackageInfo) (context)).versionCode;
    }

    public static boolean h(Context context)
    {
        context = context.getPackageManager();
        return yd.h() && context.hasSystemFeature("cn.google");
    }

    public static boolean i(Context context)
    {
        if (yd.e())
        {
            context = ((UserManager)context.getSystemService("user")).getApplicationRestrictions(context.getPackageName());
            if (context != null && "true".equals(context.getString("restricted_profile")))
            {
                return true;
            }
        }
        return false;
    }

    private static void j(Context context)
    {
        if (!i.get()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj = f;
        obj;
        JVM INSTR monitorenter ;
        if (g != null) goto _L4; else goto _L3
_L3:
        g = context.getPackageName();
        context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        if (context == null) goto _L6; else goto _L5
_L5:
        h = Integer.valueOf(context.getInt("com.google.android.gms.version"));
_L7:
        context = h;
        obj;
        JVM INSTR monitorexit ;
        if (context == null)
        {
            throw new IllegalStateException("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
        }
        continue; /* Loop/switch isn't completed */
_L6:
        h = null;
          goto _L7
        context;
        Log.wtf("GooglePlayServicesUtil", "This should never happen.", context);
          goto _L7
        context;
        obj;
        JVM INSTR monitorexit ;
        throw context;
_L4:
        if (g.equals(context.getPackageName())) goto _L7; else goto _L8
_L8:
        throw new IllegalArgumentException((new StringBuilder()).append("isGooglePlayServicesAvailable should only be called with Context from your application's package. A previous call used package '").append(g).append("' and this call used package '").append(context.getPackageName()).append("'.").toString());
        if (context.intValue() == a) goto _L1; else goto _L9
_L9:
        throw new IllegalStateException((new StringBuilder()).append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ").append(a).append(" but").append(" found ").append(context).append(".  You must have the").append(" following declaration within the <application> element: ").append("    <meta-data android:name=\"").append("com.google.android.gms.version").append("\" android:value=\"@integer/google_play_services_version\" />").toString());
    }

}
