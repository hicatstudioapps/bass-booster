// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.ProgressBar;
import com.google.android.gms.common.internal.ad;

// Referenced classes of package com.google.android.gms.common:
//            e

public class b
{

    public static final int a;
    private static final b b = new b();

    b()
    {
    }

    public static b a()
    {
        return b;
    }

    private String b(Context context, String s)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("gcore_");
        stringbuilder.append(a);
        stringbuilder.append("-");
        if (!TextUtils.isEmpty(s))
        {
            stringbuilder.append(s);
        }
        stringbuilder.append("-");
        if (context != null)
        {
            stringbuilder.append(context.getPackageName());
        }
        stringbuilder.append("-");
        if (context != null)
        {
            try
            {
                stringbuilder.append(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
        return stringbuilder.toString();
    }

    public int a(Context context)
    {
        int j = e.a(context);
        int i = j;
        if (e.b(context, j))
        {
            i = 18;
        }
        return i;
    }

    public Dialog a(Activity activity, android.content.DialogInterface.OnCancelListener oncancellistener)
    {
        Object obj1 = new ProgressBar(activity, null, 0x101007a);
        ((ProgressBar) (obj1)).setIndeterminate(true);
        ((ProgressBar) (obj1)).setVisibility(0);
        Object obj = new android.app.AlertDialog.Builder(activity);
        ((android.app.AlertDialog.Builder) (obj)).setView(((android.view.View) (obj1)));
        obj1 = e.f(activity);
        ((android.app.AlertDialog.Builder) (obj)).setMessage(activity.getResources().getString(com.google.android.gms.b.common_google_play_services_updating_text, new Object[] {
            obj1
        }));
        ((android.app.AlertDialog.Builder) (obj)).setTitle(com.google.android.gms.b.common_google_play_services_updating_title);
        ((android.app.AlertDialog.Builder) (obj)).setPositiveButton("", null);
        obj = ((android.app.AlertDialog.Builder) (obj)).create();
        e.a(activity, oncancellistener, "GooglePlayServicesUpdatingDialog", ((Dialog) (obj)));
        return ((Dialog) (obj));
    }

    public Intent a(Context context, int i, String s)
    {
        switch (i)
        {
        default:
            return null;

        case 1: // '\001'
        case 2: // '\002'
            return ad.a("com.google.android.gms", b(context, s));

        case 42: // '*'
            return ad.a();

        case 3: // '\003'
            return ad.a("com.google.android.gms");
        }
    }

    public final boolean a(int i)
    {
        return e.a(i);
    }

    public boolean a(Context context, int i)
    {
        return e.b(context, i);
    }

    public boolean a(Context context, String s)
    {
        return e.a(context, s);
    }

    public Intent b(int i)
    {
        return a(null, i, null);
    }

    public void b(Context context)
    {
        e.c(context);
    }

    static 
    {
        a = e.a;
    }
}
