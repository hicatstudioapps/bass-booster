// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.h;
import com.google.android.gms.common.api.q;
import com.google.android.gms.common.api.r;
import com.google.android.gms.common.api.u;
import com.google.android.gms.common.b;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.google.android.gms.common.internal:
//            x, y, av, o, 
//            h, r, v, t, 
//            u, p, ValidateAccountRequest, q, 
//            al, GetServiceRequest, s, ae

public abstract class m
    implements h, x
{

    public static final String zzajS[] = {
        "service_esmobile", "service_googleme"
    };
    private final Context a;
    final Handler b;
    protected AtomicInteger c;
    private final com.google.android.gms.common.internal.h d;
    private final Looper e;
    private final y f;
    private final b g;
    private final Object h;
    private al i;
    private u j;
    private IInterface k;
    private final ArrayList l;
    private com.google.android.gms.common.internal.r m;
    private int n;
    private final Set o;
    private final Account p;
    private final q q;
    private final r r;
    private final int s;

    protected m(Context context, Looper looper, int i1, com.google.android.gms.common.internal.h h1, q q1, r r1)
    {
        this(context, looper, y.a(context), com.google.android.gms.common.b.a(), i1, h1, (q)av.a(q1), (r)av.a(r1));
    }

    protected m(Context context, Looper looper, y y1, b b1, int i1, com.google.android.gms.common.internal.h h1, q q1, 
            r r1)
    {
        h = new Object();
        l = new ArrayList();
        n = 1;
        c = new AtomicInteger(0);
        a = (Context)av.a(context, "Context must not be null");
        e = (Looper)av.a(looper, "Looper must not be null");
        f = (y)av.a(y1, "Supervisor must not be null");
        g = (b)av.a(b1, "API availability must not be null");
        b = new o(this, looper);
        s = i1;
        d = (com.google.android.gms.common.internal.h)av.a(h1);
        p = h1.a();
        o = b(h1.d());
        q = q1;
        r = r1;
    }

    static u a(m m1)
    {
        return m1.j;
    }

    static al a(m m1, al al1)
    {
        m1.i = al1;
        return al1;
    }

    static void a(m m1, int i1, IInterface iinterface)
    {
        m1.b(i1, iinterface);
    }

    private boolean a(int i1, int j1, IInterface iinterface)
    {
label0:
        {
            synchronized (h)
            {
                if (n == i1)
                {
                    break label0;
                }
            }
            return false;
        }
        b(j1, iinterface);
        obj;
        JVM INSTR monitorexit ;
        return true;
        iinterface;
        obj;
        JVM INSTR monitorexit ;
        throw iinterface;
    }

    static boolean a(m m1, int i1, int j1, IInterface iinterface)
    {
        return m1.a(i1, j1, iinterface);
    }

    static q b(m m1)
    {
        return m1.q;
    }

    private Set b(Set set)
    {
        Set set1 = a(set);
        if (set1 == null)
        {
            return set1;
        }
        for (Iterator iterator = set1.iterator(); iterator.hasNext();)
        {
            if (!set.contains((Scope)iterator.next()))
            {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }

        return set1;
    }

    private void b(int i1, IInterface iinterface)
    {
        boolean flag2 = true;
        Object obj;
        boolean flag;
        boolean flag1;
        if (i1 == 3)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (iinterface != null)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        if (flag != flag1)
        {
            flag2 = false;
        }
        com.google.android.gms.common.internal.av.b(flag2);
        obj = h;
        obj;
        JVM INSTR monitorenter ;
        n = i1;
        k = iinterface;
        a(i1, iinterface);
        i1;
        JVM INSTR tableswitch 1 3: default 109
    //                   1 102
    //                   2 83
    //                   3 95;
           goto _L1 _L2 _L3 _L4
_L1:
        obj;
        JVM INSTR monitorexit ;
        return;
_L3:
        h();
          goto _L1
        iinterface;
        obj;
        JVM INSTR monitorexit ;
        throw iinterface;
_L4:
        b_();
          goto _L1
_L2:
        i();
          goto _L1
    }

    static ArrayList c(m m1)
    {
        return m1.l;
    }

    static Set d(m m1)
    {
        return m1.o;
    }

    static r e(m m1)
    {
        return m1.r;
    }

    private void h()
    {
        if (m != null)
        {
            Log.e("GmsClient", (new StringBuilder()).append("Calling connect() while still connected, missing disconnect() for ").append(a()).toString());
            f.b(a(), m, a_());
            c.incrementAndGet();
        }
        m = new com.google.android.gms.common.internal.r(this, c.get());
        if (!f.a(a(), m, a_()))
        {
            Log.e("GmsClient", (new StringBuilder()).append("unable to connect to service: ").append(a()).toString());
            b.sendMessage(b.obtainMessage(3, c.get(), 9));
        }
    }

    private void i()
    {
        if (m != null)
        {
            f.b(a(), m, a_());
            m = null;
        }
    }

    protected abstract String a();

    protected Set a(Set set)
    {
        return set;
    }

    protected void a(int i1)
    {
    }

    protected void a(int i1, Bundle bundle, int j1)
    {
        b.sendMessage(b.obtainMessage(5, j1, -1, new v(this, i1, bundle)));
    }

    protected void a(int i1, IBinder ibinder, Bundle bundle, int j1)
    {
        b.sendMessage(b.obtainMessage(1, j1, -1, new t(this, i1, ibinder, bundle)));
    }

    protected void a(int i1, IInterface iinterface)
    {
    }

    protected void a(ConnectionResult connectionresult)
    {
    }

    protected final String a_()
    {
        return d.g();
    }

    protected abstract IInterface b(IBinder ibinder);

    protected abstract String b();

    protected void b(int i1)
    {
        b.sendMessage(b.obtainMessage(6, i1, -1, new com.google.android.gms.common.internal.u(this)));
    }

    protected void b_()
    {
    }

    public void disconnect()
    {
        c.incrementAndGet();
        ArrayList arraylist = l;
        arraylist;
        JVM INSTR monitorenter ;
        int j1 = l.size();
        int i1 = 0;
_L2:
        if (i1 >= j1)
        {
            break; /* Loop/switch isn't completed */
        }
        ((p)l.get(i1)).e();
        i1++;
        if (true) goto _L2; else goto _L1
_L1:
        l.clear();
        arraylist;
        JVM INSTR monitorexit ;
        b(1, null);
        return;
        Exception exception;
        exception;
        arraylist;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void dump(String s1, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        int i1;
        synchronized (h)
        {
            i1 = n;
            as = k;
        }
        printwriter.append(s1).append("mConnectState=");
        i1;
        JVM INSTR tableswitch 1 4: default 64
    //                   1 127
    //                   2 97
    //                   3 107
    //                   4 117;
           goto _L1 _L2 _L3 _L4 _L5
_L2:
        break MISSING_BLOCK_LABEL_127;
_L1:
        printwriter.print("UNKNOWN");
_L6:
        printwriter.append(" mService=");
        if (as == null)
        {
            printwriter.println("null");
            return;
        } else
        {
            printwriter.append(b()).append("@").println(Integer.toHexString(System.identityHashCode(as.asBinder())));
            return;
        }
        s1;
        filedescriptor;
        JVM INSTR monitorexit ;
        throw s1;
_L3:
        printwriter.print("CONNECTING");
          goto _L6
_L4:
        printwriter.print("CONNECTED");
          goto _L6
_L5:
        printwriter.print("DISCONNECTING");
          goto _L6
        printwriter.print("DISCONNECTED");
          goto _L6
    }

    protected Bundle e()
    {
        return new Bundle();
    }

    protected final void f()
    {
        if (!isConnected())
        {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        } else
        {
            return;
        }
    }

    protected Bundle g()
    {
        return null;
    }

    public final Context getContext()
    {
        return a;
    }

    public final Looper getLooper()
    {
        return e;
    }

    public boolean isConnected()
    {
        Object obj = h;
        obj;
        JVM INSTR monitorenter ;
        Exception exception;
        boolean flag;
        if (n == 3)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        obj;
        JVM INSTR monitorexit ;
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean isConnecting()
    {
        Object obj = h;
        obj;
        JVM INSTR monitorenter ;
        Exception exception;
        boolean flag;
        if (n == 2)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        obj;
        JVM INSTR monitorexit ;
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void zza(u u1)
    {
        j = (u)av.a(u1, "Connection progress callbacks cannot be null.");
        b(2, null);
    }

    public void zza(ae ae)
    {
        Bundle bundle = g();
        ae = new ValidateAccountRequest(ae, (Scope[])o.toArray(new Scope[o.size()]), a.getPackageName(), bundle);
        try
        {
            i.a(new com.google.android.gms.common.internal.q(this, c.get()), ae);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("GmsClient", "service died");
            zzbT(1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("GmsClient", "Remote exception occurred", ae);
        }
    }

    public void zza(ae ae, Set set)
    {
        Object obj;
        try
        {
            obj = e();
            obj = (new GetServiceRequest(s)).a(a.getPackageName()).a(((Bundle) (obj)));
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("GmsClient", "service died");
            zzbT(1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ae ae)
        {
            Log.w("GmsClient", "Remote exception occurred", ae);
            return;
        }
        if (set == null)
        {
            break MISSING_BLOCK_LABEL_41;
        }
        ((GetServiceRequest) (obj)).a(set);
        if (!zzmn()) goto _L2; else goto _L1
_L1:
        ((GetServiceRequest) (obj)).a(zzpY()).a(ae);
_L4:
        i.a(new com.google.android.gms.common.internal.q(this, c.get()), ((GetServiceRequest) (obj)));
        return;
_L2:
        if (zzqu())
        {
            ((GetServiceRequest) (obj)).a(p);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void zzbT(int i1)
    {
        b.sendMessage(b.obtainMessage(4, c.get(), i1));
    }

    public boolean zzmJ()
    {
        return false;
    }

    public Intent zzmK()
    {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    public boolean zzmn()
    {
        return false;
    }

    public Bundle zznQ()
    {
        return null;
    }

    public IBinder zzoC()
    {
        if (i == null)
        {
            return null;
        } else
        {
            return i.asBinder();
        }
    }

    public final Account zzpY()
    {
        if (p != null)
        {
            return p;
        } else
        {
            return new Account("<<default account>>", "com.google");
        }
    }

    public void zzqp()
    {
        int i1 = g.a(a);
        if (i1 != 0)
        {
            b(1, null);
            j = new s(this);
            b.sendMessage(b.obtainMessage(3, c.get(), i1));
            return;
        } else
        {
            zza(new s(this));
            return;
        }
    }

    public final IInterface zzqs()
    {
        Object obj = h;
        obj;
        JVM INSTR monitorenter ;
        if (n == 4)
        {
            throw new DeadObjectException();
        }
        break MISSING_BLOCK_LABEL_28;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        f();
        IInterface iinterface;
        boolean flag;
        if (k != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        av.a(flag, "Client is connected but service is null");
        iinterface = k;
        obj;
        JVM INSTR monitorexit ;
        return iinterface;
    }

    public boolean zzqu()
    {
        return false;
    }

}
