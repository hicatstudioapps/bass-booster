// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

// Referenced classes of package com.google.android.gms.common.internal:
//            aw

public class ResolveAccountRequest
    implements SafeParcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new aw();
    final int a;
    private final Account b;
    private final int c;
    private final GoogleSignInAccount d;

    ResolveAccountRequest(int i, Account account, int j, GoogleSignInAccount googlesigninaccount)
    {
        a = i;
        b = account;
        c = j;
        d = googlesigninaccount;
    }

    public ResolveAccountRequest(Account account, int i, GoogleSignInAccount googlesigninaccount)
    {
        this(2, account, i, googlesigninaccount);
    }

    public Account a()
    {
        return b;
    }

    public int b()
    {
        return c;
    }

    public GoogleSignInAccount c()
    {
        return d;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        aw.a(this, parcel, i);
    }

}
