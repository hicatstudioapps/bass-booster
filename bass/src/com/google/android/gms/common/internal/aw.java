// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.common.internal.safeparcel.c;

// Referenced classes of package com.google.android.gms.common.internal:
//            ResolveAccountRequest

public class aw
    implements android.os.Parcelable.Creator
{

    public aw()
    {
    }

    static void a(ResolveAccountRequest resolveaccountrequest, Parcel parcel, int i)
    {
        int j = c.a(parcel);
        c.a(parcel, 1, resolveaccountrequest.a);
        c.a(parcel, 2, resolveaccountrequest.a(), i, false);
        c.a(parcel, 3, resolveaccountrequest.b());
        c.a(parcel, 4, resolveaccountrequest.c(), i, false);
        c.a(parcel, j);
    }

    public ResolveAccountRequest a(Parcel parcel)
    {
        GoogleSignInAccount googlesigninaccount;
        Account account;
        int i;
        int j;
        int l;
        googlesigninaccount = null;
        j = 0;
        l = com.google.android.gms.common.internal.safeparcel.a.b(parcel);
        account = null;
        i = 0;
_L7:
        int k;
        if (parcel.dataPosition() >= l)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        k = com.google.android.gms.common.internal.safeparcel.a.a(parcel);
        com.google.android.gms.common.internal.safeparcel.a.a(k);
        JVM INSTR tableswitch 1 4: default 68
    //                   1 101
    //                   2 120
    //                   3 148
    //                   4 167;
           goto _L1 _L2 _L3 _L4 _L5
_L5:
        break MISSING_BLOCK_LABEL_167;
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        com.google.android.gms.common.internal.safeparcel.a.b(parcel, k);
        k = j;
        j = i;
        i = k;
_L8:
        k = j;
        j = i;
        i = k;
        if (true) goto _L7; else goto _L6
_L6:
        k = com.google.android.gms.common.internal.safeparcel.a.d(parcel, k);
        i = j;
        j = k;
          goto _L8
_L3:
        account = (Account)com.google.android.gms.common.internal.safeparcel.a.a(parcel, k, Account.CREATOR);
        k = i;
        i = j;
        j = k;
          goto _L8
_L4:
        k = com.google.android.gms.common.internal.safeparcel.a.d(parcel, k);
        j = i;
        i = k;
          goto _L8
        googlesigninaccount = (GoogleSignInAccount)com.google.android.gms.common.internal.safeparcel.a.a(parcel, k, GoogleSignInAccount.CREATOR);
        k = i;
        i = j;
        j = k;
          goto _L8
        if (parcel.dataPosition() != l)
        {
            throw new b((new StringBuilder()).append("Overread allowed size end=").append(l).toString(), parcel);
        } else
        {
            return new ResolveAccountRequest(i, account, j, googlesigninaccount);
        }
    }

    public ResolveAccountRequest[] a(int i)
    {
        return new ResolveAccountRequest[i];
    }

    public Object createFromParcel(Parcel parcel)
    {
        return a(parcel);
    }

    public Object[] newArray(int i)
    {
        return a(i);
    }
}
