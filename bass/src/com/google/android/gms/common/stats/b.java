// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Debug;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.b.xo;
import com.google.android.gms.b.xw;
import com.google.android.gms.b.ye;
import com.google.android.gms.common.internal.g;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.google.android.gms.common.stats:
//            e, d, f, ConnectionEvent

public class b
{

    private static final Object a = new Object();
    private static b b;
    private static Integer h;
    private final List c;
    private final List d;
    private final List e;
    private final List f;
    private f g;
    private f i;

    private b()
    {
        if (c() == e.b)
        {
            c = Collections.EMPTY_LIST;
            d = Collections.EMPTY_LIST;
            e = Collections.EMPTY_LIST;
            f = Collections.EMPTY_LIST;
            return;
        }
        Object obj = (String)d.b.c();
        if (obj == null)
        {
            obj = Collections.EMPTY_LIST;
        } else
        {
            obj = Arrays.asList(((String) (obj)).split(","));
        }
        c = ((List) (obj));
        obj = (String)d.c.c();
        if (obj == null)
        {
            obj = Collections.EMPTY_LIST;
        } else
        {
            obj = Arrays.asList(((String) (obj)).split(","));
        }
        d = ((List) (obj));
        obj = (String)d.d.c();
        if (obj == null)
        {
            obj = Collections.EMPTY_LIST;
        } else
        {
            obj = Arrays.asList(((String) (obj)).split(","));
        }
        e = ((List) (obj));
        obj = (String)d.e.c();
        if (obj == null)
        {
            obj = Collections.EMPTY_LIST;
        } else
        {
            obj = Arrays.asList(((String) (obj)).split(","));
        }
        f = ((List) (obj));
        g = new f(1024, ((Long)d.f.c()).longValue());
        i = new f(1024, ((Long)d.f.c()).longValue());
    }

    public static b a()
    {
        synchronized (a)
        {
            if (b == null)
            {
                b = new b();
            }
        }
        return b;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String a(ServiceConnection serviceconnection)
    {
        return String.valueOf((long)Process.myPid() << 32 | (long)System.identityHashCode(serviceconnection));
    }

    private void a(Context context, String s, int j, String s1, String s2, String s3, String s4)
    {
        long l1 = System.currentTimeMillis();
        Object obj = null;
        String s5 = obj;
        if ((c() & e.f) != 0)
        {
            s5 = obj;
            if (j != 13)
            {
                s5 = ye.a(3, 5);
            }
        }
        long l = 0L;
        if ((c() & e.h) != 0)
        {
            l = Debug.getNativeHeapAllocatedSize();
        }
        if (j == 1 || j == 4 || j == 14)
        {
            s = new ConnectionEvent(l1, j, null, null, null, null, s5, s, SystemClock.elapsedRealtime(), l);
        } else
        {
            s = new ConnectionEvent(l1, j, s1, s2, s3, s4, s5, s, SystemClock.elapsedRealtime(), l);
        }
        context.startService((new Intent()).setComponent(e.a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", s));
    }

    private void a(Context context, String s, String s1, Intent intent, int j)
    {
        Object obj1 = null;
        if (b() && g != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s2;
        Object obj;
        if (j == 4 || j == 1)
        {
            if (!g.b(s))
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = null;
            s2 = null;
            intent = obj1;
        } else
        {
            obj = b(context, intent);
            if (obj == null)
            {
                Log.w("ConnectionTracker", String.format("Client %s made an invalid request %s", new Object[] {
                    s1, intent.toUri(0)
                }));
                return;
            }
            s2 = ((ServiceInfo) (obj)).processName;
            obj = ((ServiceInfo) (obj)).name;
            intent = ye.a(context);
            if (!a(((String) (intent)), s1, s2, ((String) (obj))))
            {
                continue; /* Loop/switch isn't completed */
            }
            g.a(s);
        }
        a(context, s, j, ((String) (intent)), s1, s2, ((String) (obj)));
        return;
        if (true) goto _L1; else goto _L3
_L3:
    }

    private boolean a(Context context, Intent intent)
    {
        intent = intent.getComponent();
        if (intent == null || g.a && "com.google.android.gms".equals(intent.getPackageName()))
        {
            return false;
        } else
        {
            return xw.a(context, intent.getPackageName());
        }
    }

    private boolean a(String s, String s1, String s2, String s3)
    {
        int j = c();
        return !c.contains(s) && !d.contains(s1) && !e.contains(s2) && !f.contains(s3) && (!s2.equals(s) || (j & com.google.android.gms.common.stats.e.g) == 0);
    }

    private static ServiceInfo b(Context context, Intent intent)
    {
        context = context.getPackageManager().queryIntentServices(intent, 128);
        if (context == null || context.size() == 0)
        {
            Log.w("ConnectionTracker", String.format("There are no handler of this intent: %s\n Stack trace: %s", new Object[] {
                intent.toUri(0), ye.a(3, 20)
            }));
            return null;
        }
        if (context.size() > 1)
        {
            Log.w("ConnectionTracker", String.format("Multiple handlers found for this intent: %s\n Stack trace: %s", new Object[] {
                intent.toUri(0), ye.a(3, 20)
            }));
            intent = context.iterator();
            if (intent.hasNext())
            {
                Log.w("ConnectionTracker", ((ResolveInfo)intent.next()).serviceInfo.name);
                return null;
            }
        }
        return ((ResolveInfo)context.get(0)).serviceInfo;
    }

    private boolean b()
    {
        while (!g.a || c() == e.b) 
        {
            return false;
        }
        return true;
    }

    private static int c()
    {
        if (h != null) goto _L2; else goto _L1
_L1:
        if (!xw.a()) goto _L4; else goto _L3
_L3:
        int j = ((Integer)d.a.c()).intValue();
_L5:
        h = Integer.valueOf(j);
_L2:
        return h.intValue();
_L4:
        j = e.b;
          goto _L5
        SecurityException securityexception;
        securityexception;
        h = Integer.valueOf(e.b);
          goto _L2
    }

    public void a(Context context, ServiceConnection serviceconnection)
    {
        context.unbindService(serviceconnection);
        a(context, a(serviceconnection), ((String) (null)), ((Intent) (null)), 1);
    }

    public void a(Context context, ServiceConnection serviceconnection, String s, Intent intent)
    {
        a(context, a(serviceconnection), s, intent, 3);
    }

    public boolean a(Context context, Intent intent, ServiceConnection serviceconnection, int j)
    {
        return a(context, context.getClass().getName(), intent, serviceconnection, j);
    }

    public boolean a(Context context, String s, Intent intent, ServiceConnection serviceconnection, int j)
    {
        if (a(context, intent))
        {
            Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
            return false;
        }
        boolean flag = context.bindService(intent, serviceconnection, j);
        if (flag)
        {
            a(context, a(serviceconnection), s, intent, 2);
        }
        return flag;
    }

    public void b(Context context, ServiceConnection serviceconnection)
    {
        a(context, a(serviceconnection), ((String) (null)), ((Intent) (null)), 4);
    }

}
