// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.google.android.gms.common.stats;

import android.os.SystemClock;
import android.support.v4.b.l;
import android.util.Log;

public class f
{

    private final long a;
    private final int b;
    private final l c;

    public f()
    {
        a = 60000L;
        b = 10;
        c = new l(10);
    }

    public f(int i, long l1)
    {
        a = l1;
        b = i;
        c = new l();
    }

    private void a(long l1, long l2)
    {
        for (int i = c.size() - 1; i >= 0; i--)
        {
            if (l2 - ((Long)c.c(i)).longValue() > l1)
            {
                c.d(i);
            }
        }

    }

    public Long a(String s)
    {
        long l2 = SystemClock.elapsedRealtime();
        long l1 = a;
        this;
        JVM INSTR monitorenter ;
        for (; c.size() >= b; Log.w("ConnectionTracker", (new StringBuilder()).append("The max capacity ").append(b).append(" is not enough. Current durationThreshold is: ").append(l1).toString()))
        {
            a(l1, l2);
            l1 /= 2L;
        }

        break MISSING_BLOCK_LABEL_84;
        s;
        this;
        JVM INSTR monitorexit ;
        throw s;
        s = (Long)c.put(s, Long.valueOf(l2));
        this;
        JVM INSTR monitorexit ;
        return s;
    }

    public boolean b(String s)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag;
        if (c.remove(s) != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        this;
        JVM INSTR monitorexit ;
        return flag;
        s;
        this;
        JVM INSTR monitorexit ;
        throw s;
    }
}
