// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;

// Referenced classes of package android.support.v4.app:
//            ak, d, i, n

class f
    implements android.view.ViewTreeObserver.OnPreDrawListener
{

    final View a;
    final Object b;
    final ArrayList c;
    final i d;
    final boolean e;
    final n f;
    final n g;
    final d h;

    f(d d1, View view, Object obj, ArrayList arraylist, i j, boolean flag, n n, 
            n n1)
    {
        h = d1;
        a = view;
        b = obj;
        c = arraylist;
        d = j;
        e = flag;
        f = n;
        g = n1;
        super();
    }

    public boolean onPreDraw()
    {
        a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (b != null)
        {
            ak.a(b, c);
            c.clear();
            android.support.v4.b.a a1 = android.support.v4.app.d.a(h, d, e, f);
            ak.a(b, d.d, a1, c);
            android.support.v4.app.d.a(h, a1, d);
            android.support.v4.app.d.a(h, d, f, g, e, a1);
        }
        return true;
    }
}
