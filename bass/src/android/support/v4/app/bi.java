// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;

// Referenced classes of package android.support.v4.app:
//            bd, bl, bj, bu

public class bi
{

    Notification A;
    public Notification B;
    public ArrayList C;
    public Context a;
    public CharSequence b;
    public CharSequence c;
    PendingIntent d;
    PendingIntent e;
    RemoteViews f;
    public Bitmap g;
    public CharSequence h;
    public int i;
    int j;
    boolean k;
    public boolean l;
    public bu m;
    public CharSequence n;
    int o;
    int p;
    boolean q;
    String r;
    boolean s;
    String t;
    public ArrayList u;
    boolean v;
    String w;
    Bundle x;
    int y;
    int z;

    public bi(Context context)
    {
        k = true;
        u = new ArrayList();
        v = false;
        y = 0;
        z = 0;
        B = new Notification();
        a = context;
        B.when = System.currentTimeMillis();
        B.audioStreamType = -1;
        j = 0;
        C = new ArrayList();
    }

    private void a(int i1, boolean flag)
    {
        if (flag)
        {
            Notification notification = B;
            notification.flags = notification.flags | i1;
            return;
        } else
        {
            Notification notification1 = B;
            notification1.flags = notification1.flags & ~i1;
            return;
        }
    }

    protected static CharSequence d(CharSequence charsequence)
    {
        while (charsequence == null || charsequence.length() <= 5120) 
        {
            return charsequence;
        }
        return charsequence.subSequence(0, 5120);
    }

    public Notification a()
    {
        return bd.a().a(this, b());
    }

    public bi a(int i1)
    {
        B.icon = i1;
        return this;
    }

    public bi a(long l1)
    {
        B.when = l1;
        return this;
    }

    public bi a(PendingIntent pendingintent)
    {
        d = pendingintent;
        return this;
    }

    public bi a(Bitmap bitmap)
    {
        g = bitmap;
        return this;
    }

    public bi a(CharSequence charsequence)
    {
        b = d(charsequence);
        return this;
    }

    public bi a(boolean flag)
    {
        a(2, flag);
        return this;
    }

    public bi b(CharSequence charsequence)
    {
        c = d(charsequence);
        return this;
    }

    public bi b(boolean flag)
    {
        a(8, flag);
        return this;
    }

    protected bj b()
    {
        return new bj();
    }

    public bi c(CharSequence charsequence)
    {
        B.tickerText = d(charsequence);
        return this;
    }

    public bi c(boolean flag)
    {
        a(16, flag);
        return this;
    }
}
