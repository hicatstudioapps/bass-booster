// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

// Referenced classes of package android.support.v4.app:
//            j, d, h, n, 
//            z

final class BackStackState
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new j();
    final int a[];
    final int b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final ArrayList j;
    final ArrayList k;

    public BackStackState(Parcel parcel)
    {
        a = parcel.createIntArray();
        b = parcel.readInt();
        c = parcel.readInt();
        d = parcel.readString();
        e = parcel.readInt();
        f = parcel.readInt();
        g = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        h = parcel.readInt();
        i = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        j = parcel.createStringArrayList();
        k = parcel.createStringArrayList();
    }

    public BackStackState(d d1)
    {
        h h1 = d1.c;
        int l;
        int i1;
        for (l = 0; h1 != null; l = i1)
        {
            i1 = l;
            if (h1.i != null)
            {
                i1 = l + h1.i.size();
            }
            h1 = h1.a;
        }

        a = new int[l + d1.e * 7];
        if (!d1.l)
        {
            throw new IllegalStateException("Not on back stack");
        }
        h1 = d1.c;
        l = 0;
        while (h1 != null) 
        {
            int ai[] = a;
            int j1 = l + 1;
            ai[l] = h1.c;
            ai = a;
            int k1 = j1 + 1;
            if (h1.d != null)
            {
                l = h1.d.p;
            } else
            {
                l = -1;
            }
            ai[j1] = l;
            ai = a;
            l = k1 + 1;
            ai[k1] = h1.e;
            ai = a;
            j1 = l + 1;
            ai[l] = h1.f;
            ai = a;
            l = j1 + 1;
            ai[j1] = h1.g;
            ai = a;
            j1 = l + 1;
            ai[l] = h1.h;
            if (h1.i != null)
            {
                int l1 = h1.i.size();
                int ai1[] = a;
                l = j1 + 1;
                ai1[j1] = l1;
                for (j1 = 0; j1 < l1;)
                {
                    a[l] = ((n)h1.i.get(j1)).p;
                    j1++;
                    l++;
                }

            } else
            {
                int ai2[] = a;
                l = j1 + 1;
                ai2[j1] = 0;
            }
            h1 = h1.a;
        }
        b = d1.j;
        c = d1.k;
        d = d1.n;
        e = d1.p;
        f = d1.q;
        g = d1.r;
        h = d1.s;
        i = d1.t;
        j = d1.u;
        k = d1.v;
    }

    public d a(z z1)
    {
        d d1 = new d(z1);
        int j1 = 0;
        for (int l = 0; l < a.length;)
        {
            h h1 = new h();
            int ai[] = a;
            int i1 = l + 1;
            h1.c = ai[l];
            if (z.a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("Instantiate ").append(d1).append(" op #").append(j1).append(" base fragment #").append(a[i1]).toString());
            }
            ai = a;
            l = i1 + 1;
            i1 = ai[i1];
            int l1;
            if (i1 >= 0)
            {
                h1.d = (n)z1.f.get(i1);
            } else
            {
                h1.d = null;
            }
            ai = a;
            i1 = l + 1;
            h1.e = ai[l];
            ai = a;
            l = i1 + 1;
            h1.f = ai[i1];
            ai = a;
            i1 = l + 1;
            h1.g = ai[l];
            ai = a;
            l = i1 + 1;
            h1.h = ai[i1];
            ai = a;
            i1 = l + 1;
            l1 = ai[l];
            l = i1;
            if (l1 > 0)
            {
                h1.i = new ArrayList(l1);
                int k1 = 0;
                do
                {
                    l = i1;
                    if (k1 >= l1)
                    {
                        break;
                    }
                    if (z.a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("Instantiate ").append(d1).append(" set remove fragment #").append(a[i1]).toString());
                    }
                    n n1 = (n)z1.f.get(a[i1]);
                    h1.i.add(n1);
                    k1++;
                    i1++;
                } while (true);
            }
            d1.a(h1);
            j1++;
        }

        d1.j = b;
        d1.k = c;
        d1.n = d;
        d1.p = e;
        d1.l = true;
        d1.q = f;
        d1.r = g;
        d1.s = h;
        d1.t = i;
        d1.u = j;
        d1.v = k;
        d1.a(1);
        return d1;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int l)
    {
        parcel.writeIntArray(a);
        parcel.writeInt(b);
        parcel.writeInt(c);
        parcel.writeString(d);
        parcel.writeInt(e);
        parcel.writeInt(f);
        TextUtils.writeToParcel(g, parcel, 0);
        parcel.writeInt(h);
        TextUtils.writeToParcel(i, parcel, 0);
        parcel.writeStringList(j);
        parcel.writeStringList(k);
    }

}
