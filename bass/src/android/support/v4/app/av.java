// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

// Referenced classes of package android.support.v4.app:
//            ay, ax, aw

public class av
{

    private static final aw a;

    public static String a(Context context, ComponentName componentname)
    {
        componentname = context.getPackageManager().getActivityInfo(componentname, 128);
        return a.a(context, componentname);
    }

    public static void a(Activity activity)
    {
        Intent intent = b(activity);
        if (intent == null)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Activity ").append(activity.getClass().getSimpleName()).append(" does not have a parent activity name specified.").append(" (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data> ").append(" element in your manifest?)").toString());
        } else
        {
            a(activity, intent);
            return;
        }
    }

    public static void a(Activity activity, Intent intent)
    {
        a.a(activity, intent);
    }

    public static Intent b(Activity activity)
    {
        return a.a(activity);
    }

    public static String c(Activity activity)
    {
        try
        {
            activity = a(activity, activity.getComponentName());
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            throw new IllegalArgumentException(activity);
        }
        return activity;
    }

    static 
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            a = new ay();
        } else
        {
            a = new ax();
        }
    }
}
