// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.os.Bundle;

// Referenced classes of package android.support.v4.app:
//            cs

class cu
{

    static Bundle a(cs cs1)
    {
        Bundle bundle = new Bundle();
        bundle.putString("resultKey", cs1.a());
        bundle.putCharSequence("label", cs1.b());
        bundle.putCharSequenceArray("choices", cs1.c());
        bundle.putBoolean("allowFreeFormInput", cs1.d());
        bundle.putBundle("extras", cs1.e());
        return bundle;
    }

    static Bundle[] a(cs acs[])
    {
        if (acs == null)
        {
            return null;
        }
        Bundle abundle[] = new Bundle[acs.length];
        for (int i = 0; i < acs.length; i++)
        {
            abundle[i] = a(acs[i]);
        }

        return abundle;
    }
}
