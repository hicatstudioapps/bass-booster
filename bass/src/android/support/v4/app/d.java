// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.support.v4.b.a;
import android.support.v4.b.e;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

// Referenced classes of package android.support.v4.app:
//            aj, i, z, w, 
//            n, ak, cv, h, 
//            f, ap, g, u, 
//            e

final class d extends aj
    implements Runnable
{

    static final boolean a;
    final z b;
    h c;
    h d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    int k;
    boolean l;
    boolean m;
    String n;
    boolean o;
    int p;
    int q;
    CharSequence r;
    int s;
    CharSequence t;
    ArrayList u;
    ArrayList v;

    public d(z z1)
    {
        m = true;
        p = -1;
        b = z1;
    }

    private i a(SparseArray sparsearray, SparseArray sparsearray1, boolean flag)
    {
        boolean flag4 = false;
        i i1 = new i(this);
        i1.d = new View(b.o.g());
        int j1 = 0;
        boolean flag1 = false;
        int l1;
        boolean flag3;
        do
        {
            l1 = ((flag4) ? 1 : 0);
            flag3 = flag1;
            if (j1 >= sparsearray.size())
            {
                break;
            }
            if (a(sparsearray.keyAt(j1), i1, flag, sparsearray, sparsearray1))
            {
                flag1 = true;
            }
            j1++;
        } while (true);
        while (l1 < sparsearray1.size()) 
        {
            int k1 = sparsearray1.keyAt(l1);
            boolean flag2 = flag3;
            if (sparsearray.get(k1) == null)
            {
                flag2 = flag3;
                if (a(k1, i1, flag, sparsearray, sparsearray1))
                {
                    flag2 = true;
                }
            }
            l1++;
            flag3 = flag2;
        }
        sparsearray = i1;
        if (!flag3)
        {
            sparsearray = null;
        }
        return sparsearray;
    }

    static a a(d d1, i i1, boolean flag, n n1)
    {
        return d1.a(i1, flag, n1);
    }

    private a a(i i1, n n1, boolean flag)
    {
        a a2 = new a();
        a a1 = a2;
        if (u != null)
        {
            android.support.v4.app.ak.a(a2, n1.n());
            if (flag)
            {
                a2.a(v);
                a1 = a2;
            } else
            {
                a1 = a(u, v, a2);
            }
        }
        if (flag)
        {
            if (n1.ai != null)
            {
                n1.ai.a(v, a1);
            }
            a(i1, a1, false);
            return a1;
        }
        if (n1.aj != null)
        {
            n1.aj.a(v, a1);
        }
        b(i1, a1, false);
        return a1;
    }

    private a a(i i1, boolean flag, n n1)
    {
        a a1 = b(i1, n1, flag);
        if (flag)
        {
            if (n1.aj != null)
            {
                n1.aj.a(v, a1);
            }
            a(i1, a1, true);
            return a1;
        }
        if (n1.ai != null)
        {
            n1.ai.a(v, a1);
        }
        b(i1, a1, true);
        return a1;
    }

    private static a a(ArrayList arraylist, ArrayList arraylist1, a a1)
    {
        if (a1.isEmpty())
        {
            return a1;
        }
        a a2 = new a();
        int j1 = arraylist.size();
        for (int i1 = 0; i1 < j1; i1++)
        {
            View view = (View)a1.get(arraylist.get(i1));
            if (view != null)
            {
                a2.put(arraylist1.get(i1), view);
            }
        }

        return a2;
    }

    private static Object a(n n1, n n2, boolean flag)
    {
        if (n1 == null || n2 == null)
        {
            return null;
        }
        if (flag)
        {
            n1 = ((n) (n2.y()));
        } else
        {
            n1 = ((n) (n1.x()));
        }
        return ak.b(n1);
    }

    private static Object a(n n1, boolean flag)
    {
        if (n1 == null)
        {
            return null;
        }
        if (flag)
        {
            n1 = ((n) (n1.w()));
        } else
        {
            n1 = ((n) (n1.t()));
        }
        return android.support.v4.app.ak.a(n1);
    }

    private static Object a(Object obj, n n1, ArrayList arraylist, a a1, View view)
    {
        Object obj1 = obj;
        if (obj != null)
        {
            obj1 = android.support.v4.app.ak.a(obj, n1.n(), arraylist, a1, view);
        }
        return obj1;
    }

    private void a(int i1, n n1, String s1, int j1)
    {
        n1.C = b;
        if (s1 != null)
        {
            if (n1.I != null && !s1.equals(n1.I))
            {
                throw new IllegalStateException((new StringBuilder()).append("Can't change tag of fragment ").append(n1).append(": was ").append(n1.I).append(" now ").append(s1).toString());
            }
            n1.I = s1;
        }
        if (i1 != 0)
        {
            if (n1.G != 0 && n1.G != i1)
            {
                throw new IllegalStateException((new StringBuilder()).append("Can't change container ID of fragment ").append(n1).append(": was ").append(n1.G).append(" now ").append(i1).toString());
            }
            n1.G = i1;
            n1.H = i1;
        }
        s1 = new h();
        s1.c = j1;
        s1.d = n1;
        a(((h) (s1)));
    }

    static void a(d d1, i i1, int j1, Object obj)
    {
        d1.a(i1, j1, obj);
    }

    static void a(d d1, i i1, n n1, n n2, boolean flag, a a1)
    {
        d1.a(i1, n1, n2, flag, a1);
    }

    static void a(d d1, a a1, i i1)
    {
        d1.a(a1, i1);
    }

    private void a(i i1, int j1, Object obj)
    {
        if (b.g != null)
        {
            int k1 = 0;
            while (k1 < b.g.size()) 
            {
                n n1 = (n)b.g.get(k1);
                if (n1.T != null && n1.S != null && n1.H == j1)
                {
                    if (n1.J)
                    {
                        if (!i1.b.contains(n1.T))
                        {
                            android.support.v4.app.ak.a(obj, n1.T, true);
                            i1.b.add(n1.T);
                        }
                    } else
                    {
                        android.support.v4.app.ak.a(obj, n1.T, false);
                        i1.b.remove(n1.T);
                    }
                }
                k1++;
            }
        }
    }

    private void a(i i1, n n1, n n2, boolean flag, a a1)
    {
        if (flag)
        {
            i1 = n2.ai;
        } else
        {
            i1 = n1.ai;
        }
        if (i1 != null)
        {
            i1.b(new ArrayList(a1.keySet()), new ArrayList(a1.values()), null);
        }
    }

    private void a(i i1, a a1, boolean flag)
    {
        int j1;
        int k1;
        if (v == null)
        {
            j1 = 0;
        } else
        {
            j1 = v.size();
        }
        k1 = 0;
        while (k1 < j1) 
        {
            String s1 = (String)u.get(k1);
            Object obj = (View)a1.get((String)v.get(k1));
            if (obj != null)
            {
                obj = android.support.v4.app.ak.a(((View) (obj)));
                if (flag)
                {
                    a(i1.a, s1, ((String) (obj)));
                } else
                {
                    a(i1.a, ((String) (obj)), s1);
                }
            }
            k1++;
        }
    }

    private void a(i i1, View view, Object obj, n n1, n n2, boolean flag, ArrayList arraylist)
    {
        view.getViewTreeObserver().addOnPreDrawListener(new f(this, view, obj, arraylist, i1, flag, n1, n2));
    }

    private static void a(i i1, ArrayList arraylist, ArrayList arraylist1)
    {
        if (arraylist != null)
        {
            for (int j1 = 0; j1 < arraylist.size(); j1++)
            {
                String s1 = (String)arraylist.get(j1);
                String s2 = (String)arraylist1.get(j1);
                a(i1.a, s1, s2);
            }

        }
    }

    private void a(a a1, i i1)
    {
        if (v != null && !a1.isEmpty())
        {
            a1 = (View)a1.get(v.get(0));
            if (a1 != null)
            {
                i1.c.a = a1;
            }
        }
    }

    private static void a(a a1, String s1, String s2)
    {
        if (s1 == null || s2 == null) goto _L2; else goto _L1
_L1:
        int i1 = 0;
_L6:
        if (i1 >= a1.size())
        {
            break; /* Loop/switch isn't completed */
        }
        if (!s1.equals(a1.c(i1))) goto _L4; else goto _L3
_L3:
        a1.a(i1, s2);
_L2:
        return;
_L4:
        i1++;
        if (true) goto _L6; else goto _L5
_L5:
        a1.put(s1, s2);
        return;
    }

    private static void a(SparseArray sparsearray, n n1)
    {
        if (n1 != null)
        {
            int i1 = n1.H;
            if (i1 != 0 && !n1.m() && n1.k() && n1.n() != null && sparsearray.get(i1) == null)
            {
                sparsearray.put(i1, n1);
            }
        }
    }

    private void a(View view, i i1, int j1, Object obj)
    {
        view.getViewTreeObserver().addOnPreDrawListener(new g(this, view, i1, j1, obj));
    }

    private boolean a(int i1, i j1, boolean flag, SparseArray sparsearray, SparseArray sparsearray1)
    {
        ViewGroup viewgroup = (ViewGroup)b.p.a(i1);
        if (viewgroup == null)
        {
            return false;
        }
        Object obj = (n)sparsearray1.get(i1);
        Object obj2 = (n)sparsearray.get(i1);
        Object obj1 = a(((n) (obj)), flag);
        sparsearray1 = ((SparseArray) (a(((n) (obj)), ((n) (obj2)), flag)));
        Object obj3 = b(((n) (obj2)), flag);
        sparsearray = null;
        ArrayList arraylist1 = new ArrayList();
        if (sparsearray1 != null)
        {
            a a1 = a(j1, ((n) (obj2)), flag);
            if (a1.isEmpty())
            {
                sparsearray = null;
                sparsearray1 = null;
            } else
            {
                if (flag)
                {
                    sparsearray = ((n) (obj2)).ai;
                } else
                {
                    sparsearray = ((n) (obj)).ai;
                }
                if (sparsearray != null)
                {
                    sparsearray.a(new ArrayList(a1.keySet()), new ArrayList(a1.values()), null);
                }
                a(j1, ((View) (viewgroup)), sparsearray1, ((n) (obj)), ((n) (obj2)), flag, arraylist1);
                sparsearray = a1;
            }
        }
        if (obj1 == null && sparsearray1 == null && obj3 == null)
        {
            return false;
        }
        ArrayList arraylist = new ArrayList();
        obj2 = a(obj3, ((n) (obj2)), arraylist, ((a) (sparsearray)), j1.d);
        if (v != null && sparsearray != null)
        {
            obj3 = (View)sparsearray.get(v.get(0));
            if (obj3 != null)
            {
                if (obj2 != null)
                {
                    android.support.v4.app.ak.a(obj2, ((View) (obj3)));
                }
                if (sparsearray1 != null)
                {
                    android.support.v4.app.ak.a(sparsearray1, ((View) (obj3)));
                }
            }
        }
        obj3 = new android.support.v4.app.e(this, ((n) (obj)));
        ArrayList arraylist2 = new ArrayList();
        a a2 = new a();
        boolean flag1 = true;
        if (obj != null)
        {
            if (flag)
            {
                flag1 = ((n) (obj)).A();
            } else
            {
                flag1 = ((n) (obj)).z();
            }
        }
        obj = android.support.v4.app.ak.a(obj1, obj2, sparsearray1, flag1);
        if (obj != null)
        {
            android.support.v4.app.ak.a(obj1, sparsearray1, viewgroup, ((aq) (obj3)), j1.d, j1.c, j1.a, arraylist2, sparsearray, a2, arraylist1);
            a(((View) (viewgroup)), j1, i1, obj);
            android.support.v4.app.ak.a(obj, j1.d, true);
            a(j1, i1, obj);
            android.support.v4.app.ak.a(viewgroup, obj);
            android.support.v4.app.ak.a(viewgroup, j1.d, obj1, arraylist2, obj2, arraylist, sparsearray1, arraylist1, obj, j1.b, a2);
        }
        return obj != null;
    }

    private a b(i i1, n n1, boolean flag)
    {
        a a1;
label0:
        {
            a1 = new a();
            n1 = n1.n();
            i1 = a1;
            if (n1 != null)
            {
                i1 = a1;
                if (u != null)
                {
                    android.support.v4.app.ak.a(a1, n1);
                    if (!flag)
                    {
                        break label0;
                    }
                    i1 = a(u, v, a1);
                }
            }
            return i1;
        }
        a1.a(v);
        return a1;
    }

    private static Object b(n n1, boolean flag)
    {
        if (n1 == null)
        {
            return null;
        }
        if (flag)
        {
            n1 = ((n) (n1.u()));
        } else
        {
            n1 = ((n) (n1.v()));
        }
        return android.support.v4.app.ak.a(n1);
    }

    private void b(i i1, a a1, boolean flag)
    {
        int k1 = a1.size();
        int j1 = 0;
        while (j1 < k1) 
        {
            String s1 = (String)a1.b(j1);
            String s2 = android.support.v4.app.ak.a((View)a1.c(j1));
            if (flag)
            {
                a(i1.a, s1, s2);
            } else
            {
                a(i1.a, s2, s1);
            }
            j1++;
        }
    }

    private void b(SparseArray sparsearray, n n1)
    {
        if (n1 != null)
        {
            int i1 = n1.H;
            if (i1 != 0)
            {
                sparsearray.put(i1, n1);
            }
        }
    }

    private void b(SparseArray sparsearray, SparseArray sparsearray1)
    {
        if (b.p.a()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        h h1 = c;
_L12:
        if (h1 == null) goto _L1; else goto _L3
_L3:
        h1.c;
        JVM INSTR tableswitch 1 7: default 72
    //                   1 82
    //                   2 95
    //                   3 214
    //                   4 226
    //                   5 238
    //                   6 251
    //                   7 263;
           goto _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L4:
        break; /* Loop/switch isn't completed */
_L11:
        break MISSING_BLOCK_LABEL_263;
_L13:
        h1 = h1.a;
          goto _L12
_L5:
        b(sparsearray1, h1.d);
          goto _L13
_L6:
        n n1 = h1.d;
        n n2;
        if (b.g != null)
        {
            int i1 = 0;
label0:
            do
            {
label1:
                {
                    n2 = n1;
                    if (i1 >= b.g.size())
                    {
                        break label0;
                    }
                    n n3 = (n)b.g.get(i1);
                    if (n1 != null)
                    {
                        n2 = n1;
                        if (n3.H != n1.H)
                        {
                            break label1;
                        }
                    }
                    if (n3 == n1)
                    {
                        n2 = null;
                    } else
                    {
                        a(sparsearray, n3);
                        n2 = n1;
                    }
                }
                i1++;
                n1 = n2;
            } while (true);
        } else
        {
            n2 = n1;
        }
        b(sparsearray1, n2);
          goto _L13
_L7:
        a(sparsearray, h1.d);
          goto _L13
_L8:
        a(sparsearray, h1.d);
          goto _L13
_L9:
        b(sparsearray1, h1.d);
          goto _L13
_L10:
        a(sparsearray, h1.d);
          goto _L13
        b(sparsearray1, h1.d);
          goto _L13
    }

    public int a()
    {
        return a(false);
    }

    int a(boolean flag)
    {
        if (o)
        {
            throw new IllegalStateException("commit already called");
        }
        if (android.support.v4.app.z.a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Commit: ").append(this).toString());
            a("  ", ((FileDescriptor) (null)), new PrintWriter(new e("FragmentManager")), ((String []) (null)));
        }
        o = true;
        if (l)
        {
            p = b.a(this);
        } else
        {
            p = -1;
        }
        b.a(this, flag);
        return p;
    }

    public aj a(n n1)
    {
        h h1 = new h();
        h1.c = 3;
        h1.d = n1;
        a(h1);
        return this;
    }

    public aj a(n n1, String s1)
    {
        a(0, n1, s1, 1);
        return this;
    }

    public i a(boolean flag, i i1, SparseArray sparsearray, SparseArray sparsearray1)
    {
        i j1;
        if (android.support.v4.app.z.a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("popFromBackStack: ").append(this).toString());
            a("  ", ((FileDescriptor) (null)), new PrintWriter(new e("FragmentManager")), ((String []) (null)));
        }
        j1 = i1;
        if (!a) goto _L2; else goto _L1
_L1:
        if (i1 != null) goto _L4; else goto _L3
_L3:
        if (sparsearray.size() != 0) goto _L6; else goto _L5
_L5:
        j1 = i1;
        if (sparsearray1.size() == 0) goto _L2; else goto _L6
_L6:
        j1 = a(sparsearray, sparsearray1, true);
_L2:
        int k1;
        int l1;
        int i2;
        int j2;
        a(-1);
        if (j1 != null)
        {
            k1 = 0;
        } else
        {
            k1 = k;
        }
        if (j1 != null)
        {
            l1 = 0;
        } else
        {
            l1 = j;
        }
        i1 = d;
_L17:
        if (i1 == null) goto _L8; else goto _L7
_L7:
        if (j1 != null)
        {
            i2 = 0;
        } else
        {
            i2 = ((h) (i1)).g;
        }
        if (j1 != null)
        {
            j2 = 0;
        } else
        {
            j2 = ((h) (i1)).h;
        }
        ((h) (i1)).c;
        JVM INSTR tableswitch 1 7: default 192
    //                   1 284
    //                   2 318
    //                   3 407
    //                   4 430
    //                   5 459
    //                   6 488
    //                   7 517;
           goto _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16
_L9:
        throw new IllegalArgumentException((new StringBuilder()).append("Unknown cmd: ").append(((h) (i1)).c).toString());
_L4:
        j1 = i1;
        if (!flag)
        {
            a(i1, v, u);
            j1 = i1;
        }
          goto _L2
_L10:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = j2;
        b.a(sparsearray, z.c(l1), k1);
_L18:
        i1 = ((h) (i1)).b;
          goto _L17
_L11:
        sparsearray = ((h) (i1)).d;
        if (sparsearray != null)
        {
            sparsearray.R = j2;
            b.a(sparsearray, z.c(l1), k1);
        }
        if (((h) (i1)).i != null)
        {
            j2 = 0;
            while (j2 < ((h) (i1)).i.size()) 
            {
                sparsearray = (n)((h) (i1)).i.get(j2);
                sparsearray.R = i2;
                b.a(sparsearray, false);
                j2++;
            }
        }
          goto _L18
_L12:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = i2;
        b.a(sparsearray, false);
          goto _L18
_L13:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = i2;
        b.c(sparsearray, z.c(l1), k1);
          goto _L18
_L14:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = j2;
        b.b(sparsearray, z.c(l1), k1);
          goto _L18
_L15:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = i2;
        b.e(sparsearray, z.c(l1), k1);
          goto _L18
_L16:
        sparsearray = ((h) (i1)).d;
        sparsearray.R = i2;
        b.d(sparsearray, z.c(l1), k1);
          goto _L18
_L8:
        if (flag)
        {
            b.a(b.n, z.c(l1), k1, true);
            j1 = null;
        }
        if (p >= 0)
        {
            b.b(p);
            p = -1;
        }
        return j1;
    }

    void a(int i1)
    {
        if (l)
        {
            if (android.support.v4.app.z.a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("Bump nesting in ").append(this).append(" by ").append(i1).toString());
            }
            h h1 = c;
            while (h1 != null) 
            {
                if (h1.d != null)
                {
                    n n1 = h1.d;
                    n1.B = n1.B + i1;
                    if (android.support.v4.app.z.a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("Bump nesting of ").append(h1.d).append(" to ").append(h1.d.B).toString());
                    }
                }
                if (h1.i != null)
                {
                    for (int j1 = h1.i.size() - 1; j1 >= 0; j1--)
                    {
                        n n2 = (n)h1.i.get(j1);
                        n2.B = n2.B + i1;
                        if (android.support.v4.app.z.a)
                        {
                            Log.v("FragmentManager", (new StringBuilder()).append("Bump nesting of ").append(n2).append(" to ").append(n2.B).toString());
                        }
                    }

                }
                h1 = h1.a;
            }
        }
    }

    void a(h h1)
    {
        if (c == null)
        {
            d = h1;
            c = h1;
        } else
        {
            h1.b = d;
            d.a = h1;
            d = h1;
        }
        h1.e = f;
        h1.f = g;
        h1.g = h;
        h1.h = i;
        e = e + 1;
    }

    public void a(SparseArray sparsearray, SparseArray sparsearray1)
    {
        if (b.p.a()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        h h1 = c;
_L12:
        if (h1 == null) goto _L1; else goto _L3
_L3:
        h1.c;
        JVM INSTR tableswitch 1 7: default 68
    //                   1 76
    //                   2 87
    //                   3 147
    //                   4 159
    //                   5 171
    //                   6 182
    //                   7 194;
           goto _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L4:
        break; /* Loop/switch isn't completed */
_L11:
        break MISSING_BLOCK_LABEL_194;
_L13:
        h1 = h1.a;
          goto _L12
_L5:
        a(sparsearray, h1.d);
          goto _L13
_L6:
        if (h1.i != null)
        {
            for (int i1 = h1.i.size() - 1; i1 >= 0; i1--)
            {
                b(sparsearray1, (n)h1.i.get(i1));
            }

        }
        a(sparsearray, h1.d);
          goto _L13
_L7:
        b(sparsearray1, h1.d);
          goto _L13
_L8:
        b(sparsearray1, h1.d);
          goto _L13
_L9:
        a(sparsearray, h1.d);
          goto _L13
_L10:
        b(sparsearray1, h1.d);
          goto _L13
        a(sparsearray, h1.d);
          goto _L13
    }

    public void a(String s1, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        a(s1, printwriter, true);
    }

    public void a(String s1, PrintWriter printwriter, boolean flag)
    {
        h h1;
        String s3;
        int i1;
        if (flag)
        {
            printwriter.print(s1);
            printwriter.print("mName=");
            printwriter.print(n);
            printwriter.print(" mIndex=");
            printwriter.print(p);
            printwriter.print(" mCommitted=");
            printwriter.println(o);
            if (j != 0)
            {
                printwriter.print(s1);
                printwriter.print("mTransition=#");
                printwriter.print(Integer.toHexString(j));
                printwriter.print(" mTransitionStyle=#");
                printwriter.println(Integer.toHexString(k));
            }
            if (f != 0 || g != 0)
            {
                printwriter.print(s1);
                printwriter.print("mEnterAnim=#");
                printwriter.print(Integer.toHexString(f));
                printwriter.print(" mExitAnim=#");
                printwriter.println(Integer.toHexString(g));
            }
            if (h != 0 || i != 0)
            {
                printwriter.print(s1);
                printwriter.print("mPopEnterAnim=#");
                printwriter.print(Integer.toHexString(h));
                printwriter.print(" mPopExitAnim=#");
                printwriter.println(Integer.toHexString(i));
            }
            if (q != 0 || r != null)
            {
                printwriter.print(s1);
                printwriter.print("mBreadCrumbTitleRes=#");
                printwriter.print(Integer.toHexString(q));
                printwriter.print(" mBreadCrumbTitleText=");
                printwriter.println(r);
            }
            if (s != 0 || t != null)
            {
                printwriter.print(s1);
                printwriter.print("mBreadCrumbShortTitleRes=#");
                printwriter.print(Integer.toHexString(s));
                printwriter.print(" mBreadCrumbShortTitleText=");
                printwriter.println(t);
            }
        }
        if (c == null)
        {
            break MISSING_BLOCK_LABEL_823;
        }
        printwriter.print(s1);
        printwriter.println("Operations:");
        s3 = (new StringBuilder()).append(s1).append("    ").toString();
        h1 = c;
        i1 = 0;
_L13:
        if (h1 == null) goto _L2; else goto _L1
_L1:
        h1.c;
        JVM INSTR tableswitch 0 7: default 424
    //                   0 702
    //                   1 710
    //                   2 718
    //                   3 726
    //                   4 734
    //                   5 742
    //                   6 750
    //                   7 758;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L11:
        break MISSING_BLOCK_LABEL_758;
_L3:
        String s2 = (new StringBuilder()).append("cmd=").append(h1.c).toString();
_L12:
        printwriter.print(s1);
        printwriter.print("  Op #");
        printwriter.print(i1);
        printwriter.print(": ");
        printwriter.print(s2);
        printwriter.print(" ");
        printwriter.println(h1.d);
        if (flag)
        {
            if (h1.e != 0 || h1.f != 0)
            {
                printwriter.print(s1);
                printwriter.print("enterAnim=#");
                printwriter.print(Integer.toHexString(h1.e));
                printwriter.print(" exitAnim=#");
                printwriter.println(Integer.toHexString(h1.f));
            }
            if (h1.g != 0 || h1.h != 0)
            {
                printwriter.print(s1);
                printwriter.print("popEnterAnim=#");
                printwriter.print(Integer.toHexString(h1.g));
                printwriter.print(" popExitAnim=#");
                printwriter.println(Integer.toHexString(h1.h));
            }
        }
        if (h1.i != null && h1.i.size() > 0)
        {
            int j1 = 0;
            while (j1 < h1.i.size()) 
            {
                printwriter.print(s3);
                if (h1.i.size() == 1)
                {
                    printwriter.print("Removed: ");
                } else
                {
                    if (j1 == 0)
                    {
                        printwriter.println("Removed:");
                    }
                    printwriter.print(s3);
                    printwriter.print("  #");
                    printwriter.print(j1);
                    printwriter.print(": ");
                }
                printwriter.println(h1.i.get(j1));
                j1++;
            }
        }
        break MISSING_BLOCK_LABEL_807;
_L4:
        s2 = "NULL";
          goto _L12
_L5:
        s2 = "ADD";
          goto _L12
_L6:
        s2 = "REPLACE";
          goto _L12
_L7:
        s2 = "REMOVE";
          goto _L12
_L8:
        s2 = "HIDE";
          goto _L12
_L9:
        s2 = "SHOW";
          goto _L12
_L10:
        s2 = "DETACH";
          goto _L12
        s2 = "ATTACH";
          goto _L12
        h1 = h1.a;
        i1++;
          goto _L13
_L2:
    }

    public int b()
    {
        return a(true);
    }

    public String c()
    {
        return n;
    }

    public void run()
    {
        if (android.support.v4.app.z.a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Run: ").append(this).toString());
        }
        if (l && p < 0)
        {
            throw new IllegalStateException("addToBackStack() called after commit()");
        }
        a(1);
        n n1;
        Object obj;
        h h1;
        n n2;
        n n3;
        int i1;
        int j1;
        int k1;
        int l1;
        int i2;
        int j2;
        if (a)
        {
            SparseArray sparsearray = new SparseArray();
            obj = new SparseArray();
            b(sparsearray, ((SparseArray) (obj)));
            obj = a(sparsearray, ((SparseArray) (obj)), false);
        } else
        {
            obj = null;
        }
        if (obj != null)
        {
            i1 = 0;
        } else
        {
            i1 = k;
        }
        if (obj != null)
        {
            j1 = 0;
        } else
        {
            j1 = j;
        }
        h1 = c;
        if (h1 == null)
        {
            break MISSING_BLOCK_LABEL_700;
        }
        if (obj != null)
        {
            k1 = 0;
        } else
        {
            k1 = h1.e;
        }
        if (obj != null)
        {
            l1 = 0;
        } else
        {
            l1 = h1.f;
        }
        h1.c;
        JVM INSTR tableswitch 1 7: default 184
    //                   1 251
    //                   2 279
    //                   3 570
    //                   4 596
    //                   5 622
    //                   6 648
    //                   7 674;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8
_L8:
        break MISSING_BLOCK_LABEL_674;
_L3:
        break; /* Loop/switch isn't completed */
_L1:
        throw new IllegalArgumentException((new StringBuilder()).append("Unknown cmd: ").append(h1.c).toString());
_L2:
        n1 = h1.d;
        n1.R = k1;
        b.a(n1, false);
_L12:
        h1 = h1.a;
        if (true) goto _L10; else goto _L9
_L10:
        break MISSING_BLOCK_LABEL_118;
_L9:
        n1 = h1.d;
        j2 = n1.H;
        if (b.g != null)
        {
            i2 = 0;
            do
            {
                n2 = n1;
                if (i2 >= b.g.size())
                {
                    break;
                }
                n3 = (n)b.g.get(i2);
                if (android.support.v4.app.z.a)
                {
                    Log.v("FragmentManager", (new StringBuilder()).append("OP_REPLACE: adding=").append(n1).append(" old=").append(n3).toString());
                }
                n2 = n1;
                if (n3.H == j2)
                {
                    if (n3 == n1)
                    {
                        n2 = null;
                        h1.d = null;
                    } else
                    {
                        if (h1.i == null)
                        {
                            h1.i = new ArrayList();
                        }
                        h1.i.add(n3);
                        n3.R = l1;
                        if (l)
                        {
                            n3.B = n3.B + 1;
                            if (android.support.v4.app.z.a)
                            {
                                Log.v("FragmentManager", (new StringBuilder()).append("Bump nesting of ").append(n3).append(" to ").append(n3.B).toString());
                            }
                        }
                        b.a(n3, j1, i1);
                        n2 = n1;
                    }
                }
                i2++;
                n1 = n2;
            } while (true);
        } else
        {
            n2 = n1;
        }
        if (n2 != null)
        {
            n2.R = k1;
            b.a(n2, false);
        }
        continue; /* Loop/switch isn't completed */
_L4:
        n1 = h1.d;
        n1.R = l1;
        b.a(n1, j1, i1);
        continue; /* Loop/switch isn't completed */
_L5:
        n1 = h1.d;
        n1.R = l1;
        b.b(n1, j1, i1);
        continue; /* Loop/switch isn't completed */
_L6:
        n1 = h1.d;
        n1.R = k1;
        b.c(n1, j1, i1);
        continue; /* Loop/switch isn't completed */
_L7:
        n1 = h1.d;
        n1.R = l1;
        b.d(n1, j1, i1);
        continue; /* Loop/switch isn't completed */
        n1 = h1.d;
        n1.R = k1;
        b.e(n1, j1, i1);
        if (true) goto _L12; else goto _L11
_L11:
        b.a(b.n, j1, i1, true);
        if (l)
        {
            b.b(this);
        }
        return;
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(128);
        stringbuilder.append("BackStackEntry{");
        stringbuilder.append(Integer.toHexString(System.identityHashCode(this)));
        if (p >= 0)
        {
            stringbuilder.append(" #");
            stringbuilder.append(p);
        }
        if (n != null)
        {
            stringbuilder.append(" ");
            stringbuilder.append(n);
        }
        stringbuilder.append("}");
        return stringbuilder.toString();
    }

    static 
    {
        boolean flag;
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        a = flag;
    }
}
