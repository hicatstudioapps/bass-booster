// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.support.v4.b.d;
import android.support.v4.b.m;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

// Referenced classes of package android.support.v4.app:
//            ar, au, w

class at extends ar
{

    static boolean a = false;
    final m b = new m();
    final m c = new m();
    final String d;
    boolean e;
    boolean f;
    private w g;

    at(String s, w w, boolean flag)
    {
        d = s;
        g = w;
        e = flag;
    }

    static w a(at at1)
    {
        return at1.g;
    }

    void a(w w)
    {
        g = w;
    }

    public void a(String s, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        boolean flag = false;
        if (b.b() > 0)
        {
            printwriter.print(s);
            printwriter.println("Active Loaders:");
            String s1 = (new StringBuilder()).append(s).append("    ").toString();
            for (int i = 0; i < b.b(); i++)
            {
                au au1 = (au)b.b(i);
                printwriter.print(s);
                printwriter.print("  #");
                printwriter.print(b.a(i));
                printwriter.print(": ");
                printwriter.println(au1.toString());
                au1.a(s1, filedescriptor, printwriter, as);
            }

        }
        if (c.b() > 0)
        {
            printwriter.print(s);
            printwriter.println("Inactive Loaders:");
            String s2 = (new StringBuilder()).append(s).append("    ").toString();
            for (int j = ((flag) ? 1 : 0); j < c.b(); j++)
            {
                au au2 = (au)c.b(j);
                printwriter.print(s);
                printwriter.print("  #");
                printwriter.print(c.a(j));
                printwriter.print(": ");
                printwriter.println(au2.toString());
                au2.a(s2, filedescriptor, printwriter, as);
            }

        }
    }

    public boolean a()
    {
        int j = b.b();
        int i = 0;
        boolean flag1 = false;
        while (i < j) 
        {
            au au1 = (au)b.b(i);
            boolean flag;
            if (au1.h && !au1.f)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            flag1 |= flag;
            i++;
        }
        return flag1;
    }

    void b()
    {
        if (a)
        {
            Log.v("LoaderManager", (new StringBuilder()).append("Starting in ").append(this).toString());
        }
        if (e)
        {
            RuntimeException runtimeexception = new RuntimeException("here");
            runtimeexception.fillInStackTrace();
            Log.w("LoaderManager", (new StringBuilder()).append("Called doStart when already started: ").append(this).toString(), runtimeexception);
        } else
        {
            e = true;
            int i = b.b() - 1;
            while (i >= 0) 
            {
                ((au)b.b(i)).a();
                i--;
            }
        }
    }

    void c()
    {
        if (a)
        {
            Log.v("LoaderManager", (new StringBuilder()).append("Stopping in ").append(this).toString());
        }
        if (!e)
        {
            RuntimeException runtimeexception = new RuntimeException("here");
            runtimeexception.fillInStackTrace();
            Log.w("LoaderManager", (new StringBuilder()).append("Called doStop when not started: ").append(this).toString(), runtimeexception);
            return;
        }
        for (int i = b.b() - 1; i >= 0; i--)
        {
            ((au)b.b(i)).e();
        }

        e = false;
    }

    void d()
    {
        if (a)
        {
            Log.v("LoaderManager", (new StringBuilder()).append("Retaining in ").append(this).toString());
        }
        if (!e)
        {
            RuntimeException runtimeexception = new RuntimeException("here");
            runtimeexception.fillInStackTrace();
            Log.w("LoaderManager", (new StringBuilder()).append("Called doRetain when not started: ").append(this).toString(), runtimeexception);
        } else
        {
            f = true;
            e = false;
            int i = b.b() - 1;
            while (i >= 0) 
            {
                ((au)b.b(i)).b();
                i--;
            }
        }
    }

    void e()
    {
        if (f)
        {
            if (a)
            {
                Log.v("LoaderManager", (new StringBuilder()).append("Finished Retaining in ").append(this).toString());
            }
            f = false;
            for (int i = b.b() - 1; i >= 0; i--)
            {
                ((au)b.b(i)).c();
            }

        }
    }

    void f()
    {
        for (int i = b.b() - 1; i >= 0; i--)
        {
            ((au)b.b(i)).k = true;
        }

    }

    void g()
    {
        for (int i = b.b() - 1; i >= 0; i--)
        {
            ((au)b.b(i)).d();
        }

    }

    void h()
    {
        if (!f)
        {
            if (a)
            {
                Log.v("LoaderManager", (new StringBuilder()).append("Destroying Active in ").append(this).toString());
            }
            for (int i = b.b() - 1; i >= 0; i--)
            {
                ((au)b.b(i)).f();
            }

            b.c();
        }
        if (a)
        {
            Log.v("LoaderManager", (new StringBuilder()).append("Destroying Inactive in ").append(this).toString());
        }
        for (int j = c.b() - 1; j >= 0; j--)
        {
            ((au)c.b(j)).f();
        }

        c.c();
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(128);
        stringbuilder.append("LoaderManager{");
        stringbuilder.append(Integer.toHexString(System.identityHashCode(this)));
        stringbuilder.append(" in ");
        android.support.v4.b.d.a(g, stringbuilder);
        stringbuilder.append("}}");
        return stringbuilder.toString();
    }

}
