// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.b.d;
import android.support.v4.b.e;
import android.support.v4.c.k;
import android.support.v4.c.l;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Referenced classes of package android.support.v4.app:
//            x, aa, w, ad, 
//            d, n, ag, ab, 
//            at, FragmentManagerState, FragmentState, BackStackState, 
//            cw, u, ba, ac, 
//            y, aj

final class z extends x
    implements k
{

    static final Interpolator A = new DecelerateInterpolator(2.5F);
    static final Interpolator B = new DecelerateInterpolator(1.5F);
    static final Interpolator C = new AccelerateInterpolator(2.5F);
    static final Interpolator D = new AccelerateInterpolator(1.5F);
    static boolean a = false;
    static final boolean b;
    static Field r = null;
    ArrayList c;
    Runnable d[];
    boolean e;
    ArrayList f;
    ArrayList g;
    ArrayList h;
    ArrayList i;
    ArrayList j;
    ArrayList k;
    ArrayList l;
    ArrayList m;
    int n;
    w o;
    u p;
    n q;
    boolean s;
    boolean t;
    boolean u;
    String v;
    boolean w;
    Bundle x;
    SparseArray y;
    Runnable z;

    z()
    {
        n = 0;
        x = null;
        y = null;
        z = new aa(this);
    }

    static Animation a(Context context, float f1, float f2)
    {
        context = new AlphaAnimation(f1, f2);
        context.setInterpolator(B);
        context.setDuration(220L);
        return context;
    }

    static Animation a(Context context, float f1, float f2, float f3, float f4)
    {
        context = new AnimationSet(false);
        Object obj = new ScaleAnimation(f1, f2, f1, f2, 1, 0.5F, 1, 0.5F);
        ((ScaleAnimation) (obj)).setInterpolator(A);
        ((ScaleAnimation) (obj)).setDuration(220L);
        context.addAnimation(((Animation) (obj)));
        obj = new AlphaAnimation(f3, f4);
        ((AlphaAnimation) (obj)).setInterpolator(B);
        ((AlphaAnimation) (obj)).setDuration(220L);
        context.addAnimation(((Animation) (obj)));
        return context;
    }

    private void a(RuntimeException runtimeexception)
    {
        Log.e("FragmentManager", runtimeexception.getMessage());
        Log.e("FragmentManager", "Activity state:");
        Object obj = new PrintWriter(new e("FragmentManager"));
        if (o != null)
        {
            try
            {
                o.a("  ", null, ((PrintWriter) (obj)), new String[0]);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                Log.e("FragmentManager", "Failed dumping state", ((Throwable) (obj)));
            }
        } else
        {
            try
            {
                a("  ", ((FileDescriptor) (null)), ((PrintWriter) (obj)), new String[0]);
            }
            catch (Exception exception)
            {
                Log.e("FragmentManager", "Failed dumping state", exception);
            }
        }
        throw runtimeexception;
    }

    static boolean a(View view, Animation animation)
    {
        return android.os.Build.VERSION.SDK_INT >= 19 && android.support.v4.c.l.a(view) == 0 && android.support.v4.c.l.b(view) && a(animation);
    }

    static boolean a(Animation animation)
    {
        boolean flag1 = false;
        if (!(animation instanceof AlphaAnimation)) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        flag = flag1;
        if (!(animation instanceof AnimationSet))
        {
            continue;
        }
        animation = ((AnimationSet)animation).getAnimations();
        int i1 = 0;
        do
        {
            flag = flag1;
            if (i1 >= animation.size())
            {
                continue;
            }
            if (animation.get(i1) instanceof AlphaAnimation)
            {
                return true;
            }
            i1++;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static int b(int i1, boolean flag)
    {
        switch (i1)
        {
        default:
            return -1;

        case 4097: 
            return !flag ? 2 : 1;

        case 8194: 
            return !flag ? 4 : 3;

        case 4099: 
            break;
        }
        return !flag ? 6 : 5;
    }

    private void b(View view, Animation animation)
    {
        while (view == null || animation == null || !a(view, animation)) 
        {
            return;
        }
        android.view.animation.Animation.AnimationListener animationlistener;
        try
        {
            if (r == null)
            {
                r = android/view/animation/Animation.getDeclaredField("mListener");
                r.setAccessible(true);
            }
            animationlistener = (android.view.animation.Animation.AnimationListener)r.get(animation);
        }
        catch (NoSuchFieldException nosuchfieldexception)
        {
            Log.e("FragmentManager", "No field with the name mListener is found in Animation class", nosuchfieldexception);
            nosuchfieldexception = null;
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            Log.e("FragmentManager", "Cannot access Animation's mListener field", illegalaccessexception);
            illegalaccessexception = null;
        }
        animation.setAnimationListener(new ad(view, animation, animationlistener));
    }

    public static int c(int i1)
    {
        switch (i1)
        {
        default:
            return 0;

        case 4097: 
            return 8194;

        case 8194: 
            return 4097;

        case 4099: 
            return 4099;
        }
    }

    private void w()
    {
        if (t)
        {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        }
        if (v != null)
        {
            throw new IllegalStateException((new StringBuilder()).append("Can not perform this action inside of ").append(v).toString());
        } else
        {
            return;
        }
    }

    public int a(android.support.v4.app.d d1)
    {
        this;
        JVM INSTR monitorenter ;
        int i1;
        if (l != null && l.size() > 0)
        {
            break MISSING_BLOCK_LABEL_100;
        }
        if (k == null)
        {
            k = new ArrayList();
        }
        i1 = k.size();
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Setting back stack index ").append(i1).append(" to ").append(d1).toString());
        }
        k.add(d1);
        this;
        JVM INSTR monitorexit ;
        return i1;
        i1 = ((Integer)l.remove(l.size() - 1)).intValue();
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Adding back stack index ").append(i1).append(" with ").append(d1).toString());
        }
        k.set(i1, d1);
        this;
        JVM INSTR monitorexit ;
        return i1;
        d1;
        this;
        JVM INSTR monitorexit ;
        throw d1;
    }

    public aj a()
    {
        return new android.support.v4.app.d(this);
    }

    public n a(int i1)
    {
        if (g == null) goto _L2; else goto _L1
_L1:
        int j1 = g.size() - 1;
_L11:
        if (j1 < 0) goto _L2; else goto _L3
_L3:
        n n1 = (n)g.get(j1);
        if (n1 == null || n1.G != i1) goto _L5; else goto _L4
_L4:
        return n1;
_L5:
        j1--;
        continue; /* Loop/switch isn't completed */
_L2:
        if (f == null)
        {
            break MISSING_BLOCK_LABEL_118;
        }
        j1 = f.size() - 1;
_L9:
        if (j1 < 0) goto _L7; else goto _L6
_L6:
        n n2;
        n2 = (n)f.get(j1);
        if (n2 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        n1 = n2;
        if (n2.G == i1) goto _L4; else goto _L8
_L8:
        j1--;
          goto _L9
_L7:
        return null;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public n a(Bundle bundle, String s1)
    {
        int i1 = bundle.getInt(s1, -1);
        if (i1 == -1)
        {
            bundle = null;
        } else
        {
            if (i1 >= f.size())
            {
                a(((RuntimeException) (new IllegalStateException((new StringBuilder()).append("Fragment no longer exists for key ").append(s1).append(": index ").append(i1).toString()))));
            }
            n n1 = (n)f.get(i1);
            bundle = n1;
            if (n1 == null)
            {
                a(((RuntimeException) (new IllegalStateException((new StringBuilder()).append("Fragment no longer exists for key ").append(s1).append(": index ").append(i1).toString()))));
                return n1;
            }
        }
        return bundle;
    }

    public n a(String s1)
    {
        if (g == null || s1 == null) goto _L2; else goto _L1
_L1:
        int i1 = g.size() - 1;
_L11:
        if (i1 < 0) goto _L2; else goto _L3
_L3:
        n n1 = (n)g.get(i1);
        if (n1 == null || !s1.equals(n1.I)) goto _L5; else goto _L4
_L4:
        return n1;
_L5:
        i1--;
        continue; /* Loop/switch isn't completed */
_L2:
        if (f == null || s1 == null)
        {
            break MISSING_BLOCK_LABEL_132;
        }
        i1 = f.size() - 1;
_L9:
        if (i1 < 0) goto _L7; else goto _L6
_L6:
        n n2;
        n2 = (n)f.get(i1);
        if (n2 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        n1 = n2;
        if (s1.equals(n2.I)) goto _L4; else goto _L8
_L8:
        i1--;
          goto _L9
_L7:
        return null;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public View a(View view, String s1, Context context, AttributeSet attributeset)
    {
        if (!"fragment".equals(s1))
        {
            return null;
        }
        String s2 = attributeset.getAttributeValue(null, "class");
        s1 = context.obtainStyledAttributes(attributeset, ag.a);
        if (s2 == null)
        {
            s2 = s1.getString(0);
        }
        int k1 = s1.getResourceId(1, -1);
        String s3 = s1.getString(2);
        s1.recycle();
        if (!android.support.v4.app.n.b(o.g(), s2))
        {
            return null;
        }
        int i1;
        if (view != null)
        {
            i1 = view.getId();
        } else
        {
            i1 = 0;
        }
        if (i1 == -1 && k1 == -1 && s3 == null)
        {
            throw new IllegalArgumentException((new StringBuilder()).append(attributeset.getPositionDescription()).append(": Must specify unique android:id, android:tag, or have a parent with an id for ").append(s2).toString());
        }
        if (k1 != -1)
        {
            s1 = a(k1);
        } else
        {
            s1 = null;
        }
        view = s1;
        if (s1 == null)
        {
            view = s1;
            if (s3 != null)
            {
                view = a(s3);
            }
        }
        s1 = view;
        if (view == null)
        {
            s1 = view;
            if (i1 != -1)
            {
                s1 = a(i1);
            }
        }
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("onCreateView: id=0x").append(Integer.toHexString(k1)).append(" fname=").append(s2).append(" existing=").append(s1).toString());
        }
        if (s1 == null)
        {
            view = android.support.v4.app.n.a(context, s2);
            view.y = true;
            int j1;
            if (k1 != 0)
            {
                j1 = k1;
            } else
            {
                j1 = i1;
            }
            view.G = j1;
            view.H = i1;
            view.I = s3;
            view.z = true;
            view.C = this;
            view.D = o;
            view.a(o.g(), attributeset, ((n) (view)).n);
            a(((n) (view)), true);
        } else
        {
            if (((n) (s1)).z)
            {
                throw new IllegalArgumentException((new StringBuilder()).append(attributeset.getPositionDescription()).append(": Duplicate id 0x").append(Integer.toHexString(k1)).append(", tag ").append(s3).append(", or parent id 0x").append(Integer.toHexString(i1)).append(" with another fragment for ").append(s2).toString());
            }
            s1.z = true;
            if (!((n) (s1)).M)
            {
                s1.a(o.g(), attributeset, ((n) (s1)).n);
            }
            view = s1;
        }
        if (n < 1 && ((n) (view)).y)
        {
            a(((n) (view)), 1, 0, 0, false);
        } else
        {
            b(view);
        }
        if (((n) (view)).T == null)
        {
            throw new IllegalStateException((new StringBuilder()).append("Fragment ").append(s2).append(" did not create a view.").toString());
        }
        if (k1 != 0)
        {
            ((n) (view)).T.setId(k1);
        }
        if (((n) (view)).T.getTag() == null)
        {
            ((n) (view)).T.setTag(s3);
        }
        return ((n) (view)).T;
    }

    Animation a(n n1, int i1, boolean flag, int j1)
    {
        Animation animation = n1.a(i1, flag, n1.R);
        if (animation == null) goto _L2; else goto _L1
_L1:
        n1 = animation;
_L4:
        return n1;
_L2:
        if (n1.R == 0)
        {
            break; /* Loop/switch isn't completed */
        }
        animation = AnimationUtils.loadAnimation(o.g(), n1.R);
        n1 = animation;
        if (animation != null) goto _L4; else goto _L3
_L3:
        if (i1 == 0)
        {
            return null;
        }
        i1 = b(i1, flag);
        if (i1 < 0)
        {
            return null;
        }
        switch (i1)
        {
        default:
            i1 = j1;
            if (j1 == 0)
            {
                i1 = j1;
                if (o.d())
                {
                    i1 = o.e();
                }
            }
            if (i1 == 0)
            {
                return null;
            } else
            {
                return null;
            }

        case 1: // '\001'
            return a(o.g(), 1.125F, 1.0F, 0.0F, 1.0F);

        case 2: // '\002'
            return a(o.g(), 1.0F, 0.975F, 1.0F, 0.0F);

        case 3: // '\003'
            return a(o.g(), 0.975F, 1.0F, 0.0F, 1.0F);

        case 4: // '\004'
            return a(o.g(), 1.0F, 1.075F, 1.0F, 0.0F);

        case 5: // '\005'
            return a(o.g(), 0.0F, 1.0F);

        case 6: // '\006'
            return a(o.g(), 1.0F, 0.0F);
        }
    }

    public void a(int i1, int j1)
    {
        if (i1 < 0)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Bad id: ").append(i1).toString());
        } else
        {
            a(((Runnable) (new ab(this, i1, j1))), false);
            return;
        }
    }

    void a(int i1, int j1, int k1, boolean flag)
    {
        if (o == null && i1 != 0)
        {
            throw new IllegalStateException("No host");
        }
        if (flag || n != i1)
        {
            n = i1;
            if (f != null)
            {
                int l1 = 0;
                boolean flag1 = false;
                for (; l1 < f.size(); l1++)
                {
                    n n1 = (n)f.get(l1);
                    if (n1 == null)
                    {
                        continue;
                    }
                    a(n1, i1, j1, k1, false);
                    if (n1.X != null)
                    {
                        flag1 |= n1.X.a();
                    }
                }

                if (!flag1)
                {
                    f();
                }
                if (s && o != null && n == 5)
                {
                    o.c();
                    s = false;
                    return;
                }
            }
        }
    }

    public void a(int i1, android.support.v4.app.d d1)
    {
        this;
        JVM INSTR monitorenter ;
        int k1;
        if (k == null)
        {
            k = new ArrayList();
        }
        k1 = k.size();
        int j1 = k1;
        if (i1 >= k1) goto _L2; else goto _L1
_L1:
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Setting back stack index ").append(i1).append(" to ").append(d1).toString());
        }
        k.set(i1, d1);
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (j1 >= i1)
        {
            break; /* Loop/switch isn't completed */
        }
        k.add(null);
        if (l == null)
        {
            l = new ArrayList();
        }
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Adding available back stack index ").append(j1).toString());
        }
        l.add(Integer.valueOf(j1));
        j1++;
        if (true) goto _L2; else goto _L3
_L3:
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Adding back stack index ").append(i1).append(" with ").append(d1).toString());
        }
        k.add(d1);
          goto _L4
        d1;
        this;
        JVM INSTR monitorexit ;
        throw d1;
    }

    void a(int i1, boolean flag)
    {
        a(i1, 0, 0, flag);
    }

    public void a(Configuration configuration)
    {
        if (g != null)
        {
            for (int i1 = 0; i1 < g.size(); i1++)
            {
                n n1 = (n)g.get(i1);
                if (n1 != null)
                {
                    n1.a(configuration);
                }
            }

        }
    }

    public void a(Bundle bundle, String s1, n n1)
    {
        if (n1.p < 0)
        {
            a(((RuntimeException) (new IllegalStateException((new StringBuilder()).append("Fragment ").append(n1).append(" is not currently in the FragmentManager").toString()))));
        }
        bundle.putInt(s1, n1.p);
    }

    void a(Parcelable parcelable, List list)
    {
        if (parcelable != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (((FragmentManagerState) (parcelable = (FragmentManagerState)parcelable)).a != null)
        {
            if (list != null)
            {
                for (int i1 = 0; i1 < list.size(); i1++)
                {
                    n n1 = (n)list.get(i1);
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("restoreAllState: re-attaching retained ").append(n1).toString());
                    }
                    FragmentState fragmentstate1 = ((FragmentManagerState) (parcelable)).a[n1.p];
                    fragmentstate1.k = n1;
                    n1.o = null;
                    n1.B = 0;
                    n1.z = false;
                    n1.v = false;
                    n1.s = null;
                    if (fragmentstate1.j != null)
                    {
                        fragmentstate1.j.setClassLoader(o.g().getClassLoader());
                        n1.o = fragmentstate1.j.getSparseParcelableArray("android:view_state");
                        n1.n = fragmentstate1.j;
                    }
                }

            }
            f = new ArrayList(((FragmentManagerState) (parcelable)).a.length);
            if (h != null)
            {
                h.clear();
            }
            int j1 = 0;
            while (j1 < ((FragmentManagerState) (parcelable)).a.length) 
            {
                FragmentState fragmentstate = ((FragmentManagerState) (parcelable)).a[j1];
                if (fragmentstate != null)
                {
                    n n3 = fragmentstate.a(o, q);
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("restoreAllState: active #").append(j1).append(": ").append(n3).toString());
                    }
                    f.add(n3);
                    fragmentstate.k = null;
                } else
                {
                    f.add(null);
                    if (h == null)
                    {
                        h = new ArrayList();
                    }
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("restoreAllState: avail #").append(j1).toString());
                    }
                    h.add(Integer.valueOf(j1));
                }
                j1++;
            }
            if (list != null)
            {
                int k1 = 0;
                while (k1 < list.size()) 
                {
                    n n2 = (n)list.get(k1);
                    if (n2.t >= 0)
                    {
                        if (n2.t < f.size())
                        {
                            n2.s = (n)f.get(n2.t);
                        } else
                        {
                            Log.w("FragmentManager", (new StringBuilder()).append("Re-attaching retained fragment ").append(n2).append(" target no longer exists: ").append(n2.t).toString());
                            n2.s = null;
                        }
                    }
                    k1++;
                }
            }
            if (((FragmentManagerState) (parcelable)).b != null)
            {
                g = new ArrayList(((FragmentManagerState) (parcelable)).b.length);
                for (int l1 = 0; l1 < ((FragmentManagerState) (parcelable)).b.length; l1++)
                {
                    list = (n)f.get(((FragmentManagerState) (parcelable)).b[l1]);
                    if (list == null)
                    {
                        a(((RuntimeException) (new IllegalStateException((new StringBuilder()).append("No instantiated fragment for index #").append(((FragmentManagerState) (parcelable)).b[l1]).toString()))));
                    }
                    list.v = true;
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("restoreAllState: added #").append(l1).append(": ").append(list).toString());
                    }
                    if (g.contains(list))
                    {
                        throw new IllegalStateException("Already added!");
                    }
                    g.add(list);
                }

            } else
            {
                g = null;
            }
            if (((FragmentManagerState) (parcelable)).c != null)
            {
                i = new ArrayList(((FragmentManagerState) (parcelable)).c.length);
                int i2 = 0;
                while (i2 < ((FragmentManagerState) (parcelable)).c.length) 
                {
                    list = ((FragmentManagerState) (parcelable)).c[i2].a(this);
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("restoreAllState: back stack #").append(i2).append(" (index ").append(((android.support.v4.app.d) (list)).p).append("): ").append(list).toString());
                        list.a("  ", new PrintWriter(new e("FragmentManager")), false);
                    }
                    i.add(list);
                    if (((android.support.v4.app.d) (list)).p >= 0)
                    {
                        a(((android.support.v4.app.d) (list)).p, ((android.support.v4.app.d) (list)));
                    }
                    i2++;
                }
            } else
            {
                i = null;
                return;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void a(n n1)
    {
label0:
        {
            if (n1.V)
            {
                if (!e)
                {
                    break label0;
                }
                w = true;
            }
            return;
        }
        n1.V = false;
        a(n1, n, 0, 0, false);
    }

    public void a(n n1, int i1, int j1)
    {
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("remove: ").append(n1).append(" nesting=").append(n1.B).toString());
        }
        int k1;
        if (!n1.f())
        {
            k1 = 1;
        } else
        {
            k1 = 0;
        }
        if (!n1.K || k1 != 0)
        {
            if (g != null)
            {
                g.remove(n1);
            }
            if (n1.O && n1.P)
            {
                s = true;
            }
            n1.v = false;
            n1.w = true;
            if (k1 != 0)
            {
                k1 = 0;
            } else
            {
                k1 = 1;
            }
            a(n1, k1, i1, j1, false);
        }
    }

    void a(n n1, int i1, int j1, int k1, boolean flag)
    {
        int l1;
label0:
        {
            if (n1.v)
            {
                l1 = i1;
                if (!n1.K)
                {
                    break label0;
                }
            }
            l1 = i1;
            if (i1 > 1)
            {
                l1 = 1;
            }
        }
        int j2 = l1;
        if (n1.w)
        {
            j2 = l1;
            if (l1 > n1.k)
            {
                j2 = n1.k;
            }
        }
        i1 = j2;
        if (n1.V)
        {
            i1 = j2;
            if (n1.k < 4)
            {
                i1 = j2;
                if (j2 > 3)
                {
                    i1 = 3;
                }
            }
        }
        if (n1.k >= i1) goto _L2; else goto _L1
_L1:
        int i2;
        int k2;
        int l2;
        if (n1.y && !n1.z)
        {
            return;
        }
        if (n1.l != null)
        {
            n1.l = null;
            a(n1, n1.m, 0, 0, true);
        }
        i2 = i1;
        l2 = i1;
        k2 = i1;
        n1.k;
        JVM INSTR tableswitch 0 4: default 184
    //                   0 194
    //                   1 582
    //                   2 913
    //                   3 913
    //                   4 963;
           goto _L3 _L4 _L5 _L6 _L6 _L7
_L3:
        i2 = i1;
_L16:
        n1.k = i2;
        return;
_L4:
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("moveto CREATED: ").append(n1).toString());
        }
        k2 = i1;
        if (n1.n != null)
        {
            n1.n.setClassLoader(o.g().getClassLoader());
            n1.o = n1.n.getSparseParcelableArray("android:view_state");
            n1.s = a(n1.n, "android:target_state");
            if (n1.s != null)
            {
                n1.u = n1.n.getInt("android:target_req_state", 0);
            }
            n1.W = n1.n.getBoolean("android:user_visible_hint", true);
            k2 = i1;
            if (!n1.W)
            {
                n1.V = true;
                k2 = i1;
                if (i1 > 3)
                {
                    k2 = 3;
                }
            }
        }
        n1.D = o;
        n1.F = q;
        z z1;
        if (q != null)
        {
            z1 = q.E;
        } else
        {
            z1 = o.i();
        }
        n1.C = z1;
        n1.Q = false;
        n1.a(o.g());
        if (!n1.Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(n1).append(" did not call through to super.onAttach()").toString());
        }
        if (n1.F == null)
        {
            o.b(n1);
        }
        if (!n1.M)
        {
            n1.h(n1.n);
        }
        n1.M = false;
        i2 = k2;
        if (n1.y)
        {
            n1.T = n1.b(n1.b(n1.n), null, n1.n);
            if (n1.T != null)
            {
                n1.U = n1.T;
                Object obj1;
                if (android.os.Build.VERSION.SDK_INT >= 11)
                {
                    android.support.v4.c.l.a(n1.T, false);
                } else
                {
                    n1.T = ba.a(n1.T);
                }
                if (n1.J)
                {
                    n1.T.setVisibility(8);
                }
                n1.a(n1.T, n1.n);
                i2 = k2;
            } else
            {
                n1.U = null;
                i2 = k2;
            }
        }
_L5:
        l2 = i2;
        if (i2 > 1)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("moveto ACTIVITY_CREATED: ").append(n1).toString());
            }
            if (!n1.y)
            {
                Object obj;
                if (n1.H != 0)
                {
                    obj1 = (ViewGroup)p.a(n1.H);
                    obj = obj1;
                    if (obj1 == null)
                    {
                        obj = obj1;
                        if (!n1.A)
                        {
                            a(((RuntimeException) (new IllegalArgumentException((new StringBuilder()).append("No view found for id 0x").append(Integer.toHexString(n1.H)).append(" (").append(n1.h().getResourceName(n1.H)).append(") for fragment ").append(n1).toString()))));
                            obj = obj1;
                        }
                    }
                } else
                {
                    obj = null;
                }
                n1.S = ((ViewGroup) (obj));
                n1.T = n1.b(n1.b(n1.n), ((ViewGroup) (obj)), n1.n);
                if (n1.T != null)
                {
                    n1.U = n1.T;
                    if (android.os.Build.VERSION.SDK_INT >= 11)
                    {
                        android.support.v4.c.l.a(n1.T, false);
                    } else
                    {
                        n1.T = ba.a(n1.T);
                    }
                    if (obj != null)
                    {
                        obj1 = a(n1, j1, true, k1);
                        if (obj1 != null)
                        {
                            b(n1.T, ((Animation) (obj1)));
                            n1.T.startAnimation(((Animation) (obj1)));
                        }
                        ((ViewGroup) (obj)).addView(n1.T);
                    }
                    if (n1.J)
                    {
                        n1.T.setVisibility(8);
                    }
                    n1.a(n1.T, n1.n);
                } else
                {
                    n1.U = null;
                }
            }
            n1.i(n1.n);
            if (n1.T != null)
            {
                n1.f(n1.n);
            }
            n1.n = null;
            l2 = i2;
        }
_L6:
        k2 = l2;
        if (l2 > 3)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("moveto STARTED: ").append(n1).toString());
            }
            n1.C();
            k2 = l2;
        }
_L7:
        i2 = k2;
        if (k2 > 4)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("moveto RESUMED: ").append(n1).toString());
            }
            n1.x = true;
            n1.D();
            n1.n = null;
            n1.o = null;
            i2 = k2;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        i2 = i1;
        if (n1.k <= i1)
        {
            continue; /* Loop/switch isn't completed */
        }
        n1.k;
        JVM INSTR tableswitch 1 5: default 1128
    //                   1 1134
    //                   2 1318
    //                   3 1277
    //                   4 1236
    //                   5 1190;
           goto _L8 _L9 _L10 _L11 _L12 _L13
_L8:
        i2 = i1;
        break; /* Loop/switch isn't completed */
_L9:
        i2 = i1;
        if (i1 >= 1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (u && n1.l != null)
        {
            obj = n1.l;
            n1.l = null;
            ((View) (obj)).clearAnimation();
        }
        if (n1.l != null)
        {
            n1.m = i1;
            i2 = 1;
            break; /* Loop/switch isn't completed */
        }
          goto _L14
_L13:
        if (i1 < 5)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("movefrom RESUMED: ").append(n1).toString());
            }
            n1.F();
            n1.x = false;
        }
_L12:
        if (i1 < 4)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("movefrom STARTED: ").append(n1).toString());
            }
            n1.G();
        }
_L11:
        if (i1 < 3)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("movefrom STOPPED: ").append(n1).toString());
            }
            n1.H();
        }
_L10:
        if (i1 < 2)
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("movefrom ACTIVITY_CREATED: ").append(n1).toString());
            }
            if (n1.T != null && o.a(n1) && n1.o == null)
            {
                e(n1);
            }
            n1.I();
            if (n1.T != null && n1.S != null)
            {
                if (n > 0 && !u)
                {
                    obj = a(n1, j1, false, k1);
                } else
                {
                    obj = null;
                }
                if (obj != null)
                {
                    n1.l = n1.T;
                    n1.m = i1;
                    ((Animation) (obj)).setAnimationListener(new ac(this, n1.T, ((Animation) (obj)), n1));
                    n1.T.startAnimation(((Animation) (obj)));
                }
                n1.S.removeView(n1.T);
            }
            n1.S = null;
            n1.T = null;
            n1.U = null;
        }
        if (true) goto _L9; else goto _L14
_L14:
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("movefrom CREATED: ").append(n1).toString());
        }
        if (!n1.M)
        {
            n1.J();
        }
        n1.Q = false;
        n1.b();
        if (!n1.Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(n1).append(" did not call through to super.onDetach()").toString());
        }
        i2 = i1;
        if (!flag)
        {
            if (!n1.M)
            {
                d(n1);
                i2 = i1;
            } else
            {
                n1.D = null;
                n1.F = null;
                n1.C = null;
                n1.E = null;
                i2 = i1;
            }
        }
        if (true) goto _L16; else goto _L15
_L15:
    }

    public void a(n n1, boolean flag)
    {
        if (g == null)
        {
            g = new ArrayList();
        }
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("add: ").append(n1).toString());
        }
        c(n1);
        if (!n1.K)
        {
            if (g.contains(n1))
            {
                throw new IllegalStateException((new StringBuilder()).append("Fragment already added: ").append(n1).toString());
            }
            g.add(n1);
            n1.v = true;
            n1.w = false;
            if (n1.O && n1.P)
            {
                s = true;
            }
            if (flag)
            {
                b(n1);
            }
        }
    }

    public void a(w w1, u u1, n n1)
    {
        if (o != null)
        {
            throw new IllegalStateException("Already attached");
        } else
        {
            o = w1;
            p = u1;
            q = n1;
            return;
        }
    }

    public void a(Runnable runnable, boolean flag)
    {
        if (!flag)
        {
            w();
        }
        this;
        JVM INSTR monitorenter ;
        if (u || o == null)
        {
            throw new IllegalStateException("Activity has been destroyed");
        }
        break MISSING_BLOCK_LABEL_40;
        runnable;
        this;
        JVM INSTR monitorexit ;
        throw runnable;
        if (c == null)
        {
            c = new ArrayList();
        }
        c.add(runnable);
        if (c.size() == 1)
        {
            o.h().removeCallbacks(z);
            o.h().post(z);
        }
        this;
        JVM INSTR monitorexit ;
    }

    public void a(String s1, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        boolean flag;
        flag = false;
        String s2 = (new StringBuilder()).append(s1).append("    ").toString();
        if (f != null)
        {
            int k2 = f.size();
            if (k2 > 0)
            {
                printwriter.print(s1);
                printwriter.print("Active Fragments in ");
                printwriter.print(Integer.toHexString(System.identityHashCode(this)));
                printwriter.println(":");
                for (int i1 = 0; i1 < k2; i1++)
                {
                    n n1 = (n)f.get(i1);
                    printwriter.print(s1);
                    printwriter.print("  #");
                    printwriter.print(i1);
                    printwriter.print(": ");
                    printwriter.println(n1);
                    if (n1 != null)
                    {
                        n1.a(s2, filedescriptor, printwriter, as);
                    }
                }

            }
        }
        if (g != null)
        {
            int l2 = g.size();
            if (l2 > 0)
            {
                printwriter.print(s1);
                printwriter.println("Added Fragments:");
                for (int j1 = 0; j1 < l2; j1++)
                {
                    n n2 = (n)g.get(j1);
                    printwriter.print(s1);
                    printwriter.print("  #");
                    printwriter.print(j1);
                    printwriter.print(": ");
                    printwriter.println(n2.toString());
                }

            }
        }
        if (j != null)
        {
            int i3 = j.size();
            if (i3 > 0)
            {
                printwriter.print(s1);
                printwriter.println("Fragments Created Menus:");
                for (int k1 = 0; k1 < i3; k1++)
                {
                    n n3 = (n)j.get(k1);
                    printwriter.print(s1);
                    printwriter.print("  #");
                    printwriter.print(k1);
                    printwriter.print(": ");
                    printwriter.println(n3.toString());
                }

            }
        }
        if (i != null)
        {
            int j3 = i.size();
            if (j3 > 0)
            {
                printwriter.print(s1);
                printwriter.println("Back Stack:");
                for (int l1 = 0; l1 < j3; l1++)
                {
                    android.support.v4.app.d d1 = (android.support.v4.app.d)i.get(l1);
                    printwriter.print(s1);
                    printwriter.print("  #");
                    printwriter.print(l1);
                    printwriter.print(": ");
                    printwriter.println(d1.toString());
                    d1.a(s2, filedescriptor, printwriter, as);
                }

            }
        }
        this;
        JVM INSTR monitorenter ;
        if (k == null) goto _L2; else goto _L1
_L1:
        int k3 = k.size();
        if (k3 <= 0) goto _L2; else goto _L3
_L3:
        printwriter.print(s1);
        printwriter.println("Back Stack Indices:");
        int i2 = 0;
_L4:
        if (i2 >= k3)
        {
            break; /* Loop/switch isn't completed */
        }
        filedescriptor = (android.support.v4.app.d)k.get(i2);
        printwriter.print(s1);
        printwriter.print("  #");
        printwriter.print(i2);
        printwriter.print(": ");
        printwriter.println(filedescriptor);
        i2++;
        if (true) goto _L4; else goto _L2
_L2:
        if (l != null && l.size() > 0)
        {
            printwriter.print(s1);
            printwriter.print("mAvailBackStackIndices: ");
            printwriter.println(Arrays.toString(l.toArray()));
        }
        this;
        JVM INSTR monitorexit ;
        if (c != null)
        {
            int l3 = c.size();
            if (l3 > 0)
            {
                printwriter.print(s1);
                printwriter.println("Pending Actions:");
                for (int j2 = ((flag) ? 1 : 0); j2 < l3; j2++)
                {
                    filedescriptor = (Runnable)c.get(j2);
                    printwriter.print(s1);
                    printwriter.print("  #");
                    printwriter.print(j2);
                    printwriter.print(": ");
                    printwriter.println(filedescriptor);
                }

            }
        }
        break MISSING_BLOCK_LABEL_710;
        s1;
        this;
        JVM INSTR monitorexit ;
        throw s1;
        printwriter.print(s1);
        printwriter.println("FragmentManager misc state:");
        printwriter.print(s1);
        printwriter.print("  mHost=");
        printwriter.println(o);
        printwriter.print(s1);
        printwriter.print("  mContainer=");
        printwriter.println(p);
        if (q != null)
        {
            printwriter.print(s1);
            printwriter.print("  mParent=");
            printwriter.println(q);
        }
        printwriter.print(s1);
        printwriter.print("  mCurState=");
        printwriter.print(n);
        printwriter.print(" mStateSaved=");
        printwriter.print(t);
        printwriter.print(" mDestroyed=");
        printwriter.println(u);
        if (s)
        {
            printwriter.print(s1);
            printwriter.print("  mNeedMenuInvalidate=");
            printwriter.println(s);
        }
        if (v != null)
        {
            printwriter.print(s1);
            printwriter.print("  mNoTransactionsBecause=");
            printwriter.println(v);
        }
        if (h != null && h.size() > 0)
        {
            printwriter.print(s1);
            printwriter.print("  mAvailIndices: ");
            printwriter.println(Arrays.toString(h.toArray()));
        }
        return;
    }

    void a(boolean flag)
    {
        if (f != null)
        {
            for (int i1 = 0; i1 < f.size(); i1++)
            {
                n n1 = (n)f.get(i1);
                if (n1 != null)
                {
                    n1.N = flag;
                }
            }

        }
    }

    boolean a(Handler handler, String s1, int i1, int j1)
    {
        if (i != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (s1 != null || i1 >= 0 || (j1 & 1) != 0) goto _L4; else goto _L3
_L3:
        if ((i1 = i.size() - 1) < 0) goto _L1; else goto _L5
_L5:
        handler = (android.support.v4.app.d)i.remove(i1);
        s1 = new SparseArray();
        SparseArray sparsearray = new SparseArray();
        handler.a(s1, sparsearray);
        handler.a(true, null, s1, sparsearray);
        h();
_L8:
        return true;
_L4:
        int k1;
        int l1;
        k1 = -1;
        if (s1 == null && i1 < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        l1 = i.size() - 1;
        break MISSING_BLOCK_LABEL_112;
        if (l1 < 0) goto _L1; else goto _L6
_L6:
        k1 = l1;
        if ((j1 & 1) == 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        j1 = l1 - 1;
        do
        {
            k1 = j1;
            if (j1 < 0)
            {
                continue; /* Loop/switch isn't completed */
            }
            handler = (android.support.v4.app.d)i.get(j1);
            if (s1 == null || !s1.equals(handler.c()))
            {
                k1 = j1;
                if (i1 < 0)
                {
                    continue; /* Loop/switch isn't completed */
                }
                k1 = j1;
                if (i1 != ((android.support.v4.app.d) (handler)).p)
                {
                    continue; /* Loop/switch isn't completed */
                }
            }
            j1--;
        } while (true);
        do
        {
            if (l1 < 0)
            {
                continue; /* Loop/switch isn't completed */
            }
            handler = (android.support.v4.app.d)i.get(l1);
            if (s1 != null && s1.equals(handler.c()) || i1 >= 0 && i1 == ((android.support.v4.app.d) (handler)).p)
            {
                continue; /* Loop/switch isn't completed */
            }
            l1--;
        } while (true);
        if (k1 == i.size() - 1) goto _L1; else goto _L7
_L7:
        s1 = new ArrayList();
        for (i1 = i.size() - 1; i1 > k1; i1--)
        {
            s1.add(i.remove(i1));
        }

        j1 = s1.size() - 1;
        SparseArray sparsearray1 = new SparseArray();
        SparseArray sparsearray2 = new SparseArray();
        for (i1 = 0; i1 <= j1; i1++)
        {
            ((android.support.v4.app.d)s1.get(i1)).a(sparsearray1, sparsearray2);
        }

        handler = null;
        i1 = 0;
        while (i1 <= j1) 
        {
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("Popping back stack state: ").append(s1.get(i1)).toString());
            }
            android.support.v4.app.d d1 = (android.support.v4.app.d)s1.get(i1);
            boolean flag;
            if (i1 == j1)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            handler = d1.a(flag, handler, sparsearray1, sparsearray2);
            i1++;
        }
        h();
          goto _L8
    }

    public boolean a(Menu menu)
    {
        boolean flag1;
        if (g != null)
        {
            int i1 = 0;
            boolean flag = false;
            do
            {
                flag1 = flag;
                if (i1 >= g.size())
                {
                    break;
                }
                n n1 = (n)g.get(i1);
                flag1 = flag;
                if (n1 != null)
                {
                    flag1 = flag;
                    if (n1.c(menu))
                    {
                        flag1 = true;
                    }
                }
                i1++;
                flag = flag1;
            } while (true);
        } else
        {
            flag1 = false;
        }
        return flag1;
    }

    public boolean a(Menu menu, MenuInflater menuinflater)
    {
        boolean flag = false;
        ArrayList arraylist1 = null;
        ArrayList arraylist = null;
        boolean flag2;
        if (g != null)
        {
            int i1 = 0;
            boolean flag1 = false;
            do
            {
                arraylist1 = arraylist;
                flag2 = flag1;
                if (i1 >= g.size())
                {
                    break;
                }
                n n1 = (n)g.get(i1);
                arraylist1 = arraylist;
                flag2 = flag1;
                if (n1 != null)
                {
                    arraylist1 = arraylist;
                    flag2 = flag1;
                    if (n1.b(menu, menuinflater))
                    {
                        flag2 = true;
                        arraylist1 = arraylist;
                        if (arraylist == null)
                        {
                            arraylist1 = new ArrayList();
                        }
                        arraylist1.add(n1);
                    }
                }
                i1++;
                flag1 = flag2;
                arraylist = arraylist1;
            } while (true);
        } else
        {
            flag2 = false;
        }
        if (j != null)
        {
            for (int j1 = ((flag) ? 1 : 0); j1 < j.size(); j1++)
            {
                menu = (n)j.get(j1);
                if (arraylist1 == null || !arraylist1.contains(menu))
                {
                    menu.s();
                }
            }

        }
        j = arraylist1;
        return flag2;
    }

    public boolean a(MenuItem menuitem)
    {
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = flag1;
        if (g == null) goto _L2; else goto _L1
_L1:
        int i1 = 0;
_L7:
        flag = flag1;
        if (i1 >= g.size()) goto _L2; else goto _L3
_L3:
        n n1 = (n)g.get(i1);
        if (n1 == null || !n1.c(menuitem)) goto _L5; else goto _L4
_L4:
        flag = true;
_L2:
        return flag;
_L5:
        i1++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void b(int i1)
    {
        this;
        JVM INSTR monitorenter ;
        k.set(i1, null);
        if (l == null)
        {
            l = new ArrayList();
        }
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Freeing back stack index ").append(i1).toString());
        }
        l.add(Integer.valueOf(i1));
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    void b(android.support.v4.app.d d1)
    {
        if (i == null)
        {
            i = new ArrayList();
        }
        i.add(d1);
        h();
    }

    void b(n n1)
    {
        a(n1, n, 0, 0, false);
    }

    public void b(n n1, int i1, int j1)
    {
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("hide: ").append(n1).toString());
        }
        if (!n1.J)
        {
            n1.J = true;
            if (n1.T != null)
            {
                Animation animation = a(n1, i1, false, j1);
                if (animation != null)
                {
                    b(n1.T, animation);
                    n1.T.startAnimation(animation);
                }
                n1.T.setVisibility(8);
            }
            if (n1.v && n1.O && n1.P)
            {
                s = true;
            }
            n1.c(true);
        }
    }

    public void b(Menu menu)
    {
        if (g != null)
        {
            for (int i1 = 0; i1 < g.size(); i1++)
            {
                n n1 = (n)g.get(i1);
                if (n1 != null)
                {
                    n1.d(menu);
                }
            }

        }
    }

    public boolean b()
    {
        return g();
    }

    public boolean b(MenuItem menuitem)
    {
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = flag1;
        if (g == null) goto _L2; else goto _L1
_L1:
        int i1 = 0;
_L7:
        flag = flag1;
        if (i1 >= g.size()) goto _L2; else goto _L3
_L3:
        n n1 = (n)g.get(i1);
        if (n1 == null || !n1.d(menuitem)) goto _L5; else goto _L4
_L4:
        flag = true;
_L2:
        return flag;
_L5:
        i1++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    void c(n n1)
    {
        if (n1.p < 0)
        {
            if (h == null || h.size() <= 0)
            {
                if (f == null)
                {
                    f = new ArrayList();
                }
                n1.a(f.size(), q);
                f.add(n1);
            } else
            {
                n1.a(((Integer)h.remove(h.size() - 1)).intValue(), q);
                f.set(n1.p, n1);
            }
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("Allocated fragment index ").append(n1).toString());
                return;
            }
        }
    }

    public void c(n n1, int i1, int j1)
    {
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("show: ").append(n1).toString());
        }
        if (n1.J)
        {
            n1.J = false;
            if (n1.T != null)
            {
                Animation animation = a(n1, i1, true, j1);
                if (animation != null)
                {
                    b(n1.T, animation);
                    n1.T.startAnimation(animation);
                }
                n1.T.setVisibility(0);
            }
            if (n1.v && n1.O && n1.P)
            {
                s = true;
            }
            n1.c(false);
        }
    }

    public boolean c()
    {
        w();
        b();
        return a(o.h(), ((String) (null)), -1, 0);
    }

    public List d()
    {
        return f;
    }

    void d(n n1)
    {
        if (n1.p < 0)
        {
            return;
        }
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("Freeing fragment index ").append(n1).toString());
        }
        f.set(n1.p, null);
        if (h == null)
        {
            h = new ArrayList();
        }
        h.add(Integer.valueOf(n1.p));
        o.a(n1.q);
        n1.r();
    }

    public void d(n n1, int i1, int j1)
    {
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("detach: ").append(n1).toString());
        }
        if (!n1.K)
        {
            n1.K = true;
            if (n1.v)
            {
                if (g != null)
                {
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("remove from detach: ").append(n1).toString());
                    }
                    g.remove(n1);
                }
                if (n1.O && n1.P)
                {
                    s = true;
                }
                n1.v = false;
                a(n1, 1, i1, j1, false);
            }
        }
    }

    void e(n n1)
    {
        if (n1.U != null)
        {
            if (y == null)
            {
                y = new SparseArray();
            } else
            {
                y.clear();
            }
            n1.U.saveHierarchyState(y);
            if (y.size() > 0)
            {
                n1.o = y;
                y = null;
                return;
            }
        }
    }

    public void e(n n1, int i1, int j1)
    {
        if (a)
        {
            Log.v("FragmentManager", (new StringBuilder()).append("attach: ").append(n1).toString());
        }
        if (n1.K)
        {
            n1.K = false;
            if (!n1.v)
            {
                if (g == null)
                {
                    g = new ArrayList();
                }
                if (g.contains(n1))
                {
                    throw new IllegalStateException((new StringBuilder()).append("Fragment already added: ").append(n1).toString());
                }
                if (a)
                {
                    Log.v("FragmentManager", (new StringBuilder()).append("add from attach: ").append(n1).toString());
                }
                g.add(n1);
                n1.v = true;
                if (n1.O && n1.P)
                {
                    s = true;
                }
                a(n1, n, i1, j1, false);
            }
        }
    }

    public boolean e()
    {
        return u;
    }

    Bundle f(n n1)
    {
        if (x == null)
        {
            x = new Bundle();
        }
        n1.j(x);
        Bundle bundle;
        Bundle bundle1;
        if (!x.isEmpty())
        {
            bundle1 = x;
            x = null;
        } else
        {
            bundle1 = null;
        }
        if (n1.T != null)
        {
            e(n1);
        }
        bundle = bundle1;
        if (n1.o != null)
        {
            bundle = bundle1;
            if (bundle1 == null)
            {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", n1.o);
        }
        bundle1 = bundle;
        if (!n1.W)
        {
            bundle1 = bundle;
            if (bundle == null)
            {
                bundle1 = new Bundle();
            }
            bundle1.putBoolean("android:user_visible_hint", n1.W);
        }
        return bundle1;
    }

    void f()
    {
        if (f != null)
        {
            int i1 = 0;
            while (i1 < f.size()) 
            {
                n n1 = (n)f.get(i1);
                if (n1 != null)
                {
                    a(n1);
                }
                i1++;
            }
        }
    }

    public boolean g()
    {
        if (e)
        {
            throw new IllegalStateException("Recursive entry to executePendingTransactions");
        }
        if (Looper.myLooper() != o.h().getLooper())
        {
            throw new IllegalStateException("Must be called from main thread of process");
        }
        boolean flag = false;
_L2:
        this;
        JVM INSTR monitorenter ;
        if (c != null && c.size() != 0)
        {
            break MISSING_BLOCK_LABEL_141;
        }
        this;
        JVM INSTR monitorexit ;
        int k1;
        if (!w)
        {
            break MISSING_BLOCK_LABEL_274;
        }
        int i1 = 0;
        int l1;
        for (k1 = 0; i1 < f.size(); k1 = l1)
        {
            n n1 = (n)f.get(i1);
            l1 = k1;
            if (n1 != null)
            {
                l1 = k1;
                if (n1.X != null)
                {
                    l1 = k1 | n1.X.a();
                }
            }
            i1++;
        }

        break; /* Loop/switch isn't completed */
        k1 = c.size();
        if (d == null || d.length < k1)
        {
            d = new Runnable[k1];
        }
        c.toArray(d);
        c.clear();
        o.h().removeCallbacks(z);
        this;
        JVM INSTR monitorexit ;
        e = true;
        for (int j1 = 0; j1 < k1; j1++)
        {
            d[j1].run();
            d[j1] = null;
        }

        break MISSING_BLOCK_LABEL_250;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        e = false;
        flag = true;
        if (true) goto _L2; else goto _L1
_L1:
        if (k1 == 0)
        {
            w = false;
            f();
        }
        return flag;
    }

    void h()
    {
        if (m != null)
        {
            for (int i1 = 0; i1 < m.size(); i1++)
            {
                ((y)m.get(i1)).a();
            }

        }
    }

    ArrayList i()
    {
        ArrayList arraylist1 = null;
        ArrayList arraylist = null;
        if (f != null)
        {
            int i1 = 0;
            do
            {
                arraylist1 = arraylist;
                if (i1 >= f.size())
                {
                    break;
                }
                n n1 = (n)f.get(i1);
                ArrayList arraylist2 = arraylist;
                if (n1 != null)
                {
                    arraylist2 = arraylist;
                    if (n1.L)
                    {
                        arraylist1 = arraylist;
                        if (arraylist == null)
                        {
                            arraylist1 = new ArrayList();
                        }
                        arraylist1.add(n1);
                        n1.M = true;
                        int j1;
                        if (n1.s != null)
                        {
                            j1 = n1.s.p;
                        } else
                        {
                            j1 = -1;
                        }
                        n1.t = j1;
                        arraylist2 = arraylist1;
                        if (a)
                        {
                            Log.v("FragmentManager", (new StringBuilder()).append("retainNonConfig: keeping retained ").append(n1).toString());
                            arraylist2 = arraylist1;
                        }
                    }
                }
                i1++;
                arraylist = arraylist2;
            } while (true);
        }
        return arraylist1;
    }

    Parcelable j()
    {
        FragmentManagerState fragmentmanagerstate;
        fragmentmanagerstate = null;
        g();
        if (b)
        {
            t = true;
        }
        if (f != null && f.size() > 0) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        FragmentState afragmentstate[];
        int j2 = f.size();
        afragmentstate = new FragmentState[j2];
        int k1 = 0;
        boolean flag = false;
        while (k1 < j2) 
        {
            n n1 = (n)f.get(k1);
            if (n1 == null)
            {
                continue;
            }
            if (n1.p < 0)
            {
                a(new IllegalStateException((new StringBuilder()).append("Failure saving state: active ").append(n1).append(" has cleared index: ").append(n1.p).toString()));
            }
            FragmentState fragmentstate = new FragmentState(n1);
            afragmentstate[k1] = fragmentstate;
            if (n1.k > 0 && fragmentstate.j == null)
            {
                fragmentstate.j = f(n1);
                if (n1.s != null)
                {
                    if (n1.s.p < 0)
                    {
                        a(new IllegalStateException((new StringBuilder()).append("Failure saving state: ").append(n1).append(" has target not in fragment manager: ").append(n1.s).toString()));
                    }
                    if (fragmentstate.j == null)
                    {
                        fragmentstate.j = new Bundle();
                    }
                    a(fragmentstate.j, "android:target_state", n1.s);
                    if (n1.u != 0)
                    {
                        fragmentstate.j.putInt("android:target_req_state", n1.u);
                    }
                }
            } else
            {
                fragmentstate.j = n1.n;
            }
            if (a)
            {
                Log.v("FragmentManager", (new StringBuilder()).append("Saved state of ").append(n1).append(": ").append(fragmentstate.j).toString());
            }
            flag = true;
            k1++;
        }
        if (flag)
        {
            break; /* Loop/switch isn't completed */
        }
        if (a)
        {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
        if (true) goto _L1; else goto _L3
_L3:
        int ai[];
label0:
        {
            if (g != null)
            {
                int l1 = g.size();
                if (l1 > 0)
                {
                    int ai1[] = new int[l1];
                    int i1 = 0;
                    do
                    {
                        ai = ai1;
                        if (i1 >= l1)
                        {
                            break;
                        }
                        ai1[i1] = ((n)g.get(i1)).p;
                        if (ai1[i1] < 0)
                        {
                            a(new IllegalStateException((new StringBuilder()).append("Failure saving state: active ").append(g.get(i1)).append(" has cleared index: ").append(ai1[i1]).toString()));
                        }
                        if (a)
                        {
                            Log.v("FragmentManager", (new StringBuilder()).append("saveAllState: adding fragment #").append(i1).append(": ").append(g.get(i1)).toString());
                        }
                        i1++;
                    } while (true);
                    break label0;
                }
            }
            ai = null;
        }
        BackStackState abackstackstate[] = fragmentmanagerstate;
        if (i != null)
        {
            int i2 = i.size();
            abackstackstate = fragmentmanagerstate;
            if (i2 > 0)
            {
                BackStackState abackstackstate1[] = new BackStackState[i2];
                int j1 = 0;
                do
                {
                    abackstackstate = abackstackstate1;
                    if (j1 >= i2)
                    {
                        break;
                    }
                    abackstackstate1[j1] = new BackStackState((android.support.v4.app.d)i.get(j1));
                    if (a)
                    {
                        Log.v("FragmentManager", (new StringBuilder()).append("saveAllState: adding back stack #").append(j1).append(": ").append(i.get(j1)).toString());
                    }
                    j1++;
                } while (true);
            }
        }
        abackstackstate1 = new FragmentManagerState();
        abackstackstate1.a = afragmentstate;
        abackstackstate1.b = ai;
        abackstackstate1.c = abackstackstate;
        return abackstackstate1;
    }

    public void k()
    {
        t = false;
    }

    public void l()
    {
        t = false;
        a(1, false);
    }

    public void m()
    {
        t = false;
        a(2, false);
    }

    public void n()
    {
        t = false;
        a(4, false);
    }

    public void o()
    {
        t = false;
        a(5, false);
    }

    public void p()
    {
        a(4, false);
    }

    public void q()
    {
        t = true;
        a(3, false);
    }

    public void r()
    {
        a(2, false);
    }

    public void s()
    {
        a(1, false);
    }

    public void t()
    {
        u = true;
        g();
        a(0, false);
        o = null;
        p = null;
        q = null;
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(128);
        stringbuilder.append("FragmentManager{");
        stringbuilder.append(Integer.toHexString(System.identityHashCode(this)));
        stringbuilder.append(" in ");
        if (q != null)
        {
            android.support.v4.b.d.a(q, stringbuilder);
        } else
        {
            android.support.v4.b.d.a(o, stringbuilder);
        }
        stringbuilder.append("}}");
        return stringbuilder.toString();
    }

    public void u()
    {
        if (g != null)
        {
            for (int i1 = 0; i1 < g.size(); i1++)
            {
                n n1 = (n)g.get(i1);
                if (n1 != null)
                {
                    n1.E();
                }
            }

        }
    }

    k v()
    {
        return this;
    }

    static 
    {
        boolean flag = false;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            flag = true;
        }
        b = flag;
    }
}
