// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.b.d;
import android.support.v4.b.l;
import android.support.v4.c.a;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;

// Referenced classes of package android.support.v4.app:
//            p, z, o, cw, 
//            at, w, q, cv, 
//            x

public class n
    implements ComponentCallbacks, android.view.View.OnCreateContextMenuListener
{

    private static final l a = new l();
    static final Object j = new Object();
    boolean A;
    int B;
    z C;
    w D;
    z E;
    n F;
    int G;
    int H;
    String I;
    boolean J;
    boolean K;
    boolean L;
    boolean M;
    boolean N;
    boolean O;
    boolean P;
    boolean Q;
    int R;
    ViewGroup S;
    View T;
    View U;
    boolean V;
    boolean W;
    at X;
    boolean Y;
    boolean Z;
    Object aa;
    Object ab;
    Object ac;
    Object ad;
    Object ae;
    Object af;
    Boolean ag;
    Boolean ah;
    cv ai;
    cv aj;
    int k;
    View l;
    int m;
    Bundle n;
    SparseArray o;
    int p;
    String q;
    Bundle r;
    n s;
    int t;
    int u;
    boolean v;
    boolean w;
    boolean x;
    boolean y;
    boolean z;

    public n()
    {
        k = 0;
        p = -1;
        t = -1;
        P = true;
        W = true;
        aa = null;
        ab = j;
        ac = null;
        ad = j;
        ae = null;
        af = j;
        ai = null;
        aj = null;
    }

    public static n a(Context context, String s1)
    {
        return a(context, s1, ((Bundle) (null)));
    }

    public static n a(Context context, String s1, Bundle bundle)
    {
        Class class1;
        Class class2;
        try
        {
            class2 = (Class)a.get(s1);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new p((new StringBuilder()).append("Unable to instantiate fragment ").append(s1).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new p((new StringBuilder()).append("Unable to instantiate fragment ").append(s1).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), context);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new p((new StringBuilder()).append("Unable to instantiate fragment ").append(s1).append(": make sure class name exists, is public, and has an").append(" empty constructor that is public").toString(), context);
        }
        class1 = class2;
        if (class2 != null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        class1 = context.getClassLoader().loadClass(s1);
        a.put(s1, class1);
        context = (n)class1.newInstance();
        if (bundle == null)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        bundle.setClassLoader(context.getClass().getClassLoader());
        context.r = bundle;
        return context;
    }

    static boolean b(Context context, String s1)
    {
        Class class1;
        Class class2;
        boolean flag;
        try
        {
            class2 = (Class)a.get(s1);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        class1 = class2;
        if (class2 != null)
        {
            break MISSING_BLOCK_LABEL_35;
        }
        class1 = context.getClassLoader().loadClass(s1);
        a.put(s1, class1);
        flag = android/support/v4/app/n.isAssignableFrom(class1);
        return flag;
    }

    public boolean A()
    {
        if (ag == null)
        {
            return true;
        } else
        {
            return ag.booleanValue();
        }
    }

    void B()
    {
        E = new z();
        E.a(D, new o(this), this);
    }

    void C()
    {
        if (E != null)
        {
            E.k();
            E.g();
        }
        Q = false;
        c();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onStart()").toString());
        }
        if (E != null)
        {
            E.n();
        }
        if (X != null)
        {
            X.g();
        }
    }

    void D()
    {
        if (E != null)
        {
            E.k();
            E.g();
        }
        Q = false;
        o();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onResume()").toString());
        }
        if (E != null)
        {
            E.o();
            E.g();
        }
    }

    void E()
    {
        onLowMemory();
        if (E != null)
        {
            E.u();
        }
    }

    void F()
    {
        if (E != null)
        {
            E.p();
        }
        Q = false;
        p();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onPause()").toString());
        } else
        {
            return;
        }
    }

    void G()
    {
        if (E != null)
        {
            E.q();
        }
        Q = false;
        d();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onStop()").toString());
        } else
        {
            return;
        }
    }

    void H()
    {
label0:
        {
            if (E != null)
            {
                E.r();
            }
            if (Y)
            {
                Y = false;
                if (!Z)
                {
                    Z = true;
                    X = D.a(q, Y, false);
                }
                if (X != null)
                {
                    if (!N)
                    {
                        break label0;
                    }
                    X.d();
                }
            }
            return;
        }
        X.c();
    }

    void I()
    {
        if (E != null)
        {
            E.s();
        }
        Q = false;
        e();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onDestroyView()").toString());
        }
        if (X != null)
        {
            X.f();
        }
    }

    void J()
    {
        if (E != null)
        {
            E.t();
        }
        Q = false;
        q();
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onDestroy()").toString());
        } else
        {
            return;
        }
    }

    public View a(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return null;
    }

    public Animation a(int i1, boolean flag, int j1)
    {
        return null;
    }

    public void a(int i1, int j1, Intent intent)
    {
    }

    final void a(int i1, n n1)
    {
        p = i1;
        if (n1 != null)
        {
            q = (new StringBuilder()).append(n1.q).append(":").append(p).toString();
            return;
        } else
        {
            q = (new StringBuilder()).append("android:fragment:").append(p).toString();
            return;
        }
    }

    public void a(int i1, String as[], int ai1[])
    {
    }

    public void a(Activity activity)
    {
        Q = true;
    }

    public void a(Activity activity, AttributeSet attributeset, Bundle bundle)
    {
        Q = true;
    }

    public void a(Context context)
    {
        Q = true;
        if (D == null)
        {
            context = null;
        } else
        {
            context = D.f();
        }
        if (context != null)
        {
            Q = false;
            a(((Activity) (context)));
        }
    }

    public void a(Context context, AttributeSet attributeset, Bundle bundle)
    {
        Q = true;
        if (D == null)
        {
            context = null;
        } else
        {
            context = D.f();
        }
        if (context != null)
        {
            Q = false;
            a(((Activity) (context)), attributeset, bundle);
        }
    }

    public void a(Intent intent, int i1)
    {
        if (D == null)
        {
            throw new IllegalStateException((new StringBuilder()).append("Fragment ").append(this).append(" not attached to Activity").toString());
        } else
        {
            D.a(this, intent, i1);
            return;
        }
    }

    void a(Configuration configuration)
    {
        onConfigurationChanged(configuration);
        if (E != null)
        {
            E.a(configuration);
        }
    }

    public void a(Bundle bundle)
    {
        Q = true;
    }

    public void a(Menu menu)
    {
    }

    public void a(Menu menu, MenuInflater menuinflater)
    {
    }

    public void a(View view, Bundle bundle)
    {
    }

    public void a(String s1, FileDescriptor filedescriptor, PrintWriter printwriter, String as[])
    {
        printwriter.print(s1);
        printwriter.print("mFragmentId=#");
        printwriter.print(Integer.toHexString(G));
        printwriter.print(" mContainerId=#");
        printwriter.print(Integer.toHexString(H));
        printwriter.print(" mTag=");
        printwriter.println(I);
        printwriter.print(s1);
        printwriter.print("mState=");
        printwriter.print(k);
        printwriter.print(" mIndex=");
        printwriter.print(p);
        printwriter.print(" mWho=");
        printwriter.print(q);
        printwriter.print(" mBackStackNesting=");
        printwriter.println(B);
        printwriter.print(s1);
        printwriter.print("mAdded=");
        printwriter.print(v);
        printwriter.print(" mRemoving=");
        printwriter.print(w);
        printwriter.print(" mResumed=");
        printwriter.print(x);
        printwriter.print(" mFromLayout=");
        printwriter.print(y);
        printwriter.print(" mInLayout=");
        printwriter.println(z);
        printwriter.print(s1);
        printwriter.print("mHidden=");
        printwriter.print(J);
        printwriter.print(" mDetached=");
        printwriter.print(K);
        printwriter.print(" mMenuVisible=");
        printwriter.print(P);
        printwriter.print(" mHasMenu=");
        printwriter.println(O);
        printwriter.print(s1);
        printwriter.print("mRetainInstance=");
        printwriter.print(L);
        printwriter.print(" mRetaining=");
        printwriter.print(M);
        printwriter.print(" mUserVisibleHint=");
        printwriter.println(W);
        if (C != null)
        {
            printwriter.print(s1);
            printwriter.print("mFragmentManager=");
            printwriter.println(C);
        }
        if (D != null)
        {
            printwriter.print(s1);
            printwriter.print("mHost=");
            printwriter.println(D);
        }
        if (F != null)
        {
            printwriter.print(s1);
            printwriter.print("mParentFragment=");
            printwriter.println(F);
        }
        if (r != null)
        {
            printwriter.print(s1);
            printwriter.print("mArguments=");
            printwriter.println(r);
        }
        if (n != null)
        {
            printwriter.print(s1);
            printwriter.print("mSavedFragmentState=");
            printwriter.println(n);
        }
        if (o != null)
        {
            printwriter.print(s1);
            printwriter.print("mSavedViewState=");
            printwriter.println(o);
        }
        if (s != null)
        {
            printwriter.print(s1);
            printwriter.print("mTarget=");
            printwriter.print(s);
            printwriter.print(" mTargetRequestCode=");
            printwriter.println(u);
        }
        if (R != 0)
        {
            printwriter.print(s1);
            printwriter.print("mNextAnim=");
            printwriter.println(R);
        }
        if (S != null)
        {
            printwriter.print(s1);
            printwriter.print("mContainer=");
            printwriter.println(S);
        }
        if (T != null)
        {
            printwriter.print(s1);
            printwriter.print("mView=");
            printwriter.println(T);
        }
        if (U != null)
        {
            printwriter.print(s1);
            printwriter.print("mInnerView=");
            printwriter.println(T);
        }
        if (l != null)
        {
            printwriter.print(s1);
            printwriter.print("mAnimatingAway=");
            printwriter.println(l);
            printwriter.print(s1);
            printwriter.print("mStateAfterAnimating=");
            printwriter.println(m);
        }
        if (X != null)
        {
            printwriter.print(s1);
            printwriter.println("Loader Manager:");
            X.a((new StringBuilder()).append(s1).append("  ").toString(), filedescriptor, printwriter, as);
        }
        if (E != null)
        {
            printwriter.print(s1);
            printwriter.println((new StringBuilder()).append("Child ").append(E).append(":").toString());
            E.a((new StringBuilder()).append(s1).append("  ").toString(), filedescriptor, printwriter, as);
        }
    }

    public boolean a(MenuItem menuitem)
    {
        return false;
    }

    public LayoutInflater b(Bundle bundle)
    {
        bundle = D.b();
        j();
        android.support.v4.c.a.a(bundle, E.v());
        return bundle;
    }

    View b(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        if (E != null)
        {
            E.k();
        }
        return a(layoutinflater, viewgroup, bundle);
    }

    public void b()
    {
        Q = true;
    }

    public void b(Menu menu)
    {
    }

    boolean b(Menu menu, MenuInflater menuinflater)
    {
        boolean flag1 = false;
        boolean flag2 = false;
        if (!J)
        {
            boolean flag = flag2;
            if (O)
            {
                flag = flag2;
                if (P)
                {
                    flag = true;
                    a(menu, menuinflater);
                }
            }
            flag1 = flag;
            if (E != null)
            {
                flag1 = flag | E.a(menu, menuinflater);
            }
        }
        return flag1;
    }

    public boolean b(MenuItem menuitem)
    {
        return false;
    }

    public void c()
    {
        Q = true;
        if (!Y)
        {
            Y = true;
            if (!Z)
            {
                Z = true;
                X = D.a(q, Y, false);
            }
            if (X != null)
            {
                X.b();
            }
        }
    }

    public void c(boolean flag)
    {
    }

    boolean c(Menu menu)
    {
        boolean flag1 = false;
        boolean flag2 = false;
        if (!J)
        {
            boolean flag = flag2;
            if (O)
            {
                flag = flag2;
                if (P)
                {
                    flag = true;
                    a(menu);
                }
            }
            flag1 = flag;
            if (E != null)
            {
                flag1 = flag | E.a(menu);
            }
        }
        return flag1;
    }

    boolean c(MenuItem menuitem)
    {
        while (!J && (O && P && a(menuitem) || E != null && E.a(menuitem))) 
        {
            return true;
        }
        return false;
    }

    public void d()
    {
        Q = true;
    }

    public void d(Bundle bundle)
    {
        Q = true;
    }

    void d(Menu menu)
    {
        if (!J)
        {
            if (O && P)
            {
                b(menu);
            }
            if (E != null)
            {
                E.b(menu);
            }
        }
    }

    boolean d(MenuItem menuitem)
    {
        while (!J && (b(menuitem) || E != null && E.b(menuitem))) 
        {
            return true;
        }
        return false;
    }

    public void e()
    {
        Q = true;
    }

    public void e(Bundle bundle)
    {
    }

    public final boolean equals(Object obj)
    {
        return super.equals(obj);
    }

    final void f(Bundle bundle)
    {
        if (o != null)
        {
            U.restoreHierarchyState(o);
            o = null;
        }
        Q = false;
        g(bundle);
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onViewStateRestored()").toString());
        } else
        {
            return;
        }
    }

    final boolean f()
    {
        return B > 0;
    }

    public final q g()
    {
        if (D == null)
        {
            return null;
        } else
        {
            return (q)D.f();
        }
    }

    public void g(Bundle bundle)
    {
        Q = true;
    }

    public final Resources h()
    {
        if (D == null)
        {
            throw new IllegalStateException((new StringBuilder()).append("Fragment ").append(this).append(" not attached to Activity").toString());
        } else
        {
            return D.g().getResources();
        }
    }

    void h(Bundle bundle)
    {
        if (E != null)
        {
            E.k();
        }
        Q = false;
        a(bundle);
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onCreate()").toString());
        }
        if (bundle != null)
        {
            bundle = bundle.getParcelable("android:support:fragments");
            if (bundle != null)
            {
                if (E == null)
                {
                    B();
                }
                E.a(bundle, null);
                E.l();
            }
        }
    }

    public final int hashCode()
    {
        return super.hashCode();
    }

    public final x i()
    {
        return C;
    }

    void i(Bundle bundle)
    {
        if (E != null)
        {
            E.k();
        }
        Q = false;
        d(bundle);
        if (!Q)
        {
            throw new cw((new StringBuilder()).append("Fragment ").append(this).append(" did not call through to super.onActivityCreated()").toString());
        }
        if (E != null)
        {
            E.m();
        }
    }

    public final x j()
    {
        if (E != null) goto _L2; else goto _L1
_L1:
        B();
        if (k < 5) goto _L4; else goto _L3
_L3:
        E.o();
_L2:
        return E;
_L4:
        if (k >= 4)
        {
            E.n();
        } else
        if (k >= 2)
        {
            E.m();
        } else
        if (k >= 1)
        {
            E.l();
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    void j(Bundle bundle)
    {
        e(bundle);
        if (E != null)
        {
            android.os.Parcelable parcelable = E.j();
            if (parcelable != null)
            {
                bundle.putParcelable("android:support:fragments", parcelable);
            }
        }
    }

    public final boolean k()
    {
        return D != null && v;
    }

    public final boolean l()
    {
        return w;
    }

    public final boolean m()
    {
        return J;
    }

    public View n()
    {
        return T;
    }

    public void o()
    {
        Q = true;
    }

    public void onConfigurationChanged(Configuration configuration)
    {
        Q = true;
    }

    public void onCreateContextMenu(ContextMenu contextmenu, View view, android.view.ContextMenu.ContextMenuInfo contextmenuinfo)
    {
        g().onCreateContextMenu(contextmenu, view, contextmenuinfo);
    }

    public void onLowMemory()
    {
        Q = true;
    }

    public void p()
    {
        Q = true;
    }

    public void q()
    {
        Q = true;
        if (!Z)
        {
            Z = true;
            X = D.a(q, Y, false);
        }
        if (X != null)
        {
            X.h();
        }
    }

    void r()
    {
        p = -1;
        q = null;
        v = false;
        w = false;
        x = false;
        y = false;
        z = false;
        A = false;
        B = 0;
        C = null;
        E = null;
        D = null;
        G = 0;
        H = 0;
        I = null;
        J = false;
        K = false;
        M = false;
        X = null;
        Y = false;
        Z = false;
    }

    public void s()
    {
    }

    public Object t()
    {
        return aa;
    }

    public String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(128);
        android.support.v4.b.d.a(this, stringbuilder);
        if (p >= 0)
        {
            stringbuilder.append(" #");
            stringbuilder.append(p);
        }
        if (G != 0)
        {
            stringbuilder.append(" id=0x");
            stringbuilder.append(Integer.toHexString(G));
        }
        if (I != null)
        {
            stringbuilder.append(" ");
            stringbuilder.append(I);
        }
        stringbuilder.append('}');
        return stringbuilder.toString();
    }

    public Object u()
    {
        if (ab == j)
        {
            return t();
        } else
        {
            return ab;
        }
    }

    public Object v()
    {
        return ac;
    }

    public Object w()
    {
        if (ad == j)
        {
            return v();
        } else
        {
            return ad;
        }
    }

    public Object x()
    {
        return ae;
    }

    public Object y()
    {
        if (af == j)
        {
            return x();
        } else
        {
            return af;
        }
    }

    public boolean z()
    {
        if (ah == null)
        {
            return true;
        } else
        {
            return ah.booleanValue();
        }
    }

}
