// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

// Referenced classes of package android.support.v4.app:
//            d, i

class g
    implements android.view.ViewTreeObserver.OnPreDrawListener
{

    final View a;
    final i b;
    final int c;
    final Object d;
    final d e;

    g(d d1, View view, i i, int j, Object obj)
    {
        e = d1;
        a = view;
        b = i;
        c = j;
        d = obj;
        super();
    }

    public boolean onPreDraw()
    {
        a.getViewTreeObserver().removeOnPreDrawListener(this);
        android.support.v4.app.d.a(e, b, c, d);
        return true;
    }
}
