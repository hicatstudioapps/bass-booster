// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.support.v4.b;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// Referenced classes of package android.support.v4.b:
//            h, i, k

abstract class f
{

    h b;
    i c;
    k d;

    f()
    {
    }

    public static boolean a(Map map, Collection collection)
    {
        for (collection = collection.iterator(); collection.hasNext();)
        {
            if (!map.containsKey(collection.next()))
            {
                return false;
            }
        }

        return true;
    }

    public static boolean a(Set set, Object obj)
    {
        boolean flag;
        boolean flag1;
        flag1 = true;
        flag = false;
        if (set != obj) goto _L2; else goto _L1
_L1:
        flag = true;
_L4:
        return flag;
_L2:
        if (!(obj instanceof Set)) goto _L4; else goto _L3
_L3:
        obj = (Set)obj;
        if (set.size() != ((Set) (obj)).size()) goto _L6; else goto _L5
_L5:
        flag = set.containsAll(((Collection) (obj)));
        if (!flag) goto _L6; else goto _L7
_L7:
        flag = flag1;
_L9:
        return flag;
_L6:
        flag = false;
        if (true) goto _L9; else goto _L8
_L8:
        set;
        return false;
        set;
        return false;
    }

    public static boolean b(Map map, Collection collection)
    {
        int j = map.size();
        for (collection = collection.iterator(); collection.hasNext(); map.remove(collection.next())) { }
        return j != map.size();
    }

    public static boolean c(Map map, Collection collection)
    {
        int j = map.size();
        Iterator iterator = map.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            if (!collection.contains(iterator.next()))
            {
                iterator.remove();
            }
        } while (true);
        return j != map.size();
    }

    protected abstract int a();

    protected abstract int a(Object obj);

    protected abstract Object a(int j, int l);

    protected abstract Object a(int j, Object obj);

    protected abstract void a(int j);

    protected abstract void a(Object obj, Object obj1);

    public Object[] a(Object aobj[], int j)
    {
        int i1 = a();
        if (aobj.length < i1)
        {
            aobj = (Object[])(Object[])Array.newInstance(((Object) (aobj)).getClass().getComponentType(), i1);
        }
        for (int l = 0; l < i1; l++)
        {
            aobj[l] = a(l, j);
        }

        if (aobj.length > i1)
        {
            aobj[i1] = null;
        }
        return aobj;
    }

    protected abstract int b(Object obj);

    protected abstract Map b();

    public Object[] b(int j)
    {
        int i1 = a();
        Object aobj[] = new Object[i1];
        for (int l = 0; l < i1; l++)
        {
            aobj[l] = a(l, j);
        }

        return aobj;
    }

    protected abstract void c();

    public Set d()
    {
        if (b == null)
        {
            b = new h(this);
        }
        return b;
    }

    public Set e()
    {
        if (c == null)
        {
            c = new i(this);
        }
        return c;
    }

    public Collection f()
    {
        if (d == null)
        {
            d = new k(this);
        }
        return d;
    }
}
