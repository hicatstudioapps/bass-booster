// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;

// Referenced classes of package android.widget:
//            SeekBar

public class VerticalSeekBar extends SeekBar
{

    private SeekBar.OnSeekBarChangeListener a;
    private int b;

    public VerticalSeekBar(Context context)
    {
        super(context);
        b = 0;
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        b = 0;
    }

    public VerticalSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        b = 0;
    }

    public int getMaximum()
    {
        this;
        JVM INSTR monitorenter ;
        int i = getMax();
        this;
        JVM INSTR monitorexit ;
        return i;
        Exception exception;
        exception;
        throw exception;
    }

    protected void onDraw(Canvas canvas)
    {
        canvas.rotate(-90F);
        canvas.translate(-getHeight(), 0.0F);
        super.onDraw(canvas);
    }

    protected void onMeasure(int i, int j)
    {
        this;
        JVM INSTR monitorenter ;
        super.onMeasure(j, i);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(j, i, l, k);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (!isEnabled())
        {
            return false;
        }
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 44
    //                   0 46
    //                   1 178
    //                   2 69
    //                   3 201;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        return true;
_L2:
        a.onStartTrackingTouch(this);
        setPressed(true);
        setSelected(true);
        continue; /* Loop/switch isn't completed */
_L4:
        super.onTouchEvent(motionevent);
        int j = getMax() - (int)(((float)getMax() * motionevent.getY()) / (float)getHeight());
        int i = j;
        if (j < 0)
        {
            i = 0;
        }
        j = i;
        if (i > getMax())
        {
            j = getMax();
        }
        setProgress(j);
        if (j != b)
        {
            b = j;
            a.onProgressChanged(this, j, true);
        }
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        setPressed(true);
        setSelected(true);
        continue; /* Loop/switch isn't completed */
_L3:
        a.onStopTrackingTouch(this);
        setPressed(false);
        setSelected(false);
        continue; /* Loop/switch isn't completed */
_L5:
        super.onTouchEvent(motionevent);
        setPressed(false);
        setSelected(false);
        if (true) goto _L1; else goto _L6
_L6:
    }

    public void setMaximum(int i)
    {
        this;
        JVM INSTR monitorenter ;
        setMax(i);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener onseekbarchangelistener)
    {
        a = onseekbarchangelistener;
    }

    public void setProgressAndThumb(int i)
    {
        this;
        JVM INSTR monitorenter ;
        setProgress(i);
        onSizeChanged(getWidth(), getHeight(), 0, 0);
        b = i;
        a.onProgressChanged(this, i, true);
        if (i != b)
        {
            b = i;
            a.onProgressChanged(this, i, true);
        }
        invalidate();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }
}
