package com.cool.bassbooster;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.audiofx.BassBoost;
import android.os.Bundle;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.VerticalSeekBar;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar.OnSeekBarChangeListener;
//import com.desaxedstudios.bassbooster.AboutActivity;
//import com.desaxedstudios.bassbooster.BassBoosterApplication;
//import com.desaxedstudios.bassbooster.BassBoosterService;
//import com.desaxedstudios.bassbooster.SettingsActivity;
//import com.desaxedstudios.bassbooster.g;
//import com.desaxedstudios.bassbooster.h;
//import com.desaxedstudios.bassbooster.i;
//import com.desaxedstudios.bassbooster.j;
//import com.desaxedstudios.bassbooster.k;
//import com.desaxedstudios.bassbooster.m;
//import com.desaxedstudios.bassbooster.checkBox;
//import com.desaxedstudios.bassbooster.o;
//import com.desaxedstudios.bassbooster.y;
import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("ResourceType")
public abstract class f extends AppCompatActivity implements OnItemSelectedListener, OnCheckedChangeListener, OnSeekBarChangeListener {
   protected ArrayList a = new ArrayList();
   protected ArrayList b = new ArrayList();
   protected Spinner c = null;
   protected short num_bands = 6;
   protected SharedPreferences preferences = null;
   protected Editor editor = null;
   protected Resources resources;
   protected Intent h = null;
   protected BassBoosterService i = null;
//   protected o j = null;
   protected IntentFilter k = new IntentFilter();
   protected Builder l = null;
   protected RelativeLayout m;
   private CheckBox checkBox = null;
   private SeekBar seekBar = null;
   private short p = 800;
   private boolean q = false;
   private boolean bCompatible_key = true;
   private AudioManager s = null;
   private ArrayList t = new ArrayList();
   private ArrayList u = new ArrayList();
   private CheckBox v = null;
   private boolean w = false;
   private boolean eComp_key = true;
   private boolean y = false;
   private ServiceConnection z = null;

   private void a(int var1, double var2, TextView var4) {
      var4.setEnabled(this.w);
      int var5 = (int)var2;
      if(var2 > 333.0D) {
         var5 = this.preferences.getInt("equalizer_value_key" + String.valueOf(var1), 0) / 100;
      }

      String var6 = var5 + "dB\n";
      if(this.num_bands < 6) {
         switch(var1) {
         case 0:
            var6 = var6 + "100";
            break;
         case 1:
            var6 = var6 + "300";
            break;
         case 2:
            var6 = var6 + "1k";
            break;
         case 3:
            var6 = var6 + "3k";
            break;
         case 4:
            var6 = var6 + "10k";
         }
      } else {
         switch(var1) {
         case 0:
            var6 = var6 + "15";
            break;
         case 1:
            var6 = var6 + "62";
            break;
         case 2:
            var6 = var6 + "250";
            break;
         case 3:
            var6 = var6 + "1k";
            break;
         case 4:
            var6 = var6 + "5k";
            break;
         case 5:
            var6 = var6 + "16k";
         }
      }

      var4.setText(var6);
      if(var4.getLineCount() > 2) {
         var4.setText(var6.replace("dB", ""));
      }

   }

   // $FF: synthetic method
   static void a(f var0, int var1, double var2, TextView var4) {
      var0.a(var1, var2, var4);
   }

   // $FF: synthetic method
   static boolean a(f var0) {
      return var0.q;
   }

   // $FF: synthetic method
   static short b(f var0) {
      return var0.p;
   }

   // $FF: synthetic method
   static CheckBox c(f var0) {
      return var0.checkBox;
   }

   // $FF: synthetic method
   static SeekBar d(f var0) {
      return var0.seekBar;
   }

   //esta es la parte de los estilos de los seekers
   private void d(int var1) {
      int var2;
      label58:
      switch(var1) {
      case 1:
         this.seekBar.setLayoutParams(new LayoutParams(-1, -2));
         LayoutParams var3 = new LayoutParams(-2, -1);
         var3.gravity = 1;
         var2 = 0;

         while(true) {
            if(var2 >= this.a.size()) {
               break label58;
            }

            ((VerticalSeekBar)this.a.get(var2)).setLayoutParams(var3);
            ++var2;
         }
      case 2:
         this.seekBar.setProgressDrawable(this.resources.getDrawable(R.drawable.vintage_bb_progress));
         var2 = 0;

         while(true) {
            if(var2 >= this.a.size()) {
               break label58;
            }

            ((VerticalSeekBar)this.a.get(var2)).setProgressDrawable(this.resources.getDrawable(R.drawable.vintage_bb_progress));
            ++var2;
         }
      case 3:
         this.seekBar.setProgressDrawable(this.resources.getDrawable(R.drawable.fire_bb_progress));
         var2 = 0;

         while(true) {
            if(var2 >= this.a.size()) {
               break label58;
            }

            ((VerticalSeekBar)this.a.get(var2)).setProgressDrawable(this.resources.getDrawable(R.drawable.fire_bb_progress));
            ++var2;
         }
      case 4:
         this.seekBar.setProgressDrawable(this.resources.getDrawable(R.drawable.green_bb_progress));
         var2 = 0;

         while(true) {
            if(var2 >= this.a.size()) {
               break label58;
            }

            ((VerticalSeekBar)this.a.get(var2)).setProgressDrawable(this.resources.getDrawable(R.drawable.green_bb_progress));
            ++var2;
         }
      case 5:
         this.seekBar.setProgressDrawable(this.resources.getDrawable(R.drawable.colored_bb_progress));
         var2 = 0;

         while(true) {
            if(var2 >= this.a.size()) {
               break label58;
            }

            ((VerticalSeekBar)this.a.get(var2)).setProgressDrawable(this.resources.getDrawable((new int[]{2130837528, 2130837529, 2130837530, 2130837531, 2130837532, 2130837533})[var2]));
            ++var2;
         }
      default:
         this.seekBar.setProgressDrawable(this.resources.getDrawable(R.drawable.seekbar_bb_progress));

         for(var2 = 0; var2 < this.a.size(); ++var2) {
            ((VerticalSeekBar)this.a.get(var2)).setProgressDrawable(this.resources.getDrawable(R.drawable.seekbar_bb_progress));
         }
      }

      if(var1 != 1) {
         this.k();
      }

   }

   // $FF: synthetic method
   static boolean e(f var0) {
      return var0.w;
   }

   // $FF: synthetic method
   static CheckBox f(f var0) {
      return var0.v;
   }

   // $FF: synthetic method
   static ArrayList g(f var0) {
      return var0.u;
   }

   // $FF: synthetic method
   static ArrayList h(f var0) {
      return var0.t;
   }

   private void k() {
      Drawable var2 = this.resources.getDrawable(R.drawable.ic_thumb_transp);
      this.seekBar.setThumb(var2);

      for(int var1 = 0; var1 < this.a.size(); ++var1) {
         ((VerticalSeekBar)this.a.get(var1)).setThumb(var2);
      }

   }

   int a(int var1) {
      return var1 / 10 + 100;
   }

   void a() {
      this.bindService(new Intent(this, BassBoosterService.class), this.z, 1);
   }

   int b(int var1) {
      return (var1 - 100) * 10;
   }

   protected abstract void b();

   double c(int var1) {
      return ((double)var1 - 100.0D) / 10.0D;
   }

   protected abstract void c();

   public void d() {
      ArrayAdapter var2 = new ArrayAdapter(this, R.layout.spinner_selected, new ArrayList(Arrays.asList(this.getResources().getStringArray(R.array.preset_array))));

      for(int var1 = 0; var1 < this.preferences.getInt("number_of_saved_custom_presets", 0); ++var1) {
         var2.add(this.preferences.getString("equalizer_custom_values_key" + String.valueOf(var1 + 1) + "_name", "Custom Preset #" + (var1 + 1)));
      }

      var2.setDropDownViewResource(R.layout.drop_down_spinner);
      this.c.setAdapter(var2);
      this.c.setSelection(this.preferences.getInt("equalizer_selected_preset_key", 0));
   }

   @TargetApi(11)
   public void e() {
      for(short var1 = 0; var1 < this.num_bands; ++var1) {
         if(VERSION.SDK_INT >= 11 && this.preferences.getInt("key_app_theme", 0) != 1) {
            ObjectAnimator var2 = ObjectAnimator.ofInt(this.a.get(var1), "progress", new int[]{this.a(this.preferences.getInt("equalizer_value_key" + String.valueOf(var1), 0))});
            var2.setDuration(400L);
            var2.setInterpolator(new DecelerateInterpolator());
            var2.start();
         } else {
            ((VerticalSeekBar)this.a.get(var1)).setProgressAndThumb(this.a(this.preferences.getInt("equalizer_value_key" + String.valueOf(var1), 0)));
         }

         this.a(var1, 1000.0D, (TextView)this.t.get(var1));
      }

   }

   public boolean f() {
      this.bCompatible_key = true;

      BassBoost var1;
      try {
         var1 = new BassBoost(0, 0);
         var1.setStrength((short)800);
         var1.setEnabled(true);
         var1.setEnabled(false);
         var1.release();
      } catch (Exception var3) {
         this.bCompatible_key = false;
      }

      if(!this.bCompatible_key) {
         try {
            var1 = new BassBoost(0, 0);
            var1.setStrength((short)800);
            var1.setEnabled(true);
            var1.setEnabled(false);
            var1.release();
            this.bCompatible_key = true;
         } catch (Exception var2) {
            this.bCompatible_key = false;
         }
      }

      return this.bCompatible_key;
   }

   public boolean g() {
      // $FF: Couldn't be decompiled
      return true;
   }

   public void h() {
      if(this.i != null) {
         this.i.a();//equalizador
         this.i.c();//notificacion
         this.i.b();
      }

   }

   public void i() {
      if(this.i != null) {
         this.i.a(3);// pongo el bajo a pinchar
         this.i.c();// la notificacion
         this.i.b();//
      }

   }

   public void j() {
      this.editor.putBoolean("customization_update_needed", false).apply();
      Intent var1 = this.getIntent();
      this.overridePendingTransition(0, 0);
      var1.addFlags(65536);
      this.finish();
      this.overridePendingTransition(0, 0);
      this.startActivity(var1);
   }

   public void onCheckedChanged(CompoundButton var1, boolean var2) {
      boolean var4 = false;
      if(var2 && !this.preferences.getBoolean("KEY_SHOWED_TUTORIAL", false)) {
         this.editor.putBoolean("KEY_SHOWED_TUTORIAL", true);
         Builder var5 = new Builder(this);
         var5.setTitle(R.string.dialog_tutorial_title);
         var5.setMessage(R.string.dialog_tutorial_message);
//         var5.setIcon(android.R.drawable.ic_dialog_info);
         var5.setPositiveButton(this.getResources().getString(17039370), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
            }
         });
         var5.show();
      }

      String var6;
      switch(var1.getId()) {
      case R.id.bassBoostCheckbox:
         if(this.preferences.getBoolean("bassboost_enabled_key", true) != var2) {
            if(var2) {
               var6 = "bb_enabled";
            } else {
               var6 = "bb_disabled";
            }

//            BassBoosterApplication.a("UI - SFX", var6);
         }

         this.q = var2;
         this.editor.putBoolean("bassboost_enabled_key", this.q).apply();
         this.seekBar.setEnabled(this.q);
         if(this.seekBar.getProgress() == 100) {
            this.seekBar.setProgress(99);
            this.seekBar.setProgress(100);
         } else {
            this.seekBar.setProgress(this.seekBar.getProgress() + 1);
            this.seekBar.setProgress(this.seekBar.getProgress() - 1);
         }

         if(this.q) {
            this.checkBox.setText(this.getString(R.string.bbCheckboxTitle) + ": " + this.p / 10 + "%");
         } else {
            this.checkBox.setText(this.getString(R.string.bbCheckboxTitle));
         }

         if(this.q && !this.preferences.getBoolean("force_bass_boost_everywhere", false)) {
            boolean var8 = var4;
            if(!this.s.isWiredHeadsetOn()) {
               var8 = var4;
               if(!this.s.isBluetoothA2dpOn()) {
                  var8 = var4;
                  if(!this.s.isBluetoothScoOn()) {
                     var8 = true;
                  }
               }
            }

            if(var8) {
               Builder var7 = new Builder(this);
               var7.setTitle(R.string.noExternalAudioDeviceTitle);
               var7.setMessage(R.string.noExternalAudioDevice);
//               var7.setIcon(android.R.drawable.ic_menu_info_details);
               var7.setPositiveButton(this.getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                     dialog.dismiss();
                  }
               });
               var7.show();
            }
         }
      case 2131492883:
      case 2131492884:
      default:
         break;
      case R.id.equalizerCheckbox:
         if(this.preferences.getBoolean("equalizer_enabled_key", true) != var2) {
            if(var2) {
               var6 = "eq_enabled";
            } else {
               var6 = "eq_disabled";
            }

//            BassBoosterApplication.a("UI - SFX", var6);
         }

         this.w = var2;
         this.editor.putBoolean("equalizer_enabled_key", this.w).apply();

         for(int var3 = 0; var3 < this.num_bands; ++var3) {
            ((VerticalSeekBar)this.a.get(var3)).setEnabled(this.w);
//            if(((VerticalSeekBar)this.a.get(var3)).getProgress() == 200) {
//               ((VerticalSeekBar)this.a.get(var3)).setProgressAndThumb(199);
//               ((VerticalSeekBar)this.a.get(var3)).setProgressAndThumb(200);
//            } else {
//               ((VerticalSeekBar)this.a.get(var3)).setProgressAndThumb(((VerticalSeekBar)this.a.get(var3)).getProgress() + 1);
//               ((VerticalSeekBar)this.a.get(var3)).setProgressAndThumb(((VerticalSeekBar)this.a.get(var3)).getProgress() - 1);
//            }
//            ((TextView)this.t.get(var3)).setEnabled(this.w);
         }
//
         this.c.setEnabled(this.w);
      }

      this.startService(this.h);
      if(this.i != null) {
         this.i();
         this.h();
         this.c();
      }

   }

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.m = (RelativeLayout)RelativeLayout.inflate(this, R.layout.activity_home, (ViewGroup)null);
      this.setContentView(this.m);
      this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
      this.editor = this.preferences.edit();
      this.resources = this.getResources();
      this.eComp_key = this.preferences.getBoolean("equalizer_compatible_key", true);
      this.bCompatible_key = this.preferences.getBoolean("bassboost_compatible_key", true);
      this.num_bands = (short)this.preferences.getInt("number_of_eq_bands_key", 5);
      boolean var2;
      if(this.preferences.getInt("CURRENT_VERSION_1ST", 0) == 0) {
         var2 = true;
      } else {
         var2 = false;
      }

      Log.d("ABassBoosterActivity", "KEY_FRESH_INSTALL = " + this.preferences.getInt("CURRENT_VERSION_1ST", 0));
//      if(var2 || !this.eComp_key || !this.bCompatible_key) {
//         int var3;
//         try {
//            var3 = this.getPackageManager().getPackageInfo(this.getPackageName(), 1).versionCode;
//         } catch (NameNotFoundException var4) {
//            var3 = 1;
//         }
//
//         Log.d("ABassBoosterActivity", "v = " + var3);
//         this.editor.putInt("CURRENT_VERSION_1ST", var3);
//         this.editor.putInt("equalizer_selected_preset_key", 0);
//         this.editor.putBoolean("equalizer_enabled_key", false);
//         this.editor.putBoolean("bassboost_enabled_key", false);
//         this.editor.apply();
//         this.w = false;
//         this.q = false;
//         this.eComp_key = this.g();
//         this.editor.putBoolean("equalizer_compatible_key", this.eComp_key).apply();
//         this.bCompatible_key = this.f();
//         this.editor.putBoolean("bassboost_compatible_key", this.bCompatible_key).apply();
//         if(!this.eComp_key && !this.bCompatible_key) {
//            this.l = new Builder(this);
//            this.l.setTitle(this.getString(2131034182));
//            this.l.setMessage(this.getString(2131034185));
//            this.l.setIcon(17301659);
//           // this.l.setPositiveButton(this.getString(17039370), new g(this));
//            //this.l.setNegativeButton("www.desaxed.com", new h(this));
//            this.l.show();
//         } else if(!this.bCompatible_key && !this.preferences.getBoolean("bassboost_not_compatible_warning_was already_shown", false)) {
//            this.l = new Builder(this);
//            this.l.setTitle(this.getString(2131034182));
//            this.l.setMessage(this.getString(2131034152));
//            this.l.setIcon(17301659);
////            this.l.setPositiveButton(this.getString(17039370), new i(this));
//            this.l.show();
//         } else if(!this.eComp_key && !this.preferences.getBoolean("equalizer_not_compatible_warning_was already_shown", false)) {
//            this.l = new Builder(this);
//            this.l.setTitle(this.getString(2131034182));
//            this.l.setMessage(this.getString(2131034180));
//            this.l.setIcon(17301659);
//           // this.l.setPositiveButton(this.getString(17039370), new j(this));
//            this.l.show();
//         } else if(var2) {
////            this.startActivity(new Intent(this, AboutActivity.class));
//            Toast.makeText(this, 2131034242, 1).show();
//         }
//      }

    /*  if(!this.bCompatible_key) {
         this.editor.putBoolean("bassboost_enabled_key", false).apply();
      }

      if(!this.eComp_key) {
         this.editor.putBoolean("equalizer_enabled_key", false).apply();
      }*/

      this.bCompatible_key = this.preferences.getBoolean("bassboost_compatible_key", true);
      this.q = this.preferences.getBoolean("bassboost_enabled_key", false);
      this.p = (short)this.preferences.getInt("bassboost_strength_key", 800);
      this.eComp_key = this.preferences.getBoolean("equalizer_compatible_key", true);
      this.w = this.preferences.getBoolean("equalizer_enabled_key", false);
      this.num_bands = (short)this.preferences.getInt("number_of_eq_bands_key", 5);
      this.t.add((TextView)this.findViewById(R.id.band0FreqTitle));
      this.t.add((TextView)this.findViewById(R.id.band1FreqTitle));
      this.t.add((TextView)this.findViewById(R.id.band2FreqTitle));
      this.t.add((TextView)this.findViewById(R.id.band3FreqTitle));
      this.t.add((TextView)this.findViewById(R.id.band4FreqTitle));
      this.t.add((TextView)this.findViewById(R.id.band5FreqTitle));
      this.u.add((LinearLayout)this.findViewById(R.id.band0Layout));
      this.u.add((LinearLayout)this.findViewById(R.id.band1Layout));
      this.u.add((LinearLayout)this.findViewById(R.id.band2Layout));
      this.u.add((LinearLayout)this.findViewById(R.id.band3Layout));
      this.u.add((LinearLayout)this.findViewById(R.id.band4Layout));
      this.u.add((LinearLayout)this.findViewById(R.id.band5Layout));
      this.seekBar = (SeekBar)this.findViewById(R.id.strengthSeekBar);
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand0));
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand1));
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand2));
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand3));
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand4));
      this.a.add((VerticalSeekBar) this.findViewById(R.id.eqBand5));
      this.checkBox = (CheckBox)this.findViewById(R.id.bassBoostCheckbox);
      this.v = (CheckBox)this.findViewById(R.id.equalizerCheckbox);
      this.c = (Spinner)this.findViewById(R.id.presetSpinner);
      this.checkBox.setOnCheckedChangeListener(this);
      this.seekBar.setOnSeekBarChangeListener(this);
      for(int var5 = 0; var5 < this.num_bands; ++var5) {
         ((VerticalSeekBar)this.a.get(var5)).setOnSeekBarChangeListener(this);
      }
      this.c.setOnItemSelectedListener(this);
      this.v.setOnCheckedChangeListener(this);
      this.d(this.preferences.getInt("key_app_theme", 0));
      this.h = new Intent(this, BassBoosterService.class);
      this.startService(this.h);
      this.a();//conectar con el servicio
      this.k = new IntentFilter("com.desaxedstudios.bassbooster.REFRESH_ACTIVITY");
      this.s = (AudioManager)this.getSystemService("audio");
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.main_menu, var1);
      return true;
   }

   public void onDestroy() {
      this.unbindService(this.z);
      super.onDestroy();
   }

   @SuppressLint({"NewApi"})
   public void onItemSelected(AdapterView var1, View var2, int var3, long var4) {
      ArrayList var7 = (new y(this)).a(var3, this.num_bands);
      int var6;
      if(var7 != null) {
         for(var6 = 0; var6 < this.num_bands; ++var6) {
            this.editor.putInt("equalizer_value_key" + String.valueOf(var6), ((Integer) var7.get(var6)).intValue());
         }

         this.editor.apply();
         this.e();
      }

      this.editor.putInt("equalizer_selected_preset_key", var3).apply();
      this.h();
      var6 = this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var3 - 21) + "_vol", -1);
      if(var6 >= 0) {
         ((AudioManager)this.getSystemService("audio")).setStreamVolume(3, var6, 0);
      }

      String[] var8 = this.resources.getStringArray(R.array.preset_array);
      if(this.y) {
         this.y = false;
      } else if(var3 != 0 && var3 < var8.length && this.w) {
       //  BassBoosterApplication.a("UI - SFX", "eq_preset", var8[var3]);
      }

   }

   public void onNothingSelected(AdapterView var1) {
      this.e();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      int var2;
      boolean var3;
      switch(var1.getItemId()) {
      case R.id.menu_settings:
         this.startActivity(new Intent(this, SettingsActivity.class));
         var3 = true;
         break;
      case 2131492924:
        // BassBoosterApplication.a("UI - Main", "menu_save");
         if(this.c.isEnabled() && this.c.getSelectedItemPosition() != 3) {
            this.y = true;
         }

         for(var2 = 0; var2 < this.num_bands && this.preferences.getBoolean("equalizer_compatible_key", true); ++var2) {
            this.b.set(var2, Integer.valueOf(this.preferences.getInt("equalizer_value_key" + String.valueOf(var2), 0)));
            this.editor.putInt("equalizer_custom_values_key" + var2, ((Integer) this.b.get(var2)).intValue());
         }

         this.editor.apply();
         this.c.setSelection(3);
         Toast.makeText(this, this.getString(2131034156), 1).show();
         var3 = true;
         break;
      case 2131492925:
      //   BassBoosterApplication.a("UI - Main", "menu_reset_eq");

         for(var2 = 0; var2 < this.num_bands && this.preferences.getBoolean("equalizer_compatible_key", true); ++var2) {
            this.editor.putInt("equalizer_value_key" + String.valueOf(var2), 0);
         }

         this.editor.apply();
         this.e();
         this.c.setSelection(0);
         this.h();
         var3 = true;
         break;
      case 2131492926:
       //  this.startActivity(new Intent(this, AboutActivity.class));
         var3 = true;
         break;
      case 2131492927:
        // BassBoosterApplication.a("UI - Main", "menu_quit");
         Toast.makeText(this, 2131034243, 1).show();
         this.editor.putBoolean("equalizer_enabled_key", false);
         this.editor.putBoolean("bassboost_enabled_key", false);
         this.editor.apply();
         this.h();
         this.i();
         this.finish();
         var3 = true;
         break;
      default:
         var3 = super.onOptionsItemSelected(var1);
      }

      return var3;
   }

   public void onPause() {
//      if(this.j != null) {
//         try {
//            this.unregisterReceiver(this.j);
//         } catch (IllegalArgumentException var2) {
//            Log.num_bands("ABassBoosterActivity", "unregisterReceiver called on unregistered receiver in AbstractBassBoosterActivity. Please ignore.");
//         }
//      }

      super.onPause();
   }

   public void onProgressChanged(SeekBar var1, int var2, boolean var3) {
      switch(var1.getId()) {
      case R.id.strengthSeekBar:
         this.p = (short)(var2 * 10);
         if(this.q) {
            this.checkBox.setText(this.getString(R.string.bbCheckboxTitle) + ": " + this.p / 10 + "%");
         } else {
            this.checkBox.setText(this.getString(R.string.bbCheckboxTitle));
         }
         break;
      case R.id.eqBand0:
         this.a(0, this.c(var2), (TextView)this.t.get(0));
         break;
      case R.id.eqBand1:
         this.a(1, this.c(var2), (TextView)this.t.get(1));
         break;
      case R.id.eqBand2:
         this.a(2, this.c(var2), (TextView)this.t.get(2));
         break;
      case R.id.eqBand3:
         this.a(3, this.c(var2), (TextView)this.t.get(3));
         break;
      case R.id.eqBand4:
         this.a(4, this.c(var2), (TextView)this.t.get(4));
         break;
      case R.id.eqBand5:
         this.a(5, this.c(var2), (TextView)this.t.get(5));
      }

   }

   @SuppressLint({"NewApi"})
   public void onResume() {
      if(this.preferences.getBoolean("customization_update_needed", false)) {
         this.j();
      }

//      if(this.j == null) {
//         //this.j = new o(this, (resources)null);
//      }

      //this.registerReceiver(this.j, this.k);
      this.i();
      this.h();

//      for(int var1 = 0; var1 < this.num_bands; ++var1) {
//         ((VerticalSeekBar)this.a.get(var1)).setProgressAndThumb(this.a(this.preferences.getInt("equalizer_value_key" + String.valueOf(var1), 0)));
//         this.a(var1, 1000.0D, (TextView)this.t.get(var1));
//      }

      this.c.setSelection(this.preferences.getInt("equalizer_selected_preset_key", 0));
      this.v.setChecked(this.preferences.getBoolean("equalizer_enabled_key", false));
      this.checkBox.setChecked(this.preferences.getBoolean("bassboost_enabled_key", false));
      this.seekBar.setProgress(this.preferences.getInt("bassboost_strength_key", 800) / 10);
      this.b();
      super.onResume();
   }

   public void onStartTrackingTouch(SeekBar var1) {
   }

   public void onStopTrackingTouch(SeekBar var1) {
      byte var2;
      switch(var1.getId()) {
      case R.id.strengthSeekBar:
        // BassBoosterApplication.a("UI - SFX", "bb_value", var1.getProgress());
         this.editor.putInt("bassboost_strength_key", this.seekBar.getProgress() * 10).apply();
         this.i();
         var2 = -1;
         break;
      case R.id.eqBand0:
         var2 = 0;
         break;
      case R.id.eqBand1:
         var2 = 1;
         break;
      case R.id.eqBand2:
         var2 = 2;
         break;
      case R.id.eqBand3:
         var2 = 3;
         break;
      case R.id.eqBand4:
         var2 = 4;
         break;
      case R.id.eqBand5:
         var2 = 5;
         break;
      default:
         var2 = -1;
      }

      if(var2 > -1) {
        // BassBoosterApplication.a("UI - SFX", "eq_value" + var2, var1.getProgress());
         this.c.setSelection(0);
         this.editor.putInt("equalizer_value_key" + String.valueOf(var2), this.b(var1.getProgress())).apply();
         this.h();
      }

   }
}
