package com.cool.bassbooster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
//import com.desaxedstudios.bassbooster.p;
//import com.desaxedstudios.bassbooster.r;

class q extends BroadcastReceiver {
   // $FF: synthetic field
   final p a;

   q(p var1) {
      this.a = var1;
   }

   public void onReceive(Context var1, Intent var2) {
      byte var3 = 1;
      if(this.a.a.getBoolean("bassboost_enabled_key", false) && !this.a.a.getBoolean("force_bass_boost_everywhere", true)) {
         String var4 = var2.getAction();
         if(!var4.equals("android.media.AUDIO_BECOMING_NOISY")) {
            label32: {
               if(var4.equals("android.intent.action.HEADSET_PLUG") && var2.hasExtra("state")) {
                  if(var2.getIntExtra("state", 0) == 0) {
                     break label32;
                  }
                  if(var2.getIntExtra("state", 0) == 1) {
                     var3 = 0;
                     break label32;
                  }
               }
               var3 = 3;
            }
         }
         if(var3 != 3) {
            this.a.a(var3);
         } else {
            (new Handler()).postDelayed(new Runnable() {
               @Override
               public void run() {
                  q.this.a.a(3);
               }
            }, 1000L);
         }
      }

   }
}
