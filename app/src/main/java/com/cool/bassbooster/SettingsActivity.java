package com.cool.bassbooster;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
//import android.support.v4.app.av;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
//import com.desaxedstudios.bassbooster.BassBoosterApplication;
//import com.desaxedstudios.bassbooster.BassBoosterService;
//import com.desaxedstudios.bassbooster.aa;
//import com.desaxedstudios.bassbooster.z;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("ResourceType")
public class SettingsActivity extends AppCompatActivity implements OnClickListener, OnItemSelectedListener, OnCheckedChangeListener {
   private SharedPreferences a = null;
   private Editor b = null;
   private CheckBox c = null;
   private CheckBox d = null;
   private CheckBox e = null;
   private CheckBox f = null;
   private CheckBox g = null;
   private CheckBox onCall = null;
   private CheckBox i = null;
   private CheckBox j = null;
   private CheckBox k = null;
   private CheckBox duringCall = null;
   private Spinner m = null;
   private Spinner n = null;
   private Spinner o = null;
   private Spinner p = null;
   private Button q = null;
   private Resources r = null;
   private int s = 0;

   // $FF: synthetic method
   static Editor a(SettingsActivity var0) {
      return var0.b;
   }

   private void b() {
//      this.e.setEnabled(false);
//      this.f.setEnabled(false);
//      this.i.setEnabled(false);
//      this.n.setEnabled(false);
////      this.j.setEnabled(false);
////      this.k.setEnabled(false);
//      this.o.setEnabled(false);
//      this.p.setEnabled(false);
//      this.h.setEnabled(false);
//      this.l.setEnabled(false);
   }

   public void a(final int var1) {
      View var3 = View.inflate(this, R.layout.dialog_checkbox, (ViewGroup)null);
      ((TextView)var3.findViewById(R.id.dialogMessage)).setText(getString(var1));
//      ((CheckBox)var3.findViewById(R.id.dialogCheckbox)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
//         @Override
//         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            String var3;
//            switch (buttonView.getId()) {
//               case 2131034187:
//                  var3 = "do_not_show_warning_for_force_bassboost";
//                  break;
//               case 2131034214:
//                  var3 = "do_not_show_warning_for_on_call";
//                  break;
//               default:
////                  text.setText(getString(R.string.runInBackgroundWarning));
//                  var3 = "do_not_show_warning_for_background_process";
//            }
//            SettingsActivity.a(SettingsActivity.this).putBoolean(var3, isChecked).apply();
//         }
//      });
      AlertDialog.Builder var2 = new AlertDialog.Builder(this);
      var2.setTitle(R.string.important);
      var2.setMessage(getString(var1));
//      var2.setIcon(android.R.drawable.stat_sys_warning);
      //var2.setView(var3);
      var2.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
         @Override
         public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
         }
      });
      var2.show();
   }

   public void onCheckedChanged(CompoundButton var1, boolean var2) {
      StringBuilder var3;
      String var4;
      switch(var1.getId()) {
      case R.id.processCheckBox:
         if(this.a.getBoolean("run_in_background", true) != var2) {
            var3 = (new StringBuilder()).append("settings_update_");
            if(var2) {
               var4 = "enabled";
            } else {
               var4 = "disabled";
            }

//            BassBoosterApplication.a("UI - Settings", var3.append(var4).toString());
         }

         this.b.putBoolean("run_in_background", var2).apply();
         if(!var2 && !this.a.getBoolean("do_not_show_warning_for_background_process", false)) {
            this.a(R.string.runInBackgroundWarning);
         }
         break;
      case R.id.iconCheckBox:
         if(this.a.getBoolean("show_status_icon", true) != var2) {
            var3 = (new StringBuilder()).append("settings_statusicon_");
            if(var2) {
               var4 = "enabled";
            } else {
               var4 = "disabled";
            }

//            BassBoosterApplication.a("UI - Settings", var3.append(var4).toString());
         }

         this.b.putBoolean("show_status_icon", var2).apply();
         break;
      case R.id.forceBassBoostCheckBox:
         if(this.a.getBoolean("force_bass_boost_everywhere", true) != var2) {
            var3 = (new StringBuilder()).append("settings_force_");
            if(var2) {
               var4 = "enabled";
            } else {
               var4 = "disabled";
            }

//            BassBoosterApplication.a("UI - Settings", var3.append(var4).toString());
         }

         this.b.putBoolean("force_bass_boost_everywhere", var2).apply();
         if(var2 && !this.a.getBoolean("do_not_show_warning_for_force_bassboost", false)) {
            this.a(R.string.forceBassBoostWarning);
         }
      case 2131492908:
      case 2131492909:
      case 2131492913:
      case 2131492914:
      case 2131492918:
      default:
         break;
      case R.id.startOnBootCheckBox:
         this.b.putBoolean("auto_start_on_phone_boot", var2).apply();
         break;
      case 2131492911:
         this.b.putBoolean("auto_detect_current_preset", var2).apply();
         break;
      case 2131492912:
         this.b.putBoolean("if_no_preset_could_be_detected_then_switch_back", var2).apply();
         break;
      case R.id.checkbox_visualizer:
         this.b.putBoolean("show_visualizer", var2).putBoolean("customization_update_needed", true).apply();
         break;
      case R.id.showDecimalCheckBox:
         this.b.putBoolean("use_decimal_precision_in_eq", var2).apply();
         break;
      case R.id.onCallPresetCheckBox:
         this.b.putBoolean("change_eq_preset_on_call", var2).apply();
         if(var2){
            this.o.setEnabled(true);
         }
         else
         this.o.setEnabled(false);
         if(var2 && !this.a.getBoolean("do_not_show_warning_for_on_call", false)) {
            this.a(R.string.onCallWarning);
         }
         break;
      case R.id.duringCallPresetCheckBox:
         if(var2)
            this.p.setEnabled(true);
         else
            this.p.setEnabled(false);
         this.b.putBoolean("change_eq_preset_during_call", var2).apply();
         if(var2 && !this.a.getBoolean("do_not_show_warning_for_on_call", false)) {
            this.a(R.string.onCallWarning);
         }
      }

      this.startService(new Intent(this.getApplicationContext(), BassBoosterService.class));
      this.b();
   }

   public void onClick(View var1) {
      switch(var1.getId()) {
      case 2131492871:
//         BassBoosterApplication.a("UI - Settings", "settings_buy");
         switch(0) {
         case 1:
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=com.desaxedstudios.bassboosterpro")));
            break;
         case 2:
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("samsungapps://ProductDetail/com.desaxedstudios.bassboosterpro")));
            break;
         case 3:
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mma://app/769ffb2d-199a-46fd-a592-f9a17c25b86c")));
            break;
         default:
            this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.desaxedstudios.bassboosterpro")));
         }
      default:
      }
   }

   @SuppressLint({"CommitPrefEdits"})
   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.activity_preferences);
      Toolbar toolbar= (Toolbar)findViewById(R.id.toolbar);
      final SpannableStringBuilder sb = new SpannableStringBuilder(getString(R.string.settings));

// Span to set text color to some RGB value
      final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);

// Span to make text bold
      final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

// Set the text color for first 4 characters
      sb.setSpan(fcs, 0, getString(R.string.settings).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

// make them also bold
      sb.setSpan(bss, 0, getString(R.string.settings).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

      Typeface font = Typeface.createFromAsset(getAssets(), "font.otf");
      sb.setSpan(new CustomTypefaceSpan("" , font), 0 , getString(R.string.settings).length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);

      toolbar.setTitle(sb);
      toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_action_overflow));
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_action_back));
      getSupportActionBar().setHomeButtonEnabled(true);
      this.a = PreferenceManager.getDefaultSharedPreferences(this);
      this.b = this.a.edit();
      this.r = this.getResources();
//      BassBoosterApplication.a.a("SettingsActivity");
      this.c = (CheckBox)this.findViewById(R.id.processCheckBox);
      this.d = (CheckBox)this.findViewById(R.id.iconCheckBox);
      this.g = (CheckBox)this.findViewById(R.id.forceBassBoostCheckBox);
      this.e = (CheckBox)this.findViewById(R.id.startOnBootCheckBox);
//      this.f = (CheckBox)this.findViewById(R.id.autoDetectCheckBox);
      this.i = (CheckBox)this.findViewById(R.id.ifNothingDetectedCheckBox);
      this.j = (CheckBox)this.findViewById(R.id.checkbox_visualizer);
      this.k = (CheckBox)this.findViewById(R.id.showDecimalCheckBox);
      this.onCall = (CheckBox)this.findViewById(R.id.onCallPresetCheckBox);
      this.duringCall = (CheckBox)this.findViewById(R.id.duringCallPresetCheckBox);
      this.n = (Spinner)this.findViewById(R.id.defaultPresetSpinner);
      this.o = (Spinner)this.findViewById(R.id.onCallPresetSpinner);
      this.p = (Spinner)this.findViewById(R.id.duringCallPresetSpinner);
      this.m = (Spinner)this.findViewById(R.id.themeSpinner);
//      this.q = (Button)this.findViewById(2131492871);
      this.c.setOnCheckedChangeListener(this);
      this.d.setOnCheckedChangeListener(this);
      this.g.setOnCheckedChangeListener(this);
      this.e.setOnCheckedChangeListener(this);
//      this.f.setOnCheckedChangeListener(this);
      this.onCall.setOnCheckedChangeListener(this);
      this.i.setOnCheckedChangeListener(this);
      this.j.setOnCheckedChangeListener(this);
      this.k.setOnCheckedChangeListener(this);
      this.duringCall.setOnCheckedChangeListener(this);
      this.n.setOnItemSelectedListener(this);
      this.o.setOnItemSelectedListener(this);
      this.p.setOnItemSelectedListener(this);
      this.m.setOnItemSelectedListener(new OnItemSelectedListener() {
         @Override
         public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            SettingsActivity.this.b.putBoolean("customization_update_needed", true).putInt("key_app_theme", position).apply();
         }

         @Override
         public void onNothingSelected(AdapterView<?> parent) {

         }
      });
//      this.q.setOnClickListener(this);
      this.c.setChecked(this.a.getBoolean("run_in_background", true));
      this.d.setChecked(this.a.getBoolean("show_status_icon", true));
      this.g.setChecked(this.a.getBoolean("force_bass_boost_everywhere", false));
      this.e.setChecked(this.a.getBoolean("auto_start_on_phone_boot", false));
//      this.f.setChecked(this.a.getBoolean("auto_detect_current_preset", false));
      boolean x=this.a.getBoolean("change_eq_preset_on_call", false);
      this.onCall.setChecked(this.a.getBoolean("change_eq_preset_on_call", false));
      this.i.setChecked(this.a.getBoolean("if_no_preset_could_be_detected_then_switch_back", false));
      this.duringCall.setChecked(this.a.getBoolean("change_eq_preset_during_call", false));
      this.k.setChecked(this.a.getBoolean("use_decimal_precision_in_eq", false));

      this.j.setChecked(this.a.getBoolean("show_visualizer", false));
      ArrayAdapter var4 = new ArrayAdapter(this, R.layout.spinner_selected, new ArrayList(Arrays.asList(this.r.getStringArray(R.array.preset_array))));

      for(int var2 = 0; var2 < this.a.getInt("number_of_saved_custom_presets", 0); ++var2) {
         var4.add(this.a.getString("equalizer_custom_values_key" + String.valueOf(var2 + 1) + "_name", "Custom Preset #" + (var2 + 1)));
      }

      var4.setDropDownViewResource(R.layout.drop_down_spinner);
      this.n.setAdapter(var4);
      this.n.setSelection(this.a.getInt("default_preset_to_switch_back_to", 0));
      this.o.setAdapter(var4);
      this.o.setSelection(this.a.getInt("preset_to_set_on_call", 21));
      this.p.setAdapter(var4);
      this.p.setSelection(this.a.getInt("preset_to_set_during_call", 21));
      var4 = new ArrayAdapter(this, R.layout.spinner_selected,new ArrayList(Arrays.asList(this.r.getStringArray(R.array.themes_array))));
      var4.setDropDownViewResource(R.layout.drop_down_spinner);
      this.m.setAdapter(var4);
      this.m.setSelection(this.a.getInt("key_app_theme", 0));
      this.b();
      this.adView = (AdView)this.findViewById(R.id.adView);
      var13 = (new AdRequest.Builder()).build();
      this.adView.loadAd(var13);
      this.adView.setAdListener(new AdListener() {
         @Override
         public void onAdLoaded() {
            super.onAdLoaded();
            adView.setVisibility(View.VISIBLE);
         }
      });
   }
   private AdView adView;
   private AdRequest var13;

   int x= 2131230786;
   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.settings_menu, var1);
      return true;
   }

   public void onItemSelected(AdapterView var1, View var2, int var3, long var4) {
      int id= var1.getId();
      switch(var1.getId()) {
      case 2131492913:
         this.b.putInt("default_preset_to_switch_back_to", var3).apply();
         break;
      case 2131624127:
         this.b.putBoolean("customization_update_needed", true).putInt("key_app_theme", var3).apply();
//         String[] var6 = this.r.getStringArray(2131099650);
//         ++this.s;
//         if(this.s >= 2 && var3 < var6.length) {
////            BassBoosterApplication.a("UI - Settings", "settings_theme", var6[var3]);
//         }
      case 2131492915:
      case 2131492916:
      case 2131492917:
      case 2131492919:
      default:
         break;
      case R.id.onCallPresetSpinner:
         this.b.putInt("preset_to_set_on_call", var3).apply();
         break;
      case R.id.duringCallPresetSpinner:
         this.b.putInt("preset_to_set_during_call", var3).apply();
      }

   }

   public void onNothingSelected(AdapterView var1) {
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2 = true;
      switch(var1.getItemId()) {
      case 16908332:
         finish();
//         av.a(this);
         break;
      case R.id.menu_reset_settings:
//         BassBoosterApplication.a("UI - Settings", "settings_reset");
         this.c.setChecked(true);
         this.d.setChecked(true);
         this.g.setChecked(false);
         this.e.setChecked(false);
//         this.f.setChecked(false);
         this.i.setChecked(false);
         this.n.setSelection(0);
         this.j.setChecked(false);
         this.o.setSelection(21);
         this.k.setChecked(false);
         this.p.setSelection(21);
         this.onCall.setChecked(false);
         this.duringCall.setChecked(false);
         this.m.setSelection(0);
         this.b.putBoolean("customization_update_needed", true).apply();
         this.b.apply();
         break;

      default:
         var2 = super.onOptionsItemSelected(var1);
      }

      return var2;
   }
}
