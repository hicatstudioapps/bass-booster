package com.cool.bassbooster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
//import com.desaxedstudios.bassboosterpro.BassBoosterService;

public class BassBoosterBroadcastReceiver extends BroadcastReceiver {
   public void onReceive(final Context var1, Intent var2) {
      SharedPreferences var3 = PreferenceManager.getDefaultSharedPreferences(var1);
      if(var3.getBoolean("auto_start_on_phone_boot", true)) {
         var1.startService(new Intent(var1, BassBoosterService.class));
         if(!var3.getBoolean("run_in_background", true)) {
            (new Handler()).postDelayed(new Runnable() {
               public void run() {
                  Intent var1x = new Intent(var1, BassBoosterService.class);
                  var1.startService(var1x);
               }
            }, 30000L);
            (new Handler()).postDelayed(new Runnable() {
               public void run() {
                  Intent var1x = new Intent(var1, BassBoosterService.class);
                  var1.startService(var1x);
               }
            }, 70000L);
            (new Handler()).postDelayed(new Runnable() {
               public void run() {
                  Intent var1x = new Intent(var1, BassBoosterService.class);
                  var1.startService(var1x);
               }
            }, 90000L);
         }
      }

   }
}
