package com.cool.bassbooster;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("ResourceType")
public class PresetDialogActivity extends Activity {
   private Editor editor = null;
   int height = 0;
   private SharedPreferences preferences = null;
   private PresetRetriever presetRetriever = null;
   private ListView presetsDialogListView = null;
   int width = 0;

   public void onCreate(Bundle var1) {
      super.onCreate(var1);
      this.setContentView(R.layout.dialog_select_preset);
      this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
      this.presetsDialogListView = (ListView)this.findViewById(R.id.presetsDialogListView);
      this.presetRetriever = new PresetRetriever(this);
      this.editor = this.preferences.edit();
      String[] var4 = this.getResources().getStringArray(R.array.preset_array);
      ArrayList var3 = new ArrayList();
      int var2;
      for(var2 = 0; var2 < var4.length; ++var2) {
         var3.add(var4[var2]);
      }

//      for(var2 = 0; var2 < this.preferences.getInt("number_of_saved_custom_presets", 0); ++var2) {
//         var3.add(this.preferences.getString("equalizer_custom_values_key" + String.valueOf(var2 + 1) + "_name", "Custom Preset #" + (var2 + 1)));
//      }

      StableArrayAdapter var5 = new StableArrayAdapter(this, 0x1090003, var3);
      this.presetsDialogListView.setAdapter(var5);
      this.presetsDialogListView.setSelection(this.preferences.getInt("equalizer_selected_preset_key", 0));
      this.presetsDialogListView.setOnItemClickListener(new OnItemClickListener() {
         public void onItemClick(AdapterView var1, View var2, int var3, long var4) {
            int var7 = PresetDialogActivity.this.preferences.getInt("number_of_eq_bands_key", 5);
            PresetDialogActivity.this.editor.putInt("equalizer_selected_preset_key", var3);
            ArrayList var8 = PresetDialogActivity.this.presetRetriever.getPresetValues(var3, var7);
            if(var8 != null) {
               for(int var6 = 0; var6 < var7; ++var6) {
                  PresetDialogActivity.this.editor.putInt("equalizer_value_key" + String.valueOf(var6), ((Integer)var8.get(var6)).intValue());
               }
            }

            var3 = PresetDialogActivity.this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var3 - 21) + "_vol", -1);
            if(var3 >= 0) {
               ((AudioManager)PresetDialogActivity.this.getSystemService("audio")).setStreamVolume(3, var3, 0);
            }

            PresetDialogActivity.this.editor.apply();
            Intent var9 = new Intent(PresetDialogActivity.this, BassBoosterService.class);
            var9.putExtra("appwidget", true);
            PresetDialogActivity.this.startService(var9);
            PresetDialogActivity.this.sendBroadcast(new Intent("com.desaxedstudios.bassboosterpro.REFRESH_ACTIVITY"));
            PresetDialogActivity.this.finish();
         }
      });
   }

   private class StableArrayAdapter extends ArrayAdapter {
      HashMap mIdMap = new HashMap();

      public StableArrayAdapter(Context var2, int var3, List var4) {
         super(var2, var3, var4);

         for(var3 = 0; var3 < var4.size(); ++var3) {
            this.mIdMap.put(var4.get(var3), Integer.valueOf(var3));
         }

      }

      public long getItemId(int var1) {
         String var2 = (String)this.getItem(var1);
         return (long)((Integer)this.mIdMap.get(var2)).intValue();
      }

      public boolean hasStableIds() {
         return true;
      }
   }
}
