package com.cool.bassbooster;

import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.VerticalSeekBar;
//import com.desaxedstudios.bassbooster.f;
//import com.desaxedstudios.bassbooster.l;
//import com.desaxedstudios.bassbooster.w;

@SuppressWarnings("ResourceType")
class k implements ServiceConnection {
   // $FF: synthetic field
   final HomeActivity a;

   k(HomeActivity var1) {
      this.a = var1;
   }

   public void onServiceConnected(ComponentName var1, IBinder var2) {
      this.a.i = ((w)var2).a();
      if(HomeActivity.a(this.a)) {
         HomeActivity.c(this.a).setText(this.a.getString(R.string.bbCheckboxTitle) + ": " + HomeActivity.b(this.a) / 10 + "%");
      } else {
         HomeActivity.c(this.a).setText(this.a.getString(R.string.bbCheckboxTitle));
      }
      HomeActivity.d(this.a).setProgress(HomeActivity.b(this.a) / 10);
      HomeActivity.c(this.a).setChecked(HomeActivity.a(this.a));
      this.a.i();
//      this.a.b();
      HomeActivity.f(this.a).setChecked(HomeActivity.e(this.a));

      int var3;
      for(var3 = 0; var3 < this.a.num_bands; ++var3) {
         this.a.b.add(Integer.valueOf(this.a.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var3), 0)));
      }

      if(this.a.num_bands < 6) {
         for(var3 = 6; var3 > this.a.num_bands; --var3) {
            ((LinearLayout)HomeActivity.g(this.a).get(var3 - 1)).setVisibility(8);
         }

//         if(!this.a.preferences.getBoolean("equalizer_few_bands_warning_was already_shown", false)) {
//            Toast.makeText(this.a, this.a.getString(2131034209) + this.a.num_bands + this.a.getString(2131034210), 1).show();
//            this.a.editor.putBoolean("equalizer_few_bands_warning_was already_shown", true).apply();
//         }
      }

//      if(this.a.num_bands > 6) {
//         Builder var4 = new Builder(this.a);
//         var4.setTitle(this.a.getString(2131034194));
//         var4.setMessage(this.a.getString(2131034195) + this.a.num_bands + this.a.getString(2131034196));
//         var4.setIcon(17301659);
//         var4.setPositiveButton(this.a.getString(17039370), new l(this));
//         var4.show();
//      }

      for(var3 = 0; var3 < this.a.num_bands; ++var3) {
         ((VerticalSeekBar)this.a.a.get(var3)).setProgressAndThumb(this.a.a(this.a.preferences.getInt("equalizer_value_key" + String.valueOf(var3), 0)));
          HomeActivity.a(this.a, var3, 1000.0D, (TextView)HomeActivity.h(this.a).get(var3));
      }

      this.a.d();//cargar los pressets
      this.a.h();
   }

   public void onServiceDisconnected(ComponentName var1) {
      this.a.i = null;
   }
}
