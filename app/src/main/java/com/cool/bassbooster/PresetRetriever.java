package com.cool.bassbooster;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
//import com.desaxedstudios.bassboosterpro.C;
import java.util.ArrayList;

public class PresetRetriever {
   String artist = null;
   Context context = null;
   String genre = null;
   private SharedPreferences preferences = null;
   private ArrayList presetsNames = new ArrayList();

   public PresetRetriever(Context var1) {
      this.context = var1;
      this.preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
   }

   private void buildCleanedPresetsList() {
      this.presetsNames.clear();

      int var1;
      for(var1 = 0; var1 < this.preferences.getInt("number_of_saved_custom_presets", 0); ++var1) {
         this.presetsNames.add(this.cleanName(this.preferences.getString("equalizer_custom_values_key" + String.valueOf(var1 + 1) + "_name", "Custom Preset #" + (var1 + 1))));
      }

      String[] var2 = this.context.getResources().getStringArray(R.array.preset_to_detect_array);

      for(var1 = 0; var1 < var2.length; ++var1) {
         this.presetsNames.add(this.cleanName(var2[var1]));
      }

   }

   @SuppressLint({"DefaultLocale"})
   private String cleanName(String var1) {
      return var1.toLowerCase().trim().replace("(", "").replace(")", "").replace("-", "").replace("+", "").replace("/", "").replace(".", "").replace("&", "and").replace(" ", "");
   }

   private int convertPriorityToPreset(int var1) {
      if(var1 + 1 <= this.preferences.getInt("number_of_saved_custom_presets", 0)) {
         var1 = var1 + 1 + 21;
      } else {
         var1 -= this.preferences.getInt("number_of_saved_custom_presets", 0);
      }

      return var1;
   }

   private ArrayList get5BandsPresetValues(int var1) {
      ArrayList var4 = new ArrayList();
      ArrayList var3;
      if(var1 <= 21) {
         switch(var1) {
         case 0:
            var3 = null;
            break;
         case 1:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.BASS_VALUES_5[var1]));
               ++var1;
            }
         case 2:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.SPEAKERS_VALUES_5[var1]));
               ++var1;
            }
         case 3:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var1), 0)));
               ++var1;
            }
         case 4:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ELECTRO_VALUES_5[var1]));
               ++var1;
            }
         case 5:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.TECHNO_VALUES_5[var1]));
               ++var1;
            }
         case 6:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.DUBSTEP_VALUES_5[var1]));
               ++var1;
            }
         case 7:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.DANCE_VALUES_5[var1]));
               ++var1;
            }
         case 8:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.POP_VALUES_5[var1]));
               ++var1;
            }
         case 9:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ROCK_VALUES_5[var1]));
               ++var1;
            }
         case 10:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.METAL_VALUES_5[var1]));
               ++var1;
            }
         case 11:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.REGGAE_VALUES_5[var1]));
               ++var1;
            }
         case 12:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.RAP_VALUES_5[var1]));
               ++var1;
            }
         case 13:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.RANDB_VALUES_5[var1]));
               ++var1;
            }
         case 14:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(var1, Integer.valueOf(C.HIPHOP_VALUES_5[var1]));
               ++var1;
            }
         case 15:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.JAZZ_VALUES_5[var1]));
               ++var1;
            }
         case 16:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.LATINO_VALUES_5[var1]));
               ++var1;
            }
         case 17:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ACOUSTIC_VALUES_5[var1]));
               ++var1;
            }
         case 18:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.CLASSICAL_VALUES_5[var1]));
               ++var1;
            }
         case 19:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.PARTY_VALUES_5[var1]));
               ++var1;
            }
         case 20:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.VOICEBOOST_VALUES_5[var1]));
               ++var1;
            }
         case 21:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 5) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.VOLUMEBOOST_VALUES_5[var1]));
               ++var1;
            }
         default:
            var3 = null;
         }
      } else {
         int var2 = 0;

         while(true) {
            var3 = var4;
            if(var2 >= 5) {
               break;
            }

            var4.add(var2, Integer.valueOf(this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var1 - 21) + "_band" + var2, 0)));
            ++var2;
         }
      }

      return var3;
   }

   private ArrayList get6BandsPresetValues(int var1) {
      ArrayList var4 = new ArrayList();
      ArrayList var3;
      if(var1 <= 21) {
         switch(var1) {
         case 0:
            var3 = null;
            break;
         case 1:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.BASS_VALUES_6[var1]));
               ++var1;
            }
         case 2:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.SPEAKERS_VALUES_6[var1]));
               ++var1;
            }
         case 3:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var1), 0)));
               ++var1;
            }
         case 4:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ELECTRO_VALUES_6[var1]));
               ++var1;
            }
         case 5:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.TECHNO_VALUES_6[var1]));
               ++var1;
            }
         case 6:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.DUBSTEP_VALUES_6[var1]));
               ++var1;
            }
         case 7:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.DANCE_VALUES_6[var1]));
               ++var1;
            }
         case 8:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.POP_VALUES_6[var1]));
               ++var1;
            }
         case 9:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ROCK_VALUES_6[var1]));
               ++var1;
            }
         case 10:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.METAL_VALUES_6[var1]));
               ++var1;
            }
         case 11:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.REGGAE_VALUES_6[var1]));
               ++var1;
            }
         case 12:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.RAP_VALUES_6[var1]));
               ++var1;
            }
         case 13:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.RANDB_VALUES_6[var1]));
               ++var1;
            }
         case 14:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(var1, Integer.valueOf(C.HIPHOP_VALUES_6[var1]));
               ++var1;
            }
         case 15:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.JAZZ_VALUES_6[var1]));
               ++var1;
            }
         case 16:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.LATINO_VALUES_6[var1]));
               ++var1;
            }
         case 17:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.ACOUSTIC_VALUES_6[var1]));
               ++var1;
            }
         case 18:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.CLASSICAL_VALUES_6[var1]));
               ++var1;
            }
         case 19:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.PARTY_VALUES_6[var1]));
               ++var1;
            }
         case 20:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.VOICEBOOST_VALUES_6[var1]));
               ++var1;
            }
         case 21:
            var1 = 0;

            while(true) {
               var3 = var4;
               if(var1 >= 6) {
                  return var3;
               }

               var4.add(Integer.valueOf(C.VOLUMEBOOST_VALUES_6[var1]));
               ++var1;
            }
         default:
            var3 = null;
         }
      } else {
         int var2 = 0;

         while(true) {
            var3 = var4;
            if(var2 >= 6) {
               break;
            }

            var4.add(var2, Integer.valueOf(this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var1 - 21) + "_band" + var2, 0)));
            ++var2;
         }
      }

      return var3;
   }

   @SuppressLint({"DefaultLocale"})
   public int getPreset(String var1, String var2) {
      this.genre = var1;
      this.artist = var2;
      int var3;
      if(this.genre != null && !this.genre.isEmpty()) {
         this.buildCleanedPresetsList();
         byte var4 = -1;

         try {
            var3 = Integer.decode(this.cleanName(this.genre)).intValue();
         } catch (NumberFormatException var5) {
            var3 = var4;
         }

         if(var3 > -1) {
            this.genre = this.getPresetNameWithID(var3);
         }

         this.genre = this.cleanName(this.genre.toLowerCase());

         for(var3 = 0; var3 < this.presetsNames.size(); ++var3) {
            if(this.genre.equals(this.presetsNames.get(var3))) {
               var3 = this.convertPriorityToPreset(var3);
               return var3;
            }
         }

         if(this.genre.contains("dancehall")) {
            var3 = 11;
            return var3;
         }

         if(this.genre.contains("randb")) {
            var3 = 13;
            return var3;
         }

         if(this.genre.contains("trap")) {
            var3 = 6;
            return var3;
         }

         for(var3 = 0; var3 < this.presetsNames.size(); ++var3) {
            if(this.genre.contains((CharSequence)this.presetsNames.get(var3))) {
               var3 = this.convertPriorityToPreset(var3);
               return var3;
            }
         }

         for(var3 = 0; var3 < this.presetsNames.size(); ++var3) {
            if(((String)this.presetsNames.get(var3)).contains(this.genre)) {
               var3 = this.convertPriorityToPreset(var3);
               return var3;
            }
         }

         if(this.genre.contains("glitch")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("drum") && this.genre.contains("bass")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("blues")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("soul")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("punk")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("country")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("triphop")) {
            var3 = 14;
            return var3;
         }

         if(this.genre.contains("club")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("ska")) {
            var3 = 11;
            return var3;
         }

         if(this.genre.contains("glitch")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("trance")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("disco")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("funk")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("grunge")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("industrial")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("alternative")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("ambient")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("vocal")) {
            var3 = 20;
            return var3;
         }

         if(this.genre.contains("acapella")) {
            var3 = 20;
            return var3;
         }

         if(this.genre.contains("acid")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("house")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("gospel")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("bass")) {
            var3 = 1;
            return var3;
         }

         if(this.genre.contains("space")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("ethnic")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("gothic")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("dream")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("gangsta")) {
            var3 = 12;
            return var3;
         }

         if(this.genre.contains("jungle")) {
            var3 = 5;
            return var3;
         }

         if(this.genre.contains("cabaret")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("newwave")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("cabaret")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("psychadelic")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("rave")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("lofi")) {
            var3 = 17;
            return var3;
         }

         if(this.genre.contains("polka")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("merengue")) {
            var3 = 16;
            return var3;
         }

         if(this.genre.contains("salsa")) {
            var3 = 16;
            return var3;
         }

         if(this.genre.contains("swing")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("bebop")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("drum")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("bluegrass")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("bigband")) {
            var3 = 15;
            return var3;
         }

         if(this.genre.contains("chorus")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("chanson")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("opera")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("chambermusic")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("sonata")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("symphony")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("primus")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("porngroove")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("grind")) {
            var3 = 10;
            return var3;
         }

         if(this.genre.contains("slowjam")) {
            var3 = 13;
            return var3;
         }

         if(this.genre.contains("tango")) {
            var3 = 16;
            return var3;
         }

         if(this.genre.contains("samba")) {
            var3 = 16;
            return var3;
         }

         if(this.genre.contains("ballad")) {
            var3 = 8;
            return var3;
         }

         if(this.genre.contains("freestyle")) {
            var3 = 4;
            return var3;
         }

         if(this.genre.contains("duet")) {
            var3 = 18;
            return var3;
         }

         if(this.genre.contains("goa")) {
            var3 = 7;
            return var3;
         }

         if(this.genre.contains("hardcore")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("terror")) {
            var3 = 5;
            return var3;
         }

         if(this.genre.contains("speedcore")) {
            var3 = 5;
            return var3;
         }

         if(this.genre.contains("indie")) {
            var3 = 8;
            return var3;
         }

         if(this.genre.contains("beat")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("crossover")) {
            var3 = 8;
            return var3;
         }

         if(this.genre.contains("contemporarychristian")) {
            var3 = 8;
            return var3;
         }

         if(this.genre.contains("oldies")) {
            var3 = 8;
            return var3;
         }

         if(this.genre.contains("newage")) {
            var3 = 9;
            return var3;
         }

         if(this.genre.contains("meditative")) {
            var3 = 17;
            return var3;
         }

         if(this.genre.contains("folk")) {
            var3 = 8;
            return var3;
         }
      }

      if(this.preferences.getBoolean("if_no_preset_could_be_detected_then_switch_back", false)) {
         var3 = this.preferences.getInt("default_preset_to_switch_back_to", 0);
      } else {
         var3 = 0;
      }

      return var3;
   }

   String getPresetNameWithID(int var1) {
      String var2;
      switch(var1) {
      case 0:
         var2 = "Blues";
         break;
      case 1:
         var2 = "Classic Rock";
         break;
      case 2:
         var2 = "Country";
         break;
      case 3:
         var2 = "Dance";
         break;
      case 4:
         var2 = "Disco";
         break;
      case 5:
         var2 = "Funk";
         break;
      case 6:
         var2 = "Grunge";
         break;
      case 7:
         var2 = "Hip-Hop";
         break;
      case 8:
         var2 = "Jazz";
         break;
      case 9:
         var2 = "Metal";
         break;
      case 10:
         var2 = "New Age";
         break;
      case 11:
         var2 = "Oldies";
         break;
      case 12:
         var2 = "Other";
         break;
      case 13:
         var2 = "Pop";
         break;
      case 14:
         var2 = "R&B";
         break;
      case 15:
         var2 = "Rap";
         break;
      case 16:
         var2 = "Reggae";
         break;
      case 17:
         var2 = "Rock";
         break;
      case 18:
         var2 = "Techno";
         break;
      case 19:
         var2 = "Industrial";
         break;
      case 20:
         var2 = "Alternative";
         break;
      case 21:
         var2 = "Ska";
         break;
      case 22:
         var2 = "Death Metal";
         break;
      case 23:
         var2 = "Pranks";
         break;
      case 24:
         var2 = "Soundtrack";
         break;
      case 25:
         var2 = "Euro-Techno";
         break;
      case 26:
         var2 = "Ambient";
         break;
      case 27:
         var2 = "Trip-Hop";
         break;
      case 28:
         var2 = "Vocal";
         break;
      case 29:
         var2 = "Jazz+Funk";
         break;
      case 30:
         var2 = "Fusion";
         break;
      case 31:
         var2 = "Trance";
         break;
      case 32:
         var2 = "Classical";
         break;
      case 33:
         var2 = "Instrumental";
         break;
      case 34:
         var2 = "Acid";
         break;
      case 35:
         var2 = "House";
         break;
      case 36:
         var2 = "Game";
         break;
      case 37:
         var2 = "Sound Clip";
         break;
      case 38:
         var2 = "Gospel";
         break;
      case 39:
         var2 = "Noise";
         break;
      case 40:
         var2 = "Alternative Rock";
         break;
      case 41:
         var2 = "Bass";
         break;
      case 42:
         var2 = "Soul";
         break;
      case 43:
         var2 = "Punk";
         break;
      case 44:
         var2 = "Space";
         break;
      case 45:
         var2 = "Meditative";
         break;
      case 46:
         var2 = "Instrumental Pop";
         break;
      case 47:
         var2 = "Instrumental Rock";
         break;
      case 48:
         var2 = "Ethnic";
         break;
      case 49:
         var2 = "Gothic";
         break;
      case 50:
         var2 = "Darkwave";
         break;
      case 51:
         var2 = "Techno-Industrial";
         break;
      case 52:
         var2 = "Electronic";
         break;
      case 53:
         var2 = "Pop-Folk";
         break;
      case 54:
         var2 = "Eurodance";
         break;
      case 55:
         var2 = "Dream";
         break;
      case 56:
         var2 = "Southern Rock";
         break;
      case 57:
         var2 = "Comedy";
         break;
      case 58:
         var2 = "Cult";
         break;
      case 59:
         var2 = "Gangsta";
         break;
      case 60:
         var2 = "Top 40";
         break;
      case 61:
         var2 = "Christian Rap";
         break;
      case 62:
         var2 = "Pop/Funk";
         break;
      case 63:
         var2 = "Jungle";
         break;
      case 64:
         var2 = "Native US";
         break;
      case 65:
         var2 = "Cabaret";
         break;
      case 66:
         var2 = "New Wave";
         break;
      case 67:
         var2 = "Psychadelic";
         break;
      case 68:
         var2 = "Rave";
         break;
      case 69:
         var2 = "Showtunes";
         break;
      case 70:
         var2 = "Trailer";
         break;
      case 71:
         var2 = "Lo-Fi";
         break;
      case 72:
         var2 = "Tribal";
         break;
      case 73:
         var2 = "Acid Punk";
         break;
      case 74:
         var2 = "Acid Jazz";
         break;
      case 75:
         var2 = "Polka";
         break;
      case 76:
         var2 = "Retro";
         break;
      case 77:
         var2 = "Musical";
         break;
      case 78:
         var2 = "Rock & Roll";
         break;
      case 79:
         var2 = "Hard Rock";
         break;
      case 80:
         var2 = "Folk";
         break;
      case 81:
         var2 = "Folk-Rock";
         break;
      case 82:
         var2 = "National Folk";
         break;
      case 83:
         var2 = "Swing";
         break;
      case 84:
         var2 = "Fast Fusion";
         break;
      case 85:
         var2 = "Bebob";
         break;
      case 86:
         var2 = "Latin";
         break;
      case 87:
         var2 = "Revival";
         break;
      case 88:
         var2 = "Celtic";
         break;
      case 89:
         var2 = "Bluegrass";
         break;
      case 90:
         var2 = "Avantgarde";
         break;
      case 91:
         var2 = "Gothic Rock";
         break;
      case 92:
         var2 = "Progressive Rock";
         break;
      case 93:
         var2 = "Psychedelic Rock";
         break;
      case 94:
         var2 = "Symphonic Rock";
         break;
      case 95:
         var2 = "Slow Rock";
         break;
      case 96:
         var2 = "Big Band";
         break;
      case 97:
         var2 = "Chorus";
         break;
      case 98:
         var2 = "Easy Listening";
         break;
      case 99:
         var2 = "Acoustic";
         break;
      case 100:
         var2 = "Humour";
         break;
      case 101:
         var2 = "Speech";
         break;
      case 102:
         var2 = "Chanson";
         break;
      case 103:
         var2 = "Opera";
         break;
      case 104:
         var2 = "Chamber Music";
         break;
      case 105:
         var2 = "Sonata";
         break;
      case 106:
         var2 = "Symphony";
         break;
      case 107:
         var2 = "Booty Bass";
         break;
      case 108:
         var2 = "Primus";
         break;
      case 109:
         var2 = "Porn Groove";
         break;
      case 110:
         var2 = "Satire";
         break;
      case 111:
         var2 = "Slow Jam";
         break;
      case 112:
         var2 = "Club";
         break;
      case 113:
         var2 = "Tango";
         break;
      case 114:
         var2 = "Samba";
         break;
      case 115:
         var2 = "Folklore";
         break;
      case 116:
         var2 = "Ballad";
         break;
      case 117:
         var2 = "Power Ballad";
         break;
      case 118:
         var2 = "Rhythmic Soul";
         break;
      case 119:
         var2 = "Freestyle";
         break;
      case 120:
         var2 = "Duet";
         break;
      case 121:
         var2 = "Punk Rock";
         break;
      case 122:
         var2 = "Drum Solo";
         break;
      case 123:
         var2 = "Acapella";
         break;
      case 124:
         var2 = "Euro-House";
         break;
      case 125:
         var2 = "Dance Hall";
         break;
      case 126:
         var2 = "Goa";
         break;
      case 127:
         var2 = "Drum & Bass";
         break;
      case 128:
         var2 = "Club - House";
         break;
      case 129:
         var2 = "Hardcore";
         break;
      case 130:
         var2 = "Terror";
         break;
      case 131:
         var2 = "Indie";
         break;
      case 132:
         var2 = "BritPop";
         break;
      case 133:
         var2 = "Negerpunk";
         break;
      case 134:
         var2 = "Polsk Punk";
         break;
      case 135:
         var2 = "Beat";
         break;
      case 136:
         var2 = "Christian Gangsta Rap";
         break;
      case 137:
         var2 = "Heavy Metal";
         break;
      case 138:
         var2 = "Black Metal";
         break;
      case 139:
         var2 = "Crossover";
         break;
      case 140:
         var2 = "Contemporary Christian";
         break;
      case 141:
         var2 = "Christian Rock";
         break;
      case 142:
         var2 = "Merengue";
         break;
      case 143:
         var2 = "Salsa";
         break;
      case 144:
         var2 = "Thrash Metal";
         break;
      case 145:
         var2 = "Anime";
         break;
      case 146:
         var2 = "JPop";
         break;
      case 147:
         var2 = "Synthpop";
         break;
      default:
         var2 = String.valueOf(var1);
      }

      return var2;
   }

   public ArrayList getPresetValues(int var1, int var2) {
      ArrayList var3;
      if(var2 == 6) {
         var3 = this.get6BandsPresetValues(var1);
      } else {
         var3 = this.get5BandsPresetValues(var1);
      }

      return var3;
   }

   public int getPresetWithId(int var1) {
      if(var1 > 21) {
         int var2 = 0;

         while(true) {
            if(var2 >= this.preferences.getInt("number_of_saved_custom_presets", 0)) {
               var1 = 0;
               break;
            }

            if(this.preferences.getInt("equalizer_custom_values_key" + String.valueOf(var2 + 1) + "_id", 0) == var1) {
               var1 = var2 + 22;
               break;
            }

            ++var2;
         }
      }

      return var1;
   }
}
