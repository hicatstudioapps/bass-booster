package com.cool.bassbooster;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import java.util.ArrayList;

@SuppressWarnings("ResourceType")
public class BassBoosterWidget extends AppWidgetProvider {
   private static final String EXTRA_BB = "bb";
   private static final String EXTRA_EQ = "eq";
   private static final String EXTRA_EQP = "eqp";
   private static final String EXTRA_RE = "re";
   private static final String EXTRA_TYPE = "type";
   private static final String EXTRA_VI = "vi";
   private Editor editor = null;
   private SharedPreferences preferences = null;

   public void onReceive(Context var1, Intent var2) {
      this.preferences = PreferenceManager.getDefaultSharedPreferences(var1);
      this.editor = this.preferences.edit();
      String var5 = var2.getStringExtra("type");
      if(var5 != null) {
         int var3;
         int var4;
         if(var5.equals("bb")) {
            var4 = this.preferences.getInt("bassboost_strength_key", 800);
            this.editor.putBoolean("bassboost_enabled_key", true);
            if(!this.preferences.getBoolean("bassboost_enabled_key", false)) {
               var3 = 200;
            } else if(var4 < 200) {
               var3 = 200;
            } else if(var4 < 600) {
               var3 = 600;
            } else if(var4 < 1000) {
               var3 = 1000;
            } else {
               var3 = var4;
               if(var4 >= 1000) {
                  this.editor.putBoolean("bassboost_enabled_key", false);
                  var3 = 150;
               }
            }

            this.editor.putInt("bassboost_strength_key", var3).apply();
         } else if(var5.equals("vi")) {
            var4 = this.preferences.getInt("virtualizer_strength_key", 0);
            if(var4 < 200) {
               var3 = 200;
            } else if(var4 < 600) {
               var3 = 600;
            } else if(var4 < 1000) {
               var3 = 1000;
            } else {
               var3 = var4;
               if(var4 >= 1000) {
                  var3 = 0;
               }
            }

            this.editor.putInt("virtualizer_strength_key", var3).apply();
         } else if(var5.equals("re")) {
            var4 = this.preferences.getInt("reverb_strength_key", 0);
            if(var4 < 2) {
               var3 = 2;
            } else if(var4 < 4) {
               var3 = 4;
            } else if(var4 < 6) {
               var3 = 6;
            } else {
               var3 = var4;
               if(var4 >= 6) {
                  var3 = 0;
               }
            }

            this.editor.putInt("reverb_strength_key", var3).apply();
         } else if(var5.equals("eq")) {
            if(this.preferences.getBoolean("equalizer_enabled_key", false)) {
               this.editor.putBoolean("equalizer_enabled_key", false).apply();
            } else {
               this.editor.putBoolean("equalizer_enabled_key", true).apply();
            }
         } else if(var5.equals("eqp") && this.preferences.getBoolean("equalizer_enabled_key", false)) {
            Intent var6 = new Intent(var1, PresetDialogActivity.class);
            var6.addFlags(268435456);
            var1.startActivity(var6);
         }

         var1.startService(new Intent(var1, BassBoosterService.class));
      }

      super.onReceive(var1, var2);
   }

   public void onUpdate(Context var1, AppWidgetManager var2, int[] var3) {
      super.onUpdate(var1, var2, var3);
      this.preferences = PreferenceManager.getDefaultSharedPreferences(var1);
      ArrayList var6 = new ArrayList();
      String[] var5 = var1.getResources().getStringArray(R.array.preset_array);

      int var4;
      for(var4 = 0; var4 < var5.length; ++var4) {
         var6.add(var5[var4]);
      }

      for(var4 = 0; var4 < this.preferences.getInt("number_of_saved_custom_presets", 0); ++var4) {
         var6.add(this.preferences.getString("equalizer_custom_values_key" + String.valueOf(var4 + 1) + "_name", "Custom Preset #" + (var4 + 1)));
      }

      RemoteViews var7 = new RemoteViews(var1.getPackageName(), R.layout.appwidget);
      if(this.preferences.getBoolean("equalizer_enabled_key", false)) {
         var7.setTextViewText(R.id.appwidgetEQPButton, (CharSequence)var6.get(this.preferences.getInt("equalizer_selected_preset_key", 0)));
         var7.setInt(R.id.appwidgetEQButton, "setBackgroundResource", R.drawable.appwidget_toggle_on);
         var7.setInt(R.id.appwidgetEQPButton, "setBackgroundResource", R.drawable.appwidget_spinner_on);
         var7.setTextColor(R.id.appwidgetEQButton, var1.getResources().getColor(R.color.activeText));
         var7.setTextColor(R.id.appwidgetEQPButton, var1.getResources().getColor(R.color.activeText));
      } else {
         var7.setTextViewText(R.id.appwidgetEQPButton, var1.getString(R.string.off));
         var7.setInt(R.id.appwidgetEQButton, "setBackgroundResource", R.drawable.toggle_off);
         var7.setInt(R.id.appwidgetEQPButton, "setBackgroundResource", R.drawable.appwidget_spinner_off);
         var7.setTextColor(R.id.appwidgetEQButton, var1.getResources().getColor(R.color.inactiveText));
         var7.setTextColor(R.id.appwidgetEQPButton, var1.getResources().getColor(R.color.inactiveText));
      }

      if(this.preferences.getBoolean("bassboost_enabled_key", false)) {
         var7.setTextViewText(R.id.appwidgetBBButton, var1.getString(R.string.BB) + ": " + this.preferences.getInt("bassboost_strength_key", 0) / 10 + "%");
         var7.setInt(R.id.appwidgetBBButton, "setBackgroundResource", R.drawable.appwidget_toggle_on);
         var7.setTextColor(R.id.appwidgetBBButton, var1.getResources().getColor(R.color.activeText));
      } else {
         var7.setTextViewText(R.id.appwidgetBBButton, var1.getString(R.string.BB) + ": " + var1.getString(R.string.off));
         var7.setInt(R.id.appwidgetBBButton, "setBackgroundResource", R.drawable.toggle_off);
         var7.setTextColor(R.id.appwidgetBBButton, var1.getResources().getColor(R.color.inactiveText));
      }

//      if(this.preferences.getInt("virtualizer_strength_key", 0) != 0) {
//         var7.setTextViewText(2131427391, var1.getString(2131230723) + ": " + this.preferences.getInt("virtualizer_strength_key", 0) / 10 + "%");
//         var7.setInt(2131427391, "setBackgroundResource", 2130837509);
//         var7.setTextColor(2131427391, var1.getResources().getColor(2131099648));
//      } else {
//         var7.setTextViewText(2131427391, var1.getString(2131230723) + ": " + var1.getString(2131230800));
//         var7.setInt(2131427391, "setBackgroundResource", 2130837610);
//         var7.setTextColor(2131427391, var1.getResources().getColor(2131099650));
//      }
//
//      if(this.preferences.getInt("reverb_strength_key", 0) != 0) {
//         var7.setTextViewText(2131427392, var1.getString(2131230722) + ": " + this.preferences.getInt("reverb_strength_key", 0));
//         var7.setInt(2131427392, "setBackgroundResource", 2130837509);
//         var7.setTextColor(2131427392, var1.getResources().getColor(2131099648));
//      } else {
//         var7.setTextViewText(2131427392, var1.getString(2131230722) + ": " + var1.getString(2131230800));
//         var7.setInt(2131427392, "setBackgroundResource", 2130837610);
//         var7.setTextColor(2131427392, var1.getResources().getColor(2131099650));
//      }

      Intent var8 = new Intent(var1, BassBoosterWidget.class);
      var8.setAction("android.appwidget.action.APPWIDGET_UPDATE");
      var8.putExtra("appWidgetIds", var3);
      var8.putExtra("type", "eq");
      var8.setData(Uri.withAppendedPath(Uri.parse("WIDGET://widget/id/"), String.valueOf(R.id.appwidgetEQButton)));
      var7.setOnClickPendingIntent(R.id.appwidgetEQButton, PendingIntent.getBroadcast(var1, 0, var8, 0x8000000));
      var8 = new Intent(var1, BassBoosterWidget.class);
      var8.setAction("android.appwidget.action.APPWIDGET_UPDATE");
      var8.putExtra("appWidgetIds", var3);
      var8.putExtra("type", "eqp");
      var8.setData(Uri.withAppendedPath(Uri.parse("WIDGET://widget/id/"), String.valueOf(R.id.appwidgetEQPButton)));
      var7.setOnClickPendingIntent(R.id.appwidgetEQPButton, PendingIntent.getBroadcast(var1, 0, var8, 0x8000000));
      var8 = new Intent(var1, BassBoosterWidget.class);
      var8.setAction("android.appwidget.action.APPWIDGET_UPDATE");
      var8.putExtra("appWidgetIds", var3);
      var8.putExtra("type", "bb");
      var8.setData(Uri.withAppendedPath(Uri.parse("WIDGET://widget/id/"), String.valueOf(R.id.appwidgetBBButton)));
      var7.setOnClickPendingIntent(R.id.appwidgetBBButton, PendingIntent.getBroadcast(var1, 0, var8, 0x8000000));
//      var8 = new Intent(var1, BassBoosterWidget.class);
//      var8.setAction("android.appwidget.action.APPWIDGET_UPDATE");
//      var8.putExtra("appWidgetIds", var3);
//      var8.putExtra("type", "vi");
//      var8.setData(Uri.withAppendedPath(Uri.parse("WIDGET://widget/id/"), String.valueOf(2131427391)));
//      var7.setOnClickPendingIntent(2131427391, PendingIntent.getBroadcast(var1, 0, var8, 134217728));
//      var8 = new Intent(var1, BassBoosterWidget.class);
//      var8.setAction("android.appwidget.action.APPWIDGET_UPDATE");
//      var8.putExtra("appWidgetIds", var3);
//      var8.putExtra("type", "re");
//      var8.setData(Uri.withAppendedPath(Uri.parse("WIDGET://widget/id/"), String.valueOf(2131427392)));
//      var7.setOnClickPendingIntent(2131427392, PendingIntent.getBroadcast(var1, 0, var8, 134217728));
      var2.updateAppWidget(var3, var7);
   }
}
