package com.cool.bassbooster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.ArrayList;

public class CallReceiver extends BroadcastReceiver {
   private static final String TAG = "CallReceiver";
   private Editor editor = null;
   private SharedPreferences preferences = null;

   public void onReceive(Context var1, Intent var2) {
      this.preferences = PreferenceManager.getDefaultSharedPreferences(var1);
      this.editor = this.preferences.edit();
      if(var2 != null && (this.preferences.getBoolean("change_eq_preset_on_call", false) || this.preferences.getBoolean("change_eq_preset_during_call", false))) {
         String var5 = var2.getAction();
         Log.d("CallReceiver", "Intent received : " + var5);
         int var4 = this.preferences.getInt("number_of_eq_bands_key", 5);
         int var3;
         ArrayList var6;
         if(var5.equals("android.intent.action.PHONE_STATE") && var2.hasExtra("state") && var2.getStringExtra("state").equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            this.editor.putBoolean("equalizer_previously_enabled", this.preferences.getBoolean("equalizer_enabled_key", false));
            this.editor.putInt("equalizer_previous_selected_preset", this.preferences.getInt("equalizer_selected_preset_key", 0));

            for(var3 = 0; var3 < var4; ++var3) {
               this.editor.putInt("equalizer_previous_values" + String.valueOf(var3), this.preferences.getInt("equalizer_value_key" + String.valueOf(var3), 0));
            }

            this.editor.apply();
            if(this.preferences.getBoolean("change_eq_preset_on_call", false)) {
               var3 = this.preferences.getInt("preset_to_set_on_call", 21);
               var6 = (new PresetRetriever(var1)).getPresetValues(var3, var4);
               this.editor.putBoolean("equalizer_enabled_key", true).apply();
               this.editor.putInt("equalizer_selected_preset_key", var3).apply();
               if(var6 != null) {
                  for(var3 = 0; var3 < var4; ++var3) {
                     this.editor.putInt("equalizer_value_key" + String.valueOf(var3), ((Integer)var6.get(var3)).intValue());
                  }
               }
            }
         } else if(this.preferences.getBoolean("change_eq_preset_during_call", false) && var5.equals("android.intent.action.PHONE_STATE") && var2.hasExtra("state") && var2.getStringExtra("state").equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            var3 = this.preferences.getInt("preset_to_set_during_call", 21);
            var6 = (new PresetRetriever(var1)).getPresetValues(var3, var4);
            this.editor.putBoolean("equalizer_enabled_key", true).apply();
            this.editor.putInt("equalizer_selected_preset_key", var3).apply();
            if(var6 != null) {
               for(var3 = 0; var3 < var4; ++var3) {
                  this.editor.putInt("equalizer_value_key" + String.valueOf(var3), ((Integer)var6.get(var3)).intValue());
               }
            }
         } else {
            this.editor.putBoolean("equalizer_enabled_key", this.preferences.getBoolean("equalizer_previously_enabled", false));
            this.editor.putInt("equalizer_selected_preset_key", this.preferences.getInt("equalizer_previous_selected_preset", 0));

            for(var3 = 0; var3 < var4; ++var3) {
               this.editor.putInt("equalizer_value_key" + String.valueOf(var3), this.preferences.getInt("equalizer_previous_values" + String.valueOf(var3), 0));
            }
         }

         this.editor.apply();
         var2 = new Intent(var1, BassBoosterService.class);
         var2.putExtra("appwidget", true);
         var1.startService(var2);
         var1.sendBroadcast(new Intent("com.desaxedstudios.bassboosterpro.REFRESH_ACTIVITY"));
      }

   }
}
