package com.cool.bassbooster;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.media.audiofx.Virtualizer;
import android.preference.PreferenceManager;
//import android.support.v4.app.bi;
import android.util.Log;
import android.widget.Toast;
//import com.desaxedstudios.bassbooster.BassBoosterActivity;
//import com.desaxedstudios.bassbooster.q;
import com.lacostra.utils.notification.NotificationUtil;
import com.lacostra.utils.notification.Sheduler;

import java.util.ArrayList;
import java.util.Calendar;

@SuppressWarnings("ResourceType")
public abstract class p extends Service {
    protected SharedPreferences a = null;
    private PendingIntent b = null;
    private PendingIntent c = null;
    private NotificationManager d = null;
    //private bi e;
    private AlarmManager f = null;
    private AudioManager g = null;
    private Calendar h = null;
    private Equalizer i = null;
    private BassBoost j = null;
    private Virtualizer k = null;
    private PresetReverb l = null;
    private BroadcastReceiver m = new q(this);

    private void d() {
//        this.h.setTimeInMillis(System.currentTimeMillis());
//        this.h.add(13, 5);
//        this.f.set(0, this.h.getTimeInMillis(), this.b);
        p.scheduleService(getApplicationContext(),0);
    }

    private CharSequence e() {
        boolean var4 = this.a.getBoolean("equalizer_enabled_key", true);
        boolean var3 = this.a.getBoolean("bassboost_enabled_key", true);
        String var7;
        if (var4) {
            int var2 = this.a.getInt("equalizer_selected_preset_key", 0);
            if (var2 <= 0) {
                var7 = "" + this.getString(R.string.eqActivated);
            } else {
                String[] var5 = this.getResources().getStringArray(R.array.preset_array);
                ArrayList var6 = new ArrayList();

                int var1;
                for (var1 = 0; var1 < var5.length; ++var1) {
                    var6.add(var5[var1]);
                }

                for (var1 = 0; var1 < this.a.getInt("number_of_saved_custom_presets", 0); ++var1) {
                    var6.add(this.a.getString("equalizer_custom_values_key" + String.valueOf(var1 + 1) + "_name", "Custom Preset #" + (var1 + 1)));
                }

                var7 = "" + this.getString(R.string.eqPreset) + (String) var6.get(var2);
            }

            if (var3) {
                var7 = var7 + "  -  ";
            }
        } else {
            var7 = "";
        }

        String var8 = var7;
        if (var3) {
            var8 = var7 + this.getString(R.string.bb) + this.a.getInt("bassboost_strength_key", 800) / 10 + "%";
        }

        return var8;
    }

    public void a() {
        // $FF: Couldn't be decompiled
        short word0;
        boolean flag;
        flag = false;
        word0 = 0;
        if (!a.getBoolean("equalizer_enabled_key", false)) {
            if (i == null) {
                return;
            } else {
//                word0 = 0;
//                for (int band = 0; band < i.getNumberOfBands(); band++) {
//                    i.setBandLevel(word0, (short) 0);
//                }
                i.setEnabled(false);
            }
        } else {
            i = new Equalizer(0, 0);
            for (int band = 0; band < i.getNumberOfBands(); band++) {
                i.setBandLevel(word0, (short) a.getInt((new StringBuilder()).append("equalizer_value_key").append(String.valueOf(word0)).toString(), 0));
            }
            i.setEnabled(true);
        }
    }

    public void a(int var1) {
        boolean var2;
        label67:
        {
            boolean var3 = true;
            if (!this.a.getBoolean("force_bass_boost_everywhere", true)) {
                var2 = var3;
                switch (var1) {
                    case 0:
                        var2 = false;
                    case 1:
                        break label67;
                    case 2:
                    default:
                        break;
                    case 3:
                        if (!this.g.isWiredHeadsetOn() && !this.g.isBluetoothA2dpOn() && !this.g.isBluetoothScoOn()) {
                            var2 = var3;
                            break label67;
                        }
                }
            }

            var2 = false;
        }

        if (this.a.getBoolean("bassboost_enabled_key", false) && !var2) {
            try {
                this.j = new BassBoost(0, 0);
                this.j.setStrength((short) this.a.getInt("bassboost_strength_key", 800));
                this.j.setEnabled(true);
            } catch (IllegalArgumentException var13) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
            } catch (UnsupportedOperationException var14) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
            } catch (RuntimeException var15) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
            } catch (Exception var16) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
            }
        } else if (this.j != null) {
            try {
                this.j.setStrength((short) 0);
            } catch (IllegalArgumentException var9) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
            } catch (UnsupportedOperationException var10) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
            } catch (RuntimeException var11) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
            } catch (Exception var12) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
            }

            try {
                if (this.j != null) {
                    this.j.setEnabled(false);
                }
            } catch (IllegalArgumentException var5) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalArgumentException.");
            } catch (UnsupportedOperationException var6) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : UnsupportedOperationException.");
            } catch (RuntimeException var7) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : IllegalStateException (RuntimeException).");
            } catch (Exception var8) {
                this.j = null;
                Log.e("BassBoosterService", "BassBoost not supported in service : Exception.");
            }
        }
    }

    public abstract void b();

    //@SuppressLint({"NewApi"})
    public void c() {
        boolean var2 = this.a.getBoolean("equalizer_enabled_key", false);
        boolean var1 = this.a.getBoolean("bassboost_enabled_key", false);
        int x = 40 * 5;
        int r = x + 6;
        if(this.a.getBoolean("show_status_icon",true))
        if (var1 || var2) {
            new NotificationUtil().notifyText(this,getResources(), 100, R.drawable.ic_notify,R.mipmap.ic_launcher, getString(R.string.app_name), e() + "", PendingIntent.getActivity(this, 50, new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            this.d.cancelAll();
        }
        else
            this.d.cancelAll();
    }

    public void onCreate() {
//        Toast.makeText(this,"Servicio en ejecucion",Toast.LENGTH_SHORT).show();
        Log.d("Services Boost", "on create");
        this.c = PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), 0);
        this.a = PreferenceManager.getDefaultSharedPreferences(this);
        this.d = (NotificationManager) this.getSystemService("notification");
//        this.f = Sheduler.getAlarmManager(getApplication());
        this.h = Calendar.getInstance();
        this.g = (AudioManager) this.getSystemService("audio");
        Intent var1 = new Intent(this.getApplicationContext(), p.class);
        var1.setAction("repeat");
        var1.putExtra("no_notif", true);
        this.b = PendingIntent.getService(this.getApplicationContext(), 0, var1, 0);
//      this.e = new bi(this);
//      this.e.a(this.c).a(BitmapFactory.decodeResource(this.getResources(), 2130837564)).a(2130837565).a((CharSequence)this.getString(2131034250)).c((CharSequence)null).a(0L).a(true).c(false).b(true).a(this.c);
        IntentFilter var2 = new IntentFilter();
        var2.addAction("android.media.AUDIO_BECOMING_NOISY");
        var2.addAction("android.intent.action.HEADSET_PLUG");
        this.registerReceiver(this.m, var2);
    }

    private static PendingIntent getPendingIntent(Context var0) {
        return PendingIntent.getService(var0, 0, new Intent(var0, BassBoosterService.class), PendingIntent.FLAG_UPDATE_CURRENT);
    }
    public static void scheduleService(final Context var0, final long var1) {

        new Thread(new Runnable() {
            public void run() {
                stopSchedule(var0);
                //432200000
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                cal.add(Calendar.SECOND,6);
                Sheduler.getAlarmManager(var0).set(1, cal.getTimeInMillis(), getPendingIntent(var0));
            }
        }).start();
    }

    public static void stopSchedule(Context var0) {
        PendingIntent var1 = getPendingIntent(var0);
        Sheduler.getAlarmManager(var0).cancel(var1);
    }
    public void onDestroy() {
        this.a(3);
        this.a();
        try {
            this.unregisterReceiver(this.m);
        } catch (IllegalArgumentException var2) {
            Log.d("BassBoosterService", "unregisterReceiver called more than once or on uninitialized receiver in BassBoosterService. Please ignore.");
        }
    }

    public int onStartCommand(Intent var1, int var2, int var3) {
        Log.d("Services Boost Started", "on start");
//        Toast.makeText(this,"On Start",Toast.LENGTH_SHORT).show();
        boolean var5 = this.a.getBoolean("equalizer_enabled_key", false);
        boolean var4 = this.a.getBoolean("bassboost_enabled_key", false);
        this.a(3);
        this.a();
        if (var1 != null) {
            if (var1.hasExtra("appwidget")) {
                this.b();
            }

            if (!var1.hasExtra("no_notif")) {
                this.c();
            }
        }

        if (!var4 && !var5) {
            p.stopSchedule(getApplicationContext());
        } else if (this.a.getBoolean("run_in_background", true)) {
            this.d();
        } else {
            p.stopSchedule(getApplicationContext());
        }

        return 2;
    }
}
