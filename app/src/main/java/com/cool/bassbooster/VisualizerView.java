package com.cool.bassbooster;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.view.View;

public class VisualizerView extends View {
   private byte[] mBytes;
   private float[] mPoints;
   private Rect mRect = new Rect();
   private Paint p = new Paint(2);
   private Path path = new Path();

   public VisualizerView(Context var1) {
      super(var1);
      this.init();
   }

   public VisualizerView(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public VisualizerView(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   private float getX(int var1) {
      return (float)(this.mRect.width() * var1 / (this.mBytes.length - 1));
   }

   private float getY(int var1) {
      this.mBytes[var1] = (byte)Math.abs(this.mBytes[var1]);
      return (float)Math.abs((byte)(this.mBytes[var1] + 128) * this.mRect.height() / 128);
   }

   private void init() {
      this.mBytes = null;
      this.setFocusable(false);
      this.setClickable(false);
      this.p.setDither(false);
      this.p.setAntiAlias(false);
   }

   protected void onDraw(Canvas var1) {
      super.onDraw(var1);
      if(this.mBytes != null) {
         if(this.mPoints == null || this.mPoints.length < this.mBytes.length * 4) {
            this.mPoints = new float[this.mBytes.length * 4];
         }

         int var3 = this.getHeight();
         int var4 = this.getWidth();
         this.mRect.set(0, 0, var4, var3);
         this.path.reset();
         this.path.moveTo(0.0F, (float)var3);

         for(int var2 = 0; var2 < this.mBytes.length - 1; ++var2) {
            this.path.lineTo(this.getX(var2), this.getY(var2 + 1));
         }

         this.path.lineTo((float)var4, (float)var3);
         this.p.setShader(new LinearGradient(0.0F, (float)(var3 / 2), 0.0F, (float)var3, -1426128896, -1442775296, TileMode.CLAMP));
         var1.drawPath(this.path, this.p);
      }

   }

   public void updateVisualizer(byte[] var1) {
      this.mBytes = var1;
      this.invalidate();
   }
}
