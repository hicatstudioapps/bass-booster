package com.cool.bassbooster;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
//import com.desaxedstudios.bassbooster.x;
import java.util.ArrayList;

public class y {
   Context a = null;
   String b = null;
   String c = null;
   private SharedPreferences d = null;
   private ArrayList e = new ArrayList();

   public y(Context var1) {
      this.a = var1;
      this.d = PreferenceManager.getDefaultSharedPreferences(this.a);
   }

   private ArrayList a(int var1) {
      Object var23 = null;
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;
      int var8 = 0;
      int var9 = 0;
      int var10 = 0;
      int var11 = 0;
      int var12 = 0;
      int var13 = 0;
      int var14 = 0;
      int var15 = 0;
      int var16 = 0;
      int var17 = 0;
      int var18 = 0;
      int var19 = 0;
      int var20 = 0;
      int var21 = 0;
      int var2 = 0;
      ArrayList var24 = new ArrayList();
      ArrayList var22;
      if(var1 <= 21) {
         var22 = (ArrayList)var23;
         label128:
         switch(var1) {
         case 0:
            return var22;
         case 1:
            while(true) {
               if(var2 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.b[var2]));
               ++var2;
            }
         case 2:
            while(true) {
               if(var3 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.d[var3]));
               ++var3;
            }
         case 3:
            var1 = 0;

            while(true) {
               if(var1 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(this.d.getInt("equalizer_custom_values_key" + String.valueOf(var1), 0)));
               ++var1;
            }
         case 4:
            while(true) {
               if(var12 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.f[var12]));
               ++var12;
            }
         case 5:
            while(true) {
               if(var6 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.h[var6]));
               ++var6;
            }
         case 6:
            while(true) {
               if(var13 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.j[var13]));
               ++var13;
            }
         case 7:
            while(true) {
               if(var11 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.l[var11]));
               ++var11;
            }
         case 8:
            while(true) {
               if(var14 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.n[var14]));
               ++var14;
            }
         case 9:
            while(true) {
               if(var4 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.p[var4]));
               ++var4;
            }
         case 10:
            while(true) {
               if(var15 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.r[var15]));
               ++var15;
            }
         case 11:
            while(true) {
               if(var16 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.t[var16]));
               ++var16;
            }
         case 12:
            while(true) {
               if(var5 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.v[var5]));
               ++var5;
            }
         case 13:
            while(true) {
               if(var7 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.x[var7]));
               ++var7;
            }
         case 14:
            while(true) {
               if(var17 >= 6) {
                  break label128;
               }

               var24.add(var17, Integer.valueOf(x.z[var17]));
               ++var17;
            }
         case 15:
            while(true) {
               if(var18 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.D[var18]));
               ++var18;
            }
         case 16:
            while(true) {
               if(var21 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.B[var21]));
               ++var21;
            }
         case 17:
            while(true) {
               if(var19 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.F[var19]));
               ++var19;
            }
         case 18:
            while(true) {
               if(var20 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.H[var20]));
               ++var20;
            }
         case 19:
            while(true) {
               if(var10 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.J[var10]));
               ++var10;
            }
         case 20:
            while(true) {
               if(var8 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.L[var8]));
               ++var8;
            }
         case 21:
            while(true) {
               if(var9 >= 6) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.N[var9]));
               ++var9;
            }
         default:
            var22 = (ArrayList)var23;
            return var22;
         }
      } else {
         for(var2 = 0; var2 < 6; ++var2) {
            var24.add(var2, Integer.valueOf(this.d.getInt("equalizer_custom_values_key" + String.valueOf(var1 - 21) + "_band" + var2, 0)));
         }
      }

      var22 = var24;
      return var22;
   }

   private ArrayList b(int var1) {
      Object var23 = null;
      int var3 = 0;
      int var4 = 0;
      int var5 = 0;
      int var6 = 0;
      int var7 = 0;
      int var8 = 0;
      int var9 = 0;
      int var10 = 0;
      int var11 = 0;
      int var12 = 0;
      int var13 = 0;
      int var14 = 0;
      int var15 = 0;
      int var16 = 0;
      int var17 = 0;
      int var18 = 0;
      int var19 = 0;
      int var20 = 0;
      int var21 = 0;
      int var2 = 0;
      ArrayList var24 = new ArrayList();
      ArrayList var22;
      if(var1 <= 21) {
         var22 = (ArrayList)var23;
         label128:
         switch(var1) {
         case 0:
            return var22;
         case 1:
            while(true) {
               if(var2 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.a[var2]));
               ++var2;
            }
         case 2:
            while(true) {
               if(var3 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.c[var3]));
               ++var3;
            }
         case 3:
            var1 = 0;

            while(true) {
               if(var1 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(this.d.getInt("equalizer_custom_values_key" + String.valueOf(var1), 0)));
               ++var1;
            }
         case 4:
            while(true) {
               if(var12 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.e[var12]));
               ++var12;
            }
         case 5:
            while(true) {
               if(var6 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.g[var6]));
               ++var6;
            }
         case 6:
            while(true) {
               if(var13 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.i[var13]));
               ++var13;
            }
         case 7:
            while(true) {
               if(var11 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.k[var11]));
               ++var11;
            }
         case 8:
            while(true) {
               if(var14 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.m[var14]));
               ++var14;
            }
         case 9:
            while(true) {
               if(var4 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.o[var4]));
               ++var4;
            }
         case 10:
            while(true) {
               if(var15 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.q[var15]));
               ++var15;
            }
         case 11:
            while(true) {
               if(var16 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.s[var16]));
               ++var16;
            }
         case 12:
            while(true) {
               if(var5 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.u[var5]));
               ++var5;
            }
         case 13:
            while(true) {
               if(var7 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.w[var7]));
               ++var7;
            }
         case 14:
            while(true) {
               if(var17 >= 5) {
                  break label128;
               }

               var24.add(var17, Integer.valueOf(x.y[var17]));
               ++var17;
            }
         case 15:
            while(true) {
               if(var18 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.C[var18]));
               ++var18;
            }
         case 16:
            while(true) {
               if(var21 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.A[var21]));
               ++var21;
            }
         case 17:
            while(true) {
               if(var19 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.E[var19]));
               ++var19;
            }
         case 18:
            while(true) {
               if(var20 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.G[var20]));
               ++var20;
            }
         case 19:
            while(true) {
               if(var10 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.I[var10]));
               ++var10;
            }
         case 20:
            while(true) {
               if(var8 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.K[var8]));
               ++var8;
            }
         case 21:
            while(true) {
               if(var9 >= 5) {
                  break label128;
               }

               var24.add(Integer.valueOf(x.M[var9]));
               ++var9;
            }
         default:
            var22 = (ArrayList)var23;
            return var22;
         }
      } else {
         for(var2 = 0; var2 < 5; ++var2) {
            var24.add(var2, Integer.valueOf(this.d.getInt("equalizer_custom_values_key" + String.valueOf(var1 - 21) + "_band" + var2, 0)));
         }
      }

      var22 = var24;
      return var22;
   }

   public ArrayList a(int var1, int var2) {
      ArrayList var3;
      if(var2 == 6) {
         var3 = this.a(var1);
      } else {
         var3 = this.b(var1);
      }

      return var3;
   }
}
