package com.cool.bassbooster;

import android.graphics.Color;
import android.graphics.Typeface;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
//import android.widget.VerticalSeekBar;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import badabing.lib.ServerUtilities;
//import com.desaxedstudios.bassbooster.BassBoosterApplication;
//import com.desaxedstudios.bassbooster.InterstitialSettingsActivity;
//import com.desaxedstudios.bassbooster.f;
//import com.desaxedstudios.bassbooster.s;
//import com.desaxedstudios.bassbooster.t;
//import com.desaxedstudios.bassbooster.u;
//import com.google.android.gms.ads.AdRequest$Builder;
//import com.google.android.gms.ads.AdView;

@SuppressWarnings("ResourceType")
public class BassBoosterActivity extends AppCompatActivity {
   Handler n = new Handler();
  // private s o;
   private long p = 0L;
   private VisualizerView mVisualizerView;
   private Visualizer mVisualizer;

   // $FF: synthetic method
//  // static s a(BassBoosterActivity var0) {
//      return var0.o;
//   }

   // $FF: synthetic method
   static void a(BassBoosterActivity var0, int var1) {
      var0.d(var1);
   }

   private void d(int var1) {
//      byte var3 = 0;
//      boolean var2;
//      if(this.preferences.getInt("key_app_theme", 0) == 1) {
//         var2 = true;
//      } else {
//         var2 = false;
//      }
//
//      int var5;
//      if(var2) {
//         var5 = -2;
//      } else {
//         var5 = (int)this.resources.getDimension(2131165188);
//      }
//
//      LayoutParams var4 = new LayoutParams(var5, -1);
//      var4.bottomMargin = var1;
//      var4.gravity = 1;
//
//      for(var1 = var3; var1 < 6; ++var1) {
//         ((VerticalSeekBar)this.a.get(var1)).setLayoutParams(var4);
//      }

   }

   protected void b() {
   }

   protected void c() {
   }

   public void onCreate(Bundle var1) {
//      super.onCreate(var1);
//      this.setContentView(R.layout.activity_main);
//      Toolbar toolbar= (Toolbar)findViewById(R.id.toolbar);
//      final SpannableStringBuilder sb = new SpannableStringBuilder(getString(R.string.app_name));
//
//// Span to set text color to some RGB value
//      final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.WHITE);
//
//// Span to make text bold
//      final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
//
//// Set the text color for first 4 characters
//      sb.setSpan(fcs, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//
//// make them also bold
//      sb.setSpan(bss, 0, getString(R.string.app_name).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//
//      Typeface font = Typeface.createFromAsset(getAssets(), "font.otf");
//      sb.setSpan(new CustomTypefaceSpan("" , font), 0 , getString(R.string.app_name).length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//      toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_launcher));
//      toolbar.setTitle(sb);
//      setSupportActionBar(toolbar);
////
////      if(this.preferences.getBoolean("show_visualizer", false)) {
////         this.setupVisualizerFxAndUI();
////         this.mVisualizer.setEnabled(true);
////      }
//      ServerUtilities.registerWithGCM(this);
   }

   private void setupVisualizerFxAndUI() {
     // this.mVisualizerView = (VisualizerView) findViewById(R.id.view);
//      this.mVisualizerView.setLayoutParams(new LayoutParams(-1, -1));
      this.mVisualizer = new Visualizer(0);
      this.mVisualizer.setEnabled(false);
      this.mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
      this.mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
         public void onFftDataCapture(Visualizer var1, byte[] var2, int var3) {
            BassBoosterActivity.this.mVisualizerView.updateVisualizer(var2);
         }

         public void onWaveFormDataCapture(Visualizer var1, byte[] var2, int var3) {
         }
      }, Visualizer.getMaxCaptureRate() / 2, false, true);
   }
   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      switch(var1.getItemId()) {
      case 2131492923:
//         this.startActivity(new Intent(this, InterstitialSettingsActivity.class));
//         var2 = true;
         break;
      case 2131492924:
//         if(System.currentTimeMillis() - this.p > 20000L) {
//            this.o.b();
//            BassBoosterApplication.a("Ads", "show_ad_on_save");
//         }

         var2 = super.onOptionsItemSelected(var1);
         break;
      case 2131492925:
//         if(System.currentTimeMillis() - this.p > 20000L) {
//            this.o.b();
//            BassBoosterApplication.a("Ads", "show_ad_on_reset");
//         }

         var2 = super.onOptionsItemSelected(var1);
         break;
      default:
         var2 = super.onOptionsItemSelected(var1);
      }

      return true;
   }

   public void onPause() {
      super.onPause();
      this.n.removeCallbacksAndMessages((Object)null);
   }

   public void onResume() {
      super.onResume();
//      this.p = System.currentTimeMillis();
//      this.n.postDelayed(new u(this), 15000L);
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      this.getMenuInflater().inflate(R.menu.main_menu, menu);
      return true;
   }
}
