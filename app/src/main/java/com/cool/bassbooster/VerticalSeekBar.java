package com.cool.bassbooster;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class VerticalSeekBar extends SeekBar {
   private OnSeekBarChangeListener a;
   private int b = 0;

   public VerticalSeekBar(Context var1) {
      super(var1);
   }

   public VerticalSeekBar(Context var1, AttributeSet var2) {
      super(var1, var2);
   }

   public VerticalSeekBar(Context var1, AttributeSet var2, int var3) {
      super(var1, var2, var3);
   }

   public int getMaximum() {
      synchronized(this){}

      int var1;
      try {
         var1 = this.getMax();
      } finally {
         ;
      }

      return var1;
   }

   protected void onDraw(Canvas var1) {
      var1.rotate(-90.0F);
      var1.translate((float)(-this.getHeight()), 0.0F);
      super.onDraw(var1);
   }

   protected void onMeasure(int var1, int var2) {
      synchronized(this){}

      try {
         super.onMeasure(var2, var1);
         this.setMeasuredDimension(this.getMeasuredHeight(), this.getMeasuredWidth());
      } finally {
         ;
      }

   }

   protected void onSizeChanged(int var1, int var2, int var3, int var4) {
      super.onSizeChanged(var2, var1, var4, var3);
   }

   public boolean onTouchEvent(MotionEvent var1) {
      boolean var4 = false;
      if(this.isEnabled()) {
         switch(var1.getAction()) {
         case 0:
            this.a.onStartTrackingTouch(this);
            this.setPressed(true);
            this.setSelected(true);
            break;
         case 1:
            this.a.onStopTrackingTouch(this);
            this.setPressed(false);
            this.setSelected(false);
            break;
         case 2:
            super.onTouchEvent(var1);
            int var3 = this.getMax() - (int)((float)this.getMax() * var1.getY() / (float)this.getHeight());
            int var2 = var3;
            if(var3 < 0) {
               var2 = 0;
            }

            var3 = var2;
            if(var2 > this.getMax()) {
               var3 = this.getMax();
            }

            this.setProgress(var3);
            if(var3 != this.b) {
               this.b = var3;
               this.a.onProgressChanged(this, var3, true);
            }

            this.onSizeChanged(this.getWidth(), this.getHeight(), 0, 0);
            this.setPressed(true);
            this.setSelected(true);
            break;
         case 3:
            super.onTouchEvent(var1);
            this.setPressed(false);
            this.setSelected(false);
         }

         var4 = true;
      }

      return var4;
   }

   public void setMaximum(int var1) {
      synchronized(this){}

      try {
         this.setMax(var1);
      } finally {
         ;
      }

   }

   public void setOnSeekBarChangeListener(OnSeekBarChangeListener var1) {
      this.a = var1;
   }

   public void setProgressAndThumb(int var1) {
      synchronized(this){}

      try {
         this.setProgress(var1);
         this.onSizeChanged(this.getWidth(), this.getHeight(), 0, 0);
         this.b = var1;
         this.a.onProgressChanged(this, var1, true);
         if(var1 != this.b) {
            this.b = var1;
            this.a.onProgressChanged(this, var1, true);
         }
         setThumb(getResources().getDrawable(R.drawable.ic_thumb_transp));
         this.invalidate();
      } finally {
         ;
      }

   }
}
