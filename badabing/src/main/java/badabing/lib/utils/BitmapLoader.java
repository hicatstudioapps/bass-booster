package badabing.lib.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.Log;

/**
 * Loads a lower resolution image version in memory, which should match the size
 * of the UI component that displays it, avoiding additional performance
 * overhead due to additional on the fly scaling.
 * 
 * @author Alejandro Casanova
 * 
 */
public class BitmapLoader {

	/**
	 * Decodes scaled down version of a resource image.
	 * 
	 * @param res
	 *            The resources object containing the image data.
	 * @param resId
	 *            The resource id of the image data.
	 * @param reqWidth
	 *            Required width of image.
	 * @param reqHeight
	 *            Required height of image.
	 * @return Lower resolution {@link Bitmap} version, or null if the image
	 *         data could not be decoded.
	 */
	public static Bitmap decodeBitmap(Resources res, int resId, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds set to true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		return BitmapFactory.decodeResource(res, resId, options);
	}

	/**
	 * Decodes scaled down version of a {@link File} image into a {@link Bitmap}
	 * . If the input stream is null, or cannot be used to decode a
	 * {@link Bitmap}, the function returns null.
	 * 
	 * @param pathName
	 *            Complete path name for the {@link File} to be decoded.
	 * @param reqWidth
	 *            Required width of image.
	 * @param reqHeight
	 *            Required height of image.
	 * @return Lower resolution {@link Bitmap} version, or null if the image
	 *         data could not be decoded.
	 */
	public static Bitmap decodeBitmap(String pathName, int reqWidth,
			int reqHeight) {

		// First decode with inJustDecodeBounds set to true to check dimensions
		final Options options = new Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(pathName, options);
	}

	/**
	 * Decodes scaled down version of an input stream into a bitmap. If the
	 * input stream is null, or cannot be used to decode a bitmap, the function
	 * returns null. The stream's position will be where ever it was after the
	 * encoded data. was read.
	 * 
	 * @param is
	 *            The input stream that holds the raw data to be decoded into a
	 *            {@link Bitmap}.
	 * @param reqWidth
	 *            Required width of image.
	 * @param reqHeight
	 *            Required height of image.
	 * @return Lower resolution {@link Bitmap} version, or null if the image
	 *         data could not be decoded.
	 */
	public static Bitmap decodeBitmap(InputStream is, int reqWidth,
			int reqHeight) {

		Bitmap result;

		BufferedInputStream bis = new BufferedInputStream(is);

		try {
			bis.mark(bis.available());

			// First decode with inJustDecodeBounds set to true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(bis, null, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bis.reset();

			result = BitmapFactory.decodeStream(bis, null, options);
		} catch (IOException e) {
			Log.e(BitmapLoader.class.getName(), e.toString());
			result = null;
		}

		return result;
	}

	/**
	 * @param options
	 *            Options that control down sampling.
	 * @param reqWidth
	 *            Required width of image.
	 * @param reqHeight
	 *            Required height of image.
	 * @return If has a value > 1, requests the decoder to sub sample the
	 *         original image, returning a smaller image to save memory. The
	 *         sample size is the number of pixels in either dimension that
	 *         correspond to a single pixel in the decoded bitmap. For example,
	 *         inSampleSize == 4 returns an image that is 1/4 the width/height
	 *         of the original, and 1/16 the number of pixels. Any value <= 1 is
	 *         treated the same as 1. Note: the decoder will try to fulfill this
	 *         request, but the resulting bitmap may have different dimensions
	 *         that precisely what has been requested.
	 */
	private static int calculateInSampleSize(Options options, int reqWidth,
			int reqHeight) {

		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			// Calculate ratios of height and width to requested height and width
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}
		return inSampleSize;
	}
}
