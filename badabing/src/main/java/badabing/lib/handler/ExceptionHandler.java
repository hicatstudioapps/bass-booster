package badabing.lib.handler;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

/**
 * A custom exception handler to send crash reports
 * @author Junior Buckeridge A.
 *
 */
public class ExceptionHandler implements Thread.UncaughtExceptionHandler{

	/**
	 * The context to send a message
	 */
	private static Context activityContext;
	/**
	 * The subject of the email
	 */
	private String subject;
	/**
	 * The email to send to
	 */
	private String emailTo;
	/**
	 * The default handler
	 */
	private UncaughtExceptionHandler defaultExceptionHandler;
	
	/**
	 * The exception handler
	 * @param currentHandler 
	 * @param subject The subject
	 * @param emailTo The email to send to
	 */
	public ExceptionHandler(UncaughtExceptionHandler currentHandler, String subject, String emailTo){
		this.defaultExceptionHandler = currentHandler;
		this.subject = subject;
		this.emailTo = emailTo;
	}
	
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		
		//build message here
		StringBuilder sb = new StringBuilder();
		String message = ex.getLocalizedMessage();
		StackTraceElement[] traces = ex.getStackTrace();
		sb.append("Message: "+message+"\n");
		for(StackTraceElement trace:traces){
			sb.append("at "+trace.getClassName()+"."+trace.getMethodName()+"("+trace.getFileName()+":"+trace.getLineNumber()+")\n");
		}
		
		//send intent
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
	            "mailto",emailTo, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, sb.toString());
		//Log.d("", sb.toString());
		
		if(activityContext != null){
			activityContext.startActivity(Intent.createChooser(emailIntent, "Send report"));
		}else{
			Log.e("", "appContext is NULL");
		}
		
		//pass throwable to original handler and let the app crash
		if(defaultExceptionHandler != null){
			defaultExceptionHandler.uncaughtException(thread, ex);
		}else{
			Log.d("", "defaultExceptionHandler is NULL");
			Thread.setDefaultUncaughtExceptionHandler(null);
			return;
		}
		
	}

	public static boolean register(Context context, 
			String subject, String emailTo) {
		
		activityContext = context;

		UncaughtExceptionHandler currentHandler = Thread.getDefaultUncaughtExceptionHandler();
		//if (debug) {
			if (currentHandler != null) {
				Log.d("", "current handler class=" + currentHandler.getClass().getName());
			}
		//}
		// don't register again if already registered
		if (!(currentHandler instanceof ExceptionHandler)) {
			// Register default exceptions handler
			Thread.setDefaultUncaughtExceptionHandler(
					new ExceptionHandler(currentHandler, subject, emailTo));
		}

		return true;
	}
}
