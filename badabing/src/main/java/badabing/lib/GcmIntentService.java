package badabing.lib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * The IntentService that handles the push notification
 * 
 * @author Junior Buckeridge A.
 * 
 */
public class GcmIntentService extends IntentService {

	private static final String KEY_TITLE = "title";
	private static final String KEY_URL = "url";
	private static final String KEY_MESSAGE = "message";
	private static final String KEY_MSG_ID = "msg_id";
	private static final String GCM_PREFIX = "gcm_cache_";
	private static long[] pattern = new long[] { 500, 500, 1000, 1000, 500, 500 };

	private static final String KEY_SOUND = "sound";
	private static final String KEY_ICON = "icon";
	private static final String KEY_VIBRATE = "vibrate";
	private static final String KEY_NOTIFY = "notify";
	private static final String KEY_ACTION = "action";
	private static final String KEY_PACKAGES = "packages";

	private String title = null;
	private String url = null;
	private String sound = null;
	private String icon = null;
	private int vibrate = 1;
	private int notify = 1;
	private String action = null;
	private JSONArray jPackages = null;

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {

			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {

				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				String message = "";
				String msg_id = null;
				boolean notDuplicated = true;
				// get data
				try {
					Bundle msgExtras = intent.getExtras();
					// Set<String> keys = msgExtras.keySet();
					// for(String key:keys){
					// try {
					// message = (key + ": "+msgExtras.get(key));
					// } catch (Exception e) {
					// e.printStackTrace();
					// }
					// }

					message = msgExtras.getString("message");

					try {
						JSONObject o = new JSONObject(message);
						msg_id = o.optString(KEY_MSG_ID, "0");
						title = o.optString(KEY_TITLE);
						url = o.optString(KEY_URL);
						if (o.has(KEY_MESSAGE)) {
							message = o.getString(KEY_MESSAGE);
						}

						sound = o.optString(KEY_SOUND, null);
						icon = o.optString(KEY_ICON, null);
						vibrate = o.optInt(KEY_VIBRATE, 1);
						notify = o.optInt(KEY_NOTIFY, 1);
						action = o.optString(KEY_ACTION, null);

						jPackages = o.optJSONArray(KEY_PACKAGES); // used to
																	// determine
																	// which app
																	// handles
																	// the
																	// message

					} catch (Exception e) {
						e.printStackTrace();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				// guarantee that only one package will handle the message
				if (jPackages != null) {
					for (int i = 0; i < jPackages.length(); i++) {
						try {
							String p = jPackages.getString(i);
							if (getPackageName().equalsIgnoreCase(p)) {
								break; // Ok, lets process the message
							} else {
								PackageManager pm = getPackageManager();
								try {
									pm.getPackageInfo(p,
											PackageManager.GET_ACTIVITIES);
									return; // this app is installed on the
											// system

								} catch (NameNotFoundException e) {
									// do nothing, 'cause this app is ot
									// installed
								}
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}

				SharedPreferences prefs = PreferenceManager
						.getDefaultSharedPreferences(this);
				String key = GCM_PREFIX + msg_id;
				if (prefs.contains(key)) {
					notDuplicated = false;
				} else {
					prefs.edit().putString(key, msg_id).commit();
				}

				if (notDuplicated) {
					// check intent
					if (!TextUtils.isEmpty(action)) {
						// Send intent
						try {
							Intent i = Intent.parseUri(action,
									Intent.URI_INTENT_SCHEME);
							i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(i);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					if (!TextUtils.isEmpty(message) && notify == 1) {
						// notifies user
						generateNotification(this, title, message, url);
					}
				}
			}
		}

		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private void generateNotification(Context context, String title,
			String message, String url) {

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		String packageName = context.getApplicationContext().getPackageName();
		ApplicationInfo info = null;
		int defaultIcon = 0;
		Intent defaultIntent = null;
		try {
			PackageManager pm = context.getApplicationContext()
					.getPackageManager();
			info = pm.getApplicationInfo(packageName, 0);
			defaultIcon = info.icon;
			defaultIntent = pm.getLaunchIntentForPackage(packageName);
			if (TextUtils.isEmpty(title)) {
				title = (String) pm.getApplicationLabel(info);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (TextUtils.isEmpty(title)) {
			title = ""; // this is just a sanity check
		}

		NotificationCompat.Builder ncb = new NotificationCompat.Builder(context);
		ncb.setAutoCancel(true);
		ncb.setLights(Color.GREEN, 500, 500);
		if (!TextUtils.isEmpty(icon)) {
			if (icon.equalsIgnoreCase("facebook")) {
				ncb.setSmallIcon(R.raw.ic_facebook);
				ncb.setColor(context.getResources().getColor(
						R.color.facebook_color));
			} else if (icon.equalsIgnoreCase("instagram")) {
				ncb.setSmallIcon(R.raw.ic_instagram);
				ncb.setColor(context.getResources().getColor(
						R.color.instagram_color));
			} else if (icon.equalsIgnoreCase("line")) {
				ncb.setSmallIcon(R.raw.ic_line);
				ncb.setColor(context.getResources()
						.getColor(R.color.line_color));
			} else if (icon.equalsIgnoreCase("twitter")) {
				ncb.setSmallIcon(R.raw.ic_twitter);
				ncb.setColor(context.getResources().getColor(
						R.color.twitter_color));
			} else if (icon.equalsIgnoreCase("whatsapp")) {
				ncb.setSmallIcon(R.raw.ic_whatsapp);
				ncb.setColor(context.getResources().getColor(
						R.color.whatsapp_color));
			} else {
				// fallback to default
				if (defaultIcon != 0)
					ncb.setSmallIcon(defaultIcon);
			}
		} else {
			if (defaultIcon != 0)
				ncb.setSmallIcon(defaultIcon);
		}

		if (!TextUtils.isEmpty(title))
			ncb.setContentTitle(title);
		ncb.setTicker(message);
		ncb.setContentText(message);
		if (vibrate == 1) {
			ncb.setVibrate(pattern);
		}
		if (!TextUtils.isEmpty(sound)) {
			Uri uriSound = null;
			// try {
			// uriSound =
			// Uri.parse("android.resource://"+context.getPackageName()+"/raw/"+sound);
			// } catch (Exception e) {
			// }
			// if(uriSound == null || sound.equalsIgnoreCase("default")){
			// uriSound =
			// RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			// }

			int soundId = getResources().getIdentifier(sound, "raw",
					getPackageName());
			if (soundId == 0 || sound.equalsIgnoreCase("sound_default")) {
				uriSound = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			} else {
				try {
					uriSound = Uri.parse("android.resource://"
							+ context.getPackageName() + "/raw/" + soundId);
				} catch (Exception e) {
				}
			}

			ncb.setSound(uriSound);

		}
		Uri uri = null;
		try {
			uri = Uri.parse(url);
		} catch (Exception e) {
		}
		PendingIntent pi = null;
		if (TextUtils.isEmpty(url) || uri == null) {
			// no url... start MainActivity
			// Intent i = new Intent(context, SplashActivity.class);
			if (defaultIntent != null) {
				defaultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				pi = PendingIntent.getActivity(context, 0, defaultIntent, 0);
			}

		} else {
			// go to URL
			pi = PendingIntent.getActivity(context, 0, new Intent(
					Intent.ACTION_VIEW, uri), 0);
		}
		if (pi != null)
			ncb.setContentIntent(pi);
		Notification notification = ncb.build();
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		notification.ledARGB = 0xff00ff00;
		notification.ledOnMS = 500;
		notification.ledOffMS = 1000;
		notificationManager.notify((int) System.currentTimeMillis(),
				notification);

	}

}
