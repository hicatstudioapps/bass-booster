package badabing.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Locale;


import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import badabing.lib.adapter.MoreAppsAdapter;
import badabing.lib.model.MoreAppsModel;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.preference.PreferenceManager;

/**
 * Library utils.
 * 
 * @author Junior Buckeridge A.
 *
 */
public class Utils {

	private static final String FILE_MORE_APPS_SERIAL = "badabing.lib.moreApps.ser";
	private static final String KEY_JSON_APPWALL = "badabing.lib.KEY_JSON_APPWALL";

	private static String URL_MORE_APPS = "https://www.dropbox.com/s/de7b1q4fssylbt6/appwall_2.json?dl=1";
	
	/**
	 * Called when we get more apps
	 * 
	 * @author Junior Buckeridge A.
	 *
	 */
	public interface MoreAppsListener {
		/**
		 * The list is refreshed
		 * 
		 * @param isFromCache
		 * @param list
		 */
		public void onMoreApps(boolean isFromCache, ArrayList<MoreAppsModel> list);
		/**
		 * The list is being refreshed from webservice
		 * @param list
		 */
		public void onRefresh(ArrayList<MoreAppsModel> list);
		/**
		 * An error has occurred while getting the apps
		 */
		public void onError();
	}
	
	/**
	 * Gets the default MoreAppsAdapter
	 * @param context
	 * @param list
	 * @return
	 */
	public static MoreAppsAdapter getDefaultAdapter(Context context, ArrayList<MoreAppsModel> list){
		return getDefaultAdapter(context, list, 0);
	}
	
	/**
	 * Gets the default MoreAppsAdapter with a custom drawable
	 * @param context
	 * @param list
	 * @param defaultDrawableRes
	 * @return
	 */
	public static MoreAppsAdapter getDefaultAdapter(Context context, ArrayList<MoreAppsModel> list, int defaultDrawableRes){
		return new MoreAppsAdapter(context, list, defaultDrawableRes);
	}
	
	/**
	 * Request to get more apps data model list
	 * 
	 * @param activity An activity for the context
	 * @param listener The listener for calling back
	 * @return asyncTask The task doing the job
	 */
	@SuppressWarnings("rawtypes")
	public static AsyncTask requestMoreApps(final Activity activity, final MoreAppsListener listener){
		AsyncTask<Void, Void, Integer> asyncTask = new AsyncTask<Void, Void, Integer>(){
			
			private ArrayList<MoreAppsModel> list;

			protected void onPreExecute() {
				list = getCachedList(activity);
				if(list != null && listener!=null){
					listener.onMoreApps(true, list);
				}
				
			};
			
			protected Integer doInBackground(Void... arg0) {
				
				int retry = 6;
				while(retry > 0){
					ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
					if(activity.isFinishing()){
						return null;
					}else if(cm.getActiveNetworkInfo()!=null){
						JSONObject o = getMoreApps(activity);
						if(o != null){
							ArrayList<MoreAppsModel> newList = loadMoreApps(o);
							if(newList == null){
								if(!activity.isFinishing() && listener!=null){
									listener.onError();
								}
							}else{
								if(list == null){
									list = new ArrayList<MoreAppsModel>();
								}
								MoreAppsModel app;
								for(int i=0; i<newList.size(); i++){
									app = newList.get(i);
									if(!list.contains(app)){
										list.add(app);
										publishProgress();
									}
								}
								
								//save to cache
								try {
									ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(activity.getFilesDir(), FILE_MORE_APPS_SERIAL)));
									oos.writeObject(list);
									oos.flush();
									oos.close();
									
								} catch (FileNotFoundException e) {
								} catch (IOException e) {
								}
								
								return 1;
							}
							
						}else if(!activity.isFinishing() && listener!=null){
							listener.onError();
						}
						
						break;
					}
					SystemClock.sleep(10000);
					retry--;
				}
				
				if(!activity.isFinishing() && listener!=null){
					listener.onError();
				}
				
				return null;
			};
			
			protected void onProgressUpdate(Void... values) {
				if(!activity.isFinishing() && listener!=null){
					listener.onRefresh(list);
				}
			};
			
			protected void onPostExecute(Integer result) {
				if(result!=null && result>0
					&& list != null && listener!=null
					&& !activity.isFinishing()){
					
					listener.onMoreApps(false, list);
				}
			};
			
		};
		asyncTask.execute();
		
		return asyncTask;
	}
	
	/**
	 * Gets the cached list of apps
	 * @param context
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static ArrayList<MoreAppsModel> getCachedList(Context context) {
		ArrayList<MoreAppsModel> list = null;
		try {
			ObjectInputStream dis = new ObjectInputStream(new FileInputStream(new File(context.getFilesDir(), FILE_MORE_APPS_SERIAL)));
			list = (ArrayList<MoreAppsModel>) dis.readObject();
			dis.close();
			
		} catch (Exception e) {
		}
		
		return list;
	}

	private static JSONObject getMoreApps(final Activity activity){
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		try {
			URLConnection conn = new URL(URL_MORE_APPS).openConnection();
			conn.addRequestProperty("Accept", "application/json");
			conn.connect();
			InputStream is = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			
			String str = sb.toString();
			sb = null;
			JSONObject o = new JSONObject(str);
			prefs.edit().putString(KEY_JSON_APPWALL, o.toString()).commit();
			return o;
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return null;
		
	}
	
	/**
	 * Loads the data based on the JSONObject obtained
	 * 
	 * @param o
	 * @return
	 */
	private static ArrayList<MoreAppsModel> loadMoreApps(JSONObject o) {
		ArrayList<MoreAppsModel> list = new ArrayList<MoreAppsModel>();
		try {
			JSONArray jArray = o.getJSONArray("menus").getJSONObject(0).getJSONArray("atributos");
			MoreAppsModel app;
			for(int i=0; i<jArray.length(); i++){
				app = getAppDetails(jArray.getJSONObject(i).getString("valor"));
				if(app != null){
					list.add(app);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}

	/**
	 * Gets the details of an app parsing the data from the market
	 * 
	 * @param appUrl
	 * @param activity
	 * @return
	 */
	private static MoreAppsModel getAppDetails(final String appUrl) {
		
		MoreAppsModel app = new MoreAppsModel("", appUrl, "", "");
		
//		try {
//			StringBuilder sb = new StringBuilder();
//			InputStream is = new URL(appUrl).openStream();
//			int cnt = 0;
//			byte [] buffer = new byte[8192];
//			while((cnt=is.read(buffer)) >= 0){
//				sb.append(new String(buffer, 0, cnt));
//			}
//			is.close();
//			
//			//parse data
//			final String data = sb.toString();
//			int index0 = data.indexOf("<img class=\"cover-image\" src=\"");
//			int index1 = data.indexOf("\"", index0+31);
//			//Log.d("", "length:"+data.length()+"; index0:"+index0+"; index1:"+index1);
//			String image = sb.substring(index0+30, index1);
//			
//			index0 = data.indexOf("<div class=\"document-title\" itemprop=\"name\"> <div>");
//			index1 = data.indexOf("</div>", index0+1);
//			String name = sb.substring(index0+50, index1);
//			
//			index0 = data.indexOf("<div class=\"id-app-orig-desc\">");
//			index1 = data.indexOf("</div>", index0+1);
//			String desc = sb.substring(index0+30, index1);
//			
//			Log.d("", name+" | "+image);
//			app = new MoreAppsModel(name, appUrl, desc, image);
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		try {
			Document doc = Jsoup.connect(app.getAppUrl() + "&hl=" + Locale.getDefault()
					.getLanguage()).timeout(30000).get();

			String description = doc.select("div[jsname=C4s9Ed]").first().ownText();

			Element img = doc.select("div.cover-container img").first();
			String urlImage = img.attr("src");
			if(urlImage.endsWith("-rw")){
				urlImage = urlImage.replace("-rw", "");
			}

			Element name = doc.select("div.document-title div").first();
			String appName = name.ownText();

			app.setAppImageUrl(urlImage);
			app.setAppName(appName);
			app.setAppDescription(description);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return app;
	}
	
	
}
